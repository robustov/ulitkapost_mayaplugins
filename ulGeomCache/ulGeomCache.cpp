//
// Copyright (C) 
// 
// File: ulGeomCacheNode.cpp
//
// Dependency Graph Node: ulGeomCache
//
// Author: Maya Plug-in Wizard 2.0
//

#include "ulGeomCache.h"

#include "pre.h"
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MPoint.h>

#include <maya/MGlobal.h>
#include <maya/MAnimControl.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnEnumAttribute.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>

MObject ulGeomCache::time;
MObject ulGeomCache::file;
MObject ulGeomCache::useMemory;
MObject ulGeomCache::startFrame;

ulGeomCache::ulGeomCache()
{
	cacheData = NULL;
	shapeData = NULL;
	cacheFrames = NULL;
}

ulGeomCache::~ulGeomCache()
{
	if ( cacheData ) delete[] cacheData;
	if ( shapeData ) delete[] shapeData;
	if ( cacheFrames ) delete[] cacheFrames;
}

MStatus ulGeomCache::deform(
	MDataBlock& data,
	MItGeometry& iter,
	const MMatrix& m,
	unsigned int multiIndex)
{
	MStatus stat;

	float envelopeVal = data.inputValue(this->envelope,&stat).asFloat();
	if( !stat)
	{
		displayStringD( "Node ulGeomCache cannot get value\n" );
		return MS::kFailure;
	}

	bool useMemoryVal = data.inputValue(this->useMemory,&stat).asBool();
	if( !stat)
	{
		displayStringD( "Node ulGeomCache cannot get value\n" );
		return MS::kFailure;
	}
	
	float timeVal = data.inputValue( this->time, &stat ).asFloat();
	if( !stat)
	{
		displayStringD( "Node ulGeomCache cannot get value\n" );
		return MS::kFailure;
	}

	float startFrameVal = data.inputValue( this->startFrame, &stat ).asFloat();
	if( !stat)
	{
		displayStringD( "Node ulGeomCache cannot get value\n" );
		return MS::kFailure;
	}

	MString fileVal = data.inputValue( this->file, &stat ).asString();
	if( !stat)
	{
		displayStringD( "Node ulGeomCache cannot get value\n" );
		return MS::kFailure;
	}

	if ( fileName != fileVal )
	{
		fileName = fileVal;

		if ( useMemoryVal && cacheFrames )
		{
			delete[] cacheData;
			delete[] cacheFrames;
			cacheData = NULL;
			cacheFrames = NULL;
		}

		if( shapeData )
		{
			delete[] shapeData;
			shapeData = NULL;
		}
	}

	FILE *stream;

	if ( !fileVal.length() )
	{
		displayString( "cannot find cache file" );
		return MS::kFailure;
	}

	char header[4] = {'a','b','c','d'};

	if( (stream = fopen( fileVal.asChar(), "rb" )) != NULL )
	{
		fread( header, sizeof( char ), 4, stream );
		if ( !strncmp( header, "ULGC", 4 ) )
		{
			fread( &version, sizeof( int ), 1, stream );
			fread( &frames, sizeof( int ), 1, stream );
			fread( &vtxCount, sizeof( int ), 1, stream );
		}
		else
		{
			displayString( "cannot read cache file" );
			return MS::kFailure;
		}
	}
	else
	{
		displayString( "cannot open cache file" );
		return MS::kFailure;
	}

	struct _stat buf;
	int fd = _fileno( stream );

	if ( !_fstat( fd, &buf ) )
	{
        MString modTime( ctime( &buf.st_mtime ) );
		if ( modificationTime != modTime )
		{
			modificationTime = modTime;

			if ( useMemoryVal && cacheFrames )
			{
				delete[] cacheData;
				delete[] cacheFrames;
				cacheData = NULL;
				cacheFrames = NULL;
			}

			if( shapeData )
			{
				delete[] shapeData;
				shapeData = NULL;
			}
		}
	}

	timeVal -= startFrameVal;

	if ( timeVal < 0 ) timeVal = 0;
	if ( timeVal > ( frames - 1 ) ) timeVal = (float)(frames - 1);

	int iTimeVal = (int)timeVal;
	int nextTime = iTimeVal + 1;

	//if ( timeVal < 0 || timeVal > ( frames - 1 ) ) return MS::kFailure;

	float offset = timeVal - iTimeVal;

	int pointsCount = vtxCount*3;
	int pointsCountOffset = pointsCount*(int)(ceil(offset)+1);

	if( !shapeData )
	{
		shapeData = new float[pointsCount];
		fseek( stream, sizeof(char)*4 + sizeof(int)*3, SEEK_SET );
		fread( shapeData, sizeof( float ), pointsCount, stream );
	}

	if ( useMemoryVal )
	{
		if ( !cacheFrames )
		{
			cacheData = new float[frames*pointsCount];
			cacheFrames = new bool[frames];

			for ( int i = 0; i < frames; i++ )
			{
				cacheFrames[i] = false;
			}
		}

		if ( !cacheFrames[iTimeVal] )
		{
			fseek( stream, sizeof(char)*4 + sizeof(int)*3 + sizeof(float)*pointsCount*iTimeVal, SEEK_SET );
			fread( &cacheData[pointsCount*iTimeVal], sizeof( float ), pointsCount, stream );

			cacheFrames[iTimeVal] = true;
		}

		if ( offset > 0 )
		{
			if ( !cacheFrames[nextTime] )
			{
				fseek( stream, sizeof(char)*4 + sizeof(int)*3 + sizeof(float)*pointsCount*nextTime, SEEK_SET );
				fread( &cacheData[pointsCount*nextTime], sizeof( float ), pointsCount, stream );
				cacheFrames[nextTime] = true;
			}
		}
	}
	else
	{
		if ( cacheData )
		{
			delete[] cacheData;
			delete[] cacheFrames;
			cacheData = NULL;
			cacheFrames = NULL;
		}

		fseek( stream, sizeof(char)*4 + sizeof(int)*3 + sizeof(float)*pointsCount*iTimeVal, SEEK_SET );
		cacheData = new float[pointsCountOffset];
		fread( cacheData, sizeof( float ), pointsCountOffset, stream );
	}

	fclose( stream );

	if ( !useMemoryVal )
	{
		iTimeVal = 0;
		nextTime = 1;
	}

	if ( offset == 0 ) nextTime = 0;

	for ( iter.reset(); !iter.isDone(); iter.next()) 
	{
		int i = iter.index();
		float weight = weightValue( data, multiIndex, i );

		MPoint position = iter.position( MSpace::kObject );

		//position.x += (( cacheData[pointsCount*iTimeVal+i*3] + (cacheData[pointsCount*nextTime+i*3] - cacheData[pointsCount*iTimeVal+i*3])*offset ) * weight - position.x)*envelopeVal;
		//position.y += (( cacheData[pointsCount*iTimeVal+i*3+1] + (cacheData[pointsCount*nextTime+i*3+1] - cacheData[pointsCount*iTimeVal+i*3+1])*offset ) * weight - position.y)*envelopeVal;
		//position.z += (( cacheData[pointsCount*iTimeVal+i*3+2] + (cacheData[pointsCount*nextTime+i*3+2] - cacheData[pointsCount*iTimeVal+i*3+2])*offset ) * weight - position.z)*envelopeVal;
		position.x += (( cacheData[pointsCount*iTimeVal+i*3] + (cacheData[pointsCount*nextTime+i*3] - cacheData[pointsCount*iTimeVal+i*3])*offset - position.x ) * weight )*envelopeVal;
		position.y += (( cacheData[pointsCount*iTimeVal+i*3+1] + (cacheData[pointsCount*nextTime+i*3+1] - cacheData[pointsCount*iTimeVal+i*3+1])*offset - position.y ) * weight )*envelopeVal;
		position.z += (( cacheData[pointsCount*iTimeVal+i*3+2] + (cacheData[pointsCount*nextTime+i*3+2] - cacheData[pointsCount*iTimeVal+i*3+2])*offset - position.z ) * weight )*envelopeVal;

		iter.setPosition( position, MSpace::kObject );
	}

	if ( !useMemoryVal && cacheData )
	{
		delete[] cacheData;
		cacheData = NULL;
	}

	return MS::kSuccess;
}

void* ulGeomCache::creator()
{
	return new ulGeomCache();
}

MStatus ulGeomCache::initialize()
{
	MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;

	MStatus				stat;

	try
	{
		{
			useMemory = nAttr.create( "useMemory", "um", MFnNumericData::kBoolean, true );
			::addAttribute(useMemory, atInput|atKeyable);
			stat = attributeAffects( useMemory, outputGeom );

			startFrame = nAttr.create( "startFrame", "sf", MFnNumericData::kFloat, 1.0 );
			::addAttribute(startFrame, atInput|atKeyable);
			stat = attributeAffects( startFrame, outputGeom );

			file = tAttr.create("cacheFile", "cf", MFnData::kString );
			::addAttribute(file,atInput);
			stat = attributeAffects( file, outputGeom );

			time = nAttr.create( "time", "t", MFnNumericData::kFloat, 1.0 );
			::addAttribute(time, atInput|atKeyable);
			stat = attributeAffects( time, outputGeom );
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}