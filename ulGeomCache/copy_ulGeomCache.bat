set BIN_DIR=.\out\ulGeomCache
set BIN_DIR=\\server.ulitka.home\bin70

mkdir %BIN_DIR%
mkdir %BIN_DIR%\bin
mkdir %BIN_DIR%\mel
mkdir %BIN_DIR%\docs

copy bin\ulGeomCache.mll			%BIN_DIR%\bin
copy mel\AEulGeomCacheTemplate.mel	%BIN_DIR%\mel
copy mel\AEulGeomSplitterTemplate.mel	%BIN_DIR%\mel
copy mel\ulGeomCacheMenu.mel		%BIN_DIR%\mel
copy mel\ulGeomCacheUI.mel			%BIN_DIR%\mel
copy mel\ulGeomSplitter.mel			%BIN_DIR%\mel

pause