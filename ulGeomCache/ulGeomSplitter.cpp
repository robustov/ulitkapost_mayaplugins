//
// Copyright (C) 
// 
// File: ulGeomCacheNode.cpp
//
// Dependency Graph Node: ulGeomCache
//
// Author: Maya Plug-in Wizard 2.0
//

#include "ulGeomSplitter.h"

#include "pre.h"
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>

#include <maya/MGlobal.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnEnumAttribute.h>

#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MItMeshPolygon.h>

MObject ulGeomSplitter::i_mesh;
MObject ulGeomSplitter::o_mesh[2];
MObject ulGeomSplitter::faces;
MObject ulGeomSplitter::type;
MObject ulGeomSplitter::merge;
MObject ulGeomSplitter::tolerance;
MObject ulGeomSplitter::uvs;
MObject ulGeomSplitter::normals;

void* ulGeomSplitter::creator()
{
	return new ulGeomSplitter();
}

MStatus ulGeomSplitter::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	MObject inMesh;
	MIntArray facesArray1;
	MIntArray facesArray2;
	MFnMeshData meshData;
	MObject mesh;
	MFnMesh meshFn;
	bool merge, uvs, normals;
	float tolerance;
	MStringArray setNames;

	if( plug == o_mesh[0] || plug == o_mesh[1] )
	{
		inMesh = data.inputValue( i_mesh, &stat ).asMesh();
		MString facesString = data.inputValue( this->faces, &stat ).asString();
		short type = data.inputValue( this->type, &stat ).asShort();
		merge = data.inputValue( this->merge, &stat ).asBool();
		tolerance = data.inputValue( this->tolerance, &stat ).asFloat();
		uvs = data.inputValue( this->uvs, &stat ).asBool();
		normals = data.inputValue( this->normals, &stat ).asBool();

		MStringArray facesStringArray;
		facesString.split( ' ', facesStringArray );

		MFnMesh inMeshFn( inMesh );
		int numPolygons = inMeshFn.numPolygons();

		inMeshFn.getUVSetNames( setNames );

		MIntArray facesArray;

		for( unsigned int i = 0; i < facesStringArray.length(); i++ )
		{
			int face = facesStringArray[i].asInt();

			if( face >= 0 && face < numPolygons )
			{
                facesArray1.append( face );
			}
		}

		for( int i = 0; i < numPolygons; i++ )
		{
			unsigned int k;
			for( k = 0; k < facesArray1.length(); k++ )
			{ 
				if( facesArray1[k] == i )
				{
					break;
				}
			}
			if( facesArray1.length() == k )
			{
                facesArray2.append( i );
			}
		}

		mesh = meshData.create();
	}

	if( plug == o_mesh[0] )
	{
		//MGlobal::displayInfo("o_mesh[0]");
		MDataHandle out = data.outputValue( plug, &stat );

		MItMeshPolygon iter( inMesh );
		MPointArray vertexArray;
		int uvId = 0;

		MIntArray setsUvIds;
		setsUvIds.setLength( setNames.length() );

		meshFn.setObject( mesh );
		//MGlobal::displayInfo( MString("Fn:") + meshFn.typeName() );
		for( unsigned int i = 0; i < setNames.length(); i++ )
		{
			meshFn.createUVSet( setNames[i] );
			//MGlobal::displayInfo(MString("setNames[i]:")+setNames[i]);
			setsUvIds[i] = 0;
		}

		for( unsigned int i = 0; i < facesArray1.length(); i++ )
		{
			int preIndex, faceId;
			iter.setIndex(facesArray1[i], preIndex );
			iter.getPoints( vertexArray );

			MFloatArray uArray;
			MFloatArray vArray;
			if(uvs) iter.getUVs( uArray, vArray );

			MVectorArray faceNormals;
			if(normals) iter.getNormals( faceNormals );

			meshFn.addPolygon(vertexArray, faceId, merge, tolerance, mesh );

			for( unsigned int k = 0; k < vertexArray.length(); k++ )
			{
				if( uvs && uArray.length() )
				{
					meshFn.setUV( uvId, uArray[k], vArray[k] );
					meshFn.assignUV( faceId, k, uvId++ );
				}

				if(normals)
				{
					meshFn.setFaceVertexNormal( faceNormals[k], faceId, k );
				}
			}
		}

		//for( unsigned int i = 0; i < facesArray1.length(); i++ )
		//{
		//	int preIndex, faceId;
		//	iter.setIndex(facesArray1[i], preIndex );
		//	iter.getPoints( vertexArray );

		//	MVectorArray faceNormals;
		//	if(normals) iter.getNormals( faceNormals );

		//	meshFn.addPolygon(vertexArray, faceId, merge, tolerance, mesh );

		//	for( unsigned int k = 0; k < vertexArray.length(); k++ )
		//	{
		//		if(uvs)
		//		{
		//			for( unsigned int l = 0; l < setNames.length(); l++ )
		//			{
		//				MFloatArray uArray;
		//				MFloatArray vArray;
		//				iter.getUVs( uArray, vArray, &setNames[l] );
		//				if( uArray.length() )
		//				{
		//					//MGlobal::displayInfo(MString("SET FOR:")+setNames[l]);
		//					meshFn.setUV( setsUvIds[l], uArray[k], vArray[k], &setNames[l] );
		//					meshFn.assignUV( faceId, k, setsUvIds[l], &setNames[l] );
		//					setsUvIds[l] += 1;
		//				}
		//			}
		//		}

		//		if(normals)
		//		{
		//			meshFn.setFaceVertexNormal( faceNormals[k], faceId, k );
		//		}
		//	}
		//}

		out.set( mesh );

		//MFnMesh meshFnTemp( out.asMesh() );
		//MGlobal::displayInfo( MString("TEMP:") + meshFnTemp.typeName() );

		data.setClean(plug);
		return MS::kSuccess;
	}

	if( plug == o_mesh[1] )
	{
		//MGlobal::displayInfo("o_mesh[1]");
		MDataHandle out = data.outputValue( plug, &stat );

		MItMeshPolygon iter( inMesh );
		MPointArray vertexArray;
		int uvId = 0;

		for( unsigned int i = 0; i < facesArray2.length(); i++ )
		{
			int preIndex, faceId;
			iter.setIndex(facesArray2[i], preIndex );
			iter.getPoints( vertexArray );

			MFloatArray uArray;
			MFloatArray vArray;
			if(uvs) iter.getUVs( uArray, vArray );

			MVectorArray faceNormals;
			if(normals) iter.getNormals( faceNormals );

			meshFn.addPolygon(vertexArray, faceId, merge, tolerance, mesh );

			for( unsigned int k = 0; k < vertexArray.length(); k++ )
			{
				if( uvs && uArray.length() )
				{
					meshFn.setUV( uvId, uArray[k], vArray[k] );
					meshFn.assignUV( faceId, k, uvId++ );
				}

				if(normals)
				{
					meshFn.setFaceVertexNormal( faceNormals[k], faceId, k );
				}
			}
		}

		out.set( mesh );

		//out.set( inMesh );
		//MFnMesh meshFn( out.asMesh() );
		//meshFn.deleteFace(1);

		data.setClean(plug);
		return MS::kSuccess;
	}

	return MS::kUnknownParameter;
}

MStatus ulGeomSplitter::initialize()
{
	MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnEnumAttribute eAttr;

	MStatus stat;

	try
	{
		{
			o_mesh[0] = tAttr.create ("outMesh1", "om_1", MFnData::kMesh, &stat);
			::addAttribute(o_mesh[0], atOutput);
			o_mesh[1] = tAttr.create ("outMesh2", "om_2", MFnData::kMesh, &stat);
			::addAttribute(o_mesh[1], atOutput);
			//attributeAffects( o_mesh[0], o_mesh[1] );

			type = eAttr.create ("type","ty", 0, &stat);
			eAttr.addField("INCLUDE", 0);
			eAttr.addField("EXCLUDE", 1);
			::addAttribute(type, atInput);
			attributeAffects( type, o_mesh[0] );
			attributeAffects( type, o_mesh[1] );

			merge = nAttr.create ("merge", "mr", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(merge, atInput|atKeyable);	
			attributeAffects( merge, o_mesh[0] );
			attributeAffects( merge, o_mesh[1] );

			tolerance = nAttr.create ("tolerance", "tlr", MFnNumericData::kFloat, 0.001, &stat);
			::addAttribute(tolerance, atInput|atKeyable);	
			attributeAffects( tolerance, o_mesh[0] );
			attributeAffects( tolerance, o_mesh[1] );

			uvs = nAttr.create ("uvs", "uvs", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(uvs, atInput|atKeyable);	
			attributeAffects( uvs, o_mesh[0] );
			attributeAffects( uvs, o_mesh[1] );

			normals = nAttr.create ("normals", "nml", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(normals, atInput|atKeyable);	
			attributeAffects( normals, o_mesh[0] );
			attributeAffects( normals, o_mesh[1] );

			i_mesh = tAttr.create ("inMesh", "im_0", MFnData::kMesh, &stat);
			::addAttribute(i_mesh, atInput);
			stat = attributeAffects( i_mesh, o_mesh[0] );
			stat = attributeAffects( i_mesh, o_mesh[1] );

			faces = tAttr.create("faces", "f_0", MFnData::kString );
			::addAttribute(faces,atInput);
			stat = attributeAffects( faces, o_mesh[0] );
			stat = attributeAffects( faces, o_mesh[1] );
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}