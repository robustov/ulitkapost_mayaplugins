#include "pre.h"
#include <maya/MFnPlugin.h>
#include "ulGeomCacheExport.h"
#include "ulGeomCache.h"
#include "ulGeomSplitter.h"

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

// #include "ulGeomCache.h"
	stat = plugin.registerNode( 
		ulGeomCache::typeName, 
		ulGeomCache::id, 
		ulGeomCache::creator, 
		ulGeomCache::initialize, 
		MPxNode::kDeformerNode );
	if (!stat) 
	{
		displayString("cant register %s", ulGeomCache::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "ulGeomCacheExport.h"
	stat = plugin.registerCommand( 
		ulGeomCacheExport::typeName.asChar(), 
		ulGeomCacheExport::creator, 
		ulGeomCacheExport::newSyntax );
	if (!stat) 
	{
		displayString("cant register %s", ulGeomCacheExport::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

// #include "ulGeomSplitter.h"
	stat = plugin.registerNode( 
		ulGeomSplitter::typeName, 
		ulGeomSplitter::id, 
		ulGeomSplitter::creator, 
		ulGeomSplitter::initialize, 
		MPxNode::kDependNode );
	if (!stat) 
	{
		displayString("cant register %s", ulGeomSplitter::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	if( !MGlobal::sourceFile("ulGeomCacheMenu.mel"))
	{
		displayString("error source AEmyCmdTemplate.mel");
	}
	else
	{
		if( !MGlobal::executeCommand("ulGeomCacheDebugMenu()"))
		{
			displayString("Dont found ulGeomCacheDebugMenu() command");
		}
		MGlobal::executeCommand("ulGeomCache_RegistryPlugin()");
	}

	MGlobal::executeCommand( "makePaintable  -at \"multiFloat\" -sm \"deformer\" \"ulGeomCache\" \"weights\";" );

	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterNode( ulGeomCache::id );
	if (!stat) displayString("cant deregister %s", ulGeomCache::typeName.asChar());

	stat = plugin.deregisterNode( ulGeomSplitter::id );
	if (!stat) displayString("cant deregister %s", ulGeomSplitter::typeName.asChar());

	stat = plugin.deregisterCommand( "ulGeomCacheExport" );
	if (!stat) displayString("cant deregister %s", ulGeomCacheExport::typeName.asChar());


	MGlobal::executeCommand("ulGeomCacheDebugMenuDelete()");
	return stat;
}
