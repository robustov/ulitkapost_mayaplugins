#include "pre.h"

const unsigned TypeIdBase = 0xda101;

// node static ids and names
#include "ulGeomCacheExport.h"
const MString ulGeomCacheExport::typeName( "ulGeomCacheExport" );


#include "ulGeomCache.h"
MTypeId ulGeomCache::id( TypeIdBase+0 );
const MString ulGeomCache::typeName( "ulGeomCache" );

#include "ulGeomSplitter.h"
MTypeId ulGeomSplitter::id( TypeIdBase+1 );
const MString ulGeomSplitter::typeName( "ulGeomSplitter" );

