//
// Copyright (C) 
// File: ulGeomCacheExportCmd.cpp
// MEL Command: ulGeomCacheExport

#include "ulGeomCacheExport.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>

//#include <maya/MStatus.h>
//#include <maya/MObject.h>
//#include <maya/MString.h>
//#include <maya/MSelectionList.h>
//#include <maya/MFnDependencyNode.h>
//#include <maya/MFnDagNode.h>
#include <maya/MDagPath.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFnMesh.h>
#include <maya/MAnimControl.h>
//#include <maya/MTime.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatPointArray.h>
//#include <maya/MPlug.h>

//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <math.h>

ulGeomCacheExport::ulGeomCacheExport()
{
}
ulGeomCacheExport::~ulGeomCacheExport()
{
}

MSyntax ulGeomCacheExport::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

//	syntax.addArg(MSyntax::kString);	// arg0
//	syntax.addFlag("f", "format", MSyntax::kString);

	syntax.addFlag("-f", "-file", MSyntax::kString);

	syntax.addFlag("-t", "-time", MSyntax::kTime, MSyntax::kTime );

	syntax.addFlag("-ws", "-worldSpace");

	syntax.useSelectionAsDefault(true);
	syntax.setObjectType( MSyntax::kSelectionList, 1, 1 );

	return syntax;
}

MStatus ulGeomCacheExport::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MStatus timeRange;
	MArgDatabase argData(syntax(), args);

//	MString argsl0;
//	argData.getCommandArgument(0, argsl0);

	MString file;
	argData.getFlagArgument( "-f", 0, file );

	MTime begin, end;
	argData.getFlagArgument( "-t", 0, begin );
	timeRange = argData.getFlagArgument( "-t", 1, end );

	bool worldSpace;
	worldSpace = argData.isFlagSet( "-ws" );

	MSelectionList list;
	argData.getObjects( list );

	if ( list.isEmpty() ) return MS::kFailure;

	MObject obj;

	list.getDependNode( 0, obj );
	MFnDagNode dag( obj );

	MFnMesh mesh;
	
	switch ( obj.apiType() )
	{
		case MFn::kTransform:
		{
			if ( dag.child(0).apiType() == MFn::kMesh )
			{
				mesh.setObject( dag.child(0) );
			}
			break;
		}

		case MFn::kMesh:
		{
			mesh.setObject( obj );
			break;
		}
	}

	if ( mesh.object().isNull() )
	{
		MGlobal::displayInfo(MString("select mesh"));
		return MS::kFailure;
	}

	FILE *stream;

	char header[4] = {'U','L','G','C'};
	int version = 1;

	if ( !file.length() ) file = "c:/cache.ugc";

	if( (stream = fopen( file.asChar(), "wb" )) != NULL )
	{
		fwrite( header, sizeof( char ), 4, stream );
		fwrite( &version, sizeof( int ), 1, stream );
	}
	else
	{
		MGlobal::displayInfo(MString("file error"));
		return MS::kFailure;
	}

	MTime cur = MAnimControl::currentTime();

	if ( !timeRange )
	{
		begin = MAnimControl::minTime();
		end = MAnimControl::maxTime();
	}

	int frames = (int)( end.value() - begin.value() + 1 );
	int vtxCount = mesh.numVertices();

	fwrite( &frames, sizeof( int ), 1, stream );
	fwrite( &vtxCount, sizeof( int ), 1, stream );

	MFloatPointArray vertices;

	for ( MTime i = begin; i <= end; i++ )
	{
		MGlobal::viewFrame( i );

		mesh.getPoints( vertices, MSpace::kObject );

		if( worldSpace )
		{
			MDagPath path;
			mesh.getPath( path );
			MFloatMatrix matrix( path.inclusiveMatrix().matrix );

			for ( unsigned int i = 0; i < vertices.length(); i++ )
			{
				vertices[i] *= matrix;
			}
		}

		for ( unsigned int i = 0; i < vertices.length(); i++ )
		{
			fwrite( &vertices[i].x, sizeof( float ), 1, stream );
			fwrite( &vertices[i].y, sizeof( float ), 1, stream );
			fwrite( &vertices[i].z, sizeof( float ), 1, stream );
		}
	}

	fclose( stream );

	MAnimControl::setCurrentTime( cur );

	MGlobal::displayInfo( MString("Cache file: ") + file );

	return MS::kSuccess;
}

void* ulGeomCacheExport::creator()
{
	return new ulGeomCacheExport();
}