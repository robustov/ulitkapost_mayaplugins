#pragma once
#include "pre.h"

//
// Copyright (C) 
// File: ulGeomCacheNode.h
//
// Deformer Node: ulGeomCache

#include <maya/MPxNode.h>
#include <maya/MPxDeformerNode.h>
#include <maya/MDataBlock.h>
#include <maya/MPlug.h>
#include <maya/MItGeometry.h>
#include <maya/MMatrix.h>

#include <maya/MTypeId.h> 

class ulGeomCache : public MPxDeformerNode
{
public:
	ulGeomCache();
	virtual	~ulGeomCache();

    virtual MStatus deform(
		MDataBlock& data,
		MItGeometry& iter,
		const MMatrix& mat,
		unsigned int multiIndex);

	static void* creator();
	static MStatus initialize();

public:
	static MObject time;		// Example input attribute
	static MObject file;
	static MObject useMemory;
	static MObject startFrame;

public:
	static MTypeId id;
	static const MString typeName;

private:
	float* cacheData;
	float* shapeData;
	bool* cacheFrames;

	MString fileName;
	MString modificationTime;

	int version;
	int frames;
	int vtxCount;
};

