#pragma once
#include "pre.h"

//
// Copyright (C) 
// File: ulGeomCacheNode.h
//
// Deformer Node: ulGeomCache

#include <maya/MPxNode.h>
#include <maya/MDataBlock.h>
#include <maya/MPlug.h>

#include <maya/MTypeId.h> 

class ulGeomSplitter : public MPxNode
{
public:
	virtual MStatus compute( const MPlug& plug, MDataBlock& dataBlock );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_mesh;
	static MObject o_mesh[2];
	static MObject faces;
	static MObject type;
	static MObject merge;
	static MObject tolerance;
	static MObject uvs;
	static MObject normals;

public:
	static MTypeId id;
	static const MString typeName;
};

