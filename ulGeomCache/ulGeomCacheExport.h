#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ulGeomCacheExportCmd.h
// MEL Command: ulGeomCacheExport

#include <maya/MPxCommand.h>

class ulGeomCacheExport : public MPxCommand
{
public:
	static const MString typeName;
	ulGeomCacheExport();
	virtual	~ulGeomCacheExport();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

