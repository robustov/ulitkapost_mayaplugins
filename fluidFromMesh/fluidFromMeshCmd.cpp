//
// Copyright (C) 
// File: fluidFromMeshCmdCmd.cpp
// MEL Command: fluidFromMeshCmd

#include "fluidFromMeshCmd.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MFnFluid.h>
#include <maya/MFnMesh.h>
#include <maya/MDagPath.h>
#include <maya/MFnParticleSystem.h>
#include <maya/MVectorArray.h>
#include <maya/MAnimControl.h>
#include "Util/evaltime.h"
/*/
loadPlugin fluidFromMesh.mll;
fluidFromMeshCmd pPlaneShape1 fluidShape1;
/*/

fluidFromMeshCmd::fluidFromMeshCmd()
{
}
fluidFromMeshCmd::~fluidFromMeshCmd()
{
}

MSyntax fluidFromMeshCmd::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.setObjectType(MSyntax::kSelectionList, 2);
//	syntax.addArg(MSyntax::kString);	// mesh
//	syntax.addArg(MSyntax::kString);	// fluid
//	syntax.addArg(MSyntax::kString);	// fluid
	syntax.addFlag("v", "verbose", MSyntax::kBoolean);
	syntax.addFlag("md", "max_density", MSyntax::kDouble);
	syntax.addFlag("fd", "factor_density", MSyntax::kDouble);
	syntax.addFlag("pfd", "particle_factor_density", MSyntax::kDouble);
	return syntax;
}

bool isValid(int3 index, unsigned int res[3])
{
	if(index[0]<0 || index[0]>=(int)res[0]) return false;
	if(index[1]<0 || index[1]>=(int)res[0]) return false;
	if(index[2]<0 || index[2]>=(int)res[0]) return false;
	return true;
}
bool isValid(int x, int y, int z, unsigned int res[3])
{
	if(x<0 || x>=(int)res[0]) return false;
	if(y<0 || y>=(int)res[0]) return false;
	if(z<0 || z>=(int)res[0]) return false;
	return true;
}
MStatus fluidFromMeshCmd::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MSelectionList sel;
	argData.getObjects(sel);

	std::list<MObject> affectobjects;
	MObject fluidobj;
	for(int a=0; a<(int)sel.length(); a++)
	{
		MObject depNode;
		if( !sel.getDependNode(a, depNode))
			continue;

		if( depNode.hasFn(MFn::kFluid))
		{
			fluidobj = depNode;
			continue;
		}
		if( depNode.hasFn(MFn::kMesh) ||
			depNode.hasFn(MFn::kParticle) )
		{
			affectobjects.push_back(depNode);
			continue;
		}
	}
//	MString meshname, fluidname;
//	argData.getCommandArgument(0, meshname);
//	argData.getCommandArgument(1, fluidname);

//	MObject meshobj, fluidobj;
//	if( !nodeFromName(meshname, meshobj))
//		return MS::kFailure;
//	if( !nodeFromName(fluidname, fluidobj))
//		return MS::kFailure;

	bool verbose=false;
	double max_density = 1, factor_density = 1, particle_factor_density=0.01;
	argData.getFlagArgument("v", 0, verbose);
	argData.getFlagArgument("md", 0, max_density);
	argData.getFlagArgument("fd", 0, factor_density);
	argData.getFlagArgument("pfd", 0, particle_factor_density);

	MFnFluid fluid(fluidobj, &stat);
	if(!stat) MS::kFailure;

	MDagPath fluidpath;
	MDagPath::getAPathTo(fluidobj, fluidpath);

	unsigned int res[3];
	fluid.getResolution(res[0], res[1], res[2]);
	double dims[3];
	fluid.getDimensions(dims[0], dims[1], dims[2]);

	float* densityGrid = fluid.density();
	if( !densityGrid) 
		return MS::kFailure;

	Util::FunEvalTimeCounter etc;
	float cellcount = (float)(res[2]*res[1]*res[0]);
	if(cellcount==0) cellcount=1;

	{
		Util::FunEvalTime fet(etc);
		// clear
		{
			for(unsigned int z = 0; z<res[2]; z++)
			{
				for(unsigned int y = 0; y<res[1]; y++)
					for(unsigned int x = 0; x<res[0]; x++)
					{
						int find = fluid.index(x, y, z);
						densityGrid[find] = 0;
					}
			}
		}

		std::list<MObject>::iterator it = affectobjects.begin();
		for(;it != affectobjects.end(); it++)
		{
			MObject obj = *it;

			MDagPath path;
			MDagPath::getAPathTo(obj, path);

			MFnMesh mesh(obj, &stat);
			if( stat)
			{
				MMatrix meshmat = path.inclusiveMatrixInverse();
				MMatrix fluidmat = fluidpath.inclusiveMatrix();
				MMatrix m = fluidmat*meshmat;

				for(unsigned int z = 0; z<res[2]; z++)
				{
					for(unsigned int y = 0; y<res[1]; y++)
					{
						for(unsigned int x = 0; x<res[0]; x++)
						{
							MPoint objectSpacePoint;
							stat = fluid.voxelCenterPosition(x, y, z, objectSpacePoint);
							objectSpacePoint = objectSpacePoint*m;
							int find = fluid.index(x, y, z);

							float val = 0;

							MPoint theClosestPoint;
							MVector theNormal;
							mesh.getClosestPointAndNormal(
								objectSpacePoint, theClosestPoint, theNormal);

							MVector dir = theClosestPoint - objectSpacePoint;
							double dot = dir*theNormal;

							if(dot>0)
								val = (float)(dir.length()*factor_density);

							densityGrid[find] += val;
						}
					}
				}
			}
			MFnParticleSystem particle(obj, &stat);
			if( stat)
			{
				MMatrix meshmat = path.inclusiveMatrix();
				MMatrix fluidmat = fluidpath.inclusiveMatrixInverse();
				MMatrix m = meshmat*fluidmat;

				// 
				MTime t = MAnimControl::currentTime();
				particle.evaluateDynamics(t, false);
				MVectorArray pos;
				particle.position(pos);
				MDoubleArray opacity;
				particle.opacity(opacity);
				for(int i=0; i<(int)pos.length(); i++)
				{
					MVector p = pos[i];
					double w = 1;
					if((int)opacity.length()>i)
						w = opacity[i];
					w*=particle_factor_density;

					MPoint fluidSpacePoint = MPoint(p)*m;

					if( fluidSpacePoint.x<-dims[0]/2 || fluidSpacePoint.x>dims[0]/2) continue;
					if( fluidSpacePoint.y<-dims[1]/2 || fluidSpacePoint.y>dims[1]/2) continue;
					if( fluidSpacePoint.z<-dims[2]/2 || fluidSpacePoint.z>dims[2]/2) continue;

					double3 xyz0, xyz1;
					int3 index0, index1;
					if(true)
					{
						int3 index;
						double3 xyz;
						fluidSpacePoint	+= MVector(dims[0]/2, dims[1]/2, dims[2]/2);
						fluidSpacePoint.x *= res[0]/dims[0];
						fluidSpacePoint.y *= res[1]/dims[1];
						fluidSpacePoint.z *= res[2]/dims[2];

						index[0] = (int)floor( fluidSpacePoint.x-0.5);
						index[1] = (int)floor( fluidSpacePoint.y-0.5);
						index[2] = (int)floor( fluidSpacePoint.z-0.5);
						xyz[0] = fluidSpacePoint.x - 0.5 - index[0];
						xyz[1] = fluidSpacePoint.y - 0.5 - index[1];
						xyz[2] = fluidSpacePoint.z - 0.5 - index[2];

						memcpy( index0, index, sizeof(index));
						index1[0] = index[0]+1;
						index1[1] = index[1]+1;
						index1[2] = index[2]+1;
						memcpy( xyz0, xyz, sizeof(xyz));
						xyz1[0] = 1-xyz[0];
						xyz1[1] = 1-xyz[1];
						xyz1[2] = 1-xyz[2];
	//					if( !isValid(index, res)) continue;

						for(int i=0; i<8; i++)
						{
							int ix = (i&1)?index1[0]:index0[0];
							int iy = (i&2)?index1[1]:index0[1];
							int iz = (i&4)?index1[2]:index0[2];
							double x = (i&1)?xyz0[0]:xyz1[0];
							double y = (i&2)?xyz0[1]:xyz1[1];
							double z = (i&4)?xyz0[2]:xyz1[2];
							float www = (float)(w*x*y*z);
							if( !isValid(ix, iy, iz, res))
								continue;

							int find = fluid.index(ix, iy, iz);
							densityGrid[find] += (float)www;
						}
					}
					else
					{
						int3 index;

						// ����� ������
						if( !fluid.toGridIndex(fluidSpacePoint, index))
							continue;
						int find = fluid.index(index[0], index[1], index[2]);

						// ��������� �������
						densityGrid[find] += (float)w;
					}
				}
			}
		}

		// ������������ �� max_density
		{
			int maxcells = 0;
			int emptycells = 0;
			for(unsigned int z = 0; z<res[2]; z++)
			{
				for(unsigned int y = 0; y<res[1]; y++)
				{
					for(unsigned int x = 0; x<res[0]; x++)
					{
						int find = fluid.index(x, y, z);
						float val = densityGrid[find];
						if( val>(float)max_density)
						{
							val=(float)max_density;
							maxcells++;
						}
						if( val==0)
						{
							emptycells++;
						}
						densityGrid[find] = val;
					}
				}
			}
			if(verbose)
			{
				displayString("fulled cells: %d (%f%%)", maxcells,   100*maxcells/cellcount);
				displayString("empty cells:  %d (%f%%)", emptycells, 100*emptycells/cellcount);
			}
			
		}
		fluid.updateGrid();
	}

	if(verbose)
	{
		displayString("computation time: %f second\n", etc.sumtime/1000);
	}

	return MS::kSuccess;
}

void* fluidFromMeshCmd::creator()
{
	return new fluidFromMeshCmd();
}

