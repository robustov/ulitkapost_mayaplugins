#pragma once
#include "pre.h"
#include <Util/FileStream.h>
#include <Math/Math.h>

#include <maya/MPxCommand.h>

struct RFParticle
{
	Math::Vec3f particle_position;
	Math::Vec3f particle_velocity;
	Math::Vec3f particle_force;
	Math::Vec3f particle_vorticity;
	Math::Vec3f normal_vector;
	int number_of_neighbors;
	Math::Vec3f Texture_vector;
	short int info_bits;
	float elapsed_particle_time;
	float isolation_time;
	float viscosity;
	float density;
	float pressure;
	float mass;
	float temperature;
	int particle_ID;
	RFParticle()
	{
		particle_position = Math::Vec3f(0);
		particle_velocity = Math::Vec3f(0);
		particle_force = Math::Vec3f(0);
		particle_vorticity = Math::Vec3f(0);
		normal_vector = Math::Vec3f(0);
		number_of_neighbors = 0;
		Texture_vector = Math::Vec3f(0);
		info_bits = 0;
		elapsed_particle_time = 0;
		isolation_time = 0;
		viscosity= 0;
		density=0;
		pressure=0;
		mass=0;
		temperature=0;
		particle_ID=0;
	}
};

struct RFHeader
{
	long int verification_code;
	char fluid_name[250];
	short int version;
	float scale_scene;
	int fluid_type;
	float elapsed_simulation_time;
	int frame_number;
	int frames_per_second;
	long int number_of_particles;
	float radius;
	Math::Vec3f pressure;// (max, min, average)
	Math::Vec3f speed;// (max, min, average)
	Math::Vec3f temperature;// (max, min, average)
	Math::Vec3f emitter_position;// version>=7
	Math::Vec3f emitter_rotation;// version>=7
	Math::Vec3f emitter_scale;// version>=7
	std::vector<RFParticle> particles;

	RFHeader()
	{
		verification_code = 0x00fabada;
		strcpy( fluid_name, "aaa");
		version = 9;
		scale_scene = 1;
		fluid_type = 8;
		elapsed_simulation_time = 0.039999999f;
		frame_number = 0;
		frames_per_second = 25;
		number_of_particles = 0;
		radius = 0.10000002f;

		pressure = Math::Vec3f(0);
		speed = Math::Vec3f(0);
		temperature = Math::Vec3f(0);
		emitter_position = Math::Vec3f(0);
		emitter_rotation = Math::Vec3f(0);
		emitter_scale = Math::Vec3f(0);
	}
};



template<class Stream> inline
Stream& operator >> (Stream& out, RFHeader& v)
{
	if(out.isSaving())
		v.number_of_particles = v.particles.size();

	out>>v.verification_code;
	for(int i=0; i<sizeof(v.fluid_name); i++)
		out>>v.fluid_name[i];
	out>>v.version;
	out>>v.scale_scene;
	out>>v.fluid_type;
	out>>v.elapsed_simulation_time;
	out>>v.frame_number;
	out>>v.frames_per_second;
	out>>v.number_of_particles;
	out>>v.radius;
	out>>v.pressure;// (max, min, average)
	out>>v.speed;// (max, min, average)
	out>>v.temperature;// (max, min, average)
	out>>v.emitter_position;// version>=7
	out>>v.emitter_rotation;// version>=7
	out>>v.emitter_scale;// version>=7

	if(out.isLoading())
		v.particles.resize(v.number_of_particles);

	for(int i=0; i<v.number_of_particles; i++)
	{
		out>>v.particles[i];
	}

	if(out.isSaving())
	{
		int s = 0;
		char ss = 0;
		out>>s;
		out>>ss;
	}
	return out;
}
template<class Stream> inline
Stream& operator >> (Stream& out, RFParticle& v)
{
	out>>v.particle_position;
	out>>v.particle_velocity;
	out>>v.particle_force;
	out>>v.particle_vorticity;
	out>>v.normal_vector;
	out>>v.number_of_neighbors;
	out>>v.Texture_vector;
	out>>v.info_bits;
	out>>v.elapsed_particle_time;
	out>>v.isolation_time;
	out>>v.viscosity;
	out>>v.density;
	out>>v.pressure;
	out>>v.mass;
	out>>v.temperature;
	out>>v.particle_ID;
	return out;
};

class exportToRealFlow : public MPxCommand
{
public:
	static const MString typeName;
	exportToRealFlow();
	virtual	~exportToRealFlow();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();

	bool readParticleCurrentFrame( 
		MObject node, 
		RFHeader& rfh, 
		Math::Matrix4f& world);

};

