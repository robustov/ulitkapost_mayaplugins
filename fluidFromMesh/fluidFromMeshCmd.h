#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: fluidFromMeshCmdCmd.h
// MEL Command: fluidFromMeshCmd

#include <maya/MPxCommand.h>

class fluidFromMeshCmd : public MPxCommand
{
public:
	static const MString typeName;
	fluidFromMeshCmd();
	virtual	~fluidFromMeshCmd();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

