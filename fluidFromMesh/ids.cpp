#include "pre.h"

//#error change the following to a unique value and then erase this line 
//const unsigned TypeIdBase = 0xXXXXXXXX;

// node static ids and names
#include "fluidFromMeshCmd.h"
const MString fluidFromMeshCmd::typeName( "fluidFromMeshCmd" );

// node static ids and names
#include "exportToRealFlow.h"
const MString exportToRealFlow::typeName( "exportToRealFlow" );
