#include "pre.h"
#include <maya/MFnPlugin.h>
#include "fluidFromMeshCmd.h"
#include "exportToRealFlow.h"

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

// #include "exportToRealFlow.h"
	stat = plugin.registerCommand( 
		exportToRealFlow::typeName.asChar(), 
		exportToRealFlow::creator, 
		exportToRealFlow::newSyntax );
	if (!stat) 
	{
		displayString("cant register %s", exportToRealFlow::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

// #include "fluidFromMeshCmd.h"
	stat = plugin.registerCommand( 
		fluidFromMeshCmd::typeName.asChar(), 
		fluidFromMeshCmd::creator, 
		fluidFromMeshCmd::newSyntax );
	if (!stat) 
	{
		displayString("cant register %s", fluidFromMeshCmd::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}


	if( !MGlobal::sourceFile("fluidFromMeshMenu.mel"))
	{
		displayString("error source AEmyCmdTemplate.mel");
	}
	else
	{
		if( !MGlobal::executeCommand("fluidFromMeshDebugMenu()"))
		{
			displayString("Dont found fluidFromMeshDebugMenu() command");
		}
		MGlobal::executeCommand("fluidFromMesh_RegistryPlugin()");
	}
	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterCommand( "exportToRealFlow" );
	if (!stat) displayString("cant deregister %s", exportToRealFlow::typeName.asChar());

	stat = plugin.deregisterCommand( "fluidFromMeshCmd" );
	if (!stat) displayString("cant deregister %s", fluidFromMeshCmd::typeName.asChar());

//	MGlobal::executeCommand("fluidFromMeshDebugMenuDelete()");
	return stat;
}
