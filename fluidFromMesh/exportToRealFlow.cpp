//
// Copyright (C) 
// File: exportToRealFlowCmd.cpp
// MEL Command: exportToRealFlow

#include "exportToRealFlow.h"

/*/
loadPlugin exportToRealflow.mll;
exportToRealFlow particleShape1 -st 1 -en 100 -fn "D:/piro/realflow/xxx%05d.bin";
unloadPlugin exportToRealflow.mll;
/*/
#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnParticleSystem.h> 
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MDagPath.h>
#include <maya/MItDag.h>
#include <maya/MAnimControl.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshPolygon.h>

#include <maya/MFloatPointArray.h>
#include <Util/misc_create_directory.h>

exportToRealFlow::exportToRealFlow()
{
}
exportToRealFlow::~exportToRealFlow()
{
}

MSyntax exportToRealFlow::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);	// arg0
	
	syntax.addFlag("fn", "filename", MSyntax::kString);
	syntax.addFlag("st", "start", MSyntax::kTime);
	syntax.addFlag("en", "end", MSyntax::kTime);
	syntax.addFlag("s", "step", MSyntax::kTime);
	return syntax;
}

MStatus exportToRealFlow::doIt( const MArgList& args)
{
//	char curDir[256];
//	GetCurrentDirectory(256, curDir);
//	displayStringD(curDir);

	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString argsl0;
	argData.getCommandArgument(0, argsl0);

	MString filename("");
	argData.getFlagArgument("fn", 0, filename);

	MTime startTime = MAnimControl::animationStartTime();
	argData.getFlagArgument("st", 0, startTime);

	MTime endTime = MAnimControl::animationEndTime();
	argData.getFlagArgument("en", 0, endTime);

	MTime stepTime(1, MTime::uiUnit());
	argData.getFlagArgument("s", 0, stepTime);

	MObject node;
	if( !nodeFromName( argsl0, node))
		return MS::kFailure;

	if( node.apiType()!=MFn::kParticle)
	{
		displayString("not Particle node");
		return MS::kFailure;
	}

	if(startTime>=endTime)
		return MS::kFailure;

	int f=0;
	for( MTime t = startTime; t<endTime; t+=stepTime, f++)
	{
		MAnimControl::setCurrentTime(t);

		MDagPath path;
		MDagPath::getAPathTo(node, path);
		MMatrix matr = path.inclusiveMatrix();

		Math::Matrix4f world;
		copy( world, matr);

		Util::FileStream fileStream;
		if( filename.length()==0)
			return MS::kFailure;

		char buf[512];
		sprintf(buf, filename.asChar(), f);

		Util::create_directory_for_file(buf);
		if( !fileStream.open(buf, true, false))
		{
			displayString("cant open %s file", buf);
			return MS::kFailure;
		}

//		displayStringD("\ntime: %f)", t.as(MTime::kSeconds));

		RFHeader rfh;
		strcpy( rfh.fluid_name, argsl0.asChar());
		rfh.frame_number = f;
		if( !readParticleCurrentFrame(node, rfh, world))
		{
			displayStringD("failed");
			return MS::kFailure;
		}

		fileStream>>rfh;
	}
	displayStringD("completed");

	return MS::kSuccess;
}

bool exportToRealFlow::readParticleCurrentFrame( 
	MObject node, 
	RFHeader& rfh, 
	Math::Matrix4f& world)
{
	int z;
	MStatus stat;
	MFnParticleSystem ps(node);
	MFnDependencyNode dn(node);

	// id
	MPlug paramplug = dn.findPlug("id", &stat);
	if( !stat) return false;
	MObject idobj;
	if( !paramplug.getValue( idobj)) 
		return false;
	MFnDoubleArrayData _id0(idobj);
	rfh.particles.resize( _id0.length());
	for(z=0; z<(int)_id0.length(); ++z) 
		rfh.particles[z].particle_ID = (int)_id0[z];

	// position
	MPlug plug_position = dn.findPlug("position", &stat);
	MObject obj_position;
	plug_position.getValue(obj_position);
	MFnVectorArrayData position(obj_position);
	if(position.length()!=rfh.particles.size())
	{
		displayString("position: position.length(%d)!=pl.size(%d)", position.length(), rfh.particles.size());
		return false;
	}
	for(z=0; z<(int)position.length(); ++z) 
	{
		Math::Vec3f p;
		p.x = (float)position[z].z;
		p.y = (float)position[z].y;
		p.z = (float)position[z].x;

		p = world*p;
		rfh.particles[z].particle_position = p;
	}

	//opacityPP
	{
		MPlug plug = dn.findPlug("velocity", &stat);
		MObject obj;
		plug.getValue(obj);
		MFnVectorArrayData val(obj);
		if(val.length()!=rfh.particles.size())
		{
			displayString("velocity: val.length(%d)!=pl.size(%d)", val.length(), rfh.particles.size());
			if(val.length()<rfh.particles.size())
				return false;
		}
		for(z=0; z<(int)rfh.particles.size(); ++z) 
		{
			Math::Vec3f p;
			p.x = (float)val[z].z;
			p.y = (float)val[z].y;
			p.z = (float)val[z].x;
			rfh.particles[z].particle_velocity = p;
		}
	}

	return true;
}

void* exportToRealFlow::creator()
{
	return new exportToRealFlow();
}

