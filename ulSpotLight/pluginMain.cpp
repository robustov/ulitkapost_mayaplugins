#include "pre.h"
#include <maya/MFnPlugin.h>
#include "ulSpotLight.h"

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

// #include "ulSpotLight.h"
	stat = plugin.registerNode( 
		ulSpotLight::typeName, 
		ulSpotLight::id, 
		ulSpotLight::creator,
		ulSpotLight::initialize, 
		MPxNode::kLocatorNode );
	if (!stat) 
	{
		displayString("cant register %s", ulSpotLight::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}


	if( !MGlobal::sourceFile("ulSpotLightMenu.mel"))
	{
		displayString("error source AEmyCmdTemplate.mel");
	}
	else
	{
		if( !MGlobal::executeCommand("ulSpotLightDebugMenu()"))
		{
			displayString("Dont found ulSpotLightDebugMenu() command");
		}
		MGlobal::executeCommand("ulSpotLight_RegistryPlugin()");
	}
	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterNode( ulSpotLight::id );
	if (!stat) displayString("cant deregister %s", ulSpotLight::typeName.asChar());


	MGlobal::executeCommand("ulSpotLightDebugMenuDelete()");
	return stat;
}
