//
// Copyright (C) 
// File: ulSpotLightCmd.cpp
// MEL Command: ulSpotLight

#include "ulSpotLight.h"

#include <maya/MGlobal.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MAngle.h>
#include "mathNmaya\mathNmaya.h"
#include "mathNgl\mathNgl.h"
#include "math\smoothstep.h"
#include <complex>

// Main
MObject ulSpotLight::i_color;
MObject ulSpotLight::i_intensity;
MObject ulSpotLight::i_dropoff;
MObject ulSpotLight::i_coneAngle;
MObject ulSpotLight::i_penumbraAngle;
MObject ulSpotLight::i_decayRate;
// Falloff
MObject ulSpotLight::i_Falloff;
MObject ulSpotLight::i_EndPersent;
MObject ulSpotLight::i_FalloffDist;
MObject ulSpotLight::i_StartDist;

// visualisation
MObject ulSpotLight::i_viewDistance;
MObject ulSpotLight::i_bViewFalloff;
MObject ulSpotLight::i_bViewIsoparma;
MObject ulSpotLight::i_viewAttenuation;

MObject ulSpotLight::o_output;


// �������: cosangle ������� ���� ����� ������ � ������
//			distance ��������� �� �����
// �����: atten
float Atten(float intensity, 
			float ConeAngle, float PenumbraAngle, 
			float Falloff, float EndPersent, float FalloffDist, float StartDist, 
			float cosangle,	
			float distance)
{
	float cosoutside = cos(ConeAngle+PenumbraAngle);
	float cosinside = cos(ConeAngle);
	float atten = Math::smoothstep(cosoutside, cosinside, cosangle);

	if( Falloff!=0)
	{
		float a = (100/EndPersent-1)/pow(FalloffDist-StartDist,Falloff);

		if( distance > StartDist)
		{
			atten *= 1 / (1 + a * pow(distance-StartDist,Falloff) );
		}
	}
	return atten*intensity;
}
// ������� distance atten
// �����: cosangle
float AttenInv(
				float intensity, 
				float ConeAngle, float PenumbraAngle, 
				float Falloff, float EndPersent, float FalloffDist, float StartDist, 
				float distance, 
				float atten)
{
	atten *= 1/intensity;
	if( Falloff!=0)
	{
		float a = (100/EndPersent-1)/pow(FalloffDist-StartDist,Falloff);
		if( distance > StartDist)
		{
			atten *= (1 + a * pow(distance-StartDist,Falloff) );
		}
	}

	float cosoutside = cos(ConeAngle+PenumbraAngle);
	float cosinside = cos(ConeAngle);
	float cosangle = Math::smoothstep_inv(cosoutside, cosinside, atten);

	return cosangle;
}

// ������� cosangle*distance atten
// �����: cosangle
float AttenInv_Comp(
				float intensity, 
				float ConeAngle, float PenumbraAngle, 
				float Falloff, float EndPersent, float FalloffDist, float StartDist, 
				float distance, 
				float atten)
{
	atten *= 1/intensity;
	if( Falloff!=0)
	{
		float a = (100/EndPersent-1)/pow(FalloffDist-StartDist,Falloff);
		if( distance > StartDist)
		{
			atten *= (1 + a * pow(distance-StartDist,Falloff) );
		}
	}

	float cosoutside = cos(ConeAngle+PenumbraAngle);
	float cosinside = cos(ConeAngle);
	float cosangle = Math::smoothstep_inv(cosoutside, cosinside, atten);

	return cosangle;
}

// ������� atten
// �����: distance
float AttenInv_Falloff(
				float intensity, 
				float Falloff, float EndPersent, float FalloffDist, float StartDist, 
				float atten)
{
	typedef std::complex<float> cf;
	float a = (100/EndPersent-1)/pow(FalloffDist-StartDist,Falloff);

	atten *= 1/intensity;

	cf d1 = pow(cf(atten),	-1/Falloff);
	cf d2 = pow(cf(atten-1), 1/Falloff);
	cf d3 = pow(cf(-a),		-1/Falloff);
	cf s = d1*d2*d3 + StartDist;
	return abs(s);
}

ulSpotLight::ulSpotLight()
{
}
ulSpotLight::~ulSpotLight()
{
}

MStatus ulSpotLight::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	if( plug == o_output )
	{
		double coneAngle = data.inputValue( i_coneAngle, &stat ).asDouble();
		coneAngle *= 0.5;
		double penumbraAngle = data.inputValue( i_penumbraAngle, &stat ).asDouble();
		float intensity = data.inputValue( i_intensity, &stat ).asFloat();

		float Falloff, EndPersent, FalloffDist, StartDist;
		Falloff = data.inputValue( i_Falloff, &stat ).asFloat();
		EndPersent = data.inputValue( i_EndPersent, &stat ).asFloat();
		FalloffDist = data.inputValue( i_FalloffDist, &stat ).asFloat();
		StartDist = data.inputValue( i_StartDist, &stat ).asFloat();

		float viewAttenuation, view_maxDist;
		viewAttenuation = data.inputValue( i_viewAttenuation, &stat ).asFloat();
		view_maxDist = data.inputValue( i_viewDistance, &stat ).asFloat();

		BuildDrawCache(
			coneAngle, 
			penumbraAngle,
			intensity,
			Falloff, 
			EndPersent, 
			FalloffDist, 
			StartDist,
			viewAttenuation, 
			view_maxDist);
		
		MDataHandle outputData = data.outputValue( plug, &stat );
		double out = outputData.asDouble();
		out += 0.01;

		data.setClean(plug);
		return MS::kSuccess;
	}

	return MS::kUnknownParameter;
}

bool ulSpotLight::isBounded() const
{
	MObject thisNode = thisMObject();
	return false;
}

MBoundingBox ulSpotLight::boundingBox() const
{
	MObject thisNode = thisMObject();

	double out;
	MPlug plug;
	plug = MPlug(thisNode, o_output);
	plug.getValue(out);

	MBoundingBox box;
	//...
	return box;
}

void ulSpotLight::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MObject thisNode = thisMObject();

	MPlug plug;

	double out;
	plug = MPlug(thisNode, o_output);
	plug.getValue(out);

	bool bViewFalloff, bViewIsoparma;
	plug = MPlug(thisNode, i_bViewIsoparma);
	plug.getValue(bViewIsoparma);
	plug = MPlug(thisNode, i_bViewFalloff);
	plug.getValue(bViewFalloff);
	float viewDistance;
	plug = MPlug(thisNode, i_viewDistance);
	plug.getValue(viewDistance);

	view.beginGL(); 

	if( bViewIsoparma)
	{
		for(int i=0; i<(int)isoparma.size(); i++)
		{
			std::vector<Math::Vec3f>& line = isoparma[i];
			drawLine( &line[0], (int)line.size());
		}
		for(i=0; i<(int)isoparmaC.size(); i++)
		{
			circle& ci = isoparmaC[i];
			drawCircleXY( ci.r*2, ci.center);
		}
	}
	if( bViewFalloff)
	{
		std::vector<Math::Vec3f>& line = intens;
		drawLine( &line[0], (int)line.size());
		
		glLineStipple(1, 0x3333);
		glEnable(GL_LINE_STIPPLE);
		for(int i=0; i<(int)intensL.size(); i++)
		{
			circle& ci = intensL[i];
			glBegin( GL_LINES );
			glVertex3f( ci.center + Math::Vec3f( ci.r,  0, 0));
			glVertex3f( ci.center + Math::Vec3f(-ci.r,  0, 0));
			glVertex3f( ci.center + Math::Vec3f( 0, -ci.r, 0));
			glVertex3f( ci.center + Math::Vec3f( 0,  ci.r, 0));
			glEnd();
			drawCircleXY( ci.r*2, ci.center);

		}
		glBegin( GL_LINES );
		glVertex3f( Math::Vec3f( 0,  0, 0));
		glVertex3f( Math::Vec3f( 0,  0, -viewDistance));
		glEnd();
			
		glDisable(GL_LINE_STIPPLE);
	}
	//...
	view.endGL();
};

void ulSpotLight::BuildDrawCache(
	double coneAngle, 
	double penumbraAngle,
	float intensity,
	float Falloff, 
	float EndPersent, 
	float FalloffDist, 
	float StartDist,
	float viewAttenuation, 
	float view_maxDist
	)
{
	isoparma.clear();
	isoparmaC.clear();
	intens.clear();
	intensL.clear();

	isoparma.reserve(4);

	float view_deltaDist = 0.05f;
	std::vector<Math::Vec3f> line;
	line.reserve( (int)(view_maxDist/view_deltaDist));

	if( true)
	{
		if( viewAttenuation==0) 
			Falloff = 0;
		bool bFirst = true;
		float isoparma_maxDist = view_maxDist;
		circle maxradius;
		maxradius.r = 0;
		if( Falloff!=0) 
		{
			float d = AttenInv_Falloff(intensity, 
				Falloff, EndPersent, FalloffDist, StartDist, 
				viewAttenuation);

			float r = d*sin( coneAngle);
			float dr = d*cos( coneAngle);
			isoparma_maxDist = d-view_deltaDist;

			maxradius = circle(Math::Vec3f(0, 0, -dr), r);
			isoparmaC.push_back(maxradius);

			int ct = 20;
			float da = coneAngle/ct;
			line.resize(ct+1);
			for(int i=0; i<(int)line.size(); i++)
			{
				float a= i*coneAngle/ct;
				float r = d*sin( a);
				float dr = d*cos( a);
				line[i] = Math::Vec3f(0, r, -dr);
			}
			bFirst = false;
		}

		for(float d=isoparma_maxDist; d>0; d-=view_deltaDist)
		{
			float cosAngle = AttenInv( intensity, (float)coneAngle, (float)penumbraAngle, 
				Falloff, EndPersent, FalloffDist, StartDist, 
				d, 
				viewAttenuation);

			float sinAngle = sqrt(1-(float)(cosAngle*cosAngle));
			float t = d*sinAngle;
			float xd = d*cosAngle;
//			if( Falloff==0)
//				t *= 1/cosAngle;

			if(maxradius.r<t)
				maxradius = circle(Math::Vec3f(0, 0, -xd), t);
			
			line.push_back(Math::Vec3f(0, t, -xd));

		}
		isoparmaC.push_back(maxradius);
		
		isoparma.push_back(line);

		for(int i=0; i<(int)line.size(); i++)
		{
			Math::Vec3f src = line[i];
			line[i] = Math::Vec3f(0, -src.y, src.z);
		}
		isoparma.push_back(line);

		for(int i=0; i<(int)line.size(); i++)
		{
			Math::Vec3f src = line[i];
			line[i] = Math::Vec3f(src.y, 0, src.z);
		}
		isoparma.push_back(line);

		for(int i=0; i<(int)line.size(); i++)
		{
			Math::Vec3f src = line[i];
			line[i] = Math::Vec3f(-src.x, 0, src.z);
		}
		isoparma.push_back(line);
	}

	if( true)
	{
		line.clear();
		for(float d=0; d<view_maxDist; d+=view_deltaDist)
		{
			float a = Atten( intensity, (float)coneAngle, (float)penumbraAngle, 
				Falloff, EndPersent, FalloffDist, StartDist, 
				1, d);
			line.push_back( Math::Vec3f(a, 0, -d));
		}
		intens = line;

		if( Falloff!=0)
		{
			float a;
			a = Atten( intensity, (float)coneAngle, (float)penumbraAngle, 
				Falloff, EndPersent, FalloffDist, StartDist, 
				1, StartDist);
			intensL.push_back( circle(Math::Vec3f(0, 0, -StartDist), a));

			a = Atten( intensity, (float)coneAngle, (float)penumbraAngle, 
				Falloff, EndPersent, FalloffDist, StartDist, 
				1, FalloffDist);
			intensL.push_back( circle(Math::Vec3f(0, 0, -FalloffDist), a));

		}
	}

	/*/
	{
		float d = 1;
		float cosAngle = AttenInv( intensity, (float)coneAngle, (float)penumbraAngle, viewAttenuation);
		float sinAngle = sqrt(1-(float)(cosAngle*cosAngle));
		float t = d*sinAngle/cosAngle;
		drawCircleXY( t*2, Math::Vec3f(0, 0, -d));
	}
	{
		float d = 2;
		float cosAngle = AttenInv( intensity, (float)coneAngle, (float)penumbraAngle, viewAttenuation);
		float sinAngle = sqrt(1-(float)(cosAngle*cosAngle));
		float t = d*sinAngle/cosAngle;
		drawCircleXY( t*2, Math::Vec3f(0, 0, -d));
	}
	/*/

}


void* ulSpotLight::creator()
{
	return new ulSpotLight();
}

MStatus ulSpotLight::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnUnitAttribute unitAttr;
	MStatus				stat;

	try
	{
		{
			i_color = numAttr.createColor("color", "cl");
			numAttr.setDefault(1.0f, 1.0f, 1.0f);
			::addAttribute(i_color, atInput);
		}
		{
			i_intensity = numAttr.create("intensity", "intensity", MFnNumericData::kFloat, 1);
			numAttr.setMin(0);
			numAttr.setSoftMax(2);
			::addAttribute(i_intensity, atInput);
		}
		{
			i_dropoff = numAttr.create("dropoff", "dropoff", MFnNumericData::kFloat, 50);
			numAttr.setMin(0);
			numAttr.setSoftMax(100);
			::addAttribute(i_dropoff, atInput);
		}
		{
			i_coneAngle = unitAttr.create("coneAngle", "coneAngle", MAngle(10, MAngle::kDegrees));
			unitAttr.setMin(0*2*M_PI/180);
			unitAttr.setSoftMax(179*2*M_PI/180);
			::addAttribute(i_coneAngle, atInput);
		}
		{
			i_penumbraAngle = unitAttr.create("penumbraAngle", "penumbraAngle", MAngle(40, MAngle::kDegrees));
//			unitAttr.setMin(-90*2*M_PI/180);
			unitAttr.setMin(0);
			unitAttr.setMax(90*2*M_PI/180);
			::addAttribute(i_penumbraAngle, atInput);
		}
		{
			i_decayRate = numAttr.create("decayRate", "decayRate", MFnNumericData::kFloat, 50);
			numAttr.setMin(0);
			numAttr.setSoftMax(100);
			::addAttribute(i_decayRate, atInput);
		}

		{
			i_viewAttenuation = numAttr.create("viewAttenuation", "viewAttenuation", MFnNumericData::kFloat, 0.5);
			numAttr.setMin(0);
			numAttr.setSoftMax(2);
			::addAttribute(i_viewAttenuation, atInput);
		}
		// Falloff
		{
			i_Falloff = numAttr.create("falloff", "falloff", MFnNumericData::kFloat, 3);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(i_Falloff, atInput);

			i_EndPersent = numAttr.create("endPersent", "endPersent", MFnNumericData::kFloat, 20);
			numAttr.setMin(0);
			numAttr.setSoftMax(100);
			::addAttribute(i_EndPersent, atInput);

			i_FalloffDist = numAttr.create("falloffDist", "falloffDist", MFnNumericData::kFloat, 5);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(i_FalloffDist, atInput);

			i_StartDist = numAttr.create("startDist", "startDist", MFnNumericData::kFloat, 0.5);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(i_StartDist, atInput);
		}
			
		{
			i_viewDistance = numAttr.create("viewDistance", "viewDistance", MFnNumericData::kFloat, 10);
			numAttr.setMin(0);
			numAttr.setSoftMax(100);
			::addAttribute(i_viewDistance, atInput);
			
			i_bViewFalloff = numAttr.create("bViewFalloff", "bViewFalloff", MFnNumericData::kBoolean, 1);
			::addAttribute(i_bViewFalloff, atInput);
			i_bViewIsoparma = numAttr.create("bViewIsoparma", "bViewIsoparma", MFnNumericData::kBoolean, 1);
			::addAttribute(i_bViewIsoparma, atInput);
		}
		{
			o_output = numAttr.create( "output", "out", MFnNumericData::kDouble, 0.01, &stat);
			::addAttribute(o_output, atOutput);

		}
		{
			MObject affect = o_output;
/*/
i_color
i_intensity
i_dropoff
i_coneAngle
i_penumbraAngle
i_decayRate
i_Falloff
i_EndPersent
i_FalloffDist
i_StartDist
i_viewDistance
i_viewAttenuation
/*/
			stat = attributeAffects( i_color, affect );
			stat = attributeAffects( i_intensity, affect );
			stat = attributeAffects( i_dropoff, affect );
			stat = attributeAffects( i_coneAngle, affect );
			stat = attributeAffects( i_penumbraAngle, affect );
			stat = attributeAffects( i_decayRate, affect );
			stat = attributeAffects( i_Falloff, affect );
			stat = attributeAffects( i_EndPersent, affect );
			stat = attributeAffects( i_FalloffDist, affect );
			stat = attributeAffects( i_StartDist, affect );
			stat = attributeAffects( i_viewDistance, affect );
			stat = attributeAffects( i_viewAttenuation, affect );
		}
		if( !MGlobal::sourceFile("AEulSpotLightTemplate.mel"))
		{
			displayString("error source AEulSpotLightTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}