#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>
#include "math\math.h"

 
class ulSpotLight : public MPxLocatorNode
{
	struct circle
	{
		Math::Vec3f center;
		float r;
		circle(Math::Vec3f center=Math::Vec3f(0), float r=1)
		{
			this->center = center;
			this->r = r;
		}
	};
	struct line
	{
		Math::Vec3f p0;
		Math::Vec3f p1;
	};
public:
	ulSpotLight();
	virtual ~ulSpotLight(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	static void* creator();
	static MStatus initialize();

// draw cache
public:
	// isoparma
	std::vector< std::vector<Math::Vec3f> > isoparma;
	std::vector< circle> isoparmaC;
	
	// intensity
	std::vector<Math::Vec3f> intens;
	std::vector<circle> intensL;

	void BuildDrawCache(
		double coneAngle, 
		double penumbraAngle,
		float intensity,
		float Falloff, 
		float EndPersent, 
		float FalloffDist, 
		float StartDist,
		float viewAttenuation, 
		float view_maxDist
		);
public:
	static MObject i_color;
	static MObject i_intensity;
	static MObject i_dropoff;
	static MObject i_coneAngle;
	static MObject i_penumbraAngle;
	static MObject i_decayRate;

	// Falloff
	static MObject i_Falloff;
	static MObject i_EndPersent;
	static MObject i_FalloffDist;
	static MObject i_StartDist;

	// visualisation
	static MObject i_viewDistance;
	static MObject i_bViewFalloff;
	static MObject i_bViewIsoparma;
	static MObject i_viewAttenuation;

public:
	static MObject o_output;

public:
	static MTypeId id;
	static const MString typeName;
};


