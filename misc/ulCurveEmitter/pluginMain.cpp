#include "pre.h"

#include <maya/MFnPlugin.h>
#include "ulCurveEmitter.h"

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

	stat = plugin.registerNode( 
		ulCurveEmitter::typeName, 
		ulCurveEmitter::id, 
		ulCurveEmitter::creator,
		ulCurveEmitter::initialize, 
		MPxNode::kEmitterNode );
	if (!stat) 
	{
		displayString("cant register %s", ulCurveEmitter::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterNode( ulCurveEmitter::id );
	if (!stat) displayString("cant deregister %s", ulCurveEmitter::typeName.asChar());

	return stat;
}
