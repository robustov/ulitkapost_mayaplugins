#include "ulCurveEmitter.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnArrayAttrsData.h>
#include <maya/MFnNurbsCurve.h>
#include "Math/cellnoise.h"
#include "Math/Rotation3.h"
#include "MathNMaya/MathNMaya.h"

#include <maya/MGlobal.h>

MTypeId ulCurveEmitter::id( TypeIdBase+00 );
const MString ulCurveEmitter::typeName( "ulCurveEmitter" );
Math::PerlinNoise ulCurveEmitter::perlinNoise;

MObject ulCurveEmitter::i_curve;
MObject ulCurveEmitter::i_perlinRamp;
MObject ulCurveEmitter::i_frequency;
MObject ulCurveEmitter::i_frequencyP;
MObject ulCurveEmitter::i_seed;
MObject ulCurveEmitter::i_curveNormalOffset, ulCurveEmitter::i_curveNormalOffsetJitter;

ulCurveEmitter::ulCurveEmitter()
{
}
ulCurveEmitter::~ulCurveEmitter()
{
}

//! Main Generate proc
void ulCurveEmitter::emit(
	int emitCount,					//!< wanted particle count
	MVectorArray& fnOutPos,			//!< output positions
	MVectorArray& fnOutVel,			//!< output velocities
	int plugIndex,					//!< N particle system
	MDataBlock& data				//!< data
	)
{
	MStatus stat;
	MTime time_ = data.inputValue(mCurrentTime).asTime();
	double time = time_.as(MTime::kSeconds);

	double freq = data.inputValue(i_frequency).asDouble();
	double freqP= data.inputValue(i_frequencyP).asDouble();

	double3& dir = data.inputValue(mDirection).asDouble3();
	double vel = data.inputValue(mSpeed).asDouble();

	MRampAttribute rampattr(thisMObject(), i_perlinRamp);
	Math::Ramp ramp;
	ramp.set( rampattr, 0.f, 1.f, 1.f);

	double curveNormalOffset = data.inputValue(i_curveNormalOffset).asDouble();
	double curveNormalOffsetJitter = data.inputValue(i_curveNormalOffsetJitter).asDouble();

	double speed = data.inputValue(mSpeed).asDouble();
	MVector direction = data.inputValue(mSpeed).asDouble3();
	MMatrix worldMatrix = data.inputValue(mWorldMatrix).asMatrix();
	double inheritFactor = data.inputValue(mInheritFactor).asDouble();
	int seed = data.inputValue(i_seed).asInt();
	float fseed = (float)(time*seed);
	srand( *(int*)&fseed);

	MArrayDataHandle adh = data.inputArrayValue(i_curve);
	std::vector<MObject> vals;
	vals.reserve(adh.elementCount());
	if(adh.elementCount()) do
	{
		int index = adh.elementIndex();
		MObject obj = adh.inputValue(&stat).asNurbsCurve();
		if( !stat) 
			continue;

		vals.push_back(obj);
		MFnNurbsCurve curve(obj);

		double start, end;
		curve.getKnotDomain(start, end);

		for(int i=0; i<emitCount; i++)
		{
			// ������� ��������
			double r = rand()/(double)RAND_MAX;

			double u = r*(end-start) + start;
			MPoint newPos;
			curve.getPointAtParam(u, newPos, MSpace::kWorld);

			{
				MVector _tg = curve.tangent(u, MSpace::kWorld);
				MVector _n = curve.normal(u, MSpace::kWorld);
				Math::Vec3f n; ::copy(n, _n);
				Math::Vec3f tg; ::copy(tg, _tg);
				double cnoa = 2*M_PI*rand()/(double)RAND_MAX;
				float cnoj = rand()/(float)RAND_MAX;
				Math::Rot3f rot((float)cnoa, tg);

				n *= (float)( (1 - cnoj*curveNormalOffsetJitter)*curveNormalOffset);
				Math::Matrix4f transform;
				rot.getMatrix(transform);
				n = transform*n;

				newPos.x += n.x;
				newPos.y += n.y;
				newPos.z += n.z;
			}

			float param = perlinNoise.noise( (float)((time+seed)*freq), (float)(r*freqP), (float)index);

			// ����������� �� �����
			float posibil = ramp.getValue( param+0.5f);

			// ���������
			float d = rand()/(float)RAND_MAX;
			if( d > posibil) continue;

			// ���������
			MVector newVel(dir[0], dir[1], dir[2]);
			newVel.normalize();
			newVel *= vel;
			// ...
			fnOutPos.append( newPos );
			fnOutVel.append( newVel );
		}

	}while( adh.next());


}

MStatus ulCurveEmitter::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	if( plug == mOutput )
		return computeOutput(plug, data);

	return MS::kUnknownParameter;
}

MStatus ulCurveEmitter::computeOutput( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;
	// Get the logical index of the element this plug refers to,
	// because the node can be emitting particles into more 
    // than one particle shape.
	int multiIndex = plug.logicalIndex( &stat );

	// Get output data arrays (position, velocity, or parentId)
	// that the particle shape is holding from the previous frame.
	MArrayDataHandle hOutArray = data.outputArrayValue( mOutput, &stat);

	// Create a builder to aid in the array construction efficiently.
	MArrayDataBuilder bOutArray = hOutArray.builder( &stat );

	// Get the appropriate data array that is being currently evaluated.
	MDataHandle hOut = bOutArray.addElement(multiIndex, &stat);

	// Create the data and apply the function set,
	// particle array initialized to length zero, 
	// fnOutput.clear()
	MFnArrayAttrsData fnOutput;
	MObject dOutput = fnOutput.create ( &stat );

	// Get the position and velocity arrays to append new particle data.
	//
	MVectorArray fnOutPos = fnOutput.vectorArray("position", &stat);
    MVectorArray fnOutVel = fnOutput.vectorArray("velocity", &stat);

	// Check if the particle object has reached it's maximum,
	// hence is full. If it is full then just return with zero particles.
	//
	bool beenFull = this->isFullValue( multiIndex, data );
	if( beenFull )
	{
		return( MS::kSuccess );
	}

	// Get deltaTime, currentTime and startTime.
	// If deltaTime <= 0.0, or currentTime <= startTime,
	// do not emit new pariticles and return.
	//
	MTime cT = this->currentTimeValue( data );
	MTime sT = this->startTimeValue( multiIndex, data );
	MTime dT = this->deltaTimeValue( multiIndex, data );
	if( (cT <= sT) || (dT <= 0.0) )
	{
		// We do not emit particles before the start time, 
		// and do not emit particles when moving backwards in time.

		// This code is necessary primarily the first time to 
		// establish the new data arrays allocated, and since we have 
		// already set the data array to length zero it does 
		// not generate any new particles.
		hOut.set( dOutput );
		data.setClean( plug );
		return( MS::kSuccess );
	}

    // Get rate and delta time.
    //
    double rate = data.inputValue(mRate, &stat).asDouble();
    double dblCount = rate * dT.as( MTime::kSeconds );
    int emitCount = (int)dblCount;

	// Generate proc
	emit(emitCount, fnOutPos, fnOutVel, multiIndex, data);

	hOut.set( dOutput );
	data.setClean( plug );

	return( MS::kSuccess );
}

void* ulCurveEmitter::creator()
{
	return new ulCurveEmitter();
}

MStatus ulCurveEmitter::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnUnitAttribute	unitAttr;
	MStatus				stat;

	try
	{
		perlinNoise.Init(0);

		{
			i_curve = typedAttr.create ("inputCurve","ic", MFnData::kNurbsCurve, MObject::kNullObj, &stat);
			typedAttr.setDisconnectBehavior(MFnAttribute::kDelete);
			::addAttribute(i_curve, atInput|atArray);
//			stat = attributeAffects( i_curve, mOutput );

			i_perlinRamp = Math::Ramp::CreateRampAttr("possibility", "pos");
			::addAttribute(i_perlinRamp, atInput);
//			stat = attributeAffects( i_perlinRamp, mOutput );

			i_frequency = numAttr.create("frequency", "fr", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.001);
			numAttr.setSoftMax(10);
			::addAttribute(i_frequency, atInput);
//			stat = attributeAffects( i_frequency, mOutput );

			i_frequencyP = numAttr.create("frequencyP", "frp", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.001);
			numAttr.setSoftMax(10);
			::addAttribute(i_frequencyP, atInput);
//			stat = attributeAffects( i_frequencyP, mOutput );

			i_curveNormalOffset = numAttr.create("curveNormalOffset", "cno", MFnNumericData::kDouble, 0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_curveNormalOffset, atInput);

			i_curveNormalOffsetJitter = numAttr.create("curveNormalOffsetJitter", "cnoj", MFnNumericData::kDouble, 0, &stat);
			numAttr.setMin(0);
			numAttr.setMax(1);
			::addAttribute(i_curveNormalOffsetJitter, atInput);

			

			i_seed = numAttr.create("ulSeed", "use", MFnNumericData::kInt, 1, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10000);
			::addAttribute(i_seed, atInput);
			
		}

		if( !MGlobal::sourceFile("AEulCurveEmitterTemplate.mel"))
		{
			displayString("error source AEulCurveEmitterTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

