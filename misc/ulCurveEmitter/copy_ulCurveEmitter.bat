set BIN_DIR=d:\out\hair
set BIN_DIR=\\server\bin

rem SN
mkdir %BIN_DIR%
mkdir %BIN_DIR%\bin
mkdir %BIN_DIR%\mel
mkdir %BIN_DIR%\slim
mkdir %BIN_DIR%\docs

copy Bin\ulCurveEmitter.mll					%BIN_DIR%\bin

copy Mel\ulCurveEmitterMenu.mel				%BIN_DIR%\mel
copy Mel\AEulCurveEmitterTemplate.mel		%BIN_DIR%\mel

pause