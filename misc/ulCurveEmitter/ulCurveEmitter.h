#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ulCurveEmitterNode.h
//
// Dependency Graph Node: ulCurveEmitter

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include <maya/MPxEmitterNode.h>
#include "MathNMaya/MayaRamp.h"
#include "Math/PerlinNoise.h"

//! ulCurveEmitter
class ulCurveEmitter : public MPxEmitterNode
{
public:
	ulCurveEmitter();
	virtual ~ulCurveEmitter(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();


	//! Main Generate proc
	void emit(
		int emitCount,					//!< wanted particle count
		MVectorArray& fnOutPos,			//!< output positions
		MVectorArray& fnOutVel,			//!< output velocities
		int plugIndex,					//!< N particle system
		MDataBlock& data				//!< data
		);

public:
	static MObject i_curve;			//!< Example input attribute
	static MObject i_perlinRamp;
	static MObject i_frequency, i_frequencyP;
	static MObject i_seed;

	static MObject i_curveNormalOffset, i_curveNormalOffsetJitter;

public:
	static MTypeId id;
	static const MString typeName;
	static Math::PerlinNoise perlinNoise;

protected:
	MStatus computeOutput( 
		const MPlug& plug, 
		MDataBlock& data );

	bool isFullValue(int plugIndex, MDataBlock& block)
	{
		MDataHandle hValue;
		if( !getPlugValue(hValue, mIsFull, plugIndex, block))
			return true;
		return hValue.asBool();
	}
	double inheritFactorValue(int plugIndex,MDataBlock& block)
	{
		MDataHandle hValue;
		if( !getPlugValue(hValue, mInheritFactor, plugIndex, block))
			return 0.0;
		return hValue.asDouble();
	}

	MTime currentTimeValue( MDataBlock& block )
	{
		MStatus stat;
		MDataHandle hValue = block.inputValue( mCurrentTime, &stat );
		if( !stat) return MTime(0.0);
		return hValue.asTime();
	}
	MTime startTimeValue( int plugIndex, MDataBlock& block )
	{
		MDataHandle hValue;
		if( !getPlugValue(hValue, mStartTime, plugIndex, block))
			return MTime(0.0);
		return hValue.asTime();
	}
	MTime deltaTimeValue( int plugIndex, MDataBlock& block )
	{
		MDataHandle hValue;
		if( !getPlugValue(hValue, mDeltaTime, plugIndex, block))
			return MTime(0.0);
		return hValue.asTime();
	}

	bool getPlugValue(MDataHandle& hValue, MObject attribute, int plugIndex, MDataBlock& block);
};

inline bool ulCurveEmitter::getPlugValue(MDataHandle& hValue, MObject attribute, int plugIndex, MDataBlock& block)
{
	MStatus stat;
	bool value = true;

	MArrayDataHandle mhValue = block.inputArrayValue( attribute, &stat );
	if( stat == MS::kSuccess )
	{
		stat = mhValue.jumpToElement( plugIndex );
		if( stat == MS::kSuccess )
		{
			hValue = mhValue.inputValue( &stat );
			return ( stat == MS::kSuccess );
		}
	}
	return false;
}


