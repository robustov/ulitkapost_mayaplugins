//
// Copyright (C) 
// File: attrInfoCmd.cpp
// MEL Command: attrInfo

#include "attrInfo.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MFnAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MPlug.h>
#include <maya/MFnNumericData.h>
#include <maya/MFnNumericAttribute.h>

attrInfo::attrInfo()
{
}
attrInfo::~attrInfo()
{
}

MSyntax attrInfo::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);	// arg0
	return syntax;
}

void displayAttrInfo(MPlug plug)
{
	MStatus stat;
	MObject attrobj = plug.attribute();
	MFn::Type type = attrobj.apiType();
	MFnAttribute attr(attrobj);

	MObject valobj;
	plug.getValue(valobj);

	displayString( "-------------------");
	displayString( "type=%s", attrobj.apiTypeStr());
	if( type==MFn::kTypedAttribute)
	{
		MFnTypedAttribute attr(attrobj);
		MFnData::Type type = attr.attrType();
		displayString( "MFnData::Type: %d", type-MFnData::kInvalid);
		switch(type)
		{
		case MFnData::kNumeric:
			{
				MFnNumericData data(valobj);
				MFnNumericData::Type ntype = data.numericType();
				displayString( "MFnNumericData::Type: %d", ntype-MFnNumericData::kInvalid);
				break;
			}
		}
	}
	if( type==MFn::kNumericAttribute)
	{
		MFnNumericAttribute attr(attrobj);
		MFnNumericData::Type type = attr.unitType();
		displayString( "MFnNumericData::Type: %d", type-MFnNumericData::kInvalid);
	}
	displayString( "attribute: %s", plug.name().asChar());
	displayString( attr.isReadable()?"Readable":"UnReadable");
	displayString( attr.isWritable()?"Writable":"UnWritable");
	displayString( attr.isConnectable()?"Connectable":"UnConnectable");
	displayString( attr.isStorable()?"Storable":"UnStorable");
	displayString( attr.isCached()?"Cached":"UnCached");
	displayString( attr.isArray()?"Array":"UnArray");
	displayString( attr.indexMatters()?"indexMatters":"UnindexMatters");
	displayString( attr.isKeyable()?"Keyable":"UnKeyable");
	displayString( attr.isHidden()?"Hidden":"UnHidden");
	displayString( attr.isUsedAsColor()?"UsedAsColor":"UnUsedAsColor");
	displayString( attr.isIndeterminant()?"Indeterminant":"UnIndeterminant");

	displayString( attr.isRenderSource()?"RenderSource":"UnRenderSource");
	displayString( attr.isDynamic()?"Dynamic":"UnDynamic");
	displayString( "disconnectBehavior=%d", attr.disconnectBehavior());
	displayString( attr.usesArrayDataBuilder()?"usesArrayDataBuilder":"UnusesArrayDataBuilder");
	displayString( attr.internal()?"internal":"Uninternal");

	if( plug.isCompound())
	{
		for( unsigned i=0; i<plug.numChildren(); i++)
		{
			MPlug pl = plug.child(i);
			displayAttrInfo(pl);
		}
	}

	
	///
	MStringArray cmds;

	displayString( "getSetAttrCmds (MPlug::kAll):");
	stat = plug.getSetAttrCmds(
		cmds,
		MPlug::kAll,		// kNonDefault, kChanged, 
		true				// useLongNames
		);
	for( int x=0; x<(int)cmds.length(); x++)
	{
		displayString( cmds[x].asChar());
	}

	displayString( "getSetAttrCmds (MPlug::kChanged):");
	stat = plug.getSetAttrCmds(
		cmds,
		MPlug::kChanged,		// kNonDefault, kChanged, 
		true				// useLongNames
		);
	for( int x=0; x<(int)cmds.length(); x++)
	{
		displayString( cmds[x].asChar());
	}

	displayString( "getSetAttrCmds (MPlug::kNonDefault):");
	stat = plug.getSetAttrCmds(
		cmds,
		MPlug::kNonDefault,		// kNonDefault, kChanged, 
		true				// useLongNames
		);
	for( int x=0; x<(int)cmds.length(); x++)
	{
		displayString( cmds[x].asChar());
	}
}

MStatus attrInfo::doIt( const MArgList& args)
{
	/*/
	MStringArray result;
	MGlobal::executeCommand("ls -nt", result);
	for( unsigned i=0; i<result.length(); i++)
	{
		MString cmd = "createNode ";
		cmd += result[i];

		MString node;
		MGlobal::executeCommand(cmd, node);

		MObject obj;
		nodeFromName(node, obj);
		MFn::Type t = obj.apiType();

		displayString( "%s = %d", result[i].asChar(), (int)t);

//		MFnDependencyNode dn(obj);
	}
	/*/


	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString argsl0;
	argData.getCommandArgument(0, argsl0);

	MObject obj;
	if( nodeFromName(argsl0, obj))
	{
	}

	MPlug plug;
	if( plugFromName(argsl0, plug))
	{
		MObject attr = plug.attribute();
		displayAttrInfo(plug);
	}
	else
	{
		displayString("attribute %s not found", argsl0.asChar());
	}

	return MS::kSuccess;
}

void* attrInfo::creator()
{
	return new attrInfo();
}

