#include "pre.h"

#include <maya/MFnPlugin.h>
#include "attrInfo.h"
#include "addXattr.h"

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

	stat = plugin.registerCommand( "addXattr", addXattr::creator, addXattr::newSyntax );
	if (!stat) {
		displayString("cant register node addXattr");
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerCommand( "attrInfo", attrInfo::creator, attrInfo::newSyntax );
	if (!stat) {
		displayString("cant register node attrInfo");
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterCommand( "addXattr" );
	if (!stat) displayString("cant deregister addXattr");

	stat = plugin.deregisterCommand( "attrInfo" );
	if (!stat) displayString("cant deregister attrInfo");

	return stat;
}
