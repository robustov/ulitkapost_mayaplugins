#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: addXattrCmd.h
// MEL Command: addXattr

#include <maya/MPxCommand.h>

class addXattr : public MPxCommand
{
public:
	addXattr();
	virtual	~addXattr();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

