#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: attrInfoCmd.h
// MEL Command: attrInfo

#include <maya/MPxCommand.h>

class attrInfo : public MPxCommand
{
public:
	attrInfo();
	virtual	~attrInfo();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

