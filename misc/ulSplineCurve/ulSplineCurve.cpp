#include "ulSplineCurve.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>

#include <maya/MGlobal.h>

#error change the following to a unique value and then erase this line 
MTypeId ulSplineCurve::id( TypeIdBase+XX );
const MString ulSplineCurve::typeName( "ulSplineCurve" );

MObject ulSplineCurve::i_input;
MObject ulSplineCurve::o_output;

ulSplineCurve::ulSplineCurve()
{
}
ulSplineCurve::~ulSplineCurve()
{
}

MStatus ulSplineCurve::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	if( plug == o_output )
	{
		MDataHandle inputData = data.inputValue( i_input, &stat );

		MDataHandle outputData = data.outputValue( plug, &stat );

		data.setClean(plug);
		return MS::kSuccess;
	}
	return MS::kUnknownParameter;
}

void* ulSplineCurve::creator()
{
	return new ulSplineCurve();
}

MStatus ulSplineCurve::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_input = numAttr.create ("i_input","iin", MFnNumericData::kDouble, 0.01, &stat);
			numAttr.setMin(0.0);	
			numAttr.setMax(1.0);	
			::addAttribute(i_input, atInput);
		}
		{
			o_output = numAttr.create( "o_output", "out", MFnNumericData::kDouble, 0.01, &stat);
			::addAttribute(o_output, atOutput);
			stat = attributeAffects( i_input, o_output );
		}
		if( !MGlobal::sourceFile("AEulSplineCurveTemplate.mel"))
		{
			displayString("error source AEulSplineCurveTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

