#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ulSplineCurveNode.h
//
// Dependency Graph Node: ulSplineCurve

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 

class ulSplineCurve : public MPxNode
{
public:
	ulSplineCurve();
	virtual ~ulSplineCurve(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_input;			// Example input attribute
	static MObject o_output;		// Example output attribute

public:
	static MTypeId id;
	static const MString typeName;
};

