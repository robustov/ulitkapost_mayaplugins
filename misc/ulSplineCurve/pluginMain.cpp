#include "pre.h"

#include <maya/MFnPlugin.h>

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

// #include "ulSplineCurve.h"
	stat = plugin.registerNode( 
		ulSplineCurve::typeName, 
		ulSplineCurve::id, 
		ulSplineCurve::creator,
		ulSplineCurve::initialize );
	if (!stat) 
	{
		displayString("cant register %s", ulSplineCurve::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterNode( ulSplineCurve::id );
	if (!stat) displayString("cant deregister %s", ulSplineCurve::typeName.asChar());

	return stat;
}
