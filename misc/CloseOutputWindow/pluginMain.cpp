#include "pre.h"

#include <maya/MFnPlugin.h>
#include "closeOutputWindow.h"

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

	stat = plugin.registerCommand( "closeOutputWindow", closeOutputWindow::creator, closeOutputWindow::newSyntax );
	if (!stat) {
		displayString("cant register node closeOutputWindow");
		stat.perror("registerCommand");
		return stat;
	}

	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterCommand( "closeOutputWindow" );
	if (!stat) displayString("cant deregister closeOutputWindow");

	return stat;
}
