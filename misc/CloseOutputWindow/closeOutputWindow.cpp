//
// Copyright (C) 
// File: closeOutputWindowCmd.cpp
// MEL Command: closeOutputWindow

#include "closeOutputWindow.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <windows.h>

closeOutputWindow::closeOutputWindow()
{
}
closeOutputWindow::~closeOutputWindow()
{
}

MSyntax closeOutputWindow::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

//	syntax.addArg(MSyntax::kString);	// arg0
//	syntax.addFlag("f", "format", MSyntax::kString);
	return syntax;
}

MStatus closeOutputWindow::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

//	MString argsl0;
//	argData.getCommandArgument(0, argsl0);

	HWND hwnd = ::FindWindow(NULL, "Output Window");
	if(hwnd)
	{
		SetWindowText(hwnd, "");
		PostMessage(hwnd, WM_CLOSE, 0, 0);
	}
//	DestroyWindow(hwnd);
//	CloseWindow( hwnd);
	return MS::kSuccess;
}

void* closeOutputWindow::creator()
{
	return new closeOutputWindow();
}

