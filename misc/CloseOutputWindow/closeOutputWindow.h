#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: closeOutputWindowCmd.h
// MEL Command: closeOutputWindow

#include <maya/MPxCommand.h>

class closeOutputWindow : public MPxCommand
{
public:
	closeOutputWindow();
	virtual	~closeOutputWindow();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

