//
// Copyright (C) 
// File: FFCurveLocatorCmd.cpp
// MEL Command: FFCurveLocator

#include "FFCurveLocator.h"

#include <maya/MGlobal.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MRampAttribute.h>
#include "MathNgl/MathNgl.h"
#include "MathNmaya/MathNmaya.h"
#include "FFGridData.h"
#include "Math/perlinnoise.h"

MObject FFCurveLocator::i_curve;

MObject FFCurveLocator::i_width;

MObject FFCurveLocator::i_window;
	MObject FFCurveLocator::i_windowposition;
	MObject FFCurveLocator::i_time;
	MObject FFCurveLocator::i_noisemagnitude;
	MObject FFCurveLocator::i_noisefrequency;
	MObject FFCurveLocator::i_noisedimention;
	MObject FFCurveLocator::i_disgrowfactor;
	MObject FFCurveLocator::i_debugShowVessel;
	MObject FFCurveLocator::i_venToper;
	MObject FFCurveLocator::i_SubGrowWidthFactor;
	MObject FFCurveLocator::i_WidthFactorJitter;

MObject FFCurveLocator::i_grow;
	MObject FFCurveLocator::i_level;
	MObject FFCurveLocator::i_bInvertDirection;
	MObject FFCurveLocator::i_vessels;
	MObject FFCurveLocator::i_maxdist;
	MObject FFCurveLocator::maxdistJitter;
	MObject FFCurveLocator::i_maxDistRamp;
//	MObject FFCurveLocator::i_trueway_propencity;
	MObject FFCurveLocator::i_growfrequency;
	MObject FFCurveLocator::i_subGrowPropencity;
	MObject FFCurveLocator::i_subGrowPhaseOffset;
	MObject FFCurveLocator::i_SubGrowHistoryOffset;
	MObject FFCurveLocator::i_seed;

MObject FFCurveLocator::i_deformer;
	MObject FFCurveLocator::i_xynoisemagnitude;
	MObject FFCurveLocator::i_xynoisemagnitudeSubGrow;
	MObject FFCurveLocator::i_xynoisefrequency;
	MObject FFCurveLocator::i_xynoisedimention;
	MObject FFCurveLocator::deformToper;

// render
	MObject FFCurveLocator::texToper;
	MObject FFCurveLocator::shaderId;

MObject FFCurveLocator::o_control;



// �������� ������ � �������� ���������
void FFCurveLocator::init(
	CurveControl* pThis, 
	MFnNurbsCurve& curve,
	int growlevel, 
	bool bInvertDirection, 
	int vessels, 
	float maxdist, 
	float trueway_propencity 
	)
{
	pThis->growlevel = growlevel;
	pThis->bInvertDirection = bInvertDirection;
	pThis->vessels = vessels;
	pThis->maxdist = maxdist; 
	pThis->trueway_propencity = trueway_propencity;


	double start, end;
	curve.getKnotDomain( start, end );
	MPoint _pt1, _pt2;
	curve.getPointAtParam(start, _pt1);
	curve.getPointAtParam(end, _pt2);
	double len = curve.length();

	// 1. ���������� ��������� ������� � �������������� � �����
	Math::Vec3f pt1, pt2;
	::copy( pt1, _pt1);
	::copy( pt2, _pt2);

	// ����� ����� �������
	std::list<Math::Vec3f> list;
	list.push_back(pt1);
	list.push_back(pt2);
	init_rec(
		pThis, 
		0, 
		5, 
		curve,
		0, 
		(float)len, 
		list, 
		list.begin()
		);
	pThis->curve.clear();
	pThis->curve.reserve(list.size());
	std::list<Math::Vec3f>::iterator it = list.begin();
	for(;it != list.end(); it++)
	{
		pThis->curve.push_back(*it);
	}
}
// ����� ����� �������
void FFCurveLocator::init_rec(
	CurveControl* pThis, 
	int level, 
	int maxlevel, 
	MFnNurbsCurve& curve,
	float startlen, 
	float endlen, 
	std::list<Math::Vec3f>& list, 
	std::list<Math::Vec3f>::iterator it1 // ����� it -> (it++)
	)
{
	if( level>=maxlevel) 
		return;
	float midlen = (startlen+endlen)/2;
	double p1 = curve.findParamFromLength(midlen);

	MPoint _ptmid;
	curve.getPointAtParam(p1, _ptmid);
	Math::Vec3f ptmid;
	::copy(ptmid, _ptmid);

	std::list<Math::Vec3f>::iterator it2 = it1; it2++;
	if( it2==list.end()) return;

	std::list<Math::Vec3f>::iterator itmid = list.insert(it2, ptmid);

	init_rec(
		pThis, 
		level+1, 
		maxlevel, 
		curve,
		startlen, 
		midlen, 
		list, 
		it1);
	init_rec(
		pThis, 
		level+1, 
		maxlevel, 
		curve,
		midlen, 
		endlen, 
		list, 
		itmid);
}


FFCurveLocator::FFCurveLocator()
{
}
FFCurveLocator::~FFCurveLocator()
{
}

MStatus FFCurveLocator::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	if( plug == o_control )
	{
		return Build_Control(plug, data);
	}

	return MS::kUnknownParameter;
}
MStatus FFCurveLocator::Build_Control( const MPlug& plug, MDataBlock& data )
{
	displayStringD( "FFCurveLocator::Build_Control");
	MStatus stat;

	MDataHandle outputData = data.outputValue(plug, &stat );

	typedef FFControlData CLASS;
	CLASS* pData = NULL;

	bool bNewData = false;
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<CLASS*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( CLASS::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<CLASS*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	if( !pData->dllproc.isValid())
	{
		pData->dllproc.LoadFormated("curveControl", "furFractalMath.dll");
	}
	if( pData->dllproc.isValid())
	{
		if( !pData->control)
		{
			MathOld::IFFCONTROL_CREATOR pc = (MathOld::IFFCONTROL_CREATOR)pData->dllproc.proc;
			pData->control = (*pc)();
		}
		if(pData->control)
		{
			CurveControl* cc = (CurveControl*)pData->control;

			int level = data.inputValue( i_level, &stat ).asInt();
			float maxdist = (float)data.inputValue( i_maxdist).asDouble();

			maxdist = maxdist*(float)(pow((double)2, level));			

			MFnNurbsCurve curve( data.inputValue( i_curve).asNurbsCurveTransformed());
			this->init(
				cc,
				curve,
				level, 
				data.inputValue( i_bInvertDirection, &stat ).asBool(), 
				data.inputValue( i_vessels).asInt(), 
				maxdist, 
				0
//				(float)data.inputValue( i_trueway_propencity).asDouble()
				);
			cc->maxdistJitter = (float)data.inputValue( maxdistJitter).asDouble();
			MRampAttribute maxDistRamp(thisMObject(), i_maxDistRamp);
			cc->maxDistRamp.set(maxDistRamp, 0, 1, 1);
			cc->growfrequency = (float)data.inputValue( i_growfrequency).asDouble();
			cc->SubGrowPropencity = (float)data.inputValue( i_subGrowPropencity).asDouble();
			cc->SubGrowPhaseOffset = (float)data.inputValue( i_subGrowPhaseOffset).asDouble();
			cc->SubGrowHistoryOffset = (int)data.inputValue( i_SubGrowHistoryOffset).asDouble();
			cc->WidthFactorJitter = (float)data.inputValue( i_WidthFactorJitter).asDouble();
			cc->SubGrowWidthFactor = (float)data.inputValue( i_SubGrowWidthFactor).asDouble();
			cc->startSeed = data.inputValue(i_seed).asInt();

//			cc-> = data.inputValue( i_grow_growPoints).asInt();
//			cc->maxdist = (float)data.inputValue( i_maxdist).asDouble();
//			cc->trueway_propencity = (float)data.inputValue( i_trueway_propencity).asDouble();
//			cc->growlevel = data.inputValue( i_level, &stat ).asInt();

			cc->bWindow = data.inputValue(i_window).asBool();
			cc->width = (float)data.inputValue(i_width).asDouble();
			cc->windowparam = (float)data.inputValue(i_windowposition).asDouble();
			cc->time = (float)data.inputValue(i_time).asDouble();
			cc->noisemagnitude = (float)data.inputValue(i_noisemagnitude).asDouble();
			cc->noisefrequency = (float)data.inputValue(i_noisefrequency).asDouble();
			cc->noisedimention = (float)data.inputValue(i_noisedimention).asDouble();
			cc->disgrowfactor = (float)data.inputValue(i_disgrowfactor).asDouble();
			cc->debugShowVessel= data.inputValue(i_debugShowVessel).asInt();
			cc->venToper= (float)data.inputValue(i_venToper).asDouble();
			

			cc->bGrow = data.inputValue(i_grow).asBool();

			cc->bDeformer = data.inputValue( i_deformer).asBool();
			cc->xynoisemagnitude = (float)data.inputValue(i_xynoisemagnitude).asDouble();
			cc->xynoisemagnitudeSubGrow = (float)data.inputValue(i_xynoisemagnitudeSubGrow).asDouble();
			cc->xynoisefrequency = (float)data.inputValue(i_xynoisefrequency).asDouble();
			cc->xynoisedimention = (float)data.inputValue(i_xynoisedimention).asDouble();
			cc->deformToper = (float)data.inputValue(deformToper).asDouble();

			cc->texToper = (float)data.inputValue(texToper).asDouble();

			OcsData ocsData(thisMObject());
			cc->ReadAttributes(&ocsData);
		}
	}
	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

bool FFCurveLocator::isBounded() const
{
	MObject thisNode = thisMObject();
	return false;
}

MBoundingBox FFCurveLocator::boundingBox() const
{
	MObject thisNode = thisMObject();

	MBoundingBox box;
	//...
	return box;
}

void FFCurveLocator::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MObject thisNode = thisMObject();

//	MPlug plug(thisNode, i_input);

	view.beginGL(); 
	//...
	view.endGL();
};


void* FFCurveLocator::creator()
{
	return new FFCurveLocator();
}

MStatus FFCurveLocator::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_window = numAttr.create ("bWindow","bwin", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_window, atInput);

			i_windowposition = numAttr.create ("windowPosition","gwp", MFnNumericData::kDouble, 1, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_windowposition, atInput|atKeyable);

			i_width = numAttr.create ("width","wi", MFnNumericData::kDouble, 1, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(2);
			::addAttribute(i_width, atInput);

			i_WidthFactorJitter = numAttr.create ("widthFactorJitter","wifj", MFnNumericData::kDouble, 0.2, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(i_WidthFactorJitter, atInput);

		}

		{
			i_curve = typedAttr.create( "curve", "cu", MFnData::kNurbsCurve);
			typedAttr.setReadable(false);
//			typedAttr.setArray(true);
//			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
//			typedAttr.setIndexMatters(false);
			::addAttribute(i_curve, atInput );

			i_time = numAttr.create ("time","iti", MFnNumericData::kDouble, 0.01, &stat);
			::addAttribute(i_time, atInput);

			i_noisemagnitude = numAttr.create ("noiseMagnitude","nom", MFnNumericData::kDouble, 0, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_noisemagnitude, atInput);

			i_noisefrequency = numAttr.create ("noiseFrequency","nof", MFnNumericData::kDouble, 0.033, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_noisefrequency, atInput);

			i_noisedimention = numAttr.create ("noiseDimention","nod", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.0001);
			numAttr.setSoftMax(2);
			::addAttribute(i_noisedimention, atInput);

			i_disgrowfactor = numAttr.create ("disGrowFactor","dgf", MFnNumericData::kDouble, 2, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(4);
			::addAttribute(i_disgrowfactor, atInput);

			i_debugShowVessel = numAttr.create ("debugShowVessel","dbgvf", MFnNumericData::kInt, -1, &stat);
			numAttr.setMin(-1);
			numAttr.setSoftMax(20);
			::addAttribute(i_debugShowVessel, atInput);
			
			i_venToper = numAttr.create ("venToper","vent", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(i_venToper, atInput);

			i_SubGrowWidthFactor = numAttr.create ("subGrowWidthFactor","sgwf", MFnNumericData::kDouble, 0.5, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(i_SubGrowWidthFactor, atInput);

		}
		{
			i_grow = numAttr.create ("bGrow","bgr", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_grow, atInput);
			
			i_vessels = numAttr.create ("vessels","gve", MFnNumericData::kInt, 6, &stat);
			numAttr.setMin(1);
			numAttr.setSoftMax(10);
			::addAttribute(i_vessels, atInput);

			i_maxdist = numAttr.create ("maxDist","gmd", MFnNumericData::kDouble, 2, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(3);
			::addAttribute(i_maxdist, atInput);

			i_maxDistRamp = MRampAttribute::createCurveRamp("maxDistRamp", "mdr");
			::addAttribute(i_maxDistRamp, atInput);

			maxdistJitter = numAttr.create ("maxdistJitter","gmdj", MFnNumericData::kDouble, 0.5, &stat);
			numAttr.setMin(0);
			numAttr.setMax(0.99);
			::addAttribute(maxdistJitter, atInput);
			

//			i_trueway_propencity = numAttr.create ("truewayPropencity","gtwp", MFnNumericData::kDouble, 0.7, &stat);
//			numAttr.setMin(0);
//			numAttr.setMax(1);
//			::addAttribute(i_trueway_propencity, atInput);

			i_growfrequency = numAttr.create ("growFrequency","grfr", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.001);
			numAttr.setSoftMax(10);
			::addAttribute(i_growfrequency, atInput);

			i_subGrowPropencity = numAttr.create ("subGrowPropencity","sgpr", MFnNumericData::kDouble, 0.6, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(2);
			::addAttribute(i_subGrowPropencity, atInput);

			i_subGrowPhaseOffset = numAttr.create ("subGrowPhaseOffset","sgpo", MFnNumericData::kDouble, 0.2, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(i_subGrowPhaseOffset, atInput);

			i_SubGrowHistoryOffset = numAttr.create ("subGrowHistoryOffset","sgho", MFnNumericData::kDouble, 20, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(40);
			::addAttribute(i_SubGrowHistoryOffset, atInput);

			i_level = numAttr.create ("level","gle", MFnNumericData::kInt, 1, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(4);
			::addAttribute(i_level, atInput);

			i_bInvertDirection = numAttr.create ("invertDirection","ind", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_bInvertDirection, atInput);
			
			i_seed = numAttr.create ("seed","se", MFnNumericData::kInt, 0, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1000);
			::addAttribute(i_seed, atInput);

		}

		MObject xynoisemagnitudemin;
		{
			i_deformer = numAttr.create ("bDeformer","bde", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_deformer, atInput);

			i_xynoisemagnitude = numAttr.create ("xyNoiseMagnitude","xynm", MFnNumericData::kDouble, 0.5, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_xynoisemagnitude, atInput);

			xynoisemagnitudemin = numAttr.create ("xynoisemagnitudemin","xynmm", MFnNumericData::kDouble, 1, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(xynoisemagnitudemin, atInput);


			i_xynoisemagnitudeSubGrow = numAttr.create ("xynoisemagnitudeSubGrow","xynms", MFnNumericData::kDouble, 0.5, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_xynoisemagnitudeSubGrow, atInput);
			

			i_xynoisefrequency = numAttr.create ("xyNoiseFrequency","xynf", MFnNumericData::kDouble, 0.033, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_xynoisefrequency, atInput);

			i_xynoisedimention = numAttr.create ("xyNoiseDimention","xynd", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.0001);
			numAttr.setSoftMax(2);
			::addAttribute(i_xynoisedimention, atInput);
			
			deformToper = numAttr.create ("deformToper","deto", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.0001);
			numAttr.setSoftMax(2);
			::addAttribute(deformToper, atInput);
		}
		{
			texToper = numAttr.create ("texToper","teto", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.0001);
			numAttr.setSoftMax(2);
			::addAttribute(texToper, atInput);

			shaderId = numAttr.create ("shaderId","shid", MFnNumericData::kInt, 0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(4);
			::addAttribute(shaderId, atInput);
		}

		{
			o_control = typedAttr.create( "control", "con", FFControlData::id);
			::addAttribute(o_control, atReadable|atWritable|atStorable|atCached|atConnectable);

			attributeAffects(i_curve, o_control);

			attributeAffects(i_window,			o_control);
			attributeAffects(i_windowposition,	o_control);
			attributeAffects(i_width,			o_control);

			attributeAffects(i_noisemagnitude,	o_control);
			attributeAffects(i_noisefrequency,	o_control);
			attributeAffects(i_noisedimention,	o_control);
			attributeAffects(i_time,			o_control);
			attributeAffects(i_disgrowfactor,	o_control);
			attributeAffects(i_debugShowVessel,	o_control);
			attributeAffects(i_venToper,		o_control);		
			attributeAffects(i_WidthFactorJitter,	o_control);
			attributeAffects(i_SubGrowWidthFactor,	o_control);

			attributeAffects(i_grow,			o_control);
			attributeAffects(i_level,			o_control);
			attributeAffects(i_vessels,			o_control);
			attributeAffects(i_maxdist,			o_control);
			attributeAffects(maxdistJitter,			o_control);
			attributeAffects(i_maxDistRamp,			o_control);
//			attributeAffects(i_trueway_propencity, o_control);
			attributeAffects(i_growfrequency, o_control);
			attributeAffects(i_bInvertDirection, o_control);
			attributeAffects(i_subGrowPropencity, o_control);
			attributeAffects(i_subGrowPhaseOffset, o_control);
			attributeAffects(i_SubGrowHistoryOffset, o_control);
			attributeAffects(i_seed,			o_control);

			attributeAffects(i_deformer,		o_control);
			attributeAffects(i_xynoisemagnitude, o_control);
			attributeAffects(xynoisemagnitudemin,	o_control);
			attributeAffects(i_xynoisemagnitudeSubGrow, o_control);
			attributeAffects(i_xynoisefrequency, o_control);
			attributeAffects(i_xynoisedimention, o_control);
			attributeAffects(deformToper, o_control);

			attributeAffects(texToper, o_control);
			attributeAffects(shaderId, o_control);
			
		}
		if( !MGlobal::sourceFile("AEFFCurveLocatorTemplate.mel"))
		{
			displayString("error source AEFFCurveLocatorTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}