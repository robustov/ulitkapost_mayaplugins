#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"
#include "Math/GridOld.h"
#include "IFFWindow.h"
#include "IFFGrow.h"
#include "Math/GridData.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", FFGridData::id );
	}
/*/

struct EdgeParams
{
	bool computed;

	EdgeParams()
	{
		computed = false;
	}
};

class FFWindowData;
class MFnNurbsCurve;

class FFGridData : public MPxData
{
public:
	GridData data;

	// ��� ����������� �����
	std::string loadedFilename;
	// ��� ���� - ����������� � HairControlAnim
	std::string filename;
	// ���� ��� �������� ����������
	bool bModify;

	void serializeData(Util::Stream& stream);

public:
	void init(
		int seed, 
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		std::vector<MathOld::IFFGrow*>& ffgr,
		float dim
		);
	
	void Step(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		std::vector<MathOld::IFFGrow*>& grows, 
		float randomOffset
		);
	// ����� ���������
	virtual void GrowStop(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		std::vector<MathOld::IFFGrow*>& grows
		);

	void draw(
		std::vector<MathOld::IGridDeformer*>& deforms,
		std::vector<MathOld::IFFWindow*>& ffds, 
		Math::Matrix4f& worldm, 
		float widthScaleValue, 
		bool bViewGridDots,
		bool bColor
		);
	void drawVertexes(
		int viewGridLevel, std::vector<MathOld::IGridDeformer*>& deforms, Math::Matrix4f& worldm, 
		Math::Vec3f viewpoint );

protected:

public:
	FFGridData();
	virtual ~FFGridData(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;

protected:
	void serialize(Util::Stream& stream);
};

