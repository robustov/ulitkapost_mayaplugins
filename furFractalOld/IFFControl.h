#pragma once

#include "Util/Stream.h"
#include "Math/GridOld.h"
#include "IFFWindow.h"
#include "IFFGrow.h"
#include "ocellaris/IRender.h"
#include "IOcsData.h"

namespace MathOld
{
	struct IFFControl : public IFFWindow, public IFFGrow, public MathOld::IGridDeformer
	{
		virtual bool isWindow()const=0;
		virtual bool isGrow()const=0;
		virtual bool isDeformer()const=0;

		virtual void copy(
			const IFFControl* arg
			)
		{
			this->growid = arg->growid;
		}
		virtual void release()
		{
			delete this;
		};

		// ������
	//	virtual void* createHistoryData()=0;
	//	virtual void deleteHistoryData(void* data)=0;

		// 
		virtual void serialize(Util::Stream& stream)
		{
			stream >> growid;
		}
		virtual void SerializeOcs(
			const char* _name, 
			cls::IRender* render, 
			bool bSave
			)=0;

		// ������ ��������� �� �����
		virtual void ReadAttributes(
			IOcsData* node
			){};


	public:
		IFFControl()
		{
			ReinitGrowId();
		}
		void ReinitGrowId()
		{
			void* p = this;
			this->growid = *(int*)(&p);
		}
		int growid;
	};
	typedef IFFControl* (*IFFCONTROL_CREATOR)();
}


