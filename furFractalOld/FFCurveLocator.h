#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>
#include "FFControlData.h"
#include "Math/CurveControl.h"

class MFnNurbsCurve;

class FFCurveLocator : public MPxNode //MPxLocatorNode
{
public:
	FFCurveLocator();
	virtual ~FFCurveLocator(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	static void* creator();
	static MStatus initialize();

	MStatus Build_Control( const MPlug& plug, MDataBlock& data );

public:
	// ��������� ����� (��� ��� ��������)
	static MObject i_window;
	static MObject i_windowposition;
	static MObject i_time;
	static MObject i_noisemagnitude;
	static MObject i_noisefrequency;
	static MObject i_noisedimention;
	static MObject i_disgrowfactor;
	static MObject i_debugShowVessel;
	static MObject i_width, i_WidthFactorJitter;
	static MObject i_SubGrowWidthFactor;
	static MObject i_venToper;

	static MObject i_grow;
	static MObject i_curve;
	static MObject i_level;
	static MObject i_bInvertDirection;
	static MObject i_vessels;
	static MObject i_maxdist, maxdistJitter;
	static MObject i_maxDistRamp;
//	static MObject i_trueway_propencity;
	static MObject i_growfrequency;
	static MObject i_subGrowPropencity, i_subGrowPhaseOffset, i_SubGrowHistoryOffset;
	static MObject i_seed;

	static MObject i_deformer;
	static MObject i_xynoisemagnitude, i_xynoisemagnitudeSubGrow;
	static MObject i_xynoisefrequency;
	static MObject i_xynoisedimention;
	static MObject deformToper;

	// render
	static MObject texToper;
	static MObject shaderId;

	static MObject o_control;

public:
	static MTypeId id;
	static const MString typeName;

protected:
	void init(
		CurveControl* pThis, 
		MFnNurbsCurve& curve,
		int growLevel, 
		bool bInvertDirection, 
		int growPoints, 
		float grow_maxdist,
		float trueway_propencity 
		);
	// ����� ����� �������
	void init_rec(
		CurveControl* pThis, 
		int level, 
		int maxlevel, 
		MFnNurbsCurve& curve,
		float startlen, 
		float endlen, 
		std::list<Math::Vec3f>& list, 
		std::list<Math::Vec3f>::iterator it // ����� it -> (it++)
		);

};


