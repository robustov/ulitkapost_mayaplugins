#include "furFractal.h"
#include <maya/MAnimControl.h>
#include <maya/MDagPath.h>
#include <maya/MFnPluginData.h>
#include "MathNgl/MathNgl.h"
#include "MathNmaya/MathNmaya.h"
#include "FFGridData.h"
#include "Math/Spline.h"
#include "ocellaris/OcellarisExport.h"
#include "Util/FileStream.h"
#include "ocellaris/renderFileImpl.h"
#include "Util/misc_create_directory.h"

// 
// ��������� 
// 
furFractal::Generator furFractal::generator;

//! ����������� ���� � �������� 
bool furFractal::Generator::OnPrepareNode(
	cls::INode& node, 
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	MObject obj = node.getObject();

	// ������� Grid Data � ����
	std::string filename;
	filename = context->getEnvironment("RIBDIR");
	filename+="/";
	filename+=node.getName();
	filename+=".griddata";
	Util::correctFileName((char*)filename.c_str());

	Util::FileStream file;
	Util::create_directory_for_file(filename.c_str());
	if( !file.open(filename.c_str(), true))
	{
		fprintf(stderr, "Cant open %s\n", filename.c_str());
		return false;
	}

	MFnDependencyNode dn(obj);
	if( dn.typeId()!=furFractal::id)
		return false;

	furFractal* ff = (furFractal*)dn.userNode();
	MDataBlock data = ff->forceCache();
	FFGridData* ffgriddata = ff->Load_Grid(data);
	GridData& gridData = ffgriddata->data;
	Math::Box3f dirtybox = gridData.getDirtyBox(true);

	gridData.serialize(file);

//		std::vector<IFFControl*> ffcs;
//		ff->LoadControls(data, ffcs);

	// ��������� ��� �����
	cls::IRender* objectContext = context->objectContext(obj, this);
	objectContext->Parameter("ff::filename", cls::PS(filename));
	objectContext->Parameter("ff::dirtybox", dirtybox);

	// ������ ���� ��� ��������!
	{
		// ������� Grid Data � ����
		std::string filename;
		filename = context->getEnvironment("RIBDIR");
		filename+="/";
		filename+=node.getName();
		filename+=".proxy.ocs";
		Util::correctFileName((char*)filename.c_str());

		cls::renderFileImpl file;
		file.openForWrite(filename.c_str(), true);

		// ��������� ������ �������!!!
		ff->RenderProxy(&file);
	}

	return true;
}

bool furFractal::Generator::OnGeometry(
	cls::INode& node,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject obj = node.getObject();

	// ������������ ��������� ���������� � ��������
	{
		std::string filename;
		cls::IRender* objectContext = context->objectContext(obj, this);
		objectContext->GetParameter("ff::filename", filename);

		objectContext->GetParameter("ff::dirtybox", box);

		cls::P<int> rType = ocExport_GetAttrValue( obj, "rType" );

		cls::P<float> renderWidth = ocExport_GetAttrValue( obj, "renderWidth" );

		cls::P<int> tesselation = ocExport_GetAttrValue( obj, "tesselation" );

		MFnDagNode dagFn( obj );
		MDagPath path;
		dagFn.getPath( path );
		Math::Matrix4f worldFFMatrix;
		::copy( worldFFMatrix, path.inclusiveMatrix() );

		render->Parameter("ff::filename", cls::PS(filename));
		render->Parameter("ff::worldFFMatrix", worldFFMatrix);
		render->Parameter("ff::rType", rType);
		render->Parameter("ff::renderWidth", renderWidth);
		render->ParameterFromAttribute("ff::renderWidth", "global::ff_renderWidth");
render->GlobalAttribute("global::ff_renderWidth", renderWidth);
		render->Parameter("ff::tesselation", tesselation);
		render->Parameter("ff::dirtybox", box);
		

		// Save Controls
		MFnDependencyNode dn(obj);
		if( dn.typeId()!=furFractal::id)
			return false;
		furFractal* ff = (furFractal*)dn.userNode();
		MDataBlock data = ff->forceCache();
		std::vector<MathOld::IFFControl*> ffcs;
		std::vector<Util::DllProcedure*> dllproc;
		ff->LoadControls(data, ffcs, &dllproc);
		std::vector<std::string> controlnames;
		controlnames.reserve(ffcs.size());
		for(int c=0; c<(int)ffcs.size(); c++)
		{
			char buf[24];
			sprintf(buf, "ctrl%02d", c);
			if( !ffcs[c]) continue;

			Util::DllProcedure* dll = dllproc[c];
			if(!dll || !dll->isValid())
				continue;

			render->Parameter( (std::string(buf)+"::entrypoint").c_str(), dll->GetFormated().c_str());
			ffcs[c]->SerializeOcs(buf, render, true);
			controlnames.push_back(buf);
		}
		render->Parameter("ff::controlnames", cls::PSA(controlnames));

		render->RenderCall("furFractalMath.dll@FF");
		return true;
	}
}
