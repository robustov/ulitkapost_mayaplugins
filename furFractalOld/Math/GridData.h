#pragma once
#include "Math/Math.h"
#include "Math/GridOld.h"
#include "IFFWindow.h"
#include "IFFGrow.h"
#include "Util/Stream.h"
#include "furFractalMath.h"

namespace cls
{
	struct IRender;
}

#pragma warning (disable: 4251 4275)
class FURFRACTALMATH_API GridData
{
public:
	GridData(void);
	~GridData(void);

	void copy( const GridData* arg );

public:
	struct EdgeData
	{
		EdgeData()
		{
			power=0;
			bGrowDirection = true;
		}
		// iffdata
		MathOld::IFFEdgeData iffdata;
		// ���� �� ������������
		float power;
		// ����������� ����� (true: v1 -> v2 false v2->v1)
		bool bGrowDirection;
	};

public:
	// ����� ���������
	int lastStep;
	// seed
	int currentseed;

// ���������:
	// ����
	MathOld::Grid grid;
	// ������� �����
	typedef MathOld::GridFreeEdge Edge;
	std::map< Edge, EdgeData> alives;
	// ������� ��������
	std::set<MathOld::GridCellId> busies;
	// ��������� ����� (����)
	std::vector< std::vector<MathOld::GridCellId> > curves;
	// ������� ������� ��� �������� ������ ����� (����)
	std::vector< std::pair< Math::Vec3f, Math::Vec3f > > baseNormals;
	// ����� edge'� �������� � �������� (����)
	std::map< MathOld::GridCellId, std::list<MathOld::GridFreeEdge> > vertexLinks;

	// ��� �����
	int boxtimestamp;
	Math::Box3f box;

	// ������� ������� ����. bExpanded - ��������� ������ ��� ����������
	Math::Box3f getDirtyBox(bool bExpanded=false);

public:
	// ������� ��� ����� ������
	void allEdges(
		const MathOld::GridEdge& edge, 
		bool sameLevel, 
		std::set<MathOld::GridEdge>& neibouredges
		);
	// ������� ��� ����� ������
	void allEdges(
		const MathOld::GridFreeEdge& edge, 
		bool sameLevel, 
		std::set<MathOld::GridFreeEdge>& neibouredges
		);
	// ����� �� �������� ����� �����
	bool canAddEdge(
		const MathOld::GridEdge& e
		);

	static MathOld::GridCellId getLowestCellPos(MathOld::Grid& grid, MathOld::GridCellId id);

	// ������� ����� �� ������������ ���������
	static bool subdivEdge( MathOld::Grid& grid, const MathOld::GridEdge& edge, std::vector<MathOld::GridCellId>& cells);
	// ������� ����� �� ������������� ������
	static bool subdivEdge( MathOld::Grid& grid, const MathOld::GridFreeEdge& edge, std::vector<MathOld::GridCellId>& cells, int level);

	struct EdgeParams
	{
		bool computed;

		EdgeParams()
		{
			computed = false;
		}
	};

	// ���� � ��������� curves
	void buildCurves();

	// ����� ����� �����, ��������� edge'� � ���� ������� ( �� pEdge )
	std::vector< MathOld::GridFreeEdge > buildHalfCurve(
		const MathOld::GridFreeEdge& cEdge,
		const MathOld::GridFreeEdge& pEdge,
		std::map< MathOld::GridFreeEdge, EdgeParams >& allEdges,
		std::map< MathOld::GridCellId, std::list<MathOld::GridFreeEdge> >& vertexLinks
		);

	// ������� ����� ����� (�����), ���������� edge( ������� buildHalfCurve � ������ �������, ���� ��������� )
	std::vector< MathOld::GridFreeEdge > buildCurve(
		const MathOld::GridFreeEdge& edge,
        std::map< MathOld::GridFreeEdge, EdgeParams >& allEdges,
		std::map< MathOld::GridCellId, std::list<MathOld::GridFreeEdge> >& vertexLinks
		);

public:
	// ��������� ������� � ocs
	// ���������� � RenderProc
	bool Render(
		Math::Matrix4f& worldFFMatrix, 
		std::vector<MathOld::IFFWindow*>& ffws,
		int renderType, 
		float renderWidth, 
		int tesselation, 
		cls::IRender* render, 
		bool bSingleGeometry=false
		);
	// ��������� ������� ��������� ������ � ocs (��� ������� render->System(...) )
	// ���������� � RenderProc
	bool RenderSingleCurve(
		const std::vector<MathOld::GridCellId>& curve,
		const std::pair< Math::Vec3f, Math::Vec3f >& curveBaseNormals,
		Math::Matrix4f& worldFFMatrix, 
		std::vector<MathOld::IFFWindow*>& ffws,
		int renderType, 
		float renderWidth, 
		int tesselation, 
		cls::IRender* render
		);

public:
	void clear();
	void serialize(Util::Stream& stream);
};


template<class Stream> inline
Stream& operator >> (Stream& out, GridData::EdgeData& v)
{
	unsigned char version = 0;
	out >> version;
	if( version>=0)
	{
		out >> v.power;
		out >> v.iffdata;
	}
	return out;
}

