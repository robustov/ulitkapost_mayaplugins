#include "StdAfx.h"
#include ".\griddata.h"

GridData::GridData(void)
{
	lastStep = 0;
	boxtimestamp = -1;
}

GridData::~GridData(void)
{
}

void GridData::clear()
{
	// ����� ���������
	lastStep = -1;
	currentseed = 0;
// ���������:
	// ����
	grid.clear();
	alives.clear();
	// ������� ��������
	busies.clear();
	// ��������� ����� (����)
	curves.clear();
	// ������� ������� ��� �������� ������ ����� (����)
	baseNormals.clear();
	// ����� edge'� �������� � �������� (����)
	vertexLinks.clear();

	// ��� �����
	boxtimestamp=-1;
}

void GridData::copy(const GridData* arg)
{
//	this->data = arg->data;
	this->lastStep = arg->lastStep;
	this->currentseed = arg->currentseed;
	this->alives = arg->alives;
	this->busies = arg->busies;
	this->grid = arg->grid;
//	this->verts = arg->verts;
//	this->ed = arg->ed;
//	this->widths = arg->widths;

//	this->growedge = arg->growedge;
//	this->growgrowdir = arg->growgrowdir;
//	this->startGrowPoint = arg->startGrowPoint;
//	this->endGrowPoint = arg->endGrowPoint;
//	this->growpoint = arg->growpoint;

	DATACOPY(curves);
	DATACOPY(vertexLinks);
	DATACOPY(baseNormals);

	DATACOPY(boxtimestamp);
	DATACOPY(box);
}
void GridData::serialize(Util::Stream& stream)
{
	int version = 6;
	stream >> version;
	if( version<6)
		return;

	if( version>=6)
	{
		stream >> lastStep;
		stream >> currentseed;

		stream >> busies;
		stream >> grid;

		stream >> curves;

		stream >> alives;

		stream >> vertexLinks;

		stream >> boxtimestamp;
		stream >> box;

		stream >> baseNormals;
	}
}

Math::Box3f GridData::getDirtyBox(bool bExpanded)
{
	if( !bExpanded && 
		grid.timestamp!=-1 && 
		this->boxtimestamp==grid.timestamp)
	{
		return this->box;
	}

	Math::Box3f box;
	grid.timestamp++;
	std::map<int, MathOld::Grid::Level>::iterator itl = grid.levels.begin();
	for(;itl != grid.levels.end(); itl++)
	{
		MathOld::Grid::Level& level = itl->second;
		std::map< MathOld::GridCellId, MathOld::GridCell>::iterator it = level.cells.begin();
		for( ; it != level.cells.end(); it++ )
		{
			MathOld::GridCell& cell = it->second;
			Math::Vec3f pt = grid.getCellPos(cell, false);
			if( bExpanded)
			{
				box.insert(pt+Math::Vec3f(grid.getLevelDimention(cell.id.level)));
				box.insert(pt-Math::Vec3f(grid.getLevelDimention(cell.id.level)));
			}
			else
			{
				box.insert(pt);
			}
		}
	}

	this->boxtimestamp = grid.timestamp;
	this->box = box;

	return box;
}

bool GridData::canAddEdge(const MathOld::GridEdge& e)
{
	if( alives.find(e)!=alives.end()) 
		return false;
	if( busies.find( e.v1())!=busies.end() &&
		busies.find( e.v2())!=busies.end()
		)
		return false;
	return true;
}

void GridData::allEdges(const MathOld::GridEdge& edge, bool sameLevel, std::set<MathOld::GridEdge>& neibouredges)
{
	if(sameLevel)
	{
		for(int i=0; i<6; i++)
		{
			MathOld::GridCellId v = edge.v1();
			MathOld::GridEdge e(v, (MathOld::enGridNeigbour3d)i);
			if( canAddEdge(e))
				neibouredges.insert(e);
		}
		for(int i=0; i<6; i++)
		{
			MathOld::GridCellId v = edge.v2();
			MathOld::GridEdge e(v, (MathOld::enGridNeigbour3d)i);
			if( canAddEdge(e))
				neibouredges.insert(e);
		}
//		neibouredges.erase(edge);
	}
	else
	{
		MathOld::GridCellId v = MathOld::Grid::getLowCell(edge.v);
		MathOld::GridEdge edge1(v, (MathOld::enGridNeigbour3d)edge.nb);
		MathOld::GridEdge edge2(edge1.v2(), (MathOld::enGridNeigbour3d)edge.nb);

		allEdges(edge1, true, neibouredges);
		allEdges(edge2, true, neibouredges);
	}
}
void GridData::allEdges(const MathOld::GridFreeEdge& edge, bool sameLevel, std::set<MathOld::GridFreeEdge>& neibouredges)
{
	if(sameLevel)
	{
		for(int i=0; i<6; i++)
		{
			MathOld::GridCellId v = edge.v1();
			MathOld::GridEdge e(v, (MathOld::enGridNeigbour3d)i);
			if( canAddEdge(e))
				neibouredges.insert(e);
		}
		for(int i=0; i<6; i++)
		{
			MathOld::GridCellId v = edge.v2();
			MathOld::GridEdge e(v, (MathOld::enGridNeigbour3d)i);
			if( canAddEdge(e))
				neibouredges.insert(e);
		}
//		neibouredges.erase(edge);
	}
	else
	{
		MathOld::GridCellId v1 = MathOld::Grid::getLowCell(edge.v1());
		MathOld::GridCellId v2 = MathOld::Grid::getLowCell(edge.v2());
		Math::Vec3i dir = MathOld::GridFreeEdge(v1, v2).direction();
		dir.x /=2;
		dir.y /=2;
		dir.z /=2;
		MathOld::GridCellId vmid(v1.level, v1.getXYZ()+dir);
		MathOld::GridFreeEdge edge1(v1, vmid);
		MathOld::GridFreeEdge edge2(vmid, v2);

		allEdges(edge1, true, neibouredges);
		allEdges(edge2, true, neibouredges);
	}
}

// ����� ����� ������� ���� edge'��
MathOld::GridCellId commonVertex( const MathOld::GridFreeEdge& edge1, const MathOld::GridFreeEdge& edge2 )
{
	if( edge1.v1() == edge2.v1() ) return edge1.v1();
	if( edge1.v1() == edge2.v2() ) return edge1.v1();
	if( edge1.v2() == edge2.v1() ) return edge1.v2();
	if( edge1.v2() == edge2.v2() ) return edge1.v2();

	return MathOld::GridCellId();
}

// ���� � ��������� curves
void GridData::buildCurves()
{
	this->curves.clear();
	this->vertexLinks.clear();

	std::map<GridData::Edge, GridData::EdgeData>::const_iterator it = alives.begin();

	std::map< MathOld::GridFreeEdge, EdgeParams > allEdges;

	for( ; it != alives.end(); it++ )
	{
		MathOld::GridFreeEdge edge = it->first;
		allEdges[edge] = EdgeParams();

		this->vertexLinks[ edge.v1() ].push_back( edge );
		this->vertexLinks[ edge.v2() ].push_back( edge );
	}

	it = alives.begin();

	for( ;it != alives.end(); it++ )
	{
		const MathOld::GridFreeEdge& edge = it->first;

		if( allEdges[edge].computed == true )
		{
			continue;
		}

		std::vector< MathOld::GridCellId > curve;
		std::vector< MathOld::GridFreeEdge > curveEdges = buildCurve( edge, allEdges, vertexLinks );

		unsigned int numSegments = (int)curveEdges.size();

		if( numSegments > 1 )
		{
			// ���������� ������� ( ������ ��������� �� ��������� edge'�� )
			for( unsigned int i = 0; i < numSegments-1; i++ )
			{
				MathOld::GridCellId cVert = commonVertex( curveEdges[i], curveEdges[i+1] );

				if( curveEdges[i].v1() == cVert )
				{
					curve.push_back( curveEdges[i].v2() );
				}
				else
				{
					curve.push_back( curveEdges[i].v1() );
				}
			}

			// �������� ��� ��������� �������
			MathOld::GridCellId cVert = commonVertex( curveEdges[numSegments-2], curveEdges[numSegments-1] );

			if( curveEdges[numSegments-1].v1() == cVert )
			{
				curve.push_back( curveEdges[numSegments-1].v1() );
				curve.push_back( curveEdges[numSegments-1].v2() );
			}
			else
			{
				curve.push_back( curveEdges[numSegments-1].v2() );
				curve.push_back( curveEdges[numSegments-1].v1() );
			}
		}
		// ���� ����� ������� �� ������ ��������
		else
		{
			curve.push_back( curveEdges[0].v1() );
			curve.push_back( curveEdges[0].v2() );
		}

		curves.push_back( curve );

		std::pair< Math::Vec3f, Math::Vec3f > normals;

		Math::Vec3f p1 = grid.getCellPos( curve[0], false );
		Math::Vec3f p2 = grid.getCellPos( curve[1], false );
		Math::Vec3f tangent = (p2 - p1).normalized();
		Math::Vec3f up(0,1,0);

		if( Math::dot( tangent, up ) == 1 )
		{
			normals.first = Math::Vec3f(1,0,0);
			normals.second = Math::Vec3f(0,0,1);
		}
		else if( Math::dot( tangent, up ) == -1 )
		{
			normals.first = Math::Vec3f(1,0,0);
			normals.second = Math::Vec3f(0,0,-1);
		}
		else
		{
			Math::Vec3f uVector = Math::cross( tangent, up ).normalized();
			Math::Vec3f normal = Math::cross( tangent, uVector ).normalized();

			normals.first = uVector;
			normals.second = normal;
		}

		baseNormals.push_back( normals );
	}
}

// ������� ����� ����� (�����), ���������� edge( ������� buildHalfCurve � ������ �������, ���� ��������� )
std::vector< MathOld::GridFreeEdge > GridData::buildCurve(
	const MathOld::GridFreeEdge& edge,
    std::map< MathOld::GridFreeEdge, EdgeParams >& allEdges,
	std::map< MathOld::GridCellId, std::list<MathOld::GridFreeEdge> >& vertexLinks
	)
{
	MathOld::GridFreeEdge currentEdge = edge;

	std::vector< MathOld::GridFreeEdge > curve;
	std::vector< MathOld::GridFreeEdge > halfCurveV1;
	std::vector< MathOld::GridFreeEdge > halfCurveV2;

	allEdges[currentEdge].computed = true;

	// ������� V1
	if( vertexLinks[currentEdge.v1()].size() == 2 )
	{
		MathOld::GridFreeEdge E1 = *vertexLinks[currentEdge.v1()].begin();

		if( E1 != currentEdge )
		{
			halfCurveV1 = buildHalfCurve( E1, currentEdge, allEdges, vertexLinks );
		}
		else
		{
			halfCurveV1 = buildHalfCurve( *(++vertexLinks[currentEdge.v1()].begin()), currentEdge, allEdges, vertexLinks );
		}
	}

	// ������� V2
	if( vertexLinks[currentEdge.v2()].size() == 2 )
	{
		MathOld::GridFreeEdge E1 = *vertexLinks[currentEdge.v2()].begin();

		if( E1 != currentEdge )
		{
			halfCurveV2 = buildHalfCurve( E1, currentEdge, allEdges, vertexLinks );
		}
		else
		{
			halfCurveV2 = buildHalfCurve( *(++vertexLinks[currentEdge.v2()].begin()), currentEdge, allEdges, vertexLinks );
		}
	}

	// �������� ����� �����
	for( unsigned int i = 0; i < halfCurveV1.size(); i++ )
	{
		curve.push_back( halfCurveV1[halfCurveV1.size()-1 - i] );
	}

	curve.push_back( currentEdge );

	for( unsigned int i = 0; i < halfCurveV2.size(); i++ )
	{
		curve.push_back( halfCurveV2[i] );
	}

	return curve;
}

// ����� ����� �����, ��������� edge'� � ���� ������� ( �� pEdge )
std::vector< MathOld::GridFreeEdge > GridData::buildHalfCurve(
	const MathOld::GridFreeEdge& cEdge,
	const MathOld::GridFreeEdge& pEdge,
	std::map< MathOld::GridFreeEdge, EdgeParams >& allEdges,
	std::map< MathOld::GridCellId, std::list<MathOld::GridFreeEdge> >& vertexLinks
	)
{
	std::vector< MathOld::GridFreeEdge > halfCurve;

	MathOld::GridFreeEdge currentEdge = cEdge;
	MathOld::GridFreeEdge prevEdge	= pEdge;

	// ������ adge
	MathOld::GridFreeEdge fCurrentEdge = cEdge;

	while( currentEdge != MathOld::GridEdgeNULL )
	{
		allEdges[currentEdge].computed = true;

		halfCurve.push_back( currentEdge );

		// ������� V1
		if( vertexLinks[currentEdge.v1()].size() == 2 )
		{
			MathOld::GridFreeEdge E1 = *vertexLinks[currentEdge.v1()].begin();
			MathOld::GridFreeEdge E2 = *(++vertexLinks[currentEdge.v1()].begin());

			if( E1 != prevEdge && E2 != prevEdge )
			{
				prevEdge = currentEdge;

				if( E1 != currentEdge )
				{
					currentEdge = E1;
				}
				else
				{
					currentEdge = E2;
				}

				// ���� ������ � ������� edge'� ������ ����� �� �����
				if( fCurrentEdge == currentEdge )
				{
					currentEdge = MathOld::GridEdgeNULL;
				}

				continue;
			}
		}

		// ������� V2
		if( vertexLinks[currentEdge.v2()].size() == 2 )
		{
			MathOld::GridFreeEdge E1 = *vertexLinks[currentEdge.v2()].begin();
			MathOld::GridFreeEdge E2 = *(++vertexLinks[currentEdge.v2()].begin());

			if( E1 != prevEdge && E2 != prevEdge )
			{
				prevEdge = currentEdge;

				if( E1 != currentEdge )
				{
					currentEdge = E1;
				}
				else
				{
					currentEdge = E2;
				}

				// ���� ������ � ������� edge'� ������ ����� �� �����
				if( fCurrentEdge == currentEdge )
				{
					currentEdge = MathOld::GridEdgeNULL;
				}

				continue;
			}
		}

		currentEdge = MathOld::GridEdgeNULL;
	}

	return halfCurve;
}

MathOld::GridCellId GridData::getLowestCellPos(MathOld::Grid& grid, MathOld::GridCellId id)
{
	MathOld::GridCellId lastid = id;
	for(;;)
	{
		id = MathOld::Grid::getLowCell(id);
		if(!grid.getCell(id))
			return lastid;
		lastid = id;
	}
}

// ������� ����� �� ������������ ���������
bool GridData::subdivEdge( MathOld::Grid& grid, const MathOld::GridEdge& edge, std::vector<MathOld::GridCellId>& cells)
{
	if( !grid.getCell(edge.v1()) ||
		!grid.getCell(edge.v2()))
	{
		return false;
	}


	MathOld::GridCellId v = MathOld::Grid::getLowCell(edge.v);
	MathOld::GridEdge edge1(v, (MathOld::enGridNeigbour3d)edge.nb);
	MathOld::GridEdge edge2(edge1.v2(), (MathOld::enGridNeigbour3d)edge.nb);

	if( !subdivEdge( grid, edge1, cells))
	{
		MathOld::GridCellId v = getLowestCellPos(grid, edge.v1());
		cells.push_back(v);
	}

	if( !subdivEdge( grid, edge2, cells))
	{
		MathOld::GridCellId v = getLowestCellPos(grid, edge.v2());
		cells.push_back( v);
	}
	return true;
}

// ������� ����� �� ������������� ������
bool GridData::subdivEdge( MathOld::Grid& grid, const MathOld::GridFreeEdge& edge, std::vector<MathOld::GridCellId>& cells, int level)
{
	if( edge.level() >= level)
	{
		cells.push_back(edge.v1());
		cells.push_back(edge.v2());
		return true;
	}

	MathOld::GridCellId v1 = MathOld::Grid::getLowCell(edge.v1());
	MathOld::GridCellId v2 = MathOld::Grid::getLowCell(edge.v2());
	Math::Vec3i vmid(v1.getXYZ()+v2.getXYZ());
	vmid = vmid/2;
	
	MathOld::GridFreeEdge edge1(v1.level, v1.getXYZ(), vmid);
	MathOld::GridFreeEdge edge2(v1.level, vmid, v2.getXYZ());

	subdivEdge( grid, edge1, cells, level);
	// �������� ������� ����� � �� ������������� �����
	cells.pop_back();

	subdivEdge( grid, edge2, cells, level);
	return true;
}
