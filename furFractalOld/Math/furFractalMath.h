#pragma once

#ifdef FURFRACTALMATH_EXPORTS
#define FURFRACTALMATH_API __declspec(dllexport)
#else
#define FURFRACTALMATH_API __declspec(dllimport)
#endif
