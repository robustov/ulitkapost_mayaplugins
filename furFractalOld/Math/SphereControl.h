#pragma once

#include "Math/Math.h"
#include "IFFControl.h"
#include "furFractalMath.h"
#include "GridData.h"
#include "Math/perlinnoise.h"

#pragma warning (disable: 4251 4275)
struct FURFRACTALMATH_API SphereControl : public MathOld::IFFControl
{
	bool bWindow, bDeformer, bGrow;

	// ��������� ����� �������� � init
	float bStart;
	float growUpPropencity;
	float growPropencity;
	float growLowPropencity;
	int maxLevel;
	int startLevel;
	int startSeed, seed;
	float maxGrow;					// ������� ��������� ���� �� ������?
	float maxGrowLow;					// ������� ��������� low ������?
	float sameFluctuation;		// ��������� ������ ������������ ��� ��������� �����������
	float samePhaseBiasConst;		// �������� �� ���� ��� ��������� �������
	float samePhaseBiasRnd;		// ��������� �������� ��� ��������� �������
	float samePhaseSecondaryFactor;	// ��������� ��� ��������� ��������

	// ��������� ����
	float windowSize;
	float disgrowfactor;
	float width0, widthExp;			// ������ � ����������
	float venToper;

	// ������������� � Start ��� Step
	Math::Matrix4f worldpos;
	Math::Matrix4f invworldpos;

	// deformer
	float time;
	float xynoisemagnitude, xynoisefrequency, xynoisedimention;
	static Math::PerlinNoise perlinnoise;
	float deformToper;

	// render
	float texToper;

	// ����� �����
	// ������ ������ ������������ (phase)
	struct GrowPoint
	{
		float phase;				// ������� ����
		Math::Vec3i direction;		// �����������
		int growCount;				// ������� ������� �� ��� �� ������
		int growCountLow;			// ������� ������� �� ������� ����
		float length;
		int threadId;
		MathOld::GridFreeEdge parent;	// �� ���� ����������
		GrowPoint(float phase=0, Math::Vec3i direction=Math::Vec3i(0), float length=0, int threadId=-1, MathOld::GridFreeEdge parent = MathOld::GridFreeEdge());
	};
	std::map<MathOld::GridFreeEdge, GrowPoint> growpoints;

	// ������� ����� (����������-phase)
//	float maxDistance, maxPhase;

public:
	SphereControl();
	void init(
		const Math::Matrix4f& worldpos
		);

	float getWidth(Math::Vec3f& v, const MathOld::GridCellId& id, float lenght, float lenghtFromEnd, float phase);
	// �������� ����� ��� ������� ������ � �����.���������
	float getGrowParam(Math::Vec3f& v, const MathOld::GridCellId& id, float lenght, float phase, float toper);

public:
	virtual bool isWindow()const{return bWindow;};
	virtual bool isGrow()const{return bGrow;};
	virtual bool isDeformer()const{return bDeformer;};

	virtual void copy(
		const MathOld::IFFControl* arg
		);

	virtual void serialize(Util::Stream& stream);
	virtual void SerializeOcs(const char* _name, cls::IRender* render, bool bSave);

	// ��������� ������ �� ����� � �����
	bool isInside(
		const MathOld::GridFreeEdge& edge, 
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata
		);

	bool addEdge(
		const MathOld::GridFreeEdge& edge, 
		const MathOld::GridFreeEdge& newedge, 
		const Math::Vec3i& direction, 
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata,					
		std::list<MathOld::IFFEdgeData>& added, 
		float phaseoffset,
		int threadId
		);

	bool StepUpLevel(
		const MathOld::GridFreeEdge& edge,		// �� ������ ����� ������
		GrowPoint& gp,						// ������ ����� �����
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata,					
		std::list<MathOld::IFFEdgeData>& added
		);
	bool StepSameLevel(
		const MathOld::GridFreeEdge& edge,		// �� ������ ����� ������
		GrowPoint& gp,						// ������ ����� �����
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata,					
		std::list<MathOld::IFFEdgeData>& added
		);
	bool StepLowLevel(
		const MathOld::GridFreeEdge& edge,		// �� ������ ����� ������
		GrowPoint& gp,						// ������ ����� �����
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata,					
		std::list<MathOld::IFFEdgeData>& added
		);

public:
	virtual float processEdge(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata,				// �������
		const MathOld::IFFEdgeData& edge, 
		float pointonedge,					// ����� �� ����� (�� 0 �� 1)
		float src_width
		);
	// ���������� �����.���������� ��� ����� �� �����
	virtual bool getTexCoord(
		float& texCoord, 
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata,					// �������
		const MathOld::IFFEdgeData& edge,			// �������� �����
		float pointonedge,					// ����� �� ����� (�� 0 �� 1)
		int texCoordType					// �� ������� ���� ��������� UV ����� ���� ������
		);
	//����� ��������� ����� ��� ������������ ����� �����
	virtual bool getMainPoint(
		const MathOld::GridCellId& v1,	//����������� ����� v1->v2
		const MathOld::GridCellId& v2,
		const std::list<MathOld::GridCellId>& v3list,	// ��������� ����� ���. ������� �� ������� v2
		GridData& griddata,
		MathOld::GridCellId& mainPoint //��������� �����, ���� ��������� true
		);

	float processCell(Math::Vec3f& v, const MathOld::GridCellId& cell, float src_width);
	// ��� ��������� �� ��������
	void draw(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata
		);

public:
	// ��������� �������
	virtual void Start(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata, 
		std::list<MathOld::IFFEdgeData>& added
		);
	// ��� ���������
	virtual void Step(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata, 
		std::list<MathOld::IFFEdgeData>& added
		);
	// ����� ���������
	virtual void GrowStop(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata, 
		std::list<MathOld::IFFEdgeData>& added
		);
	// ��������� lenghtFromEnd
	virtual void CalcLenghtFromEnd(
		GridData& griddata
		);

public:
	bool deform(
		const MathOld::Grid& grid, 
		const MathOld::GridCellId& id, 
		Math::Vec3f& v, 
		Math::Vec3f& offset,				// �������� �������� (������ ������������ ��� �����������)
		const MathOld::GridDeformerCell& deformercell,
		const Math::Matrix4f& worldMatrix);

	Math::Vec3f getNoise(
		float noisemagnitude, 
		float noisefrequency, 
		const MathOld::GridCellId& id, 
		int seed);

};
