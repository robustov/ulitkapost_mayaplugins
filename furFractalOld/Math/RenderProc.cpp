#include "stdafx.h"
#include "ocellaris/IProcedural.h"
#include "ocellaris/IRender.h"
#include "ocellaris/renderCacheImpl.h"
#include "math/spline.h"
#include "math/rotation3.h"
#include "math/perlinNoise.h"
#include "Util/DllProcedure.h"
#include "Util/FileStream.h"
#include "GridData.h"
#include "IFFControl.h"

enum RenderType
{
	CURVE_LINEAR	= 0,
	CURVE_CUBIC		= 1,
	MESH			= 2,
	SUBDIV			= 3,
	TEST			= 4,
};

// ����� ����� ������� ���� edge'��
MathOld::GridCellId commonVertex2(
	const MathOld::GridEdge& edge1,
	const MathOld::GridEdge& edge2
	);
// ����� ������ � ����� edge'�
float computeWidth(
	std::vector<MathOld::IFFWindow*>& ffws,
	Math::Matrix4f& worldMatrix,
	GridData& gridData,
	const MathOld::IFFEdgeData& edge,
	float pointOnEdge,
	float srcw 
	);
// ����� ���� �������� ( 1 - uVector �����, 2 - tangent ����� )
std::pair< Math::Vec3f, Math::Vec3f > computeNormals2p(
	Math::Vec3f& p1,
	Math::Vec3f& p2
	);
std::pair< Math::Vec3f, Math::Vec3f > computeNormals3pOrigin(
	Math::Vec3f& p1,
	Math::Vec3f& p2,
	Math::Vec3f& p3,
	const std::pair< Math::Vec3f, Math::Vec3f >& originNormals
	);
std::pair< Math::Vec3f, Math::Vec3f > computeNormals3p(
	Math::Vec3f& p1,
	Math::Vec3f& p2,
	Math::Vec3f& p3
	);
std::pair< Math::Vec3f, Math::Vec3f > computeNormals3pN(
	Math::Vec3f& p1,
	Math::Vec3f& p2,
	Math::Vec3f& p3,
    const std::pair< Math::Vec3f, Math::Vec3f >& prevNormals
	);
std::pair< Math::Vec3f, Math::Vec3f > computeNormalsN2p(
	const Math::Vec3f& normal,
	Math::Vec3f& p1,
	Math::Vec3f& p2
	);
// ����� ��������� ����� �� ������� ����� ( v1-> v2-> mainPointId )
bool getMainPointId(
	const MathOld::GridCellId& v1,
	const MathOld::GridCellId& v2,
	std::vector<MathOld::IFFWindow*>& window,
	GridData& gridData,
	std::map< MathOld::GridCellId, std::list<MathOld::GridFreeEdge> >& vertexLinks,
	MathOld::GridCellId& mainPointId
	);
// ����� ������� 2 ��������� ������ �� ������� ����� ( v1-> v2-> pos1-> pos2 )
void get2MainPointsPos(
	const MathOld::GridCellId& v1,
	const MathOld::GridCellId& v2,
	std::vector<MathOld::IFFWindow*>& window,
	MathOld::Grid& grid,
	GridData& gridData,
	std::map< MathOld::GridCellId, std::list<MathOld::GridFreeEdge> >& vertexLinks,
	Math::Vec3f& point3,
	Math::Vec3f& point4
	);
// ����� ������ �����, ��� ��������� ������� �����
bool getPointWidth(
	const MathOld::GridCellId& v1,
	const MathOld::GridCellId& v2,
	std::vector<MathOld::IFFWindow*>& window,
	GridData& gridData,
	const std::map< GridData::Edge, GridData::EdgeData>& alives,
	std::map< MathOld::GridCellId, std::list<MathOld::GridFreeEdge> >& vertexLinks,
	Math::Matrix4f& worldFFMatrix,
	float& widthAvg
	);
// ����� ���������� ���������� � �����
float computeTexCoord(
	std::vector<MathOld::IFFWindow*>& ffds,
	Math::Matrix4f& ffworldmatrix,	// world ������� �������� 
	GridData& griddata,				// �������
	const MathOld::IFFEdgeData& edge,		// �������� �����
	float pointonedge,				// ����� �� ����� (�� 0 �� 1)
	int texCoordType				// �� ������� ���� ��������� UV ����� ���� ������
	);
// ������� ��������� ��� ������, � ����������� �� ����
void computeCurveParams(
	int renderType,
	const std::vector<float>& param,
	cls::PA<float>& U,
	cls::PA<float>& V
	);

//! @ingroup implement_group
//! \brief ��������� ThreadRenderProc
//! 
//! ������ ����
//! ������� ��������� � ��������:
//! P - �������
//! N - �������
//! T - tangent
struct FFRenderProc : public cls::IProcedural
{
	// ������ ����������
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);

protected:
	bool BuildFF(cls::IRender* render);

	void TestRender(
		cls::IRender* render,
		GridData& gridData,
		std::vector<MathOld::IGridDeformer*>& deforms,
		std::vector<MathOld::IFFWindow*>& ffds, 
		Math::Matrix4f& worldm, 
		float renderWidth
		);
	void BoxRender(
		cls::IRender* render, 
		Math::Box3f& box,
		float w
		);
	void SolidBoxRender(
		cls::IRender* render, 
		Math::Box3f& box,
		float w
		);

	bool Draw();
};

FFRenderProc ffrenderproc;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IProcedural* __cdecl FF(cls::IRender* prman)
	{
		return &ffrenderproc;
	}
}

// ������ ����������
void FFRenderProc::Render(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
	if( !motionBlurSamples)
	{
		printf("FFRenderProc::Render\n");
//		Math::Box3f box(2);
//		SolidBoxRender(render, box, 0.1f);

//		render->GetParameter("ff::dirtybox", box);
//		SolidBoxRender(render, box, 0.1f);

		if( !BuildFF(render))
			return;
	}
	else
	{
//		std::vector<FFRenderData> data(motionBlurSamples);
		for(int i=0; i<motionBlurSamples; i++)
		{
			render->SetCurrentMotionPhase(motionBlurTimes[i]);
			if( !BuildFF(render))
			{
				render->SetCurrentMotionPhase(cls::nonBlurValue);
				return;
			}
		}
		render->SetCurrentMotionPhase(cls::nonBlurValue);
	}
}

void FFRenderProc::BoxRender(
	cls::IRender* render, 
	Math::Box3f& box, 
	float w
	)
{
	cls::PA<int> nverts(cls::PI_PRIMITIVE, 6);
	nverts[0] = 5;
	nverts[1] = 5;
	nverts[2] = 2;
	nverts[3] = 2;
	nverts[4] = 2;
	nverts[5] = 2;

	int i=0;
	cls::PA<Math::Vec3f> verts(cls::PI_VERTEX, 18);
	verts[i++] = box.corner(0);
	verts[i++] = box.corner(1);
	verts[i++] = box.corner(3);
	verts[i++] = box.corner(2);
	verts[i++] = box.corner(0);

	verts[i++] = box.corner(4);
	verts[i++] = box.corner(5);
	verts[i++] = box.corner(7);
	verts[i++] = box.corner(6);
	verts[i++] = box.corner(4);

	verts[i++] = box.corner(0);
	verts[i++] = box.corner(0+4);
	verts[i++] = box.corner(1);
	verts[i++] = box.corner(1+4);
	verts[i++] = box.corner(2);
	verts[i++] = box.corner(2+4);
	verts[i++] = box.corner(3);
	verts[i++] = box.corner(3+4);

	render->PushAttributes();
	render->Parameter("#width", w);
	render->Parameter("#curves::nverts", nverts);
	render->Parameter("P", verts);
	render->Parameter("#curves::interpolation", "linear");
	render->Parameter("#curves::wrap", "nonperiodic");
	render->SystemCall(cls::SC_CURVES);
	render->PopAttributes();
}
void FFRenderProc::SolidBoxRender(
	cls::IRender* render, 
	Math::Box3f& box,
	float w
	)
{
	cls::PA<int> nverts(cls::PI_PRIMITIVE, 6);
	cls::PA<int> nloops(cls::PI_PRIMITIVE, 6);
	int i=0;
	for(i=0; i<6; i++)
	{
		nverts[i] = 4;
		nloops[i] = 1;
	}

	cls::PA<Math::Vec3f> P(cls::PI_VERTEX, 8);
	for(i=0; i<8; i++)
	{
		P[i] = box.corner(i);
	}

	int mesh_verts[24] = {
		2, 3, 1, 0, 
		4, 5, 7, 6, 

		2, 3, 7, 6, 
		4, 5, 1, 0, 

		0, 2, 6, 4, 
		1, 3, 7, 5, 

//		6, 7, 5, 4, 
//		0, 1, 7, 6, 
//		3, 5, 7, 1, 
//		4, 2, 0, 6
	};

	cls::PA<int> verts(cls::PI_PRIMITIVEVERTEX, 24);
	for(i=0; i<24; i++)
		verts[i] = mesh_verts[i];

	render->PushAttributes();
		render->Parameter("P", P);
		render->Parameter("mesh::interpolation", "linear");
		render->Parameter("mesh::loops", nloops);
		render->Parameter("mesh::nverts", nverts);
		render->Parameter("mesh::verts", verts);
		render->SystemCall(cls::SC_MESH);
	render->PopAttributes();
}

void FFRenderProc::TestRender(
	cls::IRender* render,
	GridData& gridData,
	std::vector<MathOld::IGridDeformer*>& deforms,
	std::vector<MathOld::IFFWindow*>& ffds, 
	Math::Matrix4f& worldm, 
	float renderWidth
	)
{
	GridData* pThis = &gridData;

	pThis->grid.timestamp = 0;
	pThis->grid.setUpDeformers(worldm, &deforms[0], (int)deforms.size());

	printf( "alives %d\n", pThis->alives.size());
	std::map<GridData::Edge, GridData::EdgeData>::iterator it = pThis->alives.begin();
	for(int i=0; it != pThis->alives.end(); it++, i++)
	{
		const GridData::Edge& edge = it->first;
		const GridData::EdgeData& edgedata = it->second;

		Math::Vec3f v1 = pThis->grid.getCellPos( pThis->grid.addCell(edge.v1()));
		Math::Vec3f v2 = pThis->grid.getCellPos( pThis->grid.addCell(edge.v2()));

		float srcw = pow( 2.f, -edge.level());
		srcw = srcw*(float)renderWidth;
		float w = 0;
		for(int f=0; f<(int)ffds.size(); f++)
		{
 			float pointonedge = 0.5f;
			float cw = ffds[f]->processEdge(worldm, *pThis, edgedata.iffdata, pointonedge, srcw);
			if( cw<=0) continue;

			w = __max(w, cw);
		}

		if(w<0.01f)
		{
			continue;
		}

		/*/
		cls::PA<Math::Vec3f> P;
		cls::PA<int> verts;

		std::vector<MathOld::GridCellId> cells;
		GridData::subdivEdge( pThis->grid, edge, cells);
		P.reserve(cells.size());
		verts.reserve(1);
		verts.push_back( cells.size());
		for(int i=0; i<(int)cells.size(); i++)
		{
			MathOld::GridCellId id = cells[i];
			MathOld::GridCell* cell = pThis->grid.getCell(id);
			if( !cell) break;
			Math::Vec3f pos = pThis->grid.getCellPos(*cell);

			pos = Math::Vec3f((float)i);
			P.push_back(pos);
		}
		/*/
		cls::PA<Math::Vec3f> P(cls::PI_VERTEX, 2);
		cls::PA<int> verts(cls::PI_PRIMITIVE, 1);
		verts[0] = 2;
		P[0] = v1;
		P[1] = v2;
		printf("%f %f %f w=%f\n", v1.x, v1.y, v1.z, w);
		printf("%f %f %f w=%f\n", v2.x, v2.y, v2.z, w);
		w = 0.1f;
		render->PushAttributes();
		render->Parameter( "P", P);
		render->Parameter( "#width", w);
		render->Curves("linear", "nonperiodic", verts);
		render->PopAttributes();
	}

	pThis->grid.setUpDeformers(Math::Matrix4f(1), NULL, 0);

}

// ��������!
std::list<Util::DllProcedure> dllprocs;

bool FFRenderProc::BuildFF(cls::IRender* render)
{
	Math::Box3f box;
	render->GetParameter("ff::dirtybox", box);

	std::string filename;
	render->GetParameter("ff::filename", filename);

	Util::FileStream file;
	if( !file.open(filename.c_str(), false))
	{
		fprintf(stderr, "Cant load %s\n", filename.c_str());
		return false;
	}
	fprintf(stderr, "load %s\n", filename.c_str());

	GridData gridData;
	gridData.serialize(file);

	int rType;
	float renderWidth;
	int tesselation;
	Math::Matrix4f worldFFMatrix;
	render->GetParameter("ff::worldFFMatrix", worldFFMatrix);
//	render->GetParameter("worldtransform", worldFFMatrix);
	render->GetParameter("ff::rType", rType);
	render->GetParameter("ff::renderWidth", renderWidth);
	render->GetParameter("ff::tesselation", tesselation);
	RenderType renderType = (RenderType)rType;

	//if( renderType == SUBDIV )
	//{
	//	tesselation++;
	//}

	cls::PSA controlnames;
	render->GetParameter("ff::controlnames", controlnames);

	// ������ ��������
	std::vector<MathOld::IFFControl*> controls;
	std::vector<MathOld::IGridDeformer*> deforms;
	std::vector<MathOld::IFFWindow*> window;
	controls.reserve(controlnames.size());
	deforms.reserve(controlnames.size());
	window.reserve(controlnames.size());
	for(int i=0; i<controlnames.size(); i++)
	{
		std::string name = controlnames[i];
		std::string dll;
		render->GetParameter( (name+"::entrypoint").c_str(), dll);

		dllprocs.push_back(Util::DllProcedure());
		Util::DllProcedure& dllproc = dllprocs.back();
		dllproc.LoadFormated(dll.c_str(), "furFractalMath.dll");
		if( !dllproc.isValid())
			continue;
		MathOld::IFFCONTROL_CREATOR pc = (MathOld::IFFCONTROL_CREATOR)dllproc.proc;
		MathOld::IFFControl* control = (*pc)();

		control->SerializeOcs(name.c_str(), render, false);

		if( control->isDeformer())
			deforms.push_back(control);
		if( control->isWindow())
			window.push_back(control);
	}
	bool bTest = (renderType == TEST);

	if(bTest)
	{
		BoxRender(render, box, 0.1f);
		printf("%.02f %.02f %.02f\n", box.min.x, box.min.y, box.min.z);
		printf("%.02f %.02f %.02f\n", box.max.x, box.max.y, box.max.z);
		TestRender(render, gridData, deforms, window, worldFFMatrix, renderWidth);
		return true;
	}

	printf("%.02f %.02f %.02f\n", box.min.x, box.min.y, box.min.z);
	printf("%.02f %.02f %.02f\n", box.max.x, box.max.y, box.max.z);

	const std::map< GridData::Edge, GridData::EdgeData>& alives = gridData.alives;
	const std::vector< std::vector<MathOld::GridCellId> >& curves = gridData.curves;
	const std::vector< std::pair< Math::Vec3f, Math::Vec3f > >& baseNormals = gridData.baseNormals;
	std::map< MathOld::GridCellId, std::list<MathOld::GridFreeEdge> >& vertexLinks = gridData.vertexLinks;
	MathOld::Grid& grid = gridData.grid;

	printf( "curves.size() = %d\n", curves.size() );
	//for( unsigned int i = 0; i < curves.size(); i++ )
	//{
	//	printf( "curves[%d].size() = %d\n", i, curves[i].size() );
	//}

   	grid.timestamp=0;
	grid.setUpDeformers( worldFFMatrix, &deforms[0], (int)deforms.size() );

	gridData.Render( worldFFMatrix, window, rType, renderWidth, tesselation, render, false);

	grid.setUpDeformers( Math::Matrix4f::id, NULL, 0 );

	printf( "export complete\n" );
	return true;
}

bool GridData::Render(
	Math::Matrix4f& worldFFMatrix, 
	std::vector<MathOld::IFFWindow*>& window,
	int renderType, 
	float renderWidth, 
	int tesselation, 
	cls::IRender* render, 
	bool bSingleGeometry
	)
{
	int pcount=0;
	std::list<cls::renderCacheImpl<> > caches;
	GridData& gridData = *this;
	//�������� � ������ ��� �����
	for( unsigned int i = 0; i < curves.size(); i++ )
	{
		const std::vector<MathOld::GridCellId>& curve = curves[i];
		std::pair< Math::Vec3f, Math::Vec3f > baseNormal;

		if( baseNormals.size() )
		{
			baseNormal = baseNormals[i];
		}
		else
		{
			Math::Vec3f x(1,0,0);
			Math::Vec3f y(0,1,0);
			baseNormal.first = x;
			baseNormal.second = y;
		}

		if( !bSingleGeometry)
		{
			render->PushAttributes();

			if( !this->RenderSingleCurve(
				curve,
				baseNormal,
				worldFFMatrix, 
				window,
				renderType, 
				renderWidth, 
				tesselation, 
				render
				))
			{
				render->PopAttributes();
				continue;
			}

			if( renderType == CURVE_LINEAR)
			{
				render->Parameter("#curves::interpolation", "linear");
				render->Parameter("#curves::wrap", "nonperiodic");
				render->SystemCall(cls::SC_CURVES);
			}
			else if(renderType == CURVE_CUBIC) 
			{
				render->Parameter("@basis", "b-spline");
				render->Parameter("#curves::interpolation", "cubic");
				render->Parameter("#curves::wrap", "nonperiodic");
				render->SystemCall(cls::SC_CURVES);
			}
			else if( renderType == SUBDIV )
			{
				render->Parameter( "#subdiv::interpolateBoundary", true );
				render->Parameter("mesh::interpolation", "catmull-clark");
				render->SystemCall(cls::SC_MESH);
			}
			else if( renderType == MESH )
			{
				render->Parameter("mesh::interpolation", "linear");
				render->SystemCall(cls::SC_MESH);
			}
			render->PopAttributes();
		}
		else
		{
			// ������������ ��������� � ����
			caches.push_back(cls::renderCacheImpl<>() );
			cls::renderCacheImpl<>& c = caches.back();
			if( !this->RenderSingleCurve(
				curve,
				baseNormal,
				worldFFMatrix, 
				window,
				renderType, 
				renderWidth, 
				tesselation, 
				&c
				))
			{
				caches.pop_back();
				continue;
			}
			// ����������� ������ ���������
			cls::PA<int> verts;
			c.GetParameter("mesh::verts", verts);
			for(int v=0; v<verts.size(); v++)
				verts[v] += pcount;

			pcount += c.GetParameter("P").size();
		}
	}

	if(bSingleGeometry)
	{
		// ������� ���������
		if( caches.empty())
			return false;
		std::vector< std::pair< std::string, cls::Param> > paramslist;
		caches.front().GetParameterList(paramslist);
		// ��������� ��� ��������� 
		std::vector< std::pair< std::string, cls::Param> >::iterator it = paramslist.begin();
		for( ; it != paramslist.end(); it++)
		{
			std::string name = it->first;
			cls::Param src = it->second;

			// ��������� ������
			int count = 0;
			std::list<cls::renderCacheImpl<> >::iterator itc = caches.begin();
			for( ; itc != caches.end(); itc++)
			{
				cls::renderCacheImpl<>& cache = *itc;
				cls::Param p = cache.GetParameter(name.c_str());
				count += p.size();
			}

			// ����������
			cls::Param dst(src->type, src->interp, count);
			itc = caches.begin();
			int copycount = 0;
			for( ; itc != caches.end(); itc++)
			{
				cls::renderCacheImpl<>& cache = *itc;
				cls::Param p = cache.GetParameter(name.c_str());

				int size = p.sizebytes();
				if(copycount+size > dst.sizebytes())
					continue;
				memcpy( dst.data()+copycount, p.data(), size);
				copycount+=size;
			}
			render->Parameter(name.c_str(), dst);
		}
		return true;
	}
	return true;
}
// ��������� ������� ��������� ������ � ocs (��� ������� render->System(...) )
// ���������� � RenderProc
bool GridData::RenderSingleCurve(
	const std::vector<MathOld::GridCellId>& curve,
	const std::pair< Math::Vec3f, Math::Vec3f >& curveBaseNormals,
	Math::Matrix4f& worldFFMatrix, 
	std::vector<MathOld::IFFWindow*>& window,
	int renderType, 
	float renderWidth, 
	int tesselation, 
	cls::IRender* render
	)
{
	GridData& gridData = *this;

	unsigned int numSegments = (int)curve.size() - 1;

	cls::PA<int> loops( cls::PI_PRIMITIVE );
	cls::PA<int> nverts( cls::PI_PRIMITIVE );
	cls::PA<int> iverts( cls::PI_PRIMITIVEVERTEX );
	cls::PA<Math::Vec3f> P( cls::PI_VERTEX, 0, cls::PT_POINT );
	cls::PA<Math::Vec3f> N( cls::PI_VERTEX, 0, cls::PT_NORMAL );
	cls::PA<float> s( cls::PI_PRIMITIVEVERTEX );
	cls::PA<float> t( cls::PI_PRIMITIVEVERTEX );
	cls::PA<float> tex_x( cls::PI_PRIMITIVEVERTEX );
	cls::PA<float> tex_y( cls::PI_PRIMITIVEVERTEX );
	cls::PA<float> tex_z( cls::PI_PRIMITIVEVERTEX );
	cls::PA<float> v( cls::PI_PRIMITIVEVERTEX );
	cls::PA<float> v_length( cls::PI_PRIMITIVEVERTEX );
	cls::PA<float> shaderId( cls::PI_PRIMITIVEVERTEX );

	P.reserve( 4*numSegments + 4 );
	N.reserve( 4*numSegments + 4 );
	nverts.reserve( 4*numSegments );
	iverts.reserve( 4*4*numSegments );
	s.reserve( 4*4*numSegments );
	t.reserve( 4*4*numSegments );
	tex_x.reserve( 4*4*numSegments );
	tex_y.reserve( 4*4*numSegments );
	tex_z.reserve( 4*4*numSegments );
	v.reserve( 4*4*numSegments );
	v_length.reserve( 4*4*numSegments );
	shaderId.reserve( 4*4*numSegments );

	loops.reserve( 4*numSegments );

	std::vector< Math::Vec3f > linePoints;
	std::vector< Math::Vec3f > BiSplinePoints;
	std::vector< float > lineWidths;
	std::vector< Math::Vec3f > lineXYZCoord;
	std::vector< float > lineTexCoord;
	std::vector< float > lengthTexCoord;
	std::vector< float > shId;
	std::vector< float > lineTCoord;

	linePoints.reserve( numSegments + 1 );
	BiSplinePoints.reserve( 4 );
	lineWidths.reserve( numSegments + 1 );
	lineXYZCoord.reserve( numSegments + 1 );
	lineTexCoord.reserve( numSegments + 1 );
	lengthTexCoord.reserve( numSegments + 1 );
	shId.reserve( numSegments + 1 );
	lineTCoord.reserve( numSegments + 1 );
	lineTCoord.push_back(0);

	// �������� ��� ������� ������� + ������ � �.�.
	float widthSum = 0;
	for( unsigned int k = 0; k < curve.size()-1; k++ )
	{
		MathOld::GridCellId cellId = curve[k];

        Math::Vec3f pos = grid.getCellPos( *grid.getCell( cellId ) );
		linePoints.push_back( pos );

		MathOld::GridFreeEdge edge( curve[k], curve[k+1] );
		float srcw = pow( 2.f, -cellId.level);
		float width = computeWidth( window, worldFFMatrix, gridData, alives.find( edge )->second.iffdata, 0, srcw );
		widthSum += width;
		lineWidths.push_back( width );

		lineXYZCoord.push_back( Math::Vec3f( (float)cellId.x, (float)cellId.y, (float)cellId.z ) );

		float texCoord = computeTexCoord( window, worldFFMatrix, gridData, alives.find( edge )->second.iffdata, 0, 0 );
		lineTexCoord.push_back( texCoord );

		texCoord = computeTexCoord( window, worldFFMatrix, gridData, alives.find( edge )->second.iffdata, 0, 1 );
		lengthTexCoord.push_back( texCoord );

		float shaderid = computeTexCoord( window, worldFFMatrix, gridData, alives.find( edge )->second.iffdata, 0, 2 );
		shId.push_back( shaderid );

		if( k > 0 )
		{
			lineTCoord.push_back( lineTCoord[k-1] + 1/pow( 2.f, edge.level() ) );
		}
	}

	// +��������� �����
	MathOld::GridCellId cellId = curve[curve.size()-1];
	Math::Vec3f pos = grid.getCellPos( *grid.getCell( cellId ) );
	linePoints.push_back( pos );

	MathOld::GridFreeEdge edge( curve[curve.size()-2], curve[curve.size()-1] );
	float srcw = pow( 2.f, -edge.level());
	float width = computeWidth( window, worldFFMatrix, gridData, alives.find( edge )->second.iffdata, 1, srcw );
	widthSum += width;
	lineWidths.push_back( width );

	lineXYZCoord.push_back( Math::Vec3f( (float)cellId.x, (float)cellId.y, (float)cellId.z ) );

	float texCoord = computeTexCoord( window, worldFFMatrix, gridData, alives.find( edge )->second.iffdata, 1, 0 );
	lineTexCoord.push_back( texCoord );

	texCoord = computeTexCoord( window, worldFFMatrix, gridData, alives.find( edge )->second.iffdata, 1, 1 );
	lengthTexCoord.push_back( texCoord );

	float shaderid = computeTexCoord( window, worldFFMatrix, gridData, alives.find( edge )->second.iffdata, 1, 2 );
	shId.push_back( shaderid );

	lineTCoord.push_back( lineTCoord[ lineTCoord.size()-1 ] + 1/(float)pow( 2, edge.level() ) );
	
	// �� ������ � ������ ���� 0 �������
	if( widthSum <= 0 )
	{
		return false;
	}

	// ����� ����� ��� ������������ ������
	Math::Vec3f startPoint1;
	Math::Vec3f startPoint2;
	Math::Vec3f endPoint1;
	Math::Vec3f endPoint2;

	get2MainPointsPos( curve[1], curve[0], window, grid, gridData, vertexLinks, startPoint1, startPoint2 );
	get2MainPointsPos( curve[curve.size()-2], curve[curve.size()-1], window, grid, gridData, vertexLinks, endPoint1, endPoint2 );

	// ����� ������ �� ������
	float startPointWidth;
	float endPointWidth;

	if( getPointWidth( curve[1], curve[0], window, gridData, alives, vertexLinks, worldFFMatrix, startPointWidth ) )
	{
		lineWidths[0] = startPointWidth;
	}

	if( getPointWidth( curve[curve.size()-2], curve[curve.size()-1], window, gridData, alives, vertexLinks, worldFFMatrix, endPointWidth ) )
	{
		lineWidths[ lineWidths.size()-1 ] = endPointWidth;
	}

	// ������� ����� � 0-� ������� � ����� ������
	{
		int frontZero = -1;
		for( int k=0; k<(int)lineWidths.size(); k++)
		{
			if(lineWidths[k]<=0)
				frontZero = k;
			else
				break;
		}
		if(frontZero>0)
		{
			for( k=0; k<frontZero; k++)
				linePoints[k] = linePoints[frontZero];
		}
		int backZero = -1;
		for( int k=0; k<(int)lineWidths.size(); k++)
		{
			if(lineWidths[lineWidths.size()-k-1]<=0)
				backZero = k;
			else
				break;
		}
		if(backZero>0)
		{
			for(k=0; k<backZero; k++)
				linePoints[lineWidths.size()-k-1] = linePoints[lineWidths.size()-backZero-1];
		}
	}

	// ����� ��� ���������� curve_cubic � ��������
	BiSplinePoints.push_back( startPoint1 );
	BiSplinePoints.push_back( startPoint2 );
	BiSplinePoints.push_back( endPoint1 );
	BiSplinePoints.push_back( endPoint2 );

	if( tesselation > 1 )
	{
		std::vector< Math::Vec3f > smoothLinePoints;
		std::vector< float > smoothLineWidths;
		std::vector< Math::Vec3f > smoothLineXYZCoord;
		std::vector< float > smoothLineTexCoord;
		std::vector< float > smoothLengthTexCoord;
		std::vector< float > smoothShId;
		std::vector< float > smoothLineTCoord;

		int pointsCount = (int)(linePoints.size()-1)*tesselation + 1;
		smoothLinePoints.reserve( pointsCount );
		smoothLineWidths.reserve( pointsCount );
		smoothLineXYZCoord.reserve( pointsCount );
		smoothLineTexCoord.reserve( pointsCount );
		smoothLengthTexCoord.reserve( pointsCount );
		smoothShId.reserve( pointsCount );
		smoothLineTCoord.reserve( pointsCount );

		std::vector< Math::Vec3f > data;
		std::vector< float > knots;

		data.reserve( linePoints.size() + 2 );
		knots.reserve( linePoints.size() + 2 );

		knots.push_back( -1 );
		data.push_back( startPoint1 );
		for( unsigned int k = 0; k < linePoints.size(); k++ )
		{
			knots.push_back( (float)k );
			data.push_back( linePoints[k] );
		}
		knots.push_back( (float)linePoints.size() );
		data.push_back( endPoint1 );

		// ������� ������������� �����
		for( unsigned int k = 0; k < linePoints.size()-1; k++ )
		{
			for( int l = 0; l < tesselation; l++ )
			{
				Math::Vec3f smoothPos = Math::Spline3( (float)k + ( 1.f/tesselation )*l, &data[0], &knots[0], (int)data.size(), Math::Spline3f::BSpline );
				smoothLinePoints.push_back( smoothPos );

				smoothLineWidths.push_back( lineWidths[k] + ( (lineWidths[k+1]-lineWidths[k])/tesselation )*l );

				smoothLineXYZCoord.push_back( lineXYZCoord[k] + ( (lineXYZCoord[k+1]-lineXYZCoord[k])/tesselation )*l );

				smoothLineTexCoord.push_back( lineTexCoord[k] + ( (lineTexCoord[k+1]-lineTexCoord[k])/tesselation )*l );

				smoothLengthTexCoord.push_back( lengthTexCoord[k] + ( (lengthTexCoord[k+1]-lengthTexCoord[k])/tesselation )*l );

				smoothShId.push_back( shId[k] + ( (shId[k+1]-shId[k])/tesselation )*l );

				smoothLineTCoord.push_back( lineTCoord[k] + ( (lineTCoord[k+1]-lineTCoord[k])/tesselation )*l );
			}
		}
		// ��������� ��������� ����� + ������ � �.�.
		smoothLinePoints.push_back( Math::Spline3( (float)(linePoints.size()-1), &data[0], &knots[0], (int)data.size(), Math::Spline3f::BSpline ) );

		smoothLineWidths.push_back( lineWidths[ lineWidths.size()-1 ] );

		smoothLineXYZCoord.push_back( lineXYZCoord[ lineXYZCoord.size()-1 ] );

		smoothLineTexCoord.push_back( lineTexCoord[ lineTexCoord.size()-1 ] );

		smoothLengthTexCoord.push_back( lengthTexCoord[ lengthTexCoord.size()-1 ] );

		smoothShId.push_back( shId[ shId.size()-1 ] );

		smoothLineTCoord.push_back( lineTCoord[ lineTCoord.size()-1 ] );

		// ����� ����� ��� ���������� curve_cubic � ��������
		{
			std::vector< Math::Vec3f > data;
			std::vector< float > knots;

			data.reserve( 4 );
			knots.reserve( 4 );

			knots.push_back( -1 ); data.push_back( linePoints[ linePoints.size()-2 ] );
			knots.push_back( 0 ); data.push_back( linePoints[ linePoints.size()-1 ] );
			knots.push_back( 1 ); data.push_back( endPoint1 );
			knots.push_back( 2 ); data.push_back( endPoint2 );

			BiSplinePoints[2] = Math::Spline3( 1.f/tesselation, &data[0], &knots[0], (int)data.size(), Math::Spline3f::BSpline );

			data[0] = linePoints[1];
			data[1] = linePoints[0];
			data[2] = startPoint1;
			data[3] = startPoint2;

			BiSplinePoints[0] = Math::Spline3( 1.f/tesselation, &data[0], &knots[0], (int)data.size(), Math::Spline3f::BSpline );
		}

		linePoints = smoothLinePoints;
		lineWidths = smoothLineWidths;
		lineXYZCoord = smoothLineXYZCoord;
		lineTexCoord = smoothLineTexCoord;
		lengthTexCoord = smoothLengthTexCoord;
		shId = smoothShId;
		lineTCoord = smoothLineTCoord;

		P.reserve( 4*( numSegments*tesselation + 1 ) );
		N.reserve( 4*( numSegments*tesselation + 1 ) );
		nverts.reserve( 4*numSegments*tesselation );
		iverts.reserve( 4*4*numSegments*tesselation );
		s.reserve( 4*4*numSegments*tesselation );
		t.reserve( 4*4*numSegments*tesselation );
		tex_x.reserve( 4*4*numSegments*tesselation );
		tex_y.reserve( 4*4*numSegments*tesselation );
		tex_z.reserve( 4*4*numSegments*tesselation );
		v.reserve( 4*4*numSegments*tesselation );
		v_length.reserve( 4*4*numSegments*tesselation );
		shaderId.reserve( 4*4*numSegments*tesselation );

		loops.reserve( 4*numSegments*tesselation );
	}

	if( renderType == MESH || renderType == SUBDIV )
	{
		// ���������� �������
		std::vector< std::pair< Math::Vec3f, Math::Vec3f > > lineNormals;
		lineNormals.reserve( linePoints.size() );

		lineNormals.push_back( computeNormals3pOrigin( BiSplinePoints[0], linePoints[0], linePoints[1], curveBaseNormals ) );
		//lineNormals.push_back( computeNormals3p( BiSplinePoints[0], linePoints[0], linePoints[1] ) );

		for( unsigned int k = 1; k < linePoints.size()-1; k++ )
		{
			if( lineNormals[ lineNormals.size()-1 ].first.length() == 0 )
			{
				lineNormals.push_back( computeNormals3pOrigin( linePoints[k-1], linePoints[k], linePoints[k+1], curveBaseNormals ) );
				//lineNormals.push_back( computeNormals3p( linePoints[k-1], linePoints[k], linePoints[k+1] ) );
			}
			else
			{
				lineNormals.push_back( computeNormals3pN( linePoints[k-1], linePoints[k], linePoints[k+1], lineNormals[ lineNormals.size()-1 ] ) );
			}
		}

		if( lineNormals[ lineNormals.size()-1 ].first.length() == 0 )
		{
			lineNormals.push_back( computeNormals3pOrigin( linePoints[linePoints.size()-2], linePoints[linePoints.size()-1], BiSplinePoints[2], curveBaseNormals ) );
			//lineNormals.push_back( computeNormals3p( linePoints[linePoints.size()-2], linePoints[linePoints.size()-1], BiSplinePoints[2] ) );
		}
		else
		{
			lineNormals.push_back( computeNormals3pN( linePoints[linePoints.size()-2], linePoints[linePoints.size()-1], BiSplinePoints[2], lineNormals[ lineNormals.size()-1 ] ) );
		}
		//lineNormals.push_back( computeNormals3pN( linePoints[linePoints.size()-2], linePoints[linePoints.size()-1], BiSplinePoints[2], lineNormals[ lineNormals.size()-1 ] ) );

		//// ���� ����� ������� �� ������ ��������
		//if( linePoints.size() == 2 )
		//{
		//	lineNormals.push_back( computeNormals2p( linePoints[0], linePoints[1] ) );
		//	lineNormals.push_back( computeNormals2p( linePoints[0], linePoints[1] ) );
		//}
		//// ������� ������� �� 3 ������ (����������)
		//else if( linePoints.size() > 2 )
		//{
		//	// ������ �������
		//	lineNormals.push_back( computeNormals3p( linePoints[0], linePoints[1], linePoints[2] ) );
		//	//std::pair<Math::Vec3f,Math::Vec3f> n = computeNormals3p( linePoints[0], linePoints[1], linePoints[2] );
		//	//lineNormals.push_back( computeNormals3pN( startPoint, linePoints[0], linePoints[1], n ) );

		//	for( unsigned int k = 1; k < linePoints.size()-1; k++ )
		//	{
		//		lineNormals.push_back( computeNormals3pN( linePoints[k-1], linePoints[k], linePoints[k+1], lineNormals[ lineNormals.size()-1 ] ) );
		//	}

		//	// ��������� �������
		//	lineNormals.push_back( computeNormalsN2p( lineNormals[lineNormals.size()-1].first, linePoints[linePoints.size()-2], linePoints[linePoints.size()-1] ) );
		//	//lineNormals.push_back( computeNormals3pN( linePoints[linePoints.size()-2], linePoints[lineNormals.size()-1], endPoint, lineNormals[ lineNormals.size()-1 ] ) );
		//}

		for( int k = 0; k < loops.reserve_size(); k++ )
		{
			loops.push_back( 1 );
		}

		// ������ � ������ ����� � �������
		for( unsigned int k = 0; k < linePoints.size(); k++ )
		{
			Math::Vec3f pos = linePoints[k];

			Math::Vec3f normal1 = lineNormals[k].first;
			Math::Vec3f normal2 = lineNormals[k].second;

			float w = lineWidths[k] * renderWidth;

			P.push_back( pos + normal1*w );
			P.push_back( pos + normal2*w );
			P.push_back( pos - normal1*w );
			P.push_back( pos - normal2*w );

			N.push_back( normal1.normalized() );
			N.push_back( normal2.normalized() );
			N.push_back( -normal1.normalized() );
			N.push_back( -normal2.normalized() );
		}

		// ������ � ������ ������, ���������� ���������� + ���. ���������
		for( unsigned int k = 0; k < linePoints.size()-1; k++ )
		{
			for( int f = 0; f < 4; f++ )
			{
				nverts.push_back( 4 );

				if( f != 3 )
				{
					iverts.push_back( f + 0 + k*4 );
					iverts.push_back( f + 1 + k*4 );
					iverts.push_back( f + 5 + k*4 );
					iverts.push_back( f + 4 + k*4 );
				}
				else
				{
					iverts.push_back( f + 0 + k*4 );
					iverts.push_back( f + 1 - 4 + k*4 );
					iverts.push_back( f + 5 - 4 + k*4 );
					iverts.push_back( f + 4 + k*4 );
				}

				float s0 = f / (float)4;
				float s1 = s0 + 0.25f;

				s.push_back( s0 );
				s.push_back( s1 );
				s.push_back( s1 );
				s.push_back( s0 );

				//float t0 = (float)k / tesselation;
				//float t1 = (float)( k + 1 ) / tesselation;
				float t0 = lineTCoord[k];
				float t1 = lineTCoord[k+1];

				t.push_back( t0 );
				t.push_back( t0 );
				t.push_back( t1 );
				t.push_back( t1 );

				tex_x.push_back( lineXYZCoord[k].x );
				tex_x.push_back( lineXYZCoord[k].x );
				tex_x.push_back( lineXYZCoord[k+1].x );
				tex_x.push_back( lineXYZCoord[k+1].x );

				tex_y.push_back( lineXYZCoord[k].y );
				tex_y.push_back( lineXYZCoord[k].y );
				tex_y.push_back( lineXYZCoord[k+1].y );
				tex_y.push_back( lineXYZCoord[k+1].y );

				tex_z.push_back( lineXYZCoord[k].z );
				tex_z.push_back( lineXYZCoord[k].z );
				tex_z.push_back( lineXYZCoord[k+1].z );
				tex_z.push_back( lineXYZCoord[k+1].z );

				v.push_back( lineTexCoord[k] );
				v.push_back( lineTexCoord[k] );
				v.push_back( lineTexCoord[k+1] );
				v.push_back( lineTexCoord[k+1] );

				v_length.push_back( lengthTexCoord[k] );
				v_length.push_back( lengthTexCoord[k] );
				v_length.push_back( lengthTexCoord[k+1] );
				v_length.push_back( lengthTexCoord[k+1] );

				shaderId.push_back( shId[k]);
				shaderId.push_back( shId[k]);
				shaderId.push_back( shId[k+1]);
				shaderId.push_back( shId[k+1]);
			}
		}
	}

	if( renderType == CURVE_LINEAR || renderType == CURVE_CUBIC )
	{
		cls::PA<int> nvert( cls::PI_PRIMITIVE, 1 ); 
		cls::PA<Math::Vec3f> CVs( cls::PI_VERTEX, 0, cls::PT_POINT );
		cls::PA<float> W( cls::PI_VERTEX );
		cls::PA<float> U( cls::PI_VERTEX );
		cls::PA<float> V( cls::PI_VERTEX );
		cls::PA<float> U_Length( cls::PI_VERTEX );
		cls::PA<float> V_Length( cls::PI_VERTEX );
		cls::PA<float> S( cls::PI_VERTEX );
		cls::PA<float> T( cls::PI_VERTEX );

		CVs.reserve( (int)linePoints.size() );
		for( unsigned int k = 0; k < linePoints.size(); k++ )
		{
			CVs.push_back( linePoints[k] );
		}

		W.reserve( (int)lineWidths.size() );
		for( unsigned int k = 0; k < lineWidths.size(); k++ )
		{
			W.push_back( lineWidths[k] * renderWidth );
		}

		computeCurveParams( renderType, lineTexCoord, U, V );
		computeCurveParams( renderType, lengthTexCoord, U_Length, V_Length );
		computeCurveParams( renderType, lineTCoord, S, T );

		render->Parameter( "#width", W );
		render->Parameter( "u_toper", cls::PT_FLOAT, U );
		render->Parameter( "v_toper", cls::PT_FLOAT, V );
		render->Parameter( "u_length", cls::PT_FLOAT, U_Length );
		render->Parameter( "v_length", cls::PT_FLOAT, V_Length );
//		render->Parameter( "shaderId", cls::PT_FLOAT, shaderId );
		render->Parameter( "s", cls::PT_FLOAT, S );
		render->Parameter( "t", cls::PT_FLOAT, T );

		if( renderType == CURVE_LINEAR )
		{
			//U.reserve( (int)lineTexCoord.size() );
			//V.reserve( (int)lineTexCoord.size() );
			//for( unsigned int k = 0; k < lineTexCoord.size(); k++ )
			//{
			//	U.push_back( 0 );
			//	V.push_back( lineTexCoord[k] );
			//}

			//U_Length.reserve( (int)lengthTexCoord.size() );
			//V_Length.reserve( (int)lengthTexCoord.size() );
			//for( unsigned int k = 0; k < lengthTexCoord.size(); k++ )
			//{
			//	U_Length.push_back( 0 );
			//	V_Length.push_back( lengthTexCoord[k] );
			//}

			//S.reserve( (int)lineTCoord.size() );
			//T.reserve( (int)lineTCoord.size() );
			//for( unsigned int k = 0; k < lineTCoord.size(); k++ )
			//{
			//	S.push_back( 0 );
			//	T.push_back( lineTCoord[k] );
			//}

			nvert[0] = CVs.size();
			//render->Parameter( "#width", W );
			//render->Parameter( "u_toper", cls::PT_FLOAT, U );
			//render->Parameter( "v_toper", cls::PT_FLOAT, V );
			//render->Parameter( "u_length", cls::PT_FLOAT, U_Length );
			//render->Parameter( "v_length", cls::PT_FLOAT, V_Length );
			//render->Parameter( "s", cls::PT_FLOAT, S );
			//render->Parameter( "t", cls::PT_FLOAT, T );
			render->Parameter( "P", cls::PT_POINT, CVs );
			render->Parameter("#curves::nverts", nvert);
//			render->Curves( "linear", "nonperiodic", nvert );
		}
		else //renderType == CURVE_CUBIC
		{
			//U.reserve( (int)lineTexCoord.size() + 2 );
			//V.reserve( (int)lineTexCoord.size() + 2 );
			//U.push_back( 0 );
			//V.push_back( lineTexCoord[0] );
			//for( unsigned int k = 0; k < lineTexCoord.size(); k++ )
			//{
			//	U.push_back( 0 );
			//	V.push_back( lineTexCoord[k] );
			//}
			//U.push_back( 0 );
			//V.push_back( lineTexCoord[lineTexCoord.size()-1] );

			//U_Length.reserve( (int)lengthTexCoord.size() + 2 );
			//V_Length.reserve( (int)lengthTexCoord.size() + 2 );
			//U_Length.push_back( 0 );
			//V_Length.push_back( lengthTexCoord[0] );
			//for( unsigned int k = 0; k < lengthTexCoord.size(); k++ )
			//{
			//	U_Length.push_back( 0 );
			//	V_Length.push_back( lengthTexCoord[k] );
			//}
			//U_Length.push_back( 0 );
			//V_Length.push_back( lengthTexCoord[lengthTexCoord.size()-1] );

			//S.reserve( (int)lineTCoord.size() + 2 );
			//T.reserve( (int)lineTCoord.size() + 2 );
			//S.push_back( 0 );
			//T.push_back( 0 );err?
			//for( unsigned int k = 0; k < lineTCoord.size(); k++ )
			//{
			//	S.push_back( 0 );
			//	T.push_back( lineTCoord[k] );
			//}
			//S.push_back( 0 );
			//T.push_back( lineTCoord[ lineTCoord.size()-1 ] );

			cls::PA<Math::Vec3f> newCVs( cls::PI_VERTEX, 0, cls::PT_POINT );
			newCVs.reserve( CVs.size() + 2 );

			newCVs.push_back( BiSplinePoints[0] );
			for( int k = 0; k < CVs.size(); k++ )
			{
				newCVs.push_back( CVs[k] );
			}
			newCVs.push_back( BiSplinePoints[2] );

			nvert[0] = newCVs.size();
			//render->Parameter( "#width", W );
			//render->Parameter( "u_toper", cls::PT_FLOAT, U );
			//render->Parameter( "v_toper", cls::PT_FLOAT, V );
			//render->Parameter( "u_length", cls::PT_FLOAT, U_Length );
			//render->Parameter( "v_length", cls::PT_FLOAT, V_Length );
			//render->Parameter( "s", cls::PT_FLOAT, S );
			//render->Parameter( "t", cls::PT_FLOAT, T );
			render->Parameter( "P", cls::PT_POINT, newCVs );
			render->Parameter("#curves::nverts", nvert);
//			render->Curves( "cubic", "nonperiodic", nvert );
		}
	}

	if( renderType == MESH || renderType == SUBDIV )
	{
		render->Parameter( "P", P );
		render->Parameter( "N", N );
		render->Parameter( "s", s );
		render->Parameter( "t", t );
		render->Parameter( "tex_x", tex_x );
		render->Parameter( "tex_y", tex_y );
		render->Parameter( "tex_z", tex_z );
		render->Parameter( "u_toper", s );
		render->Parameter( "v_toper", v );
		render->Parameter( "u_length", s );
		render->Parameter( "v_length", v_length );
		render->Parameter( "shaderId", shaderId );
		render->Parameter("mesh::loops", loops);
		render->Parameter("mesh::nverts", nverts);
		render->Parameter("mesh::verts", iverts);
	}

	if( renderType == SUBDIV )
	{
//		render->Parameter( "#subdiv::interpolateBoundary", true );
//		render->Mesh( "catmull-clark", loops, nverts, iverts );
	}

	if( renderType == MESH )
	{
//		render->Mesh( "linear", loops, nverts, iverts );
	}
	return true;
}

// ����� ����� ������� ���� edge'��
MathOld::GridCellId commonVertex2( const MathOld::GridEdge& edge1, const MathOld::GridEdge& edge2 )
{
	if( edge1.v1() == edge2.v1() ) return edge1.v1();
	if( edge1.v1() == edge2.v2() ) return edge1.v1();
	if( edge1.v2() == edge2.v1() ) return edge1.v2();
	if( edge1.v2() == edge2.v2() ) return edge1.v2();

	return MathOld::GridCellId();
}

std::pair< Math::Vec3f, Math::Vec3f > computeNormals2p( Math::Vec3f& p1, Math::Vec3f& p2 )
{
	std::pair< Math::Vec3f, Math::Vec3f > normals;

	Math::Vec3f tangent = ( p2 - p1 ).normalized();

	Math::Vec3f upVector = Math::Vec3f( 0, 1, 0 );

	Math::Vec3f normal1 = Math::cross( tangent, upVector ).normalized();
	Math::Vec3f normal2 = Math::cross( tangent, normal1 ).normalized();

	normals.first = normal2;
	normals.second = normal1;

	return normals;
}

std::pair< Math::Vec3f, Math::Vec3f > computeNormals3p( Math::Vec3f& p1, Math::Vec3f& p2, Math::Vec3f& p3 )
{
	std::pair< Math::Vec3f, Math::Vec3f > normals;

	Math::Vec3f tangent1 = ( p2 - p1 ).normalized();
	Math::Vec3f tangent2 = ( p3 - p2 ).normalized();
	Math::Vec3f tangent = ( ( tangent1 + tangent2 ) / 2 ).normalized();

	Math::Vec3f uVector = Math::cross( tangent2, tangent1 ).normalized();

	if( uVector == Math::Vec3f(0) )
	{
		normals = computeNormals2p( p1, p2 );
	}
	else
	{
		normals.first = uVector;
		normals.second = Math::cross( uVector, tangent ).normalized();
	}

	return normals;
}

std::pair< Math::Vec3f, Math::Vec3f > computeNormals3pOrigin( Math::Vec3f& p1, Math::Vec3f& p2, Math::Vec3f& p3,
															 const std::pair< Math::Vec3f, Math::Vec3f >& originNormals )
{
	std::pair< Math::Vec3f, Math::Vec3f > normals;

	Math::Vec3f tangent1 = ( p2 - p1 ).normalized();
	Math::Vec3f tangent2 = ( p3 - p2 ).normalized();
	Math::Vec3f tangent = ( ( tangent1 + tangent2 ) / 2 ).normalized();

	if( tangent.length() == 0 )
	{
		normals.first = Math::Vec3f(0);
		normals.second = Math::Vec3f(0);

		return normals;
	}

	//std::pair< Math::Vec3f, Math::Vec3f > originNormals2;
	//Math::Vec3f x(1,0,0);
	//Math::Vec3f y(0,1,0);
	//originNormals2.first = x;
	//originNormals2.second = y;

	normals = computeNormals3pN( p1, p2, p3, originNormals );

	return normals;
}

std::pair< Math::Vec3f, Math::Vec3f > computeNormals3pN( Math::Vec3f& p1, Math::Vec3f& p2, Math::Vec3f& p3,
														const std::pair< Math::Vec3f, Math::Vec3f >& prevNormals )
{
	std::pair< Math::Vec3f, Math::Vec3f > normals;

	Math::Vec3f tangent1 = ( p2 - p1 ).normalized();
	Math::Vec3f tangent2 = ( p3 - p2 ).normalized();
	Math::Vec3f tangent = ( ( tangent1 + tangent2 ) / 2 ).normalized();

	Math::Vec3f uVector = Math::cross( tangent, prevNormals.second ).normalized();

	if( uVector == Math::Vec3f(0) )
	{
		uVector = Math::cross( prevNormals.second, prevNormals.first ).normalized();
	}

	Math::Vec3f normal = Math::cross( uVector, tangent ).normalized();

	normals.first = uVector;
	normals.second = normal;

	//Math::Vec3f uVector = Math::cross( tangent2, tangent1 ).normalized();

	//if( uVector == Math::Vec3f(0) )
	//{
	//	normals = prevNormals;
	//}
	//else
	//{
	//	Math::Vec3f normal = Math::cross( uVector, tangent ).normalized();

	//	if( ( Math::dot( uVector, prevNormals.first ) <= 0 ) || ( Math::dot( normal, prevNormals.second ) <= 0 ) )
	//	{
	//		normals.first = -uVector;
	//		normals.second = -normal;
	//	}
	//	else
	//	{
	//		normals.first = uVector;
	//		normals.second = normal;
	//	}
	//}

	return normals;
}

std::pair< Math::Vec3f, Math::Vec3f > computeNormalsN2p( const Math::Vec3f& normal, Math::Vec3f& p1, Math::Vec3f& p2 )
{
	std::pair< Math::Vec3f, Math::Vec3f > normals;

	Math::Vec3f tangent = ( p2 - p1 ).normalized();

	Math::Vec3f normal1 = Math::cross( tangent, normal ).normalized();
	Math::Vec3f normal2 = Math::cross( normal1, tangent ).normalized();

	normal2 = ( ( normal + normal2 ) / 2 ).normalized();
	normal1 = Math::cross( normal2, tangent ).normalized();

	normals.first = normal2;
	normals.second = normal1;

	return normals;
}

// ����� ������ � ����� edge'�
float computeWidth( std::vector<MathOld::IFFWindow*>& ffws, Math::Matrix4f& worldMatrix, GridData& gridData, const MathOld::IFFEdgeData& edge, float pointOnEdge, float srcw )
{
	float w = 0;
	for( int f = 0; f < (int)ffws.size(); f++ )
	{
		float cw = ffws[f]->processEdge( worldMatrix, gridData, edge, pointOnEdge, srcw );

		if( cw <= 0 )
		{
			continue;
		}
		else
		{
			w = __max( w, cw );
		}
	}

	return w;
}

bool getMainPointId(
	const MathOld::GridCellId& v1,
	const MathOld::GridCellId& v2,
	std::vector<MathOld::IFFWindow*>& window,
	GridData& gridData,
	std::map< MathOld::GridCellId, std::list<MathOld::GridFreeEdge> >& vertexLinks,
	MathOld::GridCellId& mainPointId
	)
{
	if( window.size() )
	{
        std::list<MathOld::GridCellId> v3List;
		MathOld::GridFreeEdge currentEdge( v1, v2 );

		std::list<MathOld::GridFreeEdge>& v2edges = vertexLinks[ v2 ];
		std::list<MathOld::GridFreeEdge>::iterator it = v2edges.begin();

		for( ; it != v2edges.end(); it++ )
		{
			if( *it != currentEdge )
			{
				if( it->v2() != v2 )
				{
					v3List.push_back( it->v2() );
				}
				else
				{
					v3List.push_back( it->v1() );
				}
			}
		}

		for( int i = 0; i < (int)window.size(); i++ )
		{
			if( window[i]->getMainPoint( v1, v2, v3List, gridData, mainPointId ) )
			{
				return true;
			}
		}
	}

	return false;
}

void get2MainPointsPos(
	const MathOld::GridCellId& v1,
	const MathOld::GridCellId& v2,
	std::vector<MathOld::IFFWindow*>& window,
	MathOld::Grid& grid,
	GridData& gridData,
	std::map< MathOld::GridCellId, std::list<MathOld::GridFreeEdge> >& vertexLinks,
	Math::Vec3f& point3,
	Math::Vec3f& point4
	)
{
	Math::Vec3f p1 = grid.getCellPos( *grid.getCell( v1 ) );
	Math::Vec3f p2 = grid.getCellPos( *grid.getCell( v2 ) );

	MathOld::GridCellId pointId3;
	MathOld::GridCellId pointId4;

	if( getMainPointId( v1, v2, window, gridData, vertexLinks, pointId3 ) )
	{
		point3 = grid.getCellPos( *grid.getCell( pointId3 ) );

		if( getMainPointId( v2, pointId3, window, gridData, vertexLinks, pointId4 ) )
		{
			point4 = grid.getCellPos( *grid.getCell( pointId4 ) );
		}
		else
		{
            Math::Vec3f dif = point3 - p2;
			point4 = point3 + dif;
		}
	}
	else
	{
		Math::Vec3f dif = p2 - p1;
		point3 = p2 + dif;
		point4 = point3 + dif;
	}
}

bool getPointWidth(
	const MathOld::GridCellId& v1,
	const MathOld::GridCellId& v2,
	std::vector<MathOld::IFFWindow*>& window,
	GridData& gridData,
	const std::map< GridData::Edge, GridData::EdgeData>& alives,
	std::map< MathOld::GridCellId, std::list<MathOld::GridFreeEdge> >& vertexLinks,
	Math::Matrix4f& worldFFMatrix,
	float& widthAvg
	)
{
	MathOld::GridCellId mainPointId;
	if( getMainPointId( v1, v2, window, gridData, vertexLinks, mainPointId ) )
	{
		MathOld::GridCellId selfPointId;
		if( getMainPointId( mainPointId, v2, window, gridData, vertexLinks, selfPointId ) )
		{
			if( selfPointId == v1 )
			{
				MathOld::GridFreeEdge currentEdge( v1, v2 );
				MathOld::GridFreeEdge nextEdge( mainPointId, v2 );

				float srcw = pow( 2.f, -currentEdge.level());
				widthAvg = computeWidth( window, worldFFMatrix, gridData, alives.find( currentEdge )->second.iffdata, 1, srcw );
				widthAvg += computeWidth( window, worldFFMatrix, gridData, alives.find( nextEdge )->second.iffdata, 1, srcw );
				widthAvg /= 2;

				return true;
			}
		}
	}

	return false;
}

float computeTexCoord(
	std::vector<MathOld::IFFWindow*>& ffds,
	Math::Matrix4f& ffworldmatrix,	// world ������� �������� 
	GridData& griddata,				// �������
	const MathOld::IFFEdgeData& edge,		// �������� �����
	float pointonedge,				// ����� �� ����� (�� 0 �� 1)
	int texCoordType				// �� ������� ���� ��������� UV ����� ���� ������
	)
{
	float v = 0;

	for( int f = 0; f < (int)ffds.size(); f++ )
	{
		float texc = 0;

		if( ffds[f]->getTexCoord( texc, ffworldmatrix, griddata, edge, pointonedge, texCoordType ) )
		{
			v = texc;
			break;
		}
	}

	return v;
}

void computeCurveParams(
	int renderType,
	const std::vector<float>& param,
	cls::PA<float>& U,
	cls::PA<float>& V
	)
{
	if( renderType == CURVE_LINEAR )
	{
		U.reserve( (int)param.size() );
		V.reserve( (int)param.size() );
		for( unsigned int k = 0; k < param.size(); k++ )
		{
			U.push_back( 0 );
			V.push_back( param[k] );
		}
	}
	else if( renderType == CURVE_CUBIC )
	{
		U.reserve( (int)param.size() + 2 );
		V.reserve( (int)param.size() + 2 );
		U.push_back( 0 );
		V.push_back( param[0] );
		for( unsigned int k = 0; k < param.size(); k++ )
		{
			U.push_back( 0 );
			V.push_back( param[k] );
		}
		U.push_back( 0 );
		V.push_back( param[param.size()-1] );
	}
}