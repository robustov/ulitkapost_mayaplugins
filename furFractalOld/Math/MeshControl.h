#pragma once

#include "Math/Math.h"
#include "IFFControl.h"
#include "furFractalMath.h"
#include "GridData.h"
#include "Math/Face.h"
#include "Math/perlinnoise.h"
#include "Math/matrixMxN.h"

#pragma warning (disable: 4251 4275)

class FURFRACTALMATH_API MeshControl : public MathOld::IFFControl
{
public:
	// �������� �����
	struct OpenEdge
	{
		int v_1;	// ����. �������� �����
		Math::Edge32 edge;
		int v2_;	// ����. �������� �����
		OpenEdge(){v_1=0;v2_=0;}
	};

	// ����
	bool bWindow;
	float windowparam;
	float time;						// ����� (��� ����)
	float venToper;
	float width;					// ������
	float WidthFactorJitter;		// jitter ������
	float disgrowfactor;

	// ����
	bool bGrow;
	int growlevel;					// ������� �� ������� ������
	int startSeed, seed;			// startSeed �������� seed �������� ��� Step
	int startVenCount;				// �����
	float segLenght, segLenJitter;	// ����� ��������
	float curl, curlJitter;			// �����������
	float startOffset, surfaceOffset, surfaceOffsetJitter;			// ��������
	float growFrequency;			// ������� �������� ��� �����
	float forkAngle, forkAngleJitter;	// ���� � �����
	float stopIfCross;				// ����������� ���������� ���� �����������
	std::vector<Math::Vec3f> startPositions;
	int maxSegCount;				// ������������ ����� ���������
	bool growFromOther;				// ���������� �� ������ ������
	int maxGeneration;				// ������������ ����� �������
	float growFromHairStartPhaseJitter;	// ������� �� ���� ��� ������ � �����
	Math::matrixMxN<float> growSpeed;	// �������� �������� �����
	int texResolution;				// ���������� �������
	float filletRadius;		// ������ ���������� ��� ����� � �����������


	// ���� sub
	float curl2, curl2jitter;
	float forkAngle2, forkAngle2jitter;
	float length2, length2jitter;
	float density2;
	float subgrowPhaseOffset;
	int maxGeneration2;				// ������������ ����� �������

	std::vector<Math::Vec3f> verts;			// verts
	std::vector<Math::Vec3f> vertnormals;	// N
	std::vector<Math::Vec3f> facenormals;	// FN
	std::vector<Math::Vec2f> vertsUv;		// UV
	std::vector<Math::Face32> faces;		// faces
	std::vector<Math::Face32> facesAtStart;	// faces �� ������ (��� ���������! ���� ������������ �� ��������)
	std::map< Math::Edge32, Math::Vec3f> edgenormals;	// ������� �����
	std::vector<OpenEdge> openedges;		// �������� �����

	// ����������
	bool bDeformer;
	float xynoisemagnitude, xynoisefrequency, xynoisedimention, xynoisemagnitudemin;
	float deformToper;

	// render
	float texToper;
	int shaderId;

public:
	// �������� ����� �� ����������� �����
	struct pointOnControlMesh
	{
		float distance;		// ����������
		int face;			// ������ ����� (-1 ��� �����)
		int v[3];			// ������� ���������
		float w[3];			// ��� ���������

		pointOnControlMesh()
		{
			distance = std::numeric_limits<float>::max();
			face = -1;
			v[0] = v[1] = v[2] = 0;
			w[0] = w[1] = w[2] = 0;
		}
		// �������� �� ��-�
		pointOnControlMesh(
			float distance, 
			int face, 
			Math::Face32 f, 
			Math::Vec3f bary
			)
		{
			this->distance = distance;
			this->face = face;
			v[0] = f[0]; v[1] = f[1]; v[2] = f[2];
			w[0] = bary[0]; w[1] = bary[1]; w[2] = bary[2];
		}
		// �������� �� �����
		pointOnControlMesh(
			float distance, 
			int v0, int v1,
			float w0, float w1
			)
		{
			this->distance = distance;
			this->face = -1;
			v[0] = v0; v[1] = v1; v[2] = v1;
			w[0] = w0; w[1] = w1; w[2] = 0;
		}
	};

	// �������������� � step
	struct GrowPoint
	{
		GrowPoint();
		MathOld::GridCellId id;	// ������� �������
		Math::Vec3f pt;			// ������� �����
		Math::Vec3f dir;		// ������� �����������
		float curl;				// �����������

		float phase;			// ���� ������������ �����
		float lenght;			// ������� �����
		int seed;				// seed
		bool bStop;
		MathOld::GridFreeEdge prevedge;	// ���������� �����
		int generation;			// ��������� �������

		// ��� ����������
		float filletRadius;		// ����������
		float filletParam;		// ������� ������ ���������� ����������
	};
	// ������� ����� �����
	std::list< GrowPoint> growpoints;	
	// ������������������ �����
	std::map<MathOld::GridFreeEdge, MathOld::GridFreeEdge> growparents;
	// ���������������� ����� � phase � lenght ����� ������� ������������� (���� �� ��������� ��� ����� �� ����)
	struct VertexData
	{
		float phase, lenght;
		VertexData(float phase=0, float lenght=0){this->phase = phase; this->lenght = lenght;}
	};
	std::map<MathOld::GridCellId, VertexData> modified_verts;

	// ������ ����� ����� �����������
	std::set<MathOld::GridFreeEdge> otherProcessed;

public:
	MeshControl(void);
	~MeshControl(void);

	// �������� ����� ��� ������� ������ � �����.���������
	float getGrowParam(
		const Math::Vec3f& v, 
		const MathOld::GridCellId& id, 
		float lenght, 
		float phase, 
		float toper
		);

	// ���������� �� �������� ����� �� ����������� 
	// ������ ����� �� �����������
	float getClosestPoint(
		MathOld::Grid& grid, 
		const Math::Vec3f& id, 
		pointOnControlMesh& ponm
		);
	// ����� �� �����������
	Math::Vec3f getPoint(
		const pointOnControlMesh& ponm
		);
	// ������� � �����
	Math::Vec3f getNormal(
		const pointOnControlMesh& ponm
		);
	// ����� ����������
	Math::Vec2f getUv(
		const pointOnControlMesh& ponm
		);
	// ����� ����������
	float getGrowSpeed(
		const pointOnControlMesh& ponm
		);

	// ����� �� �������� �����
	bool canAddEdge(
		MathOld::GridFreeEdge& edge, 
		float phase,
		float lenght,
		GridData& griddata, 
		std::list<MathOld::IFFEdgeData>& added
		);
	// ����� �� ������������� �������
	bool canDeformVertex(
		MathOld::GridCellId& id, 
		float phase
		);
	// ���������� ����?
	// ���� ���� ������ � ����� ������
	// ��� ���� ������ � ����� ������
	bool isStopVertex(
		MathOld::GridCellId& id, 
		float phase, 
		float lenght
		);

	// ��������� �������
	void GrowStartPoint(
		Math::Vec3f startPoint, 
		MathOld::GridCellId id,			// ��� ����� ���� = null
		float startPhase, 
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata, 
		int startVenCount,
		float filletRadius,			// ������ ����������
		std::list<MathOld::IFFEdgeData>& added, 
		bool bDump=false
		);
	// ��� ��������� gp
	bool GrowPointStep(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		int g, 
		GrowPoint& gp, 
		GridData& griddata, 
		std::list<MathOld::IFFEdgeData>& added
		);
	// ��������
	bool GrowPointFork(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		int g, 
		GrowPoint& gp, 
		GridData& griddata, 
		std::list< GrowPoint>& newgrowpoints
		);
	// ��������� subgrow
	void GrowPoint_NewSubgrow(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GrowPoint& gp, 
		const MathOld::GridCellId& previd, 
		GridData& griddata, 
		std::list< GrowPoint>& newgrowpoints
		);

	// ������������ �����������
	float genCurl();

	// ��������� lenghtFromEnd
	void CalcLenghtFromEnd(
		GridData& griddata
		);

	// ���
	float perlin(const Math::Vec3f& position, float time, float magnitude, float noisefrequency);
	static Math::PerlinNoise perlinnoise;

public:
	virtual bool isWindow()const{return bWindow;};
	virtual bool isGrow()const
	{
		return bGrow||growFromOther;
	};
	virtual bool isDeformer()const{return bDeformer;};

	virtual void copy(const IFFControl* arg);
	virtual void serialize(Util::Stream& stream);
	virtual void SerializeOcs(const char* _name, cls::IRender* render, bool bSave);
	virtual void ReadAttributes(IOcsData* node);

public:
	virtual float processEdge(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata,				// �������
		const MathOld::IFFEdgeData& edge, 
		float pointonedge,					// ����� �� ����� (�� 0 �� 1)
		float src_width
		);
	// ���������� �����.���������� ��� ����� �� �����
	virtual bool getTexCoord(
		float& texCoord, 
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata,					// �������
		const MathOld::IFFEdgeData& edge,			// �������� �����
		float pointonedge,					// ����� �� ����� (�� 0 �� 1)
		int texCoordType					// �� ������� ���� ��������� UV ����� ���� ������
		);
	//����� ��������� ����� ��� ������������ ����� �����
	virtual bool getMainPoint(
		const MathOld::GridCellId& v1,	//����������� ����� v1->v2
		const MathOld::GridCellId& v2,
		const std::list<MathOld::GridCellId>& v3list,	// ��������� ����� ���. ������� �� ������� v2
		GridData& griddata,
		MathOld::GridCellId& mainPoint //��������� �����, ���� ��������� true
		);
	// ��� ��������� �� ��������
	virtual void draw(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata
		);

public:
	// ��������� �������
	virtual void Start(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata, 
		std::list<MathOld::IFFEdgeData>& added
		);
	// ��� ���������
	virtual void Step(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata, 
		std::list<MathOld::IFFEdgeData>& added
		);
	// ����� ���������
	void GrowStop(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata, 
		std::list<MathOld::IFFEdgeData>& added
		);

public:
	virtual bool deform(
		const MathOld::Grid& grid, 
		const MathOld::GridCellId& id, 
		Math::Vec3f& v, 
		Math::Vec3f& offset,				// �������� �������� (������ ������������ ��� �����������)
		const MathOld::GridDeformerCell& deformercell,
		const Math::Matrix4f& worldMatrix);

	Math::Vec3f MeshControl::getNoise(
		float noisemagnitude, 
		float noisefrequency, 
		const MathOld::GridCellId& id, 
		int seed);
};

template <class STREAM>
STREAM& operator >> (STREAM& serializer, MeshControl::GrowPoint& v)
{
	unsigned char version = 2;
	serializer >> version;
	if( version>=0)
	{
		serializer >> v.id;	// ������� �������
		serializer >> v.pt;			// ������� �����
		serializer >> v.dir;		// ������� �����������
		serializer >> v.curl;				// �����������
		serializer >> v.lastDirection;
		serializer >> v.phase;			// ���� ������������ �����
		serializer >> v.lenght;			// ������� �����
		serializer >> v.seed;				// seed
	}
	if( version>=1)
	{
		serializer >> v.bStop;
		serializer >> v.prevedge;
	}
	if( version>=2)
	{
		// ��� ����������
		serializer >> v.filletRadius;		// ����������
		serializer >> v.filletParam;		// ������� ������ ���������� ����������
	}
	return serializer;
}

template <class STREAM>
STREAM& operator >> (STREAM& serializer, MeshControl::OpenEdge& v)
{
	unsigned char version = 0;
	serializer >> version;
	if( version>=0)
	{
		serializer >> v.v_1;
		serializer >> v.edge;
		serializer >> v.v2_;
	}
	return serializer;
}
template <class STREAM>
STREAM& operator >> (STREAM& serializer, MeshControl::VertexData& v)
{
	unsigned char version = 0;
	serializer >> version;
	if( version>=0)
	{
		serializer >> v.phase;
		serializer >> v.lenght;
	}
	return serializer;
}

