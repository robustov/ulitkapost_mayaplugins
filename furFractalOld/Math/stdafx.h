// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define DATACOPY(ARG) this->##ARG = arg->##ARG;
#define DATACOPY2(ARG1, ARG2) this->##ARG1 = arg->##ARG1, this->##ARG2 = arg->##ARG2;
#define DATACOPY3(ARG1, ARG2, ARG3) this->##ARG1 = arg->##ARG1, this->##ARG2 = arg->##ARG2, this->##ARG3 = arg->##ARG3;
#define COPYCONST(MEMBER) this->##MEMBER = ##MEMBER;
#define SERIALIZE(MEMBER) stream >> this->##MEMBER;


#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// TODO: reference additional headers your program requires here
#include <stdlib.h>
inline float rnd()
{
	return rand()/(float)RAND_MAX;
}
