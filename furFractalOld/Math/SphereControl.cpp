#include "StdAfx.h"
#include ".\spherecontrol.h"

#include <gl/gl.h>
#include "MathNGl/MathNGl.h"

extern "C"
{
	__declspec(dllexport) MathOld::IFFControl* __cdecl sphereControl()
	{
		return new SphereControl();
	}
}

//Math::PerlinNoise perlinnoise(1245);
SphereControl::GrowPoint::GrowPoint(float phase, Math::Vec3i direction, float length, int threadId, MathOld::GridFreeEdge parent)
{
	this->phase = phase;				// ������� ����
	this->direction = direction;		// �����������
	this->growCount = 0;				// ������� ������� �� ��� �� ������
	this->growCountLow = 0;			// ������� ������� �� ������� ����
	this->length = length;
	this->threadId = threadId;
	this->parent = parent;
}

SphereControl::SphereControl()
{
	width0 = 1;
	widthExp = 2;

	time = 0;
	xynoisemagnitude = 0;
	xynoisefrequency = 1;
	xynoisedimention = 1;
	maxLevel = 2;
	this->startLevel = 1;

	maxGrow = 2;					// ������� ��������� ���� �� ������?
	maxGrowLow = 3;					// ������� ��������� low ������?

	sameFluctuation = 1.5f;		// ��������� ������ ������������ ��� ��������� �����������
	samePhaseBiasConst = 0.2f;		// �������� �� ���� ��� ��������� �������
	samePhaseBiasRnd = 0.1f;		// ��������� �������� ��� ��������� �������
	samePhaseSecondaryFactor = 10;	// ��������� ��� ��������� ��������

//	maxDistance = 0.0001f;
//	maxPhase = 0;
	venToper = 0;
	this->seed = this->startSeed = 0;
	deformToper = 0;
	texToper = 0;
}

// �������� ������ � �������� ���������
void SphereControl::init(
	const Math::Matrix4f& worldpos
	)
{
	this->worldpos = worldpos;
	invworldpos = worldpos.inverted();
}


float SphereControl::processEdge(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata,				// �������
	const MathOld::IFFEdgeData& edgedata, 
	float pointonedge,					// ����� �� ����� (�� 0 �� 1)
	float src_width
	)
{
	if( windowSize<0.001f)
		return 0;

	const MathOld::GridFreeEdge& edge = edgedata.edge;
	MathOld::Grid& grid = griddata.grid;

	if( edgedata.growId != this->growid)
		return 0;

	Math::Vec3f v1 = grid.getCellPos(edge.v1());
	Math::Vec3f v2 = grid.getCellPos(edge.v2());
	Math::Vec3f v = v1*(1-pointonedge) + v2*pointonedge;

	float w = getWidth(v, edge.v1(), edgedata.lenght, edgedata.lenghtFromEnd, edgedata.phase);

	float f = pow(widthExp, edge.level());
	float levelfactor = width0*f;

	return w*src_width*levelfactor;
}
float SphereControl::getGrowParam(Math::Vec3f& v, const MathOld::GridCellId& id, float lenght, float phase, float toper)
{
	float w = windowSize - lenght;

//	Math::Vec3f p = this->invworldpos*v;
//	float len = p.length()/windowSize + phase*disgrowfactor;

	// w = 1-r^3
//	float w = (1-len*len*len);
	float wfactor = 1000000.f;
	if(toper>0)
		wfactor = 1/toper;

	w -= phase*this->disgrowfactor;
	w *= wfactor;
	return w;
}

float SphereControl::getWidth(Math::Vec3f& v, const MathOld::GridCellId& id, float lenght, float lenghtFromEnd, float phase)
{
	float w = getGrowParam(v, id, lenght, phase, this->venToper);
	float wfe = getGrowParam(v, id, windowSize - lenghtFromEnd, 0, this->venToper);
	if(wfe<w)
		w = wfe;
	w = float( atan(w)*2/M_PI);

//	if(w>1) w=1;
	return w;
}
// ���������� �����.���������� ��� ����� �� �����
bool SphereControl::getTexCoord(
	float& texCoord, 
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata,					// �������
	const MathOld::IFFEdgeData& edgedata,			// �������� �����
	float pointonedge,					// ����� �� ����� (�� 0 �� 1)
	int texCoordType					// �� ������� ���� ��������� UV ����� ���� ������
	)
{
	if( edgedata.growId != this->growid)
		return false;

	if( texCoordType==0)
	{
		const MathOld::GridFreeEdge& edge = edgedata.edge;
		MathOld::Grid& grid = griddata.grid;

		Math::Vec3f v1 = grid.getCellPos(edge.v1());
		Math::Vec3f v2 = grid.getCellPos(edge.v2());
		Math::Vec3f v = v1*(1-pointonedge) + v2*pointonedge;

		float w = getGrowParam(v, edge.v1(), edgedata.lenght, edgedata.phase, this->texToper);
		texCoord = w;
	}
	if( texCoordType==1)
	{
		const MathOld::GridFreeEdge& edge = edgedata.edge;
		MathOld::Grid& grid = griddata.grid;

		texCoord = edgedata.lenght;
	}

	return true;
}

bool SphereControl::getMainPoint(
	const MathOld::GridCellId& v1,	//����������� ����� v1->v2
	const MathOld::GridCellId& v2,
	const std::list<MathOld::GridCellId>& v3list,	// ��������� ����� ���. ������� �� ������� v2
	GridData& griddata,
	MathOld::GridCellId& mainPoint	//��������� �����, ���� ��������� true
	)
{
	MathOld::GridFreeEdge ed1(v1, v2);
	std::map< GridData::Edge, GridData::EdgeData>::iterator ital = griddata.alives.find(ed1);
	if( ital== griddata.alives.end())
		return false;
	GridData::EdgeData& edata1 = ital->second;

	if( edata1.iffdata.growId != this->growid)
		return false;

	bool bFind = false;
	float curphase = 1e32f;

	std::list<MathOld::GridCellId>::const_iterator it = v3list.begin();
	for(;it != v3list.end(); it++)
	{
		const MathOld::GridCellId& v3 = *it;
		if( v3 == v1) 
			continue;

		MathOld::GridFreeEdge ed2(v2, v3);
		ital = griddata.alives.find(ed2);
		if( ital == griddata.alives.end())
			continue;
		GridData::EdgeData& edata2 = ital->second;

		// ��������� 
		if(curphase>edata2.iffdata.phase)
//		if(	edata1.iffdata.growId   == edata2.iffdata.growId &&
//			edata1.iffdata.threadId == edata2.iffdata.threadId
//			)
		{
			mainPoint = v3;
			curphase = edata2.iffdata.phase;
			bFind = true;
		}
	}
	return bFind;
}

// ��� ��������� �� ��������
void SphereControl::draw(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata
	)
{
	if( fabs(windowSize-1)>0.01)
	{
		Math::Matrix4f ffworldmatrixinv = ffworldmatrix.inverted();
		glPushMatrix();
			const float* d;
			d = ffworldmatrixinv.data();
			glMultMatrixf(d);
			glPushMatrix();
				d = worldpos.data();
				glMultMatrixf(d);

				glLineStipple(1, 0x8888);
				glEnable(GL_LINE_STIPPLE);
				drawSphere(2*(float)windowSize);
				glDisable(GL_LINE_STIPPLE);
			glPopMatrix(); 
		glPopMatrix(); 
	}
}







// ��������� �������
void SphereControl::Start(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata, 
	std::list<MathOld::IFFEdgeData>& added
	)
{
	this->ReinitGrowId();
	srand(this->startSeed);
//	this->maxphase = 0;
	growpoints.clear();
//	maxDistance = 0.0001f;
//	maxPhase = 0;

	int level = this->startLevel;
	if(bStart)
	{
		Math::Matrix4f ffworldmatrixinv = ffworldmatrix.inverted();
		// ����� ������� � ������ ������ L
		Math::Vec3f v = worldpos*Math::Vec3f(0);
		v = ffworldmatrixinv*v;
		
		MathOld::GridCellId id1 = griddata.grid.findCube(v, level);

		MathOld::GridEdge ed(id1, MathOld::NEI_X_UP);
		added.push_back( MathOld::IFFEdgeData(ed, this->growid));

		growpoints[ed] = GrowPoint(0, MathOld::getNeigbour3dDirection((MathOld::enGridNeigbour3d)ed.nb), 0, 0);
	}
	this->seed = rand();
}
// ��������� ������ �� ����� � �����
bool SphereControl::isInside(
	const MathOld::GridFreeEdge& edge, 
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata
	)
{
	Math::Vec3f v1 = griddata.grid.getCellPos( griddata.grid.addCell(edge.v1()));
	v1 = ffworldmatrix*v1;
	Math::Vec3f v2 = griddata.grid.getCellPos( griddata.grid.addCell(edge.v2()));
	v2 = ffworldmatrix*v2;

	Math::Vec3f p1 = this->invworldpos*v1;
	Math::Vec3f p2 = this->invworldpos*v2;
	if( p1.length()>1 && 
		p2.length()>1
		) 
		return false;
	return true;
}
// �������� �����
bool SphereControl::addEdge(
	const MathOld::GridFreeEdge& edge, 
	const MathOld::GridFreeEdge& newedge, 
	const Math::Vec3i& direction, 
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata,					
	std::list<MathOld::IFFEdgeData>& added, 
	float phaseoffset, 
	int threadId
	)
{
	// ��������� ���� �� �����
	if( griddata.alives.find(newedge)!=griddata.alives.end())
		return false;

	// ������ �� � �����
	if( !isInside(newedge, ffworldmatrix, griddata))
		return false;

	// ��� ����� ������
	if( griddata.busies.find(newedge.v1())!=griddata.busies.end() &&
		griddata.busies.find(newedge.v2())!=griddata.busies.end()
		)
		return false;

	float maxwindowsize = __max( worldpos[0].length(), __max( worldpos[1].length(), worldpos[2].length()));
	float edgelen = griddata.grid.getLevelDimention(newedge.level());
	float deltalen = edgelen/maxwindowsize;

	MathOld::GridCellId newcell = newedge.v1();
	if(griddata.busies.find(newedge.v1())!=griddata.busies.end())
		newcell = newedge.v2();
	
	GrowPoint& oldgp = this->growpoints[edge];
	int seed = 0;

	// ����� phase � lenght
	float curphase = oldgp.phase;
	curphase += phaseoffset;
	float lenght = oldgp.length + deltalen;

	// �������� ����� �����
	added.push_back( MathOld::IFFEdgeData(newedge, this->growid, curphase, threadId, seed, lenght));
	this->growpoints[newedge] = GrowPoint(curphase, direction, lenght, threadId, edge);

	Math::Vec3f v1 = griddata.grid.getCellPos( griddata.grid.addCell(edge.v1()));
	v1 = ffworldmatrix*v1;
	Math::Vec3f v2 = griddata.grid.getCellPos( griddata.grid.addCell(edge.v2()));
	v2 = ffworldmatrix*v2;

	Math::Vec3f p1 = this->invworldpos*v1;
	Math::Vec3f p2 = this->invworldpos*v2;
	float d1 = p1.length();
	float d2 = p2.length();

//	maxDistance = __max(maxDistance, d1);
//	maxDistance = __max(maxDistance, d2);
//	maxPhase = __max(maxPhase, curphase);

	// � ��������
	{
		bool bAccept = true;
		MathOld::GridCellId upcell = griddata.grid.getUpCell(newcell);
		if( upcell.isValid())
		{
			MathOld::GridCell& cell = griddata.grid.addCell( upcell);
			MathOld::GridDeformerCell& def = cell.addGridDeformerCell(this->growid);
			if( def.w==0 || def.bindmapping[0]>curphase)
			{
			}
			else
			{
				bAccept = false;
			}
		}
		MathOld::GridCell& cell = griddata.grid.addCell( newcell);
		MathOld::GridDeformerCell& def = cell.addGridDeformerCell(this->growid);
		if( bAccept && (def.w==0 || def.bindmapping[0]>curphase))
		{
			def.bindmapping = Math::Vec4f(curphase, lenght, (float)seed, 0); // phase, threadId, seed
			def.w = 1;
		}
	}

	return true;
}

// ��� ���������
void SphereControl::Step(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata, 
	std::list<MathOld::IFFEdgeData>& added
	)
{
	srand(this->seed);
	// ������ �� alive �����	
	std::map<MathOld::GridFreeEdge, GridData::EdgeData>::iterator it = griddata.alives.begin();
	for(;it != griddata.alives.end(); it++)
	{
		const MathOld::GridFreeEdge& edge = it->first;
//		MathOld::GridEdge edge;
//		if( !MathOld::GridEdge::GridEdgeFromPair(freeedge.v1(), freeedge.v2(), edge))
//			continue;
		GridData::EdgeData& edgedata = it->second;

		// ���� ���� ����� - �� ������������ (������ ����� growpoints)
		if( edgedata.iffdata.growId == this->growid)
			continue;

		// ��������� ������ �� ����� � �����
		if( !isInside(edge, ffworldmatrix, griddata))
			continue;

		// ������� > this->maxLevel - �������
		if( edge.level()>this->maxLevel)
		{
			continue;
		}

		if( edge.level()<this->startLevel)
		{
			// ���������
			std::vector<MathOld::GridCellId> cells;
			GridData::subdivEdge(griddata.grid, edge, cells, this->startLevel);
				
			for(int i=1; i<(int)cells.size(); i++)
			{
				MathOld::GridFreeEdge edge(cells[i-1], cells[i]);
				std::map<MathOld::GridFreeEdge, GrowPoint>::iterator itph = this->growpoints.find(edge);
				if( itph == this->growpoints.end())
				{
					// �������� � growpoints
					this->growpoints[edge] = GrowPoint( 0, edge.direction());
				}
			}
		}
		else
		{
			std::map<MathOld::GridFreeEdge, GrowPoint>::iterator itph = this->growpoints.find(edge);
			if( itph == this->growpoints.end())
			{
				// �������� � growpoints
				this->growpoints[edge] = GrowPoint( 0, edge.direction());
			}
		}
	}

	{
		std::map<MathOld::GridFreeEdge, GrowPoint> _growpoints = this->growpoints;

		std::map<MathOld::GridFreeEdge, GrowPoint>::iterator it = _growpoints.begin();
		for( ; it != _growpoints.end(); it++)
		{
			const MathOld::GridFreeEdge& edge = it->first;
			GrowPoint& gp = it->second;

			// ��������� ������ �� ����� � �����
			if( !isInside(edge, ffworldmatrix, griddata))
				continue;

			// growUP 
			if( edge.level()>0 && rnd()<growUpPropencity)
			{
				StepUpLevel(edge, gp, ffworldmatrix, griddata, added);
			}
			// grow
			if( gp.growCount < this->maxGrow &&
				rnd()<growPropencity)
			{
				StepSameLevel(edge, gp, ffworldmatrix, griddata, added);
			}
			// grow Low
			if( gp.growCountLow < this->maxGrowLow &&
				edge.level() < maxLevel && 
				rnd()<growLowPropencity)
			{
				StepLowLevel(edge, gp, ffworldmatrix, griddata, added);
			}
		}
	}
	this->seed = rand();
}

// ����� ���������
void SphereControl::GrowStop(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata, 
	std::list<MathOld::IFFEdgeData>& added
	)
{
	// ��������� lenghtFromEnd
	this->CalcLenghtFromEnd(griddata);

	/*/
	// ������ �� ������� ������ �� ������ ������� ������!
	std::map<MathOld::GridFreeEdge, GrowPoint>::iterator it = this->growpoints.begin();
	for( ; it != this->growpoints.end(); it++)
	{
		const MathOld::GridFreeEdge& edge = it->first;
		GrowPoint& gp = it->second;

		std::map<MathOld::GridFreeEdge, GridData::EdgeData>::iterator it = griddata.alives.find(edge);
		if( it == griddata.alives.end())
			continue;
		GridData::EdgeData& edgedata = it->second;

		if( gp.growCount==0)
		{
			edgedata.iffdata.phase += 1000;
		}
	}
	/*/
}

// ��������� lenghtFromEnd
void SphereControl::CalcLenghtFromEnd(
	GridData& griddata
	)
{
	float maxwindowsize = __max( worldpos[0].length(), __max( worldpos[1].length(), worldpos[2].length()));

	// ��� ������� ����� ��������� �� �������� �� 0 ������ ��� � ����� ����� lenghtFromEnd=0
	std::map<MathOld::GridFreeEdge, GrowPoint>::iterator it = this->growpoints.begin();
	for(;it != this->growpoints.end(); it++)
	{
		MathOld::GridFreeEdge edge = it->first;
		float lfe = 0;

		// �� �������� ����
		for(;;)
		{
			// ����� �����
			float edgelen = griddata.grid.getLevelDimention(edge.level());
			float deltalen = edgelen/maxwindowsize;
			lfe += deltalen;

			std::map< GridData::Edge, GridData::EdgeData>::iterator ital = griddata.alives.find(edge);
			if( ital!=griddata.alives.end())
			{
				GridData::EdgeData& gded = ital->second;
				// ����������� ��� lenghtFromEnd ����������� �� ����������� ������ �������
				gded.iffdata.lenghtFromEnd = __max(lfe, gded.iffdata.lenghtFromEnd);
				lfe = gded.iffdata.lenghtFromEnd;
			}

			// ����. �����
			std::map<MathOld::GridFreeEdge, GrowPoint>::iterator itpar = this->growpoints.find(edge);
			if( itpar == this->growpoints.end())
				break;
			GrowPoint& gp = itpar->second;
			edge = gp.parent;
		}
	}
}

bool SphereControl::StepUpLevel(
	const MathOld::GridFreeEdge& edge,		// �� ������ ����� ������
	GrowPoint& gp,						// ������ ����� �����
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata,					
	std::list<MathOld::IFFEdgeData>& added
	)
{
	/*/
	// ���� ����� ������ v � ������ �������� ������
	MathOld::GridCellId id = MathOld::Grid::getUpCell(edge.v1());
	if( id.isValid())
	{
		MathOld::GridEdge newedge(id, (MathOld::enGridNeigbour3d)edge.nb);
		if( griddata.canAddEdge(newedge))
		{
			added.push_back( IFFEdgeData(newedge, this->growid, curphase));
			this->growpoints[newedge] = curphase;
		}
	}
	id = MathOld::Grid::getUpCell(edge.v2());
	if( id.isValid())
	{
		MathOld::GridEdge newedge(id, (MathOld::enGridNeigbour3d)(edge.nb+1));
		if( griddata.canAddEdge(newedge))
		{
			added.push_back( IFFEdgeData(newedge, this->growid, curphase));
			this->growpoints[newedge] = curphase;
		}
	}
	/*/
	return false;
}


bool SphereControl::StepSameLevel(
	const MathOld::GridFreeEdge& edge,		// �� ������ ����� ������
	GrowPoint& gp,						// ������ ����� �����
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata,					
	std::list<MathOld::IFFEdgeData>& added
	)
{
	// ���� gp gp.growCount==0 - ������ � ����������� gp.direction +- 45 ��� +-90
	if( gp.growCount==0)
	{
		// ������� ��������� �����
		Math::Vec3i v = edge.v1().getXYZ();
		if( edge.direction()==gp.direction)
			v = edge.v2().getXYZ();

		// ������� ����������� ������� � gp.direction
		Math::Vec3f dir( (float)gp.direction.x, (float)gp.direction.y, (float)gp.direction.z);
		dir += Math::Vec3f( 
			(rnd()-0.5f)*this->sameFluctuation, 
			(rnd()-0.5f)*this->sameFluctuation, 
			(rnd()-0.5f)*this->sameFluctuation
			);
		Math::Vec3i diri(
			(int)floor(dir.x+0.5f),
			(int)floor(dir.y+0.5f),
			(int)floor(dir.z+0.5f)
			);
		// climb
		diri.x = __min( __max(diri.x, -1), 1);
		diri.y = __min( __max(diri.y, -1), 1);
		diri.z = __min( __max(diri.z, -1), 1);

		MathOld::GridFreeEdge newedge(edge.level(), v, v+diri);

		float phaseoffset = 0;
		if( !addEdge(edge, newedge, diri, ffworldmatrix, griddata, added, phaseoffset, gp.threadId))
			return false;
		this->growpoints[edge].growCount++;
		return true;
	}
	else
	{
		// ������� �����������
		Math::Vec3f dir;
		dir = Math::Vec3f( 
			(rnd()-0.5f)*3.f, 
			(rnd()-0.5f)*3.f, 
			(rnd()-0.5f)*3.f
			);
		Math::Vec3i diri(
			(int)floor(dir.x+0.5f),
			(int)floor(dir.y+0.5f),
			(int)floor(dir.z+0.5f)
			);
		diri.x = __min( __max(diri.x, -1), 1);
		diri.y = __min( __max(diri.y, -1), 1);
		diri.z = __min( __max(diri.z, -1), 1);
		if(diri.length2()==0)
			return false;

		// ������� �����
		Math::Vec3i v = edge.v1().getXYZ();
		if(rnd()<0.5)
			v = edge.v2().getXYZ();

		float phaseoffset = (this->samePhaseBiasConst + rnd()*this->samePhaseBiasRnd);
		phaseoffset = this->samePhaseSecondaryFactor*phaseoffset;
		MathOld::GridFreeEdge newedge(edge.level(), v, v+dir);
		if( !addEdge(edge, newedge, diri, ffworldmatrix, griddata, added, phaseoffset, (int)growpoints.size()))
			return false;
		this->growpoints[edge].growCount++;
		return true;
	}
	return false;
}

bool SphereControl::StepLowLevel(
	const MathOld::GridFreeEdge& edge,		// �� ������ ����� ������
	GrowPoint& gp,						// ������ ����� �����
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata,					
	std::list<MathOld::IFFEdgeData>& added
	)
{
	// ������� �����
	std::set<MathOld::GridFreeEdge> neibouredges;
	griddata.allEdges(edge, false, neibouredges);
	int index = (int)( rnd()*neibouredges.size());
	std::set<MathOld::GridFreeEdge>::iterator it2 = neibouredges.begin();
	for(int i=0; i<index && it2 != neibouredges.end(); it2++, i++);
	if(it2 == neibouredges.end())
		return false;
	MathOld::GridFreeEdge newedge = *it2;

	float phaseoffset = 4 + rnd()*3.5f;

	if( !addEdge( edge, newedge, newedge.direction(), ffworldmatrix, griddata, added, phaseoffset, (int)growpoints.size()))
		return false;
	this->growpoints[edge].growCountLow++;
	return true;
}

bool SphereControl::deform(
	const MathOld::Grid& grid, 
	const MathOld::GridCellId& id, 
	Math::Vec3f& v, 
	Math::Vec3f& offset, 
	const MathOld::GridDeformerCell& deformercell,
	const Math::Matrix4f& worldMatrix)
{
	if( deformercell.defirmerId != this->growid)
		return false;

	float phase = deformercell.bindmapping[0];
	float lenght = deformercell.bindmapping[1];
	float p = this->getGrowParam(v, id, lenght, phase, this->deformToper);
	if( p<0) 
		return false;
//	p = (float)( 1-atan(p)*2/M_PI);
	if(p>1) p=1;
	p = 1-p;

	float noisemagnitude = this->xynoisemagnitude;
//	noisemagnitude *= p;
	float noisefrequency1 = this->xynoisefrequency;
	float noisefrequency2 = this->xynoisefrequency*0.1f;
//	noisefrequency *= p;

	Math::Vec3f sh1 = getNoise(noisemagnitude*p*p, noisefrequency1, id, seed);
	Math::Vec3f sh2 = getNoise(noisemagnitude*(1-p)*p, noisefrequency2, id, seed);

	v += sh1 + sh2;
	return true;
}

Math::PerlinNoise SphereControl::perlinnoise(23);

Math::Vec3f SphereControl::getNoise(
	float noisemagnitude, 
	float noisefrequency, 
	const MathOld::GridCellId& id, 
	int seed)
{
	Math::Vec3f noisearg1(
		id.x/this->xynoisedimention, 
		id.y/this->xynoisedimention+time*noisefrequency, 
		id.z/this->xynoisedimention);

	Math::Vec3f noisearg2(
		id.x/this->xynoisedimention+time*noisefrequency, 
		id.y/this->xynoisedimention, 
		id.z/this->xynoisedimention);

	Math::Vec3f noisearg3(
		id.x/this->xynoisedimention, 
		id.y/this->xynoisedimention, 
		id.z/this->xynoisedimention+time*noisefrequency);

	// ���
	float nx = perlinnoise.noise(noisearg1.x, noisearg1.y, noisearg1.z);
	nx = nx*noisemagnitude;

	float ny = perlinnoise.noise(noisearg2.x, noisearg2.y, noisearg2.z);
	ny = ny*noisemagnitude;

	float nz = perlinnoise.noise(noisearg3.x, noisearg3.y, noisearg3.z);
	nz = nz*noisemagnitude;

	return Math::Vec3f(nx, ny, nz);
}

/*/
// ������������� ������������� �������
void SphereControl::deform(
	const MathOld::Grid& grid, 
	const MathOld::GridCellId& id,
	const Math::Matrix4f& ffworldmatrix,
	Math::Vec3f& v)
{
	return;

	Math::Vec3f v1 = v;
	v1 = ffworldmatrix*v1;
	Math::Vec3f p1 = this->invworldpos*v1;
	if( p1.length()>1) 
		return;

	Math::Vec3f noisearg1(
		id.x/this->xynoisedimention, 
		id.y/this->xynoisedimention+time*this->xynoisefrequency, 
		id.z/this->xynoisedimention);

	Math::Vec3f noisearg2(
		id.x/this->xynoisedimention+time*this->xynoisefrequency, 
		id.y/this->xynoisedimention, 
		id.z/this->xynoisedimention);

	Math::Vec3f noisearg3(
		id.x/this->xynoisedimention, 
		id.y/this->xynoisedimention, 
		id.z/this->xynoisedimention+time*this->xynoisefrequency);

	float noisemagnitude = this->xynoisemagnitude;

	// ���
	float nx = perlinnoise.noise(noisearg1.x, noisearg1.y, noisearg1.z);
	nx = nx*noisemagnitude;

	float ny = perlinnoise.noise(noisearg2.x, noisearg2.y, noisearg2.z);
	ny = ny*noisemagnitude;

	float nz = perlinnoise.noise(noisearg3.x, noisearg3.y, noisearg3.z);
	nz = nz*noisemagnitude;

	v += Math::Vec3f(nx, ny, nz);
}
/*/









////////////////////////////////////////
//
// 
// 

void SphereControl::copy(
	const IFFControl* _arg
	)
{
	IFFControl::copy(_arg);

	const SphereControl* arg = (const SphereControl*)_arg;

	DATACOPY(bWindow);
	DATACOPY(disgrowfactor);
	DATACOPY(width0);
	DATACOPY(widthExp);

	DATACOPY(bGrow);
	DATACOPY(bDeformer);

	DATACOPY(bStart);
	DATACOPY(growUpPropencity);
	DATACOPY(growPropencity);
	DATACOPY(growLowPropencity);
	DATACOPY(maxLevel);
	DATACOPY(startLevel);
	DATACOPY(windowSize);

	DATACOPY(worldpos);
	DATACOPY(invworldpos);
	DATACOPY(seed);
	DATACOPY(startSeed);

	DATACOPY(maxGrow);
	DATACOPY(maxGrowLow);
	DATACOPY(sameFluctuation);
	DATACOPY(samePhaseBiasConst);
	DATACOPY(samePhaseBiasRnd);
	DATACOPY(samePhaseSecondaryFactor);

	// deformer
	DATACOPY(time);
	DATACOPY(xynoisemagnitude);
	DATACOPY(xynoisefrequency);
	DATACOPY(xynoisedimention);

//	DATACOPY(maxDistance);
//	DATACOPY(maxPhase);

	DATACOPY(venToper);
	DATACOPY(deformToper);
	DATACOPY(texToper);
}
void SphereControl::serialize(Util::Stream& stream)
{
	IFFControl::serialize(stream);

	int version = 8;
	stream >> version;
	if( version>=0)
	{
		SERIALIZE(bWindow);
		SERIALIZE(disgrowfactor);

		SERIALIZE(bGrow);
		SERIALIZE(bDeformer);

		SERIALIZE(bStart);
		SERIALIZE(growUpPropencity);
		SERIALIZE(growPropencity);
		SERIALIZE(growLowPropencity);
		SERIALIZE(maxLevel);
		SERIALIZE(windowSize);

		SERIALIZE(worldpos);
		SERIALIZE(invworldpos);
	}
	if( version>=1)
	{
		SERIALIZE(width0);
		SERIALIZE(widthExp);
	}
	if( version>=2)
	{
		SERIALIZE(time);
		SERIALIZE(xynoisemagnitude);
		SERIALIZE(xynoisefrequency);
		SERIALIZE(xynoisedimention);
	}
	if( version>=3)
	{
		float maxDistance, maxPhase;
		stream >> maxDistance;
		stream >> maxPhase;
	}
	if( version>=4)
	{
		SERIALIZE(venToper);
	}
	if( version>=5)
	{
		SERIALIZE(startLevel);
	}
	if( version>=6)
	{
		SERIALIZE(startSeed);
		SERIALIZE(seed);
	}
	if( version>=7)
	{
		SERIALIZE(maxGrow);
		SERIALIZE(maxGrowLow);
		SERIALIZE(sameFluctuation);
		SERIALIZE(samePhaseBiasConst);
		SERIALIZE(samePhaseBiasRnd);
		SERIALIZE(samePhaseSecondaryFactor);
	}
	if( version>=8)
	{
		SERIALIZE(deformToper);
		SERIALIZE(texToper);
	}
}

#define SERIALIZEOCS(ARG) if( bSave) render->Parameter( (name+"::"#ARG).c_str(),this->##ARG); else render->GetParameter( (name+"::"#ARG).c_str(),this->##ARG);

// ��������� � ocs
void SphereControl::SerializeOcs(
	const char* _name, 
	cls::IRender* render, 
	bool bSave
	)
{
	std::string name = _name;

	SERIALIZEOCS( bWindow);
	SERIALIZEOCS( windowSize);
	SERIALIZEOCS( disgrowfactor);
	SERIALIZEOCS( worldpos);
	SERIALIZEOCS( growid);
	SERIALIZEOCS( bDeformer);
	SERIALIZEOCS( width0);
	SERIALIZEOCS( widthExp);

	SERIALIZEOCS(maxGrow);
	SERIALIZEOCS(maxGrowLow);
	SERIALIZEOCS(sameFluctuation);
	SERIALIZEOCS(samePhaseBiasConst);
	SERIALIZEOCS(samePhaseBiasRnd);
	SERIALIZEOCS(samePhaseSecondaryFactor);

	SERIALIZEOCS( time);
	SERIALIZEOCS( xynoisemagnitude);
	SERIALIZEOCS( xynoisefrequency);
	SERIALIZEOCS( xynoisedimention);

//	SERIALIZEOCS(maxDistance);
//	SERIALIZEOCS(maxPhase);
	SERIALIZEOCS(venToper);
	SERIALIZEOCS(texToper);
	SERIALIZEOCS(deformToper);
}
