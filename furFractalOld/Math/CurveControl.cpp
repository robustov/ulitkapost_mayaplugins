#include "stdafx.h"

#include "CurveControl.h"
#include "Math/perlinnoise.h"
#include <gl/gl.h>
#include "MathNGl/MathNGl.h"
#include "Math/Bresenham.h"

extern "C"
{
	__declspec(dllexport) MathOld::IFFControl* __cdecl curveControl()
	{
		return new CurveControl();
	}
}

Math::PerlinNoise perlinnoise(1245);

CurveControl::CurveControl()
{
	maxdist = 4;
	maxdistJitter = 0.5f;
	width = 1;
	startGrowPoint = MathOld::GridCellId(0, 0, 0, 0);
	endGrowPoint = MathOld::GridCellId(0, 0, 0, 0);
	debugShowVessel = -1;
	texToper = deformToper = venToper = 0;
	this->seed = this->startSeed = 0;
	xynoisemagnitudeSubGrow = 0;

	SubGrowPropencity = 0.6f;
	SubGrowPhaseOffset = 0.2f;
	SubGrowHistoryOffset = 35;
	SubGrowWidthFactor = 1.0f;
	WidthFactorJitter = 0.5f;

	shaderId = 0;

	xynoisemagnitudemin = 0;
}

CurveControl::GrowPoint::GrowPoint()
{
	growtime = 0;
	seed = 0;
	lenght = 0;
}






///////////////////////////////////
//
// window
// 
float CurveControl::processEdge(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata,				// �������
	const MathOld::IFFEdgeData& edgedata, 
	float pointonedge,					// ����� �� ����� (�� 0 �� 1)
	float src_width
	)
{
	if( edgedata.growId != this->growid)
		return 0;

	const MathOld::GridFreeEdge& edge = edgedata.edge;
	MathOld::Grid& grid = griddata.grid;
	srand(edgedata.seed);

	if( edge.level() != startGrowPoint.level)
	{
//		src_width = src_width*(1-rnd()*this->WidthFactorJitter);
		src_width = src_width*this->SubGrowWidthFactor*(1-rnd()*this->WidthFactorJitter);
	}
	else
	{
		src_width = src_width*(1-edgedata.phase*this->WidthFactorJitter);
	}

	if( debugShowVessel!=-1)
	{
		// ������ �� ������ �����
		if( edgedata.threadId != debugShowVessel)
			return 0;
	}

	Math::Vec3f v1 = grid.getCellPos(edge.v1());
	Math::Vec3f v2 = grid.getCellPos(edge.v2());
	Math::Vec3f v = v1*(1-pointonedge) + v2*pointonedge;
	
	float w = this->getWidth(v, edge.v1(), edgedata.lenght, edgedata.phase);

//fprintf(stderr, "%d %d %d %d (%d) = %f\n", edge.v.level, edge.v.x, edge.v.y, edge.v.z, edge.nb, w);
//fflush(stderr);

	return w*src_width*this->width;
}


// �������� ����� ��� ������� ������ � �����.���������
float CurveControl::getGrowParam(
	const Math::Vec3f& v, 
	const MathOld::GridCellId& id, 
	float lenght, 
	float phase, 
	float toper, 
	Math::Vec2i& offsetNormal)
{
	MathOld::GridCellId next = id;
	for(;next.level > this->growlevel; )
		next = MathOld::Grid::getUpCell(next, true);
	MathOld::GridCellId offset(
		next.level, 
		next.x-this->startGrowPoint.x, 
		next.y-this->startGrowPoint.y, 
		next.z-this->startGrowPoint.z
		);

	int axis = this->growdir&(~0x1);
	int growlen=0;		// ��������� �������
	int fulllen=0;		// ������ �����
	// �������� � ���������� ����� ���������

	if( axis==(int)MathOld::NEI_X_UP)
	{
		offsetNormal = Math::Vec2i(offset.y, offset.z);
		growlen = next.x - this->startGrowPoint.x;
		fulllen = this->endGrowPoint.x - this->startGrowPoint.x;
	}
	else if( axis==(int)MathOld::NEI_Y_UP)
	{
		offsetNormal = Math::Vec2i(offset.x, offset.z);
		growlen = next.y - this->startGrowPoint.y;
		fulllen = this->endGrowPoint.y - this->startGrowPoint.y;
	}
	else // MathOld::NEI_Z_UP
	{
		offsetNormal = Math::Vec2i(offset.x, offset.y);
		growlen = next.z - this->startGrowPoint.z;
		fulllen = this->endGrowPoint.z - this->startGrowPoint.z;
	}

	float edgeparam = lenght;
	float distparam = 2*maxdist/fabs((float)fulllen);
	float w = (this->windowparam-edgeparam)/distparam;
	w = (this->windowparam-lenght);

	float wfactor = 1000000.f;
	if(toper>0)
		wfactor = 1/toper;

	w -= phase*this->disgrowfactor;
	w *= wfactor;

	return w;
}

float CurveControl::getWidth(Math::Vec3f& v, const MathOld::GridCellId& id, float lenght, float phase)
{
	Math::Vec2i offsetNormal;
	float w = this->getGrowParam(v, id, lenght, phase, this->venToper, offsetNormal);

	w = (float)( atan(w)*2/M_PI);

	// maxW - ��������� <1
	float dist = 0;
	float widthJitter = 0.5f;
	float minwidth = 0.05f;
	/*/
	srand( (int)(100.f*M_PI*threadId));
	rnd();rnd();
	float maxW = rnd()*widthJitter + (1-widthJitter);
	if(w>maxW) w=maxW;
	if(w<minwidth) return w-minwidth;
	/*/
	if(w<0) return w;

	float noisemagnitude = this->noisemagnitude;

	Math::Vec3f noisearg(
		dist/this->noisedimention + time*this->noisefrequency, 
		offsetNormal.x/(float)this->maxdist, 
		offsetNormal.y/(float)this->maxdist);
	noisearg *= 1;

	// ���
	float n = perlinnoise.noise(noisearg.x, noisearg.y, noisearg.z);
	w += n*noisemagnitude;
	if(w<minwidth) return minwidth;

	return w;
}
// ���������� �����.���������� ��� ����� �� �����
bool CurveControl::getTexCoord(
	float& texCoord, 
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata,					// �������
	const MathOld::IFFEdgeData& edgedata,			// �������� �����
	float pointonedge,					// ����� �� ����� (�� 0 �� 1)
	int texCoordType					// �� ������� ���� ��������� UV ����� ���� ������
	)
{
	if( edgedata.growId != this->growid)
		return false;

	if( texCoordType==0)
	{
		const MathOld::GridFreeEdge& edge = edgedata.edge;
		MathOld::Grid& grid = griddata.grid;

		Math::Vec3f v1 = grid.getCellPos(edge.v1());
		Math::Vec3f v2 = grid.getCellPos(edge.v2());
		Math::Vec3f v = v1*(1-pointonedge) + v2*pointonedge;

		Math::Vec2i offsetNormal;
		texCoord = this->getGrowParam(v, edge.v1(), edgedata.lenght, edgedata.phase, this->texToper, offsetNormal);
	}
	if( texCoordType==1)
	{
		const MathOld::GridFreeEdge& edge = edgedata.edge;
		MathOld::Grid& grid = griddata.grid;

		texCoord = edgedata.lenght;
	}
	if( texCoordType==2)
	{
		// shaderId
		texCoord = (float)this->shaderId;
	}

	return true;
}

bool CurveControl::getMainPoint(
	const MathOld::GridCellId& v1,	//����������� ����� v1->v2
	const MathOld::GridCellId& v2,
	const std::list<MathOld::GridCellId>& v3list,	// ��������� ����� ���. ������� �� ������� v2
	GridData& griddata,
	MathOld::GridCellId& mainPoint //��������� �����, ���� ��������� true
	)
{
	MathOld::GridFreeEdge ed1(v1, v2);
	std::map< GridData::Edge, GridData::EdgeData>::iterator ital = griddata.alives.find(ed1);
	if( ital== griddata.alives.end())
		return false;
	GridData::EdgeData& edata1 = ital->second;

	if( edata1.iffdata.growId != this->growid)
		return false;

	bool bFind = false;
	float curphase = 1e32f;

	std::list<MathOld::GridCellId>::const_iterator it = v3list.begin();
	for(;it != v3list.end(); it++)
	{
		const MathOld::GridCellId& v3 = *it;
		if( v3 == v1) 
			continue;

		MathOld::GridFreeEdge ed2(v2, v3);
		ital = griddata.alives.find(ed2);
		if( ital== griddata.alives.end())
			continue;
		GridData::EdgeData& edata2 = ital->second;

		// ��������� 
		if(curphase>edata2.iffdata.phase)
//		if(	edata1.iffdata.growId   == edata2.iffdata.growId &&
//			edata1.iffdata.threadId == edata2.iffdata.threadId
//			)
		{
			mainPoint = v3;
			curphase = edata2.iffdata.phase;
			bFind = true;
		}
	}
	return bFind;
}

// ��� ��������� �� ��������
void CurveControl::draw(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata
	)
{
	float dim = griddata.grid.dimention0;
	MathOld::Grid& grid = griddata.grid;

	float param = windowparam;
	if( windowparam>1) param = 1;
	if( windowparam<0) param = 0;

	// ����� ����� � �������� �� �����
	Math::Vec3f v1( (float)this->startGrowPoint.x, (float)this->startGrowPoint.y, (float)this->startGrowPoint.z);
	Math::Vec3f v2( (float)this->endGrowPoint.x, (float)this->endGrowPoint.y, (float)this->endGrowPoint.z);
	
	Math::Vec3f v = v1*(1-param) + v2*(param);
	MathOld::GridCellId id1(this->startGrowPoint.level, (int)floor(v.x), (int)floor(v.y), (int)floor(v.z));
	float paramedge = param*(v2-v1).length();
	paramedge = paramedge - floor(paramedge);

	MathOld::GridCellId id2 = MathOld::Grid::getNeigbour(id1, growdir);

	Math::Vec3f vv1 = grid.getCellPos(id1);
	Math::Vec3f vv2 = grid.getCellPos(id2);
	
	Math::Vec3f vv = vv1*(1-paramedge) + vv2*(paramedge);

	float r = this->maxdist * grid.getLevelDimention(this->growlevel)/dim;
	drawSphere(r, vv);
}




//////////////////////////////////////////////
//
// Grow
// 

// ��������� �������
void CurveControl::Start(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata, 
	std::list<MathOld::IFFEdgeData>& added
	)
{
	this->ReinitGrowId();
	srand(this->startSeed);

	if( this->curve.size()<2) 
		return;
	MathOld::Grid& grid = griddata.grid;

	Math::Matrix4f ffworldmatrixinv = ffworldmatrix.inverted();

	// 1. ���������� ��������� �������
	Math::Vec3f pt1 = ffworldmatrixinv*this->curve.front();
	Math::Vec3f pt2 = ffworldmatrixinv*this->curve.back();
	float dist = ((pt1-pt2).length());
	int level=0;
	for( ; ; )
	{
		float dim = grid.getLevelDimention(level);
		if( dist<dim*3/4)
		{
			level++;
			continue;
		}
		if( dist>dim*3/2)
		{
			level--;
			continue;
		}
		break;
	}

	//////////////////////////////////////
	// ����� ��������� �����
	Math::Vec3f mid = (pt2+pt1)*0.5f;
	Math::Vec3f dir = (pt2-pt1);
	dir.x = fabs(dir.x);
	dir.y = fabs(dir.y);
	dir.z = fabs(dir.z);
	Math::Vec3f posInCube;
	MathOld::GridCellId cubeid = grid.findTopLevelCube(mid, level, &posInCube);

	MathOld::GridCellId edgev1 = cubeid;
	if( posInCube.x>0.5f)
		edgev1.x++;
	if( posInCube.y>0.5f)
		edgev1.y++;
	if( posInCube.z>0.5f)
		edgev1.z++;

	MathOld::GridEdge edge;
	if( dir.x > dir.y && dir.x>dir.z)
	{
		edge = MathOld::GridEdge(
			MathOld::GridCellId(cubeid.level, cubeid.x, edgev1.y, edgev1.z), 
			MathOld::NEI_X_UP
			);
	}
	else if(dir.y >= dir.x && dir.y>dir.z)
	{
		edge = MathOld::GridEdge(
			MathOld::GridCellId(cubeid.level, edgev1.x, cubeid.y, edgev1.z), 
			MathOld::NEI_Y_UP
			);
	}
	else
	{
		edge = MathOld::GridEdge(
			MathOld::GridCellId(cubeid.level, edgev1.x, edgev1.y, cubeid.z), 
			MathOld::NEI_Z_UP
			);		
	}

	MathOld::GridCell& _cell1 = grid.addCell(edge.v1());
	MathOld::GridCell& _cell2 = grid.addCell(edge.v2());
	Math::Vec3f cellpos1 = grid.getCellPos(_cell1, false);
	Math::Vec3f cellpos2 = grid.getCellPos(_cell2, false);
	// ��� ����� � pt1 
	MathOld::GridCellId id1 = edge.v1();
	MathOld::GridCellId id2 = edge.v2();
	if( (cellpos1-pt1).length2() > (cellpos2-pt1).length2() )
		std::swap(id1, id2);

	// �������� ��������� �����
	MathOld::GridCell* cell1 = grid.getCell(id1);
	cell1->offset = pt1 - grid.getCellPos( *cell1, false);
	cellpos1 = grid.getCellPos(*cell1, false);
	{
		// deformer
		MathOld::GridDeformerCell& defcell = cell1->addGridDeformerCell(this->growid+1);
		defcell.w = 1;
		defcell.bindmapping = Math::Vec4f( 0, 0, 0, 0);
	}

	MathOld::GridCell* cell2 = grid.getCell(id2);
	cell2->offset = pt2 - grid.getCellPos( *cell2, false);
	cellpos2 = grid.getCellPos(*cell2, false);
	{
		// deformer
		MathOld::GridDeformerCell& defcell = cell2->addGridDeformerCell(this->growid+1);
		defcell.w = 1;
		defcell.bindmapping = Math::Vec4f( (float)this->curve.size()-1, 0, 0, 0);
	}

	//////////////////////////////////////
	// ����� ���������� �������� �������� ������������� ����� ����� neiboffsets
	std::map<MathOld::GridCellId, GrowNeibourData> neiboffsets;
	Start_rec(ffworldmatrixinv, grid, id1, id2, 0, (int)this->curve.size()-1, 3, neiboffsets);

	// �������� �������� �����
	std::map<MathOld::GridCellId, GrowNeibourData>::iterator it = neiboffsets.begin();
	for(;it != neiboffsets.end(); it++)
	{
		const MathOld::GridCellId& id = it->first;
		GrowNeibourData& gnd = it->second;
		if( gnd.bRoot) continue;
		
		float w = 0.5f;
		gnd.offset *= 1.f/gnd.count;
		MathOld::GridCell& cell = grid.addCell(id);
		cell.offset = gnd.offset*w;

//		GridDeformerCell& defcell = cell.addGridDeformerCell(this->growid+1);
//		defcell.w = w;
//		defcell.bindmapping = Math::Vec3f(0, 0, 0);
	}



	////////////////////////////////////////////////
	// ��������� ����� �����:

	this->growdir = edge.nb; // ����������� �����
	this->growpoints.clear();			// ������� ����� �����
	this->growpoints.reserve(vessels);
	this->modified_verts.clear();
	this->startGrowPoint = edge.v1();
	this->endGrowPoint = edge.v2();
	if( this->bInvertDirection)
	{
		std::swap(this->startGrowPoint, this->endGrowPoint);
		this->growdir = MathOld::gridInvertDirection(this->growdir);
	}
 	for(; this->startGrowPoint.level<this->growlevel; )
	{
		this->startGrowPoint = grid.getLowCell(this->startGrowPoint);
		this->endGrowPoint	 = grid.getLowCell(this->endGrowPoint);
	}
	// ���������� ����� �����
	for(int g=0; g<(int)vessels; g++)
	{
		this->growpoints.push_back(GrowPoint());
		GrowPoint& gp = this->growpoints.back();
		StartGrowPoint2(gp, g, ffworldmatrix,griddata, added);
	}
fflush(stderr);
	this->seed = rand();
	return;
}
// ��� ���������
void CurveControl::Step(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata, 
	std::list<MathOld::IFFEdgeData>& added
	)
{
	srand(this->seed);
	for( int g=0; g<(int)this->growpoints.size(); g++)
	{
		GrowPoint& gp = this->growpoints[g];
		if( StepGrowPoint2(gp, g, ffworldmatrix, griddata, added))
		{
			float sgp = this->SubGrowPropencity;
			for( ; sgp>0; sgp-=1.f)
			{
				if( sgp<1)
					if(rnd()>sgp)
						break;
				StepSubGrowPoint2(ffworldmatrix, griddata, added);
			}
		}
	}
	fflush(stderr);

	this->seed = rand();
}

void CurveControl::Start_rec(
	const Math::Matrix4f& ffworldmatrixinv, 
	MathOld::Grid& grid, 
	MathOld::GridCellId id1, 
	MathOld::GridCellId id2, 
	int cv1, 
	int cv2, 
	int maxLevel, 
	std::map<MathOld::GridCellId, GrowNeibourData>& neiboffsets
	)
{
	MathOld::GridEdge edge(id1, id2);

	// �������� �������
	neiboffsets[id1].bRoot = true;
	neiboffsets[id2].bRoot = true;
	for(int n=0; n<6; n++)
	{
		MathOld::GridCellId neiid1 = grid.getNeigbour(id1, n);
		neiboffsets[neiid1].offset += grid.addCell(id1).offset;
		neiboffsets[neiid1].count++;

		MathOld::GridCellId neiid2 = grid.getNeigbour(id2, n);
		neiboffsets[neiid2].offset += grid.addCell(id2).offset;
		neiboffsets[neiid2].count++;
	}

	if(id1.level>maxLevel)
		return;
	if( abs(cv1-cv2)<=1 )
		return;

	if( (cv1+cv2) & 0x1) 
	{
		printf("CurveControl::Start_rec UNREAL SEGMENT\n");
		return;
	}

	int cvmid = (cv1+cv2)/2;

	Math::Vec3f pt1 = this->curve[cvmid];
	pt1 = ffworldmatrixinv*pt1;

	MathOld::GridCellId xid0 = grid.getLowCell(id1);
	MathOld::GridCellId xid1 = grid.getEdgeMiddle( edge);
	MathOld::GridCellId xid2 = grid.getLowCell(id2);

	grid.addCell(xid0).offset = Math::Vec3f(0);

	MathOld::GridCell& cell1 = grid.addCell(xid1);
	cell1.offset = Math::Vec3f(0);
	Math::Vec3f pos = grid.getCellPos(cell1, false);
	cell1.offset = pt1 - pos;
	
	// deformer
	{
		MathOld::GridDeformerCell& defcell = cell1.addGridDeformerCell(this->growid+1);
		defcell.w = 1;
		defcell.bindmapping = Math::Vec4f( (float)cvmid, 0, 0, 0);
	}

	grid.addCell(xid2).offset = Math::Vec3f(0);

	Start_rec(ffworldmatrixinv, grid, xid0, xid1, cv1, cvmid, maxLevel, neiboffsets);
	Start_rec(ffworldmatrixinv, grid, xid1, xid2, cvmid, cv2, maxLevel, neiboffsets);
}


// ����������� xy � ��������� ����� � ����������
Math::Vec3i CurveControl::localXYtoWorld(const Math::Vec2i arg, const Math::Vec3i& src)
{
	int axis = this->growdir&(~0x1);
	if( axis==(int)MathOld::NEI_X_UP)
		return Math::Vec3i( 
			src.x, 
			arg.x+this->startGrowPoint.y, 
			arg.y+this->startGrowPoint.z); 
	else if( axis==(int)MathOld::NEI_Y_UP)
		return Math::Vec3i( 
			arg.x+this->startGrowPoint.x, 
			src.y, 
			arg.y+this->startGrowPoint.z);
	else // MathOld::NEI_Z_UP
		return Math::Vec3i( 
			arg.x+this->startGrowPoint.x, 
			arg.y+this->startGrowPoint.y, 
			src.z);
}
// ����������� ���������� � xy � ��������� �����
Math::Vec2i CurveControl::world2localXY(const Math::Vec3i arg, int& growlen)
{
	Math::Vec3i offset(
		arg.x-this->startGrowPoint.x, 
		arg.y-this->startGrowPoint.y, 
		arg.z-this->startGrowPoint.z
		);

	Math::Vec2i cur;
	int axis = this->growdir&(~0x1);
	if( axis==(int)MathOld::NEI_X_UP)
		cur = Math::Vec2i( offset.y, offset.z), growlen = this->endGrowPoint.x - arg.x; 
	else if( axis==(int)MathOld::NEI_Y_UP)
		cur = Math::Vec2i( offset.x, offset.z), growlen = this->endGrowPoint.y - arg.y; 
	else // MathOld::NEI_Z_UP
		cur = Math::Vec2i( offset.x, offset.y), growlen = this->endGrowPoint.z - arg.z; 

	return cur;
}

Math::Vec2f CurveControl::perlin2f(const Math::Vec3f& position, float time, float maxdist, float noisefrequency)
{
	Math::Vec3f noisearg1(
		time*noisefrequency, 
		position.y,	// + time*noisefrequency+(float)M_PI, 
		position.z);	// + time*noisefrequency-(float)M_PI);

	Math::Vec3f noisearg2(
		position.x,	//+time*noisefrequency, 
		time*noisefrequency, 
		position.z);	//+time*noisefrequency-5.33f);
	// ���
	float nx = perlinnoise.noise(noisearg1.x, noisearg1.y, noisearg1.z);
	nx = nx*maxdist;

	float ny = perlinnoise.noise(noisearg2.x, noisearg2.y, noisearg2.z);
	ny = ny*maxdist;
	Math::Vec2f neo(nx, ny);
	return neo;
}

void CurveControl::StartGrowPoint2(
	GrowPoint& gp, 
	int g,
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata, 
	std::list<MathOld::IFFEdgeData>& added
	)
{

	float dim = griddata.grid.dimention0;
	gp.lastDirection = this->growdir;
	gp.phase = rnd();
	if(g==0)
		gp.phase = 0;

	gp.freq = growfrequency*0.01f;
	gp.maxdist = this->maxdist*(1 - (1-gp.phase)*maxdistJitter);
	gp.rndposition = Math::Vec3f( rnd(), rnd(), rnd());
	gp.growtime = rnd()*growfrequency*20;
	gp.idfloat = perlin2f(gp.rndposition, gp.growtime, gp.maxdist/dim, gp.freq);
	Math::Vec2i neo( (int)floor(gp.idfloat.x+0.5f), (int)floor(gp.idfloat.y+0.5f));
	Math::Vec3i pt = localXYtoWorld( neo, this->startGrowPoint.getXYZ());
	gp.idint = neo;
	gp.id = MathOld::GridCellId( this->startGrowPoint.level, pt);
	gp.seed = rand();
}

// ��� ��������� ����� ����� �����
bool CurveControl::StepGrowPoint2(
	GrowPoint& gp, 
	int g,
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata, 
	std::list<MathOld::IFFEdgeData>& added
	)
{
	float dim = griddata.grid.dimention0;
	Math::Vec2i id = gp.idint;

	// growlen! ������� ���� �������
	Math::Vec2f cur = gp.idfloat;
	bool bInvertDir = this->growdir&(0x1);
	int axis = this->growdir&(~0x1);
	int growlen;
	int fulllen=0;		// ������ �����
	if( axis==(int)MathOld::NEI_X_UP)
	{
		growlen = this->endGrowPoint.x - gp.id.x; 
		fulllen = this->endGrowPoint.x - this->startGrowPoint.x;
	}
	else if( axis==(int)MathOld::NEI_Y_UP)
	{
		growlen = this->endGrowPoint.y - gp.id.y; 
		fulllen = this->endGrowPoint.y - this->startGrowPoint.y;
	}
	else // MathOld::NEI_Z_UP
	{
		growlen = this->endGrowPoint.z - gp.id.z; 
		fulllen = this->endGrowPoint.z - this->startGrowPoint.z;
	}
	float deltalen = 1.f/(fabs((float)fulllen)+2);

	if( bInvertDir)
		growlen = -growlen;

	if( growlen<0)
	{
		// �� ������ ������
		return false;
	}

	float maxDist = gp.maxdist/dim;
	float param = fabs( growlen/(float)fulllen);
	float maxDistScale =  this->maxDistRamp.getValue(param);
	Math::Vec2f neo = perlin2f(gp.rndposition, gp.growtime, maxDist*maxDistScale, gp.freq*dim);
	gp.idfloat = neo;
	Math::Vec2i neoi((int)(floor(neo.x+0.5f)), (int)(floor(neo.y+0.5f)));

	Math::Vec2i difi = neoi-id;

	difi.x = __max(difi.x, -1);
	difi.x = __min(difi.x,  1);
	difi.y = __max(difi.y, -1);
	difi.y = __min(difi.y,  1);
	if(abs(difi.y)+abs(difi.x)>1)
	{
		// ��������!
		// ������ ����� �� ��������� ����� � ����
		Math::Vec2i neoi1 = id+Math::Vec2i(0, difi.y);
		Math::Vec2i neoi2 = id+Math::Vec2i(difi.x, 0);
		float l1 = (neo - Math::Vec2f(neoi1)).length2();
		float l2 = (neo - Math::Vec2f(neoi2)).length2();
		if(l1<l2)
			difi.x = 0;
		else
			difi.y = 0;
	}
	neoi = id + difi;
	Math::Vec2f dif = Math::Vec2f(neo.x-neoi.x, neo.y-neoi.y);

//	Math::Vec2i neoi((int)(floor(cur.x+dif.x+0.5f)), (int)(floor(cur.y+dif.y+0.5f)));
	Math::Vec3i nexti = localXYtoWorld(neoi, Math::Vec3i(gp.id.x, gp.id.y, gp.id.z));
	MathOld::GridCellId next(gp.id.level, nexti);
	MathOld::GridFreeEdge edge;
	if( neoi==id)
	{
		// ������ ������
		next = MathOld::Grid::getNeigbour(gp.id, this->growdir);
	}
	else
	{
		// next ������ ���� �������� � id
		next = MathOld::Grid::getNeigbour(next, this->growdir);
	}
	gp.growtime += 1;
	edge = MathOld::GridFreeEdge( gp.id, next);

	//
//	gp.lastDirection = edge.nb;
	gp.history.push_front(gp.id);
	gp.id = next;
	gp.idint = neoi;
	gp.lenght += deltalen;

	if( this->canAddEdge(edge, gp.phase, griddata, added))
	{
		added.push_back(
			MathOld::IFFEdgeData( edge, this->growid, gp.phase, g, gp.seed, gp.lenght)
			);

		// ����� ����� �������� ������ ���� �� ��� �� ������� �������!!!
		std::map<MathOld::GridCellId, float>::iterator it = this->modified_verts.find(gp.id);
		if( it==this->modified_verts.end() ||
			it->second>gp.phase)
		{
			// ��������� � ���������
			MathOld::GridCell& cell = griddata.grid.addCell(gp.id);
			{
				MathOld::GridDeformerCell& def = cell.addGridDeformerCell(this->growid);
				if( def.bindmapping[3]==0 || def.bindmapping[0]>gp.phase)
				{
					def.bindmapping = Math::Vec4f(gp.phase, gp.lenght, (float)gp.seed, 1); // phase, threadId, seed
					def.w = 0;
				}
			}

			// �������� �����!!!
			Math::Vec3f pos = griddata.grid.getCellPos(cell);

			{
				// ��������� ���� ������ ������� �����
				Math::Vec3f offset;
				int axis = this->growdir&(~0x1);
				if( axis==(int)MathOld::NEI_X_UP)
					offset = Math::Vec3f(0, dif.x, dif.y); 
				else if( axis==(int)MathOld::NEI_Y_UP)
					offset = Math::Vec3f(dif.x, 0, dif.y); 
				else // MathOld::NEI_Z_UP
					offset = Math::Vec3f(dif.x, dif.y, 0); 

				offset = offset*griddata.grid.getLevelDimention(this->startGrowPoint.level);
				cell.offset = offset;
			}

			this->modified_verts[gp.id] = gp.phase;
		}
	}
	return true;
}
// ���������� ������� ����
void CurveControl::StepSubGrowPoint2(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata, 
	std::list<MathOld::IFFEdgeData>& added
	)
{
//	float SubGrowPhaseOffset = 0.00f;
	float SubGrowPhaseAdd = 0.00f;

	int axis = this->growdir&(~0x1);
	int fulllen=0;		// ������ �����
	if( axis==(int)MathOld::NEI_X_UP)
		fulllen = this->endGrowPoint.x - this->startGrowPoint.x;
	else if( axis==(int)MathOld::NEI_Y_UP)
		fulllen = this->endGrowPoint.y - this->startGrowPoint.y;
	else // MathOld::NEI_Z_UP
		fulllen = this->endGrowPoint.z - this->startGrowPoint.z;
	float deltalen = 0.5f/(fabs((float)fulllen)+2);

	// ������� 2 �������
	int s1 = (int)( rnd()*growpoints.size());
	int s2 = (int)( rnd()*growpoints.size());
	s1 = __min( s1, (int)growpoints.size()-1);
	s2 = __min( s2, (int)growpoints.size()-1);

	GrowPoint& gp1 = growpoints[s1];
	GrowPoint& gp2 = growpoints[s2];

	int historyOffset1 = __min( (int)(rnd()*this->SubGrowHistoryOffset), (int)gp1.history.size()-1);
	int historyOffset2 = 0;

	float phase;
	float lenght;
	MathOld::GridCellId id1, id2;
	if(s1!=s2)
	{
		phase = __max(gp1.phase, gp2.phase);
		lenght = __max(gp1.lenght, gp2.lenght);

		id1 = gp1.id;
		std::list<MathOld::GridCellId>::iterator it=gp1.history.begin();
		for(int i=0; it!=gp1.history.end()&&i<historyOffset1; it++, i++)
			id1 = *it;
		id2 = gp2.id;
	}
	else
	{
		// � ��������� �������
		return;
		id1 = gp1.id;

		Math::Vec3i v2( (int)(historyOffset1*(rnd()-0.5f)), (int)(historyOffset1*(rnd()-0.5f)), (int)(historyOffset1*(rnd()-0.5f)));
		v2 += historyOffset1*MathOld::getNeigbour3dDirection( (MathOld::enGridNeigbour3d)growdir);
		if( v2==Math::Vec3i(0))
			return;
		id2 = MathOld::GridCellId(id1.level, id1.getXYZ()+v2);

		phase = gp1.phase;
		lenght = gp1.lenght;
	}
	phase += rnd()*SubGrowPhaseOffset;
	int seed = rand();

	id1 = MathOld::Grid::getLowCell( id1);
	id2 = MathOld::Grid::getLowCell( id2);

	int threadId = -1;
	std::vector<Math::Vec3i> res;
	Math::Bresenham3( id1.getXYZ(), id2.getXYZ(), res);
	for(int i=1; i<(int)res.size(); i++)
	{
		MathOld::GridFreeEdge edge(id1.level, res[i-1], res[i]);
		lenght += deltalen;
		if( this->canAddEdge(edge, phase, griddata, added))
		{
			added.push_back(
				MathOld::IFFEdgeData( edge, this->growid, phase, threadId, seed, lenght)
				);
		}
		// � ��������
		if(i<(int)res.size()-1)
		{
			MathOld::GridCell& cell = griddata.grid.addCell( MathOld::GridCellId( id1.level, res[i]));
			MathOld::GridDeformerCell& def = cell.addGridDeformerCell(this->growid);
			if( def.bindmapping[3]==0 || def.bindmapping[0]>phase)
			{
				def.bindmapping = Math::Vec4f(phase, lenght, (float)seed, 1); // phase, threadId, seed
				def.w = 1;
			}
		}

		phase += SubGrowPhaseAdd;
		
	}
}

// ����� �� �������� �����
bool CurveControl::canAddEdge(
	MathOld::GridFreeEdge& edge, 
	float phase,
	GridData& griddata, 
	std::list<MathOld::IFFEdgeData>& added
	)
{
	// ���� ��� ���� ����� ����� - �� ��������� � ������
	std::map<MathOld::GridFreeEdge, GridData::EdgeData>::iterator ita = griddata.alives.find(edge);
	int beforeme=-1;
	float phasebefore = 0;
	bool bAdd = true;
	if( ita != griddata.alives.end())
	{
		if( ita->second.iffdata.phase<phase)
			bAdd = false;
		beforeme = ita->second.iffdata.threadId;
		phasebefore = ita->second.iffdata.phase;
	}
	std::list<MathOld::IFFEdgeData>::iterator it = added.begin();
	for(;it != added.end(); it++)
	{
		if(it->edge == edge)
		{
			if(it->phase<phase)
				bAdd = false;
			beforeme = it->threadId;
			phasebefore = it->phase;
		}
	}
	return bAdd;
}




////////////////////////////////
//
// deform
// 
bool CurveControl::deform(
	const MathOld::Grid& grid, 
	const MathOld::GridCellId& id, 
	Math::Vec3f& v, 
	Math::Vec3f& offset, 
	const MathOld::GridDeformerCell& deformercell,
	const Math::Matrix4f& worldMatrix)
{
	if( deformercell.defirmerId == this->growid)
	{
		// ���������� �� ����� ��������
		float phase = deformercell.bindmapping[0];
		float lenght = deformercell.bindmapping[1];
		int seed = (int)deformercell.bindmapping[2];

		Math::Vec2i offsetNormal;
		float p = this->getGrowParam(v, id, lenght, phase, this->deformToper, offsetNormal);
		if( p<0) 
			p=0;
		p = (float)( 1-atan(p)*2/M_PI);

		int z;
		Math::Vec2i xy = this->world2localXY( id.getXYZ(), z);

		// noisemagnitude
		float noisemagnitude = this->xynoisemagnitude;
		if( id.level > startGrowPoint.level)
			noisemagnitude = this->xynoisemagnitudeSubGrow;
		noisemagnitude *= p;

		// noisefrequency
		float noisefrequency1 = this->xynoisefrequency;
		float noisefrequency2 = this->xynoisefrequency*0.1f;

		Math::Vec3f sh1 = getNoise(noisemagnitude*p*p, noisefrequency1, xy, (float)z, (int)seed);
		Math::Vec3f sh2 = getNoise(noisemagnitude*(1-p)*p, noisefrequency2, xy, (float)z, (int)seed);

		offset += sh1+sh2;
		return true;
	}
	if( deformercell.defirmerId == this->growid+1)
	{
		// ���������� ������
		Math::Matrix4f ffworldmatrixinv = worldMatrix.inverted();
		int cvmid = (int)deformercell.bindmapping[0];

		if( cvmid<0 || cvmid>=(int)this->curve.size()) 
			return false;

		Math::Vec3f pt1 = this->curve[cvmid];
		pt1 = ffworldmatrixinv*pt1;
		v = pt1;
		return true;
	}
	return false;
}

// ���
Math::Vec3f CurveControl::getNoise(
	float noisemagnitude, 
	float noisefrequency, 
	const Math::Vec2i& xy, 
	float z, 
	int seed)
{
	float fjitter = 0;
	Math::Vec3f noisearg1(
		(float)(seed*M_PI), 
		(float)(seed*M_PI + z/this->xynoisedimention-time*(noisefrequency)), 
		sqrt((float)seed));

	Math::Vec3f noisearg2(
		(float)(seed*M_PI + z/this->xynoisedimention-time*(noisefrequency)), 
		sqrt((float)seed), 
		(float)(seed*M_PI));

	// ���
	float nx = perlinnoise.noise(noisearg1.x, noisearg1.y, noisearg1.z);
	nx = nx*noisemagnitude;

	float ny = perlinnoise.noise(noisearg2.x, noisearg2.y, noisearg2.z);
	ny = ny*noisemagnitude;

	Math::Vec3f sh;
	float dist2=0;
	int axis = this->growdir&(~0x1);
	if( axis==(int)MathOld::NEI_X_UP)
	{
		sh = Math::Vec3f(0, nx, ny);
	}
	else if( axis==(int)MathOld::NEI_Y_UP)
	{
		sh = Math::Vec3f(nx, 0, ny);
	}
	else // MathOld::NEI_Z_UP
	{
		sh = Math::Vec3f(nx, ny, 0);
	}
	return sh;
}

// ������������� ������������� �������
void CurveControl::deform(
	const MathOld::Grid& grid, 
	const MathOld::GridCellId& id,
	const Math::Matrix4f& worldMatrix,
	Math::Vec3f& v)
{
return;

	float dim = grid.dimention0;

	float noisemagnitude = this->xynoisemagnitude;
	if( id.level < startGrowPoint.level)
		return;
	if( id.level > startGrowPoint.level)
		noisemagnitude = this->xynoisemagnitudeSubGrow;
	float noisefrequency = this->xynoisefrequency;
	int z;
	Math::Vec2i xy = world2localXY( id.getXYZ(), z);

	Math::Vec3f sh = this->getNoise(noisemagnitude, noisefrequency, xy, (float)z, 0);

	MathOld::GridCellId offset(
		id.level, 
		id.x-this->startGrowPoint.x, 
		id.y-this->startGrowPoint.y, 
		id.z-this->startGrowPoint.z
		);

	float dist2=0;
	int axis = this->growdir&(~0x1);
	if( axis==(int)MathOld::NEI_X_UP)
	{
		dist2 = (float)( offset.y*offset.y + offset.z*offset.z); 
	}
	else if( axis==(int)MathOld::NEI_Y_UP)
	{
		dist2 = (float)( offset.x*offset.x + offset.z*offset.z); 
	}
	else // MathOld::NEI_Z_UP
	{
		dist2 = (float)( offset.x*offset.x + offset.y*offset.y); 
	}

	if( dist2>=this->maxdist*this->maxdist/(dim*dim))
		return;

	v += sh;
}











/////////////////////////////
//
// COPY N serialize
// 

void CurveControl::copy(
	const IFFControl* _arg
	)
{
	IFFControl::copy(_arg);

	const CurveControl* arg = (const CurveControl*)_arg;
	DATACOPY(bWindow);
	DATACOPY(bDeformer);
	DATACOPY(bGrow);

	// ��������� ����� �������� � init
	DATACOPY(growlevel);
	DATACOPY(bInvertDirection);
	DATACOPY(vessels);
	DATACOPY(maxdist);
	DATACOPY(maxdistJitter);
	DATACOPY(maxDistRamp);
	DATACOPY(trueway_propencity);
	DATACOPY(curve);
	DATACOPY(growfrequency);
	DATACOPY(SubGrowPropencity);
	DATACOPY(seed);
	DATACOPY(startSeed);
	DATACOPY(SubGrowPhaseOffset);
	DATACOPY(SubGrowHistoryOffset);
	DATACOPY(SubGrowWidthFactor);
	DATACOPY(WidthFactorJitter);

	// ��������� ����
	DATACOPY(windowparam);
	DATACOPY(time);
	DATACOPY(noisemagnitude);
	DATACOPY(xynoisemagnitudeSubGrow);
	DATACOPY(noisefrequency);
	DATACOPY(noisedimention);
	DATACOPY(width);
	DATACOPY(venToper);

	DATACOPY(disgrowfactor);
	DATACOPY(debugShowVessel);

	// ������������� � Start
	DATACOPY(startGrowPoint);
	DATACOPY(endGrowPoint);
	DATACOPY(growdir);
	DATACOPY(growpoints);

	// ��������
	DATACOPY(xynoisemagnitude);
	DATACOPY(xynoisemagnitudeSubGrow);
	DATACOPY(xynoisefrequency);
	DATACOPY(xynoisedimention);
	DATACOPY(deformToper);
	DATACOPY(xynoisemagnitudemin);


	// render
	DATACOPY(texToper);
	DATACOPY(shaderId);
}
void CurveControl::serialize(Util::Stream& stream)
{
	IFFControl::serialize(stream);

	int version = 14;
	stream >> version;
	if( version>=0)
	{
		SERIALIZE(bWindow);
		SERIALIZE(bDeformer);
		SERIALIZE(bGrow);

		// ��������� ����� �������� � init
		SERIALIZE(growlevel);
		SERIALIZE(vessels);
		SERIALIZE(maxdist);
		SERIALIZE(trueway_propencity);
		SERIALIZE(curve);

		// ��������� ����
		SERIALIZE(windowparam);
		SERIALIZE(time);
		SERIALIZE(noisemagnitude);
		SERIALIZE(noisefrequency);
		SERIALIZE(disgrowfactor);

		// ������������� � Start
		SERIALIZE(startGrowPoint);
		SERIALIZE(endGrowPoint);
		SERIALIZE(growdir);
		SERIALIZE(growpoints);
		// ��������
		SERIALIZE(xynoisemagnitude);
		SERIALIZE(xynoisefrequency);
	}
	if( version>=1)
	{
		SERIALIZE(bInvertDirection);
	}
	if( version>=2)
	{
		SERIALIZE(noisedimention);
		SERIALIZE(xynoisedimention);
		SERIALIZE(debugShowVessel);
	}
	if( version>=3)
	{
		SERIALIZE(width);
	}
	if( version>=4)
	{
		SERIALIZE(growfrequency);
	}
	if( version>=5)
	{
		SERIALIZE(venToper);
	}
	if( version>=6)
	{
		SERIALIZE(SubGrowPropencity);
	}
	if( version>=7)
	{
		SERIALIZE(startSeed);
		SERIALIZE(seed);
		SERIALIZE(SubGrowPhaseOffset);
	}
	if( version>=8)
	{
		SERIALIZE(SubGrowHistoryOffset);
		SERIALIZE(SubGrowWidthFactor);
		SERIALIZE(WidthFactorJitter);
	}
	if( version>=9)
	{
		SERIALIZE(xynoisemagnitudeSubGrow);
	}
	if( version>=10)
	{
		SERIALIZE(maxdistJitter);
	}
	if( version>=11)
	{
		SERIALIZE(deformToper);
		SERIALIZE(texToper);
	}
	if( version>=12)
	{
		SERIALIZE(maxDistRamp);
	}
	if( version>=13)
	{
		SERIALIZE(shaderId);
	}
	if( version>=14)
	{
		SERIALIZE(xynoisemagnitudemin);
	}
}

#define SERIALIZEOCS(ARG) if( bSave) render->Parameter( (name+"::"#ARG).c_str(),this->##ARG); else render->GetParameter( (name+"::"#ARG).c_str(),this->##ARG);

// ��������� � ocs
void CurveControl::SerializeOcs(
	const char* _name, 
	cls::IRender* render, 
	bool bSave
	)
{
	std::string name = _name;

	SERIALIZEOCS( bWindow);
	SERIALIZEOCS( windowparam);
	SERIALIZEOCS( time);
	SERIALIZEOCS( noisemagnitude);
	SERIALIZEOCS( xynoisemagnitudeSubGrow);
	SERIALIZEOCS( noisefrequency);
	SERIALIZEOCS( noisedimention);
	SERIALIZEOCS( disgrowfactor);
	SERIALIZEOCS( debugShowVessel);
	SERIALIZEOCS( width);
	SERIALIZEOCS( venToper);
	SERIALIZEOCS( SubGrowWidthFactor);
	SERIALIZEOCS( WidthFactorJitter);

	SERIALIZEOCS( bInvertDirection);			// � �������� �����������
	SERIALIZEOCS( vessels);					// ����� ������
	SERIALIZEOCS( maxdist);					// ��������� �� ������
	SERIALIZEOCS( maxdistJitter);
	SERIALIZEOCS( trueway_propencity);		// ����������� ����� � ���������� �����������
	SERIALIZEOCS( growlevel);

	SERIALIZEOCS( bDeformer);
	SERIALIZEOCS( xynoisemagnitude);
	SERIALIZEOCS( xynoisemagnitudeSubGrow);
	SERIALIZEOCS( xynoisefrequency);
	SERIALIZEOCS( xynoisedimention);
	SERIALIZEOCS(xynoisemagnitudemin);

	SERIALIZEOCS( growdir);
	SERIALIZEOCS( growid);

	SERIALIZEOCS(deformToper);
	SERIALIZEOCS(texToper);
	SERIALIZEOCS(shaderId);

	if(bSave)
	{
		render->Parameter( (name+"::curve").c_str(), 
			cls::PA<Math::Vec3f>( cls::PI_CONSTANT, this->curve));
		render->Parameter( (name+"::startGrowPoint").c_str(),	
			cls::PA<int>(cls::PI_CONSTANT, (int*)&startGrowPoint, 4));
		render->Parameter( (name+"::endGrowPoint").c_str(),	
			cls::PA<int>(cls::PI_CONSTANT, (int*)&endGrowPoint, 4));
	}
	else
	{
		cls::PA<int> _startGrowPoint, _endGrowPoint;
		render->GetParameter( (name+"::startGrowPoint").c_str(),	_startGrowPoint);
		render->GetParameter( (name+"::endGrowPoint").c_str(),		_endGrowPoint);
		startGrowPoint = MathOld::GridCellId(_startGrowPoint.data());
		endGrowPoint = MathOld::GridCellId(_endGrowPoint.data());

		cls::PA<Math::Vec3f> _curve;
		render->GetParameter( (name+"::curve").c_str(),	_curve);
		_curve.data(this->curve);
	}
}

#define READMAYAFLOAT(ARG) {cls::P<float> pf = node->getAttribute(#ARG, cls::P<float>(this->##ARG) );if( !pf.empty()) this->##ARG = pf.data();}
#define READMAYAINT(ARG) {cls::P<int> pf = node->getAttribute(#ARG, cls::P<int>(this->##ARG) );if( !pf.empty()) this->##ARG = pf.data();}

void CurveControl::ReadAttributes(IOcsData* node)
{
	READMAYAINT(shaderId);
	READMAYAFLOAT(xynoisemagnitudemin);
}
