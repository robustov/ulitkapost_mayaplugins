#pragma once

#include "Math/Math.h"
#include "IFFControl.h"
#include "furFractalMath.h"
#include "GridData.h"
#include "Math/Ramp.h"

#pragma warning (disable: 4251 4275)
struct FURFRACTALMATH_API CurveControl : public MathOld::IFFControl
{
	// ��������� ����� �������� � init
	bool bGrow;
	int growlevel;					// ������� �� ������� ������
	bool bInvertDirection;			// � �������� �����������
	int vessels;					// ����� ������
	float maxdist;					// ��������� �� ������
	float maxdistJitter;			// ������� ���������
	Math::Ramp maxDistRamp;
	std::vector<Math::Vec3f> curve;	// ���������� ������
	float growfrequency;			// ������� ��� ������������
	float SubGrowPropencity;		// ����������� ������� ��������� �.�. >1
	int startSeed, seed;			// startSeed �������� seed �������� ��� Step
	// ���������
	float SubGrowPhaseOffset;		// �������� ���� ��� ���������
	int SubGrowHistoryOffset;		// ����������� ������ ��� ���������
	// ������������� � Start
	MathOld::GridCellId startGrowPoint, endGrowPoint;
	int growdir;

	// ��������� ����
	bool bWindow;
	float windowparam;
	float time;						// ����� (��� ����)
	float noisemagnitude;
	float noisefrequency;
	float noisedimention;
	float disgrowfactor;
	int debugShowVessel;
	float width;					// ������
	float WidthFactorJitter;		// jitter ������
	float venToper;
	float SubGrowWidthFactor;		// ��������� ������ ��� ���������

	// deformer
	bool bDeformer;
	float xynoisemagnitude, xynoisefrequency, xynoisedimention, xynoisemagnitudemin;
	float xynoisemagnitudeSubGrow;
	float deformToper;

	// render
	float texToper;
	int shaderId;

	// ��������!!!
	float trueway_propencity;		// ����������� ����� � ���������� �����������(��������!!!)

	// �������������� � step
	struct GrowPoint
	{
		GrowPoint();
		MathOld::GridCellId id;	// ������� �������
		int lastDirection;		// ����������� ����� �� ����. ����
		float phase;			// ���� ������������ �����
		float lenght;			// ������� �����
		int seed;				// seed
		// ��� ������������� ���������
		float growtime;		
		Math::Vec2i idint;		// id �� ����������!!!
		Math::Vec2f idfloat;	// ������ �� float ���� �� ���� ������
		Math::Vec3f rndposition;
		float freq;				// ���� ������� ������ ����� ��������
		float maxdist;			// ���� ������� ������ ���
		std::list<MathOld::GridCellId> history;
	};
	// ������� ����� �����
	std::vector< GrowPoint> growpoints;	
	// ���������������� ����� � phase ����� ������� �������������
	std::map<MathOld::GridCellId, float> modified_verts;

public:
	CurveControl();
protected:
	// ������ ��� �������� �������
	struct GrowNeibourData
	{
		Math::Vec3f offset;
		int count;
		bool bRoot;
		GrowNeibourData():offset(0),count(0),bRoot(false){};
	};
	void Start_rec(
		const Math::Matrix4f& ffworldmatrixinv, 
		MathOld::Grid& grid, 
		MathOld::GridCellId id1, 
		MathOld::GridCellId id2, 
		int cv1, 
		int cv2, 
		int maxLevel, 
		std::map<MathOld::GridCellId, GrowNeibourData>& neibourdata
		);

	// �������� ����� ��� ������� ������ � �����.���������
	float getGrowParam(
		const Math::Vec3f& v, 
		const MathOld::GridCellId& id, 
		float lenght, 
		float phase, 
		float toper, 
		Math::Vec2i& offsetNormal
		);

	float getWidth(Math::Vec3f& v, const MathOld::GridCellId& id, float lenght, float phase);

public:
	virtual bool isWindow()const{return bWindow;};
	virtual bool isGrow()const{return bGrow;};
	virtual bool isDeformer()const{return bDeformer;};

	virtual void copy(const IFFControl* arg);
	virtual void serialize(Util::Stream& stream);
	virtual void SerializeOcs(const char* _name, cls::IRender* render, bool bSave);
	virtual void ReadAttributes(IOcsData* node);

public:
	virtual float processEdge(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata,				// �������
		const MathOld::IFFEdgeData& edge, 
		float pointonedge,					// ����� �� ����� (�� 0 �� 1)
		float src_width
		);
	// ���������� �����.���������� ��� ����� �� �����
	virtual bool getTexCoord(
		float& texCoord, 
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata,					// �������
		const MathOld::IFFEdgeData& edge,			// �������� �����
		float pointonedge,					// ����� �� ����� (�� 0 �� 1)
		int texCoordType					// �� ������� ���� ��������� UV ����� ���� ������
		);

	//����� ��������� ����� ��� ������������ ����� �����
	virtual bool getMainPoint(
		const MathOld::GridCellId& v1,	//����������� ����� v1->v2
		const MathOld::GridCellId& v2,
		const std::list<MathOld::GridCellId>& v3list,	// ��������� ����� ���. ������� �� ������� v2
		GridData& griddata,
		MathOld::GridCellId& mainPoint //��������� �����, ���� ��������� true
		);
	// ��� ��������� �� ��������
	virtual void draw(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata
		);

public:
	// ��������� �������
	virtual void Start(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata, 
		std::list<MathOld::IFFEdgeData>& added
		);
	// ��� ���������
	virtual void Step(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata, 
		std::list<MathOld::IFFEdgeData>& added
		);
protected:
	// ��� ��������� ����� ����� �����
	void StartGrowPoint2(
		GrowPoint& gp, 
		int g,
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata, 
		std::list<MathOld::IFFEdgeData>& added
		);
	bool StepGrowPoint2(
		GrowPoint& gp, 
		int g,
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata, 
		std::list<MathOld::IFFEdgeData>& added
		);
	// ���������� ������� ����
	void StepSubGrowPoint2(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata, 
		std::list<MathOld::IFFEdgeData>& added
		);

	// ����� �� �������� �����
	bool canAddEdge(
		MathOld::GridFreeEdge& edge, 
		float phase,
		GridData& griddata, 
		std::list<MathOld::IFFEdgeData>& added
		);
	// ����������� xy � ��������� ����� � ����������
	Math::Vec3i localXYtoWorld(const Math::Vec2i arg, const Math::Vec3i& src);
	// ����������� ���������� � xy � ��������� �����
	Math::Vec2i world2localXY(const Math::Vec3i arg, int& growlen);

	// ��� 2d
	Math::Vec2f perlin2f(const Math::Vec3f& position, float time, float maxdist, float noisefrequency);

public:
	/*/
	virtual bool bind(
		Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
		GridData& griddata
		);
	/*/
	virtual bool deform(
		const MathOld::Grid& grid, 
		const MathOld::GridCellId& id, 
		Math::Vec3f& v, 
		Math::Vec3f& offset,				// �������� �������� (������ ������������ ��� �����������)
		const MathOld::GridDeformerCell& deformercell,
		const Math::Matrix4f& worldMatrix);
	// ������������� ������������� �������
	virtual void deform(
		const MathOld::Grid& grid, 
		const MathOld::GridCellId& id,
		const Math::Matrix4f& worldMatrix,
		Math::Vec3f& v);

	// ���
	Math::Vec3f getNoise(float noisemagnitude, float noisefrequency, const Math::Vec2i& xy, float z, int seed);
};


template <class STREAM>
STREAM& operator >> (STREAM& serializer, CurveControl::GrowPoint& v)
{
	unsigned char version = 3;
	serializer >> version;
	if( version>=0)
	{
		serializer >> v.id;
		serializer >> v.lastDirection;
		serializer >> v.phase;
	}
	if( version>=1)
	{
		serializer >> v.growtime;		
		serializer >> v.idint;		// id �� ����������!!!
		serializer >> v.idfloat;	// ������ �� float ���� �� ���� ������
		serializer >> v.rndposition;
		serializer >> v.freq;
	}
	if( version>=2)
	{
		serializer >> v.history;
	}
	if( version>=3)
	{
		serializer >> v.lenght;
	}
	return serializer;
}
