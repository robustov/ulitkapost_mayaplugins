#include "StdAfx.h"
#include ".\meshcontrol.h"
#include "math\rotation3.h"
#include "Math/Bresenham.h"
#include <gl/gl.h>
#include "MathNGl/MathNGl.h"
#include "Math/meshutil.h"

Math::PerlinNoise MeshControl::perlinnoise(321);

Math::Vec3f rotate(Math::Vec3f ort, float ang, const Math::Vec3f& axis)
{
	Math::Matrix4f basis;
	Math::Rot3f(ang, axis).getMatrix(basis);
	ort = basis*ort;
	return ort;
}

extern "C"
{
	__declspec(dllexport) MathOld::IFFControl* __cdecl meshControl()
	{
		return new MeshControl();
	}
}

MeshControl::MeshControl(void)
{
	disgrowfactor = 0;
	startVenCount = 0;
	segLenght = 10;
	segLenJitter = 0;
	curl = 0;
	curlJitter = 0;
	startOffset = 0;
	surfaceOffset = 0;
	surfaceOffsetJitter = 0;
	growFrequency = 10;
	forkAngle = 45.f;
	forkAngleJitter = 0.5f;
	stopIfCross = 0;
	maxSegCount = 2000;				// ������������ ����� ���������
	growFromOther = false;			// ���������� �� ������ ������
	maxGeneration = 3;
	growFromHairStartPhaseJitter = 0;
	texResolution = 128;
	filletRadius = 2.f;

	xynoisemagnitude = 1;
	xynoisemagnitudemin = 0;

	// ���� sub
	curl2 = 30; curl2jitter = 0;
	forkAngle2 = 90; forkAngle2jitter=0.0f;
	length2 = 2; length2jitter=0;
	density2 = 0.0f;
	subgrowPhaseOffset = 1.f;
	maxGeneration2 = 2;

	shaderId = 0;
}

MeshControl::~MeshControl(void)
{
}

MeshControl::GrowPoint::GrowPoint()
{
//	lastDirection = Math::Vec3i(0);
	pt = Math::Vec3f(0);
	dir = Math::Vec3f(0);
	curl = 0;
	phase = 0;			// ���� ������������ �����
	lenght = 0;			// ������� �����
	seed = 0;				// seed
	bStop = false;
//	prevedge
	generation = 0;

	// ��� ����������
	filletRadius=0;		// ������� ���� ���������� (���� 0 - ��� ����������)
	filletParam=0;
}

// ���������� �� �������� ����� �� ����������� 
// ������ ����� �� �����������
float MeshControl::getClosestPoint(
	MathOld::Grid& grid, 
	const Math::Vec3f& pt, 
	pointOnControlMesh& res
	)
{
	// ��� ������ mesh ������ maxProjectionCount ��������
	std::list<pointOnControlMesh> projections;
	// �������� �� �����
	for(unsigned f=0; f<faces.size(); f++)
	{
		const Math::Face32& face = faces[f];
		//*/
		Math::Vec3f Px[4];
		for(int v=0; v<3; v++)
		{
			// ������������ ����� v, v+1
			Math::Vec3f Vn = vertnormals[face.cycle(v)];
			Math::Vec3f Fn = Math::cross( verts[face.cycle(v+1)]-verts[face.cycle(v)], verts[face.cycle(v-1)]-verts[face.cycle(v)]);
			Fn.normalize();
			float dist = Math::dot( pt-verts[face.cycle(v)], Fn);


			float fdot = Math::dot( Fn, Vn);
			Px[v] = Vn*(dist/fdot) + verts[face.cycle(v)];
		}
		// ��������� ����� Px[3]
		Math::Vec3f FnTest = Math::cross( Px[2]-Px[1], Px[0]-Px[1]);
		FnTest.normalize();
		float distTest = Math::dot( pt-Px[1], FnTest);
		Px[3] = Px[2] + FnTest;

		//���������������� ���������� ��� pt � ������������ Px[]
		Math::Vec4f bary4 = Math::CalcBaryCoords3d(Px, pt);

		Math::Vec3f bary(bary4.x, bary4.y, bary4.z);
		if( bary[0]>=0 && bary[0]<=1 && 
			bary[1]>=0 && bary[1]<=1 && 
			bary[2]>=0 && bary[2]<=1)
		{
			Math::Vec3f pr = bary[0]*verts[face[0]] + bary[1]*verts[face[1]] + bary[2]*verts[face[2]];
			float dist = (pr - pt).length();

			pointOnControlMesh ponm(dist, f, face, bary);
			projections.push_back(ponm);
		}
		/*/
		// ������� �����
		// dot �� ��������� � ����� �����
		float hs[3], esq[3], sumsq=0;
		for(int v=0; v<3; v++)
		{
			// ������������ ����� v, v+1
			Math::Edge32 edge(face.cycle(v), face.cycle(v+1));
			Math::Vec3f& en = edgenormals[edge];
			Math::Vec3f& v1 = verts[face.cycle(v+1)];
			Math::Vec3f& v0 = verts[face.cycle(v)];
			Math::Vec3f& ev = v1 - v0;
			Math::Vec3f platen = Math::cross(en, ev).normalized();	// ������� ��������� �����
			Math::Vec3f pv = pt - verts[face.cycle(v)];
			float h = Math::dot(platen, pv);	// �������� ����� �� ��������� �����
			if( fabs(h)<1e-5) h=0;
			hs[v] = h;
			esq[v] = ev.length()*h;
			sumsq += esq[v];
		}
		if( (hs[0]<=0 && hs[1]<=0 && hs[2]<=0) ||
			(hs[0]>=0 && hs[1]>=0 && hs[2]>=0))
		{
			// ������ ��������
			Math::Vec3f bary(esq[1]/sumsq, esq[2]/sumsq, esq[0]/sumsq);
			float sum = bary.x + bary.y + bary.z;
            // ��������
			Math::Vec3f pr = bary[0]*verts[face[0]] + bary[1]*verts[face[1]] + bary[2]*verts[face[2]];
			float dist = (pr - pt).length();

			pointOnControlMesh ponm(dist, f, face, bary);
			projections.push_back(ponm);
		}
		/*/
	}
	// �������� �� �������� �����
	{
		for( unsigned e=0; e<openedges.size(); e++)
		{
			const OpenEdge& ope = openedges[e];
			const Math::Edge32& edge = ope.edge;

			Math::Vec3f& v1 = verts[edge.v1];
			Math::Vec3f& v2 = verts[edge.v2];
			Math::Vec3f n1 = vertnormals[edge.v1];
			Math::Vec3f n2 = vertnormals[edge.v2];
			Math::Vec3f v_1 = v1 - verts[ope.v_1];
			Math::Vec3f v2_ = verts[ope.v2_] - v2;
			Math::Vec3f v12 = v2-v1;
			Math::Vec3f v1X = v12.normalized()+v_1.normalized();
			Math::Vec3f v2X = v12.normalized()+v2_.normalized();
			
			Math::Vec3f plate1 = Math::cross( Math::cross(v1X, n1), n1).normalized();
			Math::Vec3f plate2 = -Math::cross( Math::cross(v2X, n2), n2).normalized();

			float w1 = Math::dot(plate1, pt-v1);
			float w2 = Math::dot(plate2, pt-v2);
			if( fabs(w1)<1e-5) w1=0;
			if( fabs(w2)<1e-5) w2=0;

			if( (w1>=0 && w2>=0) || (w1<=0 && w2<=0))
			{
				if( (w1 + w2)==0)
					continue;
				float param = w1/(w2+w1);

				Math::Vec3f pr = v1*(1-param) + v2*param;
				float dist = (pr - pt).length();

				pointOnControlMesh ponm(dist, edge.v1, edge.v2, 1-param, param);
				projections.push_back(ponm);
			}
		}
	}
	// ����� �����������
	float mindist = 1e38f;
	std::list<pointOnControlMesh>::iterator itl = projections.begin();
	for(;itl != projections.end(); itl++)
	{
		if(mindist<itl->distance)
			continue;
		mindist = itl->distance;
		res = *itl;
	}
	return res.distance;
}

// ����� �� �����������
Math::Vec3f MeshControl::getPoint(
	const pointOnControlMesh& ponm
	)
{
	const float* bary = ponm.w;
	const int* face = ponm.v;
	Math::Vec3f pr = bary[0]*verts[face[0]] + bary[1]*verts[face[1]] + bary[2]*verts[face[2]];
	Math::Vec3f n = bary[0]*vertnormals[face[0]] + bary[1]*vertnormals[face[1]] + bary[2]*vertnormals[face[2]];


	pr += this->startOffset * n.normalized();

	return pr;
}
// ����� �� �����������
Math::Vec3f MeshControl::getNormal(
	const pointOnControlMesh& ponm
	)
{
	/*/
	if(ponm.face<0 || ponm.face>=(int)faces.size())
	{
		return Math::Vec3f(1, 0, 0);
	}
	Math::Face32& face = faces[ponm.face];

	Math::Vec3f Fn = Math::cross( verts[face.cycle(1)]-verts[face.cycle(0)], verts[face.cycle(-1)]-verts[face.cycle(0)]);
	Fn.normalize();
	return Fn;
	/*/

	if( vertnormals.size()!=verts.size())
		return Math::Vec3f(1, 0, 0);
	const float* bary = ponm.w;
	const int* face = ponm.v;
	Math::Vec3f n = bary[0]*vertnormals[face[0]] + bary[1]*vertnormals[face[1]] + bary[2]*vertnormals[face[2]];
	n.normalize();
	return n;
}
// ����� ����������
Math::Vec2f MeshControl::getUv(
	const pointOnControlMesh& ponm
	)
{
	if( vertsUv.size()!=verts.size())
		return Math::Vec2f(0.5f, 0.5f);

	const float* bary = ponm.w;
	const int* face = ponm.v;
	Math::Vec2f uv = bary[0]*vertsUv[face[0]] + bary[1]*vertsUv[face[1]] + bary[2]*vertsUv[face[2]];

	return uv;
}

// ����� ����������
float MeshControl::getGrowSpeed(
	const pointOnControlMesh& ponm
	)
{
	Math::Vec2f uv = this->getUv(ponm);
	if( this->growSpeed.empty())
		return 1.f;
	float v = this->growSpeed.interpolation(uv.x, uv.y, false, false);
	if(v<0.01f) v=0.01f;
	return v;
}

// �������� ����� ��� ������� ������ � �����.���������
float MeshControl::getGrowParam(
	const Math::Vec3f& v, 
	const MathOld::GridCellId& id, 
	float lenght, 
	float phase, 
	float toper
	)
{
	float w = this->windowparam - lenght;

	float wfactor = 1000000.f;
	if(toper>0)
		wfactor = 1/toper;

	w -= phase*this->disgrowfactor;
	w *= wfactor;
	return w;
}



float MeshControl::processEdge(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata,				// �������
	const MathOld::IFFEdgeData& edgedata, 
	float pointonedge,					// ����� �� ����� (�� 0 �� 1)
	float src_width
	)
{
	if( edgedata.growId != this->growid)
		return 0;

	const MathOld::GridFreeEdge& edge = edgedata.edge;
	MathOld::Grid& grid = griddata.grid;
	srand(edgedata.seed);

	Math::Vec3f v1 = grid.getCellPos(edge.v1());
	Math::Vec3f v2 = grid.getCellPos(edge.v2());
	Math::Vec3f v = v1*(1-pointonedge) + v2*pointonedge;
	
	float w = this->getGrowParam(v, edge.v1(), edgedata.lenght, edgedata.phase, this->venToper);
	float wfe = getGrowParam(v, edge.v1(), this->windowparam - edgedata.lenghtFromEnd, 0, this->venToper);
	if(wfe<w)
		w = wfe;

	w = float( atan(w)*2/M_PI);

	return w*src_width*this->width;
	return 1;
}
// ���������� �����.���������� ��� ����� �� �����
bool MeshControl::getTexCoord(
	float& texCoord, 
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata,					// �������
	const MathOld::IFFEdgeData& edgedata,			// �������� �����
	float pointonedge,					// ����� �� ����� (�� 0 �� 1)
	int texCoordType					// �� ������� ���� ��������� UV ����� ���� ������
	)
{
	if( edgedata.growId != this->growid)
		return false;

	if( texCoordType==0)
	{
		const MathOld::GridFreeEdge& edge = edgedata.edge;
		MathOld::Grid& grid = griddata.grid;

		Math::Vec3f v1 = grid.getCellPos(edge.v1());
		Math::Vec3f v2 = grid.getCellPos(edge.v2());
		Math::Vec3f v = v1*(1-pointonedge) + v2*pointonedge;

		float w = getGrowParam(v, edge.v1(), edgedata.lenght, edgedata.phase, this->texToper);
		texCoord = w;
	}
	if( texCoordType==1)
	{
		const MathOld::GridFreeEdge& edge = edgedata.edge;
		MathOld::Grid& grid = griddata.grid;

		texCoord = edgedata.lenght;
	}
	if( texCoordType==2)
	{
		// shaderId
		texCoord = (float)this->shaderId;
	}

	return true;
}

//����� ��������� ����� ��� ������������ ����� �����
bool MeshControl::getMainPoint(
	const MathOld::GridCellId& v1,	//����������� ����� v1->v2
	const MathOld::GridCellId& v2,
	const std::list<MathOld::GridCellId>& v3list,	// ��������� ����� ���. ������� �� ������� v2
	GridData& griddata,
	MathOld::GridCellId& mainPoint //��������� �����, ���� ��������� true
	)
{
	MathOld::GridFreeEdge ed1(v1, v2);
	std::map< GridData::Edge, GridData::EdgeData>::iterator ital = griddata.alives.find(ed1);
	if( ital== griddata.alives.end())
		return false;
	GridData::EdgeData& edata1 = ital->second;

	if( edata1.iffdata.growId != this->growid)
		return false;

	bool bFind = false;
	float curphase = 1e32f;
	float curlenght = 1e32f;

	std::list<MathOld::GridCellId>::const_iterator it = v3list.begin();
	for(;it != v3list.end(); it++)
	{
		const MathOld::GridCellId& v3 = *it;
		if( v3 == v1) 
			continue;

		MathOld::GridFreeEdge ed2(v2, v3);
		ital = griddata.alives.find(ed2);
		if( ital == griddata.alives.end())
			continue;
		GridData::EdgeData& edata2 = ital->second;

		// ��������� 
		if(curphase>edata2.iffdata.phase)
		{
			mainPoint = v3;
			curphase = edata2.iffdata.phase;
			curlenght = edata2.iffdata.lenght;
			bFind = true;
		}
		else if(curphase==edata2.iffdata.phase && curlenght>edata2.iffdata.lenght)
		{
			mainPoint = v3;
			curphase = edata2.iffdata.phase;
			curlenght = edata2.iffdata.lenght;
			bFind = true;
		}
	}
	return bFind;
}
// ��� ��������� �� ��������
void MeshControl::draw(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata
	)
{
	Math::Matrix4f ffworldmatrixinv = ffworldmatrix.inverted();

	bool bDrawVertsNormals = false;
	bool bDrawEdgeNormals = false;
	bool bDrawAliveNormals = false;

	if( bDrawVertsNormals)
	{
		for( int v=0; v<(int)vertnormals.size(); v++)
		{
			Math::Vec3f n = vertnormals[v];

			Math::Vec3f pt1 = this->verts[ v];
			Math::Vec3f pt2 = pt1+n;

			pt1 = ffworldmatrixinv*pt1;
			pt2 = ffworldmatrixinv*pt2;
			drawLine(pt1, pt2);
		}
	}
	// �������
	if( bDrawEdgeNormals)
	{
		std::map< Math::Edge32, Math::Vec3f>::iterator it = this->edgenormals.begin();
		for( ; it != this->edgenormals.end(); it++)
		{
			const Math::Edge32& edge = it->first;
			Math::Vec3f n = it->second;

			Math::Vec3f pt1 = (this->verts[ edge.v1] + this->verts[ edge.v2])*0.5f;
			Math::Vec3f pt2 = pt1+n;

			pt1 = ffworldmatrixinv*pt1;
			pt2 = ffworldmatrixinv*pt2;
			drawLine(pt1, pt2);
		}
	}
	if(bDrawAliveNormals)
	{
		GridData* pThis = &griddata;
		std::map<GridData::Edge, GridData::EdgeData>::iterator it = pThis->alives.begin();
		for(;it != pThis->alives.end(); it++)
		{
			const GridData::Edge& edge = it->first;
			const GridData::EdgeData& edgedata = it->second;

			Math::Vec3f v1 = pThis->grid.getCellPos( pThis->grid.addCell(edge.v1()));
			Math::Vec3f v2 = pThis->grid.getCellPos( pThis->grid.addCell(edge.v2()));

			{
				pointOnControlMesh ponm;
				this->getClosestPoint(pThis->grid, v1, ponm);
				Math::Vec3f n = this->getPoint(ponm);

				Math::Vec3f pt1 = ffworldmatrixinv*v1;
				Math::Vec3f pt2 = ffworldmatrixinv*n;
				drawLine(pt1, pt2);
			}
			{
				pointOnControlMesh ponm;
				this->getClosestPoint(pThis->grid, v2, ponm);
				Math::Vec3f n = this->getPoint(ponm);

				Math::Vec3f pt1 = ffworldmatrixinv*v2;
				Math::Vec3f pt2 = ffworldmatrixinv*n;
				drawLine(pt1, pt2);
			}
		}
	}
}
// ������������ �����������
float MeshControl::genCurl()
{
	float r = rnd()-0.5f;
	float curl = 2*this->curlJitter * r;
	if( r>0)
		curl += (1-this->curlJitter);
	else
		curl -= (1-this->curlJitter);
	curl = (float)( curl * this->curl * M_PI/180.f);
	return curl;
}
float MeshControl::perlin(const Math::Vec3f& position, float time, float magnitude, float noisefrequency)
{
	Math::Vec3f noisearg1(
		time*noisefrequency, 
		position.y,	// + time*noisefrequency+(float)M_PI, 
		position.z);	// + time*noisefrequency-(float)M_PI);

	// ���
	float nx = perlinnoise.noise(noisearg1.x, noisearg1.y, noisearg1.z);
	nx = nx*magnitude;

	return nx;
}

// ��������� �������
void MeshControl::GrowStartPoint(
	Math::Vec3f startPoint, 
	MathOld::GridCellId startid,	// ��� ����� ���� = null
	float startPhase, 
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata, 
	int startVenCount,
	float filletRadius,			// ������ ����������
	std::list<MathOld::IFFEdgeData>& added, 
	bool bDump
	)
{
	float startAng = rnd();
	Math::Matrix4f ffworldmatrixinv = ffworldmatrix.inverted();
	// ����������� �����
	for(int i=0; i<startVenCount; i++)
	{
		int level = this->growlevel;
		Math::Vec3f pt = startPoint;
		if( startid.isValid())
		{
			pt = griddata.grid.getCellPos(startid);	// ffs
			pt = ffworldmatrix*pt;	// ws
		}

		pointOnControlMesh ponm;
		float d = this->getClosestPoint(griddata.grid, pt, ponm);
		Math::Vec3f proj = this->getPoint(ponm);
		Math::Vec3f n = this->getNormal(ponm);
//		proj += n*this->startOffset;
		// �� ��������!!!
		if( ponm.face < 0)
			continue;		

		if( !startid.isValid())
		{
			pt = proj;
			Math::Vec3f ffpt = ffworldmatrixinv*pt;
			startid = griddata.grid.findClosestVertex(ffpt, level);
		}
		Math::Vec3f ffpt = ffworldmatrixinv*pt;

		// ������� ��������� ����������� �����
		Math::Vec3f ort = Math::cross(n, Math::Vec3f(1, 1, 0));
		float ang = startAng + i/(float)(startVenCount);
		ang += 0.5f*(rnd()-0.5f)/startVenCount;
		ang = (float)( ang*M_PI*2.f);
		ort = rotate(ort, ang, n);
		ort.normalize();
	float test = Math::dot(n, ort);
		
		GrowPoint gp;
		if( filletRadius!=0)
		{
			// ��������� ������ ����������
//			gp.filletRadius = (i+1)*filletRadius/(startVenCount);
			gp.filletRadius = filletRadius;
			gp.filletParam = gp.filletRadius;
		}

		gp.id = startid;
		gp.pt = pt;
		gp.dir = ort;
		gp.curl = this->genCurl();
		gp.phase = startPhase;
		gp.lenght = 0;
		gp.seed = rand();
		this->growpoints.push_back(gp);

GrowPoint& tgp = this->growpoints.back();

		MathOld::GridCell& cell = griddata.grid.addCell( gp.id);
		if( cell.offset==Math::Vec3f(0))
			cell.offset = ffpt-griddata.grid.getCellPos(cell);

		// deformer2
		{
			MathOld::GridCell& cell = griddata.grid.addCell(gp.id);
			MathOld::GridDeformerCell& def = cell.addGridDeformerCell(this->growid+1);
			def.w = 1;
			def.bindmapping[0] = (float)ponm.face;
			def.bindmapping[1] = ponm.w[0];
			def.bindmapping[2] = ponm.w[1];
			def.bindmapping[3] = d;
		}
		if( bDump)
		{
			printf("Start Grow Point: %f %f %f\n", gp.pt.x, gp.pt.y, gp.pt.z);
			printf("                  %d (%d %d %d)\n", gp.id.level, gp.id.x, gp.id.y, gp.id.z);
			printf("          offset  %f %f %f\n", cell.offset.x, cell.offset.y, cell.offset.z);
		}
	}
}

// ��������� �������
void MeshControl::Start(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata, 
	std::list<MathOld::IFFEdgeData>& added
	)
{
	this->ReinitGrowId();
	this->growpoints.clear();
	this->growparents.clear();
	this->modified_verts.clear();
	this->otherProcessed.clear();

	this->facesAtStart = this->faces;

	Math::Matrix4f ffworldmatrixinv = ffworldmatrix.inverted();
	srand(this->startSeed);

	if( bGrow)
	{
		if(this->startPositions.empty())
		{
			int level = this->growlevel;
			MathOld::GridCellId id(level, 0, 0, 0);
			MathOld::GridCell& cell = griddata.grid.addCell(id);
			Math::Vec3f pt = ffworldmatrix*griddata.grid.getCellPos(cell);	// ws

			this->GrowStartPoint(
				pt, 
				MathOld::Grid::getNullId(), 
				0, 
				ffworldmatrix,		// world ������� �������� 
				griddata, 
				this->startVenCount, 
				0.f,		/// ������ ����������
				added);
		}
		else
		{
			for(int i=0; i<(int)this->startPositions.size(); i++)
			{
				Math::Vec3f pt = this->startPositions[i];

				pointOnControlMesh ponm;
				this->getClosestPoint(griddata.grid, pt, ponm);

				float startPhase = growFromHairStartPhaseJitter*rnd();
				if(this->growSpeed.fullsize()!=1)
					startPhase = growFromHairStartPhaseJitter*(1-this->getGrowSpeed(ponm));
				this->GrowStartPoint(
					pt, 
					MathOld::Grid::getNullId(),  
					startPhase, 
					ffworldmatrix,		// world ������� �������� 
					griddata, 
					this->startVenCount, 
					0.f,		/// ������ ����������
					added);
			}
		}
	}

	// ��������
	this->seed = rand();
}
// ����� ���������
void MeshControl::GrowStop(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata, 
	std::list<MathOld::IFFEdgeData>& added
	)
{
	// ��������� lenghtFromEnd
	this->CalcLenghtFromEnd(griddata);
}

#define PRINTF_XYZ(ARG) ARG.x, ARG.y, ARG.z 

// ��� ��������� gp
bool MeshControl::GrowPointStep(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	int g, 
	GrowPoint& gp, 
	GridData& griddata, 
	std::list<MathOld::IFFEdgeData>& added
	)
{
	Math::Matrix4f ffworldmatrixinv = ffworldmatrix.inverted();
	int level = gp.id.level;
	float singlelen = griddata.grid.getLevelDimention(level);

//gp.dir = Math::Vec3f(0, -1, 0);
	Math::Vec3f dir = gp.dir*singlelen;
	// ����� ����� (������������� �� �����������)
	Math::Vec3f ptnew;
	pointOnControlMesh ponm;

	// ��������������� �� �����������
	ptnew = gp.pt + dir;

	float d = this->getClosestPoint(griddata.grid, ptnew, ponm);
	// ��������� �����������
	if( ponm.face==-1)
		return false;

	ptnew = this->getPoint( ponm);
	Math::Vec3f n = this->getNormal( ponm);
	if(gp.filletParam>0)
	{
		gp.filletParam -= singlelen;
		if(gp.filletParam>0)
		{
			float fo = gp.filletParam/gp.filletRadius;
			fo = fo*fo*gp.filletRadius;
			ptnew += fo*n;
		}
	}

	// surfaceOffset + jitter
	float p = gp.phase;
	if(p>1) p=1;
	float surfaceOffset = (1-p)*this->surfaceOffset*(1-this->surfaceOffsetJitter) + p*this->surfaceOffset;
	float offset = this->perlin( Math::Vec3f(0.1337f, (float)M_PI, 0), gp.lenght, surfaceOffset, this->growFrequency);
	offset = fabs( offset);
	ptnew += n*offset;

	// ����� ����� �������
	Math::Vec3f ffpt = ffworldmatrixinv*ptnew;	// ff space
	MathOld::GridCellId idnew = griddata.grid.findClosestVertex(ffpt, level);

	// ������� dir �� curl � ������������������ �� n
	dir = (gp.dir - n*Math::dot(n, gp.dir));
	float ang = gp.curl*singlelen;
	dir = rotate(dir, ang, n);
	dir.normalize();
//float test = Math::dot(n, dir);
//printf("n=%f %f %f dir=%f %f %f\n", n.x, n.y, n.z, PRINTF_XYZ(dir));

	float addlength = singlelen;
	addlength = addlength/(this->getGrowSpeed(ponm));
	if(idnew!=gp.id)
	{
		// ��������
		std::vector<Math::Vec3i> bresenham;
		Math::Bresenham3( gp.id.getXYZ(), idnew.getXYZ(), bresenham);

		if(bresenham.size()!=2)
		{
//			printf( "stop cause bresenhem: %d %d %d %d\n", idnew.level, idnew.x, idnew.y, idnew.z );
//			return false;
		}

		for(int bp=1; bp<(int)bresenham.size(); bp++)
		{
			MathOld::GridCellId prev_id( idnew.level, bresenham[bp-1]);
			MathOld::GridCellId next_id( idnew.level, bresenham[bp]);
			float factor = (bp)/((float)bresenham.size()-1);
			float next_length = gp.lenght + addlength*factor;
			Math::Vec3f next_pt = gp.pt*(1-factor) + ptnew*(factor);
			Math::Vec3f next_ffpt = ffworldmatrixinv*next_pt;	// ff space

			pointOnControlMesh next_ponm;
			float d = this->getClosestPoint(griddata.grid, next_pt, next_ponm);

			// ������ ������?
			if( this->isStopVertex(next_id, gp.phase, next_length))
				return false;

			MathOld::GridFreeEdge edge(prev_id, next_id);
			if( canAddEdge(edge, gp.phase, next_length, griddata, added))
			{
				added.push_back(MathOld::IFFEdgeData(edge, this->growid, gp.phase, g, gp.seed, next_length));
				if( this->canDeformVertex(next_id, gp.phase))
				{
					MathOld::GridCell& cell = griddata.grid.addCell( next_id);
					// deformer
					{
						MathOld::GridDeformerCell& def = cell.addGridDeformerCell(this->growid);
						def.w = 0;
						def.bindmapping = Math::Vec4f(gp.phase, next_length, (float)gp.seed, 0); // phase, threadId, seed
					}
					
					// deformer2
					{
						MathOld::GridDeformerCell& def = cell.addGridDeformerCell(this->growid+1);
						def.w = 1;
						def.bindmapping[0] = (float)next_ponm.face;
						def.bindmapping[1] = next_ponm.w[0];
						def.bindmapping[2] = next_ponm.w[1];
						def.bindmapping[3] = d;
					}

					// offset
					cell.offset = next_ffpt - griddata.grid.getCellPos(cell);

					this->modified_verts[next_id] = VertexData( gp.phase, next_length);
				}

				// parent
				if(gp.prevedge.isValid())
					this->growparents[edge] = gp.prevedge;
			}
			gp.prevedge = edge;
		}
	}
	// 
	gp.id = idnew;
	gp.pt = ptnew;
	gp.dir = dir;
	gp.lenght += addlength;
	return true;
}

// ��������� subgrow
void MeshControl::GrowPoint_NewSubgrow(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GrowPoint& gp, 
	const MathOld::GridCellId& previd, 
	GridData& griddata,
	std::list< GrowPoint>& newgrowpoints
	)
{
	// ���� � ��������
	float fa = this->forkAngle2*(float)M_PI*(1-this->forkAngle2jitter*rnd())/180.f;
	if(rnd()<0.5f) fa = -fa;

	// ����� ����� �����
	MathOld::GridCellId nextid = griddata.grid.getLowCell( gp.id);
//	MathOld::GridCellId id( nextid.level, (nextid.getXYZ()+previd.getXYZ())/2);
	MathOld::GridCellId id = previd;
	GrowPoint subgp;
	subgp.prevedge = gp.prevedge;
	subgp.id = id;
	subgp.pt = ffworldmatrix*griddata.grid.getCellPos(id);	// ws
	subgp.dir = gp.dir;
	// ��������� ����������� �� fa
	pointOnControlMesh ponm;
	this->getClosestPoint(griddata.grid, subgp.pt, ponm);
	Math::Vec3f normal = this->getNormal(ponm);
	subgp.dir = rotate(subgp.dir, fa, normal);

	subgp.curl = this->curl2*( rnd()*2 -1);
	subgp.curl = subgp.curl * (1-rnd()*this->curl2jitter);
	subgp.curl = (float)( subgp.curl * M_PI/180.f);
	subgp.phase = this->subgrowPhaseOffset + gp.phase + rnd();
	subgp.lenght = gp.lenght;
	subgp.seed = rand();
	subgp.bStop = false;
	subgp.generation = 0;
	subgp.filletRadius = gp.filletRadius;
	subgp.filletParam = gp.filletParam;
	newgrowpoints.push_back(subgp);
}

// ��������
bool MeshControl::GrowPointFork(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	int g, 
	GrowPoint& gp, 
	GridData& griddata, 
	std::list< GrowPoint>& newgrowpoints
	)
{
	if( gp.id.level == this->growlevel)
	{
		if( gp.generation >= this->maxGeneration)
			return false;
	}
	else
	{
		if( gp.generation >= this->maxGeneration2)
			return false;
	}

	// ������� (������� �����)
	float fa = this->forkAngle*(float)M_PI*(1-this->forkAngleJitter*rnd())/180.f;
	pointOnControlMesh ponm;
	this->getClosestPoint(griddata.grid, gp.pt, ponm);
	Math::Vec3f normal = this->getNormal(ponm);
	float curl1 = +fabs( this->genCurl());
	float curl2 = -fabs( this->genCurl());
	float phase1 = rnd();
	float phase2 = rnd();
	if(phase1>phase2) 
		phase1=0;
	else
		phase2=0;
	{
		newgrowpoints.push_back(gp);
		GrowPoint& gpnew = newgrowpoints.back();
		gpnew.phase += phase1;
		gpnew.curl = curl1;
		gpnew.seed = rand();
		gpnew.generation++;
		gpnew.dir = rotate(gpnew.dir, fa/2, normal);
	}
	{
		newgrowpoints.push_back(gp);
		GrowPoint& gpnew = newgrowpoints.back();
		gpnew.phase += phase2;
		gpnew.curl = curl2;
		gpnew.seed = rand();
		gpnew.generation++;
		gpnew.dir = rotate(gpnew.dir, -fa/2, normal);
	}
	return true;
}

// ��� ���������
void MeshControl::Step(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata, 
	std::list<MathOld::IFFEdgeData>& added
	)
{
	srand(this->seed);

	if(this->growFromOther)
	{
		// ����� �� �������

		// ����� ������� ������� �� � ������ �����������
		std::map<MathOld::GridFreeEdge, GridData::EdgeData>::iterator it = griddata.alives.begin();
		for(;it != griddata.alives.end(); it++)
		{
			const MathOld::GridFreeEdge& edge = it->first;
			if( edge.level() > this->growlevel)
				continue;
			GridData::EdgeData& edgedata = it->second;

			// ���� ���� ����� - �� ������������ (������ ����� growpoints)
			if( edgedata.iffdata.growId == this->growid)
				continue;

			// ������ �����������
			if( this->otherProcessed.find(edge)!=otherProcessed.end()) 
				continue;

			this->otherProcessed.insert(edge);

			// �������� ����� � ������ this->growLevel
			MathOld::GridCellId idcenter;
			if(edge.level() == this->growlevel)
				idcenter = edge.v1();
			else
			{
				Math::Vec3i i = (edge.v1().getXYZ()+edge.v2().getXYZ());
				idcenter = MathOld::GridCellId(edge.level()+1, i);
				for( ; idcenter.level<this->growlevel; )
					idcenter = griddata.grid.getLowCell(idcenter);
			}

			Math::Vec3f v = griddata.grid.getCellPos(idcenter);
			v = ffworldmatrix*v;

			// fillet radius
			// ��������� ���������� �������� offset �� �����������
			pointOnControlMesh ponm;
			float d = this->getClosestPoint(
				griddata.grid, v, ponm);
			if( ponm.face<0)
				continue;
			Math::Vec3f proj = this->getPoint(ponm);
			Math::Vec3f n = this->getNormal(ponm);
			Math::Vec3f projfillet = proj + n*this->filletRadius;	// ������ ������ ����������
			Math::Vec3f v_proj = v-projfillet;
			if( Math::dot(v_proj, n) <0 )
				continue;
			d = v_proj.length();

			// ������ ����� ������ �����������
			float singlelen = griddata.grid.getLevelDimention(this->growlevel);
			if( fabs(d) > singlelen)
				continue;
			float flr = (v-proj).length();		// �������� ������ ����������
			if(filletRadius<=0) flr = 0;

			MathOld::GridCellId id = idcenter;
			printf("start from other: %f, %f, %f - dist=%f\n", v.x, v.y, v.z, d);
			printf("id: %d (%d, %d, %d)\n", id.level, id.x, id.y, id.z);
			
			this->GrowStartPoint(
				v, 
				id,
				0, 
				ffworldmatrix,		// world ������� �������� 
				griddata, 
				this->startVenCount,
				flr,		// ������ ����������
				added, 
				true);
		}

	}

	// ���� subgrow
	{
		/*/
		std::list< GrowPoint>::iterator it = this->subgrowpoints.begin();
		for( int g=0; it != this->subgrowpoints.end(); it++, g++)
		{
			GrowPoint& gp = *it;
			if( gp.bStop) 
				continue;

			int level = gp.id.level;
			float singlelen = griddata.grid.getLevelDimention(level);
			float seglen = this->length2*(1 - rnd()*this->length2jitter);
			int x = (int)(seglen/singlelen);
			if(x<1)
				continue;

			for(int i=0; i<x; i++)
			{
				MathOld::GridCellId previd = griddata.grid.getLowCell( gp.id);
				if( !this->GrowPointStep(
					ffworldmatrix,		// world ������� �������� 
					g, 
					gp, 
					griddata, 
					added
					))
				{
					gp.bStop = true;
					break;
				}
			}
			gp.bStop = true;
		}
		/*/
	}

	std::list< GrowPoint> newgrowpoints;
	std::list< GrowPoint>::iterator it = this->growpoints.begin();
	for( int g=0; it != this->growpoints.end(); it++, g++)
	{
		GrowPoint& gp = *it;
		if( gp.bStop) 
			continue;

		int level = gp.id.level;
		float singlelen = griddata.grid.getLevelDimention(level);
		float seglen = this->segLenght*(1 - rnd()*this->segLenJitter);
		if( level>this->growlevel)
			seglen = this->length2*(1 - rnd()*this->length2jitter);
		int x = (int)(seglen/singlelen);
		if(x<1)
			continue;

		float subgrow_propencity = this->density2/singlelen;

		for(int i=0; i<x; i++)
		{
			MathOld::GridCellId previd = griddata.grid.getLowCell( gp.id);
			if( !this->GrowPointStep(
				ffworldmatrix,		// world ������� �������� 
				g, 
				gp, 
				griddata, 
				added
				))
			{
				gp.bStop = true;
				break;
			}

			// ��������� subgrow ����� ��� �������� ������;
			if( level==this->growlevel)
			{
				if( rnd()<subgrow_propencity)
				{
					GrowPoint_NewSubgrow(ffworldmatrix, gp, previd, griddata, newgrowpoints);
				}
			}
		}
		if( gp.bStop) 
			continue;

//		if( (int)growpoints.size() < this->maxSegCount)
		{
			this->GrowPointFork(
				ffworldmatrix, g, gp, griddata, newgrowpoints);
		}
		// �������
		gp.bStop = true;

	}
	// ����� �����
	this->growpoints.insert(this->growpoints.end(), newgrowpoints.begin(), newgrowpoints.end());

	this->seed = rand();
}
// ����� �� �������� �����
bool MeshControl::canAddEdge(
	MathOld::GridFreeEdge& edge, 
	float phase,
	float lenght,
	GridData& griddata, 
	std::list<MathOld::IFFEdgeData>& added
	)
{
	// ���� ��� ���� ����� ����� - �� ��������� � ������
	std::map<MathOld::GridFreeEdge, GridData::EdgeData>::iterator ita = griddata.alives.find(edge);
	int beforeme=-1;
	float phasebefore = 0;
	bool bAdd = true;
	if( ita != griddata.alives.end())
	{
		if( ita->second.iffdata.phase<phase)
			bAdd = false;
		if( ita->second.iffdata.lenght<lenght)
			bAdd = false;
		beforeme = ita->second.iffdata.threadId;
		phasebefore = ita->second.iffdata.phase;
	}
	std::list<MathOld::IFFEdgeData>::iterator it = added.begin();
	for(;it != added.end(); it++)
	{
		if(it->edge == edge)
		{
			if( it->phase<phase)
				bAdd = false;
			if( it->lenght<lenght)
				bAdd = false;
			beforeme = it->threadId;
			phasebefore = it->phase;
		}
	}
	return bAdd;
}

// ���������� ����?
// ���� ���� ������ � ����� ������
// ��� ���� ������ � ����� ������
bool MeshControl::isStopVertex(
	MathOld::GridCellId& id, 
	float phase, 
	float lenght
	)
{
	if( id.level>this->growlevel)
	{
//		std::map<MathOld::GridCellId, VertexData>::iterator it = this->modified_verts.find(id);
//		if( it!=this->modified_verts.end())
//			return true;

		MathOld::GridCellId idup = MathOld::Grid::getUpCell(id);
		if( idup.isValid())
		{
			std::map<MathOld::GridCellId, VertexData>::iterator it = this->modified_verts.find(id);
			if( it!=this->modified_verts.end())
				return true;
		}
		return false;
	}
	else
	{
		std::map<MathOld::GridCellId, VertexData>::iterator it = this->modified_verts.find(id);
		if( it==this->modified_verts.end())
			return false;

		if( rnd()<this->stopIfCross)
			return true;

		VertexData& vd = it->second;
		if( phase>vd.phase && lenght<vd.lenght)
			return true;
		if( phase<vd.phase && lenght>vd.lenght)
			return true;
	}

	return false;
}

// ����� �� ������������� �������
bool MeshControl::canDeformVertex(
	MathOld::GridCellId& id, 
	float phase
	)
{
	if( id.level>this->growlevel)
	{
		MathOld::GridCellId idup = MathOld::Grid::getUpCell(id);
		if( idup.isValid())
		{
			std::map<MathOld::GridCellId, VertexData>::iterator it = this->modified_verts.find(idup);
			if( it!=this->modified_verts.end())
				return false;
		}
	}

	std::map<MathOld::GridCellId, VertexData>::iterator it = this->modified_verts.find(id);
	if( it==this->modified_verts.end())
		return true;
	VertexData& vd = it->second;

	if( phase<vd.phase)
		return true;
	if( phase>vd.phase)
		return false;
	return false;
}

// ��������� lenghtFromEnd
void MeshControl::CalcLenghtFromEnd(
	GridData& griddata
	)
{
	// ��� ������� ����� ��������� �� �������� �� 0 ������ ��� � ����� ����� lenghtFromEnd=0
	std::map<MathOld::GridFreeEdge, MathOld::GridFreeEdge>::iterator it = this->growparents.begin();
	for(;it != this->growparents.end(); it++)
	{
		MathOld::GridFreeEdge edge = it->first;
		float lfe = 0;

		// �� �������� ����
		std::set<MathOld::GridFreeEdge> processedeges;
		for(;;)
		{
			// ����� �����
			float edgelen = griddata.grid.getLevelDimention(edge.level());
			float deltalen = edgelen;
			lfe += deltalen;

			std::map< GridData::Edge, GridData::EdgeData>::iterator ital = griddata.alives.find(edge);
			if( ital!=griddata.alives.end())
			{
				GridData::EdgeData& gded = ital->second;
				// ����������� ��� lenghtFromEnd ����������� �� ����������� ������ �������
				gded.iffdata.lenghtFromEnd = __max(lfe, gded.iffdata.lenghtFromEnd);
				lfe = gded.iffdata.lenghtFromEnd;
			}
			processedeges.insert(edge);

			// ����. �����
			std::map<MathOld::GridFreeEdge, MathOld::GridFreeEdge>::iterator itpar = this->growparents.find(edge);
			if( itpar == this->growparents.end())
				break;
			edge = itpar->second;
			if( !edge.isValid())
				break;
			if( processedeges.find(edge)!=processedeges.end())
				break;
		}
	}
}


Math::Vec3f MeshControl::getNoise(
	float noisemagnitude, 
	float noisefrequency, 
	const MathOld::GridCellId& id, 
	int seed)
{
	Math::Vec3f noisearg1(
		id.x/this->xynoisedimention, 
		id.y/this->xynoisedimention+time*noisefrequency, 
		id.z/this->xynoisedimention);

	Math::Vec3f noisearg2(
		id.x/this->xynoisedimention+time*noisefrequency, 
		id.y/this->xynoisedimention, 
		id.z/this->xynoisedimention);

	Math::Vec3f noisearg3(
		id.x/this->xynoisedimention, 
		id.y/this->xynoisedimention, 
		id.z/this->xynoisedimention+time*noisefrequency);

	// ���
	float nx = perlinnoise.noise(noisearg1.x, noisearg1.y, noisearg1.z);
	nx = nx*noisemagnitude;

	float ny = perlinnoise.noise(noisearg2.x, noisearg2.y, noisearg2.z);
	ny = ny*noisemagnitude;

	float nz = perlinnoise.noise(noisearg3.x, noisearg3.y, noisearg3.z);
	nz = nz*noisemagnitude;

	return Math::Vec3f(nx, ny, nz);
}

bool MeshControl::deform(
	const MathOld::Grid& grid, 
	const MathOld::GridCellId& id, 
	Math::Vec3f& v, 
	Math::Vec3f& offset,				// �������� �������� (������ ������������ ��� �����������)
	const MathOld::GridDeformerCell& deformercell,
	const Math::Matrix4f& worldMatrix
	)
{
	if( deformercell.defirmerId == this->growid)
	{
		// ���������� �� ����� ��������
		if( deformercell.defirmerId != this->growid)
			return false;

		float phase = deformercell.bindmapping[0];
		float lenght = deformercell.bindmapping[1];
		float p = this->getGrowParam(v, id, lenght, phase, this->deformToper);
		if( p<0) 
			return false;
		if(p>1) p=1;
		p = 1-p;

		float noisemagnitude = this->xynoisemagnitude;
		float noisemagnitudemin = this->xynoisemagnitudemin;

		float noisefrequency1 = this->xynoisefrequency;
		float noisefrequency2 = this->xynoisefrequency*0.1f;

		Math::Vec3f sh1 = getNoise(noisemagnitude, noisefrequency1, id, seed);
		Math::Vec3f sh2 = getNoise(noisemagnitudemin, noisefrequency2, id, seed);

		offset += sh1*p + sh2*(1-p);
		return true;
	}
	if( deformercell.defirmerId == this->growid+1)
	{
		// ����� �����
		// 1� � 2� ���� ����������
		int f = (int)deformercell.bindmapping[0];
		if(f<0) 
		{
			fprintf(stderr, "error deform %d: f<0\n", deformercell.defirmerId);
			return false;
		}
		if( f >= (int)this->facesAtStart.size())
		{
			fprintf(stderr, "error deform %d: f %d>=%d\n", deformercell.defirmerId, f, (int)this->facesAtStart.size());
			return false;
		}
		float dist = deformercell.bindmapping[3];
		Math::Vec3f bary(deformercell.bindmapping[1], deformercell.bindmapping[2], 0);
		bary[2] = 1 - bary[0] - bary[1];

		Math::Face32& face = this->facesAtStart[f];
		Math::Vec3f P = bary[0]*this->verts[face[0]] + 
			bary[1]*this->verts[face[1]] +
			bary[2]*this->verts[face[2]];
		Math::Vec3f N = bary[0]*this->vertnormals[face[0]] + 
			bary[1]*this->vertnormals[face[1]] +
			bary[2]*this->vertnormals[face[2]];
		N.normalize();
		P += N*dist;

		Math::Matrix4f ffworldmatrixinv = worldMatrix.inverted();
		v = ffworldmatrixinv*P;
		return true;
	}
	return false;
}













////////////////////////////////////////
//
// 
// 

void MeshControl::copy(const IFFControl* _arg)
{
	IFFControl::copy(_arg);

	const MeshControl* arg = (const MeshControl*)_arg;
	DATACOPY(bWindow);
	DATACOPY(windowparam);
	DATACOPY(venToper);
	DATACOPY(time);
	DATACOPY(width);
	DATACOPY(WidthFactorJitter);
	DATACOPY(disgrowfactor);

	DATACOPY(bGrow);
	DATACOPY(growlevel);
	DATACOPY(startSeed);
	DATACOPY(seed);
	DATACOPY(segLenght);
	DATACOPY(segLenJitter);
	DATACOPY(curl);
	DATACOPY(curlJitter);
	DATACOPY(startOffset);
	DATACOPY(surfaceOffset);
	DATACOPY(surfaceOffsetJitter);
	DATACOPY(growFrequency);
	DATACOPY(forkAngle);
	DATACOPY(forkAngleJitter);
	DATACOPY(stopIfCross);
	DATACOPY(startPositions);
	DATACOPY(maxSegCount);
	DATACOPY(growFromOther);
	DATACOPY(maxGeneration);
	DATACOPY(growFromHairStartPhaseJitter);
	DATACOPY(growSpeed);
	DATACOPY(texResolution);
	DATACOPY(filletRadius);
	// ���� sub
	DATACOPY2(curl2, curl2jitter);
	DATACOPY2(forkAngle2, forkAngle2jitter);
	DATACOPY2(length2, length2jitter);
	DATACOPY(density2);
	DATACOPY(subgrowPhaseOffset);
	DATACOPY(maxGeneration2);

	// mesh
	DATACOPY(verts);
	DATACOPY(vertnormals);			
	DATACOPY2(faces, facesAtStart);
	DATACOPY(edgenormals);
	DATACOPY(openedges);
	DATACOPY2(facenormals, vertsUv);

	DATACOPY(bDeformer);
	DATACOPY(xynoisemagnitude);
	DATACOPY(xynoisefrequency);
	DATACOPY(xynoisedimention);
	DATACOPY(xynoisemagnitude);
	DATACOPY(xynoisefrequency);
	DATACOPY(xynoisedimention);
	DATACOPY(xynoisemagnitudemin);
	DATACOPY(deformToper);

	DATACOPY(texToper);
	DATACOPY(shaderId);

	DATACOPY(growpoints);
	DATACOPY(growparents);
	DATACOPY(modified_verts);
	DATACOPY(otherProcessed);

	DATACOPY(startVenCount);

}
void MeshControl::serialize(Util::Stream& stream)
{
	IFFControl::serialize(stream);

	int version = 11;
	stream >> version;
	if( version>=0)
	{
		SERIALIZE(bWindow);
		SERIALIZE(windowparam);
		SERIALIZE(venToper);
		SERIALIZE(time);
		SERIALIZE(width);
		SERIALIZE(WidthFactorJitter);

		SERIALIZE(bGrow);
		SERIALIZE(growlevel);
		SERIALIZE(startSeed);
		SERIALIZE(seed);

		// mesh
		SERIALIZE(verts);
		SERIALIZE(vertnormals);			
		SERIALIZE(faces);
		SERIALIZE(edgenormals);
		SERIALIZE(openedges);

		SERIALIZE(bDeformer);
		SERIALIZE(xynoisemagnitude);
		SERIALIZE(xynoisefrequency);
		SERIALIZE(xynoisedimention);
		SERIALIZE(deformToper);

		SERIALIZE(texToper);
	}
	if(version >=1)
	{
		SERIALIZE(disgrowfactor);
		SERIALIZE(segLenght);
		SERIALIZE(segLenJitter);
		SERIALIZE(curl);
		SERIALIZE(curlJitter);
		SERIALIZE(surfaceOffset);
	}
	if(version >=2)
	{
		SERIALIZE(growFrequency);
		SERIALIZE(forkAngle);
		SERIALIZE(forkAngleJitter);
	}
	if(version >=3)
	{
		SERIALIZE(surfaceOffsetJitter);
	}
	if(version >=4)
	{
		SERIALIZE(stopIfCross);
	}
	if(version >=5)
	{
		SERIALIZE(startPositions);
	}
	if(version >=6)
	{
		SERIALIZE(maxSegCount);
		SERIALIZE(growFromOther);
		SERIALIZE(startOffset);
	}
	if(version >=7)
	{
		// ���� sub
		SERIALIZE(curl2);
		SERIALIZE(curl2jitter);
		SERIALIZE(forkAngle2);
		SERIALIZE(forkAngle2jitter);
		SERIALIZE(length2);
		SERIALIZE(length2jitter);
		SERIALIZE(density2);
		SERIALIZE(subgrowPhaseOffset);
		SERIALIZE(xynoisemagnitudemin);
		SERIALIZE(maxGeneration);
		SERIALIZE(maxGeneration2);
	}
	if(version>=8)
	{
		SERIALIZE(facesAtStart);
	}
	else
	{
		facesAtStart = faces;
	}
	if(version>=9)
	{
		SERIALIZE(startVenCount);
	}
	if(version>=10)
	{
		SERIALIZE(shaderId);
	}
	if(version>=11)
	{
		SERIALIZE(growFromHairStartPhaseJitter);
		SERIALIZE(growSpeed);
		SERIALIZE(texResolution);
		SERIALIZE(facenormals);
		SERIALIZE(vertsUv);
		SERIALIZE(filletRadius);
	}


}
#define SERIALIZEOCS(ARG) if( bSave) render->Parameter( (name+"::"#ARG).c_str(),this->##ARG); else render->GetParameter( (name+"::"#ARG).c_str(),this->##ARG);

void MeshControl::SerializeOcs(const char* _name, cls::IRender* render, bool bSave)
{
	std::string name = _name;

	SERIALIZEOCS( growid);

	SERIALIZEOCS(bWindow);
	SERIALIZEOCS(windowparam);
	SERIALIZEOCS(venToper);
	SERIALIZEOCS(time);
	SERIALIZEOCS(width);
	SERIALIZEOCS(WidthFactorJitter);
	SERIALIZEOCS(disgrowfactor);

	SERIALIZEOCS(bGrow);
	SERIALIZEOCS(growlevel);
	SERIALIZEOCS(startSeed);
	SERIALIZEOCS(seed);
	SERIALIZEOCS(segLenght);
	SERIALIZEOCS(segLenJitter);
	SERIALIZEOCS(curl);
	SERIALIZEOCS(curlJitter);
	SERIALIZEOCS(surfaceOffset);
	SERIALIZEOCS(surfaceOffsetJitter);
	SERIALIZEOCS(growFrequency);
	SERIALIZEOCS(forkAngle);
	SERIALIZEOCS(forkAngleJitter);
	SERIALIZEOCS(stopIfCross);
	// ���� sub
	SERIALIZEOCS(curl2);
	SERIALIZEOCS(curl2jitter);
	SERIALIZEOCS(forkAngle2);
	SERIALIZEOCS(forkAngle2jitter);
	SERIALIZEOCS(length2);
	SERIALIZEOCS(length2jitter);
	SERIALIZEOCS(density2);
	SERIALIZEOCS(subgrowPhaseOffset);

	// mesh
//	SERIALIZEOCS(P);
//	SERIALIZEOCS(N);			
//	SERIALIZEOCS(faces);

	SERIALIZEOCS(bDeformer);
	SERIALIZEOCS(xynoisemagnitude);
	SERIALIZEOCS(xynoisefrequency);
	SERIALIZEOCS(xynoisedimention);
	SERIALIZEOCS(xynoisemagnitudemin);
	SERIALIZEOCS(deformToper);

	SERIALIZEOCS(texToper);
	SERIALIZEOCS(shaderId);

	if( bSave)
	{
		
		render->Parameter((name+"::P").c_str(), cls::PA<Math::Vec3f>(cls::PI_VERTEX, verts));
		render->Parameter((name+"::N").c_str(), cls::PA<Math::Vec3f>(cls::PI_VERTEX, vertnormals));
		cls::PA<int> _faces(cls::PI_PRIMITIVEVERTEX);
		_faces.reserve( (int)this->facesAtStart.size()*3);
		for(int i=0; i<(int)this->facesAtStart.size(); i++)
		{
			_faces.push_back( this->facesAtStart[i][0]);
			_faces.push_back( this->facesAtStart[i][1]);
			_faces.push_back( this->facesAtStart[i][2]);
		}
		render->Parameter((name+"::faces").c_str(), _faces);
	}
	else
	{
		cls::PA<Math::Vec3f> P = render->GetParameter((name+"::P").c_str());
		P.data(this->verts);
		cls::PA<Math::Vec3f> N = render->GetParameter((name+"::N").c_str());
		N.data(this->vertnormals);

		cls::PA<int> _faces = render->GetParameter((name+"::faces").c_str());
		this->facesAtStart.clear();
		this->facesAtStart.reserve( _faces.size()/3);
		for(int i=0; i<_faces.size(); )
		{
			this->facesAtStart.push_back( Math::Face32());
			Math::Face32& f = this->facesAtStart.back();
			f.v[0] = _faces[i++];
			f.v[1] = _faces[i++];
			f.v[2] = _faces[i++];
		}
	}
//	std::map< Math::Edge32, Math::Vec3f> edgenormals;	// ������� �����
//	std::vector<OpenEdge> openedges;		// �������� �����

}

#define READMAYAFLOAT(ARG) {cls::P<float> pf = node->getAttribute(#ARG, cls::P<float>(this->##ARG) );if( !pf.empty()) this->##ARG = pf.data();}
#define READMAYAINT(ARG) {cls::P<int> pf = node->getAttribute(#ARG, cls::P<int>(this->##ARG) );if( !pf.empty()) this->##ARG = pf.data();}

void MeshControl::ReadAttributes(IOcsData* node)
{
	// ���� sub
	READMAYAFLOAT(curl2);
	READMAYAFLOAT(curl2jitter);
	READMAYAFLOAT(forkAngle2);
	READMAYAFLOAT(forkAngle2jitter);
	READMAYAFLOAT(length2);
	READMAYAFLOAT(length2jitter);
	READMAYAFLOAT(density2);
	READMAYAFLOAT(subgrowPhaseOffset);
	READMAYAINT(maxGeneration2);

	READMAYAINT(maxGeneration);

	READMAYAFLOAT(xynoisemagnitudemin);

	READMAYAINT(shaderId);

	READMAYAFLOAT(growFromHairStartPhaseJitter);
	READMAYAFLOAT(filletRadius);
}
