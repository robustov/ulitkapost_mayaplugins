//
// Copyright (C) 
// File: FFDrawCacheCmd.cpp
// MEL Command: FFDrawCache

#include "FFDrawCache.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <sstream>


FFDrawCache::FFDrawCache()
{
}
FFDrawCache::~FFDrawCache()
{
}

bool subdivEdge( MathOld::Grid& grid, const MathOld::GridEdge& edge, std::vector<MathOld::GridCellId>& cells);
bool subdivEdge( MathOld::Grid& grid, const MathOld::GridFreeEdge& edge, std::vector<MathOld::GridCellId>& cells);
float assembleTexCoord(
	std::vector<MathOld::IFFWindow*>& ffds, 
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata,					// �������
	const MathOld::IFFEdgeData& edge,			// �������� �����
	float pointonedge,					// ����� �� ����� (�� 0 �� 1)
	int texCoordType					// �� ������� ���� ��������� UV ����� ���� ������
	);

void FFDrawCache::clear()
{
	this->lines.clear();
	this->invisiblepoints.clear();
}

void FFDrawCache::Build(
	GridData* pThis,
	std::vector<MathOld::IGridDeformer*>& deforms,
	std::vector<MathOld::IFFWindow*>& ffds, 
	Math::Matrix4f& worldm, 
	float widthScaleValue
	)
{
	this->clear();
	this->invisiblepoints.reserve(10000);

	pThis->grid.timestamp++;
	pThis->grid.setUpDeformers(worldm, &deforms[0], (int)deforms.size());

	std::map<GridData::Edge, GridData::EdgeData>::iterator it = pThis->alives.begin();
	for(;it != pThis->alives.end(); it++)
	{
		const GridData::Edge& edge = it->first;
		const GridData::EdgeData& edgedata = it->second;

		Math::Vec3f v1 = pThis->grid.getCellPos( pThis->grid.addCell(edge.v1()));
		Math::Vec3f v2 = pThis->grid.getCellPos( pThis->grid.addCell(edge.v2()));

		float srcw = pow(2.f, -edge.level());
		srcw = srcw*(float)widthScaleValue;
		float w = 0;
		for(int f=0; f<(int)ffds.size(); f++)
		{
 			float pointonedge = 0.5f;
			float cw = ffds[f]->processEdge(worldm, *pThis, edgedata.iffdata, pointonedge, srcw);
			if( cw<=0) continue;

			w = __max(w, cw);
		}

		if(w<0.01f)
		{
			Math::Vec3f cavg = (v1+v2)*0.5f;
//			cavg = worldm*cavg;

			invisiblepoints.push_back(cavg);
			continue;
		}

		int iw = (int)( floor(w+0.5f));
		if(iw<1) iw=1;
		if(iw>6) iw=6;

		Line& line = this->lines[iw];

		std::vector<MathOld::GridCellId> cells;
		subdivEdge( pThis->grid, edge, cells);
		std::vector<Math::Vec3f> P, C;
		P.reserve(cells.size()*2);
		C.reserve(cells.size()*2);

		for(int i=1; i<(int)cells.size(); i++)
		{
			MathOld::GridCellId id1 = cells[i-1];
			MathOld::GridCellId id2 = cells[i];
			Math::Vec3f pos1 = pThis->grid.getCellPos(id1);
			Math::Vec3f pos2 = pThis->grid.getCellPos(id2);
			P.push_back(pos1);
			P.push_back(pos2);

			{
				float pointOnedge1 = (i-1)/(float)(cells.size()-1);
				float pointOnedge2 = i/(float)(cells.size()-1);
				float uv1 = assembleTexCoord(
					ffds, worldm, *pThis, edgedata.iffdata, pointOnedge1, 0);
				float uv2 = assembleTexCoord(
					ffds, worldm, *pThis, edgedata.iffdata, pointOnedge2, 0);
				C.push_back(Math::Vec3f(uv1));
				C.push_back(Math::Vec3f(uv2));
			}
		}
		line.P.insert(line.P.end(), P.begin(), P.end());
		line.C.insert(line.C.end(), C.begin(), C.end());
	}

	pThis->grid.setUpDeformers(Math::Matrix4f(1), NULL, 0);
}

void FFDrawCache::draw(
	bool bViewGridDots,
	bool colorize
	)
{

	std::map<int, Line>::iterator it = lines.begin();
	for(;it != lines.end(); it++)
	{
		Line& line = it->second;

		glLineWidth((float)it->first);

		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer( 3, GL_FLOAT, 0, &line.P[0]);
		if(colorize)
		{
			glEnableClientState(GL_COLOR_ARRAY);
			glColorPointer( 3, GL_FLOAT, 0, &line.C[0]);
		}

		glDrawArrays(GL_LINES, 0, line.P.size());
		if(colorize)
			glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
	}

	if( bViewGridDots)
	{
		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer( 3, GL_FLOAT, 0, &this->invisiblepoints[0]);
		glDrawArrays(GL_POINTS, 0, invisiblepoints.size());
		glDisableClientState(GL_VERTEX_ARRAY);
	}
}

MTypeId FFDrawCache::typeId() const
{
	return FFDrawCache::id;
}

MString FFDrawCache::name() const
{ 
	return FFDrawCache::typeName; 
}

void* FFDrawCache::creator()
{
	return new FFDrawCache();
}

void FFDrawCache::copy( const MPxData& other )
{
	const FFDrawCache* arg = (const FFDrawCache*)&other;

	DATACOPY(lines);
	DATACOPY(invisiblepoints);
}

MStatus FFDrawCache::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("FFDrawCache: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus FFDrawCache::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus FFDrawCache::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus FFDrawCache::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void FFDrawCache::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
//		stream >> ;
	}
}

