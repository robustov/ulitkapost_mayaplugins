#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"

#include "Math/GridOld.h"
#include "IFFWindow.h"
#include "IFFGrow.h"
#include "Math/GridData.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", FFDrawCache::id );
	}
/*/


class FFDrawCache : public MPxData
{
public:
	FFDrawCache();
	virtual ~FFDrawCache(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

	void draw(
		bool bViewGridDots,
		bool bColor
		);

public:
	// ������ ����� �� 6 - ����� � ����
	struct Line
	{
		std::vector<Math::Vec3f> P;
		std::vector<Math::Vec3f> C;
	};
	
	std::map<int, Line> lines;
	std::vector<Math::Vec3f> invisiblepoints;

	void clear();
	void Build(
		GridData* pThis,
		std::vector<MathOld::IGridDeformer*>& deforms,
		std::vector<MathOld::IFFWindow*>& ffds, 
		Math::Matrix4f& worldm,
		float widthScaleValue
		);

public:
	static MTypeId id;
	static const MString typeName;

protected:
	void serialize(Util::Stream& stream);
};
