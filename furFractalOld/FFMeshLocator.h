#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: FFMeshLocatorNode.h
//
// Dependency Graph Node: FFMeshLocator

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include <maya/MFnMesh.h> 

#include "FFControlData.h"
#include "Math/MeshControl.h"

class FFMeshLocator : public MPxNode
{
public:
	FFMeshLocator();
	virtual ~FFMeshLocator(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	///
	virtual MStatus setDependentsDirty( const MPlug& plug, MPlugArray& plugArray);

	static void* creator();
	static MStatus initialize();

	MStatus Build_Control( const MPlug& plug, MDataBlock& data );

public:

	static MObject i_window;
		static MObject i_time;
		static MObject i_windowposition;
		static MObject i_venToper;
		static MObject i_width, i_WidthFactorJitter;
		static MObject disgrowfactor;

	static MObject i_grow;
		static MObject i_mesh;
		static MObject i_level;
		static MObject i_seed;
		static MObject startVenCount;
		static MObject segLenght, segLenJitter;
		static MObject curl, curlJitter;
		static MObject startOffset, surfaceOffset, surfaceOffsetJitter;
		static MObject growFrequency;
		static MObject forkAngle; 
		static MObject forkAngleJitter;
		static MObject stopIfCross;
		static MObject startPositions;
		static MObject maxSegCount;
		static MObject growFromOther;
		static MObject invertNormals;
		static MObject texResolution;
		static MObject growSpeed;

	static MObject i_deformer;
		static MObject i_xynoisemagnitude;
		static MObject i_xynoisefrequency;
		static MObject i_xynoisedimention;
		static MObject deformToper;


	// render
	static MObject texToper;
	static MObject shaderId;

	static MObject o_control;

public:
	bool ReadMesh(
		MeshControl* pThis, 
		MFnMesh& mesh, 
		bool invertNormals
		);

public:
	static MTypeId id;
	static const MString typeName;
};

