#include "pre.h"
#include <maya/MFnPlugin.h>
#include "furFractal.h"
#include "FFSphereLocator.h"
#include "FFGridData.h"
#include "ocellaris/ocellarisExport.h"
#include "FFCurveLocator.h"
#include "FFControlData.h"
#include "FFMeshLocator.h"
#include "FFDrawCache.h"

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

// #include "FFDrawCache.h"
	stat = plugin.registerData( 
		FFDrawCache::typeName, 
		FFDrawCache::id, 
		FFDrawCache::creator);
	if (!stat) 
	{
		displayString("cant register %s", FFDrawCache::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}


// #include "FFGridData.h"
	stat = plugin.registerData( 
		FFGridData::typeName, 
		FFGridData::id, 
		FFGridData::creator);
	if (!stat) 
	{
		displayString("cant register %s", FFGridData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "FFControlData.h"
	stat = plugin.registerData( 
		FFControlData::typeName, 
		FFControlData::id, 
		FFControlData::creator);
	if (!stat) 
	{
		displayString("cant register %s", FFControlData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "FFMeshLocator.h"
	stat = plugin.registerNode( 
		FFMeshLocator::typeName, 
		FFMeshLocator::id, 
		FFMeshLocator::creator,
		FFMeshLocator::initialize );
	if (!stat) 
	{
		displayString("cant register %s", FFMeshLocator::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "FFCurveLocator.h"
	stat = plugin.registerNode( 
		FFCurveLocator::typeName, 
		FFCurveLocator::id, 
		FFCurveLocator::creator,
		FFCurveLocator::initialize, 
//		MPxNode::kLocatorNode 
		MPxNode::kDependNode
		);
	if (!stat) 
	{
		displayString("cant register %s", FFCurveLocator::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "FFSphereLocator.h"
	stat = plugin.registerNode( 
		FFSphereLocator::typeName, 
		FFSphereLocator::id, 
		FFSphereLocator::creator,
		FFSphereLocator::initialize, 
//		MPxNode::kLocatorNode 
		MPxNode::kDependNode
		);
	if (!stat) 
	{
		displayString("cant register %s", FFSphereLocator::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "furFractal.h"
	stat = plugin.registerNode( 
		furFractal::typeName, 
		furFractal::id, 
		furFractal::creator,
		furFractal::initialize, 
		MPxNode::kLocatorNode );
	if (!stat) 
	{
		displayString("cant register %s", furFractal::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	registerGenerator( furFractal::id, &furFractal::generator);


//	if( !MGlobal::sourceFile("furFractalMenu.mel"))
//	{
//		displayString("error source AEmyCmdTemplate.mel");
//	}
//	else
	{
//		if( !MGlobal::executeCommand("furFractalDebugMenu()"))
//		{
//			displayString("Dont found furFractalDebugMenu() command");
//		}
		MGlobal::executeCommand("furFractal_RegistryPlugin()");
	}
	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterData( FFDrawCache::id );
	if (!stat) displayString("cant deregister %s", FFDrawCache::typeName.asChar());

	stat = plugin.deregisterData( FFControlData::id );
	if (!stat) displayString("cant deregister %s", FFControlData::typeName.asChar());

	stat = plugin.deregisterData( FFGridData::id );
	if (!stat) displayString("cant deregister %s", FFGridData::typeName.asChar());

	stat = plugin.deregisterNode( FFMeshLocator::id );
	if (!stat) displayString("cant deregister %s", FFMeshLocator::typeName.asChar());

	stat = plugin.deregisterNode( FFCurveLocator::id );
	if (!stat) displayString("cant deregister %s", FFCurveLocator::typeName.asChar());

	stat = plugin.deregisterNode( FFSphereLocator::id );
	if (!stat) displayString("cant deregister %s", FFSphereLocator::typeName.asChar());

	unregisterGenerator( furFractal::id, &furFractal::generator);
	stat = plugin.deregisterNode( furFractal::id );
	if (!stat) displayString("cant deregister %s", furFractal::typeName.asChar());


	MGlobal::executeCommand("furFractalDebugMenuDelete()");
	return stat;
}
