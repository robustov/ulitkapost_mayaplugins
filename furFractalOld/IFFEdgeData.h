#pragma once

namespace MathOld
{

// ���������� � �����
// ��������� � IFFGrow::Start � Step
// ������������ IFFWindow
struct IFFEdgeData
{
	MathOld::GridFreeEdge edge;
	int growId;				// ��� ��������?
	int threadId;			// ����������?
	int seed;				// seed
	float phase;			// ���������� ����������� �� ����
	float lenght;			// ����� �� ������ (�������������)
	float lenghtFromEnd;	// ����� �� ����� (�������������)

	IFFEdgeData()
	{
		seed = 0;
		lenght = 0;
	}
	IFFEdgeData(const MathOld::GridFreeEdge& edge, int growId, float phase=0, int threadId=0, int seed=0, float lenght=0, float lenghtFromEnd=-1)
	{
		this->edge = edge;
		this->growId = growId;
		this->threadId = threadId;
		this->phase = phase;
		this->seed = seed;
		this->lenght = lenght;
		this->lenghtFromEnd = lenghtFromEnd;
	}
};


// ���������� � �����
// ��������� � IFFGrow::Start � Step
// ������������ IFFWindow
struct IFFEdgeDataOld
{
	MathOld::GridEdge edge;
	int growId;				// ��� ��������?
	int threadId;			// ����������?
	float phase;			// ���������� ����������� �� ����
};
}

template<class Stream> inline
Stream& operator >> (Stream& out, MathOld::IFFEdgeData& v)
{
	unsigned char version = 4;
	out >> version;
	if( version>=0)
	{
		out >> v.edge;
		out >> v.growId;		// ��� ��������?
		out >> v.phase;			// ���������� ����������� �� ����
	}
	if( version>=1)
	{
		out >> v.threadId;
	}
	if( version>=2)
	{
		out >> v.seed;
	}
	if( version>=3)
	{
		out >> v.lenght;
	}
	if( version>=4)
	{
		out >> v.lenghtFromEnd;
	}
	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, MathOld::IFFEdgeDataOld& v)
{
	unsigned char version = 1;
	out >> version;
	if( version>=0)
	{
		out >> v.edge;
		out >> v.growId;		// ��� ��������?
		out >> v.phase;			// ���������� ����������� �� ����
	}
	if( version>=1)
	{
		out >> v.threadId;
	}
	return out;
}
