#pragma once

#include "ocellaris/IRender.h"

struct IOcsData
{
	virtual cls::Param getAttribute(
		const char* name, 
		cls::Param& defvalue)
	{
		return cls::Param();
	};
};

