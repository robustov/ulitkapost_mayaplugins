set BIN_DIR=.\out\furFractal
set BIN_DIR=\\server\bin

rem SN
mkdir %BIN_DIR%
mkdir %BIN_DIR%\bin
mkdir %BIN_DIR%\mel
mkdir %BIN_DIR%\slim
mkdir %BIN_DIR%\docs
mkdir %BIN_DIR%\python

%BIN_DIR%\bin\svn\svn.exe -m "" commit %BIN_DIR%

copy binrelease85\furFractal.mll		%BIN_DIR%\bin
copy binrelease85\furFractalMath.dll	%BIN_DIR%\bin

copy python\ffCommands.py				%BIN_DIR%\python

copy mel\furFractalMenu.mel				%BIN_DIR%\mel
copy mel\AEfurFractalTemplate.mel		%BIN_DIR%\mel
copy mel\AEFFCurveLocatorTemplate.mel	%BIN_DIR%\mel
copy mel\AEFFSphereLocatorTemplate.mel	%BIN_DIR%\mel
copy mel\AEFFMeshLocatorTemplate.mel	%BIN_DIR%\mel

pause
