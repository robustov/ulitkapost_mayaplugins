//
// Copyright (C) 
// File: furFractalCmd.cpp
// MEL Command: furFractal

#include "furFractal.h"
#include <maya/MAnimControl.h>
#include <maya/MDagPath.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnMeshData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnNumericData.h>
#include <maya/MFnMatrixData.h>
#include <maya/MFileIO.h>
#include "Util/misc_create_directory.h"

#include "MathNgl/MathNgl.h"
#include "MathNmaya/MathNmaya.h"
#include "MathNmaya/MayaProgress.h" 
#include "FFGridData.h"
#include "FFControlData.h"
#include "ocellaris/renderCacheImpl.h"
#include "Util/evaltime.h"


struct Point_Deformer : public MathOld::IGridDeformer
{
	Math::Matrix4f base_inv;
	Math::Matrix4f current;
	virtual Math::Vec3f deform(const Math::Vec3f& src, const Math::Vec3f& bindmapping)
	{
		Math::Vec3f v = base_inv*src;
		v = current*v;
		return v;
	}
};

MObject furFractal::i_maxStep[2];
MObject furFractal::i_seed;
MObject furFractal::o_output;
MObject furFractal::o_drawcache;
MObject furFractal::o_mesh;

MObject furFractal::i_widthScale;

MObject furFractal::i_controls;
MObject furFractal::i_controlHistories;

MObject furFractal::i_randomOffset;

MObject furFractal::i_viewGridDots;
MObject furFractal::i_viewGrid;
MObject furFractal::i_viewGridLevel;
MObject furFractal::i_viewGridPoint;
MObject furFractal::i_drawBox;
MObject furFractal::i_color;

MObject furFractal::i_dimention;

MObject furFractal::i_renderType;
MObject furFractal::i_renderWidth;
MObject furFractal::i_tesselation;

// ����������
MObject furFractal::o_venCount, furFractal::o_segCount, furFractal::o_estimationTime;


furFractal::furFractal()
{
}
furFractal::~furFractal()
{
}

MStatus furFractal::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	if( plug == o_output )
	{
		return Build_Grid(plug, data);
	}
	if( plug == o_mesh )
	{
		return Build_OutMesh(plug, data);
	}
	if( plug == o_drawcache )
	{
		return Build_DrawCache(plug, data);
	}
	

	return MS::kUnknownParameter;
}
// ����� ���� ��������� ��� �����
MStatus furFractal::shouldSave( const MPlug& plug, bool& result )
{
	if( plug==o_output)
	{
		displayStringD("HairControlsAnim::shouldSave %s", plug.name().asChar());

		MDataBlock data = forceCache();
		FFGridData* ffgriddata = Load_Grid(data);

		std::string filename = MFileIO::currentFile().asChar();
		std::string mbname = Util::extract_filename(filename.c_str());
		filename = Util::extract_foldername(filename.c_str());
		filename += ".ffCache//";
		filename += mbname;
		filename += ".";
		std::string dgNodeName = MFnDependencyNode(thisMObject()).name().asChar();
		Util::correctFileName( (char*)dgNodeName.c_str());
		filename += dgNodeName;
		filename += ".ff";

		ffgriddata->filename = filename;

		result = true;
		return MS::kSuccess;
	}
	return MPxNode::shouldSave( plug, result);
}

MStatus furFractal::Build_Grid(
	const MPlug& plug, MDataBlock& data)
{
	displayStringD( "furFractal::Build_Grid");
	MStatus stat;

	typedef FFGridData CLASS;
	CLASS* pData = NULL;

	MDataHandle outputData = data.outputValue(plug);
	bool bNewData = false;
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<CLASS*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( CLASS::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<CLASS*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}
	pData->bModify = true;

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}
MStatus furFractal::Build_DrawCache(
	const MPlug& plug, MDataBlock& data)
{
	displayStringD( "furFractal::Build_DrawCache");
	MStatus stat;

	typedef FFDrawCache CLASS;
	CLASS* pData = NULL;

	MDataHandle outputData = data.outputValue(plug);
	bool bNewData = false;
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<CLASS*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( CLASS::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<CLASS*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	FFGridData* pFFGridData = Load_Grid(data);
	GridData* gridData = &pFFGridData->data;

	std::vector<MathOld::IGridDeformer*> deforms;
	LoadDeformers( data, deforms);
	std::vector<MathOld::IFFWindow*> ffds;
	LoadWindows(data, ffds);

	Math::Matrix4f worldpos;
	{
		MPlug plugwms(thisMObject(), this->worldMatrix);
		MPlug plugwm = plugwms.elementByLogicalIndex( 0 );

		MObject mval;
		plugwm.getValue(mval);
		MFnMatrixData se(mval);
		MMatrix m = se.matrix();
		::copy(worldpos, m);
	}
	double widthScaleValue = data.inputValue(i_widthScale).asDouble();

	bool viewGrid = data.inputValue(i_viewGrid).asBool();
	bool drawBox  = data.inputValue(i_drawBox).asBool();

	pData->clear();

	if( !viewGrid && !drawBox)
	{
		pData->Build(gridData, deforms, ffds, worldpos, (float)widthScaleValue);
	}

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

// ��������� ������ ���������
void furFractal::RenderProxy(cls::IRender* render)
{
	MDataBlock data = this->forceCache();
	FFGridData* pFFGridData = Load_Grid(data);
	GridData* gridData = &pFFGridData->data;

	std::vector<MathOld::IGridDeformer*> deforms;
	LoadDeformers( data, deforms);
	std::vector<MathOld::IFFWindow*> ffds;
	LoadWindows(data, ffds);

	Math::Matrix4f worldm;
	{
		MPlug plugwm(thisMObject(), this->worldMatrix);
		MObject mval;
		plugwm.getValue(mval);
		MFnMatrixData se(mval);
		MMatrix m = se.matrix();
		::copy(worldm, m);
	}

	GridData* pThis = gridData;

	pThis->grid.timestamp++;
	pThis->grid.setUpDeformers(worldm, &deforms[0], (int)deforms.size());

	int hilevel = 10000;
	int edgecount = 0;
	std::map<GridData::Edge, GridData::EdgeData>::iterator it = pThis->alives.begin();
	for(;it != pThis->alives.end(); it++)
	{
		const GridData::Edge& edge = it->first;
		const GridData::EdgeData& edgedata = it->second;
		if( edge.level() < hilevel)
		{
			hilevel = edge.level();
			edgecount = 0;
		}
		if( edge.level() == hilevel)
			edgecount++;
	}

	cls::PA<Math::Vec3f> P(cls::PI_VERTEX);
	cls::PA<int> nverts(cls::PI_PRIMITIVE);
	P.reserve(edgecount*2);
	nverts.reserve(edgecount);

	Math::Box3f scenebox;
	it = pThis->alives.begin();
	for(;it != pThis->alives.end(); it++)
	{
		const GridData::Edge& edge = it->first;
		const GridData::EdgeData& edgedata = it->second;

		if( hilevel!=edge.level())
			continue;
		
		Math::Vec3f v1 = pThis->grid.getCellPos( pThis->grid.addCell(edge.v1()));
		Math::Vec3f v2 = pThis->grid.getCellPos( pThis->grid.addCell(edge.v2()));

		P.push_back(v1);
		P.push_back(v2);
		nverts.push_back(2);
		scenebox.insert(v1);
		scenebox.insert(v2);
	}

	pThis->grid.setUpDeformers(Math::Matrix4f(1), NULL, 0);


	render->Attribute("file::box", scenebox);
	render->PushAttributes();
	render->Parameter("#width", 1);
	render->Parameter("#curves::nverts", nverts);
	render->Parameter("P", P);
	render->Parameter("#curves::interpolation", "linear");
	render->Parameter("#curves::wrap", "nonperiodic");
	render->SystemCall(cls::SC_CURVES);
	render->PopAttributes();
}


MStatus furFractal::Build_OutMesh(
	const MPlug& plug, MDataBlock& data)
{
	MStatus status;
	MFnMeshData dataCreator;
	MDataHandle outputHandle = data.outputValue(plug, &status);

	MObject newOutputData = dataCreator.create(&status);

	FFGridData* ffGridData = Load_Grid(data);
	if( !ffGridData) return MS::kFailure;
	GridData& griddata = ffGridData->data;

	std::vector<MathOld::IFFWindow*> ffws;
	std::vector<MathOld::IFFGrow*> ffgr;
	std::vector<MathOld::IGridDeformer*> ffds;

	this->LoadWindows( data, ffws);
//	this->LoadGrows( data, ffgr);
	this->LoadDeformers( data, ffds);

	MFnDagNode dagFn( thisMObject() );
	MDagPath path;
	dagFn.getPath( path );
	Math::Matrix4f worldFFMatrix;
	::copy( worldFFMatrix, path.inclusiveMatrix() );

	float renderWidth = (float)data.inputValue(i_renderWidth).asDouble();
	int tesselation = data.inputValue(i_tesselation).asInt();

   	griddata.grid.timestamp=0;
	griddata.grid.setUpDeformers( worldFFMatrix, &ffds[0], (int)ffds.size() );

	cls::renderCacheImpl<> cache;
	if( !griddata.Render(worldFFMatrix, ffws, 2, renderWidth, tesselation, &cache, true))
		return MS::kSuccess;

	griddata.grid.setUpDeformers( Math::Matrix4f::id, NULL, 0 );

	cls::PA<int> nverts, verts;
	cls::PA<Math::Vec3f> P, N;
	cls::PA<float> s, t, u_toper, v_toper, u_length, v_length;

	cache.GetParameter("P", P );
	cache.GetParameter("N", N );
	cache.GetParameter("s", s );
	cache.GetParameter("t", t );
	cache.GetParameter("mesh::nverts", nverts);
	cache.GetParameter("mesh::verts", verts);
	cache.GetParameter("u_toper", u_toper );
	cache.GetParameter("v_toper", v_toper );
	cache.GetParameter("u_toper", u_toper );
	cache.GetParameter("v_toper", v_toper );
	cache.GetParameter("u_length", u_length );
	cache.GetParameter("v_length", v_length );

	MPointArray vertexArray(P.size());
	MIntArray polygonCounts(nverts.size()), polygonConnects(verts.size()), uvFaces(verts.size());
	MFloatArray uArray(s.size()), vArray(t.size()), u_t(u_toper.size()), v_t(v_toper.size()), u_l(u_length.size()), v_l(v_length.size());
	int i;
	for(i=0; i<P.size(); i++)
		::copy( vertexArray[i], P[i]);
	for(i=0; i<nverts.size(); i++)
		polygonCounts[i] = nverts[i];
	for(i=0; i<verts.size(); i++)
	{
		polygonConnects[i] = verts[i];
		uvFaces[i]=i;
	}
	for(i=0; i<s.size(); i++)
		uArray[i] = s[i];
	for(i=0; i<t.size(); i++)
		vArray[i] = 1-t[i];
	for(i=0; i<u_toper.size(); i++)
		u_t[i] = u_toper[i];
	for(i=0; i<v_toper.size(); i++)
		v_t[i] = 1-v_toper[i];
	for(i=0; i<u_length.size(); i++)
		u_l[i] = u_length[i];
	for(i=0; i<v_length.size(); i++)
		v_l[i] = v_length[i];

	// ���������� cache � mesh
	MFnMesh	meshFn;
	MObject newMesh = meshFn.create(
		vertexArray.length(), polygonCounts.length(), vertexArray,
		polygonCounts, polygonConnects, 
		newOutputData, &status);
	if( newMesh.isNull())
		return MS::kSuccess;

	{
		MString uvname = "st";
		meshFn.createUVSetDataMeshWithName(uvname);
		meshFn.setUVs(uArray, vArray, &uvname);
		meshFn.assignUVs(polygonCounts, uvFaces, &uvname);
	}
	{
		MString uvname = "toper";
		meshFn.createUVSetDataMeshWithName(uvname);
		meshFn.setUVs(u_t, v_t, &uvname);
		meshFn.assignUVs(polygonCounts, uvFaces, &uvname);
	}
	if(!v_length.empty() && !u_length.empty())
	{
		MString uvname = "length";
		meshFn.createUVSetDataMeshWithName(uvname);
		meshFn.setUVs(u_l, v_l, &uvname);
		meshFn.assignUVs(polygonCounts, uvFaces, &uvname);
	}

	outputHandle.set(newOutputData);
	data.setClean( plug );
	return MS::kSuccess;
}


// ����������� �����������
bool furFractal::Rebuild(int operation)
{
	bool bInit = operation==1;

	if( bInit)
	{
		MPlug plug(thisMObject(), o_output);
		if( plug.isNull())
			return 0;

		plug.setValue(MObject::kNullObj);
	}


	MStatus stat;
	MDataBlock data = this->forceCache();
	FFGridData* ffgridData = this->Load_Grid(data);
	GridData* gridData = &ffgridData->data;

	MDagPath path;
	MDagPath::getAPathTo(thisMObject(), path);
	Math::Matrix4f worldm;
	::copy( worldm, path.inclusiveMatrix());

	int seed = data.inputValue( i_seed, &stat ).asInt();
	int maxStep = data.inputValue( i_maxStep[0], &stat ).asInt();
	int maxStepRefresh = data.inputValue( i_maxStep[1], &stat ).asInt();
	float dim = (float)data.inputValue( i_dimention).asDouble();

	std::vector<MathOld::IFFGrow*> ffgr;
	LoadGrows(data, ffgr);

	MayaProgress mp("furFractal", "Rebuild...");

	Util::FunEvalTimeCounter fetc;

	printf("startFF simulation\n");

	/*/
	std::vector<IFFControl*> ffcs;
	LoadControls(data, ffcs);
	for( int g=0; g<(int)ffcs.size(); g++)
	{
		displayString("grow: %d", ffcs[g]->growid);
	}
	/*/
	

//	gridData->grid.timestamp++;
	if( bInit)
	{
		Util::FunEvalTime fet(fetc);
		ffgridData->init(seed, worldm, ffgr, dim);
	}

	for( int i=1; gridData->lastStep<maxStep; i++)
	{
		gridData->grid.timestamp++;
		
		char text[256];
		sprintf(text, "Rebuild: step %04d vessels=%05d", gridData->lastStep, gridData->alives.size());

		bool res = mp.OnProgress(gridData->lastStep/(float)maxStep, text);
		if( !res) break;

//		displayStringD("Step %f", pData->lastTime);
		{
			Util::FunEvalTime fet(fetc);
			ffgridData->Step(
				worldm, 
				ffgr, 
				(float)data.inputValue( i_randomOffset).asDouble()
				);
		}
		gridData->lastStep++;

		if( (i%maxStepRefresh) == 0)
		{
			Build_DrawCache(MPlug(thisMObject(), o_drawcache), data);
			MGlobal::executeCommand("refresh");
		}
		fflush(stdout);
		fflush(stderr);
	}

	{
		mp.OnProgress(1, "Final");
		Util::FunEvalTime fet(fetc);
		ffgridData->GrowStop(
			worldm, 
			ffgr
			);
	}


	int venCount = gridData->alives.size();
	int segCount = gridData->curves.size();
	double estimationTime = fetc.sumtime;
	
	ffgridData->bModify = true;

	MPlug(thisMObject(), o_venCount).setValue(venCount);
	MPlug(thisMObject(), o_segCount).setValue(segCount);
	MPlug(thisMObject(), o_estimationTime).setValue(estimationTime);

	fflush(stdout);
	fflush(stderr);

	Build_DrawCache(MPlug(thisMObject(), o_drawcache), data);

	return true;
}

FFGridData* furFractal::Load_Grid(
	MDataBlock& data)
{
	typedef FFGridData CLASS;
	MObject i_attribute = o_output;

	MStatus stat;
	MDataHandle inputData = data.inputValue(i_attribute, &stat);
	MPxData* pxdata = inputData.asPluginData();
	CLASS* hgh = (CLASS*)pxdata;
	if( !hgh)
	{
		Build_Grid( MPlug(thisMObject(), i_attribute), data);
		MDataHandle inputData = data.inputValue(i_attribute, &stat);
		MPxData* pxdata = inputData.asPluginData();
		CLASS* hgh = (CLASS*)pxdata;
		return hgh;
	}
	return hgh;
}
FFDrawCache* furFractal::Load_DrawCache(
	MDataBlock& data)
{
	typedef FFDrawCache CLASS;
	MObject i_attribute = o_drawcache;

	MStatus stat;
	MDataHandle inputData = data.inputValue(i_attribute, &stat);
	MPxData* pxdata = inputData.asPluginData();
	CLASS* hgh = (CLASS*)pxdata;
	if( !hgh)
	{
		Build_DrawCache( MPlug(thisMObject(), i_attribute), data);
		MDataHandle inputData = data.inputValue(i_attribute, &stat);
		MPxData* pxdata = inputData.asPluginData();
		CLASS* hgh = (CLASS*)pxdata;
		return hgh;
	}
	return hgh;
}




bool furFractal::isBounded() const
{
	MObject thisNode = thisMObject();
	return true;
}

MBoundingBox furFractal::boundingBox() const
{
	MObject thisNode = thisMObject();
	furFractal* pThis = (furFractal*)this;

	MBoundingBox box;

	MStatus stat;
	MDataBlock data = pThis->forceCache();
	FFGridData* ffgridData = pThis->Load_Grid(data);
	GridData* gridData = &ffgridData->data;
	Math::Box3f dirtybox = gridData->getDirtyBox();

	::copy(box, dirtybox);
//	for( unsigned int i = 0; i < v.size(); i++ )
//	{
//		box.expand( MPoint( v[i].data() ) );
//	}
	return box;
}

void furFractal::LoadControls( 
	MDataBlock& data, 
	std::vector<MathOld::IFFControl*>& ffcs, 
	std::vector<Util::DllProcedure*>* dlls
	)
{
	MStatus stat;
	MArrayDataHandle inputDataCache = data.inputArrayValue(i_controls, &stat);
	MArrayDataHandle inputDataHistoryCache = data.outputArrayValue(i_controlHistories, &stat);
	ffcs.reserve( inputDataCache.elementCount());
	if( dlls)
		dlls->reserve( inputDataCache.elementCount());
	for( unsigned i=0; i<inputDataCache.elementCount(); i++)
	{
//		if( inputDataCache.jumpToElement(i))
		if( inputDataCache.jumpToArrayElement(i))
		{
			MPxData* dataCache = inputDataCache.inputValue(&stat).asPluginData();
			FFControlData* ffwd = (FFControlData*)dataCache;
			if( !ffwd) 
				continue;
			if( !ffwd->control)
				continue;
			ffcs.push_back(ffwd->control);

			if( dlls)
				dlls->push_back(&ffwd->dllproc);

			/*/
			FFControlHistoryData* pHistory = ;
			if( !inputDataHistoryCache.jumpToElement(i))
			{
				// ��������
				typedef FFControlHistoryData CLASS;
				FFControlHistoryData = NULL;

				MObject newDataObject = fnDataCreator.create( MTypeId( CLASS::id ), &stat );

				MArrayDataBuilder builder(&data, i_controlHistories, i);
				MDataHandle data = builder.addElement(i);
				data.set(newDataObject);
				inputDataHistoryCache.set(builder);
			}
			/*/
		}
	}
}

void furFractal::LoadWindows( MDataBlock& data, std::vector<MathOld::IFFWindow*>& ffds)
{
	FFGridData* griddata = furFractal::Load_Grid(data);

	// controls
	std::vector<MathOld::IFFControl*> ffcs;
	LoadControls(data, ffcs);
	for(int i=0; i<(int)ffcs.size(); i++)
	{
		if( !ffcs[i]) continue;
		if( !ffcs[i]->isWindow()) continue;

		ffds.push_back(ffcs[i]);
	}

}
void furFractal::LoadGrows( 
	MDataBlock& data, 
	std::vector<MathOld::IFFGrow*>& ffds)
{
	// controls
	std::vector<MathOld::IFFControl*> ffcs;
	LoadControls(data, ffcs);
	for(int i=0; i<(int)ffcs.size(); i++)
	{
		if( !ffcs[i]) continue;
		if( !ffcs[i]->isGrow()) continue;

		ffds.push_back(ffcs[i]);
	}
}

void furFractal::LoadDeformers( 
	MDataBlock& data, 
	std::vector<MathOld::IGridDeformer*>& ffds)
{
	// controls
	std::vector<MathOld::IFFControl*> ffcs;
	LoadControls(data, ffcs);
	ffds.reserve(ffcs.size());
	for(int i=0; i<(int)ffcs.size(); i++)
	{
		if( !ffcs[i]) continue;
		if( !ffcs[i]->isDeformer()) continue;

		ffds.push_back(ffcs[i]);
	}
}


void furFractal::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MStatus stat;
	MObject thisNode = thisMObject();

	MDataBlock data = forceCache();

	FFGridData* pData = Load_Grid(data);
	GridData* gridData = &pData->data;

	bool bViewGrid = MPlug( thisNode, i_viewGrid).asBool();
	bool bViewGridDots = MPlug( thisNode, i_viewGridDots).asBool();
	bool bDrawBox = MPlug( thisNode, i_drawBox).asBool();
	bool bColor = MPlug( thisNode, i_color).asBool();

	Math::Box3f dirtybox = gridData->getDirtyBox();
	if(bDrawBox)
	{
		view.beginGL(); 
		drawBox(dirtybox);
		view.endGL();
		return;
	}

	std::vector<MathOld::IFFWindow*> ffds;
	LoadWindows(data, ffds);

	std::vector<MathOld::IGridDeformer*> deforms;
	LoadDeformers( data, deforms);

//	MPlug plug(thisNode, i_time);
	double val;
	MPlug plug(thisNode, o_output);
	plug.getValue(val);

	MPlug widthScalePlug( thisNode, i_widthScale );
	double widthScaleValue = widthScalePlug.asDouble();

	Math::Matrix4f worldm;
	::copy( worldm, path.inclusiveMatrix());

	view.beginGL(); 
	if( !bViewGrid)
	{
		GLfloat width;
		glGetFloatv( GL_LINE_WIDTH, &width );

		FFDrawCache* drawCache = Load_DrawCache(data);

		drawCache->draw(bViewGridDots, bColor);

		glLineWidth(width);
	}
	if( bViewGrid)
	{
		MObject val;
		MPlug( thisNode, i_viewGridPoint).getValue(val);
		MFnNumericData nd(val);
		MPoint pt;
		nd.getData3Double(pt.x, pt.y, pt.z);
		Math::Vec3f _pt;
		::copy(_pt, pt);

		int viewGridLevel = MPlug( thisNode, i_viewGridLevel).asInt();
		pData->drawVertexes(viewGridLevel, deforms, worldm, _pt);
	}

	// ���� �����������
	for( int w=0; w<(int)ffds.size(); w++)
	{
		if( ffds[w])
			ffds[w]->draw(worldm, *gridData);
	}

	// ��� �����������
	{
		drawLine(Math::Vec3f(0), Math::Vec3f(2, 0, 0));
		drawLine(Math::Vec3f(0), Math::Vec3f(0, 1, 0));
		glLineStipple(1, 0x8888);
		glEnable(GL_LINE_STIPPLE);
		drawLine(Math::Vec3f(0), Math::Vec3f(0, 0, 2));
		glDisable(GL_LINE_STIPPLE);
	}

	view.endGL();
};

void* furFractal::creator()
{
	return new furFractal();
}

MStatus furFractal::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnEnumAttribute	enumAttr;
	MStatus				stat;

	try
	{
		{
			i_maxStep[0] = numAttr.create ("maxStep","mas", MFnNumericData::kInt, 50, &stat);
			::addAttribute(i_maxStep[0], atInput);

			i_maxStep[1] = numAttr.create ("maxStepRefresh","msr", MFnNumericData::kInt, 20, &stat);
			::addAttribute(i_maxStep[1], atInput);
		}
		{
			i_seed = numAttr.create ("seed","se", MFnNumericData::kInt, 0, &stat);
			numAttr.setMin(0);
			numAttr.setMax(1000);
			::addAttribute(i_seed, atInput);
		}
		{
			i_controls = typedAttr.create( "controls", "ctrs", FFControlData::id);
			typedAttr.setReadable(false);
			typedAttr.setArray(true);
			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			typedAttr.setIndexMatters(false);
			::addAttribute(i_controls, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray );
		}
		/*/
		{
			i_growUpPropencity = numAttr.create ("growUpPropencity","gup", MFnNumericData::kDouble, 0.3, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(0.3);
			numAttr.setMax(1);
			::addAttribute(i_growUpPropencity, atInput);
		}
		{
			i_growPropencity = numAttr.create ("growPropencity","gp", MFnNumericData::kDouble, 0.01, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(0.3);
			numAttr.setMax(1);
			::addAttribute(i_growPropencity, atInput);
		}
		{
			i_growLowPropencity = numAttr.create ("growLowPropencity","glp", MFnNumericData::kDouble, 0.1, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(0.3);
			numAttr.setMax(1);
			::addAttribute(i_growLowPropencity, atInput);
		}
		{
			i_maxLevel = numAttr.create ("maxLevel","ml", MFnNumericData::kInt, 2, &stat);
			::addAttribute(i_maxLevel, atInput);
		}
		/*/
		{
			i_randomOffset = numAttr.create ("randomOffset","rof", MFnNumericData::kDouble, 0.01, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_randomOffset, atInput);
		}

		{
			i_viewGridDots = numAttr.create ("viewGridDots","vgd", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_viewGridDots, atInput);

			i_viewGrid = numAttr.create ("viewGrid","vg", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_viewGrid, atInput);

			i_viewGridLevel = numAttr.create ("viewGridLevel","vgl", MFnNumericData::kInt, -1, &stat);
			numAttr.setSoftMin(-4);
			numAttr.setSoftMax(4);
			::addAttribute(i_viewGridLevel, atInput);

			
			i_viewGridPoint = numAttr.create ("viewGridPoint","vgpt", MFnNumericData::k3Double, 0, &stat);
			::addAttribute(i_viewGridPoint, atInput);

			i_drawBox = numAttr.create ("drawBox","drb", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_drawBox, atInput);

			i_color = numAttr.create ("colorize","clr", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_color, atInput);
		
		}

		/*/
		{
			i_deforms_point = typedAttr.create( "deforms_point", "dfp", MFnData::kMatrix);
			typedAttr.setReadable(false);
			typedAttr.setArray(true);
			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			typedAttr.setIndexMatters(false);
			::addAttribute(i_deforms_point, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray );
		}
		{
			i_deforms_point_base = typedAttr.create( "deforms_point_base", "dfpb", MFnData::kMatrix);
			typedAttr.setReadable(false);
			typedAttr.setArray(true);
			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			typedAttr.setIndexMatters(false);
			::addAttribute(i_deforms_point_base, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray );
		}
		{
			i_deforms_curve = typedAttr.create( "deforms_curve", "dfc", MFnData::kNurbsCurve);
			typedAttr.setReadable(false);
			typedAttr.setArray(true);
			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			typedAttr.setIndexMatters(false);
			::addAttribute(i_deforms_curve, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray );
		}
		/*/

		{
			i_renderType = enumAttr.create ("rType","rtype", 3, &stat);
			enumAttr.addField("Curve_Linear", 0);
			enumAttr.addField("Curve_Cubic", 1);
			enumAttr.addField("Mesh", 2);
			enumAttr.addField("Subdiv", 3);
			enumAttr.addField("Test", 4);
			::addAttribute(i_renderType, atInput);
		}
		{
			i_renderWidth = numAttr.create ("renderWidth","rw", MFnNumericData::kDouble, 0.05, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(2.0);
			::addAttribute(i_renderWidth, atInput);
		}
		{
			i_tesselation = numAttr.create ("tesselation","tess", MFnNumericData::kInt, 1, &stat);
			numAttr.setMin(1);
			numAttr.setSoftMax(5);
			::addAttribute(i_tesselation, atInput);
		}

		{
			o_output = typedAttr.create( "grid", "gr", FFGridData::id);
			::addAttribute(o_output, atReadable|atWritable|atStorable|atCached|atHidden);

//			stat = attributeAffects( i_time, o_output );
			stat = attributeAffects( i_seed, o_output );
//			stat = attributeAffects( i_growLowPropencity, o_output );
//			stat = attributeAffects( i_growPropencity, o_output );
//			stat = attributeAffects( i_windows, o_output );
			stat = attributeAffects( i_controls, o_output );
		}
		{
			i_widthScale = numAttr.create ("widthScale","ws", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(2.0);
			::addAttribute(i_widthScale, atInput);
		}
		{
			i_dimention = numAttr.create ("dimention","dim", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(2.0);
			::addAttribute(i_dimention, atInput);
		}
		{
			o_drawcache = typedAttr.create( "drawcache", "drc", FFDrawCache::id);
			::addAttribute(o_drawcache, atReadable|atWritable|atUnStorable|atCached|atHidden);

//			stat = attributeAffects( i_time, o_drawcache );
			stat = attributeAffects( i_seed, o_drawcache );
			stat = attributeAffects( i_controls, o_drawcache );
			stat = attributeAffects( i_viewGrid, o_drawcache );
			stat = attributeAffects( i_drawBox, o_drawcache );
			stat = attributeAffects( i_widthScale, o_drawcache );
		}

		{
			o_mesh = typedAttr.create( "outmesh", "ome", MFnData::kMesh);
			::addAttribute(o_mesh, atReadable|atWritable|atUnStorable|atCached|atConnectable|atHidden);

			stat = attributeAffects( i_renderWidth, o_mesh );
			stat = attributeAffects( i_tesselation, o_mesh );
			stat = attributeAffects( i_seed, o_mesh );
			stat = attributeAffects( i_controls, o_mesh );
		}

		// ����������
		{
			o_venCount = numAttr.create ("venCount","svk", MFnNumericData::kInt, 0, &stat);
			::addAttribute(o_venCount, atReadable|atUnWritable|atConnectable|atStorable);

			o_segCount = numAttr.create ("segCount","ssc", MFnNumericData::kInt, 0, &stat);
			::addAttribute(o_segCount, atReadable|atUnWritable|atConnectable|atStorable);

			o_estimationTime = numAttr.create ("estimationTime","set", MFnNumericData::kDouble, 0, &stat);
			::addAttribute(o_estimationTime, atReadable|atUnWritable|atConnectable|atStorable);
		}
		

		if( !MGlobal::sourceFile("AEfurFractalTemplate.mel"))
		{
			displayString("error source AEfurFractalTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}


