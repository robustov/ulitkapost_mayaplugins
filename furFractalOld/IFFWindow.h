#pragma once

#include "Math/GridOld.h"
#include "IFFEdgeData.h"
class GridData;

namespace MathOld
{
	// ��������� ����
	// �������� ��������/�����
	// �������� ������ ���������/�����
	struct IFFWindow
	{
	public:
		// ���������� ������ ��� ����� �� �����
		virtual float processEdge(
			Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
			GridData& griddata,					// �������
			const IFFEdgeData& edge,			// �������� �����
			float pointonedge,					// ����� �� ����� (�� 0 �� 1)
			float src_width						// �������� ������
			)=0;
		// ���������� �����.���������� ��� ����� �� �����
		virtual bool getTexCoord(
			float& texCoord, 
			Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
			GridData& griddata,					// �������
			const IFFEdgeData& edge,			// �������� �����
			float pointonedge,					// ����� �� ����� (�� 0 �� 1)
			int texCoordType					// �� ������� ���� ��������� UV ����� ���� ������
			){return false;};
		// ����� ��������� ����� ��� ������������ ����� �����
		virtual bool getMainPoint(
			const MathOld::GridCellId& v1,	//����������� ����� v1->v2
			const MathOld::GridCellId& v2,
			const std::list<MathOld::GridCellId>& v3list,	// ��������� ����� ���. ������� �� ������� v2
			GridData& griddata,
			MathOld::GridCellId& mainPoint	//��������� �����, ���� ��������� true
			)=0; 
		// ��� ��������� �� ��������
		virtual void draw(
			Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
			GridData& griddata
			){};
	};
}