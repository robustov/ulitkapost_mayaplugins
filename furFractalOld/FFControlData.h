#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"
#include "Math/GridOld.h"
#include "IFFWindow.h"
#include "IFFGrow.h"
#include "IFFControl.h"
#include "Util/DllProcedure.h"

#include "ocellaris/OcellarisExport.h"
struct OcsData : public IOcsData
{
	MObject obj;
public:
	OcsData( MObject& obj)
	{
		this->obj = obj;
	}

	virtual cls::Param getAttribute(
		const char* name, cls::Param& defvalue)
	{
		ocExport_AddAttrType( this->obj, name, defvalue->type, defvalue);
		return ocExport_GetAttrValue( this->obj, name);
	};
};

class FFControlData : public MPxData
{
public:
	MathOld::IFFControl* control;
	// ����� ����� ��� ��������
	Util::DllProcedure dllproc;

	MathOld::IFFWindow* getWindow(){if(control && control->isWindow()) return control; return NULL;};
	MathOld::IFFGrow* getGrow(){if(control && control->isGrow()) return control; return NULL;};
	MathOld::IGridDeformer* getDeformer(){if(control && control->isDeformer()) return control; return NULL;};

public:
	FFControlData();
	virtual ~FFControlData(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;

protected:
	void serialize(Util::Stream& stream);
};

