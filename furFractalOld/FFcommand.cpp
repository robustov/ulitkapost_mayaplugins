#include "furFractal.h"

#ifdef WIN32
#define EXPORT __declspec(dllexport) 
#else
#define EXPORT
#endif


extern "C"
{
//	export int ocs_clear();
//	export const char* ocs_getenv(const char* env);
//	export int ocs_setenv(const char* env, const char* value);

EXPORT int ff_build(int operation, char* object)
{
	MObject obj;
	if( !nodeFromName(object, obj))
		return 0;

	MFnDependencyNode dn(obj);

	furFractal* ff = (furFractal*)dn.userNode();
	ff->Rebuild(operation);

	return 1;
}

/*/
EXPORT int ff_rebind(char* object)
{
	MObject obj;
	if( !nodeFromName(object, obj))
		return 0;

	MFnDependencyNode dn(obj);
	furFractal* ff = (furFractal*)dn.userNode();
	ff->RebindDeformers();



//	plug.setValue(MObject::kNullObj);
	return 1;
}

EXPORT int ff_regrow(char* object)
{
	MObject obj;
	if( !nodeFromName(object, obj))
		return 0;

	MFnDependencyNode dn(obj);
	furFractal* ff = (furFractal*)dn.userNode();
	ff->RebindGrow();

//	plug.setValue(MObject::kNullObj);
	return 1;
}
/*/


}
