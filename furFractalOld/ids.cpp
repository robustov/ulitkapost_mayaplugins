#include "pre.h"

const unsigned TypeIdBase = 0xda120;

// node static ids and names

#include "furFractal.h"
MTypeId furFractal::id( TypeIdBase+0 );
const MString furFractal::typeName( "furFractal" );


#include "FFSphereLocator.h"
MTypeId FFSphereLocator::id( TypeIdBase+02 );
const MString FFSphereLocator::typeName( "FFSphereLocator" );

#include "FFGridData.h"
MTypeId FFGridData::id( TypeIdBase+03 );
const MString FFGridData::typeName( "FFGridData" );

#include "FFControlData.h"
MTypeId FFControlData::id( TypeIdBase+04 );
const MString FFControlData::typeName( "FFControlData" );

#include "FFCurveLocator.h"
MTypeId FFCurveLocator::id( TypeIdBase+05 );
const MString FFCurveLocator::typeName( "FFCurveLocator" );

#include "FFMeshLocator.h"
MTypeId FFMeshLocator::id( TypeIdBase+06 );
const MString FFMeshLocator::typeName( "FFMeshLocator" );

#include "FFDrawCache.h"
MTypeId FFDrawCache::id( TypeIdBase+07 );
const MString FFDrawCache::typeName( "FFDrawCache" );

