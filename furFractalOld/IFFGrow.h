#pragma once

#include "Math/GridOld.h"
#include "IFFEdgeData.h"
class GridData;

namespace MathOld
{

	// ��������� ��� ������������ 
	struct IFFGrow
	{
		// ��������� �������
		virtual void Start(
			Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
			GridData& griddata, 
			std::list<MathOld::IFFEdgeData>& added
			)=0;
		// ��� ���������
		virtual void Step(
			Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
			GridData& griddata, 
			std::list<MathOld::IFFEdgeData>& added
			)=0;
		// ����� ���������
		virtual void GrowStop(
			Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
			GridData& griddata, 
			std::list<MathOld::IFFEdgeData>& added
			){};
	};
}