//
// Copyright (C) 
// File: FFSphereLocatorCmd.cpp
// MEL Command: FFSphereLocator

#include "FFSphereLocator.h"

#include <maya/MGlobal.h>
#include <maya/MFnPluginData.h>
#include <maya/MArrayDataHandle.h>
#include "MathNgl/MathNgl.h"
#include "MathNmaya/MathNmaya.h"
#include "FFGridData.h"

MObject FFSphereLocator::i_worldMatrix;

MObject FFSphereLocator::i_window;
	MObject FFSphereLocator::i_windowSize;
	MObject FFSphereLocator::i_disgrowfactor;
	MObject FFSphereLocator::i_width0, FFSphereLocator::i_widthExp;
	MObject FFSphereLocator::i_venToper;

MObject FFSphereLocator::i_grow;
	MObject FFSphereLocator::i_bStart;
	MObject FFSphereLocator::i_growUpPropencity;
	MObject FFSphereLocator::i_growPropencity;
	MObject FFSphereLocator::i_growLowPropencity;
	MObject FFSphereLocator::i_maxLevel;
	MObject FFSphereLocator::i_startLevel;
	MObject FFSphereLocator::i_seed;
	MObject FFSphereLocator::maxGrow;
	MObject FFSphereLocator::maxGrowLow;
	MObject FFSphereLocator::sameFluctuation;
	MObject FFSphereLocator::samePhaseBiasConst;
	MObject FFSphereLocator::samePhaseBiasRnd;
	MObject FFSphereLocator::samePhaseSecondaryFactor;
	MObject FFSphereLocator::deformToper;

MObject FFSphereLocator::i_deformer;
	MObject FFSphereLocator::i_time;
	MObject FFSphereLocator::i_xynoisemagnitude;
	MObject FFSphereLocator::i_xynoisefrequency;
	MObject FFSphereLocator::i_xynoisedimention;

// render
	MObject FFSphereLocator::texToper;

MObject FFSphereLocator::o_control;

















FFSphereLocator::FFSphereLocator()
{
}
FFSphereLocator::~FFSphereLocator()
{
}

MStatus FFSphereLocator::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;
	displayStringD( "subdyWrap::compute %s", plug.name().asChar());

	if( plug == o_control)
	{
		return Build_Control(plug, data);
	}

	return MS::kUnknownParameter;
}

MStatus FFSphereLocator::Build_Control(
	const MPlug& plug, MDataBlock& data)
{
	displayStringD( "FFSphereLocator::Build_Control");
	MStatus stat;

	MDataHandle outputData = data.outputValue(plug, &stat );

	typedef FFControlData CLASS;
	CLASS* pData = NULL;

	bool bNewData = false;
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<CLASS*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( CLASS::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<CLASS*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	if( !pData->dllproc.isValid())
	{
		pData->dllproc.LoadFormated("sphereControl", "furFractalMath.dll");
	}
	if( pData->dllproc.isValid())
	{
		if( !pData->control)
		{
			MathOld::IFFCONTROL_CREATOR pc = (MathOld::IFFCONTROL_CREATOR)pData->dllproc.proc;
			pData->control = (*pc)();
		}
		if(pData->control)
		{
			SphereControl* cc = (SphereControl*)pData->control;

			MPlug plugwm(thisMObject(), this->i_worldMatrix);

			MObject mval;
			plugwm.getValue(mval);
			MFnMatrixData se(mval);
			MMatrix m = se.matrix();
			Math::Matrix4f worldpos;
			::copy(worldpos, m);

			cc->init(
				worldpos
				);

			cc->bWindow = data.inputValue(i_window).asBool();
			cc->disgrowfactor = (float)data.inputValue(i_disgrowfactor).asDouble();

			cc->width0 = (float)data.inputValue(i_width0).asDouble();
			cc->widthExp = (float)data.inputValue(i_widthExp).asDouble();
			cc->venToper= (float)data.inputValue(i_venToper).asDouble();

			cc->bDeformer = data.inputValue( i_deformer).asBool();
			cc->bGrow = data.inputValue(i_grow).asBool();

			cc->windowSize = (float)data.inputValue(i_windowSize).asDouble();
			cc->bStart = data.inputValue(i_bStart).asBool();

			cc->growUpPropencity = (float)data.inputValue(i_growUpPropencity).asDouble();
			cc->growPropencity = (float)data.inputValue(i_growPropencity).asDouble();
			cc->growLowPropencity = (float)data.inputValue(i_growLowPropencity).asDouble();
			cc->maxLevel = data.inputValue(i_maxLevel).asInt();
			cc->startLevel = data.inputValue(i_startLevel).asInt();
			cc->startSeed = data.inputValue(i_seed).asInt();
			cc->maxGrow = (float)data.inputValue(					maxGrow).asDouble();
			cc->maxGrowLow = (float)data.inputValue(				maxGrowLow).asDouble();
			cc->sameFluctuation = (float)data.inputValue(			sameFluctuation).asDouble();
			cc->samePhaseBiasConst = (float)data.inputValue(		samePhaseBiasConst).asDouble();
			cc->samePhaseBiasRnd = (float)data.inputValue(			samePhaseBiasRnd).asDouble();
			cc->samePhaseSecondaryFactor = (float)data.inputValue(	samePhaseSecondaryFactor).asDouble();


			cc->time = (float)data.inputValue(i_time).asDouble();
			cc->xynoisemagnitude = (float)data.inputValue(i_xynoisemagnitude).asDouble();
			cc->xynoisefrequency = (float)data.inputValue(i_xynoisefrequency).asDouble();
			cc->xynoisedimention = (float)data.inputValue(i_xynoisedimention).asDouble();
			cc->deformToper = (float)data.inputValue(deformToper).asDouble();

			cc->texToper = (float)data.inputValue(texToper).asDouble();

			OcsData ocsData(thisMObject());
			cc->ReadAttributes(&ocsData);
		}
	}
	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

bool FFSphereLocator::isBounded() const
{
	MObject thisNode = thisMObject();
	return false;
}

MBoundingBox FFSphereLocator::boundingBox() const
{
	MObject thisNode = thisMObject();

	MBoundingBox box;
	//...
	return box;
}

void FFSphereLocator::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MObject thisNode = thisMObject();

	double size=1;
	MPlug(thisNode, i_windowSize).getValue(size);

	view.beginGL(); 

	if( size!=1)
	{
		glLineStipple(1, 0x8888);
		glEnable(GL_LINE_STIPPLE);
		drawSphere(2);
		glDisable(GL_LINE_STIPPLE);

		drawSphere(2*(float)size);
	}
	else
	{
		drawSphere(2*(float)size);
	}

	view.endGL();
};


void* FFSphereLocator::creator()
{
	return new FFSphereLocator();
}

MStatus	FFSphereLocator::connectionMade( 
	const MPlug& plug,
	const MPlug& otherPlug,
	bool asSrc 
	)
{
	if( asSrc)
	{
		this->setExistWithoutOutConnections(false);
	}
	else
	{
		this->setExistWithoutInConnections(false);
	}
	return MPxNode::connectionMade(plug,otherPlug,asSrc );
}


MStatus FFSphereLocator::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_worldMatrix = typedAttr.create( "world", "wm", MFnData::kMatrix);
			::addAttribute(i_worldMatrix, atInput);
		}

		{
			i_window = numAttr.create ("bWindow","bwin", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_window, atInput);
		}
		{
			
			i_windowSize = numAttr.create ("windowSize","ws", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_windowSize, atInput|atKeyable);

			i_disgrowfactor = numAttr.create ("disGrowFactor","dgf", MFnNumericData::kDouble, 2, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(4);
			::addAttribute(i_disgrowfactor, atInput);

			i_width0 = numAttr.create ("width0","wi0", MFnNumericData::kDouble, 1, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(2);
			::addAttribute(i_width0, atInput);

			i_widthExp = numAttr.create ("widthExp","wie", MFnNumericData::kDouble, 0.5, &stat);
			numAttr.setMin(0.001);
			numAttr.setSoftMax(1);
			::addAttribute(i_widthExp, atInput);

			i_venToper = numAttr.create ("venToper","vent", MFnNumericData::kDouble, 10, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(i_venToper, atInput);
		}
		{
			i_bStart = numAttr.create ("bStart","bst", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_bStart, atInput);

			i_startLevel = numAttr.create ("startLevel","stl", MFnNumericData::kInt, 1, &stat);
			::addAttribute(i_startLevel, atInput);

			i_seed = numAttr.create ("seed","se", MFnNumericData::kInt, 0, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1000);
			::addAttribute(i_seed, atInput);

			maxGrow = numAttr.create ("maxGrow","mg", MFnNumericData::kDouble, 2, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(maxGrow, atInput);

			maxGrowLow = numAttr.create ("maxGrowLow","mgl", MFnNumericData::kDouble, 3, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(maxGrowLow, atInput);

			sameFluctuation = numAttr.create ("sameFluctuation","sfl", MFnNumericData::kDouble, 1.5, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(sameFluctuation, atInput);

			samePhaseBiasConst = numAttr.create ("samePhaseBiasConst","spbc", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(samePhaseBiasConst, atInput);

			samePhaseBiasRnd = numAttr.create ("samePhaseBiasRnd","spbr", MFnNumericData::kDouble, 0.5, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(samePhaseBiasRnd, atInput);

			samePhaseSecondaryFactor = numAttr.create ("samePhaseSecondaryFactor","spsf", MFnNumericData::kDouble, 4, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(samePhaseSecondaryFactor, atInput);
		}
		{
			i_grow = numAttr.create ("bGrow","bgr", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_grow, atInput);
		}
		{
			i_growUpPropencity = numAttr.create ("growUpPropencity","gup", MFnNumericData::kDouble, 0.3, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(0.3);
			numAttr.setMax(1);
			::addAttribute(i_growUpPropencity, atInput);
		}
		{
			i_growPropencity = numAttr.create ("growPropencity","gp", MFnNumericData::kDouble, 0.01, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(0.3);
			numAttr.setMax(1);
			::addAttribute(i_growPropencity, atInput);
		}
		{
			i_growLowPropencity = numAttr.create ("growLowPropencity","glp", MFnNumericData::kDouble, 0.1, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(0.3);
			numAttr.setMax(1);
			::addAttribute(i_growLowPropencity, atInput);
		}
		{
			i_maxLevel = numAttr.create ("maxLevel","ml", MFnNumericData::kInt, 2, &stat);
			::addAttribute(i_maxLevel, atInput);
		}
		{
			i_deformer = numAttr.create ("bDeformer","bde", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_deformer, atInput);

			i_time = numAttr.create ("time","iti", MFnNumericData::kDouble, 0.01, &stat);
			::addAttribute(i_time, atInput);

			i_xynoisemagnitude = numAttr.create ("xyNoiseMagnitude","xynm", MFnNumericData::kDouble, 0.5, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_xynoisemagnitude, atInput);

			i_xynoisefrequency = numAttr.create ("xyNoiseFrequency","xynf", MFnNumericData::kDouble, 0.033, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_xynoisefrequency, atInput);

			i_xynoisedimention = numAttr.create ("xyNoiseDimention","xynd", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.0001);
			numAttr.setSoftMax(2);
			::addAttribute(i_xynoisedimention, atInput);

			deformToper = numAttr.create ("deformToper","deto", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.0001);
			numAttr.setSoftMax(2);
			::addAttribute(deformToper, atInput);
		}
		{
			texToper = numAttr.create ("texToper","teto", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.0001);
			numAttr.setSoftMax(2);
			::addAttribute(texToper, atInput);
		}

		{
			o_control = typedAttr.create( "control", "con", FFControlData::id);
			::addAttribute(o_control, atReadable|atWritable|atStorable|atCached|atConnectable);

			stat = attributeAffects( i_window, o_control );
			stat = attributeAffects( i_windowSize, o_control );
			stat = attributeAffects( i_disgrowfactor, o_control );
			stat = attributeAffects( i_width0, o_control );
			stat = attributeAffects( i_widthExp, o_control );
			stat = attributeAffects( i_venToper, o_control);			
			stat = attributeAffects( i_bStart, o_control );
			stat = attributeAffects( i_grow, o_control );
			stat = attributeAffects( i_growUpPropencity, o_control );
			stat = attributeAffects( i_growPropencity, o_control );
			stat = attributeAffects( i_growLowPropencity, o_control );
			stat = attributeAffects( i_maxLevel, o_control );
			stat = attributeAffects( i_startLevel, o_control );
			stat = attributeAffects( i_worldMatrix, o_control );
			stat = attributeAffects( i_seed, o_control);
			stat = attributeAffects( maxGrow, o_control);
			stat = attributeAffects( maxGrowLow, o_control);
			stat = attributeAffects( sameFluctuation, o_control);
			stat = attributeAffects( samePhaseBiasConst, o_control);
			stat = attributeAffects( samePhaseBiasRnd, o_control);
			stat = attributeAffects( samePhaseSecondaryFactor, o_control);


			attributeAffects(i_deformer,			o_control);
			attributeAffects(i_time,			o_control);
			attributeAffects(i_xynoisemagnitude, o_control);
			attributeAffects(i_xynoisefrequency, o_control);
			attributeAffects(i_xynoisedimention, o_control);
			attributeAffects(deformToper, o_control);

			attributeAffects(texToper, o_control);
		}

		if( !MGlobal::sourceFile("AEFFSphereLocatorTemplate.mel"))
		{
			displayString("error source AEFFSphereLocatorTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}