#include "HelixCreateCmd.h"
#include "HelixNode.h"
#include "JitterNode.h"
#include <maya/MFnPlugin.h>
#include "PerlinCmd.h"

MStatus initializePlugin(MObject _obj)
{
	MStatus   status;
	MFnPlugin plugin( _obj, "UlitkaTools", "1.0", "Any");

	status = plugin.registerCommand("Helix", HelixCreateCmd::creator);
	if (!status)
	{
		status.perror("registerCommand");
		return status;
	}
	status = plugin.registerCommand("Perlin", PerlinCmd::creator, PerlinCmd::newSyntax);
	if (!status)
	{
		status.perror("registerCommand");
		return status;
	}

	status = plugin.registerNode("HelixNode", HelixNode::id, HelixNode::creator, HelixNode::initialize);
	if (!status)
	{
		status.perror("registerNode");
		return status;
	}

	status = plugin.registerNode( "JitterNode", JitterNode::id, JitterNode::creator, JitterNode::initialize);
	if (!status)
	{
		status.perror("registerNode");
	}

	return status;
}

MStatus uninitializePlugin(MObject _obj)
{
	MStatus   status;
	MFnPlugin plugin(_obj);


	status = plugin.deregisterCommand("Perlin");
	if (!status)
	{
		status.perror("deregisterCommand");
		return status;
	}
	status = plugin.deregisterCommand("Helix");
	if (!status)
	{
		status.perror("deregisterCommand");
		return status;
	}

	status = plugin.deregisterNode(HelixNode::id);
	if (!status)
	{
		status.perror("deregisterNode");
		return status;
	}

	status = plugin.deregisterNode(JitterNode::id);
	if (!status)
	{
		status.perror("deregisterNode");
		return status;
	}

	return status;
}
