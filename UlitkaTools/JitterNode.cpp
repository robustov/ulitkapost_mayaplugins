#include "JitterNode.h"
#include <maya/MGlobal.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MVectorArray.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFloatArray.h>
#include <maya/MIntArray.h>
#include <maya/MRampAttribute.h>
#include <string>

/*/
bool JitterNode::createFloatRampAttribute(MObject& main, const char* fullname, const char* shortname)
{
	MStatus stat;
	MFnNumericAttribute nAttr;
	MFnEnumAttribute eAttr;
	MFnCompoundAttribute cAttr;
	std::string fn, sn;

	fn = std::string(fullname) + "_Position";
	sn = std::string(shortname) + std::string("p");
	MObject pos = nAttr.create(fn.c_str(), sn.c_str(), MFnNumericData::kFloat, 0.0);
//	nAttr.setMin(-0.5);
//	nAttr.setMax(0.5);
	nAttr.setWritable(true);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(pos);

	fn = std::string(fullname) + std::string("_FloatValue");
	sn = std::string(shortname) + std::string("fv");
	MObject fv = nAttr.create(fn.c_str(), sn.c_str(), MFnNumericData::kFloat, 0.0);
	nAttr.setWritable(true);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(fv);

	fn = std::string(fullname) + std::string("_Interp");
	sn = std::string(shortname) + std::string("i");
	MObject in = eAttr.create(fn.c_str(), sn.c_str());
	eAttr.addField("None", 0);
	eAttr.addField("Linear", 1);
	eAttr.addField("Smooth", 2);
	eAttr.addField("Spline", 3);
	stat = addAttribute(in);

	main = cAttr.create(fullname, shortname);
	cAttr.addChild(pos);
	cAttr.addChild(fv);
	cAttr.addChild(in);
	cAttr.setWritable(true);
	cAttr.setStorable(true);
	cAttr.setKeyable(true);
	cAttr.setArray(true);
	stat = addAttribute(main);

	return true;
}
/*/

inline void displayString(const char* text, ...)
{
	va_list argList; va_start(argList, text);
	char sbuf[256];
	_vsnprintf(sbuf, 255, text, argList);
	va_end(argList);
	MGlobal::displayInfo(sbuf);
}

MTypeId     JitterNode::id(0x80163);
MObject		JitterNode::atime;
//MObject		JitterNode::ascale;
//MObject		JitterNode::alayers;
MObject		JitterNode::amagX;
MObject		JitterNode::amagY;
MObject		JitterNode::amagZ;
MObject		JitterNode::afreqX;
MObject		JitterNode::afreqY;
MObject		JitterNode::afreqZ;

MObject     JitterNode::aoutput;
MObject     JitterNode::aoutputX;
MObject     JitterNode::aoutputY;
MObject     JitterNode::aoutputZ;

MObject     JitterNode::ainput;
MObject     JitterNode::ainputX;
MObject     JitterNode::ainputY;
MObject     JitterNode::ainputZ;

MObject		JitterNode::aUseAsScale;

//MObject JitterNode::o_vectarray;
//MObject	JitterNode::rmpattr;


//MObject		JitterNode::ainterp_type;
MObject     JitterNode::aseed; 

#define LINEARY	0
#define COSINE	1
#define CUBIC	2

#define MAX_NUM_RANDS	100
#define PI	3.14159265358979323846
/*
// a simple linear interpolation
float linear_interpolate(float a, float b, float x)
{ 
	return  ((float)(a*(1-x) + b*x));
}

// cosine interpolation
float cosine_interpolate(float a, float b, float x)
{
	float ft = (float)(x * PI);
    float f  = (float)(1 - cos(ft)) * .5f;

    return  a*(1-f) + b*f;
}

// cubic interpolation
float cubic_interpolate(float v0, float v1, float v2, float v3, float x)
{	
	float P = (v3 - v2) - (v0 - v1);
	float Q = (v0 - v1) - P;
	float R = v2 - v0;
	float S = v1;

	return ((float)(P*pow(x,3) + Q*pow(x,2) + R*x + S));
}
*/

void* JitterNode::creator()
{
	return new JitterNode();
}


// This function initializes each node attribute
MStatus JitterNode::initialize()
{
	MFnNumericAttribute nAttr;
	MFnEnumAttribute eAttr;
	MStatus	stat;

	atime = nAttr.create ("time", "tm", MFnNumericData::kDouble, 0.0);
	nAttr.setKeyable(true);
	nAttr.setStorable (true);
//	nAttr.setInternal (true);
	nAttr.setHidden(false);
	stat = addAttribute (atime);	
	if (!stat) return stat;

/*/
	ascale = nAttr.create("scale", "sc", MFnNumericData::kDouble, 1.0);
	nAttr.setStorable (true);
	nAttr.setInternal (true);
	nAttr.setHidden(false);
	nAttr.setSoftMin(0);	
	nAttr.setSoftMax(1);
	stat = addAttribute(ascale);
	if (!stat) return stat;

	alayers = nAttr.create("layers", "lr", MFnNumericData::kDouble, 1.0);
	nAttr.setStorable (true);
	nAttr.setInternal (true);
	nAttr.setHidden(false);
	nAttr.setSoftMin(0);	
	nAttr.setSoftMax(1);
	stat = addAttribute(alayers);
	if (!stat) return stat;
/*/

	amagX = nAttr.create ("magX","mnx", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setStorable (true);
//	nAttr.setInternal (true);
	nAttr.setHidden(false);
	nAttr.setSoftMin(0);	
	nAttr.setSoftMax(10);	
	stat = addAttribute (amagX);	
	if (!stat) return stat;


	amagY = nAttr.create("magY", "mny", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setStorable (true);
//	nAttr.setInternal (true);
	nAttr.setHidden(false);
	nAttr.setSoftMin(0);	
	nAttr.setSoftMax(10);
	stat = addAttribute(amagY);
	if (!stat) return stat;

	amagZ = nAttr.create("magZ", "mnz", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setStorable (true);
//	nAttr.setInternal (true);
	nAttr.setHidden(false);
	nAttr.setSoftMin(0);	
	nAttr.setSoftMax(10);
	stat = addAttribute(amagZ);
	if (!stat) return stat;

	afreqX = nAttr.create("freqX", "frx", MFnNumericData::kDouble, 0.1);
	nAttr.setKeyable(true);
	nAttr.setStorable (true);
//	nAttr.setInternal (true);
	nAttr.setHidden(false);
	nAttr.setSoftMin(0);	
	nAttr.setSoftMax(1);
	stat = addAttribute(afreqX);
	if (!stat) return stat;

	afreqY = nAttr.create("freqY", "fry", MFnNumericData::kDouble, 0.1);
	nAttr.setKeyable(true);
	nAttr.setStorable (true);
//	nAttr.setInternal (true);
	nAttr.setHidden(false);
	nAttr.setSoftMin(0);	
	nAttr.setSoftMax(1);
	stat = addAttribute(afreqY);
	if (!stat) return stat;

	afreqZ = nAttr.create("freqZ", "frz", MFnNumericData::kDouble, 0.1);
	nAttr.setKeyable(true);
	nAttr.setStorable (true);
//	nAttr.setInternal (true);
	nAttr.setHidden(false);
	nAttr.setSoftMin(0);	
	nAttr.setSoftMax(1);
	stat = addAttribute(afreqZ);
	if (!stat) return stat;

	{
		aUseAsScale = nAttr.create("UseAsScale", "uas", MFnNumericData::kBoolean, 0);
		nAttr.setKeyable(true);
		nAttr.setStorable (true);
	//	nAttr.setInternal (true);
		nAttr.setHidden(false);
		stat = addAttribute(aUseAsScale);
		if (!stat) return stat;
	}
/*/	ainterp_type = eAttr.create("interp_type", "it", LINEARY);
		eAttr.addField("lineary", LINEARY);
		eAttr.addField("cosine", COSINE);
		eAttr.addField("cubic", CUBIC);
		eAttr.setStorable(false);
	stat = addAttribute(ainterp_type);
	if (!stat) return stat;
/*/

	aseed = nAttr.create("seed", "sd", MFnNumericData::kInt, 0);
	nAttr.setWritable(true);
	stat = addAttribute(aseed);

	// INPUT
	{
		ainputX = nAttr.create("inputX", "inx", MFnNumericData::kFloat, 0.0);
		nAttr.setWritable(true);
		nAttr.setStorable(true);
		nAttr.setHidden(true);
		stat = addAttribute(ainputX);
		if (!stat) return stat;

		ainputY = nAttr.create("inputY", "iny", MFnNumericData::kFloat, 0.0);
		nAttr.setWritable(true);
		nAttr.setStorable(true);
		nAttr.setHidden(true);
		stat = addAttribute(ainputY);
		if (!stat) return stat;

		ainputZ = nAttr.create("inputZ", "inz", MFnNumericData::kFloat, 0.0);
		nAttr.setWritable(true);
		nAttr.setStorable(true);
		nAttr.setHidden(true);
		stat = addAttribute(ainputZ);
		if (!stat) return stat;

		ainput = nAttr.create("input", "in", ainputX, ainputY, ainputZ);
		nAttr.setWritable(true);
		nAttr.setStorable(true);
		nAttr.setHidden(false);
		nAttr.setArray(true);
		stat = addAttribute(ainput);
		if (!stat) return stat;
	}

	// OUTPUT
	{
		aoutputX = nAttr.create("outputX", "outx", MFnNumericData::kFloat, 0.0);
//		nAttr.setWritable(true);
//		nAttr.setStorable(true);
		nAttr.setWritable(false);
		nAttr.setStorable(false);
		nAttr.setHidden(true);
		stat = addAttribute(aoutputX);
		if (!stat) return stat;

		aoutputY = nAttr.create("outputY", "outy", MFnNumericData::kFloat, 0.0);
//		nAttr.setWritable(true);
//		nAttr.setStorable(true);
		nAttr.setWritable(false);
		nAttr.setStorable(false);
		nAttr.setHidden(true);
		stat = addAttribute(aoutputY);
		if (!stat) return stat;

		aoutputZ = nAttr.create("outputZ", "outz", MFnNumericData::kFloat, 0.0);
//		nAttr.setWritable(true);
//		nAttr.setStorable(true);
		nAttr.setWritable(false);
		nAttr.setStorable(false);
		nAttr.setHidden(true);
		stat = addAttribute(aoutputZ);
		if (!stat) return stat;

		aoutput = nAttr.create("output", "out", aoutputX, aoutputY, aoutputZ);
//		nAttr.setWritable(true);
//		nAttr.setStorable(true);
//		nAttr.setStorable(true);
		nAttr.setHidden(false);
		nAttr.setWritable(false);
		nAttr.setStorable(false);
		nAttr.setArray(true);
		stat = addAttribute(aoutput);
		if (!stat) return stat;
	}

	/*/
	{
		createFloatRampAttribute(rmpattr, "clumpFlatness", "cfl");
	}
	/*/

	/*/
	{
		MFnTypedAttribute typedAttr;
		MFnVectorArrayData vectorArrayData;
		MVectorArray array;

		o_vectarray = typedAttr.create( "zzzzz", "zzz",
			MFnData::kVectorArray, vectorArrayData.create(array, &stat), &stat );
		if (!stat) return stat;
		typedAttr.setReadable(true);
		typedAttr.setWritable(true);
		typedAttr.setCached(true);
		typedAttr.setStorable(true);
		stat = addAttribute(o_vectarray);
		if (!stat) return stat;
	}
	/*/

	// set up attribute relationships
	attributeAffects(atime, aoutput);
	attributeAffects(atime, aoutputX);
	attributeAffects(atime, aoutputY);
	attributeAffects(atime, aoutputZ);

	attributeAffects(ainput, aoutput);
	attributeAffects(ainputX, aoutputX);
	attributeAffects(ainputY, aoutputY);
	attributeAffects(ainputZ, aoutputZ);

//    attributeAffects(alayers, aoutput);
//    attributeAffects(alayers, aoutputX);
//    attributeAffects(alayers, aoutputY);
//    attributeAffects(alayers, aoutputZ);
   
	attributeAffects(amagX, aoutput);
    attributeAffects(amagX, aoutputX);
    attributeAffects(amagX, aoutputY);
    attributeAffects(amagX, aoutputZ);
    attributeAffects(amagY, aoutput);
    attributeAffects(amagY, aoutputX);
    attributeAffects(amagY, aoutputY);
    attributeAffects(amagY, aoutputZ);
	attributeAffects(amagZ, aoutput);
	attributeAffects(amagZ, aoutputX);
	attributeAffects(amagZ, aoutputY);
	attributeAffects(amagZ, aoutputZ);
	attributeAffects(afreqX, aoutput);
	attributeAffects(afreqX, aoutputX);
	attributeAffects(afreqX, aoutputY);
	attributeAffects(afreqX, aoutputZ);
	attributeAffects(afreqY, aoutput);
	attributeAffects(afreqY, aoutputX);
	attributeAffects(afreqY, aoutputY);
	attributeAffects(afreqY, aoutputZ);
	attributeAffects(afreqZ, aoutput);
	attributeAffects(afreqZ, aoutputX);
	attributeAffects(afreqZ, aoutputY);
	attributeAffects(afreqZ, aoutputZ);
//	attributeAffects(ainterp_type, aoutput);
//	attributeAffects(ainterp_type, aoutputX);
//	attributeAffects(ainterp_type, aoutputY);
//	attributeAffects(ainterp_type, aoutputZ);
	attributeAffects(aseed, aoutput);
	attributeAffects(aseed, aoutputX);
	attributeAffects(aseed, aoutputY);
	attributeAffects(aseed, aoutputZ);

	return stat;
} 

JitterNode::JitterNode()
{
//	bInitiated = false;
}

JitterNode::~JitterNode()
{
}

/* Ken Perlin */

#define DOT(a,b) (a[0] * b[0] + a[1] * b[1] + a[2] * b[2]) 
#define B 256 
static int p[B +B +2]; 
static float g[B + B + 2][3]; 
static int start = 1; 

#define setup(i,b0,b1,r0,r1) t = i + 10000.0f; b0 = ((int)t) & (B-1);  b1 = (b0+1) & (B-1);  r0 = t - (int)t;  r1 = r0 - 1.0f; 


// Compute the offset and add it to input
// as the output from this node.
MStatus JitterNode::compute( const MPlug& _plug, MDataBlock& data )
{
	/*/
	if( !bInitiated)
	{
		MStatus stat;
		MRampAttribute ramp(rmpattr, thisMObject());
		MFloatArray positions(1, 0.5);
		MFloatArray values(1, 0.5);
		MIntArray interps(1, 2);
//		ramp.addEntries( positions, values, interps, &stat);
		MIntArray ind(1, 2);
		ramp.getEntries( ind, positions, values, interps, &stat);
		bInitiated = true;
	}
	/*/
	MStatus returnStatus, stat;
	MPlug plug = _plug;
//	if((plug == aoutput)||(plug == aoutputX)||(plug == aoutputY)||(plug == aoutputZ))
	if( plug == aoutputX || plug == aoutputY || plug == aoutputZ)
	{
		plug = plug.parent();
	}
	if( plug == aoutput)
	{		
		int arrayIndex = plug.logicalIndex( &stat );

		MDataHandle timevData = data.inputValue(atime);
//		MDataHandle scaleData = data.inputValue(ascale);
//		MDataHandle layerData = data.inputValue(alayers);
		MDataHandle magXData = data.inputValue(amagX);
		MDataHandle magYData = data.inputValue(amagY);
		MDataHandle magZData = data.inputValue(amagZ);
		MDataHandle freqXData = data.inputValue(afreqX);
		MDataHandle freqYData = data.inputValue(afreqY);
		MDataHandle freqZData = data.inputValue(afreqZ);
//		MDataHandle interp_typeData = data.inputValue(ainterp_type);
		MDataHandle seedData = data.inputValue(aseed);
		bool bUseAsScale = data.inputValue(aUseAsScale).asBool();

		float resX = 0, resY = 0, resZ = 0;
		MArrayDataHandle inArrayHandle = data.outputArrayValue(ainput);
		stat = inArrayHandle.jumpToElement(arrayIndex);
		if(stat)
		{
			MDataHandle inHandle = inArrayHandle.inputValue(&stat);
			if(stat)
			{
				float3& input = inHandle.asFloat3();
				resX = input[0];
				resY = input[1];
				resZ = input[2];
//displayString("in %d> %f %f %f", arrayIndex, resX, resY, resZ);
			}
		}

//		MDataHandle inHandle = data.inputValue(ainput);
//		MDataHandle inXHandle = data.inputValue(ainputX);
//		MDataHandle inYHandle = data.inputValue(ainputY);
//		MDataHandle inZHandle = data.inputValue(ainputZ);

		float lacunarity = 0;
		float layer = 0;
		float magfX = 0;
		float magfY = 0;
		float magfZ = 0;
		float freqXFactor = 0;
		float freqYFactor = 0;
		float freqZFactor = 0;

		double currentFrame = timevData.asDouble();
		magfX = (float)magXData.asDouble();
		magfY = (float)magYData.asDouble();
		magfZ = (float)magZData.asDouble();
		freqXFactor = (float)freqXData.asDouble();
		freqYFactor = (float)freqYData.asDouble();
		freqZFactor = (float)freqZData.asDouble();
		seedValue = seedData.asInt();
		double tmp0, tmp1, tmp2, x, y, z;

		srand(seedValue+arrayIndex);
		float t1 = B * rand()/(double)RAND_MAX;
		float t2 = B * rand()/(double)RAND_MAX;

		MPoint res;
		res.x = floatpnoise3(currentFrame*freqXFactor, t1, t2, 0);
		res.y = floatpnoise3(t1, currentFrame*freqYFactor, t2, 0);
		res.z = floatpnoise3(t1, t2, currentFrame*freqZFactor, 0);

//		resX = inXHandle.asFloat();
//		resY = inYHandle.asFloat();
//		resZ = inZHandle.asFloat();

		resX += magfX*res.x;
		resY += magfY*res.y;
		resZ += magfZ*res.z;

		if(bUseAsScale)
		{
			resX = exp(resX);
			resY = exp(resY);
			resZ = exp(resZ);
			float c = resX*resY*resZ;
			c = pow(c, 1/3.f);
			resX /= c;
			resY /= c;
			resZ /= c;
		};
		MArrayDataHandle outArrayHandle = data.outputArrayValue(aoutput);

		stat = outArrayHandle.jumpToElement(arrayIndex);
		if(!stat) return MS::kFailure;

		MDataHandle outHandle = outArrayHandle.outputValue(&stat);
		if(!stat) return MS::kFailure;

		outHandle.set(resX, resY, resZ);
		outHandle.setClean();

//displayString("%d> %f %f %f", arrayIndex, resX, resY, resZ);

//		MArrayDataBuilder builder = outArrayHandle.builder( &stat );
//		MDataHandle outHandle = builder.addElement( arrayIndex, &stat );
//		outHandle.set(resX, resY, resZ);

//		MDataHandle outHandle = data.outputValue(aoutput);

//		data.setClean(plug);
	}

	else
	{
		return MS::kUnknownParameter;
	}

	return MS::kSuccess;
}


inline MPoint JitterNode::pnoise3( MFloatPoint& vec , int ident) 
{
	return pnoise3( vec.x, vec.y, vec.z, ident);
}

MPoint JitterNode::pnoise3(float vx, float vy, float vz, int ident) 
{ 
	int bx0, bx1, by0, by1, bz0, bz1, b00, b10, b01, b11; 
	float rx0, rx1, ry0, ry1, rz0, rz1, *q, sx, sy, sz, a, b, c, d, t, u, v; 
	register int i, j; 

	if (start) 
	{ 
		start = 0; 
		int x=0;
		init(x); 
	} 
	setup(vx, bx0,bx1, rx0,rx1); 
	setup(vy, by0,by1, ry0,ry1); 
	setup(vz, bz0,bz1, rz0,rz1); 
	i = p[ bx0 ]; 
	j = p[ bx1 ]; 
	b00 = p[ i + by0 ]; 
	b10 = p[ j + by0 ]; 
	b01 = p[ i + by1 ]; 
	b11 = p[ j + by1 ];

	#define at(rx,ry,rz) ( rx * q[0] + ry * q[1] + rz * q[2] ) 
	#define s_curve(t) ( t * t * (3.0f - 2.0f * t) ) 
	#define lerp(t, a, b) ( a + t * (b - a) ) 

	sx = s_curve(rx0);
	sy = s_curve(ry0);
	sz = s_curve(rz0);
	q = g[ b00 + bz0 ];
	u = at(rx0,ry0,rz0);
	q = g[ b10 + bz0 ];
	v = at(rx1,ry0,rz0);
	a = lerp(sx, u, v);
	q = g[ b01 + bz0 ];
	u = at(rx0,ry1,rz0);
	q = g[ b11 + bz0 ]; 
	v = at(rx1,ry1,rz0);
	b = lerp(sx, u, v); 
	c = lerp(sy, a, b);						// interpolate in y at lo x
	q = g[ b00 + bz1 ]; 
	u = at(rx0,ry0,rz1); 
	q = g[ b10 + bz1 ]; 
	v = at(rx1,ry0,rz1); 
	a = lerp(sx, u, v); 
	q = g[ b01 + bz1 ]; 
	u = at(rx0,ry1,rz1); 
	q = g[ b11 + bz1 ]; 
	v = at(rx1,ry1,rz1); 
	b = lerp(sx, u, v); 
	d = lerp(sy, a, b);						// interpolate in y at hi x
	float ddd =  1.5f * lerp(sz, c, d);			// interpolate in z
	return MPoint(ddd, ddd, ddd);
//	return MPoint(lerp(sx, u, v), lerp(sy, a, b), lerp(sz, c, d));			// interpolate in z
}

float JitterNode::floatpnoise3(float vx, float vy, float vz, int ident) 
{ 
	int bx0, bx1, by0, by1, bz0, bz1, b00, b10, b01, b11; 
	float rx0, rx1, ry0, ry1, rz0, rz1, *q, sx, sy, sz, a, b, c, d, t, u, v; 
	register int i, j; 

	if (start) 
	{ 
		start = 0; 
		int x=0;
		init(x); 
	} 
	setup(vx, bx0,bx1, rx0,rx1); 
	setup(vy, by0,by1, ry0,ry1); 
	setup(vz, bz0,bz1, rz0,rz1); 
	i = p[ bx0 ]; 
	j = p[ bx1 ]; 
	b00 = p[ i + by0 ]; 
	b10 = p[ j + by0 ]; 
	b01 = p[ i + by1 ]; 
	b11 = p[ j + by1 ];

	#define at(rx,ry,rz) ( rx * q[0] + ry * q[1] + rz * q[2] ) 
	#define s_curve(t) ( t * t * (3.0f - 2.0f * t) ) 
	#define lerp(t, a, b) ( a + t * (b - a) ) 

	sx = s_curve(rx0);
	sy = s_curve(ry0);
	sz = s_curve(rz0);
	q = g[ b00 + bz0 ];
	u = at(rx0,ry0,rz0);
	q = g[ b10 + bz0 ];
	v = at(rx1,ry0,rz0);
	a = lerp(sx, u, v);
	q = g[ b01 + bz0 ];
	u = at(rx0,ry1,rz0);
	q = g[ b11 + bz0 ]; 
	v = at(rx1,ry1,rz0);
	b = lerp(sx, u, v); 
	c = lerp(sy, a, b);						// interpolate in y at lo x
	q = g[ b00 + bz1 ]; 
	u = at(rx0,ry0,rz1); 
	q = g[ b10 + bz1 ]; 
	v = at(rx1,ry0,rz1); 
	a = lerp(sx, u, v); 
	q = g[ b01 + bz1 ]; 
	u = at(rx0,ry1,rz1); 
	q = g[ b11 + bz1 ]; 
	v = at(rx1,ry1,rz1); 
	b = lerp(sx, u, v); 
	d = lerp(sy, a, b);						// interpolate in y at hi x
	float ddd =  1.5f * lerp(sz, c, d);			// interpolate in z
	return ddd;
//	return MPoint(lerp(sx, u, v), lerp(sy, a, b), lerp(sz, c, d));			// interpolate in z
}

void JitterNode::init(int &id) 
{
//	displayString("JitterNode::init");
	int i, j, k; 
	float v[3], s; 
 
	// Create an array of random gradient vectors uniformly on the
	// unit sphere

	srand(id);

	for (i = 0 ; i < B ; i++) 
	{ 
		do 
		{ 
			// Choose uniformly in a cube
			for (j=0 ; j<3 ; j++) 
				v[j] = (float)((rand() % (B + B)) - B) / B; 
			s = DOT(v,v); 
		} while (s > 1.0);					// If not in sphere try again
		s = sqrtf(s); 
		for (j = 0 ; j < 3 ; j++)			// Else normalize
			g[i][j] = v[j] / s; 
	} 

	// Create a pseudorandom permutation of [1..B]
	for (i = 0 ; i < B ; i++) 
		p[i] = i; 
	for(i=B ;i >0 ;i -=2)
	{ 
		k = p[i]; 
		p[i] = p[j = rand() % B]; 
		p[j] = k; 
	} 
	// Extend g and p arrays to allow for faster indexing
	for(i=0 ;i <B +2 ;i++) 
	{ 
		p[B + i] = p[i]; 
		for (j = 0 ; j < 3 ; j++) 
			g[B + i][j] = g[i][j]; 
	}
}