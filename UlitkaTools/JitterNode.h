#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include <maya/MPxNode.h>
#include <maya/MPoint.h>
#include <maya/MFloatPoint.h>
#include <maya/MString.h> 
#include <maya/MTypeId.h> 
#include <maya/MPlug.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MVector.h>

#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>

class JitterNode : public MPxNode 
{

	public:
		JitterNode();
		virtual ~JitterNode(); 

		virtual	MStatus		compute(const MPlug& plug, MDataBlock& data);

		static	void*		creator();
		static  MStatus		initialize();
	
		static	MObject		atime;		// The time value.
//		static	MObject		ascale;		// Scale of JitterNode.
//		static	MObject		alayers;	// The number of noise layers used
		static  MObject		amagX;   // magnitude X of JitterNode
		static  MObject		amagY;   // magnitude Y of JitterNode
		static  MObject		amagZ;   // magnitude Z of JitterNode
		static  MObject		afreqX;   // noise frequency X (speed of movement)
		static  MObject		afreqY;   // noise frequency Y (speed of movement)
		static  MObject		afreqZ;   // noise frequency Z (speed of movement)
//		static	MObject		ainterp_type; // interpolation method (1=linear, 2=cosine, 3=cubic)
		static	MObject		aseed;	// global seed value

		static	MObject		ainput;			 // The jittered-input value.
		static	MObject		ainputX;		 // The jittered-input value.
		static	MObject		ainputY;		 // The jittered-input value.
		static	MObject		ainputZ;		 // The jittered-input value.

		static	MObject		aoutput;		 // The jittered-output value.
		static	MObject		aoutputX;		 // The jittered-output value.
		static	MObject		aoutputY;		 // The jittered-output value.
		static	MObject		aoutputZ;		 // The jittered-output value.

		static	MObject		aUseAsScale;		 // The jittered-output value.

///		static	MObject		o_vectarray;	 // 

		static void init(int &id);
		MPoint pnoise3(MFloatPoint& vec, int ident);
		MPoint pnoise3(float vx, float vy, float vz, int ident);
		static float floatpnoise3(float vx, float vy, float vz, int ident);
		int seedValue;
		int currentFrame;
		int ident;

		static	MTypeId		id;			 // every node tpye is supposed to have its own ID
										 // since my node isn't registered with Alias/Wavefront
										 // I just made up an ID number 
		
//		static	MObject	rmpattr;
//		bool bInitiated;
//		static bool createFloatRampAttribute(MObject& attr, const char* fullname, const char* shortname);

};