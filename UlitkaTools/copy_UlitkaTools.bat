set BIN_DIR=.\out\ulGeomCache
set BIN_DIR=\\server.ulitka.home\bin70

mkdir %BIN_DIR%
mkdir %BIN_DIR%\bin
mkdir %BIN_DIR%\mel
mkdir %BIN_DIR%\docs

copy binrelease\UlitkaTools.mll		%BIN_DIR%\bin
copy mel\AEHelixNodeTemplate.mel	%BIN_DIR%\mel
copy mel\AEJitterNodeTemplate.mel	%BIN_DIR%\mel
copy mel\UlitkaToolsMenu.mel		%BIN_DIR%\mel

pause