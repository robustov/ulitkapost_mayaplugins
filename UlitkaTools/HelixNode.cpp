
#include "HelixNode.h"

MObject HelixNode::radiusMin;
MObject HelixNode::radiusMax;
MObject HelixNode::pitch;
MObject HelixNode::numCVs;
MObject HelixNode::output;
MTypeId	HelixNode::id(0x80021);

HelixNode::HelixNode() {}
HelixNode::~HelixNode() {}

void* HelixNode::creator()
{
	return new HelixNode();
}

MObject HelixNode::createCurve(float & helixRadiusMin, float & helixRadiusMax, float & helixPitch, float & helixNumCVs, MObject& data, MStatus &stat)
{
	MFnNurbsCurve curveFn;

	unsigned	deg = 3; // Curve Degree
	unsigned	ncvs = (int)helixNumCVs; // Number of CVs
	unsigned	spans = (int)helixNumCVs - deg; // Number of spans
	unsigned	nknots	= spans+2*deg-1; // Number of knots

	double	Hradius; // Helix radius
	double	Hpitch 	= helixPitch; // Helix pitch
	unsigned i;

	MPointArray	 controlVertices;
	MDoubleArray knotSequences;
	if(ncvs<5)
	{
		ncvs = 5;
		spans = 2;
	}
	for (i = 0; i < ncvs; i++)
	{
		Hradius = helixRadiusMin + i * (1.0/(float)ncvs) * (helixRadiusMax - helixRadiusMin);
		controlVertices.append(MPoint(Hradius * cos((double)i), Hpitch * (double)i, Hradius * sin((double)i)));
	}
	for (i = 0; i < nknots; i++)
	{
		knotSequences.append((double)i);
	}
	MObject curve = curveFn.create(controlVertices, knotSequences, deg, MFnNurbsCurve::kOpen, false, false, data, &stat);
	return curve;
}

MStatus HelixNode::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus status;
	if(plug == output)
	{
		MDataHandle inputRadiusMin = data.inputValue(radiusMin, &status);
		MDataHandle inputRadiusMax = data.inputValue(radiusMax, &status);
		MDataHandle inputPitch = data.inputValue(pitch, &status);
		MDataHandle inputNumCVs = data.inputValue(numCVs, &status);

		float radMin = (float)inputRadiusMin.asDouble();
		float radMax = (float)inputRadiusMax.asDouble();
		float pich = (float)inputPitch.asDouble();
		float count = (float)inputNumCVs.asDouble();

		MDataHandle curveHandle = data.outputValue(HelixNode::output);
		MFnNurbsCurveData HelixCreate;

		MObject newOutputData = HelixCreate.create(&status);
		MObject curveOut = createCurve(radMin, radMax, pich, count, newOutputData, status);
		curveHandle.set(newOutputData);
		data.setClean(plug);
	}
	else
	{
		return MS::kUnknownParameter;
	}
	return MS::kSuccess;
}

MStatus HelixNode::initialize()
{
	MFnNumericAttribute numericAttr;
	MFnTypedAttribute	typedAttr;
	MStatus status;

	radiusMin = numericAttr.create( "radiusMin", "rMin", MFnNumericData::kDouble, 0.0, &status);
	if(!status)
	{
		status.perror("ERROR creating HelixNode radiusMin attribute");
		return status;
	}
	numericAttr.setStorable(true);
	numericAttr.setKeyable(true);
	numericAttr.setSoftMin(0);
	numericAttr.setSoftMax(50);
	numericAttr.setDefault(5.0);
	status = addAttribute(radiusMin);
	if(!status)
	{
		status.perror("addAttribute(radiusMin)");
		return status;
	}

	radiusMax = numericAttr.create( "radiusMax", "rMax", MFnNumericData::kDouble, 0.0, &status);
	if(!status)
	{
		status.perror("ERROR creating HelixNode radiusMax attribute");
		return status;
	}
	numericAttr.setStorable(true);
	numericAttr.setKeyable(true);
	numericAttr.setSoftMin(0);
	numericAttr.setSoftMax(50);
	numericAttr.setDefault(5.0);
	status = addAttribute(radiusMax);
	if(!status)
	{
		status.perror("addAttribute(radiusMax)");
		return status;
	}

	pitch = numericAttr.create( "pitch", "p", MFnNumericData::kDouble, 0.0, &status);
	if(!status)
	{
		status.perror("ERROR creating HelixNode pitch attribute");
		return status;
	}
	numericAttr.setStorable(true);
	numericAttr.setKeyable(true);
	numericAttr.setSoftMin(0);
	numericAttr.setSoftMax(50);
	numericAttr.setDefault(1);
	status = addAttribute(pitch);
	if(!status)
	{
		status.perror("addAttribute(pitch)");
		return status;
	}

	numCVs = numericAttr.create( "countCV", "cCV", MFnNumericData::kDouble, 0.0, &status);
	if(!status)
	{
		status.perror("ERROR creating HelixNode numCVs attribute");
		return status;
	}
	numericAttr.setStorable(true);
	numericAttr.setKeyable(true);
	numericAttr.setSoftMin(4);
	numericAttr.setSoftMax(200);
	numericAttr.setDefault(10.0);
	status = addAttribute(numCVs);
	if(!status)
	{
		status.perror("addAttribute(numCVs)");
		return status;
	}

	output=typedAttr.create( "outCurve", "out", MFnNurbsCurveData::kNurbsCurve,&status);
	typedAttr.setStorable(false);
	typedAttr.setHidden(true);
	status = addAttribute(output);

	attributeAffects(radiusMin, output);
	attributeAffects(radiusMax, output);
	attributeAffects(pitch, output);
	attributeAffects(numCVs, output);

	return MS::kSuccess;
}