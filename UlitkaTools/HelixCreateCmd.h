#include <maya/MPxCommand.h>
#include <maya/MArgList.h>
#include <maya/MString.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MPointArray.h>
#include <maya/MPlug.h>
#include <maya/MGlobal.h>
#include <maya/MDGModifier.h>
#include <maya/MFnDagNode.h>

class HelixCreateCmd : public MPxCommand
{
	public:
		HelixCreateCmd();
		virtual ~HelixCreateCmd();
		static void* creator();
		bool isUndoable() const;
		MStatus doIt(const MArgList& args);
		MStatus redoIt();
		MStatus undoIt();

	private:
		MObject MakeCurve();
		MString HelixNodeName, curveName;
		float radius, pitch, numCVs;
};
