
#include "HelixCreateCmd.h"

HelixCreateCmd::HelixCreateCmd()
{
}

HelixCreateCmd::~HelixCreateCmd()
{
}

void* HelixCreateCmd::creator()
{
	return new HelixCreateCmd;
}

bool HelixCreateCmd::isUndoable() const
{
	return true;
}

MObject HelixCreateCmd::MakeCurve()
{

	MFnNurbsCurve curveFn;

	unsigned	deg = 3; // Curve Degree
	unsigned	ncvs = 10; // Number of CVs
	unsigned	spans = ncvs - deg; // Number of spans
	unsigned	nknots	= spans+2*deg-1; // Number of knots
	unsigned	i;

	MPointArray	 controlVertices;
	MDoubleArray knotSequences;
	if(ncvs<5)
	{
		ncvs = 5;
		spans = 2;
	}
	for (i = 0; i < ncvs; i++)
	{
		controlVertices.append(MPoint(0,0,0));
	}
	for (i = 0; i < nknots; i++)
	{
		knotSequences.append((double)i);
	}
	MObject curve = curveFn.create(controlVertices, knotSequences, deg, MFnNurbsCurve::kOpen, false, false, MObject::kNullObj);
//	curveFn.setName("Helix");
	return curve;
}

MStatus HelixCreateCmd::doIt(const MArgList& args)
{
	HelixNodeName = "";
	curveName = "";
	radius = -1;
	pitch = -1;
	numCVs = -1;

	for (unsigned i=0; i < args.length(); i++)
	{
		if ((MString("-name")==args.asString(i))||(MString("-na")==args.asString(i)))
			HelixNodeName = args.asString(++i);
		else if((MString("-radius")==args.asString(i))||(MString("-r")==args.asString(i)))
			radius = (float)args.asDouble(++i);
		else if ((MString("-pitch")==args.asString(i))||(MString("-p")==args.asString(i)))
			pitch = (float)args.asDouble(++i);
		else if ((MString("-countCV")==args.asString(i))||(MString("-cCV")==args.asString(i)))
			numCVs = (float)args.asDouble(++i);
		else
		{
			MString errorMessage = "Invalid flag: ";
			errorMessage += args.asString(i);
			displayError(errorMessage);
			return MS::kFailure;
		}
	}
	return redoIt();
}

MStatus HelixCreateCmd::redoIt()
{
	MStatus stat;
	MFnDependencyNode depNodeFn;

	if (HelixNodeName == "") depNodeFn.create("HelixNode");
	else depNodeFn.create("HelixNode", HelixNodeName);
	HelixNodeName = depNodeFn.name();

	if (radius!=-1)
	{
		MPlug radiusPlug = depNodeFn.findPlug("radius");
		radiusPlug.setValue(radius);
	}
	else if(radius==-1)
	{
		MPlug radiusPlug = depNodeFn.findPlug("radius");
		radiusPlug.setValue(1);
	}
	if (pitch!=-1)
	{
		MPlug pitchPlug = depNodeFn.findPlug("pitch");
		pitchPlug.setValue(pitch);
	}
	else if(pitch==-1)
	{
		MPlug pitchPlug = depNodeFn.findPlug("pitch");
		pitchPlug.setValue(1);
	}
	if (numCVs!=-1)
	{
		MPlug countPlug = depNodeFn.findPlug("countCV");
		countPlug.setValue(numCVs);
	}
	else if(numCVs==-1)
	{
		MPlug countPlug = depNodeFn.findPlug("countCV");
		countPlug.setValue(10);
	}

	MPointArray cvs(10);
	MDoubleArray knots(10);
	MObject curve = MakeCurve();
	MFnDependencyNode CurvNodeFn(curve);
	curveName = CurvNodeFn.setName("helix");
	MFnDagNode tmp(curve);
	MObject ChldNode = tmp.child(0);
	MFnDagNode chld(ChldNode);
	MString tmpName = chld.name();

/*
	const char* oname = curveName.asChar();
	const char* hname = HelixNodeName.asChar();
	const char* tname = tmpName.asChar();
*/
	MPlug outputPlug, createPlug;
	createPlug = chld.findPlug("create");
	outputPlug = depNodeFn.findPlug("outCurve");
	MDGModifier dgModifier;
	dgModifier.connect(outputPlug, createPlug);
	dgModifier.doIt();

	setResult(HelixNodeName);
	return stat;
}

MStatus HelixCreateCmd::undoIt()
{
	MString deleteCmd = "delete ";
	deleteCmd += HelixNodeName;
	MGlobal::executeCommand(deleteCmd);
	deleteCmd = "delete ";
	deleteCmd += curveName;
	MGlobal::executeCommand(deleteCmd);
	return MStatus::kSuccess;
}