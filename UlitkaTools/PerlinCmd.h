#include <maya/MPxCommand.h>
#include <maya/MArgList.h>
#include <maya/MString.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MPointArray.h>
#include <maya/MPlug.h>
#include <maya/MGlobal.h>
#include <maya/MDGModifier.h>
#include <maya/MFnDagNode.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include "JitterNode.h"

class PerlinCmd : public MPxCommand
{
	public:
		PerlinCmd();
		static MSyntax newSyntax();
		virtual ~PerlinCmd();
		static void* creator();
		MStatus doIt(const MArgList& args);

	private:
};
