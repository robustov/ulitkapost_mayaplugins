
#include "PerlinCmd.h"

PerlinCmd::PerlinCmd()
{
}

PerlinCmd::~PerlinCmd()
{
}

void* PerlinCmd::creator()
{
	return new PerlinCmd;
}
MSyntax PerlinCmd::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kDouble);	// shapename0
	syntax.addArg(MSyntax::kDouble);	// filename
	syntax.addArg(MSyntax::kDouble);	// filename
	return syntax;
}


MStatus PerlinCmd::doIt(const MArgList& args)
{
	MStatus stat;
	MArgDatabase argData(syntax(), args);
	MString	arg;

	double x, y, z;
	argData.getCommandArgument(0, x);
	argData.getCommandArgument(1, y);
	argData.getCommandArgument(2, z);

	double r = JitterNode::floatpnoise3(x, y, z, 0);
	setResult(r);
	return MS::kSuccess;
}
