#include <string.h>
#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnDagNode.h>
#include <maya/MDGModifier.h>
#include <maya/MString.h> 
#include <maya/MTypeId.h> 
#include <maya/MPlug.h> 
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MVectorArray.h>
#include <maya/MPointArray.h>
#include <maya/MDoubleArray.h>

class HelixNode : public MPxNode
{
public:
						HelixNode();
	virtual				~HelixNode(); 

	virtual MStatus		compute(const MPlug& plug, MDataBlock& data);
	MObject				createCurve(float & helixRadiusMin, float & helixRadiusMax, float & helixPitch, float & helixNumCVs,
		MObject& data, MStatus &stat);
	static	void*		creator();
	static	MStatus		initialize();

	static	MObject radiusMin;
	static	MObject radiusMax;
	static	MObject pitch;
	static	MObject numCVs;
	static	MObject output;
	static	MTypeId	id;
}; 