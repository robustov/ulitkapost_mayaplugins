#include "pre.h"
#include <maya/MFnPlugin.h>
#include "biConstraint.h"

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

// #include "biConstraint.h"
	stat = plugin.registerNode( 
		biConstraint::typeName, 
		biConstraint::id, 
		biConstraint::creator,
		biConstraint::initialize );
	if (!stat) 
	{
		displayString("cant register %s", biConstraint::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}


	if( !MGlobal::sourceFile("biConstraintMenu.mel"))
	{
		displayString("error source AEmyCmdTemplate.mel");
	}
	else
	{
		if( !MGlobal::executeCommand("biConstraintDebugMenu()"))
		{
			displayString("Dont found biConstraintDebugMenu() command");
		}
		MGlobal::executeCommand("biConstraint_RegistryPlugin()");
	}
	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterNode( biConstraint::id );
	if (!stat) displayString("cant deregister %s", biConstraint::typeName.asChar());


	MGlobal::executeCommand("biConstraintDebugMenuDelete()");
	return stat;
}
