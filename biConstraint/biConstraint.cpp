#include "biConstraint.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MEventMessage.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MDistance.h>
#include <maya/MAnimControl.h>
#include <maya/MFnMatrixData.h>

#include <maya/MGlobal.h>

//MObject biConstraint::i_input;
//MObject biConstraint::o_output;

MObject biConstraint::objects;
MObject biConstraint::driver;

biConstraint::biConstraint()
{
	attachSceneCallbacks();
}
biConstraint::~biConstraint()
{
	detachSceneCallbacks();
}

MStatus biConstraint::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
/*/
	MStatus stat;

	if( plug == o_output )
	{
		MDataHandle inputData = data.inputValue( i_input, &stat );

		MDataHandle outputData = data.outputValue( plug, &stat );

		data.setClean(plug);
		return MS::kSuccess;
	}
/*/
	return MS::kUnknownParameter;
}

void* biConstraint::creator()
{
	return new biConstraint();
}

MStatus biConstraint::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnMessageAttribute meAttr;
	MStatus				stat;

	try
	{
/*/
		{
			i_input = numAttr.create ("i_input","iin", MFnNumericData::kDouble, 0.01, &stat);
			numAttr.setMin(0.0);	
			numAttr.setMax(1.0);	
			::addAttribute(i_input, atInput);
		}
		{
			o_output = numAttr.create( "o_output", "out", MFnNumericData::kDouble, 0.01, &stat);
			::addAttribute(o_output, atOutput);
			stat = attributeAffects( i_input, o_output );
		}
/*/
		{
			objects = meAttr.create( "objects", "objs" );
			::addAttribute( objects, atArray );
		}
		{
			driver = numAttr.create( "driver", "driver", MFnNumericData::kInt, 0 );
			::addAttribute( driver, atInput );
		}

		if( !MGlobal::sourceFile("AEbiConstraintTemplate.mel"))
		{
			displayString("error source AEbiConstraintTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

void biConstraint::postConstructor()
{
	MPlug plugDriver( this->thisMObject(), this->driver );
	this->currentDriver = plugDriver.asInt();
	attrChangedEvent = MNodeMessage::addAttributeChangedCallback( this->thisMObject(), attrChangedCB, this );
}

void biConstraint::attachSceneCallbacks()
{
	timeChangedEvent = MEventMessage::addEventCallback( "timeChanged", timeChangedCB, this );
}

void biConstraint::detachSceneCallbacks()
{
	if( timeChangedEvent ) MMessage::removeCallback( timeChangedEvent );
	timeChangedEvent = 0;

	if( attrChangedEvent ) MMessage::removeCallback( attrChangedEvent );
	attrChangedEvent = 0;

	for( unsigned int i = 0; i < attrChangedEventArray.length(); i++ )
	{
		MMessage::removeCallback( attrChangedEventArray[i] );
	}
	attrChangedEventArray.clear();  
}

void attrChangedCB( MNodeMessage::AttributeMessage msg, MPlug& plug, MPlug& otherPlug, void* clientData )
{
	//MGlobal::displayInfo(MString("attrChangedCB"));

	if( !MAnimControl::isPlaying() )
	{
		//MGlobal::displayInfo( MString("attrChange: ") + plug.info() );

		if ( msg & MNodeMessage::kAttributeEval || msg & MNodeMessage::kAttributeSet )
		{
			//MGlobal::displayInfo( MString("kAttribute"));
			biConstraint* pBiConstraintNode = (biConstraint*)clientData;

			if( plug.attribute() == pBiConstraintNode->driver )
			{
				int driver = plug.asInt();

				MPlugArray objectsPlugArray;
				MPlug objectsPlug( pBiConstraintNode->thisMObject(), pBiConstraintNode->objects );

				objectsPlug.elementByLogicalIndex(driver).connectedTo( objectsPlugArray, true, false );

				MFnDependencyNode nodeFn( objectsPlugArray[0].node() );
				MPlug worldMatrixDriverPlug( objectsPlugArray[0].node(), nodeFn.attribute( "worldMatrix" ) );
				worldMatrixDriverPlug = worldMatrixDriverPlug.elementByLogicalIndex(0);
				MFnMatrixData matrixDataDriverFn( worldMatrixDriverPlug.asMObject() );

				MMatrix worldMatrixDriver = matrixDataDriverFn.matrix();

				MIntArray indices;
				objectsPlug.getExistingArrayAttributeIndices( indices );

				for( unsigned int i = 0; i < objectsPlug.numElements(); i++ )
				{
					objectsPlug.elementByLogicalIndex( indices[i] ).connectedTo( objectsPlugArray, true, false );
					nodeFn.setObject( objectsPlugArray[0].node() );
					MPlug worldMatrixPlug( objectsPlugArray[0].node(), nodeFn.attribute( "worldMatrix" ) );
					worldMatrixPlug = worldMatrixPlug.elementByLogicalIndex(0);
					MFnMatrixData matrixDataFn( worldMatrixPlug.asMObject() );

					MMatrix worldMatrixSlave = matrixDataFn.matrix();

					pBiConstraintNode->offsetMatrices.set( worldMatrixSlave * worldMatrixDriver.inverse(), indices[i] );
				}

				pBiConstraintNode->currentDriver = plug.asInt();
			}
		}
		else if ( msg & MNodeMessage::kConnectionMade )
		{
            //MGlobal::displayInfo( MString("kConnectionMade"));

			biConstraint* pBiConstraintNode = (biConstraint*)clientData;

			if( plug.attribute() == pBiConstraintNode->objects )
			{
				if( otherPlug.node().apiType() == MFn::kTransform )
				{
					pBiConstraintNode->attrChangedEventArray.append( MNodeMessage::addAttributeChangedCallback( otherPlug.node(), attrChangedExternalCB, clientData ) );

					MFnDependencyNode nodeFn( otherPlug.node() );
					MPlug worldMatrixPlug( otherPlug.node(), nodeFn.attribute( "worldMatrix" ) );
					worldMatrixPlug = worldMatrixPlug.elementByLogicalIndex(0);
					MFnMatrixData matrixDataFn( worldMatrixPlug.asMObject() );

					MMatrix worldMatrixSlave = matrixDataFn.matrix();

					MPlug plugDriver( pBiConstraintNode->thisMObject(), pBiConstraintNode->driver );
					int driver = plugDriver.asInt();

					MPlugArray objectsPlugArray;
					MPlug objectsPlug( pBiConstraintNode->thisMObject(), pBiConstraintNode->objects );
					objectsPlug.elementByLogicalIndex(driver).connectedTo( objectsPlugArray, true, false );

					nodeFn.setObject( objectsPlugArray[0].node() );
					MPlug worldMatrixDriverPlug( objectsPlugArray[0].node(), nodeFn.attribute( "worldMatrix" ) );
					worldMatrixDriverPlug = worldMatrixDriverPlug.elementByLogicalIndex(0);
					MFnMatrixData matrixDataDriverFn( worldMatrixDriverPlug.asMObject() );

					MMatrix worldMatrixDriver = matrixDataDriverFn.matrix();

					if( pBiConstraintNode->offsetMatrices.length() <= plug.logicalIndex() )
					{
						pBiConstraintNode->offsetMatrices.setLength( plug.logicalIndex()+1 );
					}
					pBiConstraintNode->offsetMatrices.set( worldMatrixSlave * worldMatrixDriver.inverse(), plug.logicalIndex() );
				}
			}
		}
		else
		{
			return;
		}
	}
}

void attrChangedExternalCB( MNodeMessage::AttributeMessage msg, MPlug& plug, MPlug& otherPlug, void* clientData )
{
	//MGlobal::displayInfo(MString("attrChangedExternalCB"));

	if( !MAnimControl::isPlaying() )
	{
		if ( msg & MNodeMessage::kAttributeEval || msg & MNodeMessage::kAttributeSet )
		{
			//MGlobal::displayInfo( MString("kAttribute") );

			MString plugName = plug.partialName( false, false, false, false, false, true );

			if( plugName.substring(0,8) == "translate" )
			{
				biConstraint* pBiConstraintNode = (biConstraint*)clientData;

				int driver = pBiConstraintNode->currentDriver;

				MPlugArray objectsPlugArray;
				MPlug objectsPlug( pBiConstraintNode->thisMObject(), pBiConstraintNode->objects );

				objectsPlug.elementByLogicalIndex(driver).connectedTo( objectsPlugArray, true, false );

				if( plug.node() == objectsPlugArray[0].node() )
				{
					MFnDependencyNode nodeFn( plug.node() );
					MPlug worldMatrixPlug( plug.node(), nodeFn.attribute( "worldMatrix" ) );
					worldMatrixPlug = worldMatrixPlug.elementByLogicalIndex(0);
					MFnMatrixData matrixDataFn( worldMatrixPlug.asMObject() );

					MMatrix worldMatrixDriver = matrixDataFn.matrix();

					MIntArray indices;
					objectsPlug.getExistingArrayAttributeIndices( indices );

					for( unsigned int i = 0; i < objectsPlug.numElements(); i++ )
					{
						if( indices[i] != driver )
						{
							objectsPlug.elementByLogicalIndex( indices[i] ).connectedTo( objectsPlugArray, true, false );
							nodeFn.setObject( objectsPlugArray[0].node() );

							MMatrix parentMatrix;
							parentMatrix.setToIdentity();

							MFnDagNode dagFn( objectsPlugArray[0].node() );
							if( dagFn.parentCount() > 0 )
							{
								dagFn.setObject( dagFn.parent(0) );
								MPlug worldMatrixParentPlug( dagFn.object(), dagFn.attribute( "worldMatrix" ) );
								worldMatrixParentPlug = worldMatrixParentPlug.elementByLogicalIndex(0);
								MFnMatrixData matrixParentDataFn( worldMatrixParentPlug.asMObject() );

								parentMatrix = matrixParentDataFn.matrix();
							}

							MMatrix slaveMatrix = pBiConstraintNode->offsetMatrices[ indices[i] ] * worldMatrixDriver;
							slaveMatrix = slaveMatrix * parentMatrix.inverse();

							MTransformationMatrix slaveTRMatrix( slaveMatrix );

							MVector slavePosition = slaveTRMatrix.getTranslation( MSpace::kWorld );

							MPlug txSlavePlug( objectsPlugArray[0].node(), nodeFn.attribute( "translateX" ) );
							MPlug tySlavePlug( objectsPlugArray[0].node(), nodeFn.attribute( "translateY" ) );
							MPlug tzSlavePlug( objectsPlugArray[0].node(), nodeFn.attribute( "translateZ" ) );

							txSlavePlug.setValue( MDistance( slavePosition.x ) );
							tySlavePlug.setValue( MDistance( slavePosition.y ) );
							tzSlavePlug.setValue( MDistance( slavePosition.z ) );
						}
					}
				}
				else
				{
					MFnDependencyNode nodeFn( plug.node() );
					MPlug worldMatrixPlug( plug.node(), nodeFn.attribute( "worldMatrix" ) );
					worldMatrixPlug = worldMatrixPlug.elementByLogicalIndex(0);
					MFnMatrixData matrixDataFn( worldMatrixPlug.asMObject() );

					MMatrix worldMatrixSlave = matrixDataFn.matrix();

					MPlugArray objectsPlugArray;
					MPlug objectsPlug( pBiConstraintNode->thisMObject(), pBiConstraintNode->objects );
					objectsPlug.elementByLogicalIndex(driver).connectedTo( objectsPlugArray, true, false );

					nodeFn.setObject( objectsPlugArray[0].node() );
					MPlug worldMatrixDriverPlug( objectsPlugArray[0].node(), nodeFn.attribute( "worldMatrix" ) );
					worldMatrixDriverPlug = worldMatrixDriverPlug.elementByLogicalIndex(0);
					MFnMatrixData matrixDataDriverFn( worldMatrixDriverPlug.asMObject() );

					MMatrix worldMatrixDriver = matrixDataDriverFn.matrix();

                    nodeFn.setObject( plug.node() );
					MPlug messagePlug( plug.node(), nodeFn.attribute( "message" ) );
					MPlugArray messagePlugArray;
					messagePlug.connectedTo( messagePlugArray, false, true );

					if( pBiConstraintNode->offsetMatrices.length() <= messagePlugArray[0].logicalIndex() )
					{
						pBiConstraintNode->offsetMatrices.setLength( messagePlugArray[0].logicalIndex()+1 );
					}
					pBiConstraintNode->offsetMatrices.set( worldMatrixSlave * worldMatrixDriver.inverse(), messagePlugArray[0].logicalIndex() );
				}
			}
		}
		else
		{
			return;
		}
	}
}

void timeChangedCB( void* clientData )
{
	//MGlobal::displayInfo( MString("timeChanged") );

	biConstraint* pBiConstraintNode = (biConstraint*)clientData;
	MPlug plugDriver( pBiConstraintNode->thisMObject(), pBiConstraintNode->driver );
	int driver = plugDriver.asInt();

	MPlugArray objectsPlugArray;
	MPlug objectsPlug( pBiConstraintNode->thisMObject(), pBiConstraintNode->objects );

	objectsPlug.elementByLogicalIndex(driver).connectedTo( objectsPlugArray, true, false );

	if( objectsPlugArray.length() )
	{
		MFnDependencyNode nodeFn( objectsPlugArray[0].node() );

		MPlug txPlug( objectsPlugArray[0].node(), nodeFn.attribute( "translateX" ) );
		MPlug tyPlug( objectsPlugArray[0].node(), nodeFn.attribute( "translateY" ) );
		MPlug tzPlug( objectsPlugArray[0].node(), nodeFn.attribute( "translateZ" ) );

		MDistance tx = txPlug.asMDistance();
		MDistance ty = tyPlug.asMDistance();
		MDistance tz = tzPlug.asMDistance();

		MIntArray indices;
		objectsPlug.getExistingArrayAttributeIndices( indices );

		for( unsigned int i = 0; i < objectsPlug.numElements(); i++ )
		{
			if( indices[i] != driver )
			{
				objectsPlug.elementByLogicalIndex( indices[i] ).connectedTo( objectsPlugArray, true, false );
				nodeFn.setObject( objectsPlugArray[0].node() );

				MPlug txSlavePlug( objectsPlugArray[0].node(), nodeFn.attribute( "translateX" ) );
				MPlug tySlavePlug( objectsPlugArray[0].node(), nodeFn.attribute( "translateY" ) );
				MPlug tzSlavePlug( objectsPlugArray[0].node(), nodeFn.attribute( "translateZ" ) );

				MDistance txSlave;
				MDistance tySlave;
				MDistance tzSlave;

				MMatrix slaveMatrix = pBiConstraintNode->offsetMatrices[ indices[i] ];
				MTransformationMatrix slaveTRMatrix( slaveMatrix );
				MVector slavePosition = slaveTRMatrix.getTranslation( MSpace::kWorld );

				if( txSlavePlug.isConnected() ) txSlave = txSlavePlug.asMDistance();
				else txSlave = slavePosition.x;

				if( tySlavePlug.isConnected() ) tySlave = tySlavePlug.asMDistance();
				else tySlave = slavePosition.y;

				if( tzSlavePlug.isConnected() ) tzSlave = tzSlavePlug.asMDistance();
				else tzSlave = slavePosition.z;

				txSlavePlug.setValue( MDistance( tx.asCentimeters() + txSlave.asCentimeters() ) );
				tySlavePlug.setValue( MDistance( ty.asCentimeters() + tySlave.asCentimeters() ) );
				tzSlavePlug.setValue( MDistance( tz.asCentimeters() + tzSlave.asCentimeters() ) );
			}
		}
	}
}