#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: biConstraintNode.h
//
// Dependency Graph Node: biConstraint

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h>
#include <maya/MMatrixArray.h>
#include <maya/MNodeMessage.h>
#include <maya/MCallbackIdArray.h>

class biConstraint : public MPxNode
{
public:
	biConstraint();
	virtual void postConstructor();
	virtual ~biConstraint(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
//	static MObject i_input;			// Example input attribute
//	static MObject o_output;		// Example output attribute

	static MObject    objects;
	static MObject    driver;

public:
	static MTypeId id;
	static const MString typeName;

	MCallbackIdArray	attrChangedEventArray;
	MMatrixArray		offsetMatrices;
	int					currentDriver;

protected:
	void attachSceneCallbacks();
	void detachSceneCallbacks();

private:
    MCallbackId			timeChangedEvent;
	MCallbackId			attrChangedEvent;
};

void timeChangedCB( void* clientData );
void attrChangedCB( MNodeMessage::AttributeMessage msg, MPlug & plug, MPlug & otherPlug, void* clientData );
void attrChangedExternalCB( MNodeMessage::AttributeMessage msg, MPlug & plug, MPlug & otherPlug, void* clientData );

