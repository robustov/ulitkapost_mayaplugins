set BIN_DIR=.\out\ulParticleEmitter
set BIN_DIR=\\server\bin

mkdir %BIN_DIR%
mkdir %BIN_DIR%\bin
mkdir %BIN_DIR%\mel
mkdir %BIN_DIR%\slim
mkdir %BIN_DIR%\docs

%BIN_DIR%\bin\svn\svn.exe -m "" commit %BIN_DIR%

copy BinRelease85\ulParticleEmitter.mll					%BIN_DIR%\bin

copy Mel\ulParticleEmitterMenu.mel						%BIN_DIR%\mel
copy Mel\AEulParticleEmitterTemplate.mel				%BIN_DIR%\mel

copy docs\ulParticleEmitter.txt							%BIN_DIR%\docs
pause