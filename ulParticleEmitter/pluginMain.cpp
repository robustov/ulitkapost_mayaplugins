#include "pre.h"
#include <maya/MFnPlugin.h>
#include "ulParticleEmitter.h"

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

// #include "ulParticleEmitter.h"
	stat = plugin.registerNode( 
		ulParticleEmitter::typeName, 
		ulParticleEmitter::id, 
		ulParticleEmitter::creator,
		ulParticleEmitter::initialize, 
		MPxNode::kEmitterNode );
	if (!stat) 
	{
		displayString("cant register %s", ulParticleEmitter::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}


	/*/
	if( !MGlobal::sourceFile("ulParticleEmitterMenu.mel"))
	{
		displayString("error source AEmyCmdTemplate.mel");
	}
	else
	/*/
	{
		if( !MGlobal::executeCommand("ulParticleEmitterDebugMenu()"))
		{
			displayString("Dont found ulParticleEmitterDebugMenu() command");
		}
	}
	MGlobal::executeCommand("ulParticleEmitter_RegistryPlugin()");
	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterNode( ulParticleEmitter::id );
	if (!stat) displayString("cant deregister %s", ulParticleEmitter::typeName.asChar());


	MGlobal::executeCommand("ulParticleEmitterDebugMenuDelete()");
	return stat;
}
