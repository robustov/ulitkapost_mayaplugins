#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ulParticleEmitterNode.h
//
// Dependency Graph Node: ulParticleEmitter

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include <maya/MPxEmitterNode.h>

//! ulParticleEmitter
class ulParticleEmitter : public MPxEmitterNode
{
public:
	ulParticleEmitter();
	virtual ~ulParticleEmitter(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	///
	virtual MStatus connectionMade( 
		const MPlug& plug,
		const MPlug& otherPlug,
		bool asSrc 
		);

	static void* creator();
	static MStatus initialize();


	//! Main Generate proc
	void emitOmni(
		double inheritFactor,
		unsigned int seed, 
		double minDistance,
		double maxDistance,
		double emitCount, 
		double emitSpeed, 
		double emitSpeedRandom, 
		MVectorArray& ownerPosArray, 
		MVectorArray& ownerVelArray,
		MDoubleArray& ratePP, 
		MDoubleArray& speedPP,
		MDoubleArray& speedRandomPP, 
		MDoubleArray& minDistancePP, 
		MDoubleArray& maxDistancePP,

		// output
		MVectorArray& fnOutPos,			//!< output positions
		MVectorArray& fnOutVel,			//!< output velocities
	
		std::vector<int>& emitsCountFromP	// ������: ���-�� ������ ������������ �� �������
		);

	
	//! Main Generate proc
	void emitDirectional(
		double inheritFactor,
		unsigned int seed, 
		double minDistance,
		double maxDistance,
		double emitCount, 
		double emitSpeed, 
		double emitSpeedRandom, 
		MVectorArray& ownerPosArray, 
		MVectorArray& ownerVelArray,
		MDoubleArray& ratePP, 
		MDoubleArray& speedPP,
		MDoubleArray& speedRandomPP, 
		MVector direction,
		MVectorArray& directionPP, 
		double spread,
		MDoubleArray& spreadPP,
		MDoubleArray& minDistancePP, 
		MDoubleArray& maxDistancePP,

		// output
		MVectorArray& fnOutPos,			//!< output positions
		MVectorArray& fnOutVel,			//!< output velocities
	
		std::vector<int>& emitsCountFromP	// ������: ���-�� ������ ������������ �� �������
		);

public:
	static MObject i_input;			//!< Example input attribute

public:
	static MTypeId id;
	static const MString typeName;

protected:
	MStatus computeOutput( 
		const MPlug& plug, 
		MDataBlock& data );


protected:
	bool isFullValue(int plugIndex, MDataBlock& block);
	int seedValue(int plugIndex,MDataBlock& block);
	double inheritFactorValue(int plugIndex,MDataBlock& block);
	MTime currentTimeValue( MDataBlock& block );
	MTime startTimeValue( int plugIndex, MDataBlock& block );
	MTime deltaTimeValue( int plugIndex, MDataBlock& block );

	bool getPlugValue(MDataHandle& hValue, MObject attribute, int plugIndex, MDataBlock& block);

	bool ownerVectorArray(MDataBlock& block, MObject attr, MVectorArray& ownerVectorArray);
	bool ownerDoubleArray(MDataBlock& block, MObject attr, MDoubleArray& ownerDoubleArray);
};



inline bool ulParticleEmitter::isFullValue(int plugIndex, MDataBlock& block)
{
	MDataHandle hValue;
	if( !getPlugValue(hValue, mIsFull, plugIndex, block))
		return true;
	return hValue.asBool();
}
inline double ulParticleEmitter::inheritFactorValue(int plugIndex,MDataBlock& block)
{
	MDataHandle hValue;
	if( !getPlugValue(hValue, mInheritFactor, plugIndex, block))
		return 0.0;
	return hValue.asDouble();
}
inline int ulParticleEmitter::seedValue(int plugIndex,MDataBlock& block)
{
	MDataHandle hValue;
	if( !getPlugValue(hValue, mSeed, plugIndex, block))
		return 0;
	return hValue.asLong();
}

inline MTime ulParticleEmitter::currentTimeValue( MDataBlock& block )
{
	MStatus stat;
	MDataHandle hValue = block.inputValue( mCurrentTime, &stat );
	if( !stat) return MTime(0.0);
	return hValue.asTime();
}
inline MTime ulParticleEmitter::startTimeValue( int plugIndex, MDataBlock& block )
{
	MDataHandle hValue;
	if( !getPlugValue(hValue, mStartTime, plugIndex, block))
		return MTime(0.0);
	return hValue.asTime();
}
inline MTime ulParticleEmitter::deltaTimeValue( int plugIndex, MDataBlock& block )
{
	MDataHandle hValue;
	if( !getPlugValue(hValue, mDeltaTime, plugIndex, block))
		return MTime(0.0);
	return hValue.asTime();
}

inline bool ulParticleEmitter::getPlugValue(MDataHandle& hValue, MObject attribute, int plugIndex, MDataBlock& block)
{
	MStatus stat;
	bool value = true;

	MArrayDataHandle mhValue = block.inputArrayValue( attribute, &stat );
	if( stat == MS::kSuccess )
	{
		stat = mhValue.jumpToElement( plugIndex );
		if( stat == MS::kSuccess )
		{
			hValue = mhValue.inputValue( &stat );
			return ( stat == MS::kSuccess );
		}
	}
	return false;
}


