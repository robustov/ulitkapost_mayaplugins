#include "ulParticleEmitter.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnArrayAttrsData.h>
#include <maya/MFnStringData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MQuaternion.h>

#include <maya/MGlobal.h>

MObject ulParticleEmitter::i_input;

MObject i_type;
MObject i_ratePP;
MObject i_speedPP;
MObject i_minDistancePP;
MObject i_maxDistancePP;
MObject i_directionPP;
MObject i_spreadPP;
MObject i_speedRandomPP;


//MObject i_speedRandom;

ulParticleEmitter::ulParticleEmitter()
{
}
ulParticleEmitter::~ulParticleEmitter()
{
}

//! Main Generate proc

void ulParticleEmitter::emitDirectional(
	double inheritFactor,
	unsigned int seed, 
	double _minDistance,
	double _maxDistance,
	double emitCount, 
	double emitSpeed, 
	double emitSpeedRandom, 
	MVectorArray& ownerPosArray, 
	MVectorArray& ownerVelArray,
	MDoubleArray& ratePP, 
	MDoubleArray& speedPP,
	MDoubleArray& speedRandomPP, 
	MVector _direction,
	MVectorArray& directionPP, 
	double _spread,
	MDoubleArray& spreadPP,
	MDoubleArray& minDistancePP, 
	MDoubleArray& maxDistancePP,

	// output
	MVectorArray& fnOutPos,			//!< output positions
	MVectorArray& fnOutVel,			//!< output velocities

	std::vector<int>& emitsCountFromP	// ������: ���-�� ������ ������������ �� �������
	)
{
	srand(seed);
//	displayStringD("seed=%d", seed);

	emitsCountFromP.clear();
	emitsCountFromP.resize(ownerPosArray.length(), 0);
	for( int i=0; i<(int)ownerPosArray.length(); i++)
	{
		// count
		double dcount = emitCount;
		if( i<(int)ratePP.length())
			dcount = ( emitCount*ratePP[i]);
		int count = (int)floor(dcount);
		dcount = dcount-floor(dcount);
		if( rand()/(double)RAND_MAX < dcount)
			count++;

		// distances
		double minDistance = _minDistance;
		if( i<(int)minDistancePP.length())
			minDistance = minDistancePP[i];

		double maxDistance = _maxDistance;
		if( i<(int)maxDistancePP.length())
			maxDistance = maxDistancePP[i];

		if( minDistance>maxDistance)
			count = 0;

		// direction
		MVector direction = _direction;
		if( i<(int)directionPP.length())
			direction = directionPP[i];

		double spread = _spread;
		if( i<(int)spreadPP.length())
			spread = spreadPP[i];

		// generate
		for(int p=0; p<count; p++)
		{
			MVector ort(M_PI, 1.133, 0.777);
			ort = ort^direction;

			// ������ direction
			double ang1 = 2*rand()*M_PI/RAND_MAX;
			ort = ort.rotateBy( MQuaternion( ang1, direction) );
			ort.normalize();

			// �� direction
			double ang2 = spread*rand()*M_PI/RAND_MAX;
			MVector dir = direction.rotateBy( MQuaternion(ang2, ort));
			dir.normalize();

			double dist = rand()*(maxDistance-minDistance)/RAND_MAX;
			MVector newPos = ownerPosArray[i];
			newPos += dir*dist;

			// speed
			double speed = emitSpeed;
			if( i<(int)speedPP.length())
				speed = speedPP[i];
			double speedRandom = emitSpeedRandom;
			if( i<(int)speedRandomPP.length())
				speedRandom = speedRandomPP[i];
			speed *= (1-rand()*speedRandom/(double)RAND_MAX);

			MVector newVel = dir*speed;
			newVel += ownerVelArray[i]*inheritFactor;

			fnOutPos.append( newPos );
			fnOutVel.append( newVel );
		}
		emitsCountFromP[i] = count;
	}
}



//
// OMNI:
// minDistance
// maxDistance
// rate
// ratePP
// speed
// speedPP
// speedRandom
// 
// inherite
//
void ulParticleEmitter::emitOmni(
	double inheritFactor,
	unsigned int seed, 
	double _minDistance,
	double _maxDistance,
	double emitCount, 
	double emitSpeed, 
	double emitSpeedRandom, 
	MVectorArray& ownerPosArray, 
	MVectorArray& ownerVelArray,
	MDoubleArray& ratePP, 
	MDoubleArray& speedPP,
	MDoubleArray& speedRandomPP, 
	MDoubleArray& minDistancePP, 
	MDoubleArray& maxDistancePP,

	// output
	MVectorArray& fnOutPos,			//!< output positions
	MVectorArray& fnOutVel,			//!< output velocities

	std::vector<int>& emitsCountFromP	// ������: ���-�� ������ ������������ �� �������
	)
{
//	MMatrix worldMatrix = data.inputValue(mWorldMatrix).asMatrix();
//	double inheritFactor = data.inputValue(mInheritFactor).asDouble();

	srand(seed);
//	displayStringD("seed=%d", seed);

	emitsCountFromP.clear();
	emitsCountFromP.resize(ownerPosArray.length(), 0);
	for( int i=0; i<(int)ownerPosArray.length(); i++)
	{
		// count
		double dcount = emitCount;
		if( i<(int)ratePP.length())
			dcount = ( emitCount*ratePP[i]);
		int count = (int)floor(dcount);
		dcount = dcount-floor(dcount);
		int _rand = rand();
		if( _rand/(double)RAND_MAX < dcount)
			count++;

		// distances
		double minDistance = _minDistance;
		if( i<(int)minDistancePP.length())
			minDistance = minDistancePP[i];

		double maxDistance = _maxDistance;
		if( i<(int)maxDistancePP.length())
			maxDistance = maxDistancePP[i];

		if( minDistance>maxDistance)
			count = 0;

		// generate
		for(int p=0; p<count; p++)
		{
			double dist = rand()*(maxDistance-minDistance)/RAND_MAX;
			MVector direction;
			for(;;)
			{
				direction = MVector(rand(), rand(), rand());
				direction = (direction*(1.0/RAND_MAX) - MVector(0.5, 0.5, 0.5));
				if(direction.length()>0.5)
					continue;
				direction.normalize();
				break;
			}

			MVector newPos = ownerPosArray[i];
			newPos += direction*dist;

			// speed
			double speed = emitSpeed;
			if( i<(int)speedPP.length())
				speed = speedPP[i];
			double speedRandom = emitSpeedRandom;
			if( i<(int)speedRandomPP.length())
				speedRandom = speedRandomPP[i];
			speed *= (1-rand()*speedRandom/(double)RAND_MAX);

			MVector newVel = direction*speed;
			newVel += ownerVelArray[i]*inheritFactor;

			fnOutPos.append( newPos );
			fnOutVel.append( newVel );
		}
		emitsCountFromP[i] = count;
	}

}

MStatus ulParticleEmitter::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	if( plug == mOutput )
		return computeOutput(plug, data);

	return MS::kUnknownParameter;
}

///
MStatus ulParticleEmitter::connectionMade( 
	const MPlug& plug,
	const MPlug& otherPlug,
	bool asSrc 
	)
{
	MStatus stat;
	displayStringD("ulParticleEmitter::connectionMade %s %s", plug.name().asChar(), otherPlug.name().asChar());

	if( !asSrc)
	{
		MFnAttribute attr( plug.attribute());
		if( attr.isDynamic() && attr.hasObj(MFn::kTypedAttribute))
		{
			MFnTypedAttribute tattr(plug.attribute());
			// �������� ���?
			if( tattr.attrType()==MFnData::kVectorArray ||
				tattr.attrType()==MFnData::kDoubleArray)
			{
				// ������� ���
				MFnDependencyNode dn(thisMObject());

				MFnStringData stringData;
				MFnTypedAttribute typedAttr;
				MObject dstName = typedAttr.create( attr.name()+"_TO", attr.name()+"_TO", MFnData::kString, stringData.create(attr.name(), &stat), &stat);
				typedAttr.setConnectable(true);
				typedAttr.setStorable(true);
				typedAttr.setWritable(true);
				typedAttr.setReadable(true);
				typedAttr.setHidden(false);
				typedAttr.setKeyable(true);
				dn.addAttribute(dstName);
			}
		}
	}
	

	return MPxEmitterNode::connectionMade( 
		plug,
		otherPlug,
		asSrc 
		);
}

MStatus ulParticleEmitter::computeOutput( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;
	// Get the logical index of the element this plug refers to,
	// because the node can be emitting particles into more 
    // than one particle shape.
	int multiIndex = plug.logicalIndex( &stat );

	// output
	MArrayDataHandle hOutArray = data.outputArrayValue( mOutput, &stat);
	MArrayDataBuilder bOutArray = hOutArray.builder( &stat );
	MDataHandle hOut = bOutArray.addElement(multiIndex, &stat);
	MFnArrayAttrsData fnOutput;
	MObject dOutput = fnOutput.create ( &stat );

	// Check if the particle object has reached it's maximum,
	bool beenFull = this->isFullValue( multiIndex, data );
	if( beenFull )
	{
		return( MS::kSuccess );
	}

	// Get deltaTime, currentTime and startTime.
	MTime cT = this->currentTimeValue( data );
	MTime sT = this->startTimeValue( multiIndex, data );
	MTime dT = this->deltaTimeValue( multiIndex, data );
	if( (cT <= sT) || (dT <= 0.0) )
	{
		hOut.set( dOutput );
		data.setClean( plug );
		return( MS::kSuccess );
	}


	// ����:
	MVectorArray ownerPosArray, ownerVelArray;

	if( !ownerVectorArray(data, mOwnerPosData, ownerPosArray))
		return( MS::kSuccess );

	if( !ownerVectorArray(data, mOwnerVelData, ownerVelArray))
		return( MS::kSuccess );
	
	if( ownerVelArray.length()!=ownerPosArray.length())
		return( MS::kSuccess );

	int type = data.inputValue(i_type, &stat).asShort();
	
	MDoubleArray ratePP, speedPP, minDistancePP, maxDistancePP;
	ownerDoubleArray(data, i_ratePP, ratePP);
	ownerDoubleArray(data, i_speedPP, speedPP);
	ownerDoubleArray(data, i_minDistancePP, minDistancePP);
	ownerDoubleArray(data, i_maxDistancePP, maxDistancePP);
//	i_directionPP
	MVectorArray directionPP;
	ownerVectorArray(data, i_directionPP, directionPP);

	// �� ���������
    double rate = data.inputValue(mRate, &stat).asDouble();
	double emitCount = rate*dT.as( MTime::kSeconds );
    double speed = data.inputValue(mSpeed, &stat).asDouble();

	MFnDependencyNode thisDN(thisMObject());
	MPlug i_speedRandom = thisDN.findPlug("speedRandom");
	double speedRandom = data.inputValue(i_speedRandom, &stat).asDouble();
	MPlug i_spread = thisDN.findPlug("spread");
	double spread = data.inputValue(i_spread, &stat).asDouble();
	MDoubleArray spreadPP;
	ownerDoubleArray(data, i_spreadPP, spreadPP);
	MDoubleArray speedRandomPP;
	ownerDoubleArray(data, i_speedRandomPP, speedRandomPP);
	


	double inheritFactor = inheritFactorValue( multiIndex, data );
	unsigned int seed = (unsigned int)seedValue( multiIndex, data );
	double dseed = cT.as(MTime::k100FPS);
	seed += 0xFFFFFF & ((unsigned int)dseed);
	displayStringD("ulParticleEmitter::seed = %x", seed);

    double minDistance = data.inputValue(mMinDistance, &stat).asDouble();
    double maxDistance = data.inputValue(mMaxDistance, &stat).asDouble();

	// OUTPUT
	MVectorArray fnOutPos = fnOutput.vectorArray("position", &stat);
    MVectorArray fnOutVel = fnOutput.vectorArray("velocity", &stat);
	MVector direction(0, 1, 0);

	// ������: ���-�� ������ ������������ �� �������
	std::vector<int> emitsCountFromP;

	switch( type)
	{
	case 0:
		emitOmni(
			inheritFactor, 
			seed, 
			minDistance,
			maxDistance,
			emitCount, 
			speed, 
			speedRandom, 
			ownerPosArray, 
			ownerVelArray,
			ratePP, 
			speedPP, 
			speedRandomPP, 
			minDistancePP, 
			maxDistancePP, 
			fnOutPos,			//!< output positions
			fnOutVel,			//!< output velocities
			emitsCountFromP
			);
		break;
	case 1:
		emitDirectional(
			inheritFactor, 
			seed, 
			minDistance,
			maxDistance,
			emitCount, 
			speed, 
			speedRandom, 
			ownerPosArray, 
			ownerVelArray,
			ratePP, 
			speedPP, 
			speedRandomPP, 
			direction, 
			directionPP, 
			spread,
			spreadPP,
			minDistancePP, 
			maxDistancePP, 
			fnOutPos,			//!< output positions
			fnOutVel,			//!< output velocities
			emitsCountFromP
			);
		break;
	}


	
	for( int a=0; a<(int)thisDN.attributeCount(); a++)
	{
		MObject aobj = thisDN.attribute(a);
		MFnAttribute attr(aobj);
		// ������������ �������?
		if( attr.isDynamic() && attr.hasObj(MFn::kTypedAttribute))
		{
			MFnTypedAttribute tattr(aobj);
			// �������� ���?
			if( tattr.attrType()==MFnData::kVectorArray ||
				tattr.attrType()==MFnData::kDoubleArray)
			{
				// ���� �� ������� � ������
				MString name = tattr.name()+"_TO";
				MPlug plugname = thisDN.findPlug(name);

				std::string n = data.inputValue(plugname).asString().asChar();
				if( n.empty()) 
					continue;

				// �����
				if( tattr.attrType()==MFnData::kVectorArray)
				{
					MVectorArray srcData;
					ownerVectorArray(data, aobj, srcData);

					MVectorArray dstData = fnOutput.vectorArray(n.c_str(), &stat);
					for( int i=0; i<(int)srcData.length(); i++)
					{
						if( i>=(int)emitsCountFromP.size())
							break;
						int count = emitsCountFromP[i];
						for(int p=0; p<count; p++)
							dstData.append(srcData[i]);
					}
				}
				else if( tattr.attrType()==MFnData::kDoubleArray)
				{
					MDoubleArray srcData;
					ownerDoubleArray(data, aobj, srcData);

					MDoubleArray dstData = fnOutput.doubleArray(n.c_str(), &stat);
					for( int i=0; i<(int)srcData.length(); i++)
					{
						if( i>=(int)emitsCountFromP.size())
							break;
						int count = emitsCountFromP[i];
						for(int p=0; p<count; p++)
							dstData.append(srcData[i]);
					}
				}
			}
		}
	}
/*/
//	MDoubleArray mass = fnOutput.doubleArray("mass", &stat);
	MDoubleArray sax = fnOutput.doubleArray("sax", &stat);
	for(unsigned p=0; p<fnOutPos.length(); p++)
	{
		float r = 0.001f+rand()/(float)RAND_MAX;
//		mass.append(r);
		sax.append(r);
	}
/*/

	hOut.set( dOutput );
	data.setClean( plug );

	return( MS::kSuccess );
}

void* ulParticleEmitter::creator()
{
	return new ulParticleEmitter();
}

MStatus ulParticleEmitter::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnEnumAttribute	enumAttr;
	MStatus				stat;

	try
	{
		{
			i_type = enumAttr.create("type", "tp", 0);
			enumAttr.addField("Omni", 0);
			enumAttr.addField("Directional", 1);
			::addAttribute(i_type, atInput);
		}

		{
			i_ratePP = typedAttr.create( "emitratePP", "erpp", MFnData::kDoubleArray, &stat);
			::addAttribute(i_ratePP, atReadable|atWritable|atUnStorable|atCached|atHidden);
		}
		{
			i_speedPP = typedAttr.create( "emitspeedPP", "espp", MFnData::kDoubleArray, &stat);
			::addAttribute(i_speedPP, atReadable|atWritable|atUnStorable|atCached|atHidden);
		}
		{
			i_minDistancePP = typedAttr.create( "minDistancePP", "midpp", MFnData::kDoubleArray, &stat);
			::addAttribute(i_minDistancePP, atReadable|atWritable|atUnStorable|atCached|atHidden);
		}
		{
			i_maxDistancePP = typedAttr.create( "maxDistancePP", "madpp", MFnData::kDoubleArray, &stat);
			::addAttribute(i_maxDistancePP, atReadable|atWritable|atUnStorable|atCached|atHidden);
		}
		{
			i_directionPP = typedAttr.create( "directionPP", "drpp", MFnData::kVectorArray, &stat);
			::addAttribute(i_directionPP, atReadable|atWritable|atUnStorable|atCached|atHidden);
		}
		{
			i_spreadPP = typedAttr.create( "spreadPP", "sprpp", MFnData::kDoubleArray, &stat);
			::addAttribute(i_spreadPP, atReadable|atWritable|atUnStorable|atCached|atHidden);
		}
		{
			i_speedRandomPP = typedAttr.create( "speedRandomPP", "srpp", MFnData::kDoubleArray, &stat);
			::addAttribute(i_speedRandomPP, atReadable|atWritable|atUnStorable|atCached|atHidden);
		}
//		{
//			MFnStringData stringData;
//			i_srcAttr = typedAttr.create( "srcAttr", "sra", MFnData::kString, stringData.create("mass", &stat), &stat);
//			::addAttribute(i_srcAttr, atInput);
//			i_dstAttr = typedAttr.create( "dstAttr", "dra", MFnData::kString, stringData.create("sax", &stat), &stat);
//			::addAttribute(i_dstAttr, atInput);
//		}



		if( !MGlobal::sourceFile("AEulParticleEmitterTemplate.mel"))
		{
			displayString("error source AEulParticleEmitterTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}


bool ulParticleEmitter::ownerVectorArray(
	MDataBlock& block,
	MObject attr, 
	MVectorArray &ownerVectorArray
	)
{
	MStatus status;

	bool hasOwner = false;

	MDataHandle hOwnerPos = block.inputValue( attr, &status );
	if( status == MS::kSuccess )
	{
		MObject dOwnerPos = hOwnerPos.data();
		MFnVectorArrayData fnOwnerPos( dOwnerPos );
		MVectorArray posArray = fnOwnerPos.array( &status );
		if( status == MS::kSuccess )
		{
			ownerVectorArray = posArray;
			return true;
		}
	}
	return false;
}
bool ulParticleEmitter::ownerDoubleArray(
	MDataBlock& block,
	MObject attr, 
	MDoubleArray& ownerDoubleArray
	)
{
	MStatus status;

	bool hasOwner = false;

	MDataHandle hOwnerPos = block.inputValue( attr, &status );
	if( status == MS::kSuccess )
	{
		MObject dOwnerPos = hOwnerPos.data();
		MFnDoubleArrayData fnOwnerPos( dOwnerPos );
		MDoubleArray posArray = fnOwnerPos.array( &status );
		if( status == MS::kSuccess )
		{
			ownerDoubleArray = posArray;
			return true;
		}
	}
	return false;
}
