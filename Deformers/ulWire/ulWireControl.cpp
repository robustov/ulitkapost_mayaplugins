#include "ulWireControl.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnDoubleArrayData.h>

#include <maya/MGlobal.h>

MTypeId ulWireControl::id( TypeIdBase+03 );

MObject ulWireControl::i_controlCurve;
	MObject ulWireControl::i_controlCurveMatrix;
MObject ulWireControl::i_controlCurveBase;
	MObject ulWireControl::i_controlCurveBaseMatrix;

MObject ulWireControl::i_usePaintedWeights;
MObject ulWireControl::i_wireDistance[3];
MObject ulWireControl::i_twist[3];
MObject ulWireControl::i_scale[2];
MObject ulWireControl::i_affect_rotate[1];

MObject ulWireControl::i_ulRampData[5];

MObject ulWireControl::i_testweightList;
	MObject ulWireControl::i_testWeghts;

ulWireControl::ulWireControl()
{
}
ulWireControl::~ulWireControl()
{
}

bool ulWireControl::getInternalValue( 
	const MPlug& plug,
	MDataHandle& data)
{
	displayString("getInternalValue %s", plug.name().asChar());
	return MPxNode::getInternalValue( plug, data);
}
///
bool ulWireControl::setInternalValue( 
	const MPlug& plug,
	const MDataHandle& data)
{
	displayString("setInternalValue %s", plug.name().asChar());
	return MPxNode::setInternalValue( plug, data);
}

MStatus ulWireControl::Build_Ramp(MObject& attr, MDataBlock& data)
{
	MStatus stat;

	ulRampData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( attr, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<ulRampData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( ulRampData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<ulRampData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	// distance
	if(attr == i_ulRampData[0])
	{
		bool usePaintedWeights = data.inputValue(this->i_usePaintedWeights,&stat).asBool();

		float wireDistance = (float)data.inputValue(this->i_wireDistance[0],&stat).asDouble();

		MRampAttribute rampattr(thisMObject(), this->i_wireDistance[1]);
		if( !usePaintedWeights)
		{
			pData->ramp.set( rampattr, 0.f, 1.f, 1.f);
			pData->ramp *= wireDistance;
		}
		else
			pData->ramp.clear(-1.f);

		// ��� ��� ���� �����??????????
		MArrayDataHandle ccma = data.inputArrayValue( i_testweightList);
		if(ccma.elementCount()) do
		{
			int index = ccma.elementIndex();
			MDataHandle dh = ccma.inputValue();
			MDataHandle dhch = dh.child(i_testWeghts);
			displayStringD("---%d: ", index);
			MArrayDataHandle za(dhch);

			if(za.elementCount()) do
			{
				int index2 = za.elementIndex();
				float s = za.inputValue().asFloat();
				displayStringD("%d %d: %f", index, index2, s);
			}while( za.next());
		}while( ccma.next());
	}
	else if(attr == i_ulRampData[1])
	{
		MRampAttribute twist(thisMObject(), this->i_twist[0]);
		pData->ramp.set( twist, (float)-M_PI/2, (float)M_PI/2, 0.f);
		// scale
		float twistscale = (float)data.inputValue(this->i_twist[2],&stat).asDouble();
		pData->ramp *= twistscale;
		// offset
		MAngle twistoffset = data.inputValue(this->i_twist[1],&stat).asAngle();
		pData->ramp += (float)twistoffset.as(MAngle::kRadians);
	}
	else if(attr == i_ulRampData[2])
	{
		float scale = (float)data.inputValue(this->i_scale[0],&stat).asDouble();

		MRampAttribute rampattr(thisMObject(), this->i_scale[1]);
		pData->ramp.set( rampattr, 0.f, 1.f, 1.f);
		pData->ramp *= scale;
	}
	else if(attr == i_ulRampData[3])
	{
		// distance factor
		MRampAttribute rampattr(thisMObject(), this->i_wireDistance[2]);
		pData->ramp.set( rampattr, 0.f, 1.f, 1.f);
	}
	else if(attr == i_ulRampData[4])
	{
		// affect rotate
		MRampAttribute rampattr(thisMObject(), this->i_affect_rotate[0]);
		pData->ramp.set( rampattr, 0.f, 1.f, 1.f);
	}

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

MStatus ulWireControl::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;
	if( plug==i_ulRampData[0] || 
		plug==i_ulRampData[1] ||
		plug==i_ulRampData[2] ||
		plug==i_ulRampData[3] ||
		plug==i_ulRampData[4]
		)
	{
		return Build_Ramp(plug.attribute(), data);
	}
	return MS::kUnknownParameter;
}

void* ulWireControl::creator()
{
	return new ulWireControl();
}

MStatus ulWireControl::initialize()
{
	MFnNumericAttribute nAttr;
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnCompoundAttribute cAttr;
	MFnUnitAttribute	unitAttr;
	MStatus				stat;

	try
	{
		i_ulRampData[0] = typedAttr.create( "dist_ramp", "dira", ulRampData::id);
		::addAttribute(i_ulRampData[0], atReadable|atWritable|atCached|atUnStorable|atConnectable );
		i_ulRampData[1] = typedAttr.create( "twist_ramp", "twira", ulRampData::id);
		::addAttribute(i_ulRampData[1], atReadable|atWritable|atCached|atUnStorable|atConnectable );
		i_ulRampData[2] = typedAttr.create( "scale_ramp", "scara", ulRampData::id);
		::addAttribute(i_ulRampData[2], atReadable|atWritable|atCached|atUnStorable|atConnectable );
		i_ulRampData[3] = typedAttr.create( "dist_factor_ramp", "difra", ulRampData::id);
		::addAttribute(i_ulRampData[3], atReadable|atWritable|atCached|atUnStorable|atConnectable );
		i_ulRampData[4] = typedAttr.create( "affectrotate_ramp", "afrra", ulRampData::id);
		::addAttribute(i_ulRampData[4], atReadable|atWritable|atCached|atUnStorable|atConnectable );
//		stat = attributeAffects( i_ulRampData, outputGeom );


		// controlCurve
		{
			i_controlCurve = typedAttr.create( "controlCurve", "cc", MFnData::kNurbsCurve, &stat);
			::addAttribute(i_controlCurve, atReadable|atWritable);
//			stat = attributeAffects( i_controlCurve, outputGeom );

			i_controlCurveMatrix = typedAttr.create( "controlCurveMatrix", "ccm", MFnData::kMatrix, &stat);
			::addAttribute(i_controlCurveMatrix, atReadable|atWritable);
//			stat = attributeAffects( i_controlCurveMatrix, outputGeom );
		}

		// controlCurveBase
		{
			i_controlCurveBase = typedAttr.create( "controlCurveBase", "ccb", MFnData::kNurbsCurve, &stat);
			::addAttribute(i_controlCurveBase, atReadable|atWritable);
//			stat = attributeAffects( i_controlCurveBase, outputGeom );
//			stat = attributeAffects( i_controlCurveBase, i_cache);

			i_controlCurveBaseMatrix = typedAttr.create( "controlCurveBaseMatrix", "ccbm", MFnData::kMatrix, &stat);
			::addAttribute(i_controlCurveBaseMatrix, atReadable|atWritable);
//			stat = attributeAffects( i_controlCurveBaseMatrix, i_cache);
//			stat = attributeAffects( i_controlCurveBaseMatrix, outputGeom );
		}

		// distance
		{
			i_usePaintedWeights = nAttr.create( "usePaintedWeights", "upw", MFnNumericData::kBoolean, 0);
			::addAttribute( i_usePaintedWeights, atInput|atKeyable);
			stat = attributeAffects( i_usePaintedWeights, i_ulRampData[0] );

			i_wireDistance[0] = nAttr.create( "wireDistance", "wd", MFnNumericData::kDouble, 1);
			nAttr.setMin(0);
			nAttr.setSoftMax(100);
			::addAttribute( i_wireDistance[0], atInput|atKeyable);
			stat = attributeAffects( i_wireDistance[0], i_ulRampData[0] );

			i_wireDistance[1] = CreateRampAttr("wireDistanceRamp", "wdr");
			::addAttribute( i_wireDistance[1], atInput|atKeyable);
			stat = attributeAffects( i_wireDistance[1], i_ulRampData[0] );

			i_wireDistance[2] = CreateRampAttr("wireDistanceFactorRamp", "wdfr");
			::addAttribute( i_wireDistance[2], atInput|atKeyable);
			stat = attributeAffects( i_wireDistance[2], i_ulRampData[3] );
		}

		// twist
		{
			i_twist[0] = CreateRampAttr("twist", "tw");
			::addAttribute( i_twist[0], atInput|atKeyable);
			stat = attributeAffects( i_twist[0], i_ulRampData[1] );

			i_twist[1] = unitAttr.create( "twistShift", "twsh", MAngle(0));
			unitAttr.setSoftMin( MAngle(-M_PI/2));
			unitAttr.setSoftMax( MAngle( M_PI/2));
			::addAttribute( i_twist[1], atInput|atKeyable);
			stat = attributeAffects( i_twist[1], i_ulRampData[1] );

			i_twist[2] = nAttr.create( "twistScale", "twsc", MFnNumericData::kDouble, 1.0);
			nAttr.setMin(0);
			nAttr.setSoftMax(3);
			::addAttribute( i_twist[2], atInput|atKeyable);
			stat = attributeAffects( i_twist[2], i_ulRampData[1] );
		}

		// scale
		{
			i_scale[0] = nAttr.create( "wScale", "wsc", MFnNumericData::kDouble, 1.0);
			nAttr.setMin(0);
			nAttr.setSoftMax(2);
			::addAttribute( i_scale[0], atInput|atKeyable);
			stat = attributeAffects( i_scale[0], i_ulRampData[2] );

			i_scale[1] = CreateRampAttr("wScaleRamp", "wscr");
			::addAttribute( i_scale[1], atInput|atKeyable);
			stat = attributeAffects( i_scale[1], i_ulRampData[2] );
		}
		//
		{
			i_affect_rotate[0] = CreateRampAttr("affectRotationRamp", "arr");
			::addAttribute( i_affect_rotate[0], atInput|atKeyable);
			stat = attributeAffects( i_affect_rotate[0], i_ulRampData[4] );
		}

		/*/
		{
			i_testWeghts = numAttr.create("i_testWeghtsX", "twx", MFnNumericData::kFloat, 0.5);
			numAttr.setArray(true);
			numAttr.setReadable(true);
			numAttr.setWritable(true);
			numAttr.setCached(true);
			numAttr.setStorable(true);
			numAttr.setConnectable(true);
			numAttr.setUsesArrayDataBuilder(true);
			numAttr.setKeyable(true);
			numAttr.setDisconnectBehavior(MFnAttribute::kDelete);
			numAttr.setInternal(true);

			i_testweightList = cAttr.create("i_testweightListX", "twxl");
			cAttr.addChild(i_testWeghts);
			cAttr.setUsesArrayDataBuilder(true);
			cAttr.setDisconnectBehavior(MFnAttribute::kDelete);
			cAttr.setInternal(true);
			::addAttribute( i_testweightList, atInput|atArray);
			stat = attributeAffects( i_testweightList, i_ulRampData[0] );
		}
		/*/

		if( !MGlobal::sourceFile("AEulWireControlTemplate.mel"))
		{
			displayString("error source AEulWireControlTemplate.mel");
		}
	}
	catch(MString err)
	{
		displayString("error in ulWireControl::initialize()");
		return MS::kFailure;
	}
	return MS::kSuccess;
}

