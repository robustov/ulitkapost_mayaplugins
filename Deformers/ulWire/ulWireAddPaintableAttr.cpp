//
// Copyright (C) 
// File: ulWireAddPaintableAttrCmd.cpp
// MEL Command: ulWireAddPaintableAttr

#include "ulWireAddPaintableAttr.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MFnCompoundAttribute.h>

ulWireAddPaintableAttr::ulWireAddPaintableAttr()
{
}
ulWireAddPaintableAttr::~ulWireAddPaintableAttr()
{
}

MSyntax ulWireAddPaintableAttr::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);	// node
	syntax.addArg(MSyntax::kString);	// name1
	syntax.addArg(MSyntax::kString);	// name2
	return syntax;
}

MStatus ulWireAddPaintableAttr::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString argsl0, name1, name2;
	argData.getCommandArgument(0, argsl0);
	argData.getCommandArgument(1, name1);
	argData.getCommandArgument(2, name2);

	MObject obj;
	if( !nodeFromName(argsl0, obj))
	{
		return MS::kFailure;
	}

	MFnNumericAttribute nAttr;
	MFnTypedAttribute	typedAttr;
	MFnCompoundAttribute comAttr;
	MFnUnitAttribute	unitAttr;
	MFnCompoundAttribute cAttr;

	MFnDependencyNode dn(obj);
	{
		MFnNumericAttribute numAttr;
		MObject i_testWeghts = numAttr.create(name1, name1, MFnNumericData::kFloat, 1.0);
		numAttr.setArray(true);
		numAttr.setReadable(true);
		numAttr.setWritable(true);
		numAttr.setCached(true);
		numAttr.setStorable(true);
		numAttr.setConnectable(true);
		numAttr.setUsesArrayDataBuilder(true);
		numAttr.setKeyable(true);
		numAttr.setDisconnectBehavior(MFnAttribute::kDelete);
		numAttr.setInternal(true);

		MObject i_testweightList = cAttr.create(name2, name2);
		cAttr.addChild(i_testWeghts);
		cAttr.setInternal(true);
		cAttr.setArray(true);
		cAttr.setReadable(true);
		cAttr.setWritable(true);
		cAttr.setCached(true);
		cAttr.setStorable(true);
		cAttr.setConnectable(true);
		cAttr.setUsesArrayDataBuilder(true);
		cAttr.setKeyable(true);
		cAttr.setDisconnectBehavior(MFnAttribute::kDelete);
		cAttr.setInternal(true);

		dn.addAttribute(i_testweightList);
	}

	return MS::kSuccess;
}

void* ulWireAddPaintableAttr::creator()
{
	return new ulWireAddPaintableAttr();
}

