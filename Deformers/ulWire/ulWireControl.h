#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ulWireControlNode.h
//
// Dependency Graph Node: ulWireControl

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include "ulRampData.h"

class ulWireControl : public MPxNode
{
public:
	ulWireControl();
	virtual ~ulWireControl(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

	virtual bool getInternalValue( 
		const MPlug& plug,
		MDataHandle& data);
	///
    virtual bool setInternalValue( 
		const MPlug& plug,
		const MDataHandle& data);
	

	MStatus Build_Ramp(MObject& attr, MDataBlock& data);

public:
	static MObject i_controlCurve;
		static MObject i_controlCurveMatrix;
	static MObject i_controlCurveBase;
		static MObject i_controlCurveBaseMatrix;

	static MObject i_usePaintedWeights;
	static MObject i_wireDistance[3];
	static MObject i_twist[3];
	static MObject i_scale[2];
	static MObject i_affect_rotate[1];

	static MObject i_ulRampData[5];

	static MObject i_testweightList;
		static MObject i_testWeghts;

public:
	static MTypeId id;
};

