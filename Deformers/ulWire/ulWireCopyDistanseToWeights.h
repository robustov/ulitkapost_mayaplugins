#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ulWireCopyDistanseToWeightsCmd.h
// MEL Command: ulWireCopyDistanseToWeights

#include <maya/MPxCommand.h>

class ulWireCopyDistanseToWeights : public MPxCommand
{
public:
	ulWireCopyDistanseToWeights();
	virtual	~ulWireCopyDistanseToWeights();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

