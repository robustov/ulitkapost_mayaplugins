#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"
#include "Ramp.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", ulRampData::id );
	}
/*/


class ulRampData : public MPxData
{
public:
	ulRampData();
	virtual ~ulRampData(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

	MStatus Build_Ramp(const MPlug& plug, MDataBlock& data);

public:
	static MTypeId id;
	static const MString typeName;

protected:
	void serialize(Util::Stream& stream);

public:
	Math::Ramp ramp;
};
