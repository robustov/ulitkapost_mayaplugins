//
// Copyright (C) 
// File: ulRampDataCmd.cpp
// MEL Command: ulRampData

#include "ulRampData.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <sstream>


MTypeId ulRampData::id( TypeIdBase+04 );
const MString ulRampData::typeName( "ulRampData" );

ulRampData::ulRampData()
{
}
ulRampData::~ulRampData()
{
}

MTypeId ulRampData::typeId() const
{
	return ulRampData::id;
}

MString ulRampData::name() const
{ 
	return ulRampData::typeName; 
}

void* ulRampData::creator()
{
	return new ulRampData();
}

void ulRampData::copy( const MPxData& other )
{
	const ulRampData* arg = (const ulRampData*)&other;

	this->ramp = arg->ramp;
}

MStatus ulRampData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("ulRampData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus ulRampData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus ulRampData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus ulRampData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void ulRampData::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		stream >> ramp;
	}
}

