#include "pre.h"

#include <maya/MFnPlugin.h>
#include "ulWire.h"
#include "ulWireCache.h"
#include "ulWireClearCache.h"
#include "ulWireControl.h"
#include "ulRampData.h"
#include "ulWireAddPaintableAttr.h"
#include "ulWireCopyDistanseToWeights.h"

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

	stat = plugin.registerCommand( "ulWireCopyDistanseToWeights", ulWireCopyDistanseToWeights::creator, ulWireCopyDistanseToWeights::newSyntax );
	if (!stat) {
		displayString("cant register node ulWireCopyDistanseToWeights");
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerCommand( "ulWireAddPaintableAttr", ulWireAddPaintableAttr::creator, ulWireAddPaintableAttr::newSyntax );
	if (!stat) {
		displayString("cant register node ulWireAddPaintableAttr");
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerData( 
		"ulRampData", 
		ulRampData::id, 
		ulRampData::creator);
	if (!stat) 
	{
		displayString("cant register node ulRampData");
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerNode( "ulWireControl", ulWireControl::id, ulWireControl::creator, ulWireControl::initialize );
	if (!stat) {
		displayString("cant register node ulWireControl");
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerCommand( "ulWireClearCache", ulWireClearCache::creator, ulWireClearCache::newSyntax );
	if (!stat) {
		displayString("cant register node ulWireClearCache");
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerData( 
		"ulWireCache", 
		ulWireCache::id, 
		ulWireCache::creator);
	if (!stat) 
	{
		displayString("cant register node ulWireCache");
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerNode( "ulWire", ulWire::id, ulWire::creator, ulWire::initialize, MPxNode::kDeformerNode );
	if (!stat) {
		displayString("cant register node ulWire");
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterCommand( "ulWireCopyDistanseToWeights" );
	if (!stat) displayString("cant deregister ulWireCopyDistanseToWeights");

	stat = plugin.deregisterCommand( "ulWireAddPaintableAttr" );
	if (!stat) displayString("cant deregister ulWireAddPaintableAttr");

	stat = plugin.deregisterData( ulRampData::id );
	if (!stat) displayString("cant deregister ulRampData");

	stat = plugin.deregisterNode( ulWireControl::id );
	if (!stat) displayString("cant deregister ulWireControl");

	stat = plugin.deregisterCommand( "ulWireClearCache" );
	if (!stat) displayString("cant deregister ulWireClearCache");

	stat = plugin.deregisterData( ulWireCache::id );
	if (!stat) displayString("cant deregister ulWireCache");

	stat = plugin.deregisterNode( ulWire::id );
	if (!stat) displayString("cant deregister ulWire");

	return stat;
}
