set BIN_DIR=.\out\ulWire
set BIN_DIR=\\server\bin

if "%DSTPATH%"=="" (set BIN_DIR=%BIN_DIR%) else (set BIN_DIR=%DSTPATH%)

mkdir %BIN_DIR%
mkdir %BIN_DIR%\bin
mkdir %BIN_DIR%\mel
mkdir %BIN_DIR%\slim
mkdir %BIN_DIR%\docs

copy BinRelease85\ulWire.mll						%BIN_DIR%\bin

copy Mel\ulWireMenu.mel								%BIN_DIR%\mel
copy Mel\AEulWireTemplate.mel						%BIN_DIR%\mel
copy Mel\AEulWireControlTemplate.mel				%BIN_DIR%\mel

if "%DSTPATH%"=="" (pause) else (set BIN_DIR=%BIN_DIR%)
