//
// Copyright (C) 
// File: ulWireClearCacheCmd.cpp
// MEL Command: ulWireClearCache

#include "ulWireClearCache.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include "ulWire.h"

ulWireClearCache::ulWireClearCache()
{
}
ulWireClearCache::~ulWireClearCache()
{
}

MSyntax ulWireClearCache::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);	// Wire node
//	syntax.addFlag("f", "format", MSyntax::kString);
	return syntax;
}

MStatus ulWireClearCache::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString argsl0;
	argData.getCommandArgument(0, argsl0);
	MObject obj;
	nodeFromName(argsl0, obj);
	if( obj.isNull())
	{
		MPlug plug;
		plugFromName(argsl0, plug);
		obj = plug.node();
	}
	if( obj.isNull())
		return MS::kFailure;

	MPlug plug(obj, ulWire::i_cache);
	plug.setValue(MObject::kNullObj);

	return MS::kSuccess;
}

void* ulWireClearCache::creator()
{
	return new ulWireClearCache();
}

