#pragma once
#include "pre.h"

//
// Copyright (C) 
// File: ulWireNode.h
//
// Deformer Node: ulWire

#include <maya/MPxNode.h>
#include <maya/MPxDeformerNode.h>
#include <maya/MDataBlock.h>
#include <maya/MPlug.h>
#include <maya/MItGeometry.h>
#include <maya/MMatrix.h>

#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include "ulWireCache.h"
#include "ulRampData.h"

class ulWire : public MPxDeformerNode
{
public:
	ulWire();
	virtual	~ulWire(); 

	MStatus compute( 
		const MPlug& plug,
		MDataBlock& dataBlock );
    virtual MStatus deform(
		MDataBlock& data,
		MItGeometry& iter,
		const MMatrix& mat,
		unsigned int multiIndex);

	virtual MStatus setDependentsDirty( const MPlug &, MPlugArray & );
	virtual MStatus shouldSave( const MPlug& plug, bool& result );

	virtual bool getInternalValue( 
		const MPlug& plug,
		MDataHandle& data);
	///
    virtual bool setInternalValue( 
		const MPlug& plug,
		const MDataHandle& data);
	
	static void* creator();
	static MStatus initialize();

	ulWireCache* Load_Cache(MDataBlock& data);
	ulRampData* Load_Ramp(MObject attr, MDataBlock& data);
	ulRampData* Load_Ramp(MObject attr, int index, MDataBlock& data);
	MStatus Build_Cache(const MPlug& plug, MDataBlock& data);

	// internal
protected:

	typedef std::map< int, int_weighhts_t> int_geometry_weighhts_t;
	typedef std::map< std::string, int_geometry_weighhts_t> int_ctrl_geometry_weighhts_t;
	int_ctrl_geometry_weighhts_t int_ctrl_geometry_weighhts;

public:
	// �������
		static MObject i_controlCurve;
		static MObject i_controlCurveMatrix;
		static MObject i_controlCurveBase;
		static MObject i_controlCurveBaseMatrix;
		static MObject i_dist_ramp;
		static MObject i_dist_factor_ramp;
		static MObject i_twist_ramp;
		static MObject i_scale_ramp;
		static MObject i_affectrotate_ramp;

	static MObject i_cache;
	static MObject i_bInteractiveInfluence;
	static MObject i_bProjection;

	static MObject i_addWeightNames;
//	static MObject i_testweightList;
//		static MObject i_testWeghts;

public:
	static MTypeId id;
};

