#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ulWireClearCacheCmd.h
// MEL Command: ulWireClearCache

#include <maya/MPxCommand.h>

class ulWireClearCache : public MPxCommand
{
public:
	ulWireClearCache();
	virtual	~ulWireClearCache();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

