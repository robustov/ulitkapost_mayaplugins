//
// Copyright (C) 
// File: ulWireCacheCmd.cpp
// MEL Command: ulWireCache

#include "ulWireCache.h"

#include <maya/MGlobal.h>
#include <maya/MItGeometry.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <sstream>

bool ulWireCache::Init()
{
	controlcurves.clear();
	return true;
}
bool ulWireCache::InitCurve(
	int index, 
	MMatrix& controlCurveMatrix,
	MObject& curveobj, 
	MMatrix& controlCurveBaseMatrix,
	MObject& curvebaseobj,
	Math::Ramp& distance_ramp,
	Math::Ramp& distance_factor_ramp,
	Math::Ramp& twist_ramp, 
	Math::Ramp& scale_ramp,
	Math::Ramp& affectrotate_ramp,
	int_weighhts_t* weighhts
	)
{
	controlcurves[index].controlCurveMatrix = controlCurveMatrix;
	controlcurves[index].controlCurveBaseMatrix = controlCurveBaseMatrix;
	controlcurves[index].curvebase.setObject( curvebaseobj);
	controlcurves[index].curve.setObject( curveobj);

	controlcurves[index].distance_ramp = distance_ramp;
	controlcurves[index].distance_factor_ramp = distance_factor_ramp;
	controlcurves[index].twist_ramp = twist_ramp;
	controlcurves[index].scale_ramp = scale_ramp;
	controlcurves[index].affectrotate_ramp = affectrotate_ramp;

	controlcurves[index].weighhts = weighhts;
	return true;
}

MSpace::Space space = MSpace::kWorld;

void ulWireCache::clear(
	)
{
	geometries.clear();
	bValid = true;
}

bool ulWireCache::InitTarget(
	int multiIndex, 
	MItGeometry& iter, 
	const MMatrix& mat, 
	bool bInteractiveInfluence)
{
	bool bInitMatrix = geometries.find(multiIndex)==geometries.end();
	geometryInfluence& gi = geometries[multiIndex];
	if( bInitMatrix)
		::copy( gi.worldMatrix, mat);

	// ������ �������� ������� ���������
	int p=0;
	for ( iter.reset(); !iter.isDone(); iter.next(), p++) 
	{
		MPoint _pt = iter.position();
		Math::Vec3f pt; ::copy(pt, _pt);

		int index = iter.index();
		if( gi.influences.find(index)!=gi.influences.end() && !bInteractiveInfluence)
			continue;
		pointInfluence& pi = gi.influences[index];
		pi.index = index;
		if( pi.source_pt == pt)
			continue;

		pi.reset(pt);
		pt = gi.worldMatrix*pt;

		projectionPoint(pt, pi);
	}
	return true;
}

// �������� �����
void ulWireCache::projectionPoint(
	Math::Vec3f pt,							// world space
	pointInfluence& pi						// ���������
	) const
{
	MPoint toThisPoint; ::copy(toThisPoint, pt);

	std::map<int, controldata>::const_iterator it = controlcurves.begin();
	for(;it != controlcurves.end(); it++)
	{
		int curveindex = it->first;
		const MFnNurbsCurve& curvebase = it->second.curvebase;
		double param = 0;
		MStatus stat;
		MPoint pr = curvebase.closestPoint( toThisPoint, &param, 1.0e-5, space, &stat);

		float distance = (float)(toThisPoint-pr).length();

		double l = curvebase.length();
		float min=0, max=1;
		double minP, maxP;
		curvebase.getKnotDomain(minP, maxP);
		float eps = 0.001f;
		for(int i=0; i<50; i++)	//min-max<eps
		{
			if( minP==maxP) break;
//			float factor = (float) ( (param-minP)/(maxP-minP));
			float factor = 0.5f;
			float neo = min*(1-factor) + max*(factor);

			double neoparam = curvebase.findParamFromLength(neo*l);
			if( fabs(neoparam-param)<eps)
			{
				max = neo, min = neo;
				break;
			}
			if(neoparam>param)
				maxP = neoparam, max = neo;
			else //if(neoparam<minparam)
				minP = neoparam, min = neo;
		}
		float paramNormal = (min+max)/2;

		pi.curveInfluence[curveindex].param			= (float)param;
		pi.curveInfluence[curveindex].paramNormal	= paramNormal;
		pi.curveInfluence[curveindex].distance		= distance;
	}
}

// ������ ��� ���������� ��������
float ulWireCache::getPointDeformation(
	Math::Vec3f& project,				// �������� �����
	Math::Vec3f& projectbase,			// �������� �����
	Math::Vec3f& offset,				// ��������
	Math::Rot3f& rotate,				// ��������
	int pointIndex, 
	int curveIndex, 
	const point2curveInfluence& influence,			// ��������
	float& scale
	) const
{
	scale = 0;
	offset = Math::Vec3f(0, 0, 0);
	rotate = Math::Rot3f(0, 0, 0, 0);
	project = Math::Vec3f(0, 0, 0);
	projectbase = Math::Vec3f(0, 0, 0);

	std::map<int, controldata>::const_iterator it = controlcurves.find(curveIndex);
	if( it == controlcurves.end())
		return 0;

	const controldata& cdata = it->second;

	int_weighhts_t::const_iterator wit = cdata.weighhts->find(pointIndex);
	if( wit == cdata.weighhts->end())
	{
		(*cdata.weighhts)[pointIndex] = 1.f;
		wit = cdata.weighhts->find(pointIndex);
	}

	float wireDistance = cdata.distance_ramp.getValue(influence.paramNormal);
//	float distanceFactor = -(influence.distance-wireDistance)/wireDistance;
	float distanceFactor = influence.distance/wireDistance;
	// �������������� ����
	distanceFactor = cdata.distance_factor_ramp.getValue(distanceFactor);

	distanceFactor = __min( __max(distanceFactor, 0.f), 1.f);
	// ���� ���� ��������
	if( cdata.weighhts && wireDistance<0.f)
	{
		distanceFactor = wit->second;
	}
	if( distanceFactor<=0.f) 
		return 0;

	float twist = cdata.twist_ramp.getValue(influence.paramNormal); 
	scale = cdata.scale_ramp.getValue(influence.paramNormal); 
	scale = scale - 1;
	scale = scale*distanceFactor;

	const MFnNurbsCurve& curvebase = cdata.curvebase;
	const MFnNurbsCurve& curve	   = cdata.curve;

	MPoint _projectbase, _project;
	curvebase.getPointAtParam(influence.param, _projectbase, space);
	curve.getPointAtParam(influence.param, _project, space);
	::copy(projectbase, _projectbase);
	::copy(project, _project);

	float factor = distanceFactor;
	offset = (project - projectbase)*factor;

	// rotate
	MVector _tg = curve.tangent(influence.param, space);
	MVector _tgbase = curvebase.tangent(influence.param, space);
	Math::Vec3f tg; ::copy(tg, _tg);
	Math::Vec3f tgbase; ::copy(tgbase, _tgbase);

	Math::Rot3f nrotate(twist, tg);
	nrotate = nrotate*factor;

	Math::Rot3f trotate(tgbase.normalized(), tg.normalized());
	trotate = trotate*factor;
	rotate = trotate*nrotate;
	float affectRotation = cdata.affectrotate_ramp.getValue(influence.paramNormal);
	rotate *= affectRotation;

	return distanceFactor;
}

// ������ ��� ���������� ��������
void ulWireCache::deformPoint(
	Math::Vec3f pt, 
	Math::Vec3f& ofs_de, 
	Math::Vec3f& ofs_pr, 
	float envelope,
	const pointInfluence& pi						// ��������
	) const
{
	ofs_de = Math::Vec3f(0, 0, 0);
	ofs_pr = Math::Vec3f(0, 0, 0);
	if( !pi.isValid()) return;
	float factorsum = 0;

	std::map<int, point2curveInfluence>::const_iterator iti = pi.curveInfluence.begin();
	for( ;iti != pi.curveInfluence.end(); iti++)
	{
		int curveIndex = iti->first;
		const point2curveInfluence& influence = iti->second;

		Math::Vec3f offset = Math::Vec3f(0, 0, 0);
		Math::Rot3f rotate = Math::Rot3f(0, 0, 0, 0);
		Math::Vec3f project = Math::Vec3f(0, 0, 0);
		Math::Vec3f projectbase = Math::Vec3f(0, 0, 0);
		float scale = 0;

		float factor = getPointDeformation(
			project, projectbase, offset, rotate, pi.index, curveIndex, influence, scale);
		if( !factor) continue;

		bool bRotation = true;
		bool bOffset = true;

		Math::Vec3f res_de = pt;
		if( bRotation)
		{
			Math::Matrix4f m;
			rotate.getMatrix(m);

			Math::Vec3f dir = pt - projectbase;
			Math::Vec3f dirR = m*dir;
			Math::Vec3f s = (dirR-dir);
			res_de += s*envelope;
		}
		if( bOffset)
		{
			res_de += offset*envelope;
		}
		res_de += (res_de - project)*scale*envelope;

		Math::Vec3f _ofs_de = res_de-pt;
		Math::Vec3f _ofs_pr = project-pt;
		ofs_de += _ofs_de*factor;
		ofs_pr += _ofs_pr;
		factorsum += factor;
	}
	if( factorsum>0.f)
	{
		ofs_de = ofs_de/factorsum;
		ofs_pr = ofs_pr/factorsum;
	}
}

/*/
// ���������� ��������
Math::Vec3f ulWireCache::deformPoint(
	Math::Vec3f& pt,						// �����
	const Math::Vec3f& project,				// �������� �����
	const Math::Vec3f& projectbase,			// �������� �����
	const Math::Vec3f& offset,				// ��������
	const Math::Rot3f& rotate,				// ��������
	float envelope
	)const
{
	bool bRotation = true;
	bool bOffset = true;

	Math::Vec3f res_de = pt;
	if( bRotation)
	{
//		rotate.normalize();
		Math::Matrix4f m;
		rotate.getMatrix(m);

		Math::Vec3f dir = pt - projectbase;
		Math::Vec3f dirR = m*dir;
		Math::Vec3f s = (dirR-dir);
		res_de += s*envelope;
	}
	if( bOffset)
	{
		res_de += offset*envelope;
	}
	return res_de;
}
/*/





MTypeId ulWireCache::id( TypeIdBase+01 );
const MString ulWireCache::typeName( "ulWireCache" );

ulWireCache::ulWireCache()
{
	bValid = true;
}
ulWireCache::~ulWireCache()
{
}

MTypeId ulWireCache::typeId() const
{
	return ulWireCache::id;
}

MString ulWireCache::name() const
{ 
	return ulWireCache::typeName; 
}

void* ulWireCache::creator()
{
	return new ulWireCache();
}

void ulWireCache::copy( const MPxData& other )
{
	const ulWireCache* arg = (const ulWireCache*)&other;
	this->bValid = arg->bValid;
	this->geometries = arg->geometries;
}

MStatus ulWireCache::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("ulWireCache: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus ulWireCache::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus ulWireCache::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus ulWireCache::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void ulWireCache::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		stream >> geometries;
		bValid = true;
	}
}

