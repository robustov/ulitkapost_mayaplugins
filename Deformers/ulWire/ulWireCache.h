#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MRampAttribute.h>
#include "Util/Stream.h"
#include "Math/Math.h"
#include "Math/Rotation3.h"
#include "Ramp.h"

struct point2curveInfluence
{
	float param;			// �������� �� ������
	float paramNormal;		// �������������� �������� (�� �����)
	float distance;			// ��������� �� ������ ��������
};

typedef std::map< int, float> int_weighhts_t;

// ������ � �������� ��������
struct pointInfluence
{
	// �������� ��������� ����� (object space)
	int index;
	Math::Vec3f source_pt;

	// �������� � ������
	std::map<int, point2curveInfluence> curveInfluence;

	void reset(Math::Vec3f pt){ source_pt = pt; curveInfluence.clear();}
	pointInfluence(){};
	bool isValid() const {return !curveInfluence.empty();}
};

// ������ � �������� ���������
struct geometryInfluence
{
	// �������� ������� world space
	Math::Matrix4f worldMatrix;
	// ����� index() -> pointInfluence
	std::map<int, pointInfluence> influences;
};

struct controldata
{
	MMatrix controlCurveBaseMatrix;
	MFnNurbsCurve curvebase;
	MMatrix controlCurveMatrix;
	MFnNurbsCurve curve;
	Math::Ramp distance_ramp; 
	Math::Ramp distance_factor_ramp;
	Math::Ramp twist_ramp; 
	Math::Ramp scale_ramp;
	Math::Ramp affectrotate_ramp;
	int_weighhts_t* weighhts;
};

class ulWireCache : public MPxData
{
public:
	bool bValid;
	// target geomentry (multiIndex->geometryInfluence)
	std::map<int, geometryInfluence> geometries;

	std::map<int, controldata> controlcurves;

public:
	void clear(
		);
	bool Init(
		);
	bool InitCurve(
		int index, 
		MMatrix& controlCurveMatrix,
		MObject& curveobj, 
		MMatrix& controlCurveBaseMatrix,
		MObject& curvebaseobj, 
		Math::Ramp& distance_ramp,
		Math::Ramp& distance_factor_ramp,
		Math::Ramp& twist_ramp, 
		Math::Ramp& scale_ramp, 
		Math::Ramp& affectrotate_ramp,
		int_weighhts_t* weighhts
		);

	bool InitTarget(
		int multiIndex, 
		MItGeometry& iter, 
		const MMatrix& mat, 
		bool bInteractiveInfluence);

	// �������� �����
	void projectionPoint(
		Math::Vec3f pt,							// world space
		pointInfluence& pi						// ���������
		) const;
	// ������ ��� ���������� ��������
	float getPointDeformation(
		Math::Vec3f& project,				// �������� �����
		Math::Vec3f& projectbase,			// �������� �����
		Math::Vec3f& offset,				// ��������
		Math::Rot3f& rotate,				// ��������
		int pointIndex, 
		int curveIndex, 
		const point2curveInfluence& influence,			// ��������
		float& scale
		)const;
	// ���������� ��������
	void deformPoint(
		Math::Vec3f pt, 
		Math::Vec3f& ofs_de, 
		Math::Vec3f& ofs_pr, 
		float envelope,
		const pointInfluence& pi						// ��������
		)const;

public:
	ulWireCache();
	virtual ~ulWireCache(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;

	
protected:
	void serialize(Util::Stream& stream);
};


template<class Stream> inline
Stream& operator >> (Stream& out, pointInfluence& v)
{
	out >> v.index;
	out >> v.source_pt;
	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, geometryInfluence& v)
{
	out >> v.worldMatrix >> v.influences;
	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, point2curveInfluence& v)
{
	out >> v.param >> v.paramNormal >> v.distance;
	out >> v.affectRotation;
	return out;
}
