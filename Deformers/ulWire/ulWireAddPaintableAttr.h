#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ulWireAddPaintableAttrCmd.h
// MEL Command: ulWireAddPaintableAttr

#include <maya/MPxCommand.h>

class ulWireAddPaintableAttr : public MPxCommand
{
public:
	ulWireAddPaintableAttr();
	virtual	~ulWireAddPaintableAttr();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

