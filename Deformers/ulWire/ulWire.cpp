//
// Copyright (C) 
// 
// File: ulWireNode.cpp
//
// Dependency Graph Node: ulWire
//
// Author: Maya Plug-in Wizard 2.0
//

#include "ulWire.h"

#include "pre.h"
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MPoint.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnDoubleArrayData.h>

#include <maya/MGlobal.h>

// You MUST change this to a unique value!!!  The id is a 32bit value used
// to identify this type of node in the binary file format.  
//
MTypeId ulWire::id( TypeIdBase+00 );

MObject ulWire::i_controlCurve;
	MObject ulWire::i_controlCurveMatrix;
MObject ulWire::i_controlCurveBase;
	MObject ulWire::i_controlCurveBaseMatrix;
MObject ulWire::i_cache;
MObject ulWire::i_bInteractiveInfluence;

//MObject ulWire::i_usePaintedWeights;
MObject ulWire::i_bProjection;

//MObject ulWire::i_wireDistance[2];
//MObject ulWire::i_twist[3];
//MObject ulWire::i_scale[2];

MObject ulWire::i_dist_ramp;
MObject ulWire::i_dist_factor_ramp;
MObject ulWire::i_twist_ramp;
MObject ulWire::i_scale_ramp;
MObject ulWire::i_affectrotate_ramp;

MObject ulWire::i_addWeightNames;
//MObject ulWire::i_testweightListList;
//MObject ulWire::i_testweightList;
//	MObject ulWire::i_testWeghts;

ulWire::ulWire()
{
}
ulWire::~ulWire()
{
}

MStatus ulWire::setDependentsDirty( const MPlug & plug, MPlugArray & pa)
{
	displayString("setDependentsDirty %s", plug.name().asChar());
	pa.append( MPlug(thisMObject(), outputGeom));
	return MPxDeformerNode::setDependentsDirty(plug, pa);
}
MStatus ulWire::shouldSave( const MPlug& plug, bool& result )
{
//	displayString("shouldSave %s", plug.name().asChar());
	return MPxDeformerNode::shouldSave( plug, result );
}

bool ulWire::getInternalValue( 
	const MPlug& plug,
	MDataHandle& data)
{
	MStatus stat;
	int index = plug.logicalIndex(&stat);
	if(stat || plug.isArray())
	{
		stat = MS::kSuccess;
		MPlug parent = plug;
		if(!plug.isArray())
			parent = plug.array(&stat);
		if(stat)
		{
			parent = parent.parent(&stat);
			if(stat)
			{
				int multiIndex = parent.logicalIndex(&stat);
				if(stat)
				{
					MPlug topparent = parent.array(&stat);
					if(stat)
					{
						MObject attrobj = topparent.attribute();
						MString _name = MFnAttribute(attrobj).name();
						std::string name = _name.asChar();
						name += "Weights";

						int_weighhts_t& int_weighhts = int_ctrl_geometry_weighhts[name][multiIndex];
						if( index >= 0)
						{
							float val = int_weighhts[index];
							displayStringD("%s[%d][%d] = %f", name.c_str(), multiIndex, index, val);
							data.asFloat() = val;
							return true;
						}
						else
						{
							MArrayDataHandle adh(data, &stat);
							MArrayDataBuilder builder = adh.builder(&stat);
							int_weighhts_t::iterator it = int_weighhts.begin();
							for(;it != int_weighhts.end(); it++)
							{
								int index = it->first;
								float val = it->second;
								MDataHandle dh = builder.addElement(index);
								dh.asFloat() = val;
							}
							adh.set( builder);
							/*/
							if(adh.elementCount()) do
							{
								int index = adh.elementIndex();
								float val = int_weighhts[index];
								adh.inputValue().asFloat() = val;
								displayStringD("get %d %f", index, val);
							}while( adh.next());
							/*/
							return true;
						}
					}
				}
			}
		}
	}

	displayStringD("getInternalValue %s", plug.name().asChar());
	return MPxNode::getInternalValue( plug, data);
}
///
bool ulWire::setInternalValue( 
	const MPlug& plug,
	const MDataHandle& data)
{
	MStatus stat;
	int index = plug.logicalIndex(&stat);
	if( stat || plug.isArray())
	{
		stat = MS::kSuccess;
		MPlug parent = plug;
		if(!plug.isArray())
			parent = plug.array(&stat);
		if(stat)
		{
			parent = parent.parent(&stat);
			if(stat)
			{
				int multiIndex = parent.logicalIndex(&stat);
				if(stat)
				{
					MPlug topparent = parent.array(&stat);
					if(stat)
					{
						MObject attrobj = topparent.attribute();
						MString _name = MFnAttribute(attrobj).name();
						std::string name = _name.asChar();
						name += "Weights";

						int_weighhts_t& int_weighhts = int_ctrl_geometry_weighhts[name][multiIndex];
						if( index >= 0)
						{
							float val = data.asFloat();
							displayStringD("%s[%d][%d] = %f", name.c_str(), multiIndex, index, val);
							int_weighhts[index] = val;
						}
						else
						{
							MArrayDataHandle adh(data, &stat);
							if(adh.elementCount()) do
							{
								int index = adh.elementIndex();
								float val = adh.inputValue().asFloat();
								int_weighhts[index] = val;
								displayStringD("set %d %f", index, val);
							}while( adh.next());
						}
						MDataBlock db = this->forceCache();
						MPlug pl(thisMObject(), i_bInteractiveInfluence);
						if( db.isClean(pl))
						{
							bool b;
							pl.getValue(b);
							pl.setValue(b);
						}
						return true;
					}
				}
			}
		}
	}

	displayStringD("setInternalValue %s", plug.name().asChar());
	return MPxNode::setInternalValue( plug, data);
}

ulWireCache* ulWire::Load_Cache(MDataBlock& data)
{
	MStatus _Status, stat;
	MDataHandle inputData = data.inputValue(i_cache, &stat);
	MPxData* pxdata = inputData.asPluginData();
	ulWireCache* hgh = (ulWireCache*)pxdata;
	if( !hgh || !hgh->bValid)
	{
		Build_Cache( MPlug(thisMObject(), i_cache), data);
		MDataHandle inputData = data.inputValue(i_cache, &stat);
		MPxData* pxdata = inputData.asPluginData();
		ulWireCache* hgh = (ulWireCache*)pxdata;
		return hgh;
	}
	return hgh;
}
ulRampData* ulWire::Load_Ramp(MObject attr, MDataBlock& data)
{
	MStatus _Status, stat;
	MDataHandle inputData = data.inputValue(attr, &stat);
	MPxData* pxdata = inputData.asPluginData();
	ulRampData* hgh = (ulRampData*)pxdata;
	return hgh;
}
ulRampData* ulWire::Load_Ramp(MObject attr, int index, MDataBlock& data)
{
	MStatus _Status, stat;
	MArrayDataHandle ma = data.inputArrayValue( attr);

	if( !ma.jumpToElement(index)) return NULL;

	MDataHandle inputData = ma.inputValue(&stat);
	MPxData* pxdata = inputData.asPluginData();
	ulRampData* hgh = (ulRampData*)pxdata;
	return hgh;
}

MStatus ulWire::Build_Cache(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;

	ulWireCache* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( i_cache, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<ulWireCache*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( ulWireCache::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<ulWireCache*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	// 
//	MObject curveobj = data.inputValue( this->i_controlCurveBase, &stat ).asMesh();
	pData->clear();

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}


MStatus ulWire::compute( 
	const MPlug& plug,
	MDataBlock& dataBlock )
{
	displayStringD( "ulWire::compute %s", plug.name().asChar());
	if(plug==i_cache)
		return Build_Cache(plug, dataBlock);
	return MPxDeformerNode::compute(plug, dataBlock);
}



MStatus ulWire::deform(
	MDataBlock& data,
	MItGeometry& iter,
	const MMatrix& m,
	unsigned int multiIndex)
{
	MStatus stat;
	MMatrix m_inv = m.inverse();
 
	float envelope = data.inputValue(this->envelope,&stat).asFloat();
//	if( !envelope) return MS::kSuccess;

	bool bInteractiveInfluence = data.inputValue(this->i_bInteractiveInfluence,&stat).asBool();
	bool bProjection = data.inputValue(this->i_bProjection,&stat).asBool();
//	bool usePaintedWeights = data.inputValue(this->i_usePaintedWeights,&stat).asBool();

	ulWireCache* cache = Load_Cache(data);
	if( !cache) 
		return MS::kFailure;

	MArrayDataHandle bccma = data.inputArrayValue( i_controlCurveBaseMatrix);
	MArrayDataHandle bcca  = data.inputArrayValue( i_controlCurveBase);
	MArrayDataHandle ccma  = data.inputArrayValue( i_controlCurveMatrix);
	MArrayDataHandle cca   = data.inputArrayValue( i_controlCurve);
	MArrayDataHandle awna  = data.inputArrayValue( i_addWeightNames);
	
	if(ccma.elementCount()) do
	{
		int index = ccma.elementIndex();
		MMatrix controlCurveMatrix = ccma.inputValue(&stat).asMatrix();
		if( !stat) continue;

		if( !cca.jumpToElement(index)) continue;
		MObject curveobj = cca.inputValue(&stat).asNurbsCurve();
		if( !stat || curveobj.isNull()) continue;

		if( !bccma.jumpToElement(index)) continue;
		MMatrix controlCurveBaseMatrix = bccma.inputValue(&stat).asMatrix();
		if( !stat) continue;

		if( !bcca.jumpToElement(index)) continue;
		MObject curvebaseobj = bcca.inputValue(&stat).asNurbsCurve();
		if( !stat || curvebaseobj.isNull()) continue;

		if( !awna.jumpToElement(index)) continue;
		MString wattrname = awna.inputValue(&stat).asString();
//		MObject wattrnameobj = bcca.inputValue(&stat).data();
		if( !stat) continue;

		std::string name = wattrname.asChar();
		int_geometry_weighhts_t& int_geometry_weighhts = int_ctrl_geometry_weighhts[name];
		int_weighhts_t& int_weighhts = int_geometry_weighhts[multiIndex];
		int_weighhts_t* weighhts = &int_weighhts;

		ulRampData* rampdata;

		Math::Ramp distanceramp;
		rampdata = Load_Ramp(i_dist_ramp, index, data);
		if( rampdata) distanceramp = rampdata->ramp;

		Math::Ramp twistramp;
		rampdata = Load_Ramp(i_twist_ramp, index, data);
		if( rampdata) twistramp = rampdata->ramp;

		Math::Ramp scaleramp;
		rampdata = Load_Ramp(i_scale_ramp, index, data);
		if( rampdata) scaleramp = rampdata->ramp;

		Math::Ramp distanceFactorRamp;
		rampdata = Load_Ramp(i_dist_factor_ramp, index, data);
		if( rampdata) distanceFactorRamp = rampdata->ramp;

		Math::Ramp affectrotate_ramp;
		rampdata = Load_Ramp(i_affectrotate_ramp, index, data);
		if( rampdata) affectrotate_ramp = rampdata->ramp;
//		affectrotate_ramp.clear(1.f);

		cache->InitCurve(index, 
			controlCurveMatrix, curveobj, controlCurveBaseMatrix, curvebaseobj, 
			distanceramp, distanceFactorRamp, twistramp, scaleramp, affectrotate_ramp, weighhts);

	}while( ccma.next());

	cache->InitTarget(multiIndex, iter, m, bInteractiveInfluence);

	geometryInfluence& gi = cache->geometries[multiIndex];
	iter.reset();
	for ( iter.reset(); !iter.isDone(); iter.next()) 
	{
		int index = iter.index();
		pointInfluence& pi = gi.influences[index];
		MPoint _pt = iter.position();
		float env = envelope;

		Math::Vec3f pt; copy(pt, _pt);
		pt = gi.worldMatrix * pi.source_pt;
		Math::Vec3f source_pt = pt;

		Math::Vec3f project, projectbase, offset;
		Math::Rot3f rotate;
		float dist = 0;//distanceramp.getValue(pi.paramNormal);
		float scale = 0;

		Math::Vec3f ofs_de, ofs_pr;
		cache->deformPoint(
			pt, ofs_de, ofs_pr, env, pi);
		if(bProjection)
			ofs_de = ofs_pr;

		MVector _ofsworld; copy(_ofsworld, ofs_de);
		_pt *= m;
		_pt += _ofsworld;
		_pt *= m_inv;
		iter.setPosition(_pt);
	}

	return MS::kSuccess;
}

void* ulWire::creator()
{
	return new ulWire();
}

MStatus ulWire::initialize()
{
	MFnNumericAttribute nAttr;
	MFnTypedAttribute	typedAttr;
	MFnCompoundAttribute comAttr;
	MFnUnitAttribute	unitAttr;
	MFnCompoundAttribute cAttr;
	MStatus				stat;

	try
	{
		// ��������
		{
			i_cache = typedAttr.create( "cache", "ch", ulWireCache::id);
			::addAttribute(i_cache, atReadable|atWritable|atCached|atUnStorable );
			stat = attributeAffects( i_cache, outputGeom );

			// controlCurve
			{
				i_controlCurve = typedAttr.create( "controlCurve", "cc", MFnData::kNurbsCurve, &stat);
				::addAttribute(i_controlCurve, atReadable|atWritable|atArray);
				stat = attributeAffects( i_controlCurve, outputGeom );

				i_controlCurveMatrix = typedAttr.create( "controlCurveMatrix", "ccm", MFnData::kMatrix, &stat);
				::addAttribute(i_controlCurveMatrix, atReadable|atWritable|atArray);
				stat = attributeAffects( i_controlCurveMatrix, outputGeom );
			}

			// controlCurveBase
			{
				i_controlCurveBase = typedAttr.create( "controlCurveBase", "ccb", MFnData::kNurbsCurve, &stat);
				::addAttribute(i_controlCurveBase, atReadable|atWritable|atArray);
				stat = attributeAffects( i_controlCurveBase, outputGeom );
				stat = attributeAffects( i_controlCurveBase, i_cache);

				i_controlCurveBaseMatrix = typedAttr.create( "controlCurveBaseMatrix", "ccbm", MFnData::kMatrix, &stat);
				::addAttribute(i_controlCurveBaseMatrix, atReadable|atWritable|atArray);
				stat = attributeAffects( i_controlCurveBaseMatrix, i_cache);
				stat = attributeAffects( i_controlCurveBaseMatrix, outputGeom );
			}
			// ramp
			{
				i_dist_ramp = typedAttr.create( "dist_ramp", "dira", ulRampData::id);
				::addAttribute(i_dist_ramp, atReadable|atWritable|atUnCached|atUnStorable|atArray );
				stat = attributeAffects( i_dist_ramp, outputGeom );

				i_dist_factor_ramp = typedAttr.create( "dist_factor_ramp", "difra", ulRampData::id);
				::addAttribute(i_dist_factor_ramp, atReadable|atWritable|atUnCached|atUnStorable|atArray );
				stat = attributeAffects( i_dist_factor_ramp, outputGeom );

				i_twist_ramp = typedAttr.create( "twist_ramp", "twira", ulRampData::id);
				::addAttribute(i_twist_ramp, atReadable|atWritable|atUnCached|atUnStorable|atArray );
				stat = attributeAffects( i_twist_ramp, outputGeom );

				i_scale_ramp = typedAttr.create( "scale_ramp", "scara", ulRampData::id);
				::addAttribute(i_scale_ramp, atReadable|atWritable|atUnCached|atUnStorable|atArray );
				stat = attributeAffects( i_scale_ramp, outputGeom );

				i_affectrotate_ramp = typedAttr.create( "affectrotate_ramp", "afrra", ulRampData::id);
				::addAttribute(i_affectrotate_ramp, atReadable|atWritable|atUnCached|atUnStorable|atArray );
				stat = attributeAffects( i_affectrotate_ramp, outputGeom );
			}

			i_bInteractiveInfluence = nAttr.create( "interactiveInfluence", "biin", MFnNumericData::kBoolean, 1);
			::addAttribute( i_bInteractiveInfluence, atInput|atKeyable);
			stat = attributeAffects( i_bInteractiveInfluence, outputGeom );

			i_bProjection = nAttr.create( "bProjection", "bp", MFnNumericData::kBoolean, 0);
			::addAttribute( i_bProjection, atInput|atKeyable);
			stat = attributeAffects( i_bProjection, outputGeom );
		}


		{
			i_addWeightNames = typedAttr.create( "addWeightNames", "awn", MFnData::kString, &stat);
			::addAttribute(i_addWeightNames, atInput|atArray);
			stat = attributeAffects( i_controlCurveMatrix, outputGeom );
		}

		if( !MGlobal::sourceFile("AEulWireTemplate.mel"))
		{
			displayString("error source AEulWireTemplate.mel");
 		}
	}
	catch(MString err)
	{
		displayString("error in ulWire::initialize()");
		return MS::kFailure;
	}
	return MS::kSuccess;
}

MObject CreateRampAttr(
	std::string fullname, 
	std::string shortname)
{
	MObject rampchilds[3];
	MObject i_RampAttr;
	MStatus stat;
	MFnNumericAttribute nAttr;
	MFnEnumAttribute eAttr;
	MFnCompoundAttribute cAttr;
	std::string fn, sn;

	fn = fullname + "_Position";
	sn = shortname + std::string("p");
	rampchilds[0] = nAttr.create(fn.c_str(), sn.c_str(), MFnNumericData::kFloat, 0.0);
	float vmin = -1;
	float vmax = 1;
	
	nAttr.setSoftMin(vmin);
	nAttr.setSoftMax(vmax);
//	stat = addAttribute(i_HairAttr[1]);

	fn = fullname + std::string("_FloatValue");
	sn = shortname + std::string("fv");
	rampchilds[1] = nAttr.create(fn.c_str(), sn.c_str(), MFnNumericData::kFloat, 0.0);
	nAttr.setSoftMin(vmin);
	nAttr.setSoftMax(vmax);
//	stat = addAttribute(fv);

	fn = std::string(fullname) + std::string("_Interp");
	sn = std::string(shortname) + std::string("i");
	rampchilds[2] = eAttr.create(fn.c_str(), sn.c_str());
	eAttr.addField("None",   0);
	eAttr.addField("Linear", 1);
	eAttr.addField("Smooth", 2);
	eAttr.addField("Spline", 3);
//	stat = addAttribute(in);

	i_RampAttr = cAttr.create(fullname.c_str(), shortname.c_str());
	cAttr.addChild(rampchilds[0]);
	cAttr.addChild(rampchilds[1]);
	cAttr.addChild(rampchilds[2]);
	cAttr.setWritable(true);
	cAttr.setStorable(true);
	cAttr.setKeyable(false);
	cAttr.setArray(true);
	return i_RampAttr;
}