//
// Copyright (C) 
// File: ulWireCopyDistanseToWeightsCmd.cpp
// MEL Command: ulWireCopyDistanseToWeights

#include "ulWireCopyDistanseToWeights.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>

ulWireCopyDistanseToWeights::ulWireCopyDistanseToWeights()
{
}
ulWireCopyDistanseToWeights::~ulWireCopyDistanseToWeights()
{
}

MSyntax ulWireCopyDistanseToWeights::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);	// arg0
	return syntax;
}

MStatus ulWireCopyDistanseToWeights::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString argsl0;
	argData.getCommandArgument(0, argsl0);
	
	MObject obj;
	nodeFromName(argsl0, obj);



	return MS::kSuccess;
}

void* ulWireCopyDistanseToWeights::creator()
{
	return new ulWireCopyDistanseToWeights();
}

