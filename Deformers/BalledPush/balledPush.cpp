//
// Copyright (C) 
// 
// File: balledPushNode.cpp
//
// Dependency Graph Node: balledPush
//
// Author: Maya Plug-in Wizard 2.0
//

#include "balledPush.h"

#include "pre.h"
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MPoint.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnEnumAttribute.h>

#include <maya/MGlobal.h>

// You MUST change this to a unique value!!!  The id is a 32bit value used
// to identify this type of node in the binary file format.  
//
MTypeId balledPush::id( TypeIdBase+00 );

MObject balledPush::i_balledTransform;

balledPush::balledPush()
{
}
balledPush::~balledPush()
{
}

MStatus balledPush::deform(
	MDataBlock& data,
	MItGeometry& iter,
	const MMatrix& m,
	unsigned int multiIndex)
{
	MStatus stat;
 	MMatrix m_inv = m.inverse();

	std::vector<MMatrix> vtm;
	std::vector<MMatrix> vtm_inv;
	MArrayDataHandle adh = data.inputArrayValue(this->i_balledTransform);
	vtm.reserve(adh.elementCount());
	vtm_inv.reserve(adh.elementCount());
	if(adh.elementCount()) do
	{
		MMatrix _vtm = MFnMatrixData(adh.inputValue().data()).matrix();
		MMatrix _vtm_inv = _vtm.inverse();
		vtm.push_back(_vtm);
		vtm_inv.push_back(_vtm_inv);
	}while( adh.next());
	
	float envelope = data.inputValue(this->envelope,&stat).asFloat();

	std::vector<MPoint> res;
	std::vector<double> weight;
	for ( iter.reset(); !iter.isDone(); iter.next()) 
	{
		MPoint pt = iter.position();

		pt *= m;

		res.clear();
		weight.clear();
		double wsum = 0;
		for(int i=0; i<(int)vtm_inv.size(); i++)
		{
/*/
			MPoint ptc = pt*vtm_inv[i];
			MPoint ptc0(ptc.x, 0, 0);
			MVector ptv = ptc-ptc0;
			double d = ptv.length();
			double ll = sqrt(1-ptc.x*ptc.x);
			if( d<ll)
			{
				MVector ptvofs = ptv*ll/d - ptv;
				ptc += ptvofs;
				ptc = ptc*vtm[i];
				res.push_back(ptc);
				d = 1-ll/d;
				weight.push_back(d);
				wsum += d;
			}

//*/
			MPoint ptc = pt*vtm_inv[i];

			MVector ptv = ptc;
			double d = ptv.length();
			if( d<1)
			{
				double d2 = d*(1-envelope) + 1*envelope;
				ptv *= d2/d;
				ptc = MPoint(ptv)*vtm[i];
				res.push_back(ptc);
				d = 1-d;
				weight.push_back(d);
				wsum += d;
			}
//*/
		}
		if(res.empty())
			continue;
		pt = MPoint(0, 0, 0);
		for(i=0; i<(int)res.size(); i++)
		{
			double w = weight[i]/wsum;
			pt += res[i]*w;
		}

		pt *= m_inv;
		iter.setPosition(pt);
	}

	return MS::kSuccess;
}

void* balledPush::creator()
{
	return new balledPush();
}

MStatus balledPush::initialize()
{
	MFnNumericAttribute nAttr;
	MFnTypedAttribute	typedAttr;
	MFnCompoundAttribute comAttr;
	MFnEnumAttribute eAttr;
	MFnGenericAttribute genAttr;
	MStatus				stat;

	try
	{
		i_balledTransform = typedAttr.create( "balledTransform", "bt", MFnData::kMatrix);
		::addAttribute(i_balledTransform, atInput|atArray );
		stat = attributeAffects( i_balledTransform, outputGeom );
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

