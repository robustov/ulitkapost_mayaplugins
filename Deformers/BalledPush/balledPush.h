#pragma once
#include "pre.h"

//
// Copyright (C) 
// File: balledPushNode.h
//
// Deformer Node: balledPush

#include <maya/MPxNode.h>
#include <maya/MPxDeformerNode.h>
#include <maya/MDataBlock.h>
#include <maya/MPlug.h>
#include <maya/MItGeometry.h>
#include <maya/MMatrix.h>

#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 

class balledPush : public MPxDeformerNode
{
public:
	balledPush();
	virtual	~balledPush(); 

    virtual MStatus deform(
		MDataBlock& data,
		MItGeometry& iter,
		const MMatrix& mat,
		unsigned int multiIndex);

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_balledTransform;		// ��������� ��������

public:
	static MTypeId id;
};

