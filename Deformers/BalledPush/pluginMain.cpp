#include "pre.h"

#include <maya/MFnPlugin.h>
#include "balledPush.h"

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

	stat = plugin.registerNode( "balledPush", balledPush::id, balledPush::creator, balledPush::initialize, MPxNode::kDeformerNode );
	if (!stat) {
		displayString("cant register node balledPush");
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterNode( balledPush::id );
	if (!stat) displayString("cant deregister balledPush");

	return stat;
}
