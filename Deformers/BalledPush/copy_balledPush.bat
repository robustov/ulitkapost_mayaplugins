set BIN_DIR=.\out\BalledPush
set BIN_DIR=\\server\bin70

mkdir %BIN_DIR%
mkdir %BIN_DIR%\bin
mkdir %BIN_DIR%\mel
mkdir %BIN_DIR%\slim
mkdir %BIN_DIR%\docs

copy Bin\BalledPush.mll									%BIN_DIR%\bin

copy Mel\BalledPushMenu.mel								%BIN_DIR%\mel
copy Mel\AEballedPushTemplate.mel						%BIN_DIR%\mel

pause