//
// Copyright (C) 
// 
// File: ulVolumeFilterNode.cpp
//
// Dependency Graph Node: ulVolumeFilter
//
// Author: Maya Plug-in Wizard 2.0
//

#include "ulVolumeFilter.h"

#include "pre.h"
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MPoint.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include "Math/Ramp.h"

#include <maya/MGlobal.h>

// You MUST change this to a unique value!!!  The id is a 32bit value used
// to identify this type of node in the binary file format.  
//
MTypeId ulVolumeFilter::id( TypeIdBase+00 );

MObject ulVolumeFilter::i_volumeTransform;
MObject ulVolumeFilter::i_volumeType;
MObject ulVolumeFilter::i_inputGeometry;
MObject ulVolumeFilter::i_weightRamp;

ulVolumeFilter::ulVolumeFilter()
{
}
ulVolumeFilter::~ulVolumeFilter()
{
}

MStatus ulVolumeFilter::deform(
	MDataBlock& data,
	MItGeometry& iter,
	const MMatrix& m,
	unsigned int multiIndex)
{
	MStatus stat;
 
	MMatrix m_inv = m.inverse();
	float envelope = data.inputValue(this->envelope,&stat).asFloat();
	if( !stat)
		return MS::kFailure;

	if( envelope==0.f) 
		return MS::kSuccess;

	MArrayDataHandle igah = data.inputArrayValue( this->i_inputGeometry, &stat );
	if( !stat) 
		return MS::kFailure;

	if( !igah.jumpToElement(multiIndex))
		return MS::kFailure;
	
	MDataHandle igdh = igah.inputValue();

//	bool bsubdive = (igdh.type() == MFnData::kSubdSurface);
	std::map<int, MPoint> points;
	MItGeometry iter2(igdh);
	int i=0; 
	for ( iter2.reset(); !iter2.isDone(); iter2.next(), i++) 
	{
		MPoint pt = iter2.position(MSpace::kWorld);
		int index = iter2.index();
//		if( bsubdive) 
//			index = i;
		points[index] = pt;
	}

//	MFnData::Type t = data.inputValue(this->i_volumeTransform, &stat).type();
	MMatrix vtm = MFnMatrixData(data.inputValue(this->i_volumeTransform).data()).matrix();
	MMatrix vtm_inv = vtm.inverse();


	// ramp
	Math::Ramp ramp;
	MRampAttribute rampattr(thisMObject(), this->i_weightRamp);
	ramp.set(rampattr, 0, 1, 1);

	for ( iter.reset(); !iter.isDone(); iter.next()) 
	{
		// pt1
		MPoint pt1 = iter.position(MSpace::kObject);
		int index = iter.index();
		MPoint wpt1 = pt1*m;

		// pt2
		std::map<int, MPoint>::iterator it = points.find(index);
		if( it==points.end()) continue;
		MPoint wpt2 = it->second;
//		MPoint wpt2 = pt2*m;
		
		MPoint testpoint = wpt1*vtm_inv;
		double dist = testpoint.distanceTo(MPoint(0, 0, 0));

		double w = ramp.getValue( (float)dist);

		// ��������� ���
//		double w = __min( dist, 1.);

		// ��������
		w *= envelope;
		MPoint pt = wpt1*(1-w) + wpt2*(w);
		pt = pt*m_inv;
		iter.setPosition(pt);
	}

	return MS::kSuccess;
}

void* ulVolumeFilter::creator()
{
	return new ulVolumeFilter();
}

MObject CreateRampAttr(
	std::string fullname, 
	std::string shortname);

MStatus ulVolumeFilter::initialize()
{
	MFnNumericAttribute nAttr;
	MFnTypedAttribute	typedAttr;
	MFnCompoundAttribute comAttr;
	MFnEnumAttribute eAttr;
	MFnGenericAttribute genAttr;
	MStatus				stat;

	try
	{
		i_volumeTransform = typedAttr.create( "volumeTransform", "vt", MFnData::kMatrix);
		::addAttribute(i_volumeTransform, atInput );
		stat = attributeAffects( i_volumeTransform, outputGeom );

		i_volumeType = eAttr.create("volumeType", "vtr", 2);
		eAttr.addField("Sphere", 0);
		eAttr.addField("Cylinder", 1);
		eAttr.addField("Cube", 2);
		::addAttribute(i_volumeType, atInput );
		stat = attributeAffects( i_volumeType, outputGeom );

		i_inputGeometry = genAttr.create( "inputGeometry2", "ig2" );
		genAttr.addAccept( MFnData::kLattice );
		genAttr.addAccept( MFnData::kMesh );
		genAttr.addAccept( MFnData::kNurbsCurve );
		genAttr.addAccept( MFnData::kNurbsSurface );
		genAttr.addAccept( MFnData::kSubdSurface );
		::addAttribute(i_inputGeometry, atInput|atArray );
		stat = attributeAffects( i_inputGeometry, outputGeom );

		i_weightRamp = CreateRampAttr("weightRamp", "wr");
		::addAttribute(i_weightRamp, atInput);
		stat = attributeAffects( i_weightRamp, outputGeom );

	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}


MObject CreateRampAttr(
	std::string fullname, 
	std::string shortname)
{
	MObject rampchilds[3];
	MObject i_RampAttr;
	MStatus stat;
	MFnNumericAttribute nAttr;
	MFnEnumAttribute eAttr;
	MFnCompoundAttribute cAttr;
	std::string fn, sn;

	fn = fullname + "_Position";
	sn = shortname + std::string("p");
	rampchilds[0] = nAttr.create(fn.c_str(), sn.c_str(), MFnNumericData::kFloat, 0.0);
	float vmin = -1;
	float vmax = 1;
	
	nAttr.setSoftMin(vmin);
	nAttr.setSoftMax(vmax);
//	stat = addAttribute(i_HairAttr[1]);

	fn = fullname + std::string("_FloatValue");
	sn = shortname + std::string("fv");
	rampchilds[1] = nAttr.create(fn.c_str(), sn.c_str(), MFnNumericData::kFloat, 0.0);
	nAttr.setSoftMin(vmin);
	nAttr.setSoftMax(vmax);
//	stat = addAttribute(fv);

	fn = std::string(fullname) + std::string("_Interp");
	sn = std::string(shortname) + std::string("i");
	rampchilds[2] = eAttr.create(fn.c_str(), sn.c_str());
	eAttr.addField("None",   0);
	eAttr.addField("Linear", 1);
	eAttr.addField("Smooth", 2);
	eAttr.addField("Spline", 3);
//	stat = addAttribute(in);

	i_RampAttr = cAttr.create(fullname.c_str(), shortname.c_str());
	cAttr.addChild(rampchilds[0]);
	cAttr.addChild(rampchilds[1]);
	cAttr.addChild(rampchilds[2]);
	cAttr.setWritable(true);
	cAttr.setStorable(true);
	cAttr.setKeyable(false);
	cAttr.setArray(true);
	return i_RampAttr;
}