set BIN_DIR=.\out\ulVolumeFilter
set BIN_DIR=\\server\bin70

mkdir %BIN_DIR%
mkdir %BIN_DIR%\bin
mkdir %BIN_DIR%\mel
mkdir %BIN_DIR%\slim
mkdir %BIN_DIR%\docs

copy Bin\ulVolumeFilter.mll							%BIN_DIR%\bin

copy Mel\ulVolumeFilterMenu.mel						%BIN_DIR%\mel
copy Mel\AEulVolumeFilterTemplate.mel				%BIN_DIR%\mel
copy Mel\AEulVolumeFilterShapeTemplate.mel			%BIN_DIR%\mel
pause