//
// Copyright (C) 
// File: ulVolumeFilterShapeCmd.cpp
// MEL Command: ulVolumeFilterShape

#include "ulVolumeFilterShape.h"

#include <maya/MGlobal.h>

MTypeId ulVolumeFilterShape::id( TypeIdBase+01 );

MObject ulVolumeFilterShape::i_input;
MObject ulVolumeFilterShape::o_output;

ulVolumeFilterShape::ulVolumeFilterShape()
{
}
ulVolumeFilterShape::~ulVolumeFilterShape()
{
}

MStatus ulVolumeFilterShape::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	if( plug == o_output )
	{
		MDataHandle inputData = data.inputValue( i_input, &stat );

		MDataHandle outputData = data.outputValue( plug, &stat );

		data.setClean(plug);
		return MS::kSuccess;
	}

	return MS::kUnknownParameter;
}

bool ulVolumeFilterShape::isBounded() const
{
	MObject thisNode = thisMObject();
	return false;
}

MBoundingBox ulVolumeFilterShape::boundingBox() const
{
	MObject thisNode = thisMObject();

	MBoundingBox box;
	//...
	return box;
}

void ulVolumeFilterShape::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MObject thisNode = thisMObject();

	MPlug plug(thisNode, i_input);

	view.beginGL(); 
	//...
	view.endGL();
};


void* ulVolumeFilterShape::creator()
{
	return new ulVolumeFilterShape();
}

MStatus ulVolumeFilterShape::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_input = numAttr.create ("i_input","iin", MFnNumericData::kDouble, 0.01, &stat);
			numAttr.setMin(0.0);	
			numAttr.setMax(1.0);	
			::addAttribute(i_input, atInput);
		}
		{
			o_output = numAttr.create( "o_output", "out", MFnNumericData::kDouble, 0.01, &stat);
			::addAttribute(o_output, atOutput);
			stat = attributeAffects( i_input, o_output );
		}
		if( !MGlobal::sourceFile("AEulVolumeFilterShapeTemplate.mel"))
		{
			displayString("error source AEulVolumeFilterShapeTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}