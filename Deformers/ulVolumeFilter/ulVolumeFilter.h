#pragma once
#include "pre.h"

//
// Copyright (C) 
// File: ulVolumeFilterNode.h
//
// Deformer Node: ulVolumeFilter

#include <maya/MPxNode.h>
#include <maya/MPxDeformerNode.h>
#include <maya/MDataBlock.h>
#include <maya/MPlug.h>
#include <maya/MItGeometry.h>
#include <maya/MMatrix.h>

#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 

class ulVolumeFilter : public MPxDeformerNode
{
public:
	ulVolumeFilter();
	virtual	~ulVolumeFilter(); 

    virtual MStatus deform(
		MDataBlock& data,
		MItGeometry& iter,
		const MMatrix& mat,
		unsigned int multiIndex);

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_volumeTransform;		// ��������� ��������
	static MObject i_volumeType;			// ��� ��������
	static MObject i_inputGeometry;			// ������ ������� ���������
	static MObject i_weightRamp;			// ���� ����
public:
	static MTypeId id;
};

