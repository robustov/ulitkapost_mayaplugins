#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>

 
class ulVolumeFilterShape : public MPxLocatorNode
{
public:
	ulVolumeFilterShape();
	virtual ~ulVolumeFilterShape(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_input;
	static MObject o_output;

public:
	static MTypeId id;
};


