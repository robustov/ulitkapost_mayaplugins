#include "pre.h"

#include <maya/MFnPlugin.h>
#include "ulVolumeFilterShape.h"
#include "ulVolumeFilter.h"

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

	stat = plugin.registerNode( "ulVolumeFilterShape", ulVolumeFilterShape::id, ulVolumeFilterShape::creator,
								  ulVolumeFilterShape::initialize, MPxNode::kLocatorNode );
	if (!stat) 
	{
		displayString("cant register node ulVolumeFilterShape");	
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerNode( "ulVolumeFilter", ulVolumeFilter::id, ulVolumeFilter::creator, ulVolumeFilter::initialize, MPxNode::kDeformerNode );
	if (!stat) {
		displayString("cant register node ulVolumeFilter");
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterNode( ulVolumeFilterShape::id );
	if (!stat) displayString("cant deregister ulVolumeFilterShape");

	stat = plugin.deregisterNode( ulVolumeFilter::id );
	if (!stat) displayString("cant deregister ulVolumeFilter");

	return stat;
}
