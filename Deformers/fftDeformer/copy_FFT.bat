set BIN_DIR=.\out\ocellaris
set BIN_DIR=\\server\bin

if "%DSTPATH%"=="" (set BIN_DIR=%BIN_DIR%) else (set BIN_DIR=%DSTPATH%)

rem SN
mkdir %BIN_DIR%
mkdir %BIN_DIR%\bin
mkdir %BIN_DIR%\mel

copy binrelease85\fftDeformer.mll				%BIN_DIR%\bin
copy Mel\AEfft2Template.mel						%BIN_DIR%\mel
copy Mel\fftDeformerMenu.mel					%BIN_DIR%\mel
copy binrelease85\fftWaves.dll					%BIN_DIR%\bin

pause