# Microsoft Developer Studio Project File - Name="yTwistNode" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=yTwistNode - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "fftDeformer.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "fftDeformer.mak" CFG="yTwistNode - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "yTwistNode - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "yTwistNode - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "yTwistNode - Win32 ReleaseDebug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "yTwistNode - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\Release\fft"
# PROP Intermediate_Dir "..\..\Release\fft"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "$(MAYA50)\include" /I "../Ivanov\FLT_SIGN" /I "../baikov" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "NT_PLUGIN" /D "FFTW_ENABLE_FLOAT" /FR /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 Foundation.lib OpenMayaAnim.lib OpenMaya.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /machine:I386 /out:"..\..\SuperNova\Debug\fftDeformer.mll" /libpath:"$(MAYA50)\lib" /export:initializePlugin /export:uninitializePlugin
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "yTwistNode - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\Debug\fft"
# PROP Intermediate_Dir "..\..\Debug\fft"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "$(MAYA50)\include" /I "../Ivanov\FLT_SIGN" /I "../baikov" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "NT_PLUGIN" /D "FFTW_ENABLE_FLOAT" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /debug /machine:I386
# ADD LINK32 Foundation.lib OpenMayaAnim.lib OpenMaya.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"..\..\SuperNova\Debug\fftDeformer.mll" /libpath:"$(MAYA50)\lib" /export:initializePlugin /export:uninitializePlugin
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "yTwistNode - Win32 ReleaseDebug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "simpleSo"
# PROP BASE Intermediate_Dir "simpleSo"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\ReleaseDebug\fft"
# PROP Intermediate_Dir "..\..\ReleaseDebug\fft"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /Zi /O2 /I "$(MAYA50)\include" /I "../Ivanov\FLT_SIGN" /I "../baikov" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "NT_PLUGIN" /D "FFTW_ENABLE_FLOAT" /FR /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib Foundation.lib OpenMaya.lib /nologo /subsystem:windows /dll /pdb:"ReleaseDebug\yTwistNode.pdb" /machine:I386 /out:".\yTwistNode.mll"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Foundation.lib OpenMayaAnim.lib OpenMaya.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"..\..\SuperNova\Debug\fftDeformer.mll" /libpath:"$(MAYA50)\lib" /export:initializePlugin /export:uninitializePlugin
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "yTwistNode - Win32 Release"
# Name "yTwistNode - Win32 Debug"
# Name "yTwistNode - Win32 ReleaseDebug"
# Begin Group "Ivanov"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Ivanov\FLT_SIGN\complex.cpp
# End Source File
# Begin Source File

SOURCE=..\Ivanov\FLT_SIGN\Complex.h
# End Source File
# Begin Source File

SOURCE=..\Ivanov\FLT_SIGN\mWaves.cpp
# End Source File
# Begin Source File

SOURCE=..\Ivanov\FLT_SIGN\mWaves.h
# End Source File
# Begin Source File

SOURCE=.\xmm.asm
# End Source File
# Begin Source File

SOURCE=..\Ivanov\FLT_SIGN\xmm.obj
# End Source File
# End Group
# Begin Group "Baikov"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Baikov\Geo\Array.h
# End Source File
# Begin Source File

SOURCE=..\Baikov\Geo\config.h
# End Source File
# Begin Source File

SOURCE=..\Baikov\SeaWater.cpp
# End Source File
# Begin Source File

SOURCE=..\Baikov\SeaWater.h
# End Source File
# Begin Source File

SOURCE=..\Baikov\Geo\Vector.h
# End Source File
# Begin Source File

SOURCE=..\Baikov\fftw.lib
# End Source File
# End Group
# Begin Group "Mitka"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Mitka\FftWaves.cpp
# End Source File
# Begin Source File

SOURCE=..\Mitka\FftWaves.h
# End Source File
# Begin Source File

SOURCE=..\Mitka\matrixMxN.h
# End Source File
# End Group
# Begin Source File

SOURCE=D:\3D\Maya5.0\scripts\AETemplates\AEfftTemplate.mel
# End Source File
# Begin Source File

SOURCE=.\fftDeformer.cpp

!IF  "$(CFG)" == "yTwistNode - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "yTwistNode - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "yTwistNode - Win32 ReleaseDebug"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\fftDeformer2.cpp

!IF  "$(CFG)" == "yTwistNode - Win32 Release"

!ELSEIF  "$(CFG)" == "yTwistNode - Win32 Debug"

!ELSEIF  "$(CFG)" == "yTwistNode - Win32 ReleaseDebug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\mel.txt
# End Source File
# Begin Source File

SOURCE=..\Ivanov\FLT_SIGN\OceanWaves.cpp
# End Source File
# Begin Source File

SOURCE=..\Ivanov\FLT_SIGN\OceanWaves.h
# End Source File
# Begin Source File

SOURCE=.\SrcWaves.cpp
# End Source File
# Begin Source File

SOURCE=.\SrcWaves.h
# End Source File
# Begin Source File

SOURCE=.\TestWaves.cpp
# End Source File
# Begin Source File

SOURCE=.\TestWaves.h
# End Source File
# End Target
# End Project
