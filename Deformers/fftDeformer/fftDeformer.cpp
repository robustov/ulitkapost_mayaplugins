#include <string.h>
#define REQUIRE_IOSTREAM
#define Pi                  3.14159265358979323846

#include <maya/MIOStream.h>
#include <maya/MFnPlugin.h>

#include <maya/MPxDeformerNode.h>
#include <maya/MItGeometry.h>
//using namespace std;

#include <maya/MTypeId.h> 
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>

#include <maya/MFnNumericAttribute.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnUnitAttribute.h>

#include <maya/MPoint.h>
#include <maya/MMatrix.h>
#include <maya/MTime.h>
#include <maya/MGlobal.h>
#include <maya/MFnMeshData.h>
#include <maya/MArgList.h>
#include <maya/MPxCommand.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MVector.h>
#include <maya/MBoundingBox.h>

#include <math.h>
#include <algorithm>

//#include "mWaves.h"
//#include "TestWaves.h"
//#include "SrcWaves.h"
//#include "OceanWaves.h"
//#include "../baikov/SeaWater.h"
#include "Mitka/FftWaves.h"
#include "Mitka/TestEmitter.h"
#include "Mitka/OceanEmitter.h"
#include "Mitka/OceanEmitter2.h"

inline void displayString(const char* text, ...)
{
	va_list argList; va_start(argList, text);
	char sbuf[256];
	_vsnprintf(sbuf, 255, text, argList);
	va_end(argList);
	MGlobal::displayInfo(sbuf);
}

inline void displayPoint( const char* text, const MPoint& p)
{
	displayString("%s: %g, %g, %g, %g", text, p[0], p[1], p[2], p[3]);
}

inline void displayMatrix( const MMatrix& L)
{
	displayString("%g, %g, %g, %g", L(0, 0), L(0, 1), L(0, 2), L(0, 3));
	displayString("%g, %g, %g, %g", L(1, 0), L(1, 1), L(1, 2), L(1, 3));
	displayString("%g, %g, %g, %g", L(2, 0), L(2, 1), L(2, 2), L(2, 3));
	displayString("%g, %g, %g, %g", L(3, 0), L(3, 1), L(3, 2), L(3, 3));
}

#define McheckErr(stat,msg)		\
	if ( MS::kSuccess != stat ) {	\
		MGlobal::displayError(msg);	\
		return MS::kFailure;		\
	}



class yTwist : public MPxDeformerNode
{
public:
						yTwist();
	virtual				~yTwist();

	static  void*		creator();
	static  MStatus		initialize();

	// deformation function
	//
    virtual MStatus   	deform(MDataBlock& 		block,
							   MItGeometry& 	iter,
							   const MMatrix& 	mat,
							   unsigned int 	multiIndex);

	MStatus compute ( const MPlug & plug, MDataBlock & block );

public:
	// yTwist attributes
	//
	static  MObject     mode;
	static  MObject     whatshow;
	static  MObject		filter;
	static  MObject		XZenvelope;

	static  MObject     dimention;
	static  MObject     physsize;
	static  MObject		time;
	static  MObject		outtime;

	static  MObject     WaveDumper;
	static  MObject		WindAngle;
	static  MObject		WindSpeed;

	static  MTypeId		id;

private:

};

MTypeId     yTwist::id( 0x36100e );

////////////////////////
// yTwist attributes  //
////////////////////////

MObject     yTwist::mode;
MObject     yTwist::whatshow;

MObject		yTwist::time;
MObject		yTwist::outtime;
MObject     yTwist::dimention;
MObject     yTwist::physsize;
MObject     yTwist::filter;
MObject     yTwist::XZenvelope;

MObject     yTwist::WaveDumper;
MObject     yTwist::WindAngle;
MObject     yTwist::WindSpeed;



yTwist::yTwist() 
//
//	Description:
//		constructor
//
{ 
}

yTwist::~yTwist()
//
//	Description:
//		destructor
//
{}

void* yTwist::creator()
//
//	Description:
//		create the yTwist
//
{
	return new yTwist();
}

MStatus yTwist::initialize()
//
//	Description:
//		initialize the attributes
//
{
	MStatus returnStatus;

	{
		MFnUnitAttribute unitAttr;
		yTwist::time = unitAttr.create( "time", "tm",
											MFnUnitAttribute::kTime,
											0.0, &returnStatus );
		McheckErr(returnStatus, "ERROR creating animCube time attribute\n");
		returnStatus = addAttribute( yTwist::time); 
		McheckErr(returnStatus, "ERROR adding time attribute\n");
		unitAttr.setDefault(0.0);
		unitAttr.setKeyable(true);
	}
	{
		MFnEnumAttribute enumAttr;
		yTwist::mode = enumAttr.create( "waveMode", "wmo", 1, &returnStatus );
		enumAttr.addField( "Test", 0 );
		enumAttr.addField( "Ocean", 1 );
		enumAttr.addField( "Ocean2", 2 );
		enumAttr.setDefault(0);
		addAttribute( mode); 
	}
	{
		MFnEnumAttribute enumAttr;
		yTwist::whatshow = enumAttr.create( "whatshow", "wsd", 0, &returnStatus );
		enumAttr.addField( "XYZ", 0 );
		enumAttr.addField( "XZ", 1 );
		enumAttr.addField( "Y", 2 );
		enumAttr.addField( "X", 3 );
		enumAttr.addField( "Z", 4 );
		enumAttr.addField( "Spectrum", 5 );
		enumAttr.addField( "W", 6 );
		enumAttr.addField( "Random", 7 );
		enumAttr.addField( "normals", 8 );
		enumAttr.addField( "Phillips", 9 );
		enumAttr.setDefault(0);
		addAttribute( whatshow); 
	}
	

	{
		MFnNumericAttribute nAttr;
		yTwist::dimention = nAttr.create( "dimention", "dim", MFnNumericData::kLong );
			nAttr.setDefault(32);
		nAttr.setKeyable(true);
		addAttribute( dimention); 
	}
	{
		MFnNumericAttribute nAttr;
		yTwist::outtime = nAttr.create( "outtime", "otim", MFnNumericData::kDouble );
		nAttr.setKeyable(false);
		nAttr.setWritable(false);
		addAttribute( outtime); 
	}
	
	{
		MFnNumericAttribute nAttr;
		yTwist::physsize = nAttr.create( "physsize", "phsd", MFnNumericData::kDouble, 20.0f );
		nAttr.setMin(1);
		nAttr.setSoftMax(70);
		nAttr.setMax(1000);
		nAttr.setKeyable(true);
		addAttribute( physsize); 
	}
	{
		MFnNumericAttribute nAttr;
		yTwist::XZenvelope = nAttr.create( "XZenvelope", "xzen", MFnNumericData::kDouble, 1.0f );
		nAttr.setMin(0);
		nAttr.setSoftMax(2);
		nAttr.setMax(10);
		nAttr.setKeyable(true);
		addAttribute( XZenvelope); 
	}
	
	{
		MFnNumericAttribute nAttr;
		yTwist::filter = nAttr.create( "filtersize", "flts", MFnNumericData::kLong);
		nAttr.setMin(4);
		nAttr.setMax(512);
		nAttr.setDefault(32);
		nAttr.setKeyable(true);
		addAttribute( filter); 
	}



	{ // Wind
		{
			MFnNumericAttribute nAttr;
			yTwist::WaveDumper = nAttr.create( "WaveDumper", "oced", MFnNumericData::kDouble, 1.0f );
			nAttr.setMin(0.f);
			nAttr.setSoftMax(2.1f);
			nAttr.setMax(10.f);
			nAttr.setKeyable(true);
			addAttribute( WaveDumper); 
		}
		{
			MFnUnitAttribute unitAttr;
			yTwist::WindAngle = unitAttr.create( "WindAngle", "ocean",
												MFnUnitAttribute::kAngle,
												0.0, &returnStatus );
			unitAttr.setDefault(0.0);
			unitAttr.setMin(0.f);
			unitAttr.setMax(Pi*2);
			unitAttr.setKeyable(true);
			returnStatus = addAttribute( yTwist::WindAngle);
		}
		{
			MFnNumericAttribute nAttr;
			yTwist::WindSpeed = nAttr.create( "WindSpeed", "ocesp", MFnNumericData::kDouble, 5.0f );
			nAttr.setMin(1.f);
			nAttr.setSoftMax(20.f);
			nAttr.setMax(100.f);
			nAttr.setKeyable(true);
			addAttribute( WindSpeed); 
		}
	}

	
	// affects
	//
    attributeAffects( yTwist::mode, yTwist::outputGeom );
	attributeAffects( yTwist::whatshow, yTwist::outputGeom );

    attributeAffects( yTwist::dimention, yTwist::outputGeom );
	attributeAffects( yTwist::physsize, yTwist::outputGeom );
	attributeAffects( yTwist::filter, yTwist::outputGeom );
	attributeAffects( yTwist::XZenvelope, yTwist::outputGeom );
	
	attributeAffects( yTwist::WaveDumper, yTwist::outputGeom );
	attributeAffects( yTwist::WindAngle, yTwist::outputGeom );
	attributeAffects( yTwist::WindSpeed, yTwist::outputGeom );
	
    attributeAffects( yTwist::time, yTwist::outputGeom );
	attributeAffects( yTwist::time, yTwist::outtime);
	McheckErr(returnStatus, "ERROR adding time attribute\n");

	return MS::kSuccess;
}

MStatus yTwist::compute ( const MPlug & plug, MDataBlock & block )
{
	MStatus status;
	if( plug == outtime)
	{
		MDataHandle timeData = block.inputValue(time,&status);
		McheckErr(status, "Error getting envelope data handle\n");	
		MTime curtime = timeData.asTime();
		double t = curtime.as(MTime::kSeconds);

		MDataHandle outData = block.outputValue(outtime);
		outData.set(t);

		return MS::kSuccess;
	}
	return MS::kUnknownParameter;
}

MStatus
yTwist::deform( MDataBlock& block,
				MItGeometry& iter,
				const MMatrix& m,
				unsigned int multiIndex)
//
// Method: deform
//
// Description:   Deform the point with a yTwist algorithm
//
// Arguments:
//   block		: the datablock of the node
//	 iter		: an iterator for the geometry to be deformed
//   m    		: matrix to transform the point into world space
//	 multiIndex : the index of the geometry that we are deforming
//
//
{
	MStatus status = MS::kSuccess;
	

	MDataHandle dimentionData = block.inputValue(dimention,&status);
	McheckErr(status, "Error getting angle data handle\n");
	int s = dimentionData.asLong()-1;
	if(s<8) s = 7;
	int size = 1;
	for(; s; s = s>>1) 
		size = size<<1;

	// determine the envelope (this is a global scale factor)
	//
	MDataHandle envData = block.inputValue(envelope,&status);
	McheckErr(status, "Error getting envelope data handle\n");	
	float env = envData.asFloat();	

	MDataHandle physsizeData = block.inputValue(physsize,&status);
	McheckErr(status, "Error getting envelope data handle\n");	
	float pSize = (float)physsizeData.asDouble();

	MDataHandle filterData = block.inputValue(filter,&status);
	McheckErr(status, "Error getting envelope data handle\n");	
	int filterSize = filterData.asLong();

	float xzenv = (float)block.inputValue(XZenvelope,&status).asDouble();

	MDataHandle timeData = block.inputValue(time,&status);
	McheckErr(status, "Error getting envelope data handle\n");	
	MTime curtime = timeData.asTime();

	double t = curtime.as(MTime::kSeconds);

	float fWaveDumper = (float)block.inputValue(WaveDumper,&status).asDouble();
	float fWindAngle = (float)block.inputValue(WindAngle,&status).asDouble();
	float fWindSpeed = (float)block.inputValue(WindSpeed,&status).asDouble();

	{
		FftWaves waves;

		short sMode = block.inputValue( mode ).asShort();
		short show = block.inputValue( whatshow ).asShort();

		OceanEmitter  oceanemitter(size, pSize, fWaveDumper, fWindAngle, fWindSpeed);
		OceanEmitter2 oceanemitter2(size, pSize, fWaveDumper, fWindAngle, fWindSpeed); 

		switch(sMode)
		{
		case 0:
			{
				TestEmitter emitter(size);
				waves.init(size, pSize, filterSize, &emitter);
				break;
			}
		case 1:
			{
				waves.init(size, pSize, filterSize, &oceanemitter);
				break;
			}
		case 2:
			{
				waves.init(size, pSize, filterSize, &oceanemitter2);
				break;
			}
		}
		waves.update((float)t);
 
		MBoundingBox box;
		for ( ; !iter.isDone(); iter.next()) 
		{
			MPoint pt = iter.position();
			box.expand(pt);
		}
		iter.reset();
		MVector dist = box.max()-box.min();
		float chopsScale = xzenv*(dist.x + dist.z)/2;

//	displayPoint( "em min: ", box.min());
//	displayPoint( "em max: ", box.max());

		for ( ; !iter.isDone(); iter.next()) 
		{
			MPoint pt = iter.position();
			float px = ((float)pt.x - box.min().x)/dist.x;	// [0..1]
			float pz = ((float)pt.z - box.min().z)/dist.z;	// [0..1]
			int X = (int)floor( px*(size)+0.5);
			int Y = (int)floor( pz*(size)+0.5);
			X = (X+size*10) % size;
			Y = (Y+size*10) % size;
			if(X==size) X = 0;
			if(Y==size) Y = 0;

			MVector dp(0.f, 0.f, 0.f);
			switch(show)
			{
			case 0:
//				dp.y = waves.getYvalue(X, Y);
//				dp.x = waves.getChopX(X, Y)*chopsScale;
//				dp.z = waves.getChopZ(X, Y)*chopsScale;
				dp.y = waves.getYvalue(px, pz);
				dp.x = waves.getChopX(px, pz)*chopsScale;
				dp.z = waves.getChopZ(px, pz)*chopsScale;
				break;
			case 1:
				dp.x = waves.getChopX(px, pz)*chopsScale;
				dp.z = waves.getChopZ(px, pz)*chopsScale;
				break;
			case 2:
				dp.y = waves.getYvalue(px, pz);
				break;
			case 3:
				dp.x = waves.getChopX(px, pz)*chopsScale;
				break;
			case 4:
				dp.z = waves.getChopZ(px, pz)*chopsScale;
				break;

			case 5:
				dp.y = waves.getSpectrum(X, Y);
				break;
			case 6:
				dp.y = waves.getWvalue(X, Y);
				break;
			case 7:
				dp.y = waves.getRandom(X, Y)/((float)RAND_MAX);
				break;
			case 8:
				dp.x = waves.getNormalX(X, Y)*chopsScale/size;
				dp.z = waves.getNormalZ(X, Y)*chopsScale/size;
				break;
			case 9:
				if( sMode!=2)
					dp.y = oceanemitter.getPhillips(X, Y)*100;
				else
					dp.y = oceanemitter2.getPhillips(X, Y)*100;
				break;
			}
			pt.y  += dp.y*env;

			bool bRotate = false;
			if(bRotate)
			{
				pt.x += -dp.z;
				pt.z += dp.x;
			}
			else
			{
				pt.x += dp.x;
				pt.z += dp.z;
			}
			iter.setPosition(pt);
		}
	}
	return status;
}

// standard initialization procedures
//

class CMD_AllignTo2powN : public MPxCommand 
{	
public:
	static void* creator();
	MStatus	doIt( const MArgList& args);
};

void* CMD_AllignTo2powN::creator()										
{																
	return new CMD_AllignTo2powN;										
}																

MStatus	CMD_AllignTo2powN::doIt( const MArgList& args)
{
	MStatus stat;
	if( args.length()!=1)
	{
		setResult((int)1);
		return MS::kSuccess;
	}
	int s = args.asInt(0)-1;
	if(s<=0)
	{
		setResult((int)1);
		return MS::kSuccess;
	}
	int size = 1;
	for(; s; s = s>>1) 
		size = size<<1;

	setResult(size);
	return MS::kSuccess;
}


MStatus initializePlugin( MObject obj )
{
	MStatus result;
	MFnPlugin plugin( obj, "Mitka Robustov", "3.0", "Any");
//	result = plugin.registerCommand( "allignTo2powN", CMD_AllignTo2powN::creator);
	result = plugin.registerNode( "fft2", yTwist::id, yTwist::creator, 
								  yTwist::initialize, MPxNode::kDeformerNode );

	if( !MGlobal::sourceFile("AEfft2Template.mel"))
	{
		displayString("error source AEHairShapeTemplate.mel");
	}
	
	return result;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus result;
	MFnPlugin plugin( obj );
	result = plugin.deregisterNode( yTwist::id );
//	result = plugin.deregisterCommand( "allignTo2powN");
	return result;
}
