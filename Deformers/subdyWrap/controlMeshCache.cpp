//
// Copyright (C) 
// File: controlMeshCacheCmd.cpp
// MEL Command: controlMeshCache

#include "controlMeshCache.h"

#include <maya/MGlobal.h>
#include <maya/MFnMesh.h>
#include <maya/MProgressWindow.h>
#include <maya/MFloatPointArray.h>
#include <maya/MItMeshPolygon.h>

#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include "MathNMaya/MathNMaya.h"
#include <sstream>

pointOnControlMesh pointOnControlMesh::zero(0, 0, 0, 0, 0);

bool controlMeshCache::InitMesh(
	MObject meshobj, 
	int subdivstep,
	bool bBaseInteractive, 
	bool bUseUVspace, 
	const char* UVspaceName)
{
	if( meshobj.isNull()) return false;
	MStatus stat;
	MFnMesh mesh(meshobj, &stat);
	if( !stat)
	{
		displayString("error: control shape must be MESH!");
		bValid = false;
		return false;
	}

	// ��������� ��������� � ���������
	int poligoncount = mesh.numPolygons();
	int vertexcount = mesh.numVertices();
	int facevertexcount = mesh.numFaceVertices();
	if( bValid && 
		this->poligoncount	== poligoncount &&
		this->vertexcount	== vertexcount &&
		this->facevertexcount == facevertexcount &&
		this->subdivstep	== subdivstep &&
		this->bUseUVspace == bUseUVspace &&
		this->UVspaceName == UVspaceName
		)
	{
		if( !bBaseInteractive)
		{
			bValid = true;
			return false;
		}
	}
	this->timeStamp++;
	this->bValid = false;
	this->poligoncount	= poligoncount;
	this->vertexcount	= vertexcount;
	this->facevertexcount = facevertexcount;
	this->subdivstep	= subdivstep;
	this->bUseUVspace = bUseUVspace;
	this->UVspaceName = UVspaceName;

	displayStringD( "controlMeshCache::InitMesh");

	int progressstage = 0;
	if( MGlobal::mayaState()==MGlobal::kInteractive)
	{
		MProgressWindow::reserve();
		MProgressWindow::setInterruptable(false);
		MProgressWindow::setTitle("Build control mesh cache");
		MProgressWindow::startProgress();
		MProgressWindow::setProgressRange(0, 5);
		MProgressWindow::setProgressStatus("...");
	}

	baseverts.clear();
	faces.clear();
	neoverts.clear();
	openedges.clear();

	src_verts.clear();
	src_vertnormals.clear();
	src_facenormals.clear();
//	src_squarefaces.clear();
	src_edges.clear();
	src_edgenormals.clear();

	std::vector<Math::Vec3f> vertexArray;
	{
		MFloatPointArray _vertexArray;	
		mesh.getPoints(_vertexArray, MSpace::kWorld);
		vertexArray.resize(_vertexArray.length());
		for(unsigned v=0; v<_vertexArray.length(); v++)
			::copy(vertexArray[v], _vertexArray[v]);
	}

	// ������ ����������� �����
	if( MGlobal::mayaState()==MGlobal::kInteractive)
	{
		MProgressWindow::setProgress(progressstage++);
		MProgressWindow::setProgressStatus("Read control mesh");
	}

	std::vector<Math::Polygon32> polygons;
	polygons.resize(poligoncount);
	MItMeshPolygon polyIter(meshobj);
	for(polyIter.reset(); !polyIter.isDone(); polyIter.next())
	{
		int polyIndex = polyIter.index();
		MIntArray vertices;
		polyIter.getVertices( vertices);

		Math::Polygon32& f = polygons[polyIndex];
		f.resize(vertices.length());
		for( unsigned x=0; x<vertices.length(); x++)
			f[x] = vertices[x];

	}
	std::map<Math::Edge32, int> edges;

	if( subdivstep==0)
	{
		// ��� �������
		if( MGlobal::mayaState()==MGlobal::kInteractive)
		{
			MProgressWindow::setProgress(progressstage++);
			MProgressWindow::setProgressStatus("Subdivide control mesh");
		}

		neoverts.resize(vertexArray.size());
		baseverts.resize(vertexArray.size());
		src_verts.resize(vertexArray.size());
		src_vertnormals.resize(vertexArray.size());
		for(unsigned v=0; v<vertexArray.size(); v++)
		{
			src_verts[v] = vertexArray[v];
			neoverts[v].data.resize(1);
			neoverts[v].data[0] = std::pair<int, float>(v, 1.f);
		}
		// ������������
		this->faceNormalCount = poligoncount;
		MItMeshPolygon polyIter(meshobj);
		faces.reserve(poligoncount*3);
		facesnormalindex.reserve(poligoncount*3);
		int p=0;
		for(polyIter.reset(); !polyIter.isDone(); polyIter.next(), p++)
		{
			int tngCount = 0;
			polyIter.numTriangles(tngCount);
			for(int hh = 0; hh<tngCount; hh++)
			{
				MPointArray FaceVertex;
				MIntArray FaceVtxTrngl;
				polyIter.getTriangle(hh, FaceVertex, FaceVtxTrngl);
				Math::Face32 face( FaceVtxTrngl[0], FaceVtxTrngl[1], FaceVtxTrngl[2]);
				faces.push_back(face);
				this->facesnormalindex.push_back(p);
			}
		}
		for(int p=0; p<(int)polygons.size(); p++)
		{
			Math::Polygon32& polygon = polygons[p];
			// ������ ���������� ����� ��������
			for(int t=0; t<polygon.size(); t++)
			{
				Math::Edge32 edge(polygon.cycle(t), polygon.cycle(t+1));
				if( edges.find(edge) == edges.end())
					edges[edge] = 1;
				else
					edges[edge]++;
			}
		}
	}
	else
	{
		// ��� ����������� �����
		if( MGlobal::mayaState()==MGlobal::kInteractive)
		{
			MProgressWindow::setProgress(progressstage++);
			MProgressWindow::setProgressStatus("Subdivide control mesh");
		}

		std::vector< Math::Quad32> quads;
		Math::Subdiv(subdivstep, &polygons[0], poligoncount, vertexcount, facevertexcount, quads, this->neoverts);
		baseverts.resize(neoverts.size());
		src_verts.resize(neoverts.size());
		src_vertnormals.resize(neoverts.size());

		// ������� ���������
		if( MGlobal::mayaState()==MGlobal::kInteractive)
		{
			MProgressWindow::setProgress(progressstage++);
			MProgressWindow::setProgressStatus("Subdivide control mesh");
		}

		for(unsigned v=0; v<baseverts.size(); v++)
		{
			basevertex& bv = baseverts[v];
			Math::Vec3f& vert = src_verts[v];

			// p
			vert = Math::Vec3f(0, 0, 0);
			Math::subdivWeight& sdw = neoverts[v];
			for( unsigned z=0; z<sdw.data.size(); z++)
			{
				Math::Vec3f pt = vertexArray[sdw.data[z].first];
				vert += pt*sdw.data[z].second;
			}
		}

		// ������ �����
		this->faceNormalCount = quads.size();
		this->faces.resize(quads.size()*2);
		this->facesnormalindex.resize(quads.size()*2);
		for( unsigned q=0; q<quads.size(); q++)
		{
			Math::Quad32& quad = quads[q];
			Math::Face32 qfaces[2];
			qfaces[0] = Math::Face32(quad[0], quad[1], quad[2]);
			qfaces[1] = Math::Face32(quad[2], quad[3], quad[0]);

			for(int t=0; t<2; t++)
			{
				Math::Face32& face = qfaces[t];
				int f = q*2+t;
				this->faces[f] = face;
				this->facesnormalindex[f] = q;
			}
			// ������ ���������� ����� ��������
			for(t=0; t<quad.size(); t++)
			{
				Math::Edge32 edge(quad.cycle(t), quad.cycle(t+1));
				if( edges.find(edge) == edges.end())
					edges[edge] = 1;
				else
					edges[edge]++;
			}
		}
	}

	/////////////////////////////////////////////////
	// �������� ��������� � ������ (baseverts)
	{
		if( MGlobal::mayaState()==MGlobal::kInteractive)
		{
			MProgressWindow::setProgress(progressstage++);
			MProgressWindow::setProgressStatus("Control mesh: faces");
		}
		// �������� ��������� � ������
		for( unsigned f=0; f<faces.size(); f++)
		{
			Math::Face32& face = faces[f];
			int fni = facesnormalindex[f];
			for( int v=0; v<face.size(); v++)
			{
				basevertex& bv = baseverts[face[v]];

				for( int x=0; x<(int)bv.normals.size(); x++)
					if(bv.normals[x]==fni) break;

				if( x>=(int)bv.normals.size())
					bv.normals.push_back(fni);
			}
		}
	}

	///////////////////////////////////////////////////
	// ����� �������� ����� (openedges) � ��������� basisedgevert
	{
		/*/
		for( unsigned f=0; f<faces.size(); f++)
		{
			Math::Face32& face = faces[f];
			for( int v=0; v<face.size(); v++)
			{
				Math::Edge32 edge(face.cycle(v), face.cycle(v+1));
				if( edges.find(edge) == edges.end())
					edges[edge] = 1;
				else
					edges[edge]++;
			}
		}
		/*/
		src_edges.reserve(edges.size());
		std::map<Math::Edge32, int>::iterator it = edges.begin();
		for(;it != edges.end(); it++)
		{
			const Math::Edge32& edge = it->first;
			src_edges.push_back(edge);
			// basisedgevert
			baseverts[edge.v1].basisedgevert.push_back(edge.v2);
			baseverts[edge.v2].basisedgevert.push_back(edge.v1);

			if(it->second==1)
			{
				openedge ope;
				ope.edge = edge;
				openedges.push_back(ope);
			}
		}
		for(unsigned e=0; e<openedges.size(); e++)
		{
			openedge& ope = openedges[e];
			for(unsigned e1=0; e1<openedges.size(); e1++)
			{
				if(e==e1) continue;
				int ind = openedges[e1].edge.opposite(ope.edge.v1);
				if(ind>=0)
					ope.v_1 = ind;
				ind = openedges[e1].edge.opposite(ope.edge.v2);
				if(ind>=0)
					ope.v2_ = ind;
			}
		}
	}

	// ������ �������� � �������� ������
	calcFaceNormals(
		src_verts,
		src_facenormals		// ������� ��-���
		);
//	calcFaceNormals(
//		src_verts,
//		src_facenormals,	// ������� ��-���
//		src_squarefaces		// ������� �����
//		);
	// ������� ���������
	calcVertNormals(
		src_facenormals,	// ������� ��-���
		src_vertnormals		// ������� ���������
		);
	// ������� �����
	calcEdgeNormals(
		src_facenormals,	// ������� ��-���
		src_edgenormals		// ������� �����
		);

	if( MGlobal::mayaState()==MGlobal::kInteractive)
	{
		MProgressWindow::setProgress(progressstage++);
		MProgressWindow::endProgress();
	}
	bValid = true;
	return bValid;
}

// �������� �����
// �����! ������ ����� �� ������������ �� ��-� (������� ���� ������� � �������� �������)
void controlMeshCache::projectionPoint(
	Math::Vec3f pt,			// world space
	std::vector<pointOnControlMesh>& projectionlist, 
	int maxProjectionCount
	) 
{
	projectionlist.clear();
	if( maxProjectionCount<1) maxProjectionCount=1;

	// �������� �� �����
	for(unsigned f=0; f<faces.size(); f++)
	{
		const Math::Face32& face = faces[f];
		// ������� �����
		// dot �� ��������� � ����� �����
		float hs[3], esq[3], sumsq=0;
		for(int v=0; v<3; v++)
		{
			// ������������ ����� v, v+1
			Math::Edge32 edge(face.cycle(v), face.cycle(v+1));
			Math::Vec3f& en = src_edgenormals[edge];
			Math::Vec3f& ev = src_verts[face.cycle(v+1)] - src_verts[face.cycle(v)];
			Math::Vec3f platen = Math::cross(en, ev).normalized();
			Math::Vec3f pv = pt - src_verts[face.cycle(v)];
			float h = Math::dot(platen, pv);
			if( fabs(h)<1e-5) h=0;
			hs[v] = h;
			esq[v] = ev.length()*h;
			sumsq += esq[v];
		}
		if( (hs[0]<=0 && hs[1]<=0 && hs[2]<=0) ||
			(hs[0]>=0 && hs[1]>=0 && hs[2]>=0))
		{
			// ������ ��������
			Math::Vec3f bary(esq[1]/sumsq, esq[2]/sumsq, esq[0]/sumsq);
            // ��������
			Math::Vec3f pr = bary[0]*src_verts[face[0]] + bary[1]*src_verts[face[1]] + bary[2]*src_verts[face[2]];
			float dist = (pr - pt).length();

			for(int i=0; i<(int)projectionlist.size(); i++)
			{
				if(projectionlist[i].distance>dist) break;
			}
			if(i<maxProjectionCount)
			{
				pointOnControlMesh ponm(dist, f, face, bary);
				if(i>=(int)projectionlist.size())
					projectionlist.push_back(ponm);
				else
					projectionlist.insert( &projectionlist[i], ponm);
				if((int)projectionlist.size()>maxProjectionCount)
					projectionlist.pop_back();
			}
		}
	}
	// �������� �� �������� �����
	{
		for( unsigned e=0; e<openedges.size(); e++)
		{
			const openedge& ope = openedges[e];
			const Math::Edge32& edge = ope.edge;

			Math::Vec3f& v1 = src_verts[edge.v1];
			Math::Vec3f& v2 = src_verts[edge.v2];
			Math::Vec3f n1 = src_vertnormals[edge.v1];
			Math::Vec3f n2 = src_vertnormals[edge.v2];
			Math::Vec3f v_1 = v1 - src_verts[ope.v_1];
			Math::Vec3f v2_ = src_verts[ope.v2_] - v2;
			Math::Vec3f v12 = v2-v1;
			Math::Vec3f v1X = v12.normalized()+v_1.normalized();
			Math::Vec3f v2X = v12.normalized()+v2_.normalized();
			
			Math::Vec3f plate1 = Math::cross( Math::cross(v1X, n1), n1).normalized();
			Math::Vec3f plate2 = -Math::cross( Math::cross(v2X, n2), n2).normalized();

			float w1 = Math::dot(plate1, pt-v1);
			float w2 = Math::dot(plate2, pt-v2);
			if( fabs(w1)<1e-5) w1=0;
			if( fabs(w2)<1e-5) w2=0;

			if( (w1>=0 && w2>=0) || (w1<=0 && w2<=0))
			{
				float param = w1/(w2+w1);

				Math::Vec3f pr = v1*(1-param) + v2*param;
				float dist = (pr - pt).length();

				for(int i=0; i<(int)projectionlist.size(); i++)
				{
					if(projectionlist[i].distance>dist) break;
				}
				if(i<maxProjectionCount)
				{
					pointOnControlMesh ponm(dist, edge.v1, edge.v2, 1-param, param);
					if(i>=(int)projectionlist.size())
						projectionlist.push_back(ponm);
					else
						projectionlist.insert( &projectionlist[i], ponm);
					if((int)projectionlist.size()>maxProjectionCount)
						projectionlist.pop_back();
				}
			}
		}
	}
}

// ������ ���������
void controlMeshCache::calcVerts( 
	MFnMesh& mesh, 
	std::vector<Math::Vec3f>& verts
	)const
{
	unsigned v;

	std::vector<Math::Vec3f> vertexArray;
	{
		MFloatPointArray _vertexArray;
		mesh.getPoints(_vertexArray, MSpace::kWorld);

		vertexArray.resize(_vertexArray.length());
		for(v=0; v<_vertexArray.length(); v++)
			::copy(vertexArray[v], _vertexArray[v]);
	}

	verts.resize(baseverts.size());

	// ������� ���������
	{
		for(v=0; v<verts.size(); v++)
		{
			// p
			Math::Vec3f P(0, 0, 0);
			const Math::subdivWeight& sdw = this->neoverts[v];
			for( unsigned z=0; z<sdw.data.size(); z++)
			{
				Math::Vec3f& pt = vertexArray[sdw.data[z].first];
				P += pt*sdw.data[z].second;
			}
			verts[v] = P;
		}
	}
}

// ������ �������� �����	
void controlMeshCache::calcEdgeNormals(
	const std::vector<Math::Vec3f>& facenormals,	// ������� ��-���
	std::map< Math::Edge32, Math::Vec3f>& edgenormals	// ������� �����
	)const
{
	edgenormals.clear();
	for( unsigned f=0; f<this->faces.size(); f++)
	{
		const Math::Face32& face = this->faces[f];
		int fn = this->facesnormalindex[f];
		const Math::Vec3f& norm = facenormals[fn];
		for( int v=0; v<face.size(); v++)
		{
			Math::Edge32 edge(face.cycle(v), face.cycle(v+1));
			if( edgenormals.find(edge) == edgenormals.end())
				edgenormals[edge] = Math::Vec3f(0, 0, 0);
			edgenormals[edge] += norm;
		}
	}
	std::map< Math::Edge32, Math::Vec3f>::iterator it = edgenormals.begin();
	for( ; it != edgenormals.end(); it++)
	{
		it->second.normalize();
	}
}

// ������ ������� ��������
Math::Vec3f controlMeshCache::calcVertNormal(
	int vert, 
	const std::vector<Math::Vec3f>& facenormals	// ������� ��-���
	)const
{
	const basevertex& bv = this->baseverts[vert];
	Math::Vec3f vn = Math::Vec3f(0, 0, 0);
	for(unsigned fi=0; fi<bv.normals.size(); fi++)
	{
		int fni = bv.normals[fi];
		Math::Vec3f n = facenormals[fni];
		vn += n;
	}
	vn.normalize();
	return vn;
}
// ������ TangentU ��������
Math::Vec3f controlMeshCache::calcVertTangentU(
	int vert, 
	int opositevert, 
	const std::vector<Math::Vec3f>& verts // ��������
	)const
{
//	const basevertex& bv = this->baseverts[vert];
	Math::Vec3f tangentU = verts[opositevert] - verts[vert];
	return tangentU;
}

// ������ �������� ���������
void controlMeshCache::calcVertNormals(
	const std::vector<Math::Vec3f>& facenormals,	// ������� ��-���
	std::vector<Math::Vec3f>& vertnormals		// ������� �����
	)const
{
	vertnormals.resize(this->baseverts.size());
	for(unsigned v=0; v<this->baseverts.size(); v++)
	{
		const basevertex& bv = this->baseverts[v];
		Math::Vec3f& vn = vertnormals[v];
		vn = Math::Vec3f(0, 0, 0);
		for(unsigned fi=0; fi<bv.normals.size(); fi++)
		{
			int fni = bv.normals[fi];
			Math::Vec3f n = facenormals[fni];
			vn += n;
		}
		/*/
		for(unsigned fi=0; fi<bv.faces.size(); fi++)
		{
			int f = bv.faces[fi];
			int fn = this->facesnormalindex[f];
			Math::Vec3f n = facenormals[fn];
			vn += n;
		}
		/*/
		vn.normalize();
	}
}

// ������ �������� � �������� ������
void controlMeshCache::calcFaceNormals(
	const std::vector<Math::Vec3f>& verts,		// ������� ���������
	std::vector<Math::Vec3f>& facenormals,	// ������� ��-���
	std::vector< float>& squarefaces		// ������� �����
	)const
{
	squarefaces.resize(faces.size());
	facenormals.resize( this->faceNormalCount, Math::Vec3f(0, 0, 0));
	for( unsigned f=0; f<this->faces.size(); f++)
	{
		const Math::Face32& face = this->faces[f];

		Math::Vec3f v1 = verts[face[1]] - verts[face[0]];
		Math::Vec3f v2 = verts[face[2]] - verts[face[0]];
		Math::Vec3f n = Math::cross(v1, v2);
		squarefaces[f] = n.length();
		int fni = this->facesnormalindex[f];
		facenormals[fni] += n.normalized();
	}
	for( unsigned fn=0; fn<facenormals.size(); fn++)
		facenormals[fn].normalize();
}

// ������ �������� � �������� ������
void controlMeshCache::calcFaceNormals(
	const std::vector<Math::Vec3f>& verts,		// ������� ���������
	std::vector<Math::Vec3f>& facenormals	// ������� ��-���
	)const
{
	facenormals.resize( this->faceNormalCount, Math::Vec3f(0, 0, 0));
	for( unsigned f=0; f<this->faces.size(); f++)
	{
		const Math::Face32& face = this->faces[f];

		Math::Vec3f v1 = verts[face[1]] - verts[face[0]];
		Math::Vec3f v2 = verts[face[2]] - verts[face[0]];
		Math::Vec3f n = Math::cross(v1, v2);
		int fni = this->facesnormalindex[f];
		facenormals[fni] += n.normalized();
	}
	for( unsigned fn=0; fn<facenormals.size(); fn++)
		facenormals[fn].normalize();
}

// ������ ������� ���������
void controlMeshCache::calcScale(
	float commonscale, 
	const std::map<int, float>& vertscale, 
	std::vector<float>& scales)
{
	unsigned v;

	scales.resize(baseverts.size());

	// ������� ���������
	{
		for(v=0; v<scales.size(); v++)
		{
			// p
			float S = 0;
			const Math::subdivWeight& sdw = this->neoverts[v];
			for( unsigned z=0; z<sdw.data.size(); z++)
			{
				int vi = sdw.data[z].first;
				std::map<int, float>::const_iterator it = vertscale.find(vi);
				float s = commonscale;
				if( it!=vertscale.end())
					s = it->second;

				S += s*sdw.data[z].second;
			}
			scales[v] = S;
		}
	}
}







controlMeshCache::controlMeshCache()
{
	bValid = false;
	timeStamp = 0;
}
controlMeshCache::~controlMeshCache()
{
}

MTypeId controlMeshCache::typeId() const
{
	return controlMeshCache::id;
}

MString controlMeshCache::name() const
{ 
	return controlMeshCache::typeName; 
}

void* controlMeshCache::creator()
{
	return new controlMeshCache();
}

void controlMeshCache::copy( const MPxData& other )
{
	const controlMeshCache* arg = (const controlMeshCache*)&other;
	this->bValid = arg->bValid;
	this->timeStamp = arg->timeStamp;

	this->poligoncount = arg->poligoncount;
	this->vertexcount = arg->vertexcount;
	this->facevertexcount = arg->facevertexcount;
	this->subdivstep = arg->subdivstep;
	this->bUseUVspace = arg->bUseUVspace;
	this->UVspaceName = arg->UVspaceName;

	this->faces				= arg->faces;
	this->neoverts			= arg->neoverts;
	this->facesnormalindex	= arg->facesnormalindex;
	this->faceNormalCount	= arg->faceNormalCount;

	this->src_verts			= arg->src_verts;		
	this->src_vertnormals	= arg->src_vertnormals;
	this->src_facenormals	= arg->src_facenormals;
	this->src_edges			= arg->src_edges;
//	this->src_squarefaces	= arg->src_squarefaces;
	this->src_edgenormals	= arg->src_edgenormals;

	this->baseverts			= arg->baseverts;

	this->openedges			= arg->openedges;
}

MStatus controlMeshCache::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("controlMeshCache: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus controlMeshCache::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus controlMeshCache::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus controlMeshCache::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void controlMeshCache::serialize(Util::Stream& stream)
{
displayStringD("controlMeshCache::serialize");
	int version = 4;
	stream >> version;
	if( version==4)
	{
		stream >> this->poligoncount;
		stream >> this->vertexcount;
		stream >> this->facevertexcount;
		stream >> this->subdivstep;

		stream >> this->baseverts;
		stream >> this->faces;				
		stream >> this->faceNormalCount;	
		stream >> this->facesnormalindex;	

		stream >> this->openedges;
		stream >> this->neoverts;

		stream >> this->src_verts;
		stream >> this->src_vertnormals;
		stream >> this->src_facenormals;
		stream >> this->src_edges;
//		stream >> this->src_squarefaces;

		stream >> this->timeStamp;

		bValid = true;
	}
}

