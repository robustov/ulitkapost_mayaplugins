#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include <maya/MItGeometry.h>

#include "Util/Stream.h"
#include "Math/Math.h"
#include "Math/Rotation3.h"
#include "controlMeshCache.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", targetBinding::id );
	}
/*/

/*/
// target ����� 
struct pointToBasePoint
{
	int basevertex;
	float weight;
	pointToBasePoint(int basevertex=0, float weight=0){this->basevertex=basevertex; this->weight=weight;}
};
/*/

// ������ � �������� ��������
struct pointInfluence
{
	// �������� ��������� ����� (object space)
	Math::Vec3f source_pt;
	// ����� ����� ���������� - ��� viewBinding
	Math::Vec3f deformed_pt;

	// �������� � �����������
	std::vector<pointOnControlMesh> projectionlist;
	int selectedProjection;

	pointInfluence(){};
	void reset()
	{
		selectedProjection = 0;
		projectionlist.clear();
	}
	const pointOnControlMesh& getProjection(int ipr=-1) const
	{
		if( ipr<0) ipr = selectedProjection;
		ipr = ipr%projectionlist.size();
		if(ipr<0 && ipr>=(int)projectionlist.size())
		{
			if(projectionlist.empty())
				return pointOnControlMesh::zero;
			return projectionlist[0];
		}
		return projectionlist[ipr];
	}
};

// ������ � �������� ���������
struct geometryInfluence
{
	// cacheTimeStamp
	int cacheTimeStamp;
	// �������� ������� world space
	Math::Matrix4f worldMatrix;
	// ����� index() -> pointInfluence
	std::map<int, pointInfluence> influences;

public:
	geometryInfluence();
	bool UpdateBinding( 
		MItGeometry& iter, 
		MMatrix& targetToControl, 
		controlMeshCache* cache, 
		bool bInteractive, 
		bool bUseUVspace, 
		const char* UVspaceName, 
		int projectionCount);

	Math::Vec3f deformPoint( 
		controlMeshCache* controlmesh, 
		controlMeshDeformation* deformation, 
		int index, 
		Math::Vec3f pt, 
		float envelope
		);

	Math::Vec3f deformPoint2( 
		controlMeshCache* controlmesh, 
		controlMeshDeformation* deformation, 
		int index, 
		Math::Vec3f pt, 
		float commonOffset,
		float envelope
		);
	Math::Vec3f deformPoint3( 
		controlMeshCache* controlmesh, 
		controlMeshDeformation* deformation, 
		int index, 
		Math::Vec3f pt,
		float commonOffset,
		float envelope,
		float radius
		);

	void drawBinding(
		controlMeshCache* controlmesh, 
		controlMeshDeformation* deformation,
		std::vector<int>& viewverts
		);

	void switchBinding(
		controlMeshCache* controlmesh, 
		controlMeshDeformation* deformation,
		std::vector<int>& viewverts
		);
	void dumpBinding(
		controlMeshCache* controlmesh, 
		controlMeshDeformation* deformation,
		std::vector<int>& viewverts
		);

protected:
	void getPointDeformation(
		controlMeshCache* controlmesh, 
		controlMeshDeformation* deformation, 
		Math::Vec3f& project,				// �������� �����
		Math::Vec3f& projectbase,			// �������� �����
		Math::Vec3f& offset,				// ��������
		Math::Rot3f& rotate,				// ��������
		float& scale,						// �����
//		float& deformfactor,				// ���. �������� ����������
		const pointInfluence& pi,			// ��������
		int ipr=-1							// ������ ��������
		) const;

	void drawVertexBinding(
		controlMeshCache* controlmesh, 
		controlMeshDeformation* deformation,
		pointInfluence& pi, 
		int ipr);

};

class controlMeshCache;
class controlMeshDeformation;

class targetBinding : public MPxData
{
public:
	bool bValid;

	// multiIndex -> geometryInfluence
	std::map<int, geometryInfluence> gis;


public:
	targetBinding();
	virtual ~targetBinding(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;

protected:
	void serialize(Util::Stream& stream);
};

/*/
template<class Stream> inline
Stream& operator >> (Stream& out, pointToBasePoint& v)
{
	out >> v.basevertex;	// �����
	out >> v.weight;		// �������
	return out;
}
/*/
template<class Stream> inline
Stream& operator >> (Stream& out, pointInfluence& v)
{
	out >> v.source_pt;
	out >> v.projectionlist;
	out >> v.selectedProjection;
	out >> v.deformed_pt;
	return out;
}
template<class Stream> inline
Stream& operator >> (Stream& out, geometryInfluence& v)
{
	out >> v.worldMatrix >> v.influences;
	out >> v.cacheTimeStamp;
	return out;
}
