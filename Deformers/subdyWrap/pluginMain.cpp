#include "pre.h"
#include <maya/MFnPlugin.h>
#include "subdyWrap.h"
#include "controlMeshCache.h"
#include "controlMeshDeformation.h"
#include "targetBinding.h"
#include "subdyWrapCmd.h"

// ������� ��� �������� ��������
std::map<std::string, std::string> defines;

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
// ������� ��� �������� ��������
#ifdef FREE
	defines["#PRO#"] = "0";
	MFnPlugin plugin( obj, "D.Robustov", "0.0.1 free", "Any");
#else
	defines["#PRO#"] = "1";
	MFnPlugin plugin( obj, "D.Robustov", "0.0.1", "Any");
#endif

#ifdef SAS
	defines["#ULITKA#"] = "0";
#else
	defines["#ULITKA#"] = "1";
#endif


// #include "subdyWrapCmd.h"
	stat = plugin.registerCommand( 
		subdyWrapCmd::typeName.asChar(), 
		subdyWrapCmd::creator, 
		subdyWrapCmd::newSyntax );
	if (!stat) 
	{
		displayString("cant register %s", subdyWrapCmd::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

// #include "targetBinding.h"
	stat = plugin.registerData( 
		targetBinding::typeName, 
		targetBinding::id, 
		targetBinding::creator);
	if (!stat) 
	{
		displayString("cant register %s", targetBinding::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "controlMeshDeformation.h"
	stat = plugin.registerData( 
		controlMeshDeformation::typeName, 
		controlMeshDeformation::id, 
		controlMeshDeformation::creator);
	if (!stat) 
	{
		displayString("cant register %s", controlMeshDeformation::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "controlMeshCache.h"
	stat = plugin.registerData( 
		controlMeshCache::typeName, 
		controlMeshCache::id, 
		controlMeshCache::creator);
	if (!stat) 
	{
		displayString("cant register %s", controlMeshCache::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "subdyWrap.h"
	stat = plugin.registerNode( 
		subdyWrap::typeName, 
		subdyWrap::id, 
		subdyWrap::creator, 
		subdyWrap::initialize, 
		MPxNode::kDeformerNode );
	if (!stat) 
	{
		displayString("cant register %s", subdyWrap::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}


	if( !SourceMelFromResource(MhInstPlugin, "subdyWrapMenu", "MEL", defines))
	{
		displayString("error source subdyWrapMenu.mel");
	}
	else
	{
#ifdef _DEBUG
		if( !MGlobal::executeCommand("subdyWrapDebugMenu()"))
		{
			displayString("Dont found subdyWrapDebugMenu() command");
		}
		MGlobal::executeCommand("subdyWrap_RegistryPlugin()");
#endif

#ifdef FREE
		if( !MGlobal::executeCommand("subdyWrap_CreateMenu()"))
		{
			displayString("Dont found subdyWrap_CreateMenu() command");
		}
#else
		if( !MGlobal::executeCommand("subdyWrap_RegistryPlugin()"))
		{
			displayString("Dont found subdyWrap_RegistryPlugin() command");
		}
#endif

	}
	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
#ifdef FREE
		if( !MGlobal::executeCommand("subdyWrap_DeleteMenu()"))
		{
			displayString("Dont found subdyWrap_DeleteMenu() command");
		}
#endif

	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterCommand( "subdyWrapCmd" );
	if (!stat) displayString("cant deregister %s", subdyWrapCmd::typeName.asChar());

	stat = plugin.deregisterData( targetBinding::id );
	if (!stat) displayString("cant deregister %s", targetBinding::typeName.asChar());

	stat = plugin.deregisterData( controlMeshDeformation::id );
	if (!stat) displayString("cant deregister %s", controlMeshDeformation::typeName.asChar());

	stat = plugin.deregisterData( controlMeshCache::id );
	if (!stat) displayString("cant deregister %s", controlMeshCache::typeName.asChar());

	stat = plugin.deregisterNode( subdyWrap::id );
	if (!stat) displayString("cant deregister %s", subdyWrap::typeName.asChar());

	MGlobal::executeCommand("subdyWrapDebugMenuDelete()");
	return stat;
}
