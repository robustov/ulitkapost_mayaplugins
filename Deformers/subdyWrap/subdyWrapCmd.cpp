//
// Copyright (C) 
// File: subdyWrapCmdCmd.cpp
// MEL Command: subdyWrapCmd

#include "subdyWrapCmd.h"
#include "subdyWrap.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/M3dView.h>
#include <maya/MDagPath.h>
#include <maya/MFnSingleIndexedComponent.h>
#include <maya/MIntArray.h>

subdyWrapCmd::subdyWrapCmd()
{
}
subdyWrapCmd::~subdyWrapCmd()
{
}

MSyntax subdyWrapCmd::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addFlag("c", "command", MSyntax::kString);	// command
	syntax.addFlag("o", "object", MSyntax::kString);	// wrap node
	syntax.addFlag("a", "address", MSyntax::kString);	// for "browse"

	return syntax;
}

#ifdef UNICODE
    typedef HINSTANCE (WINAPI* LPShellExecute)(HWND hwnd, LPCWSTR lpOperation, LPCWSTR lpFile, LPCWSTR lpParameters, LPCWSTR lpDirectory, INT nShowCmd);
#else
    typedef HINSTANCE (WINAPI* LPShellExecute)(HWND hwnd, LPCSTR lpOperation, LPCSTR lpFile, LPCSTR lpParameters, LPCSTR lpDirectory, INT nShowCmd);
#endif

MStatus subdyWrapCmd::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString cmd;
	if(!argData.getFlagArgument("c", 0, cmd))
		return MS::kFailure;

	if(cmd=="browse")
	{
		MString address;
		if(!argData.getFlagArgument("a", 0, address))
			return MS::kFailure;

		HINSTANCE hInstShell32 = LoadLibrary(TEXT("shell32.dll"));
		if (hInstShell32 != NULL)
		{
			bool bSuccess = false;
			LPShellExecute pShellExecute = NULL;
	#ifdef UNICODE
			pShellExecute = (LPShellExecute)GetProcAddress(hInstShell32, _TWINCE("ShellExecuteW"));
	#else
			pShellExecute = (LPShellExecute)GetProcAddress(hInstShell32, "ShellExecuteA");
	#endif
			if( pShellExecute != NULL )
			{
				std::string url = address.asChar();
				if( pShellExecute( NULL, TEXT("open"), url.c_str(), NULL, NULL, SW_SHOW ) > (HINSTANCE) 32 )
					bSuccess = true;
			}
			FreeLibrary(hInstShell32);
		}
		return MS::kSuccess;
	}


	MString argsl0;
	if(!argData.getFlagArgument("o", 0, argsl0))
		return MS::kFailure;

	MObject obj;
	nodeFromName(argsl0, obj);
	if( obj.isNull())
	{
		MPlug plug;
		plugFromName(argsl0, plug);
		obj = plug.node();
	}
	if( obj.isNull())
		return MS::kFailure;

	if(cmd=="rebind")
	{
		MPlug plug(obj, subdyWrap::i_controlMeshCache);
		plug.setValue(MObject::kNullObj);
		return MS::kSuccess;
	}
	if(cmd=="viewdeformation")
	{
		MFnDependencyNode dn(obj);
		subdyWrap* wrap = (subdyWrap*)dn.userNode();
		M3dView view = M3dView::active3dView();
		wrap->DrawDeformation(view);
		return MS::kSuccess;
	}
	if(cmd=="viewbinding" || cmd=="dumpbinding")
	{
		MFnDependencyNode dn(obj);
		if( dn.typeId() != subdyWrap::id)
		{
			displayString("NODE %s not subdywrap!!!", argsl0.asChar());
			return MS::kFailure;
		}

		MSelectionList list;
		MGlobal::getActiveSelectionList(list);

		std::vector<int> viewverts;
		for( int i=0; i<(int)list.length(); i++)
		{
			MDagPath dagPath;
			MObject component;
			list.getDagPath( i, dagPath, component);
			if( component.isNull()) continue;

			MFnSingleIndexedComponent single(component);
			if( component.apiType() != MFn::kMeshVertComponent)
				continue;
			MIntArray elements;
			single.getElements( elements );
			viewverts.resize( elements.length());
			for(int e=0; e<(int)viewverts.size(); e++)
				viewverts[e] = elements[e];
			break;
		}

		subdyWrap* wrap = (subdyWrap*)dn.userNode();
		M3dView view = M3dView::active3dView();
		if(cmd=="viewbinding")
			wrap->DrawBinding(view, viewverts);
		if(cmd=="dumpbinding")
			wrap->DumpBinding(viewverts);
		return MS::kSuccess;
	}
	if(cmd=="switchbinding")
	{
		MSelectionList list;
		MGlobal::getActiveSelectionList(list);

		MObject target = MObject::kNullObj;
		std::vector<int> viewverts;
		for( int i=0; i<(int)list.length(); i++)
		{
			MDagPath dagPath;
			MObject component;
			list.getDagPath( i, dagPath, component);
			if( component.isNull()) continue;

			MFnSingleIndexedComponent single(component);
			if( component.apiType() != MFn::kMeshVertComponent)
				continue;
			MIntArray elements;
			single.getElements( elements );
			viewverts.resize( elements.length());
			for(int e=0; e<(int)viewverts.size(); e++)
				viewverts[e] = elements[e];
			break;
		}
		MFnDependencyNode dn(obj);
		subdyWrap* wrap = (subdyWrap*)dn.userNode();
		M3dView view = M3dView::active3dView();
		wrap->SwitchBinding(target, viewverts);
		return MS::kSuccess;

	}

	return MS::kSuccess;
}

void* subdyWrapCmd::creator()
{
	return new subdyWrapCmd();
}

