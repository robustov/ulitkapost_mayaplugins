#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"
#include "Math/Subdiv.h"
#include "Math/Math.h"
#include "Math/Edge.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", controlMeshCache::id );
	}
/*/

// ��� ����������� �����
struct basevertex
{
//	std::vector<int> faces;		// ��������� �����
	std::vector<int> normals;		// ��������� �������
	std::vector<int> basisedgevert;	// ������ ��������� ����� ������� ������������ ��� ������� �������������
//	basevertex(){faces.reserve(4);}
	basevertex(){normals.reserve(4);basisedgevert.reserve(4);}
};

// �������� �����
struct openedge
{
	int v_1;	// ����. �������� �����
	Math::Edge32 edge;
	int v2_;	// ����. �������� �����
	openedge(){v_1=0;v2_=0;}
};

// �������� ����� �� ����������� �����
struct pointOnControlMesh
{
	float distance;		// ����������
	int face;			// ������ ����� (-1 ��� �����)
	int v[3];			// ������� ���������
	float w[3];			// ��� ���������

	pointOnControlMesh()
	{
		distance = std::numeric_limits<float>::max();
		face = -1;
		v[0] = v[1] = v[2] = 0;
		w[0] = w[1] = w[2] = 0;
	}
	// �������� �� ��-�
	pointOnControlMesh(
		float distance, 
		int face, 
		Math::Face32 f, 
		Math::Vec3f bary
		)
	{
		this->distance = distance;
		this->face = face;
		v[0] = f[0]; v[1] = f[1]; v[2] = f[2];
		w[0] = bary[0]; w[1] = bary[1]; w[2] = bary[2];
	}
	// �������� �� �����
	pointOnControlMesh(
		float distance, 
		int v0, int v1,
		float w0, float w1
		)
	{
		this->distance = distance;
		this->face = -1;
		v[0] = v0; v[1] = v1; v[2] = v1;
		w[0] = w0; w[1] = w1; w[2] = 0;
	}
	static pointOnControlMesh zero;
};

class controlMeshDeformation;
class MFnMesh;

class controlMeshCache : public MPxData
{
	friend class controlMeshDeformation;
// ������� � InitMesh
public:
	bool bValid;
	// ��� �������� � targetBinding
	int timeStamp;
	// ��� �������� ����������:
	int poligoncount, vertexcount, facevertexcount, subdivstep;
	bool bUseUVspace;
	std::string UVspaceName;

	// �����
	std::vector< Math::Face32> faces;
	
	// ����� �������� (����� ���������)
	std::vector< Math::subdivWeight> neoverts;

	// ������ ������� ����� (����� ����� ��� ��������)
	std::vector< int> facesnormalindex;
	// ����� �������� ��������
	int faceNormalCount;

	// ��� ��������� � ��������
	std::vector<Math::Vec3f> src_verts;			// ������� ���������
	std::vector<Math::Vec3f> src_vertnormals;	// ������� ���������
	std::vector<Math::Vec3f> src_facenormals;	// ������� ��-���
	std::vector<Math::Edge32> src_edges;		// ��� ������� �����
//	std::vector< float>		 src_squarefaces;	// ������� ������
	std::map< Math::Edge32, Math::Vec3f> src_edgenormals;	// ������� �����

	// �������� ��������� � ������
	std::vector<basevertex> baseverts;

	// �������� �����
	std::vector< openedge> openedges;

public:
	bool InitMesh(
		MObject meshobj, 
		int subdivstep,
		bool bBaseInteractive, 
		bool bUseUVspace, 
		const char* UVspaceName);

public:
	// �������� �����
	void projectionPoint(
		Math::Vec3f pt,			// world space
		std::vector<pointOnControlMesh>& projectionlist, 
		int maxProjectionCount
		);// const;
	const Math::Vec3f& getVert(int v) const
	{
		return src_verts[v];
	}

protected:
	// ���������� �������� �� ������ �����
	void calcVerts(
		MFnMesh& mesh, 
		std::vector<Math::Vec3f>& verts
		)const;

	// ������ �������� � �������� ������
	void calcFaceNormals(
		const std::vector<Math::Vec3f>& verts,		// ������� ���������
		std::vector<Math::Vec3f>& facenormals,	// ������� ��-���
		std::vector< float>& squarefaces		// ������� �����
		)const;
	// ������ �������� ������
	void calcFaceNormals(
		const std::vector<Math::Vec3f>& verts,		// ������� ���������
		std::vector<Math::Vec3f>& facenormals	// ������� ��-���
		)const;
	// ������ �������� ���������
	void calcVertNormals(
		const std::vector<Math::Vec3f>& facenormals,	// ������� ��-���
		std::vector<Math::Vec3f>& vertnormals	// ������� ���������
		)const;
	// ������ �������� �����	
	void calcEdgeNormals(
		const std::vector<Math::Vec3f>& facenormals,	// ������� ��-���
		std::map< Math::Edge32, Math::Vec3f>& src_edgenormals	// ������� �����
		)const;

	// ������ ������� ���������
	void calcScale(
		float commonscale, 
		const std::map<int, float>& vertscale, 
		std::vector<float>& scales);

	// ������ ������� ��������
	Math::Vec3f calcVertNormal(
		int vert, 
		const std::vector<Math::Vec3f>& facenormals	// ������� ��-���
		)const;
	// ������ TangentU ��������
	Math::Vec3f calcVertTangentU(
		int vert, 
		int opositevert, 
		const std::vector<Math::Vec3f>& verts // ��������
		)const;

public:
	controlMeshCache();
	virtual ~controlMeshCache(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;

protected:
	void serialize(Util::Stream& stream);
};

template<class Stream> inline
Stream& operator >> (Stream& out, basevertex& v)
{
//	out >> v.faces;			// ��������� �����
	out >> v.normals;		// ��������� �������
	out >> v.basisedgevert;
	return out;
}
template<class Stream> inline
Stream& operator >> (Stream& out, openedge& v)
{
	out >> v.v_1 >> v.edge >> v.v2_;
	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, pointOnControlMesh& v)
{
	out >> v.distance;		// ����������
	out >> v.face;			// ������ ����� (-1 ��� �����)
	out >> v.v[0] >> v.v[1] >> v.v[2];			// ������� ���������
	out >> v.w[0] >> v.w[1] >> v.w[2];			// ��� ���������
	return out;
}
