#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: subdyWrapCmdCmd.h
// MEL Command: subdyWrapCmd

#include <maya/MPxCommand.h>

class subdyWrapCmd : public MPxCommand
{
public:
	static const MString typeName;
	subdyWrapCmd();
	virtual	~subdyWrapCmd();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

