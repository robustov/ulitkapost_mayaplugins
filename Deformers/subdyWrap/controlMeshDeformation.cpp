//
// Copyright (C) 
// File: controlMeshDeformationCmd.cpp
// MEL Command: controlMeshDeformation

#include "controlMeshDeformation.h"

#include <maya/MGlobal.h>
#include <maya/MFnMesh.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <sstream>
#include "controlMeshCache.h"
#include <mathNgl/mathNgl.h>

bool controlMeshDeformation::buildDeformations(
	MObject meshobj, 
	Math::Matrix4f controlToWorld, 
	Math::Matrix4f controlBaseToWorld, 
	controlMeshCache* cache, 
	float scale,						// ����� scale
	std::map<int, float>& vertscale		// ������������ �����
	)
{
	displayStringD( "controlMeshDeformation::buildDeformations");

	bValid = false;
	cacheTimeStamp = cache->timeStamp;

	this->controlToWorld = controlToWorld;
	this->controlBaseToWorld = controlBaseToWorld;

	unsigned v;

	MFnMesh mesh(meshobj);
	std::vector<Math::Vec3f> verts;
	cache->calcVerts(mesh, verts);
	// ��������� controlToBase
	for(v=0; v<verts.size(); v++)
	{
		Math::Vec3f& pt = verts[v];
		pt = controlToWorld*pt;
	}

	// scale
	std::vector<float> scales;
	cache->calcScale(scale, vertscale, scales);

	// ������� � ������� ������ 
	std::vector<Math::Vec3f> norms;
	cache->calcFaceNormals( verts, norms);
	/*/
	std::vector<float> deformfactor;
	cache->calcFaceNormals( verts, norms, deformfactor);
	for(unsigned f=0; f<deformfactor.size(); f++)
	{
		float sq = deformfactor[f];
		// �����!!! cache->
		deformfactor[f] = sqrt( sq/cache->src_squarefaces[f]);
	}
	/*/

	basedeform.resize(verts.size());
	// ������� ���������
	{
		Math::Matrix4As3f controlBaseToWorld3f(controlBaseToWorld);
		for(v=0; v<basedeform.size(); v++)
		{
			// �����!!! cache->
			basevertexdeform& bd = basedeform[v];
			// ��������
			Math::Vec3f src_V = controlBaseToWorld*cache->src_verts[v];
			bd.offset = verts[v] - src_V;

			basevertex& bv = cache->baseverts[v];

			bd.normal = Math::Vec3f(0.f);
			bd.transform = Math::Matrix4f(0.f);
			for(int t=0; t<(int)bv.basisedgevert.size(); t++)
			{
				int opositevert = bv.basisedgevert[t];

				Math::Vec3f N = cache->calcVertNormal( v, norms);
				// �����
				Math::Matrix3f Basis, oldBasis;
				Math::Matrix4f transform;
				calcVertexDeformation(
					verts[v], 
					src_V, 
					N, 
					controlBaseToWorld3f*cache->src_vertnormals[v], 
					cache->calcVertTangentU( v, opositevert, verts), 
					controlBaseToWorld3f*cache->calcVertTangentU( v, opositevert, cache->src_verts),
					Basis, 
					oldBasis,
					transform);
				bd.transform[0] += transform[0];
				bd.transform[1] += transform[1];
				bd.transform[2] += transform[2];
				bd.transform[3] += transform[3];

				// �������
				Math::Rot3f rotate(oldBasis[NORMAL], Basis[NORMAL]);
				bd.rotate = rotate;
				bd.normal += N;
			}
			if( !bv.basisedgevert.empty())
			{
				float factor = 1.f/bv.basisedgevert.size();
				bd.transform.scale( Math::Vec4f( factor));
				bd.normal *= factor;
			}
			bd.normal.normalize();


			/*/
			bd.transform.resize( bv.basisedgevert.size());
			for(int t=0; t<(int)bv.basisedgevert.size(); t++)
			{
				int opositevert = bv.basisedgevert[t];

				// �����
				Math::Matrix3f Basis, oldBasis;
				Math::Matrix4f transform;
				calcVertexDeformation(
					verts[v], 
					src_V, 
					cache->calcVertNormal( v, norms), 
					controlBaseToWorld3f*cache->src_vertnormals[v], 
					cache->calcVertTangentU( v, opositevert, verts), 
					controlBaseToWorld3f*cache->calcVertTangentU( v, opositevert, cache->src_verts),
					Basis, 
					oldBasis,
					transform);
				bd.transform[t] = transform;

				// �������
				Math::Rot3f rotate(oldBasis[NORMAL], Basis[NORMAL]);
				bd.rotate = rotate;
			}
			/*/

			// �����
			bd.scale = scales[v];
		}
	}
	bValid = true;

	return bValid;
}
// �������� �����
void controlMeshDeformation::drawDeformation(
	MObject meshobj, 
	Math::Matrix4f controlToWorld, 
	Math::Matrix4f controlBaseToWorld, 
	controlMeshCache* cache,
	bool drawMesh,
	bool drawNormals
	)
{
	float normallen = 0.1f;
	float scale = 1;
	std::map<int, float> vertscale;

	MFnMesh mesh(meshobj);
	std::vector<Math::Vec3f> verts;
	cache->calcVerts(mesh, verts);
	// ��������� controlToBase
	unsigned v;
	for(v=0; v<verts.size(); v++)
	{
		Math::Vec3f& pt = verts[v];
		pt = controlToWorld*pt;
	}

	// scale
	std::vector<float> scales;
	cache->calcScale(scale, vertscale, scales);

	// ������� � ������� ������ 
	std::vector<Math::Vec3f> norms;
	cache->calcFaceNormals( verts, norms);
	/*/
	std::vector<float> deformfactor;
	cache->calcFaceNormals( verts, norms, deformfactor);
	for(unsigned f=0; f<deformfactor.size(); f++)
	{
		float sq = deformfactor[f];
		// �����!!! cache->
		deformfactor[f] = sqrt( sq/cache->src_squarefaces[f]);
	}
	/*/

//	basedeform.resize(verts.size());
	// ������� ���������
	{
		Math::Matrix4As3f controlBaseToWorld3f(controlBaseToWorld);

		for(v=0; v<verts.size(); v++)
		{
			Math::Vec3f src_V = controlBaseToWorld*cache->src_verts[v];
			Math::Vec3f offset = verts[v] - src_V;

			// ��������
			glLineStipple(1, 0x8888);
			glEnable(GL_LINE_STIPPLE);
			glColor3f(1, 1, 1);
			glBegin( GL_LINES );
			glVertex3f( verts[v]);
			glVertex3f( src_V);
			glEnd();
			glDisable(GL_LINE_STIPPLE);

			basevertex& bv = cache->baseverts[v];
			if( bv.basisedgevert.empty()) continue;
			int opositevert = bv.basisedgevert[0];

			// �����
			Math::Matrix3f Basis, oldBasis;
			Math::Matrix4f transform;
			calcVertexDeformation(
				verts[v], 
				src_V, 
				cache->calcVertNormal( v, norms), 
				controlBaseToWorld3f*cache->src_vertnormals[v], 
				cache->calcVertTangentU( v, opositevert, verts), 
				controlBaseToWorld3f*cache->calcVertTangentU( v, opositevert, cache->src_verts),
				Basis, 
				oldBasis,
				transform);

			glColor3f(1, 1, 1);
			glBegin( GL_LINES );
			glVertex3f( verts[v]);
			glVertex3f( verts[v]+Basis[NORMAL]*normallen);
			glVertex3f( verts[v]);
			glVertex3f( verts[v]+Basis[TANGENTU]*normallen);
			glVertex3f( verts[v]);
			glVertex3f( verts[v]+Basis[TANGENTV]*normallen);
			glEnd();

			glColor3f(1, 0, 0);
			glBegin( GL_LINES );
			glVertex3f( src_V);
			glVertex3f( src_V+oldBasis[NORMAL]*normallen);
			glVertex3f( src_V);
			glVertex3f( src_V+oldBasis[TANGENTU]*normallen);
			glVertex3f( src_V);
			glVertex3f( src_V+oldBasis[TANGENTV]*normallen);
			glEnd();

			// ������� ����� 
			Math::Vec3f srctest = src_V+oldBasis[TANGENTU]*normallen;
			Math::Vec3f test = transform*srctest;
//			Math::Vec3f test;
//			test = transform1*srctest;
//			test = transform2*test;
			glColor3f(0, 0, 1);
			glBegin( GL_LINES );
			glVertex3f( test);
			glVertex3f( srctest);
			glEnd();
			
		}
	}
}

void controlMeshDeformation::drawMesh(
	MObject meshobj, 
	Math::Matrix4f controlToWorld, 
	controlMeshCache* cache
	)
{
	MFnMesh mesh(meshobj);
	std::vector<Math::Vec3f> verts;
	cache->calcVerts(mesh, verts);
	// ��������� controlToBase
	unsigned v;
	for(v=0; v<verts.size(); v++)
	{
		Math::Vec3f& pt = verts[v];
		pt = controlToWorld*pt;
	}

	glColor3f(1, 1, 1);
	glBegin( GL_LINES );
	for(int e=0; e<(int)cache->src_edges.size(); e++)
	{
		Math::Edge32& edge = cache->src_edges[e];
		glVertex3f( verts[edge.v1]);
		glVertex3f( verts[edge.v2]);
	}
	glEnd();

}
void controlMeshDeformation::calcVertexDeformation(
	const Math::Vec3f& V, 
	const Math::Vec3f& srcV, 
	const Math::Vec3f& N, 
	const Math::Vec3f& srcN, 
	const Math::Vec3f& tangentU, 
	const Math::Vec3f& srcTangentU, 
	Math::Matrix3f& Basis, 
	Math::Matrix3f& oldBasis,
	Math::Matrix4f& transform 
	)
{
	// �����
	Basis[NORMAL] = N;
	Basis[TANGENTU] = tangentU;
	Basis[TANGENTV] = Math::cross( Basis[TANGENTU], Basis[NORMAL]);
	Basis[TANGENTU] = Math::cross( Basis[TANGENTV], Basis[NORMAL]);
	Basis[TANGENTU].normalize();
	Basis[TANGENTV].normalize();

	// ������ �����
	oldBasis[NORMAL] = srcN;
	oldBasis[TANGENTU] = srcTangentU;
	oldBasis[TANGENTV] = Math::cross( oldBasis[TANGENTU], oldBasis[NORMAL]);
	oldBasis[TANGENTU] = Math::cross( oldBasis[TANGENTV], oldBasis[NORMAL]);
	oldBasis[NORMAL].normalize();
	oldBasis[TANGENTU].normalize();
	oldBasis[TANGENTV].normalize();

	// transform ������� �� ������� ������ � �����
	Math::Matrix4f transform1;
	transform1[0] = Math::Vec4f(oldBasis[0], 0);
	transform1[1] = Math::Vec4f(oldBasis[1], 0);
	transform1[2] = Math::Vec4f(oldBasis[2], 0);
	transform1[3] = Math::Vec4f(srcV, 1);
	transform1.invert();
	Math::Matrix4f transform2;
	transform2[0] = Math::Vec4f(Basis[0], 0);
	transform2[1] = Math::Vec4f(Basis[1], 0);
	transform2[2] = Math::Vec4f(Basis[2], 0);
	transform2[3] = Math::Vec4f(V, 1);
	transform = transform1*transform2;
}


controlMeshDeformation::controlMeshDeformation()
{
	bValid = false;
	cacheTimeStamp = -1;
}
controlMeshDeformation::~controlMeshDeformation()
{
}

MTypeId controlMeshDeformation::typeId() const
{
	return controlMeshDeformation::id;
}

MString controlMeshDeformation::name() const
{ 
	return controlMeshDeformation::typeName; 
}

void* controlMeshDeformation::creator()
{
	return new controlMeshDeformation();
}

void controlMeshDeformation::copy( const MPxData& other )
{
	const controlMeshDeformation* arg = (const controlMeshDeformation*)&other;
	this->bValid = arg->bValid;
	this->cacheTimeStamp = arg->cacheTimeStamp;
	this->basedeform = arg->basedeform;
	this->controlToWorld = arg->controlToWorld;
	this->controlBaseToWorld = arg->controlBaseToWorld;
}

MStatus controlMeshDeformation::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("controlMeshDeformation: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus controlMeshDeformation::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus controlMeshDeformation::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus controlMeshDeformation::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void controlMeshDeformation::serialize(Util::Stream& stream)
{
displayStringD("controlMeshDeformation::serialize");
	int version = 2;
	stream >> version;
	if( version==2)
	{
		stream >> this->basedeform;
		stream >> this->cacheTimeStamp;
		stream >> this->controlToWorld;
		stream >> this->controlBaseToWorld;
		this->bValid = true;
	}
}

