#include "pre.h"

// PRO - ������ ��� PRO ������
// #ifdef SUBDYWRAP_PRO

// ��. mayaIds.txt
const unsigned TypeIdBase = 0xDA020;

// node static ids and names

#include "subdyWrap.h"
MTypeId subdyWrap::id( TypeIdBase+00 );
const MString subdyWrap::typeName( "subdyWrap" );


#include "controlMeshCache.h"
MTypeId controlMeshCache::id( TypeIdBase+01 );
const MString controlMeshCache::typeName( "subdyWrapControlMeshCache" );

#include "controlMeshDeformation.h"
MTypeId controlMeshDeformation::id( TypeIdBase+02 );
const MString controlMeshDeformation::typeName( "subdyWrapControlMeshDeformation" );


#include "targetBinding.h"
MTypeId targetBinding::id( TypeIdBase+03 );
const MString targetBinding::typeName( "subdyWrapTargetBinding" );

#include "subdyWrapCmd.h"
const MString subdyWrapCmd::typeName( "subdyWrapCmd" );

