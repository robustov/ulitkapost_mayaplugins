#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"
#include "Math/Math.h"
#include "Math/Rotation3.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", controlMeshDeformation::id );
	}
/*/

// ���������� ����������� �����
struct basevertexdeform
{
	Math::Vec3f offset;
	Math::Rot3f rotate;
	Math::Matrix4f transform;	// ��������+������� / �� ����� ����� � ��������
	Math::Vec3f normal;			// ������� � �����
	float scale;					// ��������� � ����������� ����� ��������

//	float deformationfactor;		// ������� ���������
	basevertexdeform()
	{
//		deformationfactor=0;
		scale=1;
	}
};

class controlMeshCache;

class controlMeshDeformation : public MPxData
{
public:
	bool bValid;
	int cacheTimeStamp;
	// ����������
	std::vector<basevertexdeform> basedeform;

	// ������� � ������ ������� ����������
	Math::Matrix4f controlToWorld;
	Math::Matrix4f controlBaseToWorld;

public:
	bool buildDeformations(
		MObject meshobj, 
		Math::Matrix4f controlToWorld, 
		Math::Matrix4f controlBaseToWorld, 
		controlMeshCache* cache, 
		float scale,						// ����� scale
		std::map<int, float>& vertscale		// ������������ �����
		);

	const basevertexdeform& getDeformation(int v) const
	{
		return basedeform[v];
	}

	void drawDeformation(
		MObject meshobj, 
		Math::Matrix4f controlToWorld, 
		Math::Matrix4f controlBaseToWorld, 
		controlMeshCache* controlmesh,
		bool drawMesh,
		bool drawNormals
		);
	void drawMesh(
		MObject meshobj, 
		Math::Matrix4f controlToWorld, 
		controlMeshCache* controlmesh
		);
protected:
	static const int NORMAL=1, TANGENTU=0, TANGENTV=2;
	void calcVertexDeformation(
		const Math::Vec3f& V, 
		const Math::Vec3f& srcV, 
		const Math::Vec3f& N, 
		const Math::Vec3f& srcN, 
		const Math::Vec3f& tangentU, 
		const Math::Vec3f& srcTangentU, 
		Math::Matrix3f& Basis, 
		Math::Matrix3f& oldBasis,
		Math::Matrix4f& transform 
		);

public:
	controlMeshDeformation();
	virtual ~controlMeshDeformation(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;

protected:
	void serialize(Util::Stream& stream);
};

template<class Stream> inline
Stream& operator >> (Stream& out, basevertexdeform& v)
{
	out >> v.offset;
	out >> v.rotate;
	out >> v.transform;
	out >> v.normal;
//	out >> v.deformationfactor;
	return out;
}
