//
// Copyright (C) 
// File: targetBindingCmd.cpp
// MEL Command: targetBinding

#include "targetBinding.h"

#include <maya/MGlobal.h>
#include <maya/MProgressWindow.h>

#include "MathNMaya/MathNMaya.h"
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <mathNgl/mathNgl.h>
#include <sstream>
#include "controlMeshDeformation.h"
#include "controlMeshCache.h"

geometryInfluence::geometryInfluence()
{
	cacheTimeStamp = -1;
}

bool geometryInfluence::UpdateBinding( 
	MItGeometry& iter, 
	MMatrix& targetToControl, 
	controlMeshCache* cache, 
	bool bInteractive, 
	bool bUseUVspace, 
	const char* UVspaceName, 
	int projectionCount)
{
//	bValid = false;
	bool bProgressed = false;
	bool bProgress = MGlobal::mayaState()==MGlobal::kInteractive;

	::copy( worldMatrix, targetToControl);

	if(cache->timeStamp!=this->cacheTimeStamp)
		influences.clear();
	else
		bProgress = false;

	this->cacheTimeStamp = cache->timeStamp;

	// ������ �������� ������� ���������
	int updatedcount = 0;
	int p=0;
	for ( iter.reset(); !iter.isDone(); iter.next(), p++) 
	{
		MPoint _pt = iter.position();
		Math::Vec3f pt; ::copy(pt, _pt);

		int index = iter.index();
		std::map<int, pointInfluence>::iterator myit = this->influences.find(index);
		if( myit!=this->influences.end() && !bInteractive)
			continue;
		pt = this->worldMatrix*pt;
		pointInfluence& pi = this->influences[index];
		if( pi.source_pt == pt && myit!=this->influences.end())
			continue;

		if( bProgress)
		{
			MProgressWindow::reserve();
			MProgressWindow::setInterruptable(false);
			MProgressWindow::setTitle("Binding targets");
			MProgressWindow::startProgress();
			MProgressWindow::setProgressRange(0, iter.count());
			MProgressWindow::setProgressStatus("...");
			bProgressed = true;
		}
		if( bProgressed && (p&0x20)==0)
			MProgressWindow::setProgress(p);
		pi.source_pt = pt;
		pi.reset();
//		pt = this->worldMatrix*pt;

		cache->projectionPoint(pt, pi.projectionlist, projectionCount);
		updatedcount++;
	}
	if( updatedcount)
	{
		displayStringD("targetBinding::UpdateBinding - update %d verts", updatedcount);
		if( bProgressed)
		{
			MProgressWindow::endProgress();
		}
	}

//	bValid = true;
	return true;
}

Math::Vec3f geometryInfluence::deformPoint( 
	controlMeshCache* controlmesh, 
	controlMeshDeformation* deformation, 
	int index, 
	Math::Vec3f pt, 
	float envelope
	)
{
	Math::Vec3f project = pt, projectbase = pt;
	Math::Vec3f offset;
	Math::Rot3f rotate;
//	float deformfactor;
	float scale;

	pointInfluence& pi = this->influences[index];

	this->getPointDeformation(
		controlmesh, deformation, project, projectbase, offset, rotate, scale, 
//		deformfactor, 
		pi);

	Math::Vec3f res_de = pt;
	// Rotation
	{
		Math::Matrix4f m;
		rotate.getMatrix(m);

		Math::Vec3f dir = pt - projectbase;
		Math::Vec3f dirR = m*dir;
		Math::Vec3f s = (dirR-dir);
		res_de += s*envelope;
	}
	// Offset
	{
		res_de += offset*envelope;
	}
	// Scale
	{
		Math::Vec3f dirtoproject = res_de - project;
		Math::Vec3f scalevec = -dirtoproject*(1-scale);
		res_de += scalevec*envelope;
	}

	// deformfactor - ������� ����������

	pi.deformed_pt = res_de;
	return res_de;
}

Math::Vec3f geometryInfluence::deformPoint2( 
	controlMeshCache* controlmesh, 
	controlMeshDeformation* deformation, 
	int index, 
	Math::Vec3f pt, 
	float commonOffset,
	float envelope
	)
{
	pointInfluence& pi = this->influences[index];
	int ipr = -1;
	const pointOnControlMesh& ponm = pi.getProjection(ipr);

	float scale = 0;
	Math::Vec3f newpt = Math::Vec3f(0, 0, 0);
	Math::Vec3f project = Math::Vec3f(0, 0, 0);

	Math::Vec3f normal = Math::Vec3f(0, 0, 0);
	for( unsigned iv=0; iv<3; iv++)
	{
		int v = ponm.v[iv];
		float w = ponm.w[iv];
		Math::Vec3f vert = controlmesh->getVert(v);
		vert = deformation->controlBaseToWorld*vert;
		const basevertexdeform& bvd = deformation->getDeformation(v);

		Math::Vec3f dest = bvd.transform * pt;
		newpt += dest*w;

		dest = bvd.transform * vert;
		project += dest * w;

		scale += (bvd.scale) * w;

		normal += bvd.normal;
	}
	// Scale & offset
	{
		Math::Vec3f dirtoproject = newpt - project;
		Math::Vec3f scalevec = -dirtoproject*(1-scale);
		Math::Vec3f offsetvec = normal.normalized()*commonOffset;
		newpt += scalevec;
		newpt += offsetvec;
	}

	// envelope
	Math::Vec3f res_de = pt*(1-envelope) + newpt*envelope;

	pi.deformed_pt = newpt;
	return res_de;
}

Math::Vec3f geometryInfluence::deformPoint3( 
	controlMeshCache* controlmesh, 
	controlMeshDeformation* deformation, 
	int index, 
	Math::Vec3f pt, 
	float commonOffset,
	float envelope, 
	float radius
	)
{
	if(radius<=0)
	{
		return this->deformPoint2( 
			controlmesh, 
			deformation, 
			index, 
			pt, 
			commonOffset, 
			envelope
			);
	}

	pointInfluence& pi = this->influences[index];

	Math::Vec3f sumpt = Math::Vec3f(0, 0, 0);
	float sumfactor=0;

	for(int pr=0; pr<(int)pi.projectionlist.size(); pr++)
	{
		const pointOnControlMesh& ponm = pi.getProjection(pr);
		if( pr>0 && ponm.distance > radius)
			continue;
		float factor = ponm.distance - radius;

		float scale = 0;
		Math::Vec3f newpt = Math::Vec3f(0, 0, 0);
		Math::Vec3f project = Math::Vec3f(0, 0, 0);
		Math::Vec3f normal = Math::Vec3f(0, 0, 0);

		for( unsigned iv=0; iv<3; iv++)
		{
			int v = ponm.v[iv];
			float w = ponm.w[iv];
			Math::Vec3f vert = controlmesh->getVert(v);
			vert = deformation->controlBaseToWorld*vert;
			const basevertexdeform& bvd = deformation->getDeformation(v);

			Math::Vec3f dest = bvd.transform * pt;
			newpt += dest*w;

			dest = bvd.transform * vert;
			project += dest * w;

			scale += (bvd.scale) * w;
			normal += bvd.normal;
		}
		// Scale
		{
			Math::Vec3f dirtoproject = newpt - project;
			Math::Vec3f scalevec = -dirtoproject*(1-scale);
			newpt += scalevec;
			Math::Vec3f offsetvec = normal.normalized()*commonOffset;
			newpt += offsetvec;
		}

		sumpt += newpt*factor;
		sumfactor += factor;
	}
	if( sumfactor==0) sumfactor = 1;
	sumpt = sumpt*(1/sumfactor);

	// envelope
	Math::Vec3f res_de = pt*(1-envelope) + sumpt*envelope;

	pi.deformed_pt = sumpt;
	return res_de;
}

// ������ ��� ���������� �������� (world space)
void geometryInfluence::getPointDeformation(
	controlMeshCache* controlmesh, 
	controlMeshDeformation* deformation, 
	Math::Vec3f& project,				// �������� �����
	Math::Vec3f& projectbase,			// �������� �����
	Math::Vec3f& offset,				// ��������
	Math::Rot3f& rotate,				// ��������
	float& scale,						// �����
//	float& deformfactor,				// ���. �������� ����������
	const pointInfluence& pi, 			// ��������
	int ipr								// ������ ��������
	) const
{
	offset = Math::Vec3f(0, 0, 0);
	rotate = Math::Rot3f(0, 0, 0, 0);
	scale = 0;
//	deformfactor = 0;

	project = Math::Vec3f(0, 0, 0);
	projectbase = Math::Vec3f(0, 0, 0);
	const pointOnControlMesh& ponm = pi.getProjection(ipr);
	for( unsigned iv=0; iv<3; iv++)
	{
		int v = ponm.v[iv];
		float w = ponm.w[iv];
		Math::Vec3f vert = controlmesh->getVert(v);
		vert = deformation->controlBaseToWorld*vert;
		const basevertexdeform& bvd = deformation->getDeformation(v);

		Math::Rot3f r = bvd.rotate.normalized();
		rotate += r*w;
		offset += bvd.offset*w;
		project += (vert + bvd.offset) * w;
		projectbase += (vert) * w;
//		deformfactor += bvd.deformationfactor * w;
		scale += (bvd.scale) * w;
	}
}

void geometryInfluence::drawBinding(
	controlMeshCache* controlmesh, 
	controlMeshDeformation* deformation,
	std::vector<int>& viewverts
	)
{
//	Math::Matrix4f world = this->worldMatrix;
//	world.invert();
	Math::Matrix4f world = Math::Matrix4f::id;

	glPushMatrix();
	float* d = world.data();
	glMultMatrixf(d);
	{
		std::map<int, pointInfluence>::iterator it =  this->influences.begin();
		for( ; it != this->influences.end(); it++)
		{
			if( !viewverts.empty())
			{
				for(int i=0; i<(int)viewverts.size(); i++)
					if( viewverts[i] == it->first)
						break;
				if( i>=(int)viewverts.size()) continue;
			}
			pointInfluence& pi = it->second;

			glColor3f(1, 1, 1);
			drawVertexBinding(controlmesh, deformation, pi, -1);

			for(int i=0; i<(int)pi.projectionlist.size(); i++)
			{
				if(i==pi.selectedProjection) continue;

				glLineStipple(1, 0x8888);
				glEnable(GL_LINE_STIPPLE);

				glColor3f(1, 0, 0);
				drawVertexBinding(controlmesh, deformation, pi, i);

				glDisable(GL_LINE_STIPPLE);
			}
		}
	}
	glPopMatrix();
}

void geometryInfluence::dumpBinding(
	controlMeshCache* controlmesh, 
	controlMeshDeformation* deformation,
	std::vector<int>& viewverts
	)
{
	std::map<int, pointInfluence>::iterator it =  this->influences.begin();
	for( ; it != this->influences.end(); it++)
	{
		if( !viewverts.empty())
		{
			for(int i=0; i<(int)viewverts.size(); i++)
				if( viewverts[i] == it->first)
					break;
			if( i>=(int)viewverts.size()) continue;
		}
		pointInfluence& pi = it->second;

		printf("vertex %d:\n", it->first);
		for(int i=0; i<(int)pi.projectionlist.size(); i++)
		{
			const pointOnControlMesh& ponm = pi.getProjection(i);
			printf("  face %d distance %f\n", ponm.face, ponm.distance);
		}
	}
	fflush(stdout);
}

void geometryInfluence::drawVertexBinding(
	controlMeshCache* controlmesh, 
	controlMeshDeformation* deformation,
	pointInfluence& pi, 
	int ipr
	)
{
	Math::Vec3f project, projectbase;
	Math::Vec3f offset;
	Math::Rot3f rotate;
//	float deformfactor;
	float scale = 1;
	getPointDeformation(
		controlmesh, deformation, project, projectbase, offset, rotate, scale, 
//		deformfactor, 
		pi, ipr);

	glBegin( GL_LINES );
	glVertex3f( pi.deformed_pt);
	glVertex3f( project);
	glEnd();
}

void geometryInfluence::switchBinding(
	controlMeshCache* controlmesh, 
	controlMeshDeformation* deformation,
	std::vector<int>& viewverts
	)
{
	std::map<int, pointInfluence>::iterator it =  this->influences.begin();
	for( ; it != this->influences.end(); it++)
	{
		{
			for(int i=0; i<(int)viewverts.size(); i++)
				if( viewverts[i] == it->first)
					break;
			if( i>=(int)viewverts.size()) continue;
		}
		pointInfluence& pi = it->second;

		pi.selectedProjection++;
		if(pi.selectedProjection>=(int)pi.projectionlist.size())
			pi.selectedProjection = 0;
	}
}




targetBinding::targetBinding()
{
	bValid = false;
}
targetBinding::~targetBinding()
{
}

MTypeId targetBinding::typeId() const
{
	return targetBinding::id;
}

MString targetBinding::name() const
{ 
	return targetBinding::typeName; 
}

void* targetBinding::creator()
{
	return new targetBinding();
}

void targetBinding::copy( const MPxData& other )
{
	const targetBinding* arg = (const targetBinding*)&other;
	this->bValid = arg->bValid;
//	this->cacheTimeStamp = arg->cacheTimeStamp;
	this->gis = arg->gis;
}

MStatus targetBinding::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("targetBinding: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus targetBinding::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus targetBinding::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus targetBinding::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void targetBinding::serialize(Util::Stream& stream)
{
displayStringD("targetBinding::serialize");
	int version = 3;
	stream >> version;
	if( version==3)
	{
		stream >> gis;
		this->bValid = true;
	}
}

