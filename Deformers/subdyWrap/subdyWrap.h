#pragma once
#include "pre.h"

//
// Copyright (C) 
// File: subdyWrapNode.h
//
// Deformer Node: subdyWrap

#include <maya/MPxNode.h>
#include <maya/MPxDeformerNode.h>
#include <maya/MDataBlock.h>
#include <maya/MPlug.h>
#include <maya/MItGeometry.h>
#include <maya/MMatrix.h>

#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 

class controlMeshCache;
class controlMeshDeformation;
class targetBinding;
struct geometryInfluence;
class M3dView;

class subdyWrap : public MPxDeformerNode
{
public:
	subdyWrap();
	virtual	~subdyWrap(); 

	virtual MStatus subdyWrap::compute( 
		const MPlug& plug,
		MDataBlock& dataBlock);

    virtual MStatus deform(
		MDataBlock& data,
		MItGeometry& iter,
		const MMatrix& mat,
		unsigned int multiIndex);

	static void* creator();
	static MStatus initialize();

protected:
	MStatus Build_controlMeshCache(
		const MPlug& plug, MDataBlock& data);
	MStatus Build_controlMeshDeformation(
		const MPlug& plug, MDataBlock& data);
	/*/
	MStatus Build_targetBinding(
		const MPlug& plug, MDataBlock& data);
	/*/

protected:
	controlMeshCache* Load_controlMeshCache(
		MDataBlock& data);
	controlMeshDeformation* Load_controlMeshDeformation(
		int cacheTimeStamp, 
		MDataBlock& data);
	geometryInfluence* Load_targetBinding(
		MDataBlock& data, int multiIndex, MItGeometry& iter, MMatrix& targetToControl);

// Drawing
public:
	void DrawBinding(
		M3dView& view, 
		std::vector<int>& viewverts
		);
	void DrawDeformation(
		M3dView& view
		);
	void beginOverlayDraw(M3dView& view);
	void endOverlayDraw(M3dView& view);

// DUMP
public:
	// DumpBinding
	void DumpBinding(
		std::vector<int>& viewverts
		);

// SwitchBinding
public:
	void SwitchBinding(
		MObject target, 
		std::vector<int>& viewverts
		);

public:
	// ����������� ����� � ���������
	static MObject i_controlMesh;
	static MObject i_controlMeshMatrix;
	static MObject i_controlMeshBase;
	static MObject i_controlMeshBaseMatrix;
	static MObject i_controlSubdivSteps;
#ifndef FREE
	static MObject i_controlBaseInteractive;		// ��� ����� ��������� i_controlMeshBase ��� i_controlMeshBaseMatrix ������������� ���
	static MObject i_useUVspace;				// ��� ������������� ������������ UVspace
	static MObject i_controlUVspaceName;		// ��� UVspace ����������� ����� ��� i_useUVspace=true
#endif
	// ��� ����������� ����� (���������)
	static MObject i_controlMeshCache;
	// ��� ���������� ����������� ����� 
	static MObject i_controlMeshDeformation;

	// target �����
#ifndef FREE
	static MObject i_targetInteractive;			// ��� ����� ��������� Base ��� BaseMatrix ������������� ��� ��������
	static MObject i_targetUVspaceName;
#endif
	// ������ ����� ��������
	static MObject i_targetBinding;				// ��� UVspace ����� ������������� ��� ��������
	
	// ����� scale
	static MObject i_commonScale;				// ������ bProjection
#ifndef FREE
	// scale ������ (������� ������ �� ������� ��������)
	static MObject i_verticiesScale;

	// ������� �������� ��� ����� �������
	static MObject i_projectionCount;
	// ������ ��� ���������, ���� 0 - ��� ���������
	static MObject i_blendRadius;
	// offset. ������� ��� ���������
	static MObject i_commonOffset;
#endif

	// �������� BaseControlMesh
	static MObject i_viewBaseControlMesh;		


public:
	static MTypeId id;
	static const MString typeName;


};

