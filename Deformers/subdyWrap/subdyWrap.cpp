//
// Copyright (C) 
// 
// File: subdyWrapNode.cpp
//
// Dependency Graph Node: subdyWrap
//
// Author: Maya Plug-in Wizard 2.0
//

#include "subdyWrap.h"

#include "pre.h"
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MPoint.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnStringData.h>

#include <maya/MGlobal.h>
#include <maya/M3dView.h>

#include "controlMeshCache.h"
#include "controlMeshDeformation.h"
#include "targetBinding.h"

#include "MathNMaya/MathNMaya.h"


// ����������� ����� � ���������
MObject subdyWrap::i_controlMesh;
MObject subdyWrap::i_controlMeshMatrix;
MObject subdyWrap::i_controlMeshBase;
MObject subdyWrap::i_controlMeshBaseMatrix;
MObject subdyWrap::i_controlSubdivSteps;
#ifndef FREE
MObject subdyWrap::i_controlBaseInteractive;		// ��� ����� ��������� i_controlMeshBase ��� i_controlMeshBaseMatrix ������������� ���
MObject subdyWrap::i_useUVspace;				// ��� ������������� ������������ UVspace
MObject subdyWrap::i_controlUVspaceName;		// ��� UVspace ����������� ����� ��� i_useUVspace=true
#endif
// ��� ����������� ����� (���������)
MObject subdyWrap::i_controlMeshCache;
// ��� ���������� ����������� ����� 
MObject subdyWrap::i_controlMeshDeformation;

// target �����
#ifndef FREE
MObject subdyWrap::i_targetInteractive;			// ��� ����� ��������� Base ��� BaseMatrix ������������� ��� ��������
MObject subdyWrap::i_targetUVspaceName;
#endif
// ������ ����� ��������
MObject subdyWrap::i_targetBinding;				// ��� UVspace ����� ������������� ��� ��������

// ����� scale
MObject subdyWrap::i_commonScale;				// ������ bProjection
#ifndef FREE
// scale ������ (������� ������ �� ������� ��������)
MObject subdyWrap::i_verticiesScale;
// ������� �������� ��� ����� �������
MObject subdyWrap::i_projectionCount;
// ������ ��� ���������, ���� 0 - ��� ���������
MObject subdyWrap::i_blendRadius;
// offset. ������� ��� ���������
MObject subdyWrap::i_commonOffset;
#endif

// �������� BaseControlMesh
MObject subdyWrap::i_viewBaseControlMesh;		

subdyWrap::subdyWrap()
{
}
subdyWrap::~subdyWrap()
{
}

MStatus subdyWrap::compute( 
	const MPlug& plug,
	MDataBlock& dataBlock )
{
	displayStringD( "subdyWrap::compute %s", plug.name().asChar());
	if(plug==i_controlMeshCache)
		return Build_controlMeshCache(plug, dataBlock);
	if(plug==i_controlMeshDeformation)
		return Build_controlMeshDeformation(plug, dataBlock);
//	if( plug.isElement() && plug.parent()==i_targetBinding)
//		return Build_targetBinding(plug, dataBlock);
	return MPxDeformerNode::compute(plug, dataBlock);
}

MStatus subdyWrap::deform(
	MDataBlock& data,
	MItGeometry& iter,
	const MMatrix& m,
	unsigned int multiIndex)
{
displayStringD( "subdyWrap::deform multiIndex=%d", multiIndex);
	MStatus stat;
 
	bool bViewBinding = false;
	float envelope = data.inputValue(this->envelope,&stat).asFloat();
	if( !stat) return MS::kFailure;
	if( !envelope) return MS::kSuccess;

	controlMeshCache* cache = Load_controlMeshCache(data);
	if( !cache) return MS::kFailure;

	controlMeshDeformation* deformation = Load_controlMeshDeformation(cache->timeStamp, data);
	if( !deformation) return MS::kFailure;

	// �������
	MMatrix cmBaseSpace = MFnMatrixData(data.inputValue(this->i_controlMeshBaseMatrix).data()).matrix();
	MMatrix cmSpace = MFnMatrixData(data.inputValue(this->i_controlMeshMatrix).data()).matrix();
	MMatrix cmBaseSpace_inv = cmBaseSpace.inverse();
//	MMatrix targetToControl = m * cmBaseSpace_inv;
//	MMatrix targetToControl_inv = targetToControl.inverse();
	MMatrix targetToControl = m;
	MMatrix targetToControl_inv = m.inverse();
	float scale = (float)data.inputValue(i_commonScale).asDouble();
	double blendRadius = data.inputValue( this->i_blendRadius, &stat).asDouble();
	double commonOffset = data.inputValue( this->i_commonOffset, &stat).asDouble();

	MMatrix bindingTransform = m * cmBaseSpace_inv;
	geometryInfluence* binding = Load_targetBinding(data, multiIndex, iter, bindingTransform);
	if( !binding) return MS::kFailure;

	for ( iter.reset(); !iter.isDone(); iter.next()) 
	{
		int index = iter.index();

		MPoint _pt = iter.position();
		_pt = _pt * targetToControl;
		Math::Vec3f pt; 
		copy(pt, _pt);

//		pt = binding->deformPoint2(cache, deformation, index, pt, commonOffset, envelope);
		pt = binding->deformPoint3(cache, deformation, index, pt, (float)commonOffset, envelope, (float)blendRadius);

		::copy(_pt, pt);
		_pt = _pt * targetToControl_inv;
		iter.setPosition(_pt);
	}

	if(bViewBinding)
	{
		M3dView view = M3dView::active3dView();
		std::vector<int> viewverts;
		DrawBinding(view, viewverts);
	}

	return MS::kSuccess;
}




MStatus subdyWrap::Build_controlMeshCache(
	const MPlug& plug, MDataBlock& data)
{
//	displayStringD( "subdyWrap::Build_controlMeshCache");
	MStatus stat;

	controlMeshCache* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( i_controlMeshCache, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<controlMeshCache*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( controlMeshCache::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<controlMeshCache*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	int subdivstep = data.inputValue( this->i_controlSubdivSteps, &stat).asInt();
#ifndef FREE
	bool bBaseInfluence = data.inputValue( this->i_controlBaseInteractive, &stat).asBool();
	bool bUseUVspace = data.inputValue( this->i_useUVspace, &stat).asBool();
	int projectionCount = data.inputValue( this->i_projectionCount, &stat).asInt();
	double blendRadius = data.inputValue( this->i_blendRadius, &stat).asDouble();
	MString UVspaceName = data.inputValue( this->i_controlUVspaceName, &stat).asString();
#else
	bool bBaseInfluence = false;
	bool bUseUVspace = false;
	int projectionCount = 1;
	MString UVspaceName = "";
#endif

	MObject meshobj = data.inputValue( this->i_controlMeshBase, &stat ).asMesh();

	pData->InitMesh(meshobj, subdivstep, bBaseInfluence, bUseUVspace, UVspaceName.asChar());

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}
MStatus subdyWrap::Build_controlMeshDeformation(
	const MPlug& plug, MDataBlock& data)
{
//	displayStringD( "subdyWrap::Build_controlMeshDeformation");
	MStatus stat;

	bool bNewData = false;
	MDataHandle outputData = data.outputValue( i_controlMeshDeformation, &stat );
	MPxData* userdata = outputData.asPluginData();
	controlMeshDeformation* pData = static_cast<controlMeshDeformation*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( controlMeshDeformation::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<controlMeshDeformation*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	controlMeshCache* cache = Load_controlMeshCache(data);
	if( !cache) return MS::kFailure;
	
	float scale = (float)data.inputValue( this->i_commonScale, &stat).asDouble();
	MMatrix cmBaseSpace = MFnMatrixData(data.inputValue(this->i_controlMeshBaseMatrix).data()).matrix();
	MMatrix cmSpace = MFnMatrixData(data.inputValue(this->i_controlMeshMatrix).data()).matrix();
	MMatrix _controlToBase = cmSpace * cmBaseSpace.inverse();
	Math::Matrix4f controlToBase;
	::copy( controlToBase, _controlToBase);
	
	Math::Matrix4f controlToWorld;
	::copy( controlToWorld, cmSpace);
	Math::Matrix4f controlBaseToWorld;
	::copy( controlBaseToWorld, cmBaseSpace);

	std::map<int, float> vertscale;
#ifndef FREE
	MArrayDataHandle adh = data.inputArrayValue( i_verticiesScale);
	int elemCount = adh.elementCount();
	for ( int idx=0; idx<elemCount; idx++ )
	{
		int elemIndex = adh.elementIndex();
		float val = (float)adh.inputValue().asDouble();
		vertscale[elemIndex] = val;
		adh.next();
	}
#endif

	MObject meshobj = data.inputValue( this->i_controlMesh, &stat ).asMesh();

	// ������ ������������� ����������� �����
	pData->buildDeformations(
		meshobj, controlToWorld, controlBaseToWorld, cache, scale, vertscale);

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}
/*/
MStatus subdyWrap::Build_targetBinding(
	const MPlug& plug, MDataBlock& data)
{
	MStatus stat;

	bool bNewData = false;
	MDataHandle outputData = data.outputValue( plug, &stat );
	MPxData* userdata = outputData.asPluginData();
	targetBinding* pData = static_cast<targetBinding*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( targetBinding::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<targetBinding*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	bool bInteractive = data.inputValue( this->i_targetInteractive, &stat).asBool();
	bool bUseUVspace = data.inputValue( this->i_useUVspace, &stat).asBool();
	MString UVspaceName = data.inputValue( this->i_targetUVspaceName, &stat).asString();

	pData->InitBinding(
		bInteractive, bUseUVspace, UVspaceName.asChar());

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}
/*/






controlMeshCache* subdyWrap::Load_controlMeshCache(
	MDataBlock& data)
{
	MStatus stat;
	MDataHandle inputData = data.inputValue(i_controlMeshCache, &stat);
	MPxData* pxdata = inputData.asPluginData();
	controlMeshCache* hgh = (controlMeshCache*)pxdata;
	if( !hgh || !hgh->bValid)
	{
		Build_controlMeshCache( MPlug(thisMObject(), i_controlMeshCache), data);
		MDataHandle inputData = data.inputValue(i_controlMeshCache, &stat);
		MPxData* pxdata = inputData.asPluginData();
		controlMeshCache* hgh = (controlMeshCache*)pxdata;
		if( !hgh->bValid) return NULL;
		return hgh;
	}
	return hgh;
}
controlMeshDeformation* subdyWrap::Load_controlMeshDeformation(
	int cacheTimeStamp, 
	MDataBlock& data)
{
	MStatus stat;
	MDataHandle inputData = data.inputValue(i_controlMeshDeformation, &stat);
	MPxData* pxdata = inputData.asPluginData();
	controlMeshDeformation* hgh = (controlMeshDeformation*)pxdata;
	if( !hgh || !hgh->bValid || cacheTimeStamp!=hgh->cacheTimeStamp)
	{
		Build_controlMeshDeformation( MPlug(thisMObject(), i_controlMeshDeformation), data);
		MDataHandle inputData = data.inputValue(i_controlMeshDeformation, &stat);
		MPxData* pxdata = inputData.asPluginData();
		controlMeshDeformation* hgh = (controlMeshDeformation*)pxdata;
		if( !hgh->bValid) return NULL;
		return hgh;
	}
	return hgh;
}

geometryInfluence* subdyWrap::Load_targetBinding(
	MDataBlock& data, 
	int multiIndex, 
	MItGeometry& iter, 
	MMatrix& targetToControl
	)
{
	MStatus stat;

	bool bNewData = false;
	MDataHandle outputData = data.outputValue( i_targetBinding, &stat );
	MPxData* userdata = outputData.asPluginData();
	targetBinding* pData = static_cast<targetBinding*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( targetBinding::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<targetBinding*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}


#ifndef FREE
	bool bInteractive = data.inputValue( this->i_targetInteractive, &stat).asBool();
	bool bUseUVspace = data.inputValue( this->i_useUVspace, &stat).asBool();
	int projectionCount = data.inputValue( this->i_projectionCount, &stat).asInt();
	double blendRadius = data.inputValue( this->i_blendRadius, &stat).asDouble();
	MString UVspaceName = data.inputValue( this->i_targetUVspaceName, &stat).asString();
#else
	bool bInteractive = 0;
	bool bUseUVspace = 0;
	int projectionCount = 1;
	MString UVspaceName = "";
#endif

	controlMeshCache* cache = Load_controlMeshCache(data);
	if( !cache) return NULL;

	
	geometryInfluence& hgh = pData->gis[multiIndex];

	hgh.UpdateBinding(
		iter, targetToControl, cache, bInteractive, bUseUVspace, UVspaceName.asChar(), projectionCount);

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();

	if( bNewData)
	{
		MDataHandle outputData = data.outputValue( i_targetBinding, &stat );
		MPxData* userdata = outputData.asPluginData();
		pData = static_cast<targetBinding*>(userdata);
 	}

	return &pData->gis[multiIndex];
}

void subdyWrap::DrawBinding(
	M3dView& view, 
	std::vector<int>& viewverts
	)
{
	MStatus stat;

	MDataBlock data = forceCache();

	controlMeshCache* cache = Load_controlMeshCache(data);
	if( !cache) return;

	controlMeshDeformation* deformation = Load_controlMeshDeformation(cache->timeStamp, data);
	if( !deformation) return;

	beginOverlayDraw(view);

//		MMatrix cmSpace = MFnMatrixData(data.inputValue(this->i_controlMeshMatrix).data()).matrix();
	MMatrix cmSpace = MFnMatrixData(data.inputValue(this->i_controlMeshBaseMatrix).data()).matrix();
	Math::Matrix4f world;
	::copy(world, cmSpace);
	
//	glPushMatrix();
//	float* d = world.data();
//	glMultMatrixf(d);

	MPxData* pxdata = data.inputValue(i_targetBinding, &stat).asPluginData();
	targetBinding* tb = (targetBinding*)pxdata;
	if( tb)
	{
		std::map<int, geometryInfluence>::iterator it = tb->gis.begin();
		for(;it != tb->gis.end(); it++)
		{
			geometryInfluence& gi = it->second;
			gi.drawBinding(cache, deformation, viewverts);
		}
	}
//	glPopMatrix();
	endOverlayDraw(view);
}
// DumpBinding
void subdyWrap::DumpBinding(
	std::vector<int>& viewverts
	)
{
	MStatus stat;

	MDataBlock data = forceCache();

	controlMeshCache* cache = Load_controlMeshCache(data);
	if( !cache) return;

	controlMeshDeformation* deformation = Load_controlMeshDeformation(cache->timeStamp, data);
	if( !deformation) return;

	MMatrix cmSpace = MFnMatrixData(data.inputValue(this->i_controlMeshBaseMatrix).data()).matrix();
	Math::Matrix4f world;
	::copy(world, cmSpace);
	
	MPxData* pxdata = data.inputValue(i_targetBinding, &stat).asPluginData();
	targetBinding* tb = (targetBinding*)pxdata;
	if( tb)
	{
		std::map<int, geometryInfluence>::iterator it = tb->gis.begin();
		for(;it != tb->gis.end(); it++)
		{
			geometryInfluence& gi = it->second;
			gi.dumpBinding(cache, deformation, viewverts);
		}
	}
}

void subdyWrap::DrawDeformation(
	M3dView& view
	)
{
	MStatus stat;

	MDataBlock data = forceCache();

	controlMeshCache* cache = Load_controlMeshCache(data);
	if( !cache) return;

	controlMeshDeformation* deformation = Load_controlMeshDeformation(cache->timeStamp, data);
	if( !deformation) return;

	float scale = (float)data.inputValue( this->i_commonScale, &stat).asDouble();
	MMatrix cmBaseSpace = MFnMatrixData(data.inputValue(this->i_controlMeshBaseMatrix).data()).matrix();
	MMatrix cmSpace = MFnMatrixData(data.inputValue(this->i_controlMeshMatrix).data()).matrix();
	MMatrix _controlToBase = cmSpace * cmBaseSpace.inverse();
	Math::Matrix4f controlToBase;
	::copy( controlToBase, _controlToBase);

	Math::Matrix4f controlToWorld;
	::copy( controlToWorld, cmSpace);
	Math::Matrix4f controlBaseToWorld;
	::copy( controlBaseToWorld, cmBaseSpace);

	MObject meshobj = data.inputValue( this->i_controlMesh, &stat ).asMesh();

	beginOverlayDraw(view);

//		MMatrix cmSpace = MFnMatrixData(data.inputValue(this->i_controlMeshMatrix).data()).matrix();
//	MMatrix cmSpace = MFnMatrixData(data.inputValue(this->i_controlMeshBaseMatrix).data()).matrix();
	Math::Matrix4f world;
	::copy(world, cmBaseSpace);
	
//	glPushMatrix();
//	float* d = world.data();
//	glMultMatrixf(d);
	
//	deformation->drawDeformation(meshobj, controlToWorld, controlBaseToWorld, cache, true, true);
	deformation->drawMesh(meshobj, controlToWorld, cache);

//	glPopMatrix();
	endOverlayDraw(view);
}

void subdyWrap::beginOverlayDraw(M3dView& view)
{

	#ifndef _WIN32
        glXSwapBuffers(view.display(), view.window() );
    #else
        SwapBuffers(view.deviceContext() );
    #endif
	
	view.beginOverlayDrawing();
		/////////////////////////////
		view.clearOverlayPlane();
		glMatrixMode(GL_MODELVIEW);

}
void subdyWrap::endOverlayDraw(M3dView& view)
{
    view.endOverlayDrawing();
}



void subdyWrap::SwitchBinding(
	MObject target, 
	std::vector<int>& viewverts
	)
{
	MStatus stat;
	MDataBlock data = forceCache();

	controlMeshCache* cache = Load_controlMeshCache(data);
	if( !cache) return;

	controlMeshDeformation* deformation = Load_controlMeshDeformation(cache->timeStamp, data);
	if( !deformation) return;

	MPxData* pxdata = data.inputValue(i_targetBinding, &stat).asPluginData();
	targetBinding* tb = (targetBinding*)pxdata;
	if( tb)
	{
		std::map<int, geometryInfluence>::iterator it = tb->gis.begin();
		for(;it != tb->gis.end(); it++)
		{
			geometryInfluence& gi = it->second;
			gi.switchBinding(cache, deformation, viewverts);
		}
	}
	{
		MPlug aff(thisMObject(), i_commonScale);
		double v;
		aff.getValue(v);
		aff.setValue(v);
	}
}




void* subdyWrap::creator()
{
	return new subdyWrap();
}

MStatus subdyWrap::initialize()
{
	MFnNumericAttribute nAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_controlMesh = typedAttr.create( "controlMesh", "cm", MFnData::kMesh, &stat);
			::addAttribute(i_controlMesh, atInput|atHidden);

			i_controlMeshMatrix = typedAttr.create( "controlMeshMatrix", "cmm", MFnData::kMatrix, &stat);
			typedAttr.setWorldSpace(true);
			::addAttribute(i_controlMeshMatrix, atInput|atHidden);

			i_controlMeshBase = typedAttr.create( "controlMeshBase", "cmb", MFnData::kMesh, &stat);
			::addAttribute(i_controlMeshBase, atInput|atHidden);

			i_controlMeshBaseMatrix = typedAttr.create( "controlMeshBaseMatrix", "cmbm", MFnData::kMatrix, &stat);
			typedAttr.setWorldSpace(true);
			::addAttribute(i_controlMeshBaseMatrix, atInput|atHidden);

			i_controlSubdivSteps = nAttr.create( "controlSubdivSteps", "cmss", MFnNumericData::kInt, 1);
			nAttr.setMin(0);
			nAttr.setSoftMax(8);
			::addAttribute(i_controlSubdivSteps, atInput);

#ifndef FREE
			i_controlBaseInteractive = nAttr.create( "controlBaseInteractive", "cmbi", MFnNumericData::kBoolean, 0);
			::addAttribute(i_controlBaseInteractive, atInput);

			i_useUVspace = nAttr.create( "useUVspace", "uuv", MFnNumericData::kBoolean, 0);
			::addAttribute(i_useUVspace, atInput);

			MFnStringData stringData;
			i_controlUVspaceName = typedAttr.create( "controlUVspaceName", "cuvs", MFnData::kString, stringData.create(""));
			::addAttribute(i_controlUVspaceName, atInput);
#endif
			
			i_controlMeshCache = typedAttr.create( "controlMeshCache", "cmc", controlMeshCache::id);
			::addAttribute(i_controlMeshCache, atReadable|atWritable|atCached|atHidden );

			i_controlMeshDeformation = typedAttr.create( "controlMeshDeformation", "cmde", controlMeshDeformation::id);
			::addAttribute(i_controlMeshDeformation, atReadable|atWritable|atCached|atHidden );


#ifndef FREE
			// ��� ����� ��������� Base ��� BaseMatrix ������������� ��� ��������
			i_targetInteractive = nAttr.create( "targetInteractive", "tabi", MFnNumericData::kBoolean, 0);
			::addAttribute(i_targetInteractive, atInput);

			i_targetUVspaceName = typedAttr.create( "targetUVspaceName", "tuvs", MFnData::kString, stringData.create(""));
			::addAttribute(i_targetUVspaceName, atInput);
#endif
			// ������ ����� ��������
			i_targetBinding = typedAttr.create( "subdTargetBinding", "stabi", targetBinding::id);
			::addAttribute(i_targetBinding, atInput|atHidden );

			// ����� scale
			i_commonScale = nAttr.create( "commonScale", "sc", MFnNumericData::kDouble, 1);
			nAttr.setMin(0);
			nAttr.setSoftMax(2);
			::addAttribute(i_commonScale, atInput);
#ifndef FREE
			// scale ������ (������� ������ �� ������� ��������)
			i_verticiesScale = nAttr.create( "verticiesScale", "vsc", MFnNumericData::kDouble, 1);
			nAttr.setMin(0);
			nAttr.setSoftMax(2);
			::addAttribute(i_verticiesScale, atArray|atInput|atHidden);

			i_projectionCount = nAttr.create( "projectionCount", "pc", MFnNumericData::kInt, 2);
			nAttr.setMin(1);
			nAttr.setSoftMax(3);
			::addAttribute(i_projectionCount, atInput);

			i_blendRadius = nAttr.create( "blendRadius", "blr", MFnNumericData::kDouble, 0);
			nAttr.setMin(0);
			nAttr.setSoftMax(5);
			::addAttribute(i_blendRadius, atInput);

			i_commonOffset = nAttr.create( "commonOffset", "cof", MFnNumericData::kDouble, 0);
			nAttr.setSoftMin(0);
			nAttr.setSoftMax(5);
			::addAttribute(i_commonOffset, atInput);

#endif

			
			// �������� BaseControlMesh
			i_viewBaseControlMesh = nAttr.create( "viewBaseControlMesh", "vbcm", MFnNumericData::kBoolean, 0);
			::addAttribute(i_viewBaseControlMesh, atInput);
			

			// to i_controlMeshCache
			{
				MObject affect = i_controlMeshCache;
				stat = attributeAffects( i_controlMeshBase, affect );
				stat = attributeAffects( i_controlMeshBaseMatrix, affect );
				stat = attributeAffects( i_controlSubdivSteps, affect );
#ifndef FREE
				stat = attributeAffects( i_controlBaseInteractive, affect );
				stat = attributeAffects( i_useUVspace, affect );
				stat = attributeAffects( i_controlUVspaceName, affect );
				stat = attributeAffects( i_projectionCount, affect );
#endif
			}

			// to i_controlMeshDeformation
			{
				MObject affect = i_controlMeshDeformation;
				stat = attributeAffects( i_controlMesh, affect );
				stat = attributeAffects( i_controlMeshMatrix, affect );
				stat = attributeAffects( i_controlMeshBase, affect );
				stat = attributeAffects( i_controlMeshBaseMatrix, affect );
				stat = attributeAffects( i_controlSubdivSteps, affect );
				stat = attributeAffects( i_controlMeshCache, affect );
				stat = attributeAffects( i_commonScale, affect );
#ifndef FREE
				stat = attributeAffects( i_controlBaseInteractive, affect );
				stat = attributeAffects( i_useUVspace, affect );
				stat = attributeAffects( i_controlUVspaceName, affect );
				stat = attributeAffects( i_verticiesScale, affect );
				stat = attributeAffects( i_projectionCount, affect );
#endif
			}

			// to outputGeom
			{
				MObject affect = outputGeom;
				stat = attributeAffects( i_controlMesh, affect );
				stat = attributeAffects( i_controlMeshMatrix, affect );
				stat = attributeAffects( i_controlMeshBase, affect );
				stat = attributeAffects( i_controlMeshBaseMatrix, affect );
				stat = attributeAffects( i_controlSubdivSteps, affect );
				stat = attributeAffects( i_controlMeshCache, affect );
				stat = attributeAffects( i_controlMeshDeformation, affect );
				stat = attributeAffects( i_targetBinding, affect );
				stat = attributeAffects( i_commonScale, affect );
#ifndef FREE
				stat = attributeAffects( i_controlBaseInteractive, affect );
				stat = attributeAffects( i_useUVspace, affect );
				stat = attributeAffects( i_controlUVspaceName, affect );
				stat = attributeAffects( i_targetInteractive, affect );
				stat = attributeAffects( i_targetUVspaceName, affect );
				stat = attributeAffects( i_verticiesScale, affect );
				stat = attributeAffects( i_projectionCount, affect );
				stat = attributeAffects( i_blendRadius, affect );
				stat = attributeAffects( i_commonOffset, affect );
#endif
			}

		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	if( !SourceMelFromResource(MhInstPlugin, "AEsubdyWrapTemplate", "MEL", defines))
	{
		displayString("error source AEsubdyWrapTemplate.mel");
	}
	return MS::kSuccess;
}

