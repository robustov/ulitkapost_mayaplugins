set BIN_DIR=.\out\deformers
set BIN_DIR=\\server\bin

mkdir %BIN_DIR%
mkdir %BIN_DIR%\bin
mkdir %BIN_DIR%\mel
mkdir %BIN_DIR%\slim
mkdir %BIN_DIR%\docs

copy Mel\ulDeformersMenu.mel					%BIN_DIR%\mel

rem -------------------------------------
copy BinRelease85\subdyWrap.mll					%BIN_DIR%\bin
copy Mel\ulsubdyWrapMenu.mel					%BIN_DIR%\mel

rem -------------------------------------
rem copy Bin\ulWrap.mll								%BIN_DIR%\bin

rem copy Mel\ulWrapMenu.mel							%BIN_DIR%\mel
rem copy Mel\AEulWrapTemplate.mel					%BIN_DIR%\mel

pause