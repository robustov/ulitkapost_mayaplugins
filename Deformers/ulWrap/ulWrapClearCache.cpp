//
// Copyright (C) 
// File: ulWrapClearCacheCmd.cpp
// MEL Command: ulWrapClearCache

#include "ulWrapClearCache.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include "ulWrap.h"

ulWrapClearCache::ulWrapClearCache()
{
}
ulWrapClearCache::~ulWrapClearCache()
{
}

MSyntax ulWrapClearCache::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);	// wrap node
//	syntax.addFlag("f", "format", MSyntax::kString);
	return syntax;
}

MStatus ulWrapClearCache::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString argsl0;
	argData.getCommandArgument(0, argsl0);
	MObject obj;
	nodeFromName(argsl0, obj);
	if( obj.isNull())
	{
		MPlug plug;
		plugFromName(argsl0, plug);
		obj = plug.node();
	}
	if( obj.isNull())
		return MS::kFailure;

	MPlug plug(obj, ulWrap::i_cache);
	plug.setValue(MObject::kNullObj);

	return MS::kSuccess;
}

void* ulWrapClearCache::creator()
{
	return new ulWrapClearCache();
}

