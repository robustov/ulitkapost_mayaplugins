//
// Copyright (C) 
// File: ulWrapCacheCmd.cpp
// MEL Command: ulWrapCache

#include "ulWrapCache.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <sstream>
#include <maya/MPoint.h>
#include <maya/MFnMesh.h>
#include <maya/MFloatPointArray.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItGeometry.h>
#include <maya/MQuaternion.h>

#include "Util/EvalTime.h"
#include "Math/Edge.h"

#include <maya/MProgressWindow.h>


MTypeId ulWrapCache::id( TypeIdBase+01 );
const MString ulWrapCache::typeName( "ulWrapCache" );


bool ulWrapCache::InitMesh(
	MObject meshobj, int subdivstep, bool bBaseInfluence
	)
{
	bValid = true;
	MFnMesh mesh(meshobj);

	int poligoncount = mesh.numPolygons();
	int vertexcount = mesh.numVertices();
	int facevertexcount = mesh.numFaceVertices();
	if( this->poligoncount	== poligoncount &&
		this->vertexcount	== vertexcount &&
		this->facevertexcount == facevertexcount &&
		this->subdivstep	== subdivstep
		)
	{
		if( !bBaseInfluence)
			return false;
	}
	this->poligoncount	= poligoncount;
	this->vertexcount	= vertexcount;
	this->facevertexcount = facevertexcount;
	this->subdivstep	= subdivstep;

	int progressstage = 0;
	
	if( MGlobal::mayaState()==MGlobal::kInteractive)
	{
		MProgressWindow::reserve();
		MProgressWindow::setInterruptable(false);
		MProgressWindow::setTitle("Build binding cache");
		MProgressWindow::startProgress();
		MProgressWindow::setProgressRange(0, 5);
		MProgressWindow::setProgressStatus("...");
	}

	baseverts.clear();
	faces.clear();
	neoverts.clear();
	geometries.clear();
	openedges.clear();

	src_verts.clear();
	src_vertnormals.clear();
	src_facenormals.clear();
	src_squarefaces.clear();
	src_edgenormals.clear();

	std::vector<Math::Vec3f> vertexArray;
	{
		MFloatPointArray _vertexArray;	
		mesh.getPoints(_vertexArray, MSpace::kWorld);
		vertexArray.resize(_vertexArray.length());
		for(unsigned v=0; v<_vertexArray.length(); v++)
			::copy(vertexArray[v], _vertexArray[v]);
	}

	// ������ ����������� �����
	if( MGlobal::mayaState()==MGlobal::kInteractive)
	{
		MProgressWindow::setProgress(progressstage++);
		MProgressWindow::setProgressStatus("������ ����������� �����");
	}

	std::vector<Math::Polygon32> polygons;
	polygons.resize(poligoncount);
	MItMeshPolygon polyIter(meshobj);
	for(polyIter.reset(); !polyIter.isDone(); polyIter.next())
	{
		int polyIndex = polyIter.index();
		MIntArray vertices;
		polyIter.getVertices( vertices);

		Math::Polygon32& f = polygons[polyIndex];
		f.resize(vertices.length());
		for( unsigned x=0; x<vertices.length(); x++)
			f[x] = vertices[x];

	}

	if( subdivstep==0)
	{
		// ��� �������
		if( MGlobal::mayaState()==MGlobal::kInteractive)
		{
			MProgressWindow::setProgress(progressstage++);
			MProgressWindow::setProgressStatus("��� ����������� �����");
		}

		neoverts.resize(vertexArray.size());
		baseverts.resize(vertexArray.size());
		src_verts.resize(vertexArray.size());
		src_vertnormals.resize(vertexArray.size());
		for(unsigned v=0; v<vertexArray.size(); v++)
		{
			src_verts[v] = vertexArray[v];
			neoverts[v].data.resize(1);
			neoverts[v].data[0] = std::pair<int, float>(v, 1.f);
		}
		// ������������
		this->faceNormalCount = poligoncount;
		MItMeshPolygon polyIter(meshobj);
		faces.reserve(poligoncount*3);
		facesnormalindex.reserve(poligoncount*3);
		int p=0;
		for(polyIter.reset(); !polyIter.isDone(); polyIter.next(), p++)
		{
			int tngCount = 0;
			polyIter.numTriangles(tngCount);
			for(int hh = 0; hh<tngCount; hh++)
			{
				MPointArray FaceVertex;
				MIntArray FaceVtxTrngl;
				polyIter.getTriangle(hh, FaceVertex, FaceVtxTrngl);
				Math::Face32 face( FaceVtxTrngl[0], FaceVtxTrngl[1], FaceVtxTrngl[2]);
				faces.push_back(face);
				this->facesnormalindex.push_back(p);
			}
		}
	}
	else
	{
		// ��� ����������� �����
		if( MGlobal::mayaState()==MGlobal::kInteractive)
		{
			MProgressWindow::setProgress(progressstage++);
			MProgressWindow::setProgressStatus("��� ����������� �����");
		}

		std::vector< Math::Quad32> quads;
		Math::Subdiv(subdivstep, &polygons[0], poligoncount, vertexcount, facevertexcount, quads, this->neoverts);
		baseverts.resize(neoverts.size());
		src_verts.resize(neoverts.size());
		src_vertnormals.resize(neoverts.size());

		// ������� ���������
		if( MGlobal::mayaState()==MGlobal::kInteractive)
		{
			MProgressWindow::setProgress(progressstage++);
			MProgressWindow::setProgressStatus("������� ���������");
		}

		for(unsigned v=0; v<baseverts.size(); v++)
		{
			basevertex& bv = baseverts[v];
			Math::Vec3f& vert = src_verts[v];

			// p
			vert = Math::Vec3f(0, 0, 0);
			Math::subdivWeight& sdw = neoverts[v];
			for( unsigned z=0; z<sdw.data.size(); z++)
			{
				Math::Vec3f pt = vertexArray[sdw.data[z].first];
				vert += pt*sdw.data[z].second;
			}
		}

		// ������ �����
		this->faceNormalCount = quads.size();
		this->faces.resize(quads.size()*2);
		this->facesnormalindex.resize(quads.size()*2);
		for( unsigned q=0; q<quads.size(); q++)
		{
			Math::Quad32& quad = quads[q];
			Math::Face32 qfaces[2];
			qfaces[0] = Math::Face32(quad[0], quad[1], quad[2]);
			qfaces[1] = Math::Face32(quad[2], quad[3], quad[0]);

			for(int t=0; t<2; t++)
			{
				Math::Face32& face = qfaces[t];
				int f = q*2+t;
				this->faces[f] = face;
				this->facesnormalindex[f] = q;
			}
		}
	}

	// �������� ��������� � ���������
	if( MGlobal::mayaState()==MGlobal::kInteractive)
	{
		MProgressWindow::setProgress(progressstage++);
		MProgressWindow::setProgressStatus("�������� ��������� � ���������");
	}

	{
		// �������� ��������� � ������
		for( unsigned f=0; f<faces.size(); f++)
		{
			Math::Face32& face = faces[f];
			for( int v=0; v<face.size(); v++)
			{
				basevertex& bv = baseverts[face[v]];
				bv.faces.push_back(f);
			}
		}
	}

	// ����� �������� �����
	{
		std::map<Math::Edge32, int> edges;
		for( unsigned f=0; f<faces.size(); f++)
		{
			Math::Face32& face = faces[f];
			for( int v=0; v<face.size(); v++)
			{
				Math::Edge32 edge(face.cycle(v), face.cycle(v+1));
				if( edges.find(edge) == edges.end())
					edges[edge] = 1;
				else
					edges[edge]++;
			}
		}
		std::map<Math::Edge32, int>::iterator it = edges.begin();
		for(;it != edges.end(); it++)
		{
			if(it->second==1)
			{
				openedge ope;
				ope.edge = it->first;
				openedges.push_back(ope);
			}
		}
		for(unsigned e=0; e<openedges.size(); e++)
		{
			openedge& ope = openedges[e];
			for(unsigned e1=0; e1<openedges.size(); e1++)
			{
				if(e==e1) continue;
				int ind = openedges[e1].edge.opposite(ope.edge.v1);
				if(ind>=0)
					ope.v_1 = ind;
				ind = openedges[e1].edge.opposite(ope.edge.v2);
				if(ind>=0)
					ope.v2_ = ind;
			}
		}
	}

	// ������ �������� � �������� ������
	calcFaceNormals(
		src_verts,
		src_facenormals,	// ������� ��-���
		src_squarefaces		// ������� �����
		);
	// ������� ���������
	calcVertNormals(
		src_facenormals,	// ������� ��-���
		src_vertnormals		// ������� ���������
		);
	// ������� �����
	calcEdgeNormals(
		src_facenormals,	// ������� ��-���
		src_edgenormals		// ������� �����
		);

	geometries.clear();
	if( MGlobal::mayaState()==MGlobal::kInteractive)
	{
		MProgressWindow::setProgress(progressstage++);
		MProgressWindow::endProgress();
	}
	return true;
}

// ������ ���������
void ulWrapCache::calcVerts( 
	MFnMesh& mesh, 
	std::vector<Math::Vec3f>& verts
	)const
{
	unsigned v;

	std::vector<Math::Vec3f> vertexArray;
	{
		MFloatPointArray _vertexArray;
		mesh.getPoints(_vertexArray, MSpace::kWorld);

		vertexArray.resize(_vertexArray.length());
		for(v=0; v<_vertexArray.length(); v++)
			::copy(vertexArray[v], _vertexArray[v]);
	}

	verts.resize(baseverts.size());

	// ������� ���������
	{
		for(v=0; v<verts.size(); v++)
		{
			// p
			Math::Vec3f P(0, 0, 0);
			const Math::subdivWeight& sdw = this->neoverts[v];
			for( unsigned z=0; z<sdw.data.size(); z++)
			{
				Math::Vec3f& pt = vertexArray[sdw.data[z].first];
				P += pt*sdw.data[z].second;
			}
			verts[v] = P;
		}
	}
}

// ������ �������� �����	
void ulWrapCache::calcEdgeNormals(
	std::vector<Math::Vec3f>& facenormals,	// ������� ��-���
	std::map< Math::Edge32, Math::Vec3f>& edgenormals	// ������� �����
	)const
{
	edgenormals.clear();
	for( unsigned f=0; f<this->faces.size(); f++)
	{
		const Math::Face32& face = this->faces[f];
		int fn = this->facesnormalindex[f];
		Math::Vec3f& norm = facenormals[fn];
		for( int v=0; v<face.size(); v++)
		{
			Math::Edge32 edge(face.cycle(v), face.cycle(v+1));
			if( edgenormals.find(edge) == edgenormals.end())
				edgenormals[edge] = Math::Vec3f(0, 0, 0);
			edgenormals[edge] += norm;
		}
	}
	std::map< Math::Edge32, Math::Vec3f>::iterator it = edgenormals.begin();
	for( ; it != edgenormals.end(); it++)
	{
		it->second.normalize();
	}
}

// ������ �������� ���������
void ulWrapCache::calcVertNormals(
	std::vector<Math::Vec3f>& facenormals,	// ������� ��-���
	std::vector<Math::Vec3f>& vertnormals		// ������� �����
	)const
{
	vertnormals.resize(this->baseverts.size());
	for(unsigned v=0; v<this->baseverts.size(); v++)
	{
		const basevertex& bv = this->baseverts[v];
		Math::Vec3f& vn = vertnormals[v];
		vn = Math::Vec3f(0, 0, 0);
		for(unsigned fi=0; fi<bv.faces.size(); fi++)
		{
			int f = bv.faces[fi];
			int fn = this->facesnormalindex[f];
			Math::Vec3f n = facenormals[fn];
			vn += n;
		}
		vn.normalize();
	}
}

// ������ �������� � �������� ������
void ulWrapCache::calcFaceNormals(
	std::vector<Math::Vec3f>& verts,		// ������� ���������
	std::vector<Math::Vec3f>& facenormals,	// ������� ��-���
	std::vector< float>& squarefaces		// ������� �����
	)const
{
	squarefaces.resize(faces.size());
	facenormals.resize( this->faceNormalCount, Math::Vec3f(0, 0, 0));
	for( unsigned f=0; f<this->faces.size(); f++)
	{
		const Math::Face32& face = this->faces[f];

		Math::Vec3f v1 = verts[face[1]] - verts[face[0]];
		Math::Vec3f v2 = verts[face[2]] - verts[face[0]];
		Math::Vec3f n = Math::cross(v1, v2);
		squarefaces[f] = n.length();
		facenormals[this->facesnormalindex[f]] += n.normalized();
	}
	for( unsigned fn=0; fn<facenormals.size(); fn++)
		facenormals[fn].normalize();
}

bool ulWrapCache::InitTarget(
	int multiIndex, 
	MItGeometry& iter, 
	const MMatrix& mat, 
	bool bInteractiveInfluence)
{
	bool bInitMatrix = geometries.find(multiIndex)==geometries.end();
	geometryInfluence& gi = geometries[multiIndex];
//	if( bInitMatrix)
		::copy( gi.worldMatrix, mat);


	// ������ �������� ������� ���������
	int p=0;
	for ( iter.reset(); !iter.isDone(); iter.next(), p++) 
	{
		MPoint _pt = iter.position();
		Math::Vec3f pt; ::copy(pt, _pt);

		int index = iter.index();
		if( gi.influences.find(index)!=gi.influences.end() && !bInteractiveInfluence)
			continue;
		pt = gi.worldMatrix*pt;
		pointInfluence& pi = gi.influences[index];
		if( pi.source_pt == pt)
			continue;

		pi.source_pt = pt;
		pi.reset();
//		pt = gi.worldMatrix*pt;

		projectionPoint(pt, pi, src_verts, src_vertnormals, src_edgenormals);
	}
	return true;
}

// �������� �����
void ulWrapCache::projectionPoint(
	Math::Vec3f pt,							// world space
	pointInfluence& pi,						// ���������
	std::vector<Math::Vec3f>& verts,					// ������� ���������
	std::vector<Math::Vec3f>& vertnormals,				// ������� ���������
	std::map< Math::Edge32, Math::Vec3f>& edgenormals	// ������� �����
	) const
{
	float minDist = 1e38f, minDistEdge = 1e38f;
	Math::Face32 minFace, minFaceEdge;
	Math::Vec3f minBary, minBaryEdge;
	bool bFind = false, bFindEdge = false;

	for(unsigned f=0; f<faces.size(); f++)
	{
		const Math::Face32& face = faces[f];
		// ������� �����
		// dot �� ��������� � ����� �����
		float hs[3], esq[3], sumsq=0;
		for(int v=0; v<3; v++)
		{
			// ������������ ����� v, v+1
			Math::Edge32 edge(face.cycle(v), face.cycle(v+1));
			Math::Vec3f& en = edgenormals[edge];
			Math::Vec3f& ev = verts[face.cycle(v+1)] - verts[face.cycle(v)];
			Math::Vec3f platen = Math::cross(en, ev).normalized();
			Math::Vec3f pv = pt - verts[face.cycle(v)];
			float h = Math::dot(platen, pv);
			if( fabs(h)<1e-5) h=0;
			hs[v] = h;
			esq[v] = ev.length()*h;
			sumsq += esq[v];
		}
		if( (hs[0]<=0 && hs[1]<=0 && hs[2]<=0) ||
			(hs[0]>=0 && hs[1]>=0 && hs[2]>=0))
		{
			// ������ ��������
			Math::Vec3f bary(esq[1]/sumsq, esq[2]/sumsq, esq[0]/sumsq);
            // ��������
			Math::Vec3f pr = bary[0]*verts[face[0]] + bary[1]*verts[face[1]] + bary[2]*verts[face[2]];
			float dist = (pr - pt).length();
			if( dist < minDist)
			{
				minDist = dist;
				minFace = face;
				minBary = bary;
				bFind = true;
			}
		}
	}
	// ���������� �� �������� �����
	{
		for( unsigned e=0; e<openedges.size(); e++)
		{
			const openedge& ope = openedges[e];
			const Math::Edge32& edge = ope.edge;

			Math::Vec3f& v1 = verts[edge.v1];
			Math::Vec3f& v2 = verts[edge.v2];
			Math::Vec3f n1 = vertnormals[edge.v1];
			Math::Vec3f n2 = vertnormals[edge.v2];
			Math::Vec3f v_1 = v1 - verts[ope.v_1];
			Math::Vec3f v2_ = verts[ope.v2_] - v2;
			Math::Vec3f v12 = v2-v1;
			Math::Vec3f v1X = v12.normalized()+v_1.normalized();
			Math::Vec3f v2X = v12.normalized()+v2_.normalized();
			
			Math::Vec3f plate1 = Math::cross( Math::cross(v1X, n1), n1).normalized();
			Math::Vec3f plate2 = -Math::cross( Math::cross(v2X, n2), n2).normalized();

			float w1 = Math::dot(plate1, pt-v1);
			float w2 = Math::dot(plate2, pt-v2);
			if( fabs(w1)<1e-5) w1=0;
			if( fabs(w2)<1e-5) w2=0;

			if( (w1>=0 && w2>=0) || (w1<=0 && w2<=0))
			{
				float param = w1/(w2+w1);

				Math::Vec3f pr = v1*(1-param) + v2*param;
				float dist = (pr - pt).length();
				if( dist < minDistEdge)
				{
					minDistEdge = dist;
					minFaceEdge = Math::Face32(edge.v1, edge.v2, 0);
					minBaryEdge = Math::Vec3f(1-param, param, 0);
					bFindEdge = true;
				}
			}
		}
	}

	if( minDistEdge<minDist)
		bFind = false;
	if( !bFind)
		minDistEdge = 0.f;
	if( bFindEdge)
	{
		pi.distanceToEdge = minDistEdge;
		pi.basepointsEdge.push_back( pointToBasePoint( minFaceEdge[0], minBaryEdge[0]));
		pi.basepointsEdge.push_back( pointToBasePoint( minFaceEdge[1], minBaryEdge[1]));
	}
	if( bFind)
	{
		pi.basepoints.push_back( pointToBasePoint( minFace[0], minBary[0]));
		pi.basepoints.push_back( pointToBasePoint( minFace[1], minBary[1]));
		pi.basepoints.push_back( pointToBasePoint( minFace[2], minBary[2]));
	}
}


bool ulWrapCache::getDeformations(
	MObject meshobj,			// ��������������� �����
	const Math::Matrix4f& controlToBase,			// �������������� � ������������ base �����������
	std::vector<Math::Vec3f>& src_verts,			// �������� ��������
	std::vector<Math::Vec3f>& src_vertnormals,		// �������� ������� ���������
	std::vector< float>& src_squarefaces,			// �������� ������� ������
	std::vector<basevertexdeform>& basedeform)
{
	unsigned v;

	MFnMesh mesh(meshobj);
	std::vector<Math::Vec3f> verts;
	calcVerts(mesh, verts);
	// ��������� controlToBase
	for(v=0; v<verts.size(); v++)
	{
		Math::Vec3f& pt = verts[v];
		pt = controlToBase*pt;
	}

	// ������� � ������� ������ 
	std::vector<Math::Vec3f> norms;
	std::vector<float> deformfactor;
	calcFaceNormals( verts, norms, deformfactor);
	for(unsigned f=0; f<faces.size(); f++)
	{
		float sq = deformfactor[f];
		deformfactor[f] = sqrt( sq/src_squarefaces[f]);
	}

	basedeform.resize(baseverts.size());
	// ������� ���������
	{
		for(v=0; v<basedeform.size(); v++)
		{
			basevertexdeform& bd = basedeform[v];
			// ��������
			bd.offset = verts[v] - src_verts[v];

			basevertex& bv = baseverts[v];
			Math::Vec3f N(0, 0, 0);
			bd.deformationfactor = 0;
			for(unsigned f=0; f<bv.faces.size(); f++)
			{
				int gf = bv.faces[f];
				int fn = facesnormalindex[gf];
				N += norms[fn];
				bd.deformationfactor += deformfactor[gf];
			}
			bd.deformationfactor /= bv.faces.size();
			N.normalize();

			Math::Rot3f rotate(src_vertnormals[v], N);
			bd.rotate = rotate;
		}
	}
	return true;
}

// ������ ��� ���������� ��������
void ulWrapCache::getPointDeformation(
	Math::Vec3f& project,				// �������� �����
	Math::Vec3f& projectbase,			// �������� �����
	Math::Vec3f& offset,				// ��������
	Math::Rot3f& rotate,				// ��������
	float& deformfactor,				// ���. �������� ����������
	const pointInfluence& pi,						// ��������
	const std::vector<basevertexdeform>& basedeform,	// ������ ��� ����������
	const std::vector<Math::Vec3f>& verts,		// ������� ���������
	float wireDistance
	) const
{
	offset = Math::Vec3f(0, 0, 0);
	rotate = Math::Rot3f(0, 0, 0, 0);
	deformfactor = 0;

	// � �����������
	project = Math::Vec3f(0, 0, 0);
	projectbase = Math::Vec3f(0, 0, 0);
	for( unsigned iv=0; iv<pi.basepoints.size(); iv++)
	{
		const pointToBasePoint& ptbp = pi.basepoints[iv];
		const basevertexdeform& bvd = basedeform[ptbp.basevertex];
//		const basevertex& bv = baseverts[ptbp.basevertex];
		const Math::Vec3f& vert = verts[ptbp.basevertex];

		Math::Rot3f r = bvd.rotate.normalized();
		rotate += r*ptbp.weight;
		offset += bvd.offset*ptbp.weight;
		project += (vert + bvd.offset) * ptbp.weight;
		projectbase += (vert) * ptbp.weight;
		deformfactor += bvd.deformationfactor * ptbp.weight;
	}
	// � �����
	if(pi.distanceToEdge<wireDistance || pi.distanceToEdge==0.f)
	{
		float distWeigth = 0;
		if( wireDistance!=0)
			distWeigth = sqrt((wireDistance-pi.distanceToEdge)/wireDistance);

		if( pi.distanceToEdge==0.f)
			distWeigth = 1;
		if( pi.basepointsEdge.empty()) 
			distWeigth = 1;

		distWeigth = 1-distWeigth;
		Math::Vec3f projectEdge(0, 0, 0);				// �������� �����
		Math::Vec3f projectbaseEdge(0, 0, 0);			// �������� �����
		Math::Vec3f offsetEdge(0, 0, 0);				// ��������
		Math::Rot3f rotateEdge(0, 0, 0, 0);						// ��������
		float deformfactorEdge = 0;

		for( unsigned iv=0; iv<pi.basepointsEdge.size(); iv++)
		{
			const pointToBasePoint& ptbp = pi.basepointsEdge[iv];
			const basevertexdeform& bvd = basedeform[ptbp.basevertex];
	//		const basevertex& bv = baseverts[ptbp.basevertex];
			const Math::Vec3f& vert = verts[ptbp.basevertex];

			Math::Rot3f r = bvd.rotate.normalized();
			rotateEdge += r*ptbp.weight;
			offsetEdge += ptbp.weight*bvd.offset;
			projectEdge += (vert + bvd.offset) * ptbp.weight;
			projectbaseEdge += (vert) * ptbp.weight;
			deformfactorEdge += bvd.deformationfactor * ptbp.weight;
		}
		project		= project*distWeigth +		projectEdge*(1-distWeigth);
		projectbase = projectbase*distWeigth +	projectbaseEdge*(1-distWeigth);
		offset		= offset*distWeigth +		offsetEdge*(1-distWeigth);
		rotate		= rotate*distWeigth +		rotateEdge*(1-distWeigth);
		deformfactor= deformfactor*distWeigth + deformfactorEdge*(1-distWeigth);
	}
}

// ���������� ��������
Math::Vec3f ulWrapCache::deformPoint(
	Math::Vec3f& pt,						// �����
	const Math::Vec3f& project,				// �������� �����
	const Math::Vec3f& projectbase,			// �������� �����
	const Math::Vec3f& offset,				// ��������
	const Math::Rot3f& rotate,				// ��������
	float envelope
	)const
{
	bool bRotation = true;
	bool bOffset = true;

	Math::Vec3f res_de = pt;
	if( bRotation)
	{
//		rotate.normalize();
		Math::Matrix4f m;
		rotate.getMatrix(m);

		Math::Vec3f dir = pt - projectbase;
		Math::Vec3f dirR = m*dir;
		Math::Vec3f s = (dirR-dir);
		res_de += s*envelope;
	}
	if( bOffset)
	{
		res_de += offset*envelope;
	}
	return res_de;
}



ulWrapCache::ulWrapCache()
{
	bValid = false;
}
ulWrapCache::~ulWrapCache()
{
}

MTypeId ulWrapCache::typeId() const
{
	return ulWrapCache::id;
}

MString ulWrapCache::name() const
{ 
	return ulWrapCache::typeName; 
}

void* ulWrapCache::creator()
{
	return new ulWrapCache();
}

void ulWrapCache::copy( const MPxData& other )
{
	const ulWrapCache* arg = (const ulWrapCache*)&other;

	this->bValid			= arg->bValid;

	this->poligoncount = arg->poligoncount;
	this->vertexcount = arg->vertexcount;
	this->facevertexcount = arg->facevertexcount;
	this->subdivstep = arg->subdivstep;

	this->baseverts			= arg->baseverts;
	this->faces				= arg->faces;
	this->faceNormalCount	= arg->faceNormalCount;
	this->facesnormalindex	= arg->facesnormalindex;

	this->openedges			= arg->openedges;
	this->neoverts			= arg->neoverts;

	this->src_verts			= arg->src_verts;		
	this->src_vertnormals	= arg->src_vertnormals;
	this->src_facenormals	= arg->src_facenormals;
	this->src_squarefaces	= arg->src_squarefaces;
	this->src_edgenormals	= arg->src_edgenormals;

	this->geometries		= arg->geometries;
}

MStatus ulWrapCache::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("ulWrapCache: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus ulWrapCache::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus ulWrapCache::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus ulWrapCache::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void ulWrapCache::serialize(Util::Stream& stream)
{
	int version = 2;
	stream >> version;
	if( version>=2)
	{
		stream >> this->poligoncount;
		stream >> this->vertexcount;
		stream >> this->facevertexcount;
		stream >> this->subdivstep;

		stream >> this->baseverts;
		stream >> this->faces;				
		stream >> this->faceNormalCount;	
		stream >> this->facesnormalindex;	

		stream >> this->openedges;
		stream >> this->neoverts;

		stream >> this->src_verts;
		stream >> this->src_vertnormals;
		stream >> this->src_facenormals;
		stream >> this->src_squarefaces;

		stream >> this->geometries;
		bValid = true;
	}
}

