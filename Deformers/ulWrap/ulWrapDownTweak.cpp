//
// Copyright (C) 
// File: ulWrapDownTweakCmd.cpp
// MEL Command: ulWrapDownTweak

#include "ulWrapDownTweak.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include "ulWrap.h"

ulWrapDownTweak::ulWrapDownTweak()
{
}
ulWrapDownTweak::~ulWrapDownTweak()
{
}

MSyntax ulWrapDownTweak::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);	// up tweak				tweak.vlist[0].vertex
	syntax.addArg(MSyntax::kString);	// up tweak	geom		tweak.geometry[0]	
	syntax.addArg(MSyntax::kString);	// ulWrap deformer
	syntax.addArg(MSyntax::kString);	// down tweak			tweak.vlist[0].vertex	
	syntax.addArg(MSyntax::kString);	// down tweak geometry	tweak.geometry[0]	
	return syntax;
}

MStatus ulWrapDownTweak::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString str_up_tweak, str_up_tweak_geom, str_ulWrap, str_down_tweak, str_down_tweak_geom;
	argData.getCommandArgument(0, str_up_tweak);
	argData.getCommandArgument(1, str_up_tweak_geom);
	argData.getCommandArgument(2, str_ulWrap);
	argData.getCommandArgument(3, str_down_tweak);
	argData.getCommandArgument(4, str_down_tweak_geom);

	MPlug up_tweak, up_tweak_geom, wrapOutput, down_tweak, down_tweak_geom;
	::plugFromName(str_up_tweak,		up_tweak);
	::plugFromName(str_up_tweak_geom,	up_tweak_geom);
	::plugFromName(str_ulWrap,			wrapOutput);
	::plugFromName(str_down_tweak,		down_tweak);
	::plugFromName(str_down_tweak_geom, down_tweak_geom);

	MObject wrapobj = wrapOutput.node();
	MFnDependencyNode dn(wrapobj);
	MTypeId tid = dn.typeId();
	if( tid != ulWrap::id)
		return MS::kFailure;
	ulWrap* wrap = (ulWrap*)dn.userNode();
//	wrap->DownTweak(up_tweak, up_tweak_geom, wrapOutput, down_tweak, down_tweak_geom);

	return MS::kSuccess;
}

void* ulWrapDownTweak::creator()
{
	return new ulWrapDownTweak();
}

