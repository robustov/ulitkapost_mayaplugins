#pragma once
#include "pre.h"

//
// Copyright (C) 
// File: ulWrapNode.h
//
// Deformer Node: ulWrap

#include <maya/MPxNode.h>
#include <maya/MPxDeformerNode.h>
#include <maya/MDataBlock.h>
#include <maya/MPlug.h>
#include <maya/MItGeometry.h>
#include <maya/MMatrix.h>

#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include "Math/Subdiv.h"
#include "ulWrapCache.h"
#include "ulWrapDeformation.h"

// ulWrap
class ulWrap : public MPxDeformerNode
{
public:
	ulWrap();
	virtual	~ulWrap(); 

	virtual MStatus compute( 
		const MPlug& plug,
		MDataBlock& dataBlock );

    virtual MStatus deform(
		MDataBlock& data,
		MItGeometry& iter,
		const MMatrix& mat,
		unsigned int multiIndex);

	/*/
	MStatus DownTweak(
		MPlug up_tweak, MPlug up_tweak_geom, 
		MPlug wrapOutput, 
		MPlug down_tweak, MPlug down_tweak_geom);
	/*/


	static void* creator();
	static MStatus initialize();

	ulWrapCache* Load_Cache(MDataBlock& data);
	MStatus Build_Cache(const MPlug& plug, MDataBlock& data);

	ulWrapDeformation* Load_Deformation(MDataBlock& data);
	MStatus Build_Deformation(const MPlug& plug, MDataBlock& data);



public:
	static MObject i_controlMesh;
	static MObject i_controlMeshMatrix;
	static MObject i_controlMeshBase;
	static MObject i_controlMeshBaseMatrix;
	static MObject i_subdivSteps;
	static MObject i_cache;
	static MObject i_deformation;

	static MObject i_bProjection;
//	static MObject i_bRotation;
//	static MObject i_bOffset;

	// ������������� ��� ��������� ������� ��������� �������� �����
	static MObject i_bInteractiveInfluence;
	// ������������� ��� ��������� ������� ��������� ������� ����������� �����
	static MObject i_bBaseInfluence;

	// ��������� wire ���������
	static MObject i_wireDistance;		// ��� �������������� ����������� �����
	static MObject i_wireTwist;			// ����� ��������
	static MObject i_wireScale;			// ����� scale

	// blend
	static MObject i_blendMesh;
	static MObject i_mainMeshDeformFactor;
	static MObject i_blendMeshDeformFactor;

	// �������������� ������
	static MObject o_handles;
		static MObject ih_o_vertindex;
		static MObject ih_o_translate;
		static MObject ih_o_rotate;
		static MObject ih_o_scale;
	static MObject i_handles;
		static MObject ih_i_rotate;
		static MObject ih_i_scale;

public:
	static MTypeId id;
};

