#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ulWrapDownTweakCmd.h
// MEL Command: ulWrapDownTweak

#include <maya/MPxCommand.h>

class ulWrapDownTweak : public MPxCommand
{
public:
	ulWrapDownTweak();
	virtual	~ulWrapDownTweak();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

