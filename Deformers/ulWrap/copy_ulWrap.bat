set BIN_DIR=.\out\ulWrap
set BIN_DIR=\\server\bin70

mkdir %BIN_DIR%
mkdir %BIN_DIR%\bin
mkdir %BIN_DIR%\mel
mkdir %BIN_DIR%\slim
mkdir %BIN_DIR%\docs

copy Bin\ulWrap.mll								%BIN_DIR%\bin

copy Mel\ulDeformersMenu.mel					%BIN_DIR%\mel
copy Mel\ulWrapMenu.mel							%BIN_DIR%\mel
copy Mel\AEulWrapTemplate.mel					%BIN_DIR%\mel

pause