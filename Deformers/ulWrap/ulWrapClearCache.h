#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ulWrapClearCacheCmd.h
// MEL Command: ulWrapClearCache

#include <maya/MPxCommand.h>

class ulWrapClearCache : public MPxCommand
{
public:
	ulWrapClearCache();
	virtual	~ulWrapClearCache();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

