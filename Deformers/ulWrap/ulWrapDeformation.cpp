//
// Copyright (C) 
// File: ulWrapDeformationCmd.cpp
// MEL Command: ulWrapDeformation

#include "ulWrapDeformation.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <sstream>


MTypeId ulWrapDeformation::id( TypeIdBase+02 );
const MString ulWrapDeformation::typeName( "ulWrapDeformation" );

ulWrapDeformation::ulWrapDeformation()
{
	bValid = false;
}
ulWrapDeformation::~ulWrapDeformation()
{
}

MTypeId ulWrapDeformation::typeId() const
{
	return ulWrapDeformation::id;
}

MString ulWrapDeformation::name() const
{ 
	return ulWrapDeformation::typeName; 
}

void* ulWrapDeformation::creator()
{
	return new ulWrapDeformation();
}

void ulWrapDeformation::copy( const MPxData& other )
{
	const ulWrapDeformation* arg = (const ulWrapDeformation*)&other;
//	this->data = arg->data;
	this->bValid		= arg->bValid;
	this->basedeform	= arg->basedeform;
}

MStatus ulWrapDeformation::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("ulWrapDeformation: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus ulWrapDeformation::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus ulWrapDeformation::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus ulWrapDeformation::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void ulWrapDeformation::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		stream >> this->basedeform;
		this->bValid = true;
	}
}

