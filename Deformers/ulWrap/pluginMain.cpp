#include "pre.h"

#include <maya/MFnPlugin.h>
#include "ulWrap.h"
#include "ulWrapCache.h"
#include "ulWrapDownTweak.h"
#include "ulWrapClearCache.h"
#include "ulWrapDeformation.h"

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

	stat = plugin.registerData( 
		"ulWrapDeformation", 
		ulWrapDeformation::id, 
		ulWrapDeformation::creator);
	if (!stat) 
	{
		displayString("cant register node ulWrapDeformation");
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerCommand( "ulWrapClearCache", ulWrapClearCache::creator, ulWrapClearCache::newSyntax );
	if (!stat) {
		displayString("cant register node ulWrapClearCache");
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerCommand( "ulWrapDownTweak", ulWrapDownTweak::creator, ulWrapDownTweak::newSyntax );
	if (!stat) {
		displayString("cant register node ulWrapDownTweak");
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerData( 
		"ulWrapCache", 
		ulWrapCache::id, 
		ulWrapCache::creator);
	if (!stat) 
	{
		displayString("cant register node ulWrapCache");
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerNode( "ulWrap", ulWrap::id, ulWrap::creator, ulWrap::initialize, MPxNode::kDeformerNode );
	if (!stat) {
		displayString("cant register node ulWrap");
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterData( ulWrapDeformation::id );
	if (!stat) displayString("cant deregister ulWrapDeformation");

	stat = plugin.deregisterCommand( "ulWrapClearCache" );
	if (!stat) displayString("cant deregister ulWrapClearCache");

	stat = plugin.deregisterCommand( "ulWrapDownTweak" );
	if (!stat) displayString("cant deregister ulWrapDownTweak");

	stat = plugin.deregisterData( ulWrapCache::id );
	if (!stat) displayString("cant deregister ulWrapCache");

	stat = plugin.deregisterNode( ulWrap::id );
	if (!stat) displayString("cant deregister ulWrap");

	return stat;
}
