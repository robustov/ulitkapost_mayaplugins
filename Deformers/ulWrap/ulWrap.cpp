//
// Copyright (C) 
// 
// File: ulWrapNode.cpp
//
// Dependency Graph Node: ulWrap
//
// Author: Maya Plug-in Wizard 2.0
//

#include "ulWrap.h"

#include "pre.h"
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MPoint.h>
#include <maya/MFnMesh.h>
#include <maya/MFloatPointArray.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnComponent.h>
#include <maya/MFnCompoundAttribute.h>

#include <maya/MGlobal.h>
#include "Util/EvalTime.h"

// You MUST change this to a unique value!!!  The id is a 32bit value used
// to identify this type of node in the binary file format.  
//
MTypeId ulWrap::id( TypeIdBase+00 );

MObject ulWrap::i_controlMesh;
MObject ulWrap::i_controlMeshMatrix;
MObject ulWrap::i_controlMeshBase;
MObject ulWrap::i_controlMeshBaseMatrix;
MObject ulWrap::i_subdivSteps;
MObject ulWrap::i_cache;
MObject ulWrap::i_deformation;
MObject ulWrap::i_bProjection;
//MObject ulWrap::i_bRotation;
//MObject ulWrap::i_bOffset;
MObject ulWrap::i_bInteractiveInfluence;
MObject ulWrap::i_bBaseInfluence;

// ��������� wire ���������
MObject ulWrap::i_wireDistance;		// ��� �������������� ����������� �����
MObject ulWrap::i_wireTwist;		// ����� ��������
MObject ulWrap::i_wireScale;		// ����� scale

// blend
MObject ulWrap::i_blendMesh;
MObject ulWrap::i_mainMeshDeformFactor;
MObject ulWrap::i_blendMeshDeformFactor;

// �������������� ������
MObject ulWrap::o_handles;
	MObject ulWrap::ih_o_vertindex;
	MObject ulWrap::ih_o_translate;
	MObject ulWrap::ih_o_rotate;
	MObject ulWrap::ih_o_scale;
MObject ulWrap::i_handles;
	MObject ulWrap::ih_i_rotate;
	MObject ulWrap::ih_i_scale;

ulWrap::ulWrap()
{
}
ulWrap::~ulWrap()
{
}

/*/
// connectAttr -f tweak2.vlist[0].vertex[0] pPlaneShape2.tweakLocation
// ulWrapDownTweak tweak2.vlist[0].vertex tweak2.outputGeometry[0] ulWrap1.input[0] tweak1.vlist[0].vertex tweak1.outputGeometry[0]
MStatus ulWrap::DownTweak(
	MPlug up_tweak, MPlug up_tweak_geom, 
	MPlug wrapOutput, 
	MPlug down_tweak, MPlug down_tweak_geom)
{
	MStatus stat;
	int multiIndex = wrapOutput.logicalIndex();
	MDataBlock data = forceCache();

	MObject meshobj = data.inputValue( this->i_controlMesh, &stat ).asMesh();
	MObject meshbaseobj = data.inputValue( this->i_controlMeshBase, &stat ).asMesh();
	

	ulWrapCache* cache = Load_Cache(data);
	if( !cache) return MS::kFailure;
//	cache->InitTarget(multiIndex, iter, m);

	std::vector<Math::Vec3f> verts;		// ������� ���������
	std::vector<Math::Vec3f> vertnormals;	// ������� ���������
	std::map< Math::Edge32, Math::Vec3f> edgenormals;	// ������� �����
	std::vector<Math::Vec3f> facenormals;	// ������� ��-���
	std::vector< float>		 squarefaces;	// ������� �����

	// ������� ���������
	std::map<int, Math::Vec3f> inputGeometry;
	{
		geometryInfluence& gi = cache->geometries[multiIndex];
		std::map<int, pointInfluence>::iterator it = gi.influences.begin();
		for(;it != gi.influences.end(); it++)
			inputGeometry[it->first] = it->second.source_pt;
	}

	// �������������� ���������
	std::map<int, Math::Vec3f> outputGeometry;
	{
		displayString("%s", up_tweak_geom.name().asChar());
		MItGeometry iter(data.inputValue( up_tweak_geom));
		for( iter.reset(); !iter.isDone(); iter.next())
		{
			int index = iter.index();
			MPoint pt = iter.position();
			displayString("%d {%f %f %f}", iter.index(), pt.x, pt.y, pt.z);
			copy( outputGeometry[index], pt);
		}
	}

	// ������ ���������
	MFnMesh mesh(meshobj);
	cache->calcVerts(
		mesh, 
		verts);

	// ������ �������� � �������� ������
	cache->calcFaceNormals(
		verts,
		facenormals,	// ������� ��-���
		squarefaces		// ������� �����
		);
	// ������� ���������
	cache->calcVertNormals(
		facenormals,	// ������� ��-���
		vertnormals		// ������� ���������
		);
	// ������� �����
	cache->calcEdgeNormals(
		facenormals,	// ������� ��-���
		edgenormals		// ������� �����
		);

	std::vector<basevertexdeform> inv_basedeform;
	cache->getDeformations( meshbaseobj, verts, vertnormals, squarefaces, inv_basedeform);

	displayString("%s", up_tweak.name().asChar());
	for( unsigned i=0; i<up_tweak.numElements(); i++)
	{
		MPlug src = up_tweak.elementByPhysicalIndex(i);
		int index = src.logicalIndex();

		Math::Vec3f pt = outputGeometry[index]; 
		Math::Vec3f pt_in = inputGeometry[index]; 
//		pt = pt*world;
		pointInfluence pi;
		cache->projectionPoint(pt, pi, verts, vertnormals, edgenormals);
		Math::Vec3f project = pt, projectbase = pt;
		Math::Vec3f offset;
		Math::Rot3f rotate;
		float deformfactor;
		cache->getPointDeformation(
			project, projectbase, offset, rotate, deformfactor, 
			pi, inv_basedeform, verts, 0);
//		offset = -offset;
//		rotate = rotate*(-1);
//		std::swap(project, projectbase);
		Math::Vec3f pt_res = cache->deformPoint(
			pt, project, projectbase, offset, rotate, 1);
		pt_res = pt_res - pt_in;

		MPlug dst = down_tweak.elementByLogicalIndex(index);
		MObject val;
		dst.getValue(val);
		MFnNumericData nd(val);
		Math::Vec3f currentoffset;
		nd.getData(currentoffset.x, currentoffset.y, currentoffset.z);
		pt_res += currentoffset;
		val = nd.create(MFnNumericData::k3Float);
		nd.setData(pt_res.x, pt_res.y, pt_res.z);
		dst.setValue(val);

		val = nd.create(MFnNumericData::k3Float);
		nd.setData(0.f, 0.f, 0.f);
		src.setValue(val);

//		MPlug dst = down_tweak.elementByLogicalIndex(p.logicalIndex());
//		MFnNumericData dst_nd(dst);
//		dst_nd.setData(v.x, v.y, v.z);
//		dst_nd.getData(v.x, v.y, v.z);
	}

	displayString("%s", down_tweak.name().asChar());
	for( i=0; i<down_tweak.numElements(); i++)
	{
		MPlug p = down_tweak.elementByPhysicalIndex(i);
		MObject up_val;
		p.getValue(up_val);
		MFnNumericData nd(up_val);
		MFloatVector v;
		nd.getData(v.x, v.y, v.z);
		displayString( "%d %s {%f %f %f}", p.logicalIndex(), up_val.apiTypeStr(), v.x, v.y, v.z);
	}

//	MFn
	return MS::kSuccess;
}
/*/

ulWrapCache* ulWrap::Load_Cache(MDataBlock& data)
{
	MStatus _Status, stat;
	MDataHandle inputData = data.inputValue(i_cache, &stat);
	MPxData* pxdata = inputData.asPluginData();
	ulWrapCache* hgh = (ulWrapCache*)pxdata;
	if( !hgh || !hgh->bValid)
	{
		Build_Cache( MPlug(thisMObject(), i_cache), data);
		MDataHandle inputData = data.inputValue(i_cache, &stat);
		MPxData* pxdata = inputData.asPluginData();
		ulWrapCache* hgh = (ulWrapCache*)pxdata;
		return hgh;
	}
	return hgh;
}

ulWrapDeformation* ulWrap::Load_Deformation(MDataBlock& data)
{
	MStatus _Status, stat;
	MDataHandle inputData = data.inputValue(i_deformation, &stat);
	MPxData* pxdata = inputData.asPluginData();
	ulWrapDeformation* hgh = (ulWrapDeformation*)pxdata;
	if( !hgh || !hgh->bValid)
	{
		Build_Deformation( MPlug(thisMObject(), i_deformation), data);
		MDataHandle inputData = data.inputValue(i_deformation, &stat);
		MPxData* pxdata = inputData.asPluginData();
		ulWrapDeformation* hgh = (ulWrapDeformation*)pxdata;
		return hgh;
	}
	return hgh;
}

MStatus ulWrap::Build_Cache(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;

	ulWrapCache* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( i_cache, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<ulWrapCache*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( ulWrapCache::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<ulWrapCache*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	int subdivstep = data.inputValue( this->i_subdivSteps, &stat).asInt();
	bool bBaseInfluence = data.inputValue( this->i_bBaseInfluence, &stat).asBool();
	bBaseInfluence = bBaseInfluence|bNewData;
	MObject meshobj = data.inputValue( this->i_controlMeshBase, &stat ).asMesh();
	pData->InitMesh(meshobj, subdivstep, bBaseInfluence);

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

MStatus ulWrap::Build_Deformation(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;

	ulWrapDeformation* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( i_deformation, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<ulWrapDeformation*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( ulWrapDeformation::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<ulWrapDeformation*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	ulWrapCache* cache = Load_Cache(data);
	if( !cache) return MS::kFailure;
	
	MMatrix cmBaseSpace = MFnMatrixData(data.inputValue(this->i_controlMeshBaseMatrix).data()).matrix();
	MMatrix cmSpace = MFnMatrixData(data.inputValue(this->i_controlMeshMatrix).data()).matrix();
	MMatrix _controlToBase = cmSpace * cmBaseSpace.inverse();
	Math::Matrix4f controlToBase;
	::copy( controlToBase, _controlToBase);
	
	MObject meshobj = data.inputValue( this->i_controlMesh, &stat ).asMesh();

	// ������ ������������� ����������� �����
	pData->basedeform.clear();
	cache->getDeformations(meshobj, controlToBase, cache->src_verts, cache->src_vertnormals, cache->src_squarefaces, pData->basedeform);
	pData->bValid = true;

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}


MStatus ulWrap::compute( 
	const MPlug& plug,
	MDataBlock& dataBlock )
{
	displayStringD( "ulWrap::compute %s", plug.name().asChar());
	if(plug==i_cache)
		return Build_Cache(plug, dataBlock);
	if(plug==i_deformation)
		return Build_Deformation(plug, dataBlock);
	return MPxDeformerNode::compute(plug, dataBlock);
}

MStatus ulWrap::deform(
	MDataBlock& data,
	MItGeometry& iter,
	const MMatrix& m,
	unsigned int multiIndex)
{
	MStatus stat;
	MMatrix m_inv = m.inverse();
 
	float envelope = data.inputValue(this->envelope,&stat).asFloat();
	if( !envelope) return MS::kSuccess;
	bool bProjection = data.inputValue(this->i_bProjection,&stat).asBool();
//	bool bRotation = data.inputValue(this->i_bRotation,&stat).asBool();
//	bool bOffset = data.inputValue(this->i_bOffset,&stat).asBool();
	bool bInteractiveInfluence = data.inputValue(this->i_bInteractiveInfluence,&stat).asBool();

	float wireDistance = (float)data.inputValue(this->i_wireDistance,&stat).asDouble();
	float wireTwist = (float)data.inputValue(this->i_wireTwist,&stat).asDouble();
	float wireScale = (float)data.inputValue(this->i_wireScale,&stat).asDouble();

	MMatrix cmBaseSpace = MFnMatrixData(data.inputValue(this->i_controlMeshBaseMatrix).data()).matrix();
	MMatrix cmSpace = MFnMatrixData(data.inputValue(this->i_controlMeshMatrix).data()).matrix();
	MMatrix cmBaseSpace_inv = cmBaseSpace.inverse();
	MMatrix targetToBase = m * cmBaseSpace_inv;
	MMatrix targetToBase_inv = targetToBase.inverse();
//	MMatrix _controlToBase = cmSpace * cmBaseSpace_inv;
//	Math::Matrix4f controlToBase;
//	::copy( controlToBase, _controlToBase);

	ulWrapCache* cache = Load_Cache(data);
	if( !cache) return MS::kFailure;
	// ������ ������������� ����������� �����
	ulWrapDeformation* deformation = Load_Deformation(data);
	if( !deformation) return MS::kFailure;
	std::vector<basevertexdeform>& basedeform = deformation->basedeform;
//	std::vector<basevertexdeform> basedeform;
//	cache->getDeformations(meshobj, cache->src_verts, cache->src_vertnormals, cache->src_squarefaces, basedeform);

	// ������������� ������� �����
	cache->InitTarget(multiIndex, iter, targetToBase, bInteractiveInfluence);
	

	MObject meshobj = data.inputValue( this->i_controlMesh, &stat ).asMesh();
	MObject blendMeshobj = data.inputValue( this->i_blendMesh, &stat ).asMesh();
	MFnMesh blendMesh(blendMeshobj);
	MPointArray vertexArray;
	blendMesh.getPoints(vertexArray);

	double mainMeshDeformFactor = data.inputValue( this->i_mainMeshDeformFactor).asDouble();
	double blendMeshDeformFactor = data.inputValue( this->i_blendMeshDeformFactor).asDouble();


	// ����������
	geometryInfluence& gi = cache->geometries[multiIndex];
	srand(0);
	iter.reset();

	int p=0;
//printf("multiIndex=%d count=%d\n", multiIndex, iter.count());
	for ( iter.reset(); !iter.isDone(); iter.next(), p++) 
	{
		int index = iter.index();
		pointInfluence& pi = gi.influences[index];

		MPoint _pt = iter.position();
		_pt = _pt * targetToBase;

//		_pt *= m;
		float env = envelope;
		Math::Vec3f pt; copy(pt, _pt);
//		pt = gi.worldMatrix * pi.source_pt;
//		Math::Vec3f source_pt = pt;

		{
			Math::Vec3f project = pt, projectbase = pt;
			Math::Vec3f offset;
			Math::Rot3f rotate;
			float deformfactor;

			cache->getPointDeformation(
				project, projectbase, offset, rotate, deformfactor, 
				pi, basedeform, cache->src_verts, wireDistance);

			Math::Vec3f res_pr = pt + (project-pt)*env;
			Math::Vec3f res_de = cache->deformPoint(pt, project, projectbase, offset, rotate, env);
			Math::Vec3f res_deblend = res_de;

			if( bProjection)
				pt = res_pr;
			else
			{
				if( blendMeshDeformFactor==mainMeshDeformFactor)
					blendMeshDeformFactor = 1e38;

				deformfactor = (deformfactor-mainMeshDeformFactor)/(blendMeshDeformFactor-mainMeshDeformFactor);
				deformfactor = __min( 1, __max( 0, deformfactor));
//					deformfactor = 0.1*__max( 0, -deformfactor+1.2f);
//					pt += Math::Vec3f(0, rand()*deformfactor/(float)RAND_MAX, 0);
//					env *= deformfactor;
				
				pt = (1-deformfactor)*res_de + (deformfactor)*res_deblend;
			}

//			copy(_pt, pt);
		}

//		MVector _ofsworld; copy(_ofsworld, pt-source_pt);
//		_pt += _ofsworld;
//		_pt *= m_inv;
		::copy(_pt, pt);
		_pt = _pt * targetToBase_inv;
		iter.setPosition(_pt);
	}

	return MS::kSuccess;
}

void* ulWrap::creator()
{
	return new ulWrap();
}

MStatus ulWrap::initialize()
{
	MFnNumericAttribute nAttr, numAttr;
	MFnTypedAttribute	typedAttr;
	MFnCompoundAttribute comAttr;

	MStatus				stat;

	bool bStorable = false;
	try
	{
		// �������
		{
//			int storable = atUnStorable;
			int storeCache = atStorable;

			// �� ���� ������������: i_controlMeshBase i_subdivSteps i_bBaseInfluence
			i_cache = typedAttr.create( "cacheMesh", "cme", ulWrapCache::id);
			::addAttribute(i_cache, atReadable|atWritable|atCached|storeCache );
			stat = attributeAffects( i_cache, outputGeom );

			i_deformation = typedAttr.create( "deformation", "cde", ulWrapDeformation::id);
			::addAttribute(i_deformation, atReadable|atWritable|atCached|atUnStorable );
			stat = attributeAffects( i_cache, i_deformation );
			stat = attributeAffects( i_deformation, outputGeom );
			

			i_controlMesh = typedAttr.create( "controlMesh", "cm", MFnData::kMesh, &stat);
			::addAttribute(i_controlMesh, atInput);
			stat = attributeAffects( i_controlMesh, outputGeom );
			stat = attributeAffects( i_controlMesh, i_deformation );

			i_controlMeshMatrix = typedAttr.create( "controlMeshMatrix", "cmm", MFnData::kMatrix, &stat);
			::addAttribute(i_controlMeshMatrix, atInput);
			stat = attributeAffects( i_controlMeshMatrix, outputGeom );
			stat = attributeAffects( i_controlMeshMatrix, i_deformation );

			i_controlMeshBase = typedAttr.create( "controlMeshBase", "cmb", MFnData::kMesh, &stat);
			::addAttribute(i_controlMeshBase, atInput);
			stat = attributeAffects( i_controlMeshBase, outputGeom );
			stat = attributeAffects( i_controlMeshBase, i_cache);
			stat = attributeAffects( i_controlMeshBase,	i_deformation );

			i_controlMeshBaseMatrix = typedAttr.create( "controlMeshBaseMatrix", "cmbm", MFnData::kMatrix, &stat);
			::addAttribute(i_controlMeshBaseMatrix, atInput);
			stat = attributeAffects( i_controlMeshBaseMatrix, outputGeom );
			stat = attributeAffects( i_controlMeshBaseMatrix, i_deformation );

			i_bProjection = nAttr.create( "bProjection", "bp", MFnNumericData::kBoolean, 0);
			::addAttribute( i_bProjection, atInput|atKeyable);
			stat = attributeAffects( i_bProjection, outputGeom );

			i_subdivSteps = nAttr.create( "subdivSteps", "ss", MFnNumericData::kInt, 0);
			nAttr.setMin(0);
			nAttr.setSoftMax(8);
			::addAttribute( i_subdivSteps, atInput|atKeyable);
			stat = attributeAffects( i_subdivSteps, outputGeom );
			stat = attributeAffects( i_subdivSteps, i_cache);
			stat = attributeAffects( i_subdivSteps, i_deformation);

//			i_bRotation = nAttr.create( "bRotation", "bro", MFnNumericData::kBoolean, 1);
//			::addAttribute( i_bRotation, atInput|atKeyable);
//			stat = attributeAffects( i_bRotation, outputGeom );

//			i_bOffset = nAttr.create( "bOffset", "bof", MFnNumericData::kBoolean, 1);
//			::addAttribute( i_bOffset, atInput|atKeyable);
//			stat = attributeAffects( i_bOffset, outputGeom );

			i_bInteractiveInfluence = nAttr.create( "interactiveInfluence", "biin", MFnNumericData::kBoolean, 1);
			::addAttribute( i_bInteractiveInfluence, atInput|atKeyable);
			stat = attributeAffects( i_bInteractiveInfluence, outputGeom );
			
			i_bBaseInfluence = nAttr.create( "baseInfluence", "bbin", MFnNumericData::kBoolean, 1);
			::addAttribute( i_bBaseInfluence, atInput|atKeyable);
			stat = attributeAffects( i_bBaseInfluence, outputGeom );
			stat = attributeAffects( i_bBaseInfluence, i_cache );
			stat = attributeAffects( i_bBaseInfluence, i_deformation );
		}			

		// ��������� wire ���������
		{
			// ��� �������������� ����������� �����
			i_wireDistance = nAttr.create( "wireDistance", "wd", MFnNumericData::kDouble, 0.0);
			nAttr.setMin(0);
			nAttr.setSoftMax(1);
			::addAttribute( i_wireDistance, atInput|atKeyable);
			stat = attributeAffects( i_wireDistance, outputGeom );

			// ����� ��������
			i_wireTwist = nAttr.create( "wireTwist", "wtw", MFnNumericData::kDouble, 0);
			nAttr.setSoftMin(-90);
			nAttr.setSoftMax(90);
			::addAttribute( i_wireTwist, atInput|atKeyable);
			stat = attributeAffects( i_wireTwist, outputGeom );

			// ����� scale
			i_wireScale = nAttr.create( "wireScale", "wsc", MFnNumericData::kDouble, 1);
			nAttr.setMin(0);
			nAttr.setSoftMax(2);
			::addAttribute( i_wireScale, atInput|atKeyable);
			stat = attributeAffects( i_wireScale, outputGeom );
		}

		// blend
		{
			i_blendMesh = typedAttr.create( "blendMesh", "bme", MFnData::kMesh, &stat);
			::addAttribute(i_blendMesh, atInput);
			stat = attributeAffects( i_blendMesh, outputGeom );

			
			i_mainMeshDeformFactor = nAttr.create( "mainMeshDeformFactor", "mmdf", MFnNumericData::kDouble, 1);
			nAttr.setMin(0.01);
			nAttr.setSoftMax(3);
			::addAttribute( i_mainMeshDeformFactor, atInput|atKeyable);
			stat = attributeAffects( i_mainMeshDeformFactor, outputGeom );

			i_blendMeshDeformFactor = nAttr.create( "blendMeshDeformFactor", "bmdf", MFnNumericData::kDouble, 2);
			::addAttribute( i_blendMeshDeformFactor, atInput|atKeyable);
			nAttr.setMin(0.01);
			nAttr.setSoftMax(3);
			stat = attributeAffects( i_blendMeshDeformFactor, outputGeom );
		}

		// �������������� ������
		{
			{
				o_handles = comAttr.create("o_handles", "ohnds");

				ih_o_translate = numAttr.createPoint("ih_o_translate", "ihot");
				comAttr.addChild(ih_o_translate);

				ih_o_rotate = numAttr.createPoint("ih_o_rotate", "ihor");
				comAttr.addChild(ih_o_rotate);

				ih_o_scale = numAttr.createPoint("ih_o_scale", "ihos");
				comAttr.addChild(ih_o_scale);

				comAttr.setArray(true);
				comAttr.setUsesArrayDataBuilder(true);
				::addAttribute(o_handles, atOutput);
				stat = attributeAffects( i_controlMesh, o_handles );
			}

			{
				i_handles = comAttr.create("i_handles", "ihnds");

				ih_i_rotate = numAttr.createPoint("ih_i_rotate", "ihir");
				comAttr.addChild(ih_i_rotate);

				ih_i_scale = numAttr.createPoint("ih_i_scale", "ihis");
				comAttr.addChild(ih_i_scale);

				comAttr.setArray(true);
				comAttr.setUsesArrayDataBuilder(true);
				::addAttribute(i_handles, atInput);
				stat = attributeAffects( i_handles, outputGeom );
			}
		}

	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

