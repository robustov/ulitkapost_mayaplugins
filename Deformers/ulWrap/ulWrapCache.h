#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include <maya/MFnMesh.h>
#include "Util/Stream.h"
#include "Math/Subdiv.h"
#include "Math/Math.h"
#include "Math/Edge.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", ulWrapCache::id );
	}
/*/

// ���������� ����������� �����
struct basevertexdeform
{
	Math::Vec3f offset;
	Math::Rot3f rotate;
	float deformationfactor;		// ������� ���������
	basevertexdeform():offset(0),rotate(0) {deformationfactor=0;}
};

// ��� ����������� �����
struct basevertex
{
//	Math::Vec3f pos;			// �����
//	Math::Vec3f normal;			// �������
	std::vector<int> faces;		// ��������� �����
	basevertex(){faces.reserve(4);}
};

// target ����� 
struct pointToBasePoint
{
	int basevertex;
	float weight;
	pointToBasePoint(int basevertex=0, float weight=0){this->basevertex=basevertex; this->weight=weight;}
};

// ������ � �������� ��������
struct pointInfluence
{
	// �������� ��������� ����� (object space)
	Math::Vec3f source_pt;

	// �������� � �����������
	std::vector< pointToBasePoint> basepoints;
	// �������� � �����
	float distanceToEdge;
	std::vector< pointToBasePoint> basepointsEdge;

	pointInfluence(){};
	void reset()
	{
		distanceToEdge = 1e38f;
		basepoints.clear();
		basepointsEdge.clear();
	}
};

// ������ � �������� ���������
struct geometryInfluence
{
	// �������� ������� world space
	Math::Matrix4f worldMatrix;
	// ����� index() -> pointInfluence
	std::map<int, pointInfluence> influences;
};

// �������� �����
struct openedge
{
	int v_1;	// ����. �������� �����
	Math::Edge32 edge;
	int v2_;	// ����. �������� �����
	openedge(){v_1=0;v2_=0;}
};

class ulWrapCache : public MPxData
{
// ������� � InitMesh
public:
	bool bValid;
	int poligoncount, vertexcount, facevertexcount, subdivstep;

	// ��� ����������� �����
	std::vector<basevertex> baseverts;
	// �����
	std::vector< Math::Face32> faces;
	// ����� �������� ��������
	int faceNormalCount;
	// ������ ������� �����
	std::vector< int> facesnormalindex;
	
	// �������� �����
	std::vector< openedge> openedges;
	// ����� ��������
	std::vector< Math::subdivWeight> neoverts;

	//��� ��������
	std::vector<Math::Vec3f> src_verts;			// ������� ���������
	std::vector<Math::Vec3f> src_vertnormals;	// ������� ���������
	std::vector<Math::Vec3f> src_facenormals;	// ������� ��-���
	std::vector< float>		 src_squarefaces;	// ������� �����
	std::map< Math::Edge32, Math::Vec3f> src_edgenormals;	// ������� �����

// ������� � InitTarget
public:
	// target geomentry (multiIndex->geometryInfluence)
	std::map<int, geometryInfluence> geometries;

public:
	bool InitMesh(
		MObject mesh, 
		int subdivstep,
		bool bBaseInfluence);
public:
	bool InitTarget(
		int multiIndex, 
		MItGeometry& iter, 
		const MMatrix& mat, 
		bool bInteractiveInfluence);

public:
	// ������ ��� ����������
	bool getDeformations(
		MObject meshobj,				// ��������������� �����
		const Math::Matrix4f& controlToBase, // �������������� � ������������ base �����������
		std::vector<Math::Vec3f>& src_verts,			// �������� ��������
		std::vector<Math::Vec3f>& src_vertnormals,		// �������� ������� ���������
		std::vector< float>& src_squarefaces,			// �������� ������� ������
		std::vector<basevertexdeform>& basedeform	// ������ ��� ����������
		);
	// ������ ��� ���������� ��������
	void getPointDeformation(
		Math::Vec3f& project,				// �������� �����
		Math::Vec3f& projectbase,			// �������� �����
		Math::Vec3f& offset,				// ��������
		Math::Rot3f& rotate,				// ��������
		float& deformfactor,				// ���. �������� ����������
		const pointInfluence& pi,						// ��������
		const std::vector<basevertexdeform>& basedeform,	// ������ ��� ����������
		const std::vector<Math::Vec3f>& verts,
		float wireDistance
		)const;
	// ���������� ��������
	Math::Vec3f deformPoint(
		Math::Vec3f& pt,						// �����
		const Math::Vec3f& project,				// �������� �����
		const Math::Vec3f& projectbase,			// �������� �����
		const Math::Vec3f& offset,				// ��������
		const Math::Rot3f& rotate,				// ��������
		float envelope
		)const;

public:
	// ������ ���������
	void calcVerts( 
		MFnMesh& mesh, 
		std::vector<Math::Vec3f>& verts
		)const;
	// ������ �������� � �������� ������
	void calcFaceNormals(
		std::vector<Math::Vec3f>& verts,		// ������� ���������
		std::vector<Math::Vec3f>& facenormals,	// ������� ��-���
		std::vector< float>& squarefaces		// ������� �����
		)const;
	// ������ �������� ���������
	void calcVertNormals(
		std::vector<Math::Vec3f>& facenormals,	// ������� ��-���
		std::vector<Math::Vec3f>& vertnormals	// ������� ���������
		)const;
	// ������ �������� �����	
	void calcEdgeNormals(
		std::vector<Math::Vec3f>& facenormals,	// ������� ��-���
		std::map< Math::Edge32, Math::Vec3f>& src_edgenormals	// ������� �����
		)const;

	// �������� �����
	void projectionPoint(
		Math::Vec3f pt,			// world space
		pointInfluence& pi,		// ���������
		std::vector<Math::Vec3f>& verts,		// ������� ���������
		std::vector<Math::Vec3f>& vertnormals,	// ������� ���������
		std::map< Math::Edge32, Math::Vec3f>& edgenormals	// ������� �����
		) const;


public:
	ulWrapCache();
	virtual ~ulWrapCache(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;

protected:
	void serialize(Util::Stream& stream);
};

template<class Stream> inline
Stream& operator >> (Stream& out, basevertex& v)
{
//	out >> v.pos;			// �����
//	out >> v.normal;		// �������
	out >> v.faces;			// ��������� �����
	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, pointToBasePoint& v)
{
	out >> v.basevertex;	// �����
	out >> v.weight;		// �������
	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, pointInfluence& v)
{
	out >> v.source_pt >> v.basepoints;
	out >> v.distanceToEdge;
	out >> v.basepointsEdge;
	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, openedge& v)
{
	out >> v.v_1 >> v.edge >> v.v2_;
	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, geometryInfluence& v)
{
	out >> v.worldMatrix >> v.influences;
	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, basevertexdeform& v)
{
	out >> v.offset >> v.rotate >> v.deformationfactor;
	return out;
}
