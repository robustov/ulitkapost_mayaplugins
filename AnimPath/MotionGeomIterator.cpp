#include "MotionGeomIterator.h"

#include <maya/MIOStream.h>

MotionGeomIterator::MotionGeomIterator( void * geom, MObjectArray & comps )
	: MPxGeometryIterator( geom, comps ),
	geometry( (MotionGeom*)geom )
{
	reset();
}

MotionGeomIterator::MotionGeomIterator( void * geom, MObject & comps )
	: MPxGeometryIterator( geom, comps ),
	geometry( (MotionGeom*)geom )
{
	reset();
}

void MotionGeomIterator::reset()
//
// Description
//
//  	
//   Resets the iterator to the start of the components so that another
//   pass over them may be made.
//
{
	MPxGeometryIterator::reset();
	setCurrentPoint( 0 );
	if ( NULL != geometry ) 
	{
		int maxVertex = geometry->getVertexTangentCount();
		setMaxPoints( maxVertex );
	}
}

/* override */
MPoint MotionGeomIterator::point() const
//
// Description
//
//    Returns the point for the current element in the iteration.
//    This is used by the transform tools for positioning the
//    manipulator in component mode. It is also used by deformers.	 
//
{
	MPoint pnt;
	if ( NULL != geometry ) 
	{
		pnt = geometry->getVertexTangent( index() );
	}
	displayStringD("MotionGeomIterator::point %d: %f %f %f", index(), pnt.x, pnt.y, pnt.z);
	return pnt;
}

/* override */
void MotionGeomIterator::setPoint( const MPoint & pnt ) const
//
// Description
//
//    Set the point for the current element in the iteration.
//    This is used by deformers.	 
//
{
	if ( NULL != geometry ) 
	{
//		geometry->vertices.set( pnt, index() );
	}
	displayStringD("MotionGeomIterator::setPoint %d: %f %f %f", index(), pnt.x, pnt.y, pnt.z);
}

/* override */
int MotionGeomIterator::iteratorCount() const
{
//
// Description
//
//    Return the number of vertices in the iteration.
//    This is used by deformers such as smooth skinning
//
	return geometry->getVertexTangentCount();
	
}

/* override */
bool MotionGeomIterator::hasPoints() const
//
// Description
//
//    Returns true since the shape data has points.
//
{
	return true;
}

