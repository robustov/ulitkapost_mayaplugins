AnimPath for Maya 5.0, 6.0, 6.5, 7.0

Description:
============
AnimPath is a shape to edit Animation Curves of "translate" attribute of an object in a viewport.
With AnimPath you can also view trajectory of object, view keyframes and transform all keys at once.

Features:
=========
- AnimPath can be used to view trajectory of any object even if the object is not connected to AnimCurve;
   To view trajectory of an object not connected to AnimCurve "Show Timeline" checkbox must be checked in Attribute Editor;
   To update AnimCurve press "Refresh" button in Attribute Editor;
- AnimPath supports Character animation;
- AnipPath can be transformed (shifted/rotated); Animation keys will be recalculated after "Apply transform to AC" button is pressed.

Usage:
======
1. Load AnimPath.mll plugin;
2. Select object with animated "translate" attribute;
3. Select "Animate/Attach Anim Path" menu item;
4. Check Maya's "Select by component type" checkbox;
5. Edit keys (Note: if AnimPath is deleted, animation is not lost or corrupted).

Future:
=======
- Import/Export AnimPath to Nurbs.

Author:
=======
  Dmitriy Robustov, robustov@mail.ru

EULA:
=====
  - the software is free for non commercial use.
  
  
  
 
