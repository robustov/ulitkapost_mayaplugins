#include "MotionGeom.h"

MotionGeom::MotionGeom()
{
}

int MotionGeom::getVertexTangentCount()
{
	return keyView.size()*3;
}
MPoint MotionGeom::getVertexTangent(int index)
{
	if(index < getVertexCount())
		return getVertex(index);
	index -= getVertexCount();
	if(index < getVertexCount())
		return getInTangent(index);
	index -= getVertexCount();
	return getOutTangent(index);
}
MTime MotionGeom::getVertexTangentTime(int index)
{
	index = index % getVertexCount();
	return getVertexTime(index);
}
enVertexTangentType MotionGeom::getVertexTangentType(int index) // ��� �� �������
{
	index  = index / getVertexCount();
	return (enVertexTangentType)index;
}

int MotionGeom::getVertexCount()
{
	return keyView.size();
}

MPoint MotionGeom::getVertex(int index)
{
	std::map<MTime, KeyData>::iterator it = keyView.begin();
	for ( int vertexIndex=0; it != keyView.end(); it++, vertexIndex++ )
	{
		if(vertexIndex!=index) continue;
		return it->second.pt;
	}
	return MPoint(0, 0, 0);
}
MPoint MotionGeom::getInTangent(int index)
{
	std::map<MTime, KeyData>::iterator it = getVertexIt(index);
	if( it==keyView.end()) return MPoint(0, 0, 0);
	MPoint pt2 = it->second.inTangent;
	return pt2;
}
MPoint MotionGeom::getOutTangent(int index)
{
	std::map<MTime, KeyData>::iterator it = getVertexIt(index);
	if( it==keyView.end()) return MPoint(0, 0, 0);
	MPoint pt2 = it->second.outTangent;
	return pt2;
}
MTime MotionGeom::getVertexTime(int index)
{
	std::map<MTime, KeyData>::iterator it = getVertexIt(index);
	if( it==keyView.end()) return MTime();
	return it->first;
}


// TrackVertex
MBoundingBox MotionGeom::boundingBox() 
{
	MBoundingBox box;
	for( int vertexIndex=0; vertexIndex < getVertexTangentCount(); vertexIndex++ )
	{
		MPoint pt = getVertexTangent(vertexIndex);
		box.expand(pt);
	}
	for(unsigned i=0; i<drawData.size(); i++)
	{
		Vector3f& vr = drawData[i];
		MPoint pt(vr.x, vr.y, vr.z);
		box.expand(pt);
	}
	return box;
}


void MotionGeom::updateVertexTangent(int index, MVector ofs, 
	MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ)
{
	if(index < getVertexCount())
	{
		updateVertex(index, ofs, acX, acY, acZ);
		return;
	};
	index -= getVertexCount();
	if(index < getVertexCount())
	{
		updateInTangent(index, ofs, acX, acY, acZ);
		return;
	};
	index -= getVertexCount();
	{
		updateOutTangent(index, ofs, acX, acY, acZ);
		return;
	};
}

void MotionGeom::updateVertex(int index, MVector ofs,
	MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ)
{
	std::map<MTime, KeyData>::iterator it = getVertexIt(index);
	if( it==keyView.end()) return;

	MTime t = it->first;
	it->second.pt += ofs;
	updateCurve(acX, t, ofs.x);
	updateCurve(acY, t, ofs.y);
	updateCurve(acZ, t, ofs.z);
}

// deleteVertex
void MotionGeom::deleteVertex(int index, MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ)
{
	std::map<MTime, KeyData>::iterator it = getVertexIt(index);
	if( it==keyView.end()) return;

	MTime t = it->first;

	deleteKey(acX, t);
	deleteKey(acY, t);
	deleteKey(acZ, t);
}

// insertVertex return vertex index
int MotionGeom::insertVertex(MTime t, MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ, MPlug worldMatrix)
{
	KeyData kd;
	kd.pt = evaluate(worldMatrix, t);
	kd.inTangent = getTangent(acX, acY, acZ, t, true);
	kd.outTangent = getTangent(acX, acY, acZ, t, false);
	keyView[t] = kd;

	std::map<MTime, KeyData>::iterator it = keyView.find(t);
	if( it==keyView.end()) return 0;
	return std::distance(keyView.begin(), it);
}

// selectVertex
void MotionGeom::selectVertex(int index, MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ)
{
	std::map<MTime, KeyData>::iterator it = getVertexIt(index);
	if( it==keyView.end()) return;

	MTime t = it->first;

	selectKey(acX, t);
	selectKey(acY, t);
	selectKey(acZ, t);
}


void MotionGeom::updateInTangent(int index, MVector ofs,
	MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ)
{
	std::map<MTime, KeyData>::iterator it = getVertexIt(index);
	if( it==keyView.end()) return;

	MTime t = it->first;
//	ofs = ofs - it->second.pt;
	updateCurveTangent(acX, t, ofs.x, true);
	updateCurveTangent(acY, t, ofs.y, true);
	updateCurveTangent(acZ, t, ofs.z, true);

//	displayString("updateInTangent: %f, %f, %f", ofs.x, ofs.y, ofs.z);
}

void MotionGeom::updateOutTangent(int index, MVector ofs,
	MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ)
{
	std::map<MTime, KeyData>::iterator it = getVertexIt(index);
	if( it==keyView.end()) return;

	MTime t = it->first;
//	ofs = ofs - it->second.pt;
	updateCurveTangent(acX, t, ofs.x, false);
	updateCurveTangent(acY, t, ofs.y, false);
	updateCurveTangent(acZ, t, ofs.z, false);

//	displayString("updateOutTangent: %f, %f, %f", ofs.x, ofs.y, ofs.z);
}

void MotionGeom::load(
	MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ, 
	MPlug worldMatrix, MPlug parentMatrixInv, MTime timestep, 
	bool bInitTimeline, bool bAddAbsentKeys, double scaleTangent)
{
	displayStringD("MotionGeom::load");
	this->scaleTangent = scaleTangent;
	drawData.clear();
	keyView.clear();

	loadSingle(acX);
	loadSingle(acY);
	loadSingle(acZ);

	std::map<MTime, KeyData>::iterator it = keyView.begin();
	for(;it != keyView.end(); it++)
	{
		MTime t = it->first;

		if( bAddAbsentKeys)
		{
			addAbsentKey(acX, t);
			addAbsentKey(acY, t);
			addAbsentKey(acZ, t);
		}

		MMatrix W     = getMatrixForPlug( worldMatrix, t);
		MMatrix Wprin = getMatrixForPlug( parentMatrixInv, t);
		MMatrix Wpr   = Wprin.inverse();
		MMatrix L = W * Wprin;

		it->second.pt = evaluate(worldMatrix, it->first);
		MVector dTin  = getTangent(acX, acY, acZ, t, true);
		MVector dTout = getTangent(acX, acY, acZ, t, false);

		it->second.inTangent  = (MPoint(0, 0, 0, 1)*move(L, -dTin))*Wpr;
		it->second.outTangent = (MPoint(0, 0, 0, 1)*move(L, dTout))*Wpr;
	}

	if( bInitTimeline || timestep!=MTime(0, MTime::kFilm))
	{
		for( it = keyView.begin(); ; it++)
		{
			MTime start, end;
			// start
			if( it == keyView.begin())
			{
				if(!bInitTimeline)
				{
					if( it == keyView.end())
						break;
					else
						continue;
				}
				start = MAnimControl::minTime();
			}
			else
			{
				std::map<MTime, KeyData>::iterator itl = it;
				--itl;
				start = itl->first;
			}
			// end
			if( it == keyView.end())
			{
				if(!bInitTimeline)
					break;
				end = MAnimControl::maxTime();
			}
			else
			{
				end = it->first;
			}

			for( MTime t = start; t<=end; t+=timestep)
			{
				MPoint pt = evaluate(worldMatrix, t);
				drawData.push_back(Vector3f((float)pt.x, (float)pt.y, (float)pt.z));
			}
			MPoint pt = evaluate(worldMatrix, end);
			drawData.push_back(Vector3f((float)pt.x, (float)pt.y, (float)pt.z));

			if( it==keyView.end()) 
				break;
		}
	}
}

void MotionGeom::addAbsentKey(MFnAnimCurve ac, MTime t)
{
	unsigned key;
	if( ac.find( t, key))
		return;

	double value = ac.evaluate(t);
	ac.addKeyframe(t, value, MFnAnimCurve::kTangentGlobal, MFnAnimCurve::kTangentGlobal);
//	ac.addKey(t, value, MFnAnimCurve::kTangentSmooth, MFnAnimCurve::kTangentSmooth);
}

MVector MotionGeom::getTangent(MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ, MTime t, bool inTangent)
{
	MVector x;
	MVector y;
	MAngle angle; //double w;

	y[0] = getCurveTangent(acX, t, inTangent);
	y[1] = getCurveTangent(acY, t, inTangent);
	y[2] = getCurveTangent(acZ, t, inTangent);

//	y = y*scaleTangent;
	return y;
}

MPoint MotionGeom::evaluate(MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ, MTime t)
{
	MPoint pt( 
		acX.evaluate(t), 
		acY.evaluate(t), 
		acZ.evaluate(t));
	return pt;
}
MPoint MotionGeom::evaluate(
	MPlug worldMatrix, MTime t)
{
	MMatrix m = getMatrixForPlug(worldMatrix, t);
	MPoint pt(0,0,0,1);
	pt *= m;
	return pt;
}

void MotionGeom::loadSingle(MFnAnimCurve ac)
{
	int keys = ac.numKeys();
	for(int k=0; k<keys; k++)
	{
		MTime t = ac.time(k);
		KeyData kd;
		keyView[t] = kd;
	}
}

void MotionGeom::updateCurve(MFnAnimCurve ac, MTime t, double ofs)
{
	unsigned key;
	if( ac.find( t, key))
	{
		double value = ac.value(key) + ofs;
		ac.setValue(key, value);
	}
	else
	{
		double value = ac.evaluate(t) + ofs;
		ac.addKey(t, value);
	}
}
void MotionGeom::deleteKey(MFnAnimCurve ac, MTime t)
{
	unsigned key;
	if( ac.find( t, key))
	{
		ac.remove(key);
	}
}
void MotionGeom::selectKey(MFnAnimCurve ac, MTime t)
{
	char buf[1024];
	_snprintf(buf, 1024, "selectKey -add -k -t %f %s;", t.as(MTime::uiUnit()), ac.name().asChar());
	MCommandResult res;
	MGlobal::executeCommand(buf, res, true);
}

double MotionGeom::getCurveTangent(MFnAnimCurve ac, MTime t, bool inTangent)
{
	MAngle angle; double w;

	unsigned key;
	if( !ac.find( t, key))
		return 0;
	ac.getTangent(key, angle, w, inTangent);
	double comp = tan(angle.asRadians())*w;
//	double comp = tan(angle.asRadians());
	comp = comp*scaleTangent;
	return comp;
}

void MotionGeom::updateCurveTangent(MFnAnimCurve ac, MTime t, double value, bool bInTangent)
{
	unsigned key;
	if( !ac.find( t, key)) return;
	if( bInTangent)
		value = -value;

	value = value/scaleTangent;

	MAngle angle1; double w;
	ac.getTangent(key, angle1, w, bInTangent);
	MAngle angle(atan(value/w));
//	MAngle angle(atan(value));
	ac.setTangent(key, angle, w, bInTangent);
}


void MotionGeom::draw(M3dView &view, bool bDrawNumbers)
{
	float lastPointSize;
	glGetFloatv( GL_POINT_SIZE, &lastPointSize );

	if(true)
	{
		if( drawData.size()>=2)
		{
			glLineStipple(1, 0xFFFF);
			glEnable(GL_LINE_STIPPLE);
			glEnableClientState(GL_VERTEX_ARRAY);
			glVertexPointer(3, GL_FLOAT, 0, &*drawData.begin());
			glDrawArrays (GL_LINE_STRIP, 0, drawData.size());
			glDisableClientState (GL_VERTEX_ARRAY);
			glDisable(GL_LINE_STIPPLE);

			glPointSize( 1.5 );
			glEnableClientState(GL_VERTEX_ARRAY);
			glVertexPointer(3, GL_FLOAT, 0, &*drawData.begin());
			glDrawArrays (GL_POINTS, 0, drawData.size());
			glDisableClientState (GL_VERTEX_ARRAY);
		}
	}

	glPointSize( 4.0 );

	std::map<MTime, KeyData>::iterator it = keyView.begin();
	for(int vi=0; it != keyView.end(); it++, vi++)
	{
		MTime t = it->first;
		MPoint pt = it->second.pt;

		glBegin( GL_POINTS );
		glVertex3f( (float)pt[0], (float)pt[1], (float)pt[2] );
		glEnd();

		if( bDrawNumbers)
		{
			MString str;
			str.set(t.as(MTime::uiUnit()));
			view.drawText(str, pt);
		}

//		drawTangents(vi, false);
	}
	glPointSize( lastPointSize );
}


void MotionGeom::drawVertex(int index)
{
	MPoint vertex = getVertex(index);

	glBegin( GL_POINTS );
	glVertex3f( (float)vertex[0], 
				(float)vertex[1], 
				(float)vertex[2] );
	glEnd();
}
void MotionGeom::drawTangents(int index, bool bDrawPoint)
{
	drawInTangent(index, bDrawPoint, true);
	drawOutTangent(index, bDrawPoint, true);
}
void MotionGeom::drawInTangent(int index, bool bDrawPoint, bool bDrawBone)
{
	if( index==0)
		return;

	std::map<MTime, KeyData>::iterator it = getVertexIt(index);
	if( it==keyView.end()) return;

	MTime t = it->first;
	MPoint pt = it->second.pt;
	{
		MPoint pt2 = it->second.inTangent;
		if( bDrawPoint)
		{
			glBegin( GL_POINTS );
			glVertex3f( (float)pt2[0], (float)pt2[1], (float)pt2[2] );
			glEnd();
		}
		if( bDrawBone)
		{
			glLineStipple(1, 0x8888);
			glEnable(GL_LINE_STIPPLE);

			glBegin( GL_LINES);
			glVertex3f( (float)pt2[0], (float)pt2[1], (float)pt2[2] );
			glVertex3f( (float)pt[0], (float)pt[1], (float)pt[2] );
			glEnd();

			glDisable(GL_LINE_STIPPLE);
		}
	}
}
void MotionGeom::drawOutTangent(int index, bool bDrawPoint, bool bDrawBone)
{
	if( index==getVertexCount()-1)
		return;

	std::map<MTime, KeyData>::iterator it = getVertexIt(index);
	if( it==keyView.end()) return;

	MTime t = it->first;
	MPoint pt = it->second.pt;
	{
		MPoint pt2 = it->second.outTangent;
		if( bDrawPoint)
		{
			glBegin( GL_POINTS );
			glVertex3f( (float)pt2[0], (float)pt2[1], (float)pt2[2] );
			glEnd();
		}
		if( bDrawBone)
		{
			glLineStipple(1, 0x8888);
			glEnable(GL_LINE_STIPPLE);

			glBegin( GL_LINES);
			glVertex3f( (float)pt2[0], (float)pt2[1], (float)pt2[2] );
			glVertex3f( (float)pt[0], (float)pt[1], (float)pt[2] );
			glEnd();

			glDisable(GL_LINE_STIPPLE);
		}
	}
}

std::map<MTime, MotionGeom::KeyData>::iterator MotionGeom::getVertexIt(int index)
{
	std::map<MTime, KeyData>::iterator it = keyView.begin();
	for ( int vertexIndex=0; it != keyView.end(); it++, vertexIndex++ )
	{
		if(vertexIndex!=index) continue;
		return it;
	}
	return it;
}
