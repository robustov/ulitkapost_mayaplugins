#pragma once

#include "pre.h"

enum enVertexTangentType 
{
	vtt_vertex = 0,
	vtt_intangent = 1,
	vtt_outtangent = 2
};

struct MotionGeom
{
	MotionGeom();

	int getVertexTangentCount();
	MPoint getVertexTangent(int index);
	MTime getVertexTangentTime(int index);
	enVertexTangentType getVertexTangentType(int index); // ��� �� �������

	// VertexCount
	int getVertexCount();
	// Vertex
	MPoint getVertex(int v);
	MPoint getInTangent(int v);
	MPoint getOutTangent(int v);
	MTime getVertexTime(int index);


	// TrackVertex
	MBoundingBox boundingBox();

	// updateVertex
	void updateVertexTangent(int v, MVector ofs, 
		MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ);
	// insertVertex return vertex index
	int insertVertex(MTime t, MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ, MPlug worldMatrix);
	// deleteVertex
	void deleteVertex(int index, MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ);

	// selectVertex
	void selectVertex(int index, MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ);

	// draw
	void draw(M3dView &view, bool bDrawNumbers);
	void drawVertex(int index);
	void drawTangents(int index, bool bDrawPoint);
	void drawInTangent(int index, bool bDrawPoint=true, bool bDrawBone=false);
	void drawOutTangent(int index, bool bDrawPoint=true, bool bDrawBone=false);

	// load
	void load(
		MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ, 
		MPlug worldMatrix, MPlug parentMatrixInv, 
		MTime timestep, 
		bool bInitTimeline/*/=true/*/, 
		bool bAddAbsentKeys/*/=false/*/, 
		double scaleTangent);


protected:
	struct Vector3f
	{
		float x, y, z;
		Vector3f(float x=0, float y=0, float z=0){this->x=x;this->y=y;this->z=z;}
		Vector3f(const Vector3f& arg)
		{
			x = arg.x; y = arg.y; z = arg.z;
		}
		Vector3f& operator =(const Vector3f& arg)
		{
			x = arg.x; y = arg.y; z = arg.z; return *this;
		}
	};
	// ������ ������� �������
	std::vector<Vector3f> drawData;
	// ������ ������
	struct KeyData
	{
		MPoint pt;
		MPoint inTangent;
		MPoint outTangent;
	};
	std::map<MTime, KeyData> keyView;

	double scaleTangent;

protected:
	void updateVertex(int index, MVector ofs,  
		MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ);
	void updateInTangent(int index, MVector ofs,  
		MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ);
	void updateOutTangent(int index, MVector ofs,  
		MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ);

	void addAbsentKey(MFnAnimCurve ac, MTime t);

protected:
	void loadSingle(MFnAnimCurve ac);

	MPoint evaluate(
		MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ, MTime t);
	MPoint evaluate(
		MPlug worldMatrix, MTime t);

	MVector getTangent(MFnAnimCurve acX, MFnAnimCurve acY, MFnAnimCurve acZ, MTime t, bool inTangent);

	double getCurveTangent(MFnAnimCurve ac, MTime t, bool inTangent);
	void updateCurveTangent(MFnAnimCurve ac, MTime t, double value, bool bInTangent);

	void updateCurve(MFnAnimCurve ac, MTime t, double value);
	void deleteKey(MFnAnimCurve ac, MTime t);
	void selectKey(MFnAnimCurve ac, MTime t);

	std::map<MTime, KeyData>::iterator getVertexIt(int index);

};
