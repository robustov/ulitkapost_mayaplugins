#pragma once

#include "pre.h"

/////////////////////////////////////////////////////////////////////
//
// UI class	- defines the UI part of a shape node
//
class AnimPathShapeUI : public MPxSurfaceShapeUI
{
public:
	AnimPathShapeUI();
	virtual ~AnimPathShapeUI(); 

	virtual void	getDrawRequests( const MDrawInfo & info,
									 bool objectAndActiveOnly,
									 MDrawRequestQueue & requests );
	virtual void	draw( const MDrawRequest & request,
						  M3dView & view ) const;
	virtual bool	select( MSelectInfo &selectInfo,
							MSelectionList &selectionList,
							MPointArray &worldSpaceSelectPts ) const;

	static  void *  creator();


private:
	void setRequestColor( 
		MDrawRequest&,
		const MDrawInfo& );

	bool selectVertices( 
		MSelectInfo &selectInfo,
		MSelectionList &selectionList,
		MPointArray &worldSpaceSelectPts,
		bool bSelectTangentAlways) const;

	void drawVertices( const MDrawRequest & request,
								M3dView & view ) const;

	// Draw Tokens
	//
	enum {
		kDrawWireframe=0,
		kDrawVertices=1, 
		kDrawTangents=0x800, 
		kDrawNumbers=0x8000, 
		kLastToken
	};
};
