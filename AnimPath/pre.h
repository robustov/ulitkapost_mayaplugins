#pragma once

#include <math.h>
#include <maya/MIOStream.h>

#include <maya/MPxNode.h>   

#include <maya/MPxLocatorNode.h> 

#include <maya/MString.h> 
#include <maya/MTypeId.h> 
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MAngle.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MColor.h>
#include <maya/M3dView.h>
#include <maya/MDistance.h>

#include <maya/MFnUnitAttribute.h>
#include <maya/MFnNumericAttribute.h>

#include <maya/MFn.h>
#include <maya/MPxNode.h>
#include <maya/MPxManipContainer.h> 
#include <maya/MCommandResult.h> 

#include <maya/MFnCircleSweepManip.h>
#include <maya/MFnDirectionManip.h>
#include <maya/MFnDiscManip.h>
#include <maya/MFnDistanceManip.h> 
#include <maya/MFnFreePointTriadManip.h>
#include <maya/MFnStateManip.h>
#include <maya/MFnToggleManip.h>

#include <maya/MPxContext.h>
#include <maya/MPxSelectionContext.h>

#include <maya/MFnNumericData.h>
#include <maya/MManipData.h>
#include <maya/MTime.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnMatrixData.h>
#include <maya/MMatrix.h>
#include <maya/MGlobal.h>
#include <maya/MAnimMessage.h>
#include <maya/MMessage.h>
#include <maya/MPlugArray.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MAnimControl.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MNodeMessage.h>

#include <maya/MIOStream.h>
#include <maya/MPxSurfaceShape.h>
#include <maya/MPxSurfaceShapeUI.h>
//#include <maya/MFnPlugin.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MPoint.h>
#include <maya/MPlug.h>
#include <maya/MDrawData.h>
#include <maya/MDrawRequest.h>
#include <maya/MSelectionMask.h>
#include <maya/MSelectionList.h>
#include <maya/MDagPath.h>
#include <maya/MMaterial.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFnNurbsCurveData.h>
#include <GL/glu.h>
#include <maya/MPointArray.h>
#include <maya/MDoubleArray.h>

#include <maya/MIOStream.h>
#include <maya/MPxSurfaceShape.h>
#include <maya/MPxSurfaceShapeUI.h>
#include <maya/MAttributeSpec.h>
#include <maya/MAttributeSpecArray.h>
#include <maya/MAttributeIndex.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MPoint.h>
#include <maya/MPlug.h>
#include <maya/MDrawData.h>
#include <maya/MDrawRequest.h>
#include <maya/MSelectionMask.h>
#include <maya/MSelectionList.h>
#include <maya/MDagPath.h>
#include <maya/MMaterial.h>
#include <maya/MFnSingleIndexedComponent.h>
#include <GL/glu.h>

#include <vector>
#include <map>
#include <list>
#include <set>

#define e \
    if (MS::kSuccess != s) { \
		char buf[256];\
		sprintf(buf, "ERROR %d ", __LINE__);\
		MGlobal::displayError(buf);\
    }

inline void displayString(const char* text, ...)
{
	va_list argList; va_start(argList, text);
	char sbuf[256];
	_vsnprintf(sbuf, 255, text, argList);
	va_end(argList);
	MGlobal::displayInfo(sbuf);
}
inline void displayStringD(const char* text, ...)
{
#ifdef _DEBUG
	va_list argList; va_start(argList, text);
	char sbuf[256];
	_vsnprintf(sbuf, 255, text, argList);
	va_end(argList);
	MGlobal::displayInfo(sbuf);
#endif
}
inline MMatrix getMatrixForPlug(MPlug worldMatrix, MTime t)
{
	MDGContext context(t);
	MObject val;
	worldMatrix.getValue(val, context);
	MFnMatrixData matrData(val);
	MMatrix m = matrData.matrix();
	return m;
}
inline MMatrix getMatrixForPlug(MPlug worldMatrix)
{
	MObject val;
	worldMatrix.getValue(val);
	MFnMatrixData matrData(val);
	MMatrix m = matrData.matrix();
	return m;
}

inline MMatrix move( const MMatrix& m, const MVector& v)
{
	MMatrix L = m;
	L(3, 0) += v[0];
	L(3, 1) += v[1];
	L(3, 2) += v[2];
//	L(3, 3) += v[3];
	return L;
}

inline void displayPoint( const MPoint& p)
{
	displayString("%g, %g, %g, %g", p[0], p[1], p[2], p[3]);
}

inline void displayMatrix( const MMatrix& L)
{
	displayString("%g, %g, %g, %g", L(0, 0), L(0, 1), L(0, 2), L(0, 3));
	displayString("%g, %g, %g, %g", L(1, 0), L(1, 1), L(1, 2), L(1, 3));
	displayString("%g, %g, %g, %g", L(2, 0), L(2, 1), L(2, 2), L(2, 3));
	displayString("%g, %g, %g, %g", L(3, 0), L(3, 1), L(3, 2), L(3, 3));
}
// CMD
inline MStatus nodeFromName(MString name, MObject & obj)
{
	MSelectionList tempList;
    tempList.add( name );
    if ( tempList.length() > 0 ) 
	{
		tempList.getDependNode( 0, obj );
		return MS::kSuccess;
	}
	return MS::kFailure;
}
// CMD
inline MStatus plugFromName(MString name, MPlug & plug)
{
	MSelectionList tempList;
    tempList.add( name );
    if ( tempList.length() > 0 ) 
	{
		tempList.getPlug( 0, plug );
		return MS::kSuccess;
	}
	return MS::kFailure;
}
