#include "AnimPathShapeUI.h"
#include "AnimPathShapeNode.h"

#define ACTIVE_VERTEX_COLOR		16	// yellow

#define LEAD_COLOR				18	// green
#define ACTIVE_COLOR			15	// white
#define ACTIVE_AFFECTED_COLOR	8	// purple
#define DORMANT_COLOR			4	// blue
#define HILITE_COLOR			17	// pale blue

AnimPathShapeUI::AnimPathShapeUI() 
{
}
AnimPathShapeUI::~AnimPathShapeUI() 
{
}

void* AnimPathShapeUI::creator()
{
	return new AnimPathShapeUI();
}

void AnimPathShapeUI::getDrawRequests( 
	const MDrawInfo & info,
	bool objectAndActiveOnly,
	MDrawRequestQueue & queue )
{
	M3dView::DisplayStatus DisplayStatus = info.displayStatus();
//	displayStringD("AnimPathShapeUI::getDrawRequests 0x%x", DisplayStatus);
	// The draw data is used to pass geometry through the 
	// draw queue. The data should hold all the information
	// needed to draw the shape.
	//
	AnimPathShape* shapeNode = (AnimPathShape*)surfaceShape();
	shapeNode->updateGeometry();

	bool bSelectTangentAlways = shapeNode->tangentsAlways();
	MotionGeom* mGeom = shapeNode->getCachedGeometry();
	if( !mGeom)
		return;

	bool bDrawNumbers = shapeNode->isDrawNumbers();
	if( DisplayStatus==M3dView::kHilite)
		bDrawNumbers = true;

	MDrawRequest request = info.getPrototype( *this );
	MDrawData data;
	getDrawData( mGeom, data );
	request.setDrawData( data );

	
	request.setToken( 
		kDrawWireframe|
		(bSelectTangentAlways?kDrawTangents:0)|
		(bDrawNumbers?kDrawNumbers:0));
	setRequestColor(request, info);
	queue.add( request );

	if ( !objectAndActiveOnly ) 
	{
		// Active components
		//
		if ( surfaceShape()->hasActiveComponents() ) 
		{
			MDrawRequest activeVertexRequest = info.getPrototype( *this ); 
			activeVertexRequest.setDrawData( data );
		    activeVertexRequest.setToken( 
				kDrawVertices| (bSelectTangentAlways?kDrawTangents:0)
				);
		    activeVertexRequest.setColor( ACTIVE_VERTEX_COLOR,
										  M3dView::kActiveColors );

		    MObjectArray clist = surfaceShape()->activeComponents();
		    MObject vertexComponent = clist[0]; // Should filter list
		    activeVertexRequest.setComponent( vertexComponent );

			queue.add( activeVertexRequest );
		}
	}
}

void AnimPathShapeUI::draw( 
	const MDrawRequest & request, 
	M3dView & view ) const
{ 	
	MDrawData data = request.drawData();
	MotionGeom* geom = (MotionGeom*)data.geometry();
	short token = request.token();
	bool drawNumbers = (token & (int)kDrawNumbers)!=0;
	token &= ~kDrawNumbers;
	bool drawTangents = (token & (int)kDrawTangents)!=0;
	token &= ~kDrawTangents;

	view.beginGL(); 

	int i;
	switch( token )
	{
		case kDrawWireframe:
			geom->draw(view, drawNumbers);
			if( drawTangents)
			{
				float lastPointSize;
				glGetFloatv( GL_POINT_SIZE, &lastPointSize );
				glPointSize( 4.0 );

				for(i=0; i<geom->getVertexCount(); i++)
					geom->drawTangents(i, true);

				glPointSize( lastPointSize);
			}
			break;
		case kDrawVertices:
			drawVertices(request, view);
			break;
	}
	view.endGL(); 
}

void AnimPathShapeUI::drawVertices( 
	const MDrawRequest & request,
	M3dView & view ) const
{
	MDrawData data = request.drawData();
	MotionGeom* geom = (MotionGeom*)data.geometry();

	bool bSelectTangentAlways = (request.token() & kDrawTangents)!=0;

	bool lightingWasOn = glIsEnabled( GL_LIGHTING ) ? true : false;
	if ( lightingWasOn ) 
	{
		glDisable( GL_LIGHTING );
	}
	float lastPointSize;
	glGetFloatv( GL_POINT_SIZE, &lastPointSize );
	glPointSize( 4.0 );

	MObject comp = request.component();
	if ( ! comp.isNull() ) 
	{
		MFnSingleIndexedComponent fnComponent( comp );
		for ( int i=0; i<fnComponent.elementCount(); i++ )
		{
			int index = fnComponent.element( i );

			enVertexTangentType vttype = geom->getVertexTangentType(index);
			index = index%geom->getVertexCount();
			if(vttype==vtt_vertex)
			{
				view.setDrawColor(ACTIVE_VERTEX_COLOR);
				geom->drawVertex(index);
				view.setDrawColor(ACTIVE_AFFECTED_COLOR);
				if( !bSelectTangentAlways)
				{
					geom->drawInTangent(index, true, true);
					geom->drawOutTangent(index, true, true);
				}
			}
			else if(vttype==vtt_intangent)
			{
				view.setDrawColor(ACTIVE_VERTEX_COLOR);
				geom->drawInTangent(index, true, true);
				view.setDrawColor(ACTIVE_AFFECTED_COLOR);
				if( !bSelectTangentAlways)
				{
					geom->drawOutTangent(index, true, true);
				}
			}
			else if(vttype==vtt_outtangent)
			{
				view.setDrawColor(ACTIVE_VERTEX_COLOR);
				geom->drawOutTangent(index, true, true);
				view.setDrawColor(ACTIVE_AFFECTED_COLOR);
				if( !bSelectTangentAlways)
				{
					geom->drawInTangent(index, true, true);
				}
			}
		}
	}
	// Restore the state
	//
	if ( lightingWasOn ) 
	{
		glEnable( GL_LIGHTING );
	}
	glPointSize( lastPointSize );

}


bool AnimPathShapeUI::select( 
	MSelectInfo &selectInfo,
	MSelectionList &selectionList,
	MPointArray &worldSpaceSelectPts ) const
{
	bool selected = false;
	bool componentSelected = false;
	bool hilited = false;

	AnimPathShape* shapeNode = (AnimPathShape*)surfaceShape();
	bool bSelectTangentAlways = shapeNode->tangentsAlways();

	hilited = (selectInfo.displayStatus() == M3dView::kHilite);
	if ( hilited ) 
	{
		componentSelected = selectVertices( selectInfo, selectionList,
											worldSpaceSelectPts, bSelectTangentAlways );
		selected = selected || componentSelected;
	}

	if ( !selected )
	{
		AnimPathShape* meshNode = (AnimPathShape*)surfaceShape();
		MotionGeom* geom = meshNode->getCachedGeometry();

		MSelectionMask priorityMask( MSelectionMask::kSelectCurves );
		MSelectionList item;
		item.add( selectInfo.selectPath() );
		MPoint xformedPt;
		if ( selectInfo.singleSelection() ) 
		{
			MPoint center = meshNode->boundingBox().center();
			xformedPt = center;
			xformedPt *= selectInfo.selectPath().inclusiveMatrix();
		}

		M3dView view = selectInfo.view();
		view.beginSelect();

		if( geom)
			geom->draw(view, false);

		if( view.endSelect()>0)
		{
			selectInfo.addSelection( item, xformedPt, selectionList,
									worldSpaceSelectPts, priorityMask, false );
			selected = true;
		}
	}
	return selected;
}

void selectPoint(
	bool singleSelection, 
	MPoint currentPoint, int vertexIndex, MMatrix alignmentMatrix,
	double& previousZ, int& closestPointVertexIndex, MPoint& singlePoint,
	MFnSingleIndexedComponent& fnComponent)
{
	if ( singleSelection ) 
	{
		MPoint xformedPoint = currentPoint;
		xformedPoint.homogenize();
		xformedPoint*= alignmentMatrix;
		double z = xformedPoint.z;
		if ( closestPointVertexIndex < 0 || z > previousZ ) 
		{
			closestPointVertexIndex = vertexIndex;
			singlePoint = currentPoint;
			previousZ = z;
		}
	} 
	else 
	{
		// multiple selection, store all elements
		//
		fnComponent.addElement( vertexIndex );
	}
}

bool AnimPathShapeUI::selectVertices( 
	MSelectInfo &selectInfo,
	MSelectionList &selectionList,
	MPointArray &worldSpaceSelectPts,
	bool bSelectTangentAlways) const
{
	bool selected = false;
	M3dView view = selectInfo.view();

	MPoint 		xformedPoint;
	MPoint 		currentPoint;
	MPoint 		selectionPoint;
	double		previousZ = 0.0;
 	int			closestPointVertexIndex = -1;

	const MDagPath & path = selectInfo.multiPath();

	// Create a component that will store the selected vertices
	//
	MFnSingleIndexedComponent fnComponent;
	MObject surfaceComponent = fnComponent.create( MFn::kMeshVertComponent );
	int vertexIndex;

	// if the user did a single mouse click and we find > 1 selection
	// we will use the alignmentMatrix to find out which is the closest
	//
	MMatrix	alignmentMatrix;
	MPoint singlePoint; 
	bool singleSelection = selectInfo.singleSelection();
	if( singleSelection ) 
	{
		alignmentMatrix = selectInfo.getAlignmentMatrix();
	}

	// Get the geometry information
	//
	AnimPathShape* meshNode = (AnimPathShape*)surfaceShape();
	MotionGeom* geom = meshNode->getCachedGeometry();
	if( !geom) return false;

	// SELECT TANGENTS
	std::set<int> selVerts;
	if( bSelectTangentAlways)
	{
		for(int i=0; i<geom->getVertexCount(); i++)
		{
			selVerts.insert(i);
		}
	}
	else if ( surfaceShape()->hasActiveComponents() ) 
	{
		MObjectArray clist = surfaceShape()->activeComponents();
		MObject vertexComponent = clist[0]; // Should filter list

		if ( vertexComponent.hasFn(MFn::kSingleIndexedComponent) ) 
		{
			MFnSingleIndexedComponent fnVtxComp( vertexComponent );
			int len = fnVtxComp.elementCount();
			for ( int i = 0; i < len; i++ )
			{
				int vertexIndex = fnVtxComp.element(i);
				vertexIndex = vertexIndex % geom->getVertexCount();
				selVerts.insert(vertexIndex);

				displayStringD("SELECTED %d", vertexIndex);
			}
		}
	}

	// SELECT VERTECIES
	if( !selected)
	{
		std::set<int>::iterator it = selVerts.begin();
		for(;it!=selVerts.end(); it++)
		{
			int vertexIndex = *it;
			view.beginSelect();
			geom->drawInTangent(vertexIndex);
			if ( view.endSelect() > 0 )	// Hit count > 0
			{
				MPoint currentPoint = geom->getInTangent(vertexIndex);
				selected = true;
				selectPoint(
					singleSelection, currentPoint, vertexIndex+geom->getVertexCount(), alignmentMatrix,
					previousZ, closestPointVertexIndex, singlePoint, fnComponent);
				displayStringD("SELECT IN TANGENT %d", vertexIndex);
			}

			view.beginSelect();
			geom->drawOutTangent(vertexIndex);
			if ( view.endSelect() > 0 )	// Hit count > 0
			{
				MPoint currentPoint = geom->getOutTangent(vertexIndex);
				selected = true;
				selectPoint(
					singleSelection, currentPoint, vertexIndex+geom->getVertexCount()*2, alignmentMatrix,
					previousZ, closestPointVertexIndex, singlePoint, fnComponent);
				displayStringD("SELECT OUT TANGENT %d", vertexIndex);
			}

			/*/
			view.beginSelect();
			geom->drawVertex(vertexIndex);
			if ( view.endSelect() > 0 )	// Hit count > 0
			{
				MPoint currentPoint = geom->getVertex(vertexIndex);
				selected = true;
				selectPoint(
					singleSelection, currentPoint, vertexIndex, alignmentMatrix,
					previousZ, closestPointVertexIndex, singlePoint, fnComponent);

				displayStringD("SELECT POINT %d", vertexIndex);
			}
			/*/
		}

		for ( vertexIndex=0; vertexIndex<geom->getVertexCount(); vertexIndex++ )
		{
			MPoint currentPoint = geom->getVertex(vertexIndex);

			// Sets OpenGL's render mode to select and stores
			// selected items in a pick buffer
			//
			view.beginSelect();
			geom->drawVertex(vertexIndex);
			if ( view.endSelect() > 0 )	// Hit count > 0
			{
				selected = true;
				selectPoint(
					singleSelection, currentPoint, vertexIndex, alignmentMatrix,
					previousZ, closestPointVertexIndex, singlePoint, fnComponent);

				displayStringD("SELECT POINT %d", vertexIndex);
			}
		}
	}

	// If single selection, insert the closest point into the array
	//
	if ( selected && selectInfo.singleSelection() ) 
	{
		fnComponent.addElement(closestPointVertexIndex);

		// need to get world space position for this vertex
		//
		selectionPoint = singlePoint;
		selectionPoint *= path.inclusiveMatrix();
	}

	// Add the selected component to the selection list
	//
	if ( selected ) 
	{
		MSelectionList selectionItem;
		selectionItem.add( path, surfaceComponent );

		/*/
		�������� �� ��������?
		int elemCount = fnComponent.elementCount();
		for ( int idx=0; idx<elemCount; idx++ )
		{
			int elemIndex = fnComponent.element( idx );
			meshNode->selectVertexTangent(elemIndex);
		}
		/*/


		MSelectionMask mask( MSelectionMask::kSelectComponentsMask );
		selectInfo.addSelection(
			selectionItem, selectionPoint,
			selectionList, worldSpaceSelectPts,
			mask, true );
	}

	return selected;
}


void AnimPathShapeUI::setRequestColor( 
	MDrawRequest& request,
	const MDrawInfo& info )
{
	M3dView::DisplayStatus displayStatus = info.displayStatus();
	M3dView::ColorTable activeColorTable = M3dView::kActiveColors;
	M3dView::ColorTable dormantColorTable = M3dView::kDormantColors;
	switch ( displayStatus )
	{
		case M3dView::kLead :
			request.setColor( LEAD_COLOR, activeColorTable );
			break;
		case M3dView::kActive :
			request.setColor( ACTIVE_COLOR, activeColorTable );
			break;
		case M3dView::kActiveAffected :
			request.setColor( ACTIVE_AFFECTED_COLOR, activeColorTable );
			break;
		case M3dView::kDormant :
			request.setColor( DORMANT_COLOR, dormantColorTable );
			break;
		case M3dView::kHilite :
			request.setColor( HILITE_COLOR, activeColorTable );
			break;
		default:	
			break;
	}
}

