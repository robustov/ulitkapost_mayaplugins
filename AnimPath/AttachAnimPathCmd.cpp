#include "AttachAnimPathCMD.h"
#include <maya/MArgList.h>
#include <maya/MObject.h>
#include <maya/MDGModifier.h>
#include <maya/MGlobal.h>
#include <maya/MSelectionList.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MPlug.h>
#include <maya/MCommandResult.h>
#include <maya/MStringArray.h>
#include <maya/MFnDagNode.h>
#include <stdio.h>
#include "pre.h"

//string $nname[] = eval("deformer -type Water2dDeformer");
//eval("connectAttr time1.o " + $nname[0] + ".time");
//
//MStatus nodeFromName(MString name, MObject & obj);
MStatus nodeFromArg(MObject & obj, const MArgList& args, int index);

void* AttachAnimPathCMD::creator()										
{																
	return new AttachAnimPathCMD;										
}																

MStatus	AttachAnimPathCMD::doIt( const MArgList& args)
{
	MStatus stat;
	if( args.length()!=1)
		return MS::kFailure;

	MObject animObj;
	if( !nodeFromArg( animObj, args, 0))
		return MS::kFailure;

	/*/
	MString fabricName = args.asString( 0, &stat );

	char buf[1024];
	MCommandResult result;
	// "deformer "
	_snprintf(buf, 1024, "select %s", fabricName.asChar());
	if( !MGlobal::executeCommand(buf, true))
		return MS::kFailure;
	_snprintf(buf, 1024, "deformer -type Water2dDeformer");
	if( !MGlobal::executeCommand(buf, result, true))
		return MS::kFailure;

	MStringArray deformerName;
	result.getResult(deformerName);
	MGlobal::displayInfo( deformerName[0]);
	MObject deformer;
	if( !nodeFromName(deformerName[0], deformer))
		return MS::kFailure;
	_snprintf(buf, 1024, "connectAttr time1.o %s.time", deformerName[0].asChar());
	if( !MGlobal::executeCommand(buf, result, true))
		return MS::kFailure;

	MFnDependencyNode dnDeformer(deformer);
	MFnDagNode dnEmitter(emitterObj);
	MObject emitterShape = MObject::kNullObj;
	for( unsigned i=0; i<dnEmitter.childCount(); i++)
	{
		emitterShape = dnEmitter.child(i);
		if( emitterShape.hasFn(MFn::kMesh))
			break;
		emitterShape = MObject::kNullObj;
	}
	if( emitterShape == MObject::kNullObj)
	{
		MGlobal::displayInfo( "can't find MFn::kMesh shape");
		return MS::kFailure;
	}
	MFnDependencyNode dnEmitterShape(emitterShape);

	MDGModifier dgmodifer;
	
	if( !dgmodifer.connect( dnEmitterShape.findPlug("o"), dnDeformer.findPlug("emitter")))
		return MS::kFailure;
	if( !dgmodifer.connect( dnEmitter.findPlug("worldMatrix"), dnDeformer.findPlug("emittertransform")))
		return MS::kFailure;
	if( !dgmodifer.doIt())
		return MS::kFailure;

	/*/
	return MS::kSuccess;
}
/*/
// CMD
MStatus nodeFromName(MString name, MObject & obj)
{
	MSelectionList tempList;
    tempList.add( name );
    if ( tempList.length() > 0 ) 
	{
		tempList.getDependNode( 0, obj );
		return MS::kSuccess;
	}
	return MS::kFailure;
}
/*/
MStatus nodeFromArg(MObject & obj, const MArgList& args, int index)
{
	MStatus stat;
	if( (int)args.length() <= index )
	{
		MGlobal::displayError( "Failed to parse particle node name argument." );
		return MS::kFailure;
	}
	MString particleName = args.asString( index, &stat );
	if (stat != MS::kSuccess)
	{
		MGlobal::displayError( "Failed to parse particle node name argument." );
		return MS::kFailure;
	}
	stat = nodeFromName( particleName, obj );
	return MS::kSuccess;
}
