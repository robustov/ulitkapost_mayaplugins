#pragma once

#include <maya/MPxGeometryIterator.h>
#include <maya/MPoint.h>
#include "pre.h"
#include "MotionGeom.h"

class MotionGeomIterator : public MPxGeometryIterator
{
public:
	MotionGeomIterator( void * userGeometry, MObjectArray & components );
	MotionGeomIterator( void * userGeometry, MObject & components );

    //////////////////////////////////////////////////////////
	//
	// Overrides
	//
    //////////////////////////////////////////////////////////

	virtual void		reset();
	virtual MPoint		point() const;
	virtual void		setPoint( const MPoint & ) const;
	virtual int			iteratorCount() const;
	virtual bool        hasPoints() const;

public:
	MotionGeom* 		geometry;
};
