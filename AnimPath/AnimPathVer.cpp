//
// Copyright (C) 
// File: AnimPathVerCmd.cpp
// MEL Command: AnimPathVer

#include "AnimPathVer.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>

AnimPathVer::AnimPathVer()
{
}
AnimPathVer::~AnimPathVer()
{
}

MSyntax AnimPathVer::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

//	syntax.addArg(MSyntax::kString);	// arg0
//	syntax.addFlag("f", "format", MSyntax::kString);
	return syntax;
}

MStatus AnimPathVer::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

//	MString argsl0;
//	argData.getCommandArgument(0, argsl0);

#ifdef ANIM_PATH_FREEVER
	setResult(1);
#else
	setResult(0);
#endif

	return MS::kSuccess;
}

void* AnimPathVer::creator()
{
	return new AnimPathVer();
}

