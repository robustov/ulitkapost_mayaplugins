#pragma once

#include <maya/MPxCommand.h>

class AttachAnimPathCMD : public MPxCommand 
{	
public:
	static void* creator();
	MStatus	doIt( const MArgList& args);
};
