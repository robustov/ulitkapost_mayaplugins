#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: AnimPathBuyItCmd.h
// MEL Command: AnimPathBuyIt

#include <maya/MPxCommand.h>

class AnimPathBuyIt : public MPxCommand
{
public:
	AnimPathBuyIt();
	virtual	~AnimPathBuyIt();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();

	static MString name;
};

