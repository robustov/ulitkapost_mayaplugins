;InstallOptions Test Script
;Written by Ramon
;This script demonstrates the power of the new control "LINK"
;that allows you to execute files, send mails, open wepsites, etc.
;--------------------------

!define TEMP1 $R0 ;Temp variable

; The name of the installer
Name "AnimPath For Maya"

; The file to write
OutFile "$%SASBIN%\AnimPathSetup.exe"

;--------------------------------
ReserveFile "${NSISDIR}\Plugins\InstallOptions.dll"
ReserveFile "AnimPathInstaller.ini"
ReserveFile "AnimPathInstaller2.ini"
ReserveFile "AnimPathInstaller.bmp"

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDir "$PROGRAMFILES\SAS"
;InstallDirRegKey HKLM "Software\AnimPath" "Install_Dir"

;--------------------------------

; Pages

Page custom Start
Page custom SetCustom
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

Function .onInit
	InitPluginsDir
	File /oname=$PLUGINSDIR\test_start.ini "AnimPathInstaller.ini"
	File /oname=$PLUGINSDIR\test.ini "AnimPathInstaller2.ini"
	File /oname=$PLUGINSDIR\AnimPathInstaller.bmp "AnimPathInstaller.bmp"
;	MessageBox MB_OK $PLUGINSDIR

FunctionEnd

Function Start
	WriteINIStr $PLUGINSDIR\test_start.ini "Field 1" "Text" "$PLUGINSDIR\AnimPathInstaller.bmp"
	Push ${TEMP1}
		InstallOptions::dialog "$PLUGINSDIR\test_start.ini"
    Pop ${TEMP1}

  Pop ${TEMP1}
FunctionEnd

Function SetCustom

	ReadRegStr $R0 HKLM "SOFTWARE\Alias|Wavefront\Maya\5.0\Setup\InstallPath" "MAYA_INSTALL_LOCATION" 
	IfErrors aaa
		StrCpy $R0 "$R0bin\plug-ins"
		WriteIniStr $PLUGINSDIR\test.ini "Field 1" "State" $R0
	aaa:

	ReadRegStr $R0 HKLM "SOFTWARE\Alias|Wavefront\Maya\6.0\Setup\InstallPath" "MAYA_INSTALL_LOCATION" 
	IfErrors xxx
	
		StrCpy $R0 "$R0bin\plug-ins"
		WriteIniStr $PLUGINSDIR\test.ini "Field 2" "State" $R0
	xxx:

	ReadRegStr $R0 HKLM "SOFTWARE\Alias|Wavefront\Maya\6.5\Setup\InstallPath" "MAYA_INSTALL_LOCATION" 
	IfErrors yyy
		StrCpy $R0 "$R0bin\plug-ins"
		WriteIniStr $PLUGINSDIR\test.ini "Field 3" "State" $R0
	yyy:

	ReadRegStr $R0 HKLM "SOFTWARE\Alias|Wavefront\Maya\7.0\Setup\InstallPath" "MAYA_INSTALL_LOCATION" 
	IfErrors zzz
		StrCpy $R0 "$R0bin\plug-ins"
		WriteIniStr $PLUGINSDIR\test.ini "Field 4" "State" $R0
	zzz:

	ReadRegStr $R0 HKLM "SOFTWARE\Alias|Wavefront\Maya\8.0\Setup\InstallPath" "MAYA_INSTALL_LOCATION" 
	IfErrors zzz2
		StrCpy $R0 "$R0bin\plug-ins"
		WriteIniStr $PLUGINSDIR\test.ini "Field 9" "State" $R0
	zzz2:

	ReadRegStr $R0 HKLM "SOFTWARE\Autodesk\Maya\8.5\Setup\InstallPath" "MAYA_INSTALL_LOCATION" 
	IfErrors zzz3
		StrCpy $R0 "$R0bin\plug-ins"
		WriteIniStr $PLUGINSDIR\test.ini "Field 11" "State" $R0
	zzz3:

	Push ${TEMP1}
		InstallOptions::dialog "$PLUGINSDIR\test.ini"
    Pop ${TEMP1}

  Pop ${TEMP1}

FunctionEnd


;--------------------------------

; The stuff to install
Section "AnimPath"

  SectionIn RO
   ; Set output path to the installation directory.
  SetOutPath $INSTDIR
 
  DeleteRegValue HKLM SOFTWARE\AnimPath "5.0" 
  ReadINIStr ${TEMP1} "$PLUGINSDIR\test.ini" "Field 1" "State"
  StrCmp ${TEMP1} "" l0
	  File /oname=${TEMP1}\AnimPath.mll "$%SASBIN%\bin\AnimPath50.mll"
	  WriteRegStr HKLM SOFTWARE\AnimPath "5.0" ${TEMP1}\AnimPath.mll
  l0:
  
  DeleteRegValue HKLM SOFTWARE\AnimPath "6.0" 
  ReadINIStr ${TEMP1} "$PLUGINSDIR\test.ini" "Field 2" "State"
  StrCmp ${TEMP1} "" l1
	  File /oname=${TEMP1}\AnimPath.mll "$%SASBIN%\bin\AnimPath60.mll"
	  WriteRegStr HKLM SOFTWARE\AnimPath "6.0" ${TEMP1}\AnimPath.mll
  l1:
  
  DeleteRegValue HKLM SOFTWARE\AnimPath "6.5" 
  ReadINIStr ${TEMP1} "$PLUGINSDIR\test.ini" "Field 3" "State"
  StrCmp ${TEMP1} "" l2
	  File /oname=${TEMP1}\AnimPath.mll "$%SASBIN%\bin\AnimPath65.mll"
	  WriteRegStr HKLM SOFTWARE\AnimPath "6.5" ${TEMP1}\AnimPath.mll
  l2:

  DeleteRegValue HKLM SOFTWARE\AnimPath "7.0" 
  ReadINIStr ${TEMP1} "$PLUGINSDIR\test.ini" "Field 4" "State"
  StrCmp ${TEMP1} "" l3
	  File /oname=${TEMP1}\AnimPath.mll "$%SASBIN%\bin\AnimPath70.mll"
	  WriteRegStr HKLM SOFTWARE\AnimPath "7.0" ${TEMP1}\AnimPath.mll
  l3:
  
  DeleteRegValue HKLM SOFTWARE\AnimPath "8.0" 
  ReadINIStr ${TEMP1} "$PLUGINSDIR\test.ini" "Field 9" "State"
  StrCmp ${TEMP1} "" l4
	  File /oname=${TEMP1}\AnimPath.mll "$%SASBIN%\bin\AnimPath80.mll"
	  WriteRegStr HKLM SOFTWARE\AnimPath "8.0" ${TEMP1}\AnimPath.mll
  l4:

  DeleteRegValue HKLM SOFTWARE\AnimPath "8.5" 
  ReadINIStr ${TEMP1} "$PLUGINSDIR\test.ini" "Field 11" "State"
  StrCmp ${TEMP1} "" l5
	  File /oname=${TEMP1}\AnimPath.mll "$%SASBIN%\bin\AnimPath85.mll"
	  WriteRegStr HKLM SOFTWARE\AnimPath "8.5" ${TEMP1}\AnimPath.mll
  l5:
  
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\AnimPath "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\AnimPath" "DisplayName" "AnimPath For Maya"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\AnimPath" "UninstallString" '"$INSTDIR\uninstallAnimPath.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\AnimPath" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\AnimPath" "NoRepair" 1
  WriteUninstaller "uninstallAnimPath.exe"
  
SectionEnd

;--------------------------------
; Uninstaller
Section "Uninstall"

; Remove files
  ReadRegStr $R0 HKLM SOFTWARE\AnimPath "5.0" 
  IfErrors l0
	Delete $R0
  l0:	

  ReadRegStr $R0 HKLM SOFTWARE\AnimPath "6.0" 
  IfErrors l1
	Delete $R0
  l1:	

  ReadRegStr $R0 HKLM SOFTWARE\AnimPath "6.5" 
  IfErrors l2
	Delete $R0
  l2:	
  
  ReadRegStr $R0 HKLM SOFTWARE\AnimPath "7.0" 
  IfErrors l3
	Delete $R0
  l3:	
	
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\AnimPath"
  DeleteRegKey HKLM SOFTWARE\AnimPath

  ; Remove uninstaller
  Delete $INSTDIR\uninstallAnimPath.exe

  ; Remove directories used

SectionEnd
