//
// Copyright (C) 
// File: AnimPathBuyItCmd.cpp
// MEL Command: AnimPathBuyIt

#include "AnimPathBuyIt.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>

AnimPathBuyIt::AnimPathBuyIt()
{
}
AnimPathBuyIt::~AnimPathBuyIt()
{
}

MSyntax AnimPathBuyIt::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

//	syntax.addArg(MSyntax::kString);	// arg0
//	syntax.addFlag("f", "format", MSyntax::kString);
	return syntax;
}

#ifdef UNICODE
    typedef HINSTANCE (WINAPI* LPShellExecute)(HWND hwnd, LPCWSTR lpOperation, LPCWSTR lpFile, LPCWSTR lpParameters, LPCWSTR lpDirectory, INT nShowCmd);
#else
    typedef HINSTANCE (WINAPI* LPShellExecute)(HWND hwnd, LPCSTR lpOperation, LPCSTR lpFile, LPCSTR lpParameters, LPCSTR lpDirectory, INT nShowCmd);
#endif

MStatus AnimPathBuyIt::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

//	::system("iexplore ");
//	MString argsl0;
//	argData.getCommandArgument(0, argsl0);

	char* strReadmePath = "http://www.shareit.com/product.html?cart=1&productid=300038001&stylefrom=300038001&nolselection=1&sessionid=257418460&random=a3dfaad9b0ee51af1b11e4364401fc57";
	HINSTANCE hInstShell32 = LoadLibrary(TEXT("shell32.dll"));
	if (hInstShell32 != NULL)
	{
		bool bSuccess = false;
		LPShellExecute pShellExecute = NULL;
#ifdef UNICODE
		pShellExecute = (LPShellExecute)GetProcAddress(hInstShell32, _TWINCE("ShellExecuteW"));
#else
		pShellExecute = (LPShellExecute)GetProcAddress(hInstShell32, "ShellExecuteA");
#endif
		if( pShellExecute != NULL )
		{
			if( pShellExecute( NULL, TEXT("open"), strReadmePath, NULL, NULL, SW_SHOW ) > (HINSTANCE) 32 )
				bSuccess = true;
		}
		FreeLibrary(hInstShell32);
	}
	return MS::kSuccess;
}

void* AnimPathBuyIt::creator()
{
	return new AnimPathBuyIt();
}

