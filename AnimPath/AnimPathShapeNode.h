#ifndef _AnimPathShapeNode
#define _AnimPathShapeNode
//
// Copyright (C) 
// 
// File: AnimPathShapeNode.h
//
// Dependency Graph Node: AnimPathShape
//
// Author: Maya Plug-in Wizard 2.0
//

#include "pre.h"
#include "MotionGeom.h"

 
class AnimPathShape : public MPxSurfaceShape
{
public:
	AnimPathShape();
	virtual ~AnimPathShape(); 
	///
	virtual void postConstructor();

	virtual bool			getInternalValue( const MPlug&,
											  MDataHandle&);
    virtual bool			setInternalValue( const MPlug&,
											  const MDataHandle&);

	virtual MStatus compute( const MPlug& plug, MDataBlock& data );

	static  void* creator();
	static  MStatus initialize();

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

public:
	// Attribute to component (components)
	//
	void componentToPlugs( 
		MObject &,
		MSelectionList & ) const;
	MatchResult matchComponent( 
		const MSelectionList& item,
		const MAttributeSpecArray& spec,
		MSelectionList& list );
	bool match(	
		const MSelectionMask & mask,
		const MObjectArray& componentList ) const;
	///
	MStatus connectionMade( const MPlug& plug, const MPlug& otherPlug, bool asSrc );
	MStatus connectionBroken( const MPlug& plug, const MPlug& otherPlug, bool asSrc );

public:
	void transformUsing( 
		const MMatrix & mat,
		const MObjectArray & componentList,
		MPxSurfaceShape::MVertexCachingMode cachingMode,
		MPointArray* pointCache);

public:
	bool deleteComponents( 
		const MObjectArray& componentList,
		MDoubleArray& undoInfo );
	bool undeleteComponents( 
		const MObjectArray& componentList,
		MDoubleArray& undoInfo ); 

public:
	// Associates a user defined iterator with the shape (components)
	//
	MPxGeometryIterator* geometryIteratorSetup( 
		MObjectArray&, MObject&,
		bool forReadOnly = false );
	bool acceptsGeometryIterator( 
		bool  writeable=true );
	bool acceptsGeometryIterator( 
		MObject&, 
		bool writeable=true,
		bool forReadOnly = false);


public:
	// Update
	void updateGeometry();
	// MotionGeom
	MotionGeom* getCachedGeometry();
	// 
	bool isDrawNumbers();
	double scaleTangents();
	bool tangentsAlways();

	void selectVertexTangent(int index);

public:

	// There needs to be a MObject handle declared for each attribute that
	// the node will have.  These handles are needed for getting and setting
	// the values later.
	//

	static	MObject		i_AnimCurveX;
	static	MObject		i_AnimCurveY;
	static	MObject		i_AnimCurveZ;
	static	MObject		i_WorldMatrix;
	static	MObject		i_ParentInverseMatrix;

	static	MObject		i_TimeStep;
	static	MObject		i_bNumbers;
	static	MObject		i_bShowTimeline;
	static	MObject		i_scaleTangents;
	static	MObject		i_bTangentsAlways;

	// The typeid is a unique 32bit indentifier that describes this node.
	// It is used to save and retrieve nodes of this type from the binary
	// file format.  If it is not unique, it will cause file IO problems.
	//
	static MTypeId id;
	static MString name;

public:
	bool bNeedReload;

protected:
	MotionGeom geometry;

	MBoundingBox box;

	MBoundingBox computeBoundingBox();

	MMatrix getParentInverseMatrix( MTime t);

	MCallbackId AnimCallbackId;
	static void AnimMessageCallback( MObjectArray &editedCurves, void *clientData );

	#if MAYA_API_VERSION > 600
	MCallbackId AnimKeyframeCallbackId;
	static void AnimKeyframeEditedCallback( MObjectArray &editedCurves, void *clientData );
	#endif

	bool IsMineCurve(MObjectArray &editedCurves);

	MCallbackId NodeCallbackId;
	MCallbackId TranslateNodeCallbackId;
	static void ObjectAttributeChange(
		MNodeMessage::AttributeMessage msg,
		MPlug & plug,
		MPlug & otherPlug,
		void* clientData);
	static void TranslateAttributeChange(
		MNodeMessage::AttributeMessage msg,
		MPlug & plug,
		MPlug & otherPlug,
		void* clientData);
public:
	static MObject getAnimCurveForPlug(MPlug pl);
	static MMatrix getParentInverseMatrix( MObject obj, MTime t);
};

#endif
