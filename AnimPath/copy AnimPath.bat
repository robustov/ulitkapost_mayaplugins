set BIN_DIR=d:\out\AnimPath
set BIN_DIR=\\server\bin70

rem mkdir %BIN_DIR%
rem mkdir %BIN_DIR%\bin50
rem mkdir %BIN_DIR%\bin60
rem mkdir %BIN_DIR%\bin65
rem mkdir %BIN_DIR%\bin70
rem mkdir %BIN_DIR%\mel
rem mkdir %BIN_DIR%\docs

rem copy Bin\AnimPath50.mll					%BIN_DIR%\bin50\AnimPath.mll
rem copy Bin\AnimPath.mll					%BIN_DIR%\bin60\AnimPath.mll
rem copy Bin\AnimPath65.mll					%BIN_DIR%\bin65\AnimPath.mll

copy Bin\AnimPath.mll						%BIN_DIR%\bin\AnimPath.mll

rem copy Mel\AEAnimPathShapeTemplate.mel	%BIN_DIR%\mel
copy Mel\attachAnimPath.mel					%BIN_DIR%\mel

rem copy Docs\AnimPathReadMe.txt			%BIN_DIR%\docs
rem copy Docs\AnimPathReadMeRus.txt			%BIN_DIR%\docs

pause