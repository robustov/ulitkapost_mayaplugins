#include "AnimPathShapeNode.h"
#include "AnimPathShapeTransform.h"
#include "AnimPathBuyIt.h"

const unsigned long MITKA_ID_RANGE = 0xDA000;

MTypeId getId(MString key, unsigned long id)
{
	LONG   lResult;
	HKEY   hKey = NULL;

	MTypeId defval(MITKA_ID_RANGE + id);
	
	// Open the rdp-tcp key
	lResult = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
		TEXT("software\\mitka"),
		0, KEY_QUERY_VALUE | KEY_SET_VALUE, &hKey);
	if (lResult != ERROR_SUCCESS) 
	{
//		printf( TEXT("RegOpenKeyEx failed with error %u\n"), lResult);
//		printf( TEXT("%s = %x\n"), key.asChar(), MITKA_ID_RANGE+id);
		return defval;
	}

	// Query the Security value
	DWORD val; DWORD dwSize = sizeof(val);
	DWORD type;
	lResult = RegQueryValueEx(hKey, key.asChar(), NULL, &type, (LPBYTE)&val,
		&dwSize);
	if (lResult != ERROR_SUCCESS) 
	{
		RegCloseKey(hKey);
//		printf( TEXT("RegQueryValueEx %s failed with error %u\n"), key.asChar(), lResult);
//		printf( TEXT("%s = %x\n"), key.asChar(), MITKA_ID_RANGE+id);
		return defval;
	}
	if( type!= REG_DWORD)
	{
		RegCloseKey(hKey);
//		printf( TEXT("RegQueryValueEx %s failed! Value must have REG_DWORD\n"), key.asChar());
//		printf( TEXT("%s = %x\n"), key.asChar(), MITKA_ID_RANGE+id);
		return defval;
	}

	RegCloseKey(hKey);

//	printf( TEXT("%s = %x\n"), key.asChar(), val+id);

	MTypeId tval(val+id);
	return val;
}


MString AnimPathBuyIt::name = "AnimPathBuyIt";
MString AnimPathShapeTransform::name = "AnimPathShapeTransform";

MString AnimPathShape::name = "AnimPathShape";
MTypeId AnimPathShape::id = getId( AnimPathShape::name, 0x0);
#include "AnimPathVer.h"
const MString AnimPathVer::typeName( "AnimPathVer" );

