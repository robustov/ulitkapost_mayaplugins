//
// Copyright (C) 
// 
// File: pluginMain.cpp
//
// Author: Maya Plug-in Wizard 2.0
//

#include "AnimPathShapeNode.h"
#include "AnimPathShapeUI.h"
#include "AttachAnimPathCmd.h"
#include "AnimPathShapeTransform.h"

#include <maya/MFnPlugin.h>
#include "AnimPathVer.h"

MStatus SourceMelFromResource(HINSTANCE hModule, LPCSTR lpName, LPCSTR lpType)
{
	HRSRC res = ::FindResource( hModule, lpName, lpType);
	if( !res)
	{
		DWORD err = GetLastError();
		displayString("FindResource %x %s %s FAILED = %x", hModule, lpName, lpType, err);
		return MS::kFailure;
	}

	HGLOBAL mem = ::LoadResource(hModule, res);
	if(!mem)
	{
		DWORD err = GetLastError();
		displayString("LoadResource %x FAILED = %x", hModule, err);
		return MS::kFailure;
	}

	char* data = (char*)LockResource(mem);
	if(!data)
		return MS::kFailure;

	MString str(data);
	MStatus stat = MGlobal::executeCommand(str);
	return stat;
}

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
//
//	Description:
//		this method is called when the plug-in is loaded into Maya.  It 
//		registers all of the services that this plug-in provides with 
//		Maya.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{ 
	MStatus   status, stat;
	MFnPlugin plugin( obj, "Dmitriy Robustov", "0.2", "Any");

// #include "AnimPathVer.h"
	stat = plugin.registerCommand( 
		AnimPathVer::typeName.asChar(), 
		AnimPathVer::creator, 
		AnimPathVer::newSyntax );
	if (!stat) 
	{
		displayString("cant register %s", AnimPathVer::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerCommand( AnimPathShapeTransform::name, AnimPathShapeTransform::creator, AnimPathShapeTransform::newSyntax );
	if (!stat) {
		displayString("cant register node AnimPathShapeTransform");
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

	status = plugin.registerShape(  AnimPathShape::name, 
									AnimPathShape::id, 
									AnimPathShape::creator,
									AnimPathShape::initialize, 
									&AnimPathShapeUI::creator);
	if (!status) {
		status.perror("registerNode");
		uninitializePlugin(obj);
		return status;
	}

	if( !SourceMelFromResource(MhInstPlugin, "AEAnimPathShapeTemplate", "MEL"))
	{
		displayString("Dont found AEAnimPathShapeTemplate.mel");
	}
	if( !SourceMelFromResource(MhInstPlugin, "attachAnimPath", "MEL"))
	{
		displayString("Dont found attachAnimPath.mel");
	}
	if( !MGlobal::executeCommand("AEAnimPathMenu"))
	{
		displayString("Dont found AEAnimPathMenu command");
	}

//	if( !MGlobal::sourceFile("AEAnimPathShapeTemplate.mel"))
//	{
//		displayString("Dont found AEAnimPathShapeTemplate.mel");
//	}
//	if( !MGlobal::executeCommand("AEAnimPathMenu"))
//	{
//		displayString("Dont found GlobalAnimPathMenu command");
//	}

	return status;
}

MStatus uninitializePlugin( MObject obj)
//
//	Description:
//		this method is called when the plug-in is unloaded from Maya. It 
//		deregisters all of the services that it was providing.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{
	MStatus   status, stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterCommand( "AnimPathVer" );
	if (!stat) displayString("cant deregister %s", AnimPathVer::typeName.asChar());

	plugin.deregisterNode( AnimPathShape::id );
	plugin.deregisterCommand( AnimPathShapeTransform::name );

	MGlobal::executeCommand("deleteUI AEAnimPathMenuItem");

	return status;
}
