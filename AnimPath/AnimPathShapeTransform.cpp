//
// Copyright (C) 
// File: AnimPathShapeTransformCmd.cpp
// MEL Command: AnimPathShapeTransform

#include "AnimPathShapeTransform.h"
#include "AnimPathShapeNode.h"
#include "MotionGeom.h"
#include <maya/MArgDatabase.h>
#include <maya/MGlobal.h>

AnimPathShapeTransform::AnimPathShapeTransform()
{
}
AnimPathShapeTransform::~AnimPathShapeTransform()
{
}

MSyntax AnimPathShapeTransform::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);	// arg0
//	syntax.addFlag("f", "format", MSyntax::kString);
	return syntax;
}

MStatus AnimPathShapeTransform::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString argsl0;
	argData.getCommandArgument(0, argsl0);

	MObject this_object;
	MPlug plug;
	if( !plugFromName(argsl0, plug))
		return MS::kFailure;
	this_object = plug.node();

	if( plug==AnimPathShape::i_bNumbers)
	{
		// refresh
		MFnDependencyNode dn(this_object);
		MPxNode* pxnode = dn.userNode(&stat);

		AnimPathShape* path = (AnimPathShape*)pxnode;
		path->bNeedReload = true;
		path->childChanged( MPxSurfaceShape::kObjectChanged );
	}
	else if( plug==AnimPathShape::worldMatrix)
	{
		// apply to AC
		MObject acX = AnimPathShape::getAnimCurveForPlug( MPlug(this_object, AnimPathShape::i_AnimCurveX));
		MObject acY = AnimPathShape::getAnimCurveForPlug( MPlug(this_object, AnimPathShape::i_AnimCurveY));
		MObject acZ = AnimPathShape::getAnimCurveForPlug( MPlug(this_object, AnimPathShape::i_AnimCurveZ));

		MPlug mMyPl(this_object, AnimPathShape::worldMatrix);
		MPlug wmPl(this_object, AnimPathShape::i_WorldMatrix);
		MPlug pimPl(this_object, AnimPathShape::i_ParentInverseMatrix);
		MMatrix mat = getMatrixForPlug(plug);
	//	MTransformationMatrix tm = MFnTransform(this_object).transformation();
	//	MMatrix mat = tm.asMatrix();

		MotionGeom geometry;
		MTime timestep((double )1);
		geometry.load(acX, acY, acZ, wmPl, pimPl, timestep, false, true, 1.0);

		for( int i=0; i<geometry.getVertexTangentCount(); i++)
		{
			int elemIndex = i;
			MPoint pt = geometry.getVertexTangent(elemIndex);
			MTime t = geometry.getVertexTangentTime(elemIndex);
			MMatrix m = AnimPathShape::getParentInverseMatrix(this_object, t);
			MVector ofs = pt*mat;

			int type = elemIndex/geometry.getVertexCount();
			int vi = elemIndex%geometry.getVertexCount();
			ofs = ofs - geometry.getVertex(vi);
			ofs *= m;
			if( vi==0 && type==1)
				continue;
			if( vi==geometry.getVertexCount()-1 && type==2)
				continue;
			geometry.updateVertexTangent(elemIndex, ofs, acX, acY, acZ);
		}

		MFnDagNode parentNode(MFnDagNode(this_object).parent(0));
//		MFnTransform tr(parentNode);

		MString cmd = "makeIdentity -a false -t 1 -r 1 -s 1 ";
		cmd += parentNode.name();
		MGlobal::executeCommand(cmd, true, true);
	}

	return MS::kSuccess;
}

MStatus AnimPathShapeTransform::undoIt( )
{
	return MPxCommand::undoIt( );
}
MStatus AnimPathShapeTransform::redoIt( )
{
	return MPxCommand::redoIt( );
}

void* AnimPathShapeTransform::creator()
{
	return new AnimPathShapeTransform();
}

