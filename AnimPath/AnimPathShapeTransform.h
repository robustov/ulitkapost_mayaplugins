#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: AnimPathShapeTransformCmd.h
// MEL Command: AnimPathShapeTransform

#include <maya/MPxCommand.h>

class AnimPathShapeTransform : public MPxCommand
{
public:
	AnimPathShapeTransform();
	virtual	~AnimPathShapeTransform();

	MStatus	doIt( const MArgList& );
	virtual MStatus   	undoIt( );
	virtual MStatus   	redoIt( );

	bool isUndoable() const{return true;};
	static MSyntax newSyntax();
	static void* creator();

	static MString name;
protected:

};

