#include "AnimPathShapeNode.h"
#include "MotionGeomIterator.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MAnimUtil.h>
#include <maya/MDistance.h>
#include <maya/MFnUnitAttribute.h>

#include <maya/MGlobal.h>

// You MUST change this to a unique value!!!  The id is a 32bit value used
// to identify this type of node in the binary file format.  
//

// Example attributes
// 
MObject AnimPathShape::i_AnimCurveX;
MObject AnimPathShape::i_AnimCurveY;
MObject AnimPathShape::i_AnimCurveZ;
MObject AnimPathShape::i_WorldMatrix;
MObject AnimPathShape::i_ParentInverseMatrix;
MObject AnimPathShape::i_TimeStep;
MObject AnimPathShape::i_bNumbers;
MObject AnimPathShape::i_bShowTimeline;
MObject AnimPathShape::i_scaleTangents;
MObject AnimPathShape::i_bTangentsAlways;



void AnimPathShape::AnimMessageCallback( MObjectArray &editedCurves, void *clientData )
{
	AnimPathShape* pThis = (AnimPathShape*)clientData;
	if( !pThis->IsMineCurve(editedCurves)) return;

	pThis->bNeedReload = true;
	pThis->childChanged(kObjectChanged);
	displayStringD("AnimPathShape::AnimMessageCallback");
}
#if MAYA_API_VERSION > 600
void AnimPathShape::AnimKeyframeEditedCallback( MObjectArray &editedCurves, void *clientData )
{
	AnimPathShape* pThis = (AnimPathShape*)clientData;
//	if( !pThis->IsMineCurve(editedCurves)) return;

	pThis->bNeedReload = true;
	pThis->childChanged(kObjectChanged);
	displayStringD("AnimPathShape::AnimKeyframeEditedCallback");
}
#endif

bool AnimPathShape::IsMineCurve(MObjectArray &editedCurves)
{
	MObject this_object = thisMObject();
	MObject acX = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveX));
	MObject acY = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveY));
	MObject acZ = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveZ));
	for(int i=0; i<(int)editedCurves.length(); i++)
	{
		if(editedCurves[i]==acX) return true;
		if(editedCurves[i]==acY) return true;
		if(editedCurves[i]==acZ) return true;
	}
	return false;
}


void AnimPathShape::ObjectAttributeChange(
	MNodeMessage::AttributeMessage msg,
	MPlug & plug,
	MPlug & otherPlug,
	void* clientData)
{
	displayStringD("ObjectAttributeChange %d", msg);
	AnimPathShape* pThis = (AnimPathShape*)clientData;
	pThis->bNeedReload = true;
}

// ������ �� ����� :( ���������
// �� �������� ����� ����!!!
void AnimPathShape::TranslateAttributeChange(
	MNodeMessage::AttributeMessage msg,
	MPlug & plug,
	MPlug & otherPlug,
	void* clientData)
{
	displayStringD("TranslateAttributeChange %x %s -> %s ", msg, 
		plug.name().asChar(), 
		otherPlug.name().asChar());

	if( (msg & MNodeMessage::kAttributeSet)==0)
		return;

	AnimPathShape* pThis = (AnimPathShape*)clientData;
	if( plug.partialName() == "t" ||
		plug.partialName() == "tx" ||
		plug.partialName() == "ty" ||
		plug.partialName() == "tz")
	{
//		pThis->bNeedReload = true;
	}
}

AnimPathShape::AnimPathShape() 
{
	bNeedReload = true;
	AnimCallbackId = MAnimMessage::addAnimCurveEditedCallback(&AnimPathShape::AnimMessageCallback, this);
	#if MAYA_API_VERSION > 600
	AnimKeyframeCallbackId = 0;
	#endif
	TranslateNodeCallbackId = 0;
	NodeCallbackId = 0;
}

///
void AnimPathShape::postConstructor()
{
	MObject oNode = thisMObject();
	NodeCallbackId = MNodeMessage::addAttributeChangedCallback(oNode, ObjectAttributeChange, this);
	#if MAYA_API_VERSION > 600
	AnimKeyframeCallbackId = MAnimMessage::addAnimKeyframeEditedCallback(&AnimPathShape::AnimKeyframeEditedCallback, this);
	#endif
}

AnimPathShape::~AnimPathShape() 
{
	MMessage::removeCallback(AnimCallbackId);
	#if MAYA_API_VERSION > 600
	MMessage::removeCallback(AnimKeyframeCallbackId);
	#endif
	MMessage::removeCallback(NodeCallbackId);
	if(TranslateNodeCallbackId)
		MMessage::removeCallback(TranslateNodeCallbackId);
	
}

bool AnimPathShape::getInternalValue( const MPlug& plug, MDataHandle& data)
{
	displayStringD("AnimPathShape::getInternalValue %s", plug.name().asChar());

	if( plug == i_scaleTangents)
	{
		return MPxSurfaceShape::getInternalValue( plug, data );
	}
	if( plug == mControlPoints && !plug.isArray()) 
	{
		int index = plug.logicalIndex();
		MotionGeom* geometry = getCachedGeometry();

		MPoint pnt = geometry->getVertexTangent(index);
		data.set(pnt[0], pnt[1], pnt[2] );
	displayStringD("set %f %f %f", pnt[0], pnt[1], pnt[2]);
		return true;
	}

	return MPxSurfaceShape::getInternalValue( plug, data );
}
bool AnimPathShape::setInternalValue( const MPlug& plug, const MDataHandle& data)
{
	displayStringD("AnimPathShape::setInternalValue %s", plug.name().asChar());

	if( plug == i_scaleTangents)
	{
		bNeedReload = true;
		return MPxSurfaceShape::setInternalValue( plug, data );
	}
	return MPxSurfaceShape::setInternalValue( plug, data );
}

MStatus AnimPathShape::compute( const MPlug& plug, MDataBlock& data )
{
	bNeedReload = true;
	displayStringD("AnimPathShape::compute");
	return MS::kUnknownParameter;
}

MStatus AnimPathShape::connectionMade( const MPlug& plug, const MPlug& otherPlug, bool asSrc )
{
	/*/
	if(!asSrc && !TranslateNodeCallbackId)
	{
		if( plug==i_WorldMatrix)
		{
			this->setExistWithoutInConnections(false);
			MObject oNode = otherPlug.node();
			TranslateNodeCallbackId = MNodeMessage::addAttributeChangedCallback(oNode, TranslateAttributeChange, this);
		}
	}
	/*/
	return MS::kSuccess;
}
MStatus AnimPathShape::connectionBroken( const MPlug& plug, const MPlug& otherPlug, bool asSrc )
{
	return MS::kSuccess;
}

void* AnimPathShape::creator()
{
	return new AnimPathShape();
}

bool AnimPathShape::isBounded() const 
{
	return true; 
}

MBoundingBox AnimPathShape::boundingBox() const
{
	AnimPathShape* nonConstThis = const_cast <AnimPathShape*> (this);
	nonConstThis->updateGeometry();
	return box;
}







// Attribute to component (components)
//
void AnimPathShape::componentToPlugs( 
	MObject & component,
	MSelectionList & list ) const
{
	if ( component.hasFn(MFn::kSingleIndexedComponent) ) 
	{
		MFnSingleIndexedComponent fnVtxComp( component );
    	MObject thisNode = thisMObject();
		MPlug plug( thisNode, mControlPoints );
		// If this node is connected to a tweak node, reset the
		// plug to point at the tweak node.
		//
		convertToTweakNodePlug(plug);

		int len = fnVtxComp.elementCount();

		for ( int i = 0; i < len; i++ )
		{
			plug.selectAncestorLogicalIndex(fnVtxComp.element(i),
											plug.attribute());
			list.add(plug);
		}
	}
}
MPxSurfaceShape::MatchResult AnimPathShape::matchComponent( 
	const MSelectionList& item,
	const MAttributeSpecArray& spec,
	MSelectionList& list )
{
	MPxSurfaceShape::MatchResult result = MPxSurfaceShape::kMatchOk;
	MAttributeSpec attrSpec = spec[0];
	int dim = attrSpec.dimensions();

	// Look for attributes specifications of the form :
	//     vtx[ index ]
	//     vtx[ lower:upper ]
	//
	if ( (1 == spec.length()) && (dim > 0) && (attrSpec.name() == "vtx") ) 
	{
		MotionGeom* geometry = getCachedGeometry();
		int numVertices = geometry->getVertexTangentCount();
		MAttributeIndex attrIndex = attrSpec[0];

		int upper = 0;
		int lower = 0;
		if ( attrIndex.hasLowerBound() ) 
		{
			attrIndex.getLower( lower );
		}
		if ( attrIndex.hasUpperBound() ) 
		{
			attrIndex.getUpper( upper );
		}

		// Check the attribute index range is valid
		//
		if ( (lower > upper) || (upper >= numVertices) ) 
		{
			result = MPxSurfaceShape::kMatchInvalidAttributeRange;	
		}
		else 
		{
			MDagPath path;
			item.getDagPath( 0, path );
			MFnSingleIndexedComponent fnVtxComp;
			MObject vtxComp = fnVtxComp.create( MFn::kMeshVertComponent );

			for ( int i=lower; i<=upper; i++ )
			{
				fnVtxComp.addElement( i );
			}
			list.add( path, vtxComp );
		}
	}
	else 
	{
		// Pass this to the parent class
		return MPxSurfaceShape::matchComponent( item, spec, list );
	}
	return result;
}
bool AnimPathShape::match(	
	const MSelectionMask & mask,
	const MObjectArray& componentList ) const
{
	bool result = false;

	if( componentList.length() == 0 ) 
	{
		result = mask.intersects( MSelectionMask::kSelectMeshes );
	}
	else 
	{
		for ( int i=0; i<(int)componentList.length(); i++ ) 
		{
			if ( (componentList[i].apiType() == MFn::kMeshVertComponent) &&
				 (mask.intersects(MSelectionMask::kSelectMeshVerts))) 
			{
				result = true;
				break;
			}
		}
	}
	return result;
}



void AnimPathShape::transformUsing( const MMatrix & mat,
										const MObjectArray & componentList,
										MPxSurfaceShape::MVertexCachingMode cachingMode,
										MPointArray* pointCache)
{
	displayStringD("AnimPathShape::transformUsing");
    MStatus stat;
	MotionGeom* geomPtr = getCachedGeometry();

	MObject this_object = thisMObject();
	MObject acX = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveX));
	MObject acY = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveY));
	MObject acZ = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveZ));

	bool savePoints  = (cachingMode == MPxSurfaceShape::kSavePoints);
	unsigned int i=0,j=0;
	unsigned int len = componentList.length();

	if (cachingMode == MPxSurfaceShape::kRestorePoints) 
	{
		unsigned int cacheLen = pointCache->length();
		if (len > 0) 
		{
			// traverse the component list
			//
			for ( i = 0; i < len && j < cacheLen; i++ )
			{
				MObject comp = componentList[i];
				MFnSingleIndexedComponent fnComp( comp );
				int elemCount = fnComp.elementCount();
				for ( int idx=0; idx<elemCount && j < cacheLen; idx++, ++j ) 
				{
					int elemIndex = fnComp.element( idx );
					MPoint pt = (*pointCache)[j];
					MPoint ptCur = geomPtr->getVertex(elemIndex%geomPtr->getVertexCount());;

					MTime t = geomPtr->getVertexTangentTime(elemIndex);
					MMatrix m = getParentInverseMatrix(t);

					MVector ofs = pt - ptCur;
					ofs *= m;
					geomPtr->updateVertexTangent(elemIndex, ofs, acX, acY, acZ);
				}
			}
		} 
	}

	if (cachingMode == MPxSurfaceShape::kSavePoints || 
		cachingMode == MPxSurfaceShape::kUpdatePoints ) 
	{
		// Traverse the componentList 
		//
		for ( i=0; i<len; i++ )
		{
			MObject comp = componentList[i];
			MFnSingleIndexedComponent fnComp( comp );
			int elemCount = fnComp.elementCount();

			if (savePoints && 0 == i) 
			{
				pointCache->setSizeIncrement(elemCount);
			}
			for ( int idx=0; idx<elemCount; idx++ )
			{
				int elemIndex = fnComp.element( idx );
				MPoint pt = geomPtr->getVertexTangent(elemIndex);
				if (savePoints) 
					pointCache->append(pt);

				MTime t = geomPtr->getVertexTangentTime(elemIndex);
				MMatrix m = getParentInverseMatrix(t);

				MVector ofs = pt*mat;
				if(elemIndex<geomPtr->getVertexCount())
				{
					/*/
					if( ofs[0]==0 && ofs[1]==0 && ofs[2]==0)
					{
						// rotate || scale for vertex
						// ��������� ��������
						{
							int tangentElement = elemIndex+geomPtr->getVertexCount();
							MPoint pt = geomPtr->getVertexTangent(tangentElement);
							MVector ofs = (pt*mat - geomPtr->getVertex(elemIndex))*m;
							geomPtr->updateVertexTangent(tangentElement, ofs, acX, acY, acZ);
						}
						if(true)
						{
							int tangentElement = elemIndex+geomPtr->getVertexCount()*2;
							MPoint pt = geomPtr->getVertexTangent(tangentElement);
							MVector ofs = (pt*mat - geomPtr->getVertex(elemIndex))*m;
							geomPtr->updateVertexTangent(tangentElement, ofs, acX, acY, acZ);
						}
						continue;
					}
					/*/
				}

				ofs = ofs - geomPtr->getVertex(elemIndex%geomPtr->getVertexCount());
				ofs *= m;
				geomPtr->updateVertexTangent(elemIndex, ofs, acX, acY, acZ);
				displayStringD("ofs: %f, %f, %f", ofs.x, ofs.y, ofs.z);
			}
		}
	}
}

bool AnimPathShape::deleteComponents( 
	const MObjectArray& componentList,
	MDoubleArray& undoInfo )
{
	MotionGeom* geomPtr = getCachedGeometry();

	MObject this_object = thisMObject();
	MObject acX = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveX));
	MObject acY = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveY));
	MObject acZ = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveZ));
	unsigned int len = componentList.length();

	unsigned int i;
	for ( i=0; i<len; i++ )
	{
		MObject comp = componentList[i];
		MFnSingleIndexedComponent fnComp( comp );
		int elemCount = fnComp.elementCount();

		for ( int idx=0; idx<elemCount; idx++ )
		{
			int elemIndex = fnComp.element( idx );

			if( elemIndex>=geomPtr->getVertexCount())
				continue;

			MTime   t = geomPtr->getVertexTime(elemIndex);
			MPoint pt = geomPtr->getVertex(elemIndex);

			undoInfo.append(t.as(MTime::kSeconds));
			undoInfo.append(pt[0]);
			undoInfo.append(pt[1]);
			undoInfo.append(pt[2]);

			geomPtr->deleteVertex(elemIndex, acX, acY, acZ);
		}
	}
	return true;
}
bool AnimPathShape::undeleteComponents( 
	const MObjectArray& componentList,
	MDoubleArray& undoInfo )
{
	MObject this_object = thisMObject();
	MObject acX = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveX));
	MObject acY = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveY));
	MObject acZ = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveZ));
	MPlug wmPl(this_object, i_WorldMatrix);
	MotionGeom* geomPtr = getCachedGeometry();

	unsigned int len = componentList.length();

	unsigned int i;
	for(i=0;i<undoInfo.length()/4; i++)
	{
		MTime t( undoInfo[i*4+0], MTime::kSeconds);
		int elemIndex = geomPtr->insertVertex(t, acX, acY, acZ, wmPl);

		MPoint pt = geomPtr->getVertex(elemIndex);
		MVector ofs = MPoint(undoInfo[i*4+1], undoInfo[i*4+2], undoInfo[i*4+3]) - pt;

		geomPtr->updateVertexTangent(elemIndex, ofs, acX, acY, acZ);
	}

	return true;
}

void AnimPathShape::selectVertexTangent(int elemIndex)
{
	MotionGeom* geomPtr = getCachedGeometry();
	if( elemIndex>=geomPtr->getVertexCount())
		return;

	MObject this_object = thisMObject();
	MObject acX = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveX));
	MObject acY = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveY));
	MObject acZ = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveZ));

	geomPtr->selectVertex(elemIndex, acX, acY, acZ);
}


MPxGeometryIterator* AnimPathShape::geometryIteratorSetup( 
	MObjectArray& componentList, 
	MObject& components,
	bool forReadOnly )
{
	MotionGeomIterator* result = NULL;
	if ( components.isNull() ) 
	{
		result = new MotionGeomIterator( getCachedGeometry(), componentList );
	}
	else 
	{
		result = new MotionGeomIterator( getCachedGeometry(), components );
	}
	return result;
}
bool AnimPathShape::acceptsGeometryIterator( bool  writeable )
{
	return true;
}
bool AnimPathShape::acceptsGeometryIterator( 
	MObject&, 
	bool writeable,
	bool forReadOnly )
{
	return true;
}


// Update
void AnimPathShape::updateGeometry()
{
	MObject this_object = thisMObject();

	if( !bNeedReload)
		return;

	displayStringD("AnimPathShape::updateGeometry");

	double st = scaleTangents();
	MObject acX = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveX));
	MObject acY = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveY));
	MObject acZ = getAnimCurveForPlug( MPlug(this_object, i_AnimCurveZ));

	MPlug tsPl(this_object, i_TimeStep);
	MTime timestep;
	tsPl.getValue(timestep);
	if( timestep == MTime(0.0))
		timestep = MTime(1.0, MTime::uiUnit());

	bool bShowTimeline;
	MPlug(this_object, i_bShowTimeline).getValue(bShowTimeline);

	MPlug wmPl(this_object, i_WorldMatrix);
	MPlug pimPl(this_object, i_ParentInverseMatrix);
	geometry.load(acX, acY, acZ, wmPl, pimPl, timestep, bShowTimeline, false, st);

	this->box = computeBoundingBox();

	childChanged( MPxSurfaceShape::kBoundingBoxChanged );
	childChanged( MPxSurfaceShape::kObjectChanged );
	bNeedReload = false;
}

MBoundingBox AnimPathShape::computeBoundingBox()
{
	MBoundingBox result;	
	AnimPathShape* nonConstThis = const_cast <AnimPathShape*> (this);
	MotionGeom* geom = nonConstThis->getCachedGeometry();

	result = geom->boundingBox();
	return result;
}

// MotionGeom
MotionGeom* AnimPathShape::getCachedGeometry()
{
	return &geometry;
}


bool AnimPathShape::isDrawNumbers()
{
	MObject this_object = thisMObject();
	MPlug dnPl(this_object, i_bNumbers);
	bool bDrawNumbers;
	dnPl.getValue(bDrawNumbers);
	return bDrawNumbers;
}
double AnimPathShape::scaleTangents()
{
	MObject this_object = thisMObject();
	MPlug dnPl(this_object, i_scaleTangents);
	double vscaleTangents;
	dnPl.getValue(vscaleTangents);
	return vscaleTangents;
}
bool AnimPathShape::tangentsAlways()
{
	MObject this_object = thisMObject();
	MPlug dnPl(this_object, i_bTangentsAlways);
	bool value;
	dnPl.getValue(value);
	return value;
}

MStatus AnimPathShape::initialize()
{
	// This sample creates a single input float attribute and a single
	// output float attribute.
	//
	MStatus s;

	{
		MFnMatrixAttribute matrixAttribute;
		i_WorldMatrix = matrixAttribute.create("inputMatrix", "imat", MFnMatrixAttribute::kFloat);
		matrixAttribute.setHidden(true);
		matrixAttribute.setStorable(false);
		s = addAttribute(i_WorldMatrix); e;
	}
	{
		MFnMatrixAttribute matrixAttribute;
		i_ParentInverseMatrix = matrixAttribute.create("inputPIMatrix", "ipim", MFnMatrixAttribute::kFloat);
		matrixAttribute.setHidden(true);
		matrixAttribute.setStorable(false);
		s = addAttribute(i_ParentInverseMatrix); e;
	}
	{
		MFnUnitAttribute genAttribute;
		i_AnimCurveX = genAttribute.create("inputCurveX", "icx", MDistance(0), &s); e;
		genAttribute.setHidden( true );
		genAttribute.setStorable(true);
		genAttribute.setKeyable(true);
		s = addAttribute(i_AnimCurveX); e;
	}
	{
		MFnUnitAttribute genAttribute;
		i_AnimCurveY = genAttribute.create("inputCurveY", "icy", MDistance(0), &s); e;
		genAttribute.setHidden( true );
		genAttribute.setStorable(true);
		genAttribute.setKeyable(true);
		s = addAttribute(i_AnimCurveY); e;
	}
	{
		MFnUnitAttribute genAttribute;
		i_AnimCurveZ = genAttribute.create("inputCurveZ", "icz", MDistance(0), &s); e;
		genAttribute.setHidden( true );
		genAttribute.setStorable(true);
		genAttribute.setKeyable(true);
		s = addAttribute(i_AnimCurveZ); e;
	}
	{
		MFnUnitAttribute unitAttr;
		i_TimeStep = unitAttr.create( "timeStep", "tms",
											MFnUnitAttribute::kTime,
											0.0, &s ); e;
		unitAttr.setMin(0);
		unitAttr.setMax(20);
		unitAttr.setDefault(MTime(1.0, MTime::uiUnit()));
		unitAttr.setKeyable(true);
		s = addAttribute( i_TimeStep); e;
	}
	{
		MFnNumericAttribute nAttr;
		i_bNumbers = nAttr.create( "showNumbers", "bsn", MFnNumericData::kBoolean, 0, &s ); e;
		nAttr.setDefault(false);
		nAttr.setKeyable(true);
		s = addAttribute( i_bNumbers); e;

		i_bShowTimeline = nAttr.create( "showTimeline", "bstl", MFnNumericData::kBoolean, 1, &s ); e;
		nAttr.setDefault(false);
		nAttr.setKeyable(true);
		s = addAttribute( i_bShowTimeline); e;

		i_scaleTangents = nAttr.create( "scaleTangents", "sct", MFnNumericData::kDouble, 1, &s ); e;
		nAttr.setMin(0.1);
		nAttr.setSoftMax(3);
		nAttr.setDefault(false);
		nAttr.setInternal(true);
		nAttr.setKeyable(true);
		s = addAttribute( i_scaleTangents); e;

		i_bTangentsAlways = nAttr.create( "tangentsAlways", "bta", MFnNumericData::kBoolean, 1, &s ); e;
		nAttr.setDefault(false);
		nAttr.setKeyable(true);
		s = addAttribute( i_bTangentsAlways); e;
	}

	return MS::kSuccess;

}




MObject AnimPathShape::getAnimCurveForPlug(MPlug pl)
{
displayStringD( pl.name().asChar());
	MObject animpath;
	MPlugArray pa;
	pl.connectedTo(pa, true, false);
	if( pa.length() != 1)
		return MObject::kNullObj;

	pl = pa[0];
displayStringD( pl.name().asChar());
	pl.connectedTo(pa, true, false);
	if( pa.length() == 1)
	{
		pl = pa[0];
		animpath = pl.node();
		if( !animpath.hasFn(MFn::kAnimCurve))
			goto xxx;
	}
	else
	{
xxx:
displayStringD( "try MAnimUtil::findAnimation");
		MStatus stat;
		MObjectArray animations;
		bool bAnimation = MAnimUtil::findAnimation(pl, animations, &stat);
		int len = animations.length();
		if( bAnimation && len==1)
		{
			animpath = animations[0];
		}
		else
			return MObject::kNullObj;
	}


	if( !animpath.hasFn(MFn::kAnimCurve))
		return MObject::kNullObj;
	
	return animpath;
}


MMatrix AnimPathShape::getParentInverseMatrix( MTime t)
{
	MObject this_object = thisMObject();
	MPlug piPl(this_object, i_ParentInverseMatrix);
	MObject val;
	MDGContext context(t);
	piPl.getValue(val, context);
	MFnMatrixData matrData(val);
	MMatrix m = matrData.matrix();
	return m;
}
MMatrix AnimPathShape::getParentInverseMatrix( MObject this_object, MTime t)
{
	MPlug piPl(this_object, i_ParentInverseMatrix);
	MObject val;
	MDGContext context(t);
	piPl.getValue(val, context);
	MFnMatrixData matrData(val);
	MMatrix m = matrData.matrix();
	return m;
}
