set DOC_DIR=\\server\bin70
set DOC_DIR=m:\docs\

mkdir %DOC_DIR%
mkdir %DOC_DIR%\animpath
mkdir %DOC_DIR%\animpath\en
mkdir %DOC_DIR%\animpath\en\animpath
mkdir %DOC_DIR%\animpath\ru
mkdir %DOC_DIR%\animpath\ru\animpath
mkdir %DOC_DIR%\animpath\img
mkdir %DOC_DIR%\animpath\img\animpath

copy en\config.php		%DOC_DIR%\animpath\en\animpath
copy en\main.html		%DOC_DIR%\animpath\en\animpath
copy en\menu.txt		%DOC_DIR%\animpath\en\animpath

copy ru\config.php		%DOC_DIR%\animpath\ru\animpath
copy ru\main.html		%DOC_DIR%\animpath\ru\animpath
copy ru\menu.txt		%DOC_DIR%\animpath\ru\animpath

copy img\animpath.jpg	%DOC_DIR%\animpath\img\animpath

pause
