#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: AnimPathVerCmd.h
// MEL Command: AnimPathVer

#include <maya/MPxCommand.h>

class AnimPathVer : public MPxCommand
{
public:
	static const MString typeName;
	AnimPathVer();
	virtual	~AnimPathVer();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

