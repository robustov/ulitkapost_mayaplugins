#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>

 
class StreakShape : public MPxLocatorNode
{
public:
	StreakShape();
	virtual ~StreakShape(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	static void* creator();
	static MStatus initialize();

public:
	MObject getParticle() const;

public:
	static MObject i_message;
	static MObject o_output;

	static MObject i_particleRenderType;
	static MObject i_multiCountReplace;
	static MObject i_Width, i_WidthJitter;
	static MObject i_WidthFactorForShadows;
	static MObject i_PutNormalTo;
	static MObject i_PutAgeTo;
	/*/
	static MObject i_bAddPoints;
	static MObject i_PointsCount1;
	static MObject i_PointsWidth1;
	static MObject i_PointsCount2;
	static MObject i_PointsWidth2;
	/*/
	static MObject i_bRgbPP;
	static MObject i_bOpacityPP;
	static MObject i_dencityRamp;
	static MObject i_radiusRamp;
	static MObject i_opacityRamp;
	static MObject i_noiseRadius;
	static MObject i_noiseFreq;
	static MObject i_time;

	static MObject i_fetchSeedFrom;

public:
	static MTypeId id;
	static const MString typeName;
};


