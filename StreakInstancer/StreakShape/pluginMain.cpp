#include "pre.h"
#include <maya/MFnPlugin.h>
#include "StreakShape.h"
#include "StreakGenerator.h"
#include "ocellaris/OcellarisExport.h"

StreakGenerator streakGenerator;

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

// #include "StreakShape.h"
	stat = plugin.registerNode( 
		StreakShape::typeName, 
		StreakShape::id, 
		StreakShape::creator,
		StreakShape::initialize, 
		MPxNode::kLocatorNode );
	if (!stat) 
	{
		displayString("cant register %s", StreakShape::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	registerGenerator(StreakShape::id, &streakGenerator);


//	if( !MGlobal::sourceFile("StreakShapeMenu.mel"))
//	{
//		displayString("error source AEmyCmdTemplate.mel");
//	}
//	else
	{
		if( !MGlobal::executeCommand("StreakShapeDebugMenu()"))
		{
			displayString("Dont found StreakShapeDebugMenu() command");
		}
		MGlobal::executeCommand("StreakShape_RegistryPlugin()");
	}
	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	unregisterGenerator(StreakShape::id, &streakGenerator);

	stat = plugin.deregisterNode( StreakShape::id );
	if (!stat) displayString("cant deregister %s", StreakShape::typeName.asChar());


//	MGlobal::executeCommand("StreakShapeDebugMenuDelete()");
	return stat;
}
