#include "StreakGenerator.h"
#include "StreakShape.h"
#include "ocellaris/OcellarisExport.h"

#include <maya/MFnDependencyNode.h>
#include <maya/MFnParticleSystem.h> 
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MAnimControl.h>
#include <maya/MFnParticleSystem.h>
#include <math/Ramp.h>

//! @ingroup implement_group
//! \brief ����������� ��������� ��� Instancer (particleSystem)
//! 

bool StreakGenerator::OnGeometry(
	cls::INode& _node,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = _node.getObject();
	int z;
	MStatus stat;
	MFnDependencyNode dn(node);
	if( dn.typeId()!=StreakShape::id)
		return false;

	double framedur = MTime(1.0, MTime::uiUnit()).as(MTime::kSeconds);
	float fps = (float)(1/framedur);

	StreakShape* streakShape = (StreakShape*)dn.userNode();
	MObject partictesobj = streakShape->getParticle();
	if(partictesobj.isNull())
		return false;

	MFnParticleSystem ps(partictesobj);
	MFnDependencyNode dnps(partictesobj);

	cls::Param multiCountReplace = ocExport_GetAttrValue(node, "multiCountReplace");
	cls::Param width = ocExport_GetAttrValue(node, "width");
	cls::Param WidthJitter = ocExport_GetAttrValue(node, "widthJitter");
	cls::Param widthFactorForShadows = ocExport_GetAttrValue(node, "widthFactorForShadows");
	cls::Param putNormalTo = ocExport_GetAttrValue(node, "putNormalTo");
	cls::Param putAgeTo = ocExport_GetAttrValue(node, "putAgeTo");
	cls::PS fetchSeedFrom = ocExport_GetAttrValue(node, StreakShape::i_fetchSeedFrom);

	cls::P<float> noiseRadius = ocExport_GetAttrValue(node, StreakShape::i_noiseRadius);
	cls::P<float> noiseFreq   = ocExport_GetAttrValue(node, StreakShape::i_noiseFreq);

	MPlug tmplug(node, StreakShape::i_time);
	MTime _time;
	tmplug.getValue(_time);
	cls::P<float> time( (float)_time.as(MTime::kSeconds));
	
	/*/
	cls::Param bAddPoints = ocExport_GetAttrValue(node, "bAddPoints");
	cls::Param pointsCount1 = ocExport_GetAttrValue(node, "pointsCount1");
	cls::Param pointsWidth1 = ocExport_GetAttrValue(node, "pointsWidth1");
	cls::Param pointsCount2 = ocExport_GetAttrValue(node, "pointsCount2");
	cls::Param pointsWidth2 = ocExport_GetAttrValue(node, "pointsWidth2");
	/*/

	cls::P<bool> bRgbPP = ocExport_GetAttrValue(node, "bRgbPP");
	cls::P<bool> bOpacityPP = ocExport_GetAttrValue(node, "bOpacityPP");

	cls::PA<float> _ids = ocExport_GetAttrArrayValue(partictesobj, "id");
	cls::PA<int> ids(cls::PI_VERTEX, _ids.size());
	for(z=0; z<_ids.size(); ++z) 
		ids[z] = (int)_ids[z];

	cls::PA<int> seed;
	if( !fetchSeedFrom.empty())
	{
		cls::PA<float> _seed = ocExport_GetAttrArrayValue(partictesobj, fetchSeedFrom.data());
		if( _seed.size()==_ids.size())
		{
			seed = cls::PA<int>(cls::PI_VERTEX, _ids.size());
			for(z=0; z<_seed.size(); ++z) 
				ids[z] = (int)_seed[z];
		}
	}
	

	cls::P<int> my_particleRenderType = ocExport_GetAttrValue(node, "particleRenderType");
	cls::Param particleRenderType = ocExport_GetAttrValue(partictesobj, "particleRenderType");
	cls::PA<Math::Vec3f> position = ocExport_GetAttrArrayValue(partictesobj, "position");
	position->type = cls::PT_POINT;
	position->interp = cls::PI_VERTEX;
	cls::PA<Math::Vec3f> velocity = ocExport_GetAttrArrayValue(partictesobj, "velocity");
	position->type = cls::PT_VECTOR;
	position->interp = cls::PI_VERTEX;
	cls::PA<float> radiusPP = ocExport_GetAttrArrayValue(partictesobj, "radiusPP");
//	cls::P<float> radius = ocExport_GetAttrValue(partictesobj, "radius");
	cls::Param age = ocExport_GetAttrArrayValue(partictesobj, "age");
	age->interp = cls::PI_VERTEX;
	cls::P<float> multiRadius = ocExport_GetAttrValue(partictesobj, "multiRadius");
	cls::Param multiCount = ocExport_GetAttrValue(partictesobj, "multiCount");
	cls::Param tailFade = ocExport_GetAttrValue(partictesobj, "tailFade");
	cls::P<float> tailSize = ocExport_GetAttrValue(partictesobj, "tailSize");
	if( tailSize.empty()) tailSize = 1.f;
	cls::PA<Math::Vec3f> rgbPP(cls::PI_VERTEX);
	cls::PA<float> opacityPP;

	if( !tailSize.empty())
		tailSize.data() = tailSize.data()/fps;

	{
		MRampAttribute rampattr(node, StreakShape::i_dencityRamp);
		Math::Ramp ramp;
		ramp.set(rampattr);
		cls::PA<float> pramp = ramp.Ramp2Ocs();
		render->Parameter("dencityRamp", pramp);
	}
	{
		MRampAttribute rampattr(node, StreakShape::i_radiusRamp);
		Math::Ramp ramp;
		ramp.set(rampattr);
		cls::PA<float> pramp = ramp.Ramp2Ocs();
		render->Parameter("radiusRamp", pramp);
	}
	{
		MRampAttribute rampattr(node, StreakShape::i_opacityRamp);
		Math::Ramp ramp;
		ramp.set(rampattr);
		cls::PA<float> pramp = ramp.Ramp2Ocs();
		render->Parameter("opacityRamp", pramp);
	}

	if(*bRgbPP)
	{
		rgbPP = ocExport_GetAttrArrayValue(partictesobj, "rgbPP");
		if( rgbPP.size()<ids.size()) *bRgbPP = false;
		rgbPP->interp = cls::PI_VERTEX;
		if( *bRgbPP)
			render->Parameter("rgbPP", rgbPP);
	}
	else
	{
		cls::P<float> r = ocExport_GetAttrValue(partictesobj, "colorRed");
		cls::P<float> g = ocExport_GetAttrValue(partictesobj, "colorGreen");
		cls::P<float> b = ocExport_GetAttrValue(partictesobj, "colorBlue");
		if(!r.empty() && !g.empty() && !b.empty())
			render->Parameter("rgbPP", Math::Vec3f(*r, *g, *b));

	}
	if(*bOpacityPP)
	{
		opacityPP = ocExport_GetAttrArrayValue(partictesobj, "opacityPP");
		if( opacityPP.size()<ids.size()) *bOpacityPP = false;
		opacityPP->interp = cls::PI_VERTEX;
		if( *bOpacityPP)
		   	render->Parameter("opacityPP", opacityPP);
	}
	else
	{
		opacityPP = ocExport_GetAttrValue(partictesobj, "opacity");
		if( !opacityPP.empty())
	   	render->Parameter("opacityPP", opacityPP);
	}


//	render->Parameter("bBlur", bBlur);
//   	render->Parameter("frame0", fr0);
//	render->Parameter("frame1", fr1);
//	render->Parameter("shutterOpen",	open);
//	render->Parameter("shutterClose",	close);


	if( my_particleRenderType.empty() || *my_particleRenderType == 20)
		render->Parameter("particleRenderType", particleRenderType);
	else
		render->Parameter("particleRenderType", my_particleRenderType);
	render->Parameter("particleid", ids);
	render->Parameter("seed", seed);
	render->Parameter("P", position);
	render->Parameter("velocity", position);
	render->Parameter("age", age);
	render->Parameter("radiusPP", radiusPP);
//	render->Parameter("radius", radius);
	render->Parameter("tailFade", tailFade);
	render->Parameter("tailSize", tailSize);
	render->Parameter("multiRadius", multiRadius);
	render->Parameter("multiCount", multiCount);

	render->Parameter("multiCountReplace", multiCountReplace);
	render->Parameter("width", width);
	render->Parameter("widthJitter", WidthJitter);
	render->Parameter("widthFactorForShadows", widthFactorForShadows);
	render->Parameter("putNormalTo", putNormalTo);
	render->Parameter("putAgeTo", putAgeTo);
	/*/
	render->Parameter("bAddPoints", bAddPoints);
	render->Parameter("pointsCount1", pointsCount1);
	render->Parameter("pointsWidth1", pointsWidth1);
	render->Parameter("pointsCount2", pointsCount1);
	render->Parameter("pointsWidth2", pointsWidth1);
	/*/

	render->Parameter("noiseRadius", noiseRadius);
	render->Parameter("noiseFrequency", noiseFreq);
	render->Parameter("time", time);

	/*/
	if( bBlur)
	{
		MAnimControl::setCurrentTime(t1);
		ps.evaluateDynamics(t1, false);

		cls::PA<float> _ids1 = ocExport_GetAttrArrayValue(partictesobj, "id");
		cls::PA<int> ids1(cls::PI_VERTEX, _ids1.size());
		for(z=0; z<_ids1.size(); ++z) 
			ids1[z] = (int)_ids1[z];
		cls::PA<Math::Vec3f> position1 = ocExport_GetAttrArrayValue(partictesobj, "position");
		position1->type = cls::PT_POINT;
		position1->interp = cls::PI_VERTEX;

		render->Parameter("id1", ids1);
		render->Parameter("P1", position1);
		MIntArray pids;
		ps.particleIds( pids);
		displayString("> %d", pids.length());
	}
	/*/

	render->RenderCall("StreakMath.dll@Streak");

	for( z=0; z<position.size(); z++)
	{
		float r = 1.f;
		if( !multiRadius.empty())
			r = multiRadius.data();
		if( radiusPP.size()>z)
			r = radiusPP[z];
		
		if( !noiseRadius.empty())
			r += *noiseRadius;

		box.insert( position[z], r);
	}

//	if( bBlur)
//	{
//		MAnimControl::setCurrentTime(curtime);
//	}

	return true;
}
