//
// Copyright (C) 
// File: StreakShapeCmd.cpp
// MEL Command: StreakShape

#include "StreakShape.h"

#include <maya/MGlobal.h>
#include <maya/MFnStringData.h>
#include <mathNgl/mathNgl.h>
#include <mathNmaya/MayaRamp.h>
#include <maya/MFnEnumAttribute.h>

MObject StreakShape::i_message;
MObject StreakShape::o_output;

MObject StreakShape::i_particleRenderType;
MObject StreakShape::i_multiCountReplace;
MObject StreakShape::i_Width, StreakShape::i_WidthJitter;
MObject StreakShape::i_WidthFactorForShadows;
MObject StreakShape::i_PutNormalTo;
MObject StreakShape::i_PutAgeTo;
/*/
MObject StreakShape::i_bAddPoints;
MObject StreakShape::i_PointsCount1;
MObject StreakShape::i_PointsWidth1;
MObject StreakShape::i_PointsCount2;
MObject StreakShape::i_PointsWidth2;
/*/
MObject StreakShape::i_bRgbPP;
MObject StreakShape::i_bOpacityPP;
MObject StreakShape::i_dencityRamp;
MObject StreakShape::i_radiusRamp;
MObject StreakShape::i_opacityRamp;
MObject StreakShape::i_noiseRadius;
MObject StreakShape::i_noiseFreq;
MObject StreakShape::i_time;

MObject StreakShape::i_fetchSeedFrom;

StreakShape::StreakShape()
{
}
StreakShape::~StreakShape()
{
}
MObject StreakShape::getParticle() const
{
	MPlug plug(thisMObject(), i_message);

	MPlugArray pa;
	plug.connectedTo(pa, true, false);
	if(pa.length()!=1) 
		return MObject::kNullObj;

	MObject obj = pa[0].node();
	if( !obj.hasFn(MFn::kParticle))
		return MObject::kNullObj;
	return obj;
}

MStatus StreakShape::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	if( plug == o_output )
	{
//		MDataHandle inputData = data.inputValue( i_input, &stat );

//		MDataHandle outputData = data.outputValue( plug, &stat );

		data.setClean(plug);
		return MS::kSuccess;
	}

	return MS::kUnknownParameter;
}

bool StreakShape::isBounded() const
{
	MObject thisNode = thisMObject();
	return false;
}

MBoundingBox StreakShape::boundingBox() const
{
	MObject thisNode = thisMObject();

	MBoundingBox box;
	//...
	return box;
}

void StreakShape::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MObject thisNode = thisMObject();
	MFnDependencyNode nd(thisNode);
	MString str = nd.name();
	
	double multiRadius = 0;
	MObject partictesobj = getParticle();
	if(!partictesobj.isNull())
	{
		MFnDependencyNode particlend(partictesobj);
		MPlug plug = particlend.findPlug("multiRadius");
		if( !plug.isNull())
		{
			plug.getValue(multiRadius);
		}
	}

	view.beginGL(); 

	if( multiRadius!=0.0)
	{
		drawSphere((float)multiRadius);
	}
	
	view.drawText(str, MPoint(0, 0, 0));

	view.endGL();
};


void* StreakShape::creator()
{
	return new StreakShape();
}

MStatus StreakShape::initialize()
{
	MFnUnitAttribute    unitAttr;
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnMessageAttribute	mesAttr;
	MFnEnumAttribute enumAttr;
	MStatus				stat;
	MFnStringData stringData;

	try
	{
		{
			i_message = mesAttr.create( "input", "iin", &stat);
			::addAttribute(i_message, atInput);
		}
		{
			o_output = numAttr.create( "o_output", "out", MFnNumericData::kDouble, 0.01, &stat);
			::addAttribute(o_output, atOutput);
		}

		{
			i_particleRenderType = enumAttr.create("particleRenderType", "prt", 20);
			enumAttr.addField("MultiPoint", 0);
			enumAttr.addField("MultiStreak", 1);
			enumAttr.addField("Numeric", 2);
			enumAttr.addField("Points", 3);
			enumAttr.addField("Spheres", 4);
			enumAttr.addField("Sprites", 5);
			enumAttr.addField("Streak", 6);
			enumAttr.addField("Blobby ", 7);
			enumAttr.addField("Cloud (s/w)", 8);
			enumAttr.addField("Tube (s/w)", 9);
			enumAttr.addField("DEFAULT", 20);

			::addAttribute(i_particleRenderType, atInput);

			i_multiCountReplace = numAttr.create("multiCountReplace", "mcr", MFnNumericData::kInt);
			numAttr.setMin(0);
			numAttr.setSoftMax(50);
			::addAttribute(i_multiCountReplace, atInput);

			i_Width = numAttr.create("width", "wi", MFnNumericData::kDouble, 0.1);
			numAttr.setMin(0);
			numAttr.setSoftMax(0.5);
			::addAttribute(i_Width, atInput);

			i_WidthJitter = numAttr.create("widthJitter", "wij", MFnNumericData::kDouble, 0.0);
			numAttr.setMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_WidthJitter, atInput);

			i_WidthFactorForShadows = numAttr.create("widthFactorForShadows", "wffs", MFnNumericData::kDouble, 1);
			numAttr.setMin(0);
			numAttr.setSoftMax(2);
			::addAttribute(i_WidthFactorForShadows, atInput);

			i_PutNormalTo = typedAttr.create ("putNormalTo", "pnt", MFnData::kString, &stat);
			::addAttribute(i_PutNormalTo, atInput);

			i_PutAgeTo = typedAttr.create ("putAgeTo", "pat", MFnData::kString, stringData.create("ageNormPP", &stat), &stat);
			::addAttribute(i_PutAgeTo, atInput);

			i_fetchSeedFrom = typedAttr.create ("fetchSeedFrom", "fsf", MFnData::kString, stringData.create("seed", &stat), &stat);
			::addAttribute(i_fetchSeedFrom, atInput);
			/*/
			i_bAddPoints = numAttr.create("bAddPoints", "ap", MFnNumericData::kBoolean);
			::addAttribute(i_bAddPoints, atInput);

			i_PointsCount1 = numAttr.create("pointsCount1", "pc1", MFnNumericData::kInt);
			numAttr.setMin(0);
			numAttr.setSoftMax(50);
			::addAttribute(i_PointsCount1, atInput);

			i_PointsWidth1 = numAttr.create("pointsWidth1", "pw1", MFnNumericData::kDouble);
			numAttr.setMin(0);
			numAttr.setSoftMax(0.5);
			::addAttribute(i_PointsWidth1, atInput);

			i_PointsCount2 = numAttr.create("pointsCount2", "pc2", MFnNumericData::kInt);
			numAttr.setMin(0);
			numAttr.setSoftMax(50);
			::addAttribute(i_PointsCount2, atInput);

			i_PointsWidth2 = numAttr.create("pointsWidth2", "pw2", MFnNumericData::kDouble);
			numAttr.setMin(0);
			numAttr.setSoftMax(0.5);
			::addAttribute(i_PointsWidth2, atInput);
			/*/

			i_bRgbPP = numAttr.create("bRgbPP", "brp", MFnNumericData::kBoolean);
			::addAttribute(i_bRgbPP, atInput);

			i_bOpacityPP = numAttr.create("bOpacityPP", "bop", MFnNumericData::kBoolean);
			::addAttribute(i_bOpacityPP, atInput);

			i_dencityRamp = Math::Ramp::CreateRampAttr("dencityRamp", "den");
			::addAttribute(i_dencityRamp, atInput);
			
			i_radiusRamp = Math::Ramp::CreateRampAttr("radiusRamp", "radr");
			::addAttribute(i_radiusRamp, atInput);

			i_opacityRamp = Math::Ramp::CreateRampAttr("opacityRamp", "opar");
			::addAttribute(i_opacityRamp, atInput);

			{
				i_noiseRadius = numAttr.create("noiseRadius", "nra", MFnNumericData::kDouble, 0);
				numAttr.setMin(0);
				numAttr.setSoftMax(1);
				::addAttribute(i_noiseRadius, atInput);

				i_noiseFreq = numAttr.create("noiseFreq", "nfr", MFnNumericData::kDouble, 1);
				numAttr.setMin(0.0001);
				numAttr.setSoftMax(2);
				::addAttribute(i_noiseFreq, atInput);

				i_time = unitAttr.create ("time","tm", MTime(0.0));
				::addAttribute(i_time, atInput);
			}
		}

		if( !MGlobal::sourceFile("AEStreakShapeTemplate.mel"))
		{
			displayString("error source AEStreakShapeTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}