#include "StreakShape.h"
#include "ocellaris/OcellarisExport.h"

#include <maya/MFnDependencyNode.h>
#include <maya/MFnParticleSystem.h> 
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MAnimControl.h>
#include <maya/MFnFluid.h>
#include <math/Ramp.h>

#include "ocellaris/IGeometryGenerator.h"
#include "ocellaris/INode.h"

//! @ingroup implement_group
//! \brief ��������� ��������� ��� ParticleSystem
//! ������ ����� �������������� Points, Spheres � Blobby (� ������� ��������)
//! 
struct FluidToPointsGenerator : public cls::IGeometryGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		)
	{
		return "FluidToPointsGenerator";
	}

	// ��� �������� � ����
	virtual void OnLoadToMaya(
		MObject& generatornode
		);
	bool OnGeometry(
		cls::INode& node,				//!< ������ 
		cls::IRender* render,			//!< render
		Math::Box3f& box,				//!< box 
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
};

FluidToPointsGenerator fluidtopointsgenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl FluidToPoints()
	{
		return &fluidtopointsgenerator;
	}
}
// ��� �������� � ����
void FluidToPointsGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
	MObject attr;
	attr = ocExport_AddAttrType(generatornode, "particlePerCell", cls::PT_INT, cls::P<int>(10));
	attr = ocExport_AddAttrType(generatornode, "widthDencityFactor", cls::PT_FLOAT, cls::P<float>(1));
	attr = ocExport_AddAttrType(generatornode, "constWidth", cls::PT_FLOAT, cls::P<float>(0));
	attr = ocExport_AddAttrType(generatornode, "opacityDencityFactor", cls::PT_FLOAT, cls::P<float>(0));
	attr = ocExport_AddAttrType(generatornode, "constOpacity", cls::PT_FLOAT, cls::P<float>(0.1f));
}

bool FluidToPointsGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	MFnFluid fluid(node);


	cls::PA<unsigned int> res(cls::PI_CONSTANT, 3);
	fluid.getResolution(res[0], res[1], res[2]);
	double x, y, z;
	fluid.getDimensions(x, y, z);
	cls::PA<float> dim(cls::PI_CONSTANT, 3);
	dim[0] = (float)x; dim[1] = (float)y; dim[2] = (float)z;

	if(render)
	{
		render->Parameter( "@particlePerCell", 
			ocExport_GetAttrValue(generatornode, "particlePerCell"));
		render->Parameter( "widthDencityFactor", 
			ocExport_GetAttrValue(generatornode, "widthDencityFactor"));
		render->Parameter( "constWidth", 
			ocExport_GetAttrValue(generatornode, "constWidth"));
		render->Parameter( "opacityDencityFactor", 
			ocExport_GetAttrValue(generatornode, "opacityDencityFactor"));
		render->Parameter( "constOpacity", 
			ocExport_GetAttrValue(generatornode, "constOpacity"));

		render->Parameter("#resolution", res);
		render->Parameter("#dimentions", dim);
//		render->Parameter("world", world);

		int s = res[0]*res[1]*res[2];
		int gs = fluid.gridSize();

		float* densityGrid = fluid.density();
		if( densityGrid)
		{
			float ds = 0;
			cls::PA<float> density(cls::PI_CONSTANT, s);

			for(unsigned int z = 0; z<res[2]; z++)
				for(unsigned int y = 0; y<res[1]; y++)
					for(unsigned int x = 0; x<res[0]; x++)
					{
						int find = fluid.index(x, y, z);
						int myind = x + y*res[0] + z*res[0]*res[1];
						if(find>=0 && find<gs)
						{
							density[myind] = densityGrid[find];
							ds += density[myind];
						}
						else
							density[myind] = 0;
					}

			render->Parameter("#densitySum", ds);
			render->Parameter("#density", density);
		}
		float *rGrid, *gGrid, *bGrid;
		fluid.getColors(rGrid, gGrid, bGrid);
		if(rGrid && gGrid && bGrid) 
		{
			cls::PA<Math::Vec3f> color(cls::PI_CONSTANT, s);

			for(unsigned int z = 0; z<res[2]; z++)
				for(unsigned int y = 0; y<res[1]; y++)
					for(unsigned int x = 0; x<res[0]; x++)
					{
						int find = fluid.index(x, y, z);
						int myind = x + y*res[0] + z*res[0]*res[1];
						if(find>=0 && find<gs)
							color[myind] = Math::Vec3f(rGrid[find], gGrid[find], bGrid[find]);
						else
							color[myind] = Math::Vec3f(0, 0, 0);
					
					}
			render->Parameter("#color", color);
		}
		render->RenderCall("StreakMath.dll@FluidToPoints");
	}

	box.min = Math::Vec3f(-dim[0]/2, -dim[1]/2, -dim[2]/2);
	box.max = Math::Vec3f( dim[0]/2,  dim[1]/2,  dim[2]/2);
	return true;
}
