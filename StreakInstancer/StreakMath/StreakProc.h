#pragma once
#include "ocellaris/IProcedural.h"

//! @ingroup implement_group
//! \brief ��������� ������ ��������� �������� ��������
//! 
struct StreakProc : public cls::IProcedural
{
	// ������ ����������
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);
protected:
	float getRaspredelenie(float m0, float m1);
};
