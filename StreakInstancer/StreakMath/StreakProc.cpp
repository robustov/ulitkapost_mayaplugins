#include "StdAfx.h"
#include ".\streakproc.h"
#include "ocellaris\IRender.h"
#include <math/Ramp.h>
#include "Math/PerlinNoise.h"

StreakProc streakProc;

extern "C"
{
	__declspec(dllexport) cls::IProcedural* __cdecl Streak(cls::IRender* prman)
	{
		return &streakProc;
	}
}

float StreakProc::getRaspredelenie(float m0, float m1)
{
	float mmax = __max(m0, m1);
	for(;;)
	{
		float v = ((float)rand()/(float)RAND_MAX);
		float rnd2 = mmax*((float)rand()/(float)RAND_MAX);
		float mv = m0*(1-v)+m1*v;
		if(rnd2<mv) continue;
		return v;
	}
}

Math::PerlinNoise noise;

// ������ ����������
void StreakProc::Render(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
/*/
	int bBlur = 0;
	int fr0=0, fr1=1;
	float open=0, close=1;
	render->GetParameter("bBlur", bBlur);
   	render->GetParameter("frame0", fr0);
	render->GetParameter("frame1", fr1);
	render->GetParameter("shutterOpen",	open);
	render->GetParameter("shutterClose",	close);
/*/
	bool bBlur = motionBlurSamples!=0;

	std::string passname;
	if( render->GetAttribute("@passtype", passname))
	{
		printf( "%s\n", passname.c_str());
	}

	if(bBlur)
		render->SetCurrentMotionPhase(motionBlurTimes[0]);

	cls::P<int> _particleRenderType = render->GetParameter("particleRenderType");
	cls::PA<int> _id				= render->GetParameter("particleid");
	cls::PA<Math::Vec3f> _position	= render->GetParameter("P");
	cls::PA<Math::Vec3f> _velocity	= render->GetParameter("velocity");
	cls::PA<float> _age				= render->GetParameter("age");
	cls::PA<float> _radiusPP		= render->GetParameter("radiusPP");
//	cls::P<float> _radius			= render->GetParameter("radius");
	cls::P<float> _width			= render->GetParameter("width");
	cls::P<float> _tailFade			= render->GetParameter("tailFade");
	cls::P<float> _tailSize			= render->GetParameter("tailSize");
	cls::P<float> _multiRadius		= render->GetParameter("multiRadius");
	cls::P<int> _multiCount			= render->GetParameter("multiCount");

	cls::P<int> _multiCountReplace	= render->GetParameter("multiCountReplace");
	cls::P<float> _widthFactorForShadows	= render->GetParameter("widthFactorForShadows");
	cls::PS _putNormalTo			= render->GetParameter("putNormalTo");
	cls::PS _putAgeTo				= render->GetParameter("putAgeTo");
//	cls::P<bool> _bAddPoints		= render->GetParameter("bAddPoints");
//	cls::P<float> _pointsCount1		= render->GetParameter("pointsCount1");
//	cls::P<float> _pointsWidth1		= render->GetParameter("pointsWidth1");
//	cls::P<float> _pointsCount2		= render->GetParameter("pointsCount2");
//	cls::P<float> _pointsWidth2		= render->GetParameter("pointsWidth2");

	cls::PA<float> _dencityRamp = render->GetParameter("dencityRamp");
	cls::PA<float> _radiusRamp  = render->GetParameter("radiusRamp");
	cls::PA<float> _opacityRamp = render->GetParameter("opacityRamp");
	

	float noiseRadius = 0;
	float noiseFrequency = 1;		// � �������
	float time = 0;					// ����� �������
	render->GetParameter("noiseRadius",		noiseRadius);
	render->GetParameter("noiseFrequency",	noiseFrequency);
	render->GetParameter("time",			time);
	float time1 = time;					// ����

	cls::PA<int> seed = render->GetParameter("seed");

	Math::Ramp dencityRamp = Math::Ramp::Ocs2Ramp(_dencityRamp);
	Math::Ramp radiusRamp  = Math::Ramp::Ocs2Ramp(_radiusRamp);
	Math::Ramp opacityRamp = Math::Ramp::Ocs2Ramp(_opacityRamp);

	float widthJitter=0;
	render->GetParameter("widthJitter", widthJitter);

	cls::PA<Math::Vec3f> _rgbPP		= render->GetParameter("rgbPP");
	cls::PA<float> _opacityPP	= render->GetParameter("opacityPP");

	cls::PA<int> _id1;
	cls::PA<Math::Vec3f> _position1;
	if(bBlur)
	{
		render->SetCurrentMotionPhase(motionBlurTimes[motionBlurSamples-1]);
		_id1			= render->GetParameter("particleid");
		_position1		= render->GetParameter("P");
		render->GetParameter("time", time1);
	}
	render->SetCurrentMotionPhase(cls::nonBlurValue);


	bool bRgbPP = (_rgbPP.size()==_id.size());
	bool bOpacityPP = (_opacityPP.size()==_id.size());

	// type
	int type = 0;
	_particleRenderType.get(type);
	bool bMulti  = (type==0 || type==1);
	bool bPoints = (type==0 || type==3);

	//tailSize
	float tailSize = 0;
	_tailSize.get(tailSize);

	//Radius
	float minRadius=0;
	_multiRadius.get(minRadius);

	// _multiCount
	int multiCount = 1;
	_multiCount.get(multiCount);
	if(_multiCountReplace.isValid()) 
		if( _multiCountReplace.data()!=0) 
			multiCount = _multiCountReplace.data();
	if( !bMulti)
		multiCount = 1;

	// uniformWidth
	float uniformWidth = 0.1f, WidthFactorForShadows = 1.f;
	_width.get(uniformWidth);
	_widthFactorForShadows.get(WidthFactorForShadows);
	if( passname=="shadow")
		uniformWidth *= WidthFactorForShadows;

	float uniformWidthFactor = 1;

	bool bRadiusPP = _radiusPP.size()>=_position.size();

	int particleCount = _id.size();
	int nlines = multiCount*particleCount;
	int vertsPerLine = (bPoints)?(1):(2);
	int vc = nlines*vertsPerLine;

	// bAges 
	bool bAges  = false;
	if(!_putAgeTo.empty())
		if( *_putAgeTo.data()) bAges  = true;
	if( _age.empty()) bAges = false;

	// bNorms
	bool bNorms = false;
	if(!_putNormalTo.empty())
		if( *_putNormalTo.data()) bNorms = true;
	if(!bMulti) bNorms = false;

	// bBlur
	if( _position1.empty() || _id1.empty())
		bBlur = false;

	std::map<int, int> id1_map;
	if(bBlur)
	{
		for(int i=0; i<_id1.size(); i++)
		{
			int id = _id1[i];
			id1_map[id] = i;
		}
	}

	cls::PA<Math::Vec3f> pos(cls::PI_VERTEX, vc, cls::PT_POINT);
	cls::PA<Math::Vec3f> pos1(cls::PI_VERTEX, bBlur?vc:0, cls::PT_POINT);
	cls::PA<float> widths(cls::PI_VARYING, vc);
	cls::PA<int> nverts(cls::PI_PRIMITIVE, nlines);
	cls::PA<Math::Vec3f> norms(cls::PI_VERTEX, bNorms?vc:0, cls::PT_NORMAL);
	cls::PA<float> ages(cls::PI_VARYING, bAges?vc:0);

	cls::PA<Math::Vec3f> Cs(cls::PI_VARYING, bRgbPP?vc:0, cls::PT_COLOR);
	cls::PA<Math::Vec3f> Os(cls::PI_VARYING, vc, cls::PT_COLOR);

	Math::Box3f box;
	srand(0);
	for(int s=0; s<particleCount; s++)
	{
		Math::Vec3f p = _position[s];
		Math::Vec3f pblur = p;
		Math::Vec3f v = _velocity[s];
		float radius = minRadius;
		float age = 0;
		if( bRadiusPP)
			radius = _radiusPP[s];
		if( !_age.empty())
			age = _age[s];

		int id = _id[s];
		if( s<seed.size())
		{
			srand(seed[s]);
		}
		else
		{
			srand(id);
			srand( rand()+id);
			rand();
		}

		float width = uniformWidth;
		float widthFactor = uniformWidthFactor;
		if(bBlur)
		{
			std::map<int, int>::iterator it = id1_map.find(_id[s]);
			if( it!=id1_map.end())
			{	
				int fr1index = it->second;
				pblur = _position1[fr1index];
			}
			else
				widthFactor = 0;
		}

		Math::Vec3f OsC(1);
		if(bOpacityPP)
			OsC = Math::Vec3f(_opacityPP[s]);
		else if( _opacityPP.size()==1)
			OsC = Math::Vec3f(_opacityPP[0]);

		for(int m=0; m<multiCount; m++)
		{
			int i = s*multiCount + m;
			nverts[i] = vertsPerLine;

			float w = width;
			Math::Vec3f n(0, 1, 0);
			Math::Vec3f p0 = p;
			Math::Vec3f p1 = pblur;
			Math::Vec3f ofs(0), ofs1(0);
			Math::Vec3f opacity = OsC;
			if( bMulti)
			{
				// ��������� �������
				ofs = Math::Vec3f(1, 1, 1);
				for(;;)
				{
					ofs.x = 1-2*rand()/(float)RAND_MAX;
					ofs.y = 1-2*rand()/(float)RAND_MAX;
					ofs.z = 1-2*rand()/(float)RAND_MAX;

					float ofsparam = ofs.length();
					if( ofsparam>1) 
						continue;
					float prop = dencityRamp.getValue(ofsparam);

					if( prop>0.99) 
						break;
					float proptest = rand()/(float)RAND_MAX;
					if(proptest>prop)
						continue;
					break;
				}
				float ofsparam = ofs.length();
				w *= radiusRamp.getValue(ofsparam);

				ofs *= radius;
				ofs1 = ofs;

				// ���
				if( noiseRadius>0 && noiseFrequency>0)
				{
					// 
					Math::Vec4f rands(
						rand()/(float)RAND_MAX+0.1333f*s+0.333f*m, 
						rand()/(float)RAND_MAX+0.1333f*s+0.333f*m, 
						rand()/(float)RAND_MAX+0.1333f*s+0.333f*m, 
						rand()/(float)RAND_MAX+0.1333f*s+0.333f*m
						);
#ifdef _DEBUG
					printf("%d(%d): %f %f %f\n", s, m, rands.x, rands.y, rands.z);
#endif
					Math::Vec3f noiseofs(0);
					float p = time/noiseFrequency+rands[3];
					noiseofs[0] = noise.noise(p, rands[0], (float)M_PI);
					noiseofs[1] = noise.noise((float)(1/M_PI), p, rands[1]);
					noiseofs[2] = noise.noise(rands[2], 0.56f, p);

					noiseofs *= noiseRadius;
					ofs += noiseofs;

					if(bBlur)
					{
						float p = time1/noiseFrequency+rands[3];
						noiseofs[0] = noise.noise(p, rands[0], (float)M_PI);
						noiseofs[1] = noise.noise((float)(1/M_PI), p, rands[1]);
						noiseofs[2] = noise.noise(rands[2], 0.56f, p);
						noiseofs *= noiseRadius;
						ofs1 += noiseofs;
					}
				}

				n = ofs;
				n.normalize();

				opacity *= opacityRamp.getValue(ofsparam);
			}
			int vi = i*vertsPerLine;
//if(vi==0)
//	displayString("w = %f, widthFactor=%f", w, widthFactor);
			w *= widthFactor;

			float wj = 1-widthJitter*rand()/(float)RAND_MAX;
			if(wj<0.001) wj=0.001f;
			w *= wj;
			

			p0+=ofs;
			pos[vi+0] = p0;
			widths[vi+0] = w;
			if( bNorms)
				norms[vi+0] = n;
			if( bAges)
				ages[vi+0] = age;
			if(bRgbPP)
				Cs[vi+0] = _rgbPP[s];
			if(!Os.empty())
				Os[vi+0] = opacity;

			box.insert(p0);

			if(bBlur)
			{
				p1 += ofs1;
				pos1[vi+0] = p1;
				box.insert(p1);
			}

			if( !bPoints)
			{
				Math::Vec3f p1 = p0-v*tailSize;
				pos[vi+1] = p1;
				widths[vi+1] = w;
				if( bNorms)
					norms[vi+1] = n;
				if( bAges)
					ages[vi+1] = age;
				if(bRgbPP)
					Cs[vi+1] = _rgbPP[s];
				if(!Os.empty())
					Os[vi+0] = opacity;
				box.insert(p1);
			}
		}
	}


	render->PushAttributes();
//	render->Parameter("constantwidth", 0.1f);
	render->Parameter("#width", widths);

	if(bRgbPP)
	{	
		render->Parameter("Cs", Cs);
	}
	else if( _rgbPP.size()==1)
	{
		Math::Vec3f CsC = _rgbPP[0];
		cls::PA<Math::Vec3f> Cs(cls::PI_VARYING, vc, cls::PT_COLOR);
		for(int i=0; i<vc; i++)
			Cs[i] = CsC;
		render->Parameter("Cs", Cs);
	}

	if(!Os.empty())
	{	
		render->Parameter("Os", Os);
	}


	if( !bBlur)
	{
		render->Parameter("P", pos);
	}
	else
	{
		render->SetCurrentMotionPhase(motionBlurTimes[0]);
		render->Parameter("P", pos);

		render->SetCurrentMotionPhase(motionBlurTimes[motionBlurSamples-1]);
		render->Parameter("P", pos1);

		render->SetCurrentMotionPhase(cls::nonBlurValue);
	}

	if( bNorms)
		render->Parameter( _putNormalTo.data(), norms);
	if( bAges)
		render->Parameter( _putAgeTo.data(), ages);

	if( bPoints)
	{
		render->SystemCall(cls::SC_POINTS);
	}
	else
	{
		render->Parameter("#curves::interpolation", "linear");
		render->Parameter("#curves::wrap", "nonperiodic");
		render->Parameter("#curves::nverts", nverts);
		render->SystemCall(cls::SC_CURVES);
	}
	render->PopAttributes();

}
