#include "StdAfx.h"

#include "ocellaris\IRender.h"
#include <math/Ramp.h>
#include "Math/PerlinNoise.h"
#include "ocellaris/IProcedural.h"

struct FluidToPointsProc : public cls::IProcedural
{
	// ������ ����������
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);
protected:
	float getRaspredelenie(float m0, float m1);
};
FluidToPointsProc fluidtopointsproc;

extern "C"
{
	__declspec(dllexport) cls::IProcedural* __cdecl FluidToPoints(cls::IRender* prman)
	{
		return &fluidtopointsproc;
	}
}

inline float rnd()
{
	float r = rand()/(float)RAND_MAX;
	return r;
}

float FluidToPointsProc::getRaspredelenie(float m0, float m1)
{
	float mmax = __max(m0, m1);
	for(;;)
	{
		float v = ((float)rand()/(float)RAND_MAX);
		float rnd2 = mmax*((float)rand()/(float)RAND_MAX);
		float mv = m0*(1-v)+m1*v;
		if(rnd2<mv) continue;
		return v;
	}
}

Math::PerlinNoise noisefluid;

// ������ ����������
void FluidToPointsProc::Render(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
	if( motionBlurTimes)
		render->SetCurrentMotionPhase(motionBlurTimes[0]);

	// ��� ������ ����!!!
	cls::PA<unsigned int> res = render->GetParameter("#resolution");
	cls::PA<float> dim = render->GetParameter("#dimentions");
	float ds = 0;
	render->GetParameter("#densitySum", ds);
	cls::PA<float> density = render->GetParameter("#density");

	if(res.size()!=3 || dim.size()!=3)
		return;

	if( density.size()!=res[0]*res[1]*res[2])
		return;

	cls::PA<Math::Vec3f> color = render->GetParameter("#color");
	if( color.size()!=density.size()) 
		color = cls::Param();

	Math::Vec3f dimcell( dim[0]/res[0], dim[1]/res[1], dim[2]/res[2]);
	Math::Vec3f dimoffset( -0.5f*dim[0], -0.5f*dim[1], -0.5f*dim[2]);
	float middimcell = (dimcell.x+dimcell.y+dimcell.z)/3;
	int seed = 1020930;
	int particlePerCell = 20;
	render->GetParameter( "@particlePerCell", particlePerCell);
	srand(seed);

	cls::PA<Math::Vec3f> P(cls::PI_VERTEX, particlePerCell*density.size());
	cls::PA<float> W(cls::PI_VARYING, particlePerCell*density.size());
	cls::PA<Math::Vec3f> Os(cls::PI_VARYING, particlePerCell*density.size(), cls::PT_COLOR);
	cls::PA<Math::Vec3f> Cs(cls::PI_VARYING, color.empty()?0:particlePerCell*density.size(), cls::PT_COLOR);
	
	float widthDencityFactor = 1;
	float opacityDencityFactor = 1;
	float constOpacity = 0.1f;
	float constWidth = 0.0f;
	render->GetParameter( "widthDencityFactor", widthDencityFactor);
	render->GetParameter( "opacityDencityFactor", opacityDencityFactor); 
	render->GetParameter( "constOpacity", constOpacity);
	render->GetParameter( "constWidth", constWidth);

	constWidth *= middimcell;
	widthDencityFactor *= middimcell;
	opacityDencityFactor *= 1;

	// ������� �������
	int vindex=0;
	for(unsigned int z = 0; z<res[2]; z++)
	{
		for(unsigned int y = 0; y<res[1]; y++)
		{
			for(unsigned int x = 0; x<res[0]; x++)
			{
				for(int i=0; i<particlePerCell; i++)
				{
					int myind = x + y*res[0] + z*res[0]*res[1];

					// �������
					Math::Vec3f v( rnd(), rnd(), rnd());
					v = v*dimcell;
					v += Math::Vec3f( (float)x, (float)y, (float)z)*dimcell + dimoffset;

					// ������ 
					float w = density[myind]*widthDencityFactor + constWidth;

					// opacity
					float os = density[myind]*opacityDencityFactor;
					os += constOpacity;

					P[vindex] = v;
					W[vindex] = w;
					Os[vindex] = Math::Vec3f( os);

					// color
					if(!color.empty())
						Cs[vindex] = color[myind];

					vindex++;
				}
			}
		}
	}

	render->SetCurrentMotionPhase(cls::nonBlurValue);

	// ���� ��� �����
	{
		render->PushAttributes();
		render->Parameter("P", P);
		render->Parameter("width", W);
		render->Parameter("Os", Os);
		if( !Cs.empty())
			render->Parameter("Cs", Cs);

		render->SystemCall(cls::SC_POINTS);
		render->PopAttributes();
	}

	/*/
	if( !motionBlurTimes)
	{
		render->Parameter("P", pos);
	}
	else
	{
		render->SetCurrentMotionPhase(motionBlurTimes[0]);
		render->Parameter("P", pos);

		render->SetCurrentMotionPhase(motionBlurTimes[motionBlurSamples-1]);
		render->Parameter("P", pos1);

		render->SetCurrentMotionPhase(cls::nonBlurValue);
	}

	if( bPoints)
	{
		render->SystemCall(cls::SC_POINTS);
	}
	else
	{
		render->Parameter("#curves::interpolation", "linear");
		render->Parameter("#curves::wrap", "nonperiodic");
		render->Parameter("#curves::nverts", nverts);
		render->SystemCall(cls::SC_CURVES);
	}
	render->PopAttributes();
	/*/

}
