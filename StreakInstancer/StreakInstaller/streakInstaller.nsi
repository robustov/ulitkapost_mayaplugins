; example2.nsi
;
; This script is based on example1.nsi, but it remember the directory, 
; has uninstall support and (optionally) installs start menu shortcuts.
;
; It will install example2.nsi into a directory that the user selects,

;--------------------------------
!include "WordFunc.nsh"
!include "TextFunc.nsh"

!insertmacro un.WordAdd


!ifdef MAYA70
	Name "streak70"
	OutFile "$%ULITKABIN%\streak70.exe"
!endif
!ifdef MAYA2008
	Name "streak2008"
	OutFile "$%ULITKABIN%\streak2008.exe"
!endif


; The default installation directory
InstallDir ""

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
!ifdef MAYA70
	InstallDirRegKey HKLM "Software\ulitkabin70" "Install_Dir"
!endif
!ifdef MAYA2008
	InstallDirRegKey HKLM "Software\ulitkabin2008" "Install_Dir"
!endif


;--------------------------------
; Pages

Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "Ocllaris"

	SectionIn RO

	;-----------------------------------------
	; BIN
	SetOutPath $INSTDIR\bin
  
		; �� ������� �� ������ ����
		File "$%ULITKABIN%\binrelease\StreakMath.dll"
	  
		!ifdef MAYA70
			File "$%ULITKABIN%\binrelease\StreakShape.mll"
		!endif
		!ifdef MAYA2008
			File "$%ULITKABIN%\binrelease85\StreakShape.mll"
		!endif
	
	;-----------------------------------------
	; MEL
	SetOutPath $INSTDIR\mel

		File "$%ULITKABIN%\mel\AEStreakShapeTemplate.mel"
		File "$%ULITKABIN%\mel\StreakShapeMenu.mel"	


	;-----------------------------------------
	; Write the installation path into the registry
	!ifdef MAYA70
		WriteRegStr HKLM SOFTWARE\ulitkabin70 "Install_Dir" "$INSTDIR"
	!endif
	!ifdef MAYA2008
		WriteRegStr HKLM SOFTWARE\ulitkabin2008 "Install_Dir" "$INSTDIR"
	!endif
  
SectionEnd

;--------------------------------
; Uninstaller
Section "Uninstall"

SectionEnd
