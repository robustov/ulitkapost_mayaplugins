//
// Copyright (C) a.mashuta
// 
// File: motionArcsCmd.cpp
//
// MEL Command: motionArcs
//
// Author: Maya Plug-in Wizard 2.0
//

#include "motionArcsCmd.h"

#include <pre.h>
#include <maya/MGlobal.h>
#include <maya/MSelectionList.h>
#include <maya/MItSelectionList.h>
#include <maya/MDagPath.h>
#include <maya/MFnTransform.h>
#include <maya/MPlug.h>
#include <maya/MMatrixArray.h>
#include <maya/MAnimControl.h>

MStatus motionArcs::doIt( const MArgList& )
{
	MStatus status = MS::kSuccess;

	// Get a list of currently selected objects
	MSelectionList selection;
	MGlobal::getActiveSelectionList( selection );
	
	// Iterate over the transform nodes
	MItSelectionList iter( selection, MFn::kTransform );
	for ( ; !iter.isDone(); iter.next() )
	{
		MDagPath dagPath;
		if ( MS::kSuccess != iter.getDagPath( dagPath ) )
		{
			displayError( "cannot to get dagPath for transform object." );
			continue;
		}
		status = createMotionArcsShapeFor( dagPath );
		if ( MS::kSuccess != status )
		{
			displayError( MString() + "error while creating motionArcs for " + dagPath.fullPathName() + " node" );
			continue;
		}
		displayString( (MString("motionArcs was created for ") + dagPath.fullPathName() + "").asChar() );
	}
	return redoIt();
}

MStatus motionArcs::redoIt()
{
	dgMod.doIt();
	setResult( "motionArcs command executed!\n" );
	return MS::kSuccess;
}

MStatus motionArcs::undoIt()
{
	dgMod.undoIt();
	dagMod.undoIt();
    MGlobal::displayInfo( "motionArcs command undone!\n" );
	return MS::kSuccess;
}

void* motionArcs::creator()
{
	return new motionArcs();
}

motionArcs::motionArcs()
{}

motionArcs::~motionArcs()
{
}

bool motionArcs::isUndoable() const
{
	return true;
}

MStatus motionArcs::createMotionArcsShapeFor( const MDagPath& dagPath )
{
	MStatus status;
	status = configureNodeForRigthUpdating();
	if ( MS::kSuccess != status )
	{
		displayError( "error while configure node for right updating..." );
		return status;
	}

	MFnDependencyNode snapshotNode;
	status = createSnapshotNode( snapshotNode );
	if ( MS::kSuccess != status )
	{
		displayError( "error while creating snapshot and configuring node..." );
	}
	
	MFnTransform objectTransformFn( dagPath );
	status = connectObjectToSnapshotNode( objectTransformFn, snapshotNode );
	if ( MS::kSuccess != status )
	{
		displayError( "error while connecting object's plugs to snapshot's plugs..." );
		return status;
	}
	
	MFnDependencyNode maShapeNode;
	status = createMotionArcsShapeNode( maShapeNode );
	if ( MS::kSuccess != status )
	{
		displayError( "error while creating motionArcsShape node..." );
		return status;
	}

	status = dgMod.connect( snapshotNode.findPlug( "points" ), maShapeNode.findPlug( "points" ) );
	if ( MS::kSuccess != status ) 
	{
		displayError( "cannot to connect snapshot's points to motionArcs' ones..." );
		return status;
	}
	
	status = dgMod.connect( snapshotNode.findPlug( "frames" ), maShapeNode.findPlug( "frames" ) );
	if (   MS::kSuccess != status ) 
	{
		displayError( "cannot to connect snapshot's frames to motionArcs' ones..." );
		return status;
	}
		
	return MS::kSuccess;
}

MStatus motionArcs::configureNodeForRigthUpdating()
{
	MString name = "__tmpGroup__";
	MStatus status;
	status = dgMod.commandToExecute( "snapshot -st 1 -et 2 -ch true -i 1000000 -name " + name);
	if ( MS::kSuccess != status )
	{
		displayError( "error while executing snapshot command..." );
		return status;
	}
	
	status = dgMod.commandToExecute( "delete " + name + "Group" );
	if ( MS::kSuccess != status )
	{
		displayError( "error while executing snapshot command..." );
		return status;
	}
	return MS::kSuccess;
}

MStatus motionArcs::createSnapshotNode( MFnDependencyNode& snapshotNode )
{
	MStatus status = MS::kSuccess;
	MObject snapshot = dgMod.createNode( "snapshot", &status );
	if ( MS::kSuccess != status )
	{
		displayError( "error while creating snapshot node..." );
		return status;
	}
	snapshotNode.setObject( snapshot );
	MPlug startTimePlug = snapshotNode.findPlug( "startTime" );
	startTimePlug.setValue( MAnimControl::minTime() );
	MPlug endTimePlug = snapshotNode.findPlug( "endTime" );
	endTimePlug.setValue( MAnimControl::maxTime() );
	return MS::kSuccess;
}

MStatus motionArcs::connectObjectToSnapshotNode( const MFnTransform& objectTransformFn, const MFnDependencyNode& snapshotNode )
{
	objectTransformFn.findPlug( "worldMatrix" ).evaluateNumElements();
	
	MStatus status = dgMod.connect( objectTransformFn.findPlug( "worldMatrix" ).elementByPhysicalIndex( 0 ), snapshotNode.findPlug( "inputMatrix" ) );
	if ( MS::kSuccess != status )
		//|| MS::kSuccess != dgMod.connect( transformFn.findPlug( "selectHandle" ), snapshotNode.findPlug( "localPosition" ) ) )
	{
		displayError( "cannot to connect object's worldMatrix[0] to snapshot's inputMatrix..." );
		return status;
	}
	return MS::kSuccess;
}

MStatus	motionArcs::createMotionArcsShapeNode( MFnDependencyNode& maShapeNode )
{
	MStatus status = MS::kSuccess;
	MObject maTranform = dagMod.createNode( "motionArcsShape", MObject::kNullObj, &status );
	if ( MS::kSuccess != status )
	{
		displayError( "error while creating motionArcsShape..." );
		return status;
	}
	dagMod.renameNode(maTranform, "motionArcs1");
	dagMod.doIt();
	
	MFnTransform maTransformFn( maTranform );
	MObject maShape = maTransformFn.child( 0 );
	maShapeNode.setObject( maShape );
	MObject pointsPlug = maShapeNode.findPlug( "points" );
	if ( pointsPlug.isNull() )
	{
		displayError( "points plug is null..." );
		return MS::kFailure;
	}
	MObject framesPlug = maShapeNode.findPlug( "frames" );
	if ( framesPlug.isNull() ) 
	{
		displayError( "frames plug is null..." );
		return MS::kFailure;
	}
	return MS::kSuccess;
}