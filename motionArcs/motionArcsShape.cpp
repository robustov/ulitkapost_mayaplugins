// Copyright (C) 
// File: motionArcsShape.cpp
// Node: motionArcsShape

#include "motionArcsShape.h"

#include <pre.h>
#include <utils.h>
#include <maya/MAnimControl.h>
#include <maya/MAnimUtil.h>
#include <maya/MGlobal.h>
#include <maya/MFnNumericData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MFn.h>
#include <maya/MColor.h>
#include <maya/MFnTransform.h>
#include <maya/MAnimMessage.h>
#include <maya/MFnDependencyNode.h>
#include <string>
#include <sstream>
#include <algorithm>
#include <cmath>

// attributes
MObject motionArcsShape::points;
MObject motionArcsShape::frames;
MObject motionArcsShape::usePointsSize;
MObject motionArcsShape::pointIncrement;
MObject motionArcsShape::pointsSize;
MObject motionArcsShape::widthMultiplier;
MObject motionArcsShape::colorMultiplier;
MObject motionArcsShape::coordinates;
MObject motionArcsShape::update;
MObject motionArcsShape::startFrame;
MObject motionArcsShape::curFrame;
MObject motionArcsShape::endFrame;
MObject motionArcsShape::aroundFrame;
MObject motionArcsShape::freezeFrame;
MObject motionArcsShape::inheritedOverrideColor;
MObject motionArcsShape::allFrames;
MObject motionArcsShape::useInheritedColor;
MObject motionArcsShape::color;

// default values
const double motionArcsShape::WIDTH_MULTIPLIER			= 3.0;
const double motionArcsShape::COLOR_MULTIPLIER			= 64.0;
const bool	 motionArcsShape::USE_POINTS_SIZE			= false;
const double motionArcsShape::POINT_INCREMENT			= 2.0;
const double motionArcsShape::POINTS_SIZE				= 4.0;
const bool	 motionArcsShape::SHOW_ALL_TRAJECTORY		= false;
const int	 motionArcsShape::UPDATE					= 2;
const double motionArcsShape::START_FRAME				= 1.0;
const double motionArcsShape::CUR_FRAME					= 12.0;
const double motionArcsShape::END_FRAME					= 24.0;
const int	 motionArcsShape::AROUND_FRAME				= 10;
const bool	 motionArcsShape::FREEZE_FRAME				= false;
const bool	 motionArcsShape::USE_INHERITED_COLOR		= true;
const int	 motionArcsShape::INHERITED_OVERRIDE_COLOR	= 14;


// other constants...
const MString motionArcsShape::internalErrorMessage( "MotionArcs internal error. Detailed error's information: " );

// --------- MPxLocatorNode INTERFACE -----------------------------------------------
motionArcsShape::motionArcsShape()
{
}
motionArcsShape::~motionArcsShape()
{
}

MStatus motionArcsShape::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	if( false )
	{
		data.setClean(plug);
		displayString( "compute is called" );
		return MS::kSuccess;
	}

	return MS::kUnknownParameter;
}

bool motionArcsShape::isBounded() const
{
	return true;
}

MBoundingBox motionArcsShape::boundingBox() const
{
	MPointArray pts;
	getPathPoints( pts );
	
	MBoundingBox box;
	unsigned int arcStart = getArcStartIndex();
	unsigned int arcEnd = getArcEndIndex( pts.length() );
	for ( unsigned int i = arcStart; i <= arcEnd; i++ )
		box.expand( pts[i] );
	return box;
}

void motionArcsShape::draw(M3dView &view, 
						   const MDagPath &path, 
						   M3dView::DisplayStyle style,
						   M3dView::DisplayStatus status)
{
	view.beginGL();

	MPointArray pts;
	getPathPoints( pts );

	drawLines( linesColor( view ), pts, getWidthMultiplier(), getColorMultiplier() );
	drawPoints( pts, getPointsSize() );

	view.endGL();
};

void* motionArcsShape::creator()
{
	return new motionArcsShape();
}

MStatus motionArcsShape::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	displayString( "// initializing of motionArcsShape..." );

	try
	{
		createFramesAttr();
		createPointsAttr();
		createUsePointsSizeAttr();
		createPointIncrementAttr();
		createPointsSizeAttr();
		createWidthMultiplierAttr();
		createColorMultiplierAttr();
		createCoordinatesAttr();
		createUpdateAttr();
		createStartFrameAttr();
		createCurFrameAttr();
		createEndFrameAttr();
		createAroundFrameAttr();
		createFreezeFrameAttr();
		createUseInheritedColorAttr();
		createInheritedOverrideColorAttr();
		createColorAttr();
		createAllFramesAttr();

		if( !MGlobal::sourceFile("AEmotionArcsShapeTemplate.mel") )
		{
			displayString("error source AEmotionArcsShapeTemplate.mel");
		}
	}
	catch(MString err)
	{
		displayString( (MString(" exception was catched while creating motionArcsShape attributes:") + err).asChar() );
		return MS::kFailure;
	}

	displayString( "initializing of motionArcsShape is done." );

	return MS::kSuccess;
}
// ------------------------------------------------------------------------------------

// ----------------- PRIVATE ----------------------------------------------------------
void motionArcsShape::getPathPoints( MPointArray& pts ) const
{
	getPathPointsFromPointsAttr( pts );
	//getPathPointsFromCoordinatesAttr( pts );
}

void motionArcsShape::getPathPointsFromPointsAttr( MPointArray& pts ) const
{
	MStatus status;
	MObject thisNode = thisMObject();
	MFnDagNode dagFn( thisNode, &status );  
	if ( MS::kSuccess != status )
	{
		displayStringError( (internalErrorMessage + " cannot create transformFn while getting path points.").asChar() );
		return;
	}

	MPlug pointsPlug = dagFn.findPlug( points, &status );
	MObject array;
	pointsPlug.getValue( array );
	MFnPointArrayData pointArrayFn( array );
	pointArrayFn.copyTo( pts );
}

// need to optimize speed... very much :)
void motionArcsShape::getPathPointsFromCoordinatesAttr( MPointArray& pts ) const
{
/*	MStatus status;
	MObject thisNode = thisMObject();
	MFnDagNode dagFn( thisNode, &status );  

	MPlug coorPlug = dagFn.findPlug( coordinates, &status );
	MTime startTime = MAnimControl::minTime();
	MTime endTime = MAnimControl::maxTime();
	
	double framesCnt = framesBetween( startTime, endTime );
	pts.clear();
	pts.setSizeIncrement( framesCnt );
	MTime oneFrame( 1.0 );
	for ( int i = 1 ; i <= framesCnt ; i++ )
	{
		MPoint pt;
		getNextPoint( coorPlug, startTime + oneFrame * (i - 1), pt );
		pts.append( pt );
	}*/
}

void motionArcsShape::getNextPoint( MPlug& coorPlug, MTime& time, MPoint& pt ) const
{
	MDGContext context( time );
	MObject coordinatesObj;
	coorPlug.getValue( coordinatesObj, context );

	MFnNumericData fnData;
	fnData.setObject( coordinatesObj );
	fnData.getData( pt.x, pt.y, pt.z );
}

double motionArcsShape::getFPS() const
{
	MTime oneFrame( 1.0 );
	MTime oneSecond( 1000.0, MTime::kMilliseconds );
	return oneSecond.value() / oneFrame.as( MTime::kMilliseconds );
}

double motionArcsShape::framesBetween( const MTime& startTime, const MTime& endTime ) const
{
	MTime oneFrame( 1.0 );
	return ( endTime - startTime + oneFrame ).as( MTime::kMilliseconds ) / 1000.0 * getFPS();
}

unsigned int motionArcsShape::getArcStartIndex() const
{
	if ( showAllFrames() )
		return 0;
	
	double cur = freezedFrame() ? getCurrentFrame() : MAnimControl::currentTime().value();
	int start = (int)(double)(cur - getAroundFrame() - getStartFrame()); // ok
	return std::max( start, 0 );
}

unsigned int motionArcsShape::getArcEndIndex( unsigned int length ) const
{
	if ( showAllFrames() )
		return length - 1;

	double cur = freezedFrame() ? getCurrentFrame() : MAnimControl::currentTime().value();
	int end = (int)(double)(cur + getAroundFrame() - getStartFrame()); // ok
	return std::min( (unsigned int) end, length - 1);
}

bool motionArcsShape::needToUseInheritedColor() const
{
	MPlug usePlug = MFnDagNode( thisMObject() ).findPlug( useInheritedColor );
	return usePlug.asBool();
}


MColor motionArcsShape::getColor() const
{
	MPlug colorPlug = MFnDagNode( thisMObject() ).findPlug( color );
	float r = colorPlug.child(0).asFloat();
	float g = colorPlug.child(1).asFloat();
	float b = colorPlug.child(2).asFloat();
	return MColor( r, g, b );
}

MColor motionArcsShape::linesColor( M3dView& view ) const
{
	if ( !needToUseInheritedColor() )
		return getColor();
	int colorIndex = MPlug( thisMObject(), inheritedOverrideColor ).asInt() - 1;
	return view.colorAtIndex( colorIndex );
}

double motionArcsShape::getStartFrame() const
{
	MPlug startFramePlug = MFnDagNode( thisMObject() ).findPlug( startFrame );
	return startFramePlug.asMTime().value();
}

double motionArcsShape::getEndFrame() const
{
	MPlug endFramePlug = MFnDagNode( thisMObject() ).findPlug( endFrame );
	return endFramePlug.asMTime().value();
}

double motionArcsShape::getCurrentFrame() const
{
	MPlug curFramePlug = MFnDagNode( thisMObject() ).findPlug( curFrame );
	return curFramePlug.asMTime().value();
}

int motionArcsShape::getAroundFrame() const
{
	MPlug aroundFramePlug = MFnDagNode( thisMObject() ).findPlug( aroundFrame );
	return aroundFramePlug.asInt();
}

float motionArcsShape::getPointsSize() const
{
	MFnDagNode dagFn( thisMObject() );
	MPlug pointsSizePlug = dagFn.findPlug( pointsSize );
	return pointsSizePlug.asFloat();
}

bool motionArcsShape::showAllFrames() const
{
	MFnDagNode dagFn( thisMObject() );
	MPlug allFramesPlug = dagFn.findPlug( allFrames );
	return allFramesPlug.asBool();
}

bool motionArcsShape::freezedFrame() const
{
	MFnDagNode dagFn( thisMObject() );
	MPlug freezeFramePlug = dagFn.findPlug( freezeFrame );
	return freezeFramePlug.asBool();
}

void motionArcsShape::drawPoints( const MPointArray& pts, float size ) const
{
	if ( equiv( (float)0.0, (float)size ) )
		return;
	glPushAttrib( GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT | GL_PIXEL_MODE_BIT | GL_POINT_BIT);

	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	glEnable( GL_POINT_SMOOTH );
	glHint( GL_POINT_SMOOTH_HINT, GL_DONT_CARE );

	unsigned int arcStart = getArcStartIndex();
	unsigned int arcEnd = getArcEndIndex( pts.length() );
	
	if ( needToUsePointsSize() )
		for ( unsigned int i = arcStart; i <= arcEnd; i++ )
			drawPoint( pts[i], size );
	else
	{
		float multiplier = getWidthMultiplier();
		for ( unsigned int i = arcStart; i <= arcEnd; i++ )
			drawPoint( pts[i], i, arcStart, arcEnd, multiplier, getPointIncrement() );
	}
	
	glPopAttrib();	
}

void motionArcsShape::drawPoint( const MPoint& pt, float size ) const
{
	glPointSize( size );

	glBegin( GL_POINTS );
	glVertex3f( float(pt.x), float(pt.y), float(pt.z) );
	glEnd();	
}

void motionArcsShape::drawPoint( const MPoint& pt, unsigned int index, unsigned int arcStart, unsigned int arcEnd, float multiplier, float increment ) const
{
	glPointSize( getPointSizeByIndex( index, arcStart, arcEnd, multiplier ) + increment );
	
	glBegin( GL_POINTS );
	glVertex3f( float(pt.x), float(pt.y), float(pt.z) );
	glEnd();	
}

float motionArcsShape::getPointSizeByIndex( unsigned int index, unsigned int arcStart, unsigned int arcEnd, float multiplier ) const
{
	if ( index > arcStart && index < arcEnd )
		return std::max( getWidthByIndex( index, arcStart, arcEnd, multiplier ), 
						 getWidthByIndex( index + 1, arcStart, arcEnd, multiplier ) );
	if ( index == arcStart )
		return getWidthByIndex( std::min( arcStart + 1, arcEnd ) , arcStart, arcEnd, multiplier );
	if ( index == arcEnd )
		return getWidthByIndex( index , arcStart, arcEnd, multiplier );
	return 50.0; // if something wrong...
}

float motionArcsShape::getPointIncrement() const
{
	MFnDagNode dagFn( thisMObject() );
	MPlug pointIncrementPlug = dagFn.findPlug( pointIncrement );
	return pointIncrementPlug.asFloat();
}

bool motionArcsShape::needToUsePointsSize() const
{
	MFnDagNode dagFn( thisMObject() );
	MPlug usePointsSizePlug = dagFn.findPlug( usePointsSize );
	return usePointsSizePlug.asBool();
}

float motionArcsShape::getWidthMultiplier() const
{
	MFnDagNode dagFn( thisMObject() );
	MPlug widthMultiplierPlug = dagFn.findPlug( widthMultiplier );
	return widthMultiplierPlug.asFloat();
}

double motionArcsShape::getColorMultiplier() const
{
	MFnDagNode dagFn( thisMObject() );
	MPlug colorMultiplierPlug = dagFn.findPlug( colorMultiplier );
	return colorMultiplierPlug.asDouble();
}

void motionArcsShape::drawLines( const MColor& color, const MPointArray& pts, float widthMultiplier, double colorMultiplier ) const
{
	if ( equiv( (float)widthMultiplier, (float)0.0 ) )
		return;
	glPushAttrib( GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT | GL_PIXEL_MODE_BIT | GL_LINE_BIT );

	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	glEnable( GL_LINE_SMOOTH );	
	glHint( GL_LINE_SMOOTH_HINT, GL_DONT_CARE );

	unsigned int arcStart = getArcStartIndex();
	unsigned int arcEnd = getArcEndIndex( pts.length() );
	for ( unsigned int i = arcStart + 1; i <= arcEnd; i++ )
	{
		double dist = (pts[i] - pts[i-1]).length();
		if ( equiv( dist, 0.0) )
			continue;
		float width = getWidthByIndex( i, arcStart, arcEnd, widthMultiplier );
		glLineWidth( width );
		glBegin( GL_LINE_STRIP );
		//glColor3f( 0.00390625 * dist * colorMultiplier, 0.0, 0.0 );
		glColor3f( color.r, color.g, color.b );
		glVertex3f( float(pts[i-1].x), float(pts[i-1].y), float(pts[i-1].z) );
		glVertex3f( float(pts[i].x), float(pts[i].y), float(pts[i].z) );
		glEnd();
	}

	glPopAttrib();
}

float motionArcsShape::getWidthByIndex( unsigned int index, unsigned int arcStart, unsigned int arcEnd, float widthMultiplier) const
{
	double cur = MAnimControl::currentTime().value() - getStartFrame();
	if ( equiv( double(index), cur ) || equiv( double(index - 1), cur ) || (cur < index && cur > index - 1) )
		return widthMultiplier + 2.0f;
	
	double half = (arcEnd - arcStart) / 2.0;
	double x = index;
	double a = widthMultiplier;
	double b = showAllFrames() ? half : ( freezedFrame() ? getCurrentFrame() : MAnimControl::currentTime().value() ) - getStartFrame();
	double c = half / 1.5;
	
	return (float)((gaussian( x, a, b, c ) + gaussian( x - 1.0f, a, b, c )) / 2.0);
}

double motionArcsShape::gaussian( double x, double a, double b, double c ) const
{
	double power = - (x - b) * (x - b) / (2.0 * c * c);
	return a * exp( power );
}

void motionArcsShape::createFramesAttr()
{
	createTypedAttr( 
		frames, "frames", "fms",
		MFnData::kIntArray, MObject::kNullObj,
		atInput );
}

void motionArcsShape::createPointsAttr()
{
	createTypedAttr( 
		points, "points", "pts",
		MFnData::kPointArray, MObject::kNullObj,
		atInput );
}

void motionArcsShape::createUsePointsSizeAttr()
{
	createNumericAttr(
		usePointsSize, "usePointsSize","ups", MFnNumericData::kBoolean, USE_POINTS_SIZE,
		atInput,
		false, false, 0.0,
		false, false, 0.0 );
}

void motionArcsShape::createPointIncrementAttr()
{
	createNumericAttr(
		pointIncrement, "pointIncrement","pin", MFnNumericData::kFloat, POINT_INCREMENT,
		atInput,
		true, false, -6.0,
		true, false,  6.0 );
}

void motionArcsShape::createPointsSizeAttr()
{
	createNumericAttr(
		pointsSize, "pointsSize","ps", MFnNumericData::kFloat, POINTS_SIZE,
		atInput,
		true, false, 0.0,
		true, false, 9.0 );
}

void motionArcsShape::createWidthMultiplierAttr()
{
	createNumericAttr(
		widthMultiplier, "widthMultiplier","wmu", MFnNumericData::kFloat, WIDTH_MULTIPLIER,
		atInput,
		true, false, 0.0,
		true, false, 6.0 );
}

void motionArcsShape::createColorMultiplierAttr()
{
	createNumericAttr(
		colorMultiplier, "colorMultiplier","cmu", MFnNumericData::kDouble, COLOR_MULTIPLIER,
		atInput,
		true, false, 0.0,
		true, false, 256.0 );
}

void motionArcsShape::createCoordinatesAttr()
{
	createNumericAttr(
		coordinates, "coordinates","c_s", MFnNumericData::k3Double, 0.0,
		atInput,
		false, false, 0.0,
		false, false, 0.0 );
}

void motionArcsShape::createUpdateAttr()
{
	createNumericAttr(
		update, "update","up", MFnNumericData::kInt, UPDATE,
		atInput,
		true, false, 0.0,
		true, false, 2.0 );
}

void motionArcsShape::createStartFrameAttr()
{
	createUnitAttr<MTime>(
		startFrame, "startFrame","stf", MFnUnitAttribute::kTime, START_FRAME,
		atInput,
		false, true, 1.0,
		false, true, 24.0 );
}

void motionArcsShape::createCurFrameAttr()
{
	createUnitAttr<MTime>(
		curFrame, "curFrame","cuf", MFnUnitAttribute::kTime, CUR_FRAME,
		atInput,
		false, true, 1.0,
		false, true, 24.0 );
}

void motionArcsShape::createEndFrameAttr()
{
	createUnitAttr<MTime>(
		endFrame, "endFrame","enf", MFnUnitAttribute::kTime, END_FRAME,
		atInput,
		false, true, 0.0,
		false, true, 24.0 );
}

void motionArcsShape::createAroundFrameAttr()
{
	createNumericAttr(
		aroundFrame, "aroundFrame","arf", MFnNumericData::kInt, AROUND_FRAME,
		atInput,
		true, false, 0.0,
		false, true, 12.0 );
}

void motionArcsShape::createFreezeFrameAttr()
{
	createNumericAttr(
		freezeFrame, "freezeFrame","frf", MFnNumericData::kBoolean, FREEZE_FRAME,
		atInput,
		false, false, 0.0,
		false, false, 0.0 );
}

void motionArcsShape::createUseInheritedColorAttr()
{
	createNumericAttr(
		useInheritedColor, "useInheritedColor","uic", MFnNumericData::kBoolean, USE_INHERITED_COLOR,
		atInput,
		false, false, 0.0,
		false, false, 0.0 );
}

void motionArcsShape::createInheritedOverrideColorAttr()
{
	createNumericAttr(
		inheritedOverrideColor, "inheritedOverrideColor","ioc", MFnNumericData::kInt, INHERITED_OVERRIDE_COLOR,
		atInput,
		false, false, 0.0,
		false, false, 0.0 );
}

void motionArcsShape::createColorAttr()
{
	MString attrName = "color";
	MString shortName = "clr";
	int flags = atInput;

	displayStringD( (attrName + "_b").asChar() );

	MStatus				stat;
	MFnNumericAttribute	numAttr;
	color = numAttr.createColor( attrName, shortName, &stat );
	if ( MS::kSuccess != stat )
	{
		displayStringError( (MString( MString( "error while creating " ) + attrName + " attribute") + stat.errorString()).asChar() );
		throw MString( MString( attrName + " attribute exception: ").asChar() );
	}
	
	displayStringD( (attrName + "_m").asChar() );
	::addAttribute( color, flags );

	displayStringD( (attrName + "_e").asChar() );
}

void motionArcsShape::createAllFramesAttr()
{
	createNumericAttr(
		allFrames, "allFrames","alf", MFnNumericData::kBoolean, SHOW_ALL_TRAJECTORY,
		atInput,
		false, false, 0.0,
		false, false, 0.0 );
}
// ------------------------------------------------------------------------------------

void motionArcsShape::createTypedAttr( 
	MObject& object, const MString& attrName, const MString& shortName, 
	const MFnData::Type& type, const MObject& defaultValue, int flags )
{
	displayStringD( (attrName + "_b").asChar() );

	MStatus				stat;
	MFnTypedAttribute	typedAttr;
	object = typedAttr.create( attrName, shortName, type, defaultValue, &stat );
	if ( MS::kSuccess != stat )
	{
		displayStringError( (MString( MString( "error while creating " ) + attrName + " attribute") + stat.errorString()).asChar() );
		throw MString( MString( attrName + " attribute exception: ").asChar() );
	}
	
	displayStringD( (attrName + "_m").asChar() );
	::addAttribute( object, flags );

	displayStringD( (attrName + "_e").asChar() );
}

void motionArcsShape::createNumericAttr( 
		MObject& object, const MString& attrName, const MString& shortName, 
		const MFnNumericData::Type& type, double defaultValue, int flags, 
		bool useMin, bool useSoftMin, double minValue, 
		bool useMax, bool useSoftMax, double maxValue )
{
	displayStringD( (attrName + "_b").asChar() );

	MStatus				stat;
	MFnNumericAttribute	numAttr;
	object = numAttr.create( attrName, shortName, type, defaultValue, &stat );
	if ( MS::kSuccess != stat )
	{
		displayStringError( (MString( MString( "error while creating " ) + attrName + " attribute") + stat.errorString()).asChar() );
		throw MString( MString( attrName + " attribute exception: ").asChar() );
	}
	if ( useMin )
		numAttr.setMin( minValue );
	if ( useSoftMin )
		numAttr.setSoftMin( minValue );

	if ( useMax )
		numAttr.setMax( maxValue );
	if ( useSoftMax )
		numAttr.setSoftMax( maxValue );
	
	displayStringD( (attrName + "_m").asChar() );
	::addAttribute( object, flags );

	displayStringD( (attrName + "_e").asChar() );
}