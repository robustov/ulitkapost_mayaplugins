set BIN_DIR=\\server\bin
set BIN_DIR=.\out\motionArcs

if "%DSTPATH%"=="" (set BIN_DIR=%BIN_DIR%) else (set BIN_DIR=%DSTPATH%)

rem SN
mkdir %BIN_DIR%
mkdir %BIN_DIR%\bin
mkdir %BIN_DIR%\mel
rem mkdir %BIN_DIR%\slim
rem mkdir %BIN_DIR%\docs
rem mkdir %BIN_DIR%\python

copy binrelease85\motionArcs.mll			%BIN_DIR%\bin

copy mel\AEmotionArcsShapeTemplate.mel		%BIN_DIR%\mel
copy mel\motionArcsMenu.mel					%BIN_DIR%\mel

if "%DSTPATH%"=="" (pause) else (set BIN_DIR=%BIN_DIR%)

