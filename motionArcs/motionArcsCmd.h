#ifndef _motionArcsCmd
#define _motionArcsCmd
//
// Copyright (C) a.mashuta
// 
// File: motionArcsCmd.h
//
// MEL Command: motionArcs
//
// Author: Maya Plug-in Wizard 2.0
//

#include <maya/MPxCommand.h>
#include <maya/MDgModifier.h>
#include <maya/MDagModifier.h>
#include <maya/MDagPath.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnTransform.h>

class MArgList;

class motionArcs : public MPxCommand
{

public:
				motionArcs();
	virtual		~motionArcs();

	MStatus		doIt( const MArgList& );
	MStatus		redoIt();
	MStatus		undoIt();
	bool		isUndoable() const;

	static		void* creator();
	static		MSyntax newSyntax();

private:
	MDGModifier		dgMod;
	MDagModifier	dagMod;
	MStatus			createMotionArcsShapeFor( const MDagPath& dagPath );
	MStatus			configureNodeForRigthUpdating();
	MStatus			createSnapshotNode( MFnDependencyNode& snapshotNode );
	MStatus			connectObjectToSnapshotNode( const MFnTransform& objectTransformFn, const MFnDependencyNode& snapshotNode );
	MStatus			createMotionArcsShapeNode( MFnDependencyNode& maShapeNode );
};

#endif