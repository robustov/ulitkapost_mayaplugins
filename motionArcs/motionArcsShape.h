// Copyright (C) 
// File: motionArcsShape.h
// Node: motionArcsShape

#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>
#include <string>
 
class motionArcsShape : public MPxLocatorNode {
public:
	static		 MTypeId	id;
	static const MString	typeName;
	
public:
	motionArcsShape();
	virtual ~motionArcsShape(); 

	virtual MStatus			compute( 
		const MPlug& plug, 
		MDataBlock& data );

	virtual void			draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool			isBounded() const;
	virtual MBoundingBox	boundingBox() const; 

	static void*	creator();
	static MStatus	initialize();

public:
	static MObject frames;
	static MObject points;
	static MObject usePointsSize;
	static MObject pointIncrement;
	static MObject pointsSize;
	static MObject widthMultiplier;
	static MObject colorMultiplier;
	static MObject coordinates;
	static MObject update;
	static MObject startFrame;
	static MObject curFrame;
	static MObject endFrame;
	static MObject aroundFrame;
	static MObject freezeFrame;
	static MObject useInheritedColor;
	static MObject inheritedOverrideColor;
	static MObject allFrames;
	static MObject color;

private:
	static void createTypedAttr(
		MObject& object, const MString& attrName, const MString& shortName, 
		const MFnData::Type& type, const MObject& defaultValue, int flags );
		
	static void createNumericAttr(
		MObject& object, const MString& attrName, const MString& shortName, 
		const MFnNumericData::Type& type, double defaultValue, int flags, 
		bool useMin, bool useSoftMin, double minValue, 
		bool useMax, bool useSoftMax, double maxValue );
	
	template<typename T>
	static void createUnitAttr( 
		MObject& object, const MString& attrName, const MString& shortName, 
		const MFnUnitAttribute::Type& type, double defaultValue, int flags, 
		bool useMin, bool useSoftMin, const T& minValue, 
		bool useMax, bool useSoftMax, const T& maxValue );

	static void createFramesAttr();
	static void createPointsAttr();
	static void createUsePointsSizeAttr();
	static void createPointIncrementAttr();
	static void createPointsSizeAttr();
	static void createWidthMultiplierAttr();
	static void createColorMultiplierAttr();
	static void createCoordinatesAttr();
	static void createUpdateAttr();
	static void createStartFrameAttr();
	static void createCurFrameAttr();
	static void createEndFrameAttr();
	static void createAroundFrameAttr();
	static void createFreezeFrameAttr();
	static void createUseInheritedColorAttr();
	static void createInheritedOverrideColorAttr();
	static void createAllFramesAttr();
	static void createColorAttr();

private:
	static const double		WIDTH_MULTIPLIER;
	static const double		COLOR_MULTIPLIER;
	static const bool		USE_POINTS_SIZE;
	static const double		POINT_INCREMENT;
	static const double		POINTS_SIZE;
	static const bool		SHOW_ALL_TRAJECTORY;
	static const int		UPDATE;
	static const double		START_FRAME;
	static const double		CUR_FRAME;
	static const double		END_FRAME;
	static const int		AROUND_FRAME;
	static const bool		FREEZE_FRAME;
	static const bool		USE_INHERITED_COLOR;
	static const int		INHERITED_OVERRIDE_COLOR;
	
	static const MString	internalErrorMessage;
		
private:
	float		getWidthMultiplier() const;
	double		getColorMultiplier() const;
	float		getPointsSize() const;
	bool		needToUseInheritedColor() const;
	MColor		getColor() const;
	MColor		linesColor( M3dView& view ) const;
	double		getStartFrame() const;
	double		getEndFrame() const;
	double		getCurrentFrame() const;
	int			getAroundFrame() const;
	bool		showAllFrames() const;
	bool		needToUsePointsSize() const;
	float		getPointIncrement() const;
	bool		freezedFrame() const;

	void		getPathPoints( MPointArray& pts ) const;
	void		leaveAroundFramePoints( MPointArray& pts ) const;
	void		getPathPointsFromPointsAttr( MPointArray& pts ) const;
	void		getPathPointsFromCoordinatesAttr( MPointArray& pts ) const;
	void		getNextPoint( MPlug& coorPlug, MTime& time, MPoint& pt ) const;
	unsigned int getArcStartIndex() const;
	unsigned int getArcEndIndex( unsigned int length ) const;
	
	double		getFPS() const;
	double		framesBetween( const MTime& startTime, const MTime& endTime ) const;

	void		drawPoints( const MPointArray& pts, float size ) const;
	void		drawPoint( const MPoint& pt, float size ) const;
	void		drawPoint( const MPoint& pt, unsigned int index, unsigned int arcStart, unsigned int arcEnd, float multiplier, float increment ) const;
	float		getPointSizeByIndex( unsigned int index, unsigned int arcStart, unsigned int arcEnd, float multiplier ) const;

	void		drawLines( const MColor& color, const MPointArray& pts, float widthMultiplier, double colorMultiplier ) const;
	float		getWidthByIndex( unsigned int index, unsigned int arcStart, unsigned int arcEnd, float widthMultiplier) const;
	double		gaussian( double x, double a, double b, double c ) const;
};

template<typename T>
static void motionArcsShape::createUnitAttr( 
	MObject& object, const MString& attrName, const MString& shortName, 
	const MFnUnitAttribute::Type& type, double defaultValue, int flags, 
	bool useMin, bool useSoftMin, const T& minValue, 
	bool useMax, bool useSoftMax, const T& maxValue )
{
	displayStringD( (attrName + "_b").asChar() );

	MStatus				stat;
	MFnUnitAttribute	unitAttr;
	object = unitAttr.create( attrName, shortName, type, defaultValue, &stat );
	if ( MS::kSuccess != stat )
	{
		displayStringError( (MString( MString( "error while creating " ) + attrName + " attribute") + stat.errorString()).asChar() );
		throw MString( MString( attrName + " attribute exception: ").asChar() );
	}
	if ( useMin )
		unitAttr.setMin( minValue );
	if ( useSoftMin )
		unitAttr.setSoftMin( minValue );

	if ( useMax )
		unitAttr.setMax( maxValue );
	if ( useSoftMax )
		unitAttr.setSoftMax( maxValue );
	
	displayStringD( (attrName + "_m").asChar() );
	::addAttribute( object, flags );

	displayStringD( (attrName + "_e").asChar() );
}