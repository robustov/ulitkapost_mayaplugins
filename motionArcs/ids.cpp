#include "pre.h"

const unsigned TypeIdBase = 0xDA050;

// node static ids and names

#include "motionArcsShape.h"
MTypeId motionArcsShape::id( TypeIdBase+00 );
const MString motionArcsShape::typeName( "motionArcsShape" );