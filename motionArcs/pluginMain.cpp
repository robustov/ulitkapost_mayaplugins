//
// Copyright (C) a.mashuta
// 
// File: pluginMain.cpp
//
// Author: Maya Plug-in Wizard 2.0
//

#include "motionArcsCmd.h"
#include "motionArcsShape.h"

#include <maya/MFnPlugin.h>

MStatus uninitializePlugin( MObject obj);

MStatus initializePlugin( MObject obj )
//
//	Description:
//		this method is called when the plug-in is loaded into Maya.  It 
//		registers all of the services that this plug-in provides with 
//		Maya.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{ 
	MStatus   status = MS::kSuccess;
	MFnPlugin plugin( obj, "a.mashuta", "2008", "Any");

	status = plugin.registerNode( 
		motionArcsShape::typeName, 
		motionArcsShape::id, 
		motionArcsShape::creator,
		motionArcsShape::initialize, 
		MPxNode::kLocatorNode );
	if (!status) 
	{
		displayString("cant register %s", motionArcsShape::typeName.asChar());
		status.perror("registerNode");
		uninitializePlugin(obj);
		return status;
	}

	if( !MGlobal::sourceFile("motionArcsMenu.mel"))
	{
		displayString("error source AEmyCmdTemplate.mel");
	}
	else
	{
		if( !MGlobal::executeCommand("motionArcsDebugMenu()"))
		{
			displayString("Dont found motionArcsDebugMenu() command");
		}
		MGlobal::executeCommand("motionArcs_RegistryPlugin()");
	}

	status = plugin.registerCommand( "motionArcs", motionArcs::creator );
	if (!status) {
		status.perror("registerCommand");
		return status;
	}

	return status;
}

MStatus uninitializePlugin( MObject obj )
//
//	Description:
//		this method is called when the plug-in is unloaded from Maya. It 
//		deregisters all of the services that it was providing.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{
	MStatus   status = MS::kSuccess;
	MFnPlugin plugin( obj );

	status = plugin.deregisterCommand( "motionArcs" );
	if (!status) {
		status.perror("deregisterCommand");
		return status;
	}
	
	status = plugin.deregisterNode( motionArcsShape::id );
	if (!status) displayString("cant deregister %s", motionArcsShape::typeName.asChar());

	MGlobal::executeCommand("motionArcsDebugMenuDelete()");

	return status;
}
