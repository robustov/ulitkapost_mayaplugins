#pragma once

#include <cmath>
#include <maya/MPoint.h>

inline long double distance( const MPoint& p1, const MPoint& p2 )
{
	long double sum = 0;
	sum += (p1.x - p2.x) * (p1.x - p2.x);
	sum += (p1.y - p2.y) * (p1.y - p2.y);
	sum += (p1.z - p2.z) * (p1.z - p2.z);
	
	return pow( sum, (long double) 0.5 );
}