#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>
#include "FFDeformModiferData.h"
 
class FFDeformModifer : public MPxLocatorNode
{
public:
	FFDeformModifer();
	virtual ~FFDeformModifer(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	static void* creator();
	static MStatus initialize();

	MStatus Build_Control( const MPlug& plug, MDataBlock& data );

public:
	static MObject i_type;
	static MObject i_radius;
	static MObject i_affect;
	static MObject o_modifer;

public:
	static MTypeId id;
	static const MString typeName;
};


