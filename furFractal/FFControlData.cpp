//
// Copyright (C) 
// File: FFControlDataCmd.cpp
// MEL Command: FFControlData

#include "FFControlData.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include "FFGridData.h"
#include "ocellaris/renderMemImpl.h"
#include <sstream>




FFControlData::FFControlData()
{
#ifdef DLLPROCEDURE_DUMP
printf("FFControlData %x\n", (int)this);
#endif 
	control = NULL;
}
FFControlData::~FFControlData()
{
#ifdef DLLPROCEDURE_DUMP
printf("~FFControlData %x\n", (int)this);
#endif 

	if( control)
		control->release();
	control = 0;
	dllproc.Free();
}

MTypeId FFControlData::typeId() const
{
	return FFControlData::id;
}

MString FFControlData::name() const
{ 
	return FFControlData::typeName; 
}

void* FFControlData::creator()
{
	return new FFControlData();
}


void FFControlData::copy( const MPxData& other )
{
	const FFControlData* arg = (const FFControlData*)&other;

	DATACOPY(dllproc);
	if( dllproc.isValid())
	{
		IFFCONTROL_CREATOR pc = (IFFCONTROL_CREATOR)dllproc.proc;
		control = (*pc)();
		control->copy(arg->control);
	}

//	DATACOPY(bWindow)
//	DATACOPY(worldpos)
//	DATACOPY(invworldpos)
//	DATACOPY(windowSize)
//	DATACOPY(bStart)

//	DATACOPY(bGrow)
//	DATACOPY(growUpPropencity)
//	DATACOPY(growPropencity)
//	DATACOPY(growLowPropencity)
//	DATACOPY(maxLevel)
}

MStatus FFControlData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("FFControlData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus FFControlData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus FFControlData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus FFControlData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void FFControlData::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version==0)
	{
		if(stream.isLoading())
		{
			if( control)
				control->release();
			control = 0;
		}
		stream >> dllproc.dll;
		stream >> dllproc.entrypoint;
		if(stream.isLoading())
		{
			if(dllproc.dll=="furFractal.mll")
				dllproc.dll = "furFractalNeoMath.dll";
			dllproc.Load();
			if( dllproc.isValid())
			{
				IFFCONTROL_CREATOR pc = (IFFCONTROL_CREATOR)dllproc.proc;
				control = (*pc)();
			}
		}
		if(control)
			control->serialize(stream);
	}
	// ���� �� ������������!!!
	if( version==1)
	{
		if(!stream.isLoading())
		{
			cls::renderMemImpl memrender;
			memrender.openForWrite();
			if(control)
			{
				memrender.Parameter( "xxx::entrypoint", this->dllproc.GetFormated().c_str());

				control->SerializeOcs("xxx", &memrender, true);
				stream >> memrender.getWriteBuffer();
			}
		}
		else
		{
			std::vector<char> buffer;
			stream >> buffer;
			cls::renderMemImpl memrender;
			memrender.openForRead(&buffer[0], (int)buffer.size());

			cls::renderCacheImpl<> cache;
			memrender.Render(&cache);

			std::string dllform;
			cache.GetParameter("xxx::entrypoint", dllform);

			this->dllproc.LoadFormated(dllform.c_str());
			if( dllproc.isValid())
			{
				IFFCONTROL_CREATOR pc = (IFFCONTROL_CREATOR)dllproc.proc;
				control = (*pc)();
				control->SerializeOcs("xxx", &cache, false);
			}
		}
	}
}

