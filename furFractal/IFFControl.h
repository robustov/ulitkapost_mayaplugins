#pragma once

#include <vector>

#include "Util/Stream.h"
#include "Util\STLStream.h"
#include "Math/Grid.h"
#include "IFFWindow.h"
#include "IFFGrow.h"
#include "IFFDeform.h"
#include "ocellaris/IRender.h"
#include "IOcsData.h"

#include "Util/DllProcedure.h"

struct IFFControl //: public IFFWindow, public IFFGrow, public Math::IGridDeformer
{
	int controlid;

	std::vector<Util::DllProcedure> growdlls;
	std::vector<IFFGrow*> grows;

	std::vector<Util::DllProcedure> deformdlls;
	std::vector<IFFDeform*> deforms;

public:
	
	virtual void getGrows(std::vector<IFFGrow*>& grows)
	{
		grows.insert(grows.end(), this->grows.begin(), this->grows.end());
	}
	virtual void getDeformers(std::vector<IFFDeform*>& deforms)
	{
		for(int i=0; i<(int)this->deforms.size(); i++)
		{
			this->deforms[i]->control = this;
		}
		deforms.insert(deforms.end(), this->deforms.begin(), this->deforms.end());
	}
	// ���������� ���������� �� ����������
	static bool sortDeformersPred(const IFFDeform* arg1, const IFFDeform* arg2)
	{
		int p1 = arg1->getPriority();
		int p2 = arg2->getPriority();

		return p1>p2;
	}
	// ���������� ���������� �� ����������
	static void sortDeformers(std::vector<IFFDeform*>& deforms)
	{
		std::sort(deforms.begin(), deforms.end(), sortDeformersPred);
	}

public:
	virtual std::string Type()
	{
		return "";
	}
	// �������������� ���������
	virtual void BindControls(GridData& griddata, std::vector<IFFControl*>& controls)
	{
	}
	// ��������� �� ��������
	virtual void draw(GridData& griddata)
	{
	}

	virtual void copy(
		const IFFControl* arg
		)
	{
		int i=0;
		this->controlid = arg->controlid;

		this->growdlls = arg->growdlls;
		this->grows.resize(this->growdlls.size());
		for(i=0; i<(int)this->growdlls.size(); i++)
		{
			Util::DllProcedure& dllproc = this->growdlls[i];
			if( dllproc.isValid())
			{
				IFFGROW_CREATOR pc = (IFFGROW_CREATOR)dllproc.proc;
				this->grows[i] = (*pc)();
				if(this->grows[i])
				{
					this->grows[i]->copy(arg->grows[i]);
				}
			}
		}

		/*/
		this->deformdlls = arg->deformdlls;
		this->deforms.resize(this->deformdlls.size());
		for(i=0; i<(int)this->deformdlls.size(); i++)
		{
			Util::DllProcedure& dllproc = this->deformdlls[i];
			if( dllproc.isValid())
			{
				IFFDEFORM_CREATOR pc = (IFFDEFORM_CREATOR)dllproc.proc;
				this->deforms[i] = (*pc)();
				if(this->deforms[i])
				{
					this->deforms[i]->copy(arg->deforms[i]);
				}
			}
		}
		/*/
	}
	virtual void release()
	{
#ifdef DLLPROCEDURE_DUMP
printf("release %x\n", (int)this);
#endif 
		delete this;
	};

	// 
	virtual void serialize(Util::Stream& stream);

	virtual void SerializeOcs(
		const char* _name, 
		cls::IRender* render, 
		bool bSave
		);

	// ������ ��������� �� �����
	virtual void ReadAttributes(
		IOcsData* node
		)
	{
	};


public:
	IFFControl()
	{
		controlid = 0;
	}
	virtual ~IFFControl()
	{
		releaseAll();
	}
	void ReinitGrowId()
	{
		void* p = this;
		this->controlid = *(int*)(&p);
	}
	void releaseAll()
	{
		int i;
		for(i=0; i<(int)this->grows.size(); i++)
		{
//			if( this->grows[i]) 
//				this->grows[i]->release();
			this->grows[i] = 0;
		}
		this->grows.clear();
		this->growdlls.clear();

		for(i=0; i<(int)this->deforms.size(); i++)
		{
//			if( this->deforms[i]) 
//				this->deforms[i]->release();
			this->deforms[i] = 0;
		}
		this->deforms.clear();
		this->deformdlls.clear();
	}
};
typedef IFFControl* (*IFFCONTROL_CREATOR)();

inline void IFFControl::SerializeOcs(
	const char* _name, 
	cls::IRender* render, 
	bool bSave
	)
{
	std::string name = _name;

	SERIALIZEOCS(controlid);

	/*/
	if( bSave) 
	{
		{
			std::vector<std::string> grownames;
			grownames.reserve(growdlls.size());
			for(int c=0; c<(int)growdlls.size(); c++)
			{
				char buf[24];
				sprintf(buf, "%sgrow%02d", _name, c);
				if( !this->grows[c]) continue;

				Util::DllProcedure& dll = growdlls[c];
				if(!dll.isValid())
					continue;

				render->Parameter( (std::string(buf)+"::entrypoint").c_str(), dll.GetFormated().c_str());
				this->grows[c]->SerializeOcs(buf, render, true);
				grownames.push_back(buf);
			}
			render->Parameter((name+"::grownames").c_str(), cls::PSA(grownames));
		}
	}
	else
	{
		cls::PSA grownames;
		render->GetParameter( (name+"::grownames").c_str(), grownames);
		this->growdlls.clear();
		this->grows.clear();
		this->growdlls.reserve(grownames.size());
		this->grows.reserve(grownames.size());
		for(int i=0; i<grownames.size(); i++)
		{
			std::string growname = grownames[i];
			std::string dll;
			render->GetParameter( (growname+"::entrypoint").c_str(), dll);

			Util::DllProcedure dllproc;
			dllproc.LoadFormated(dll.c_str(), "furFractalNeoMath.dll");
			if( !dllproc.isValid())
				continue;

			IFFGROW_CREATOR pc = (IFFGROW_CREATOR)dllproc.proc;
			IFFGrow* grow = (*pc)();
			if( !grow) 
				continue;
			grow->SerializeOcs(growname.c_str(), render, false);

			this->growdlls.push_back(dllproc);
			this->grows.push_back(grow);
		}

	}

	// deformers
	if( bSave) 
	{
		{
			std::vector<std::string> deformnames;
			deformnames.reserve(deformdlls.size());
			for(int c=0; c<(int)deformdlls.size(); c++)
			{
				char buf[24];
				sprintf(buf, "%sdeform%02d", _name, c);
				if( !this->deforms[c]) continue;

				Util::DllProcedure& dll = deformdlls[c];
				if(!dll.isValid())
					continue;

				render->Parameter( (std::string(buf)+"::entrypoint").c_str(), dll.GetFormated().c_str());
				this->deforms[c]->SerializeOcs(buf, render, true);
				deformnames.push_back(buf);
			}
			render->Parameter((name+"::deformnames").c_str(), cls::PSA(deformnames));
		}
	}
	else
	{
		cls::PSA deformnames;
		render->GetParameter( (name+"::deformnames").c_str(), deformnames);
		this->deformdlls.clear();
		this->deforms.clear();
		this->deformdlls.reserve(deformnames.size());
		this->deforms.reserve(deformnames.size());
		for(int i=0; i<deformnames.size(); i++)
		{
			std::string deformname = deformnames[i];
			std::string dll;
			render->GetParameter( (deformname+"::entrypoint").c_str(), dll);

			Util::DllProcedure dllproc;
			dllproc.LoadFormated(dll.c_str(), "furFractalNeoMath.dll");
			if( !dllproc.isValid())
				continue;

			IFFDEFORM_CREATOR pc = (IFFDEFORM_CREATOR)dllproc.proc;
			IFFDeform* deform = (*pc)();
			if( !deform) 
				continue;
			deform->SerializeOcs(deformname.c_str(), render, false);
			deform->control = this;

			this->deformdlls.push_back(dllproc);
			this->deforms.push_back(deform);
		}

	}
	/*/

}

inline void IFFControl::serialize(Util::Stream& stream)
{
	int version = 1;
	stream >> version;
	if( version>=0)
	{
		// LOAD
		int i;
		if(stream.isLoading())
		{
			releaseAll();

			SERIALIZE(growdlls);
			this->grows.resize(this->growdlls.size(), NULL);
			for(i=0; i<(int)this->growdlls.size(); i++)
			{
				Util::DllProcedure& dllproc = this->growdlls[i];
				if( dllproc.isValid())
				{
					IFFGROW_CREATOR pc = (IFFGROW_CREATOR)dllproc.proc;
					this->grows[i] = (*pc)();
					if(this->grows[i])
					{
						this->grows[i]->serialize(stream);
					}
				}
			}

			SERIALIZE(deformdlls);
			this->deforms.resize(this->deformdlls.size(), NULL);
			for(i=0; i<(int)this->deformdlls.size(); i++)
			{
				Util::DllProcedure& dllproc = this->deformdlls[i];
				if( dllproc.isValid())
				{
					IFFDEFORM_CREATOR pc = (IFFDEFORM_CREATOR)dllproc.proc;
					this->deforms[i] = (*pc)();
					if(this->deforms[i])
					{
						this->deforms[i]->serialize(stream);
					}
				}
			}

		}
		else
		{
			// SAVE
			SERIALIZE(growdlls);
			for(i=0; i<(int)this->grows.size(); i++)
			{
				if(this->grows[i])
					this->grows[i]->serialize(stream);
			}

			SERIALIZE(deformdlls);
			for(i=0; i<(int)this->deforms.size(); i++)
			{
				if(this->deforms[i])
					this->deforms[i]->serialize(stream);
			}
		}
	}
	if(version>=1)
	{
		SERIALIZE(controlid);
	}
}

