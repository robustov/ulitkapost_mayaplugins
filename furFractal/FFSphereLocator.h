#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>

#include "FFControlData.h"
#include "Math/SphereControl.h"

 
class FFSphereLocator : public MPxNode
{
public:
	FFSphereLocator();
	virtual ~FFSphereLocator(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 
	MStatus	connectionMade(
		const MPlug& plug,
		const MPlug& otherPlug,
		bool asSrc );

	static void* creator();
	static MStatus initialize();

public:
	MStatus Build_Control(
		const MPlug& plug, MDataBlock& data);

public:
	static MObject i_worldMatrix;

	static MObject i_window;
		static MObject i_windowSize;		// �� 0 �� 1
		static MObject i_disgrowfactor;
		static MObject i_width0, i_widthExp;
		static MObject i_venToper;
	
	
	static MObject i_grow;
		static MObject i_bStart;
		static MObject i_growUpPropencity;
		static MObject i_growPropencity;
		static MObject i_growLowPropencity;
		static MObject i_startLevel;
		static MObject i_maxLevel;
		static MObject i_seed;
		static MObject maxGrow;
		static MObject maxGrowLow;
		static MObject sameFluctuation;
		static MObject samePhaseBiasConst;
		static MObject samePhaseBiasRnd;
		static MObject samePhaseSecondaryFactor;

	static MObject i_deformer;
		static MObject i_time;
		static MObject i_xynoisemagnitude;
		static MObject i_xynoisefrequency;
		static MObject i_xynoisedimention;
		static MObject deformToper;

	// render
	static MObject texToper;

	static MObject o_control;

public:
	static MTypeId id;
	static const MString typeName;
};


