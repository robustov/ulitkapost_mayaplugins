//
// Copyright (C) 
// File: FFGridDataCmd.cpp
// MEL Command: FFGridData

#include "FFGridData.h"

#include <maya/MGlobal.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MAnimControl.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include "MathNgl/MathNgl.h"
#include "MathNmaya/MathNmaya.h"
#include "Util/FileStream.h"
#include <sstream>
#include "Util/misc_create_directory.h"
#include "IFFControl.h"

// ����� �� ������ ����
Math::GridCellId getGetLowestCellPos(Math::Grid& grid, Math::GridCellId id)
{
return id;
/*/
	Math::GridCellId lastid = id;
	for(;;)
	{
		id = Math::Grid::getLowCell(id);
		if(!grid.getCell(id))
			return lastid;
		lastid = id;
	}
/*/
}
bool subdivEdge( Math::Grid& grid, const Math::GridEdge& edge, std::vector<Math::GridCellId>& cells)
{
	if( !grid.getCell(edge.v1()) ||
		!grid.getCell(edge.v2()))
	{
		return false;
	}


	Math::GridCellId v = Math::Grid::getLowCell(edge.v);
	Math::GridEdge edge1(v, (Math::enGridNeigbour3d)edge.nb);
	Math::GridEdge edge2(edge1.v2(), (Math::enGridNeigbour3d)edge.nb);

	if( !subdivEdge( grid, edge1, cells))
	{
		Math::GridCellId v = getGetLowestCellPos(grid, edge.v1());
		cells.push_back(v);
	}

	if( !subdivEdge( grid, edge2, cells))
	{
		Math::GridCellId v = getGetLowestCellPos(grid, edge.v2());
		cells.push_back( v);
	}
	return true;
}

bool subdivEdge( Math::Grid& grid, const Math::GridFreeEdge& edge, std::vector<Math::GridCellId>& cells)
{
	if( !grid.getCell(edge.v1()) ||
		!grid.getCell(edge.v2()))
	{
		return false;
	}


	Math::GridCellId v1 = Math::Grid::getLowCell( edge.v1());
	Math::GridCellId v2 = Math::Grid::getLowCell( edge.v2());
	Math::GridCellId vmid( 
		v1.level,
		(v1.x+v2.x)/2,
		(v1.y+v2.y)/2,
		(v1.z+v2.z)/2);

	Math::GridFreeEdge edge1(v1, vmid);
	Math::GridFreeEdge edge2(vmid, v2);

	if( !subdivEdge( grid, edge1, cells))
	{
		Math::GridCellId v = getGetLowestCellPos(grid, edge.v1());
		cells.push_back(v);
	}

	if( !subdivEdge( grid, edge2, cells))
	{
		Math::GridCellId v = getGetLowestCellPos(grid, edge.v2());
		cells.push_back( v);
	}
	return true;
}

void FFGridData::init(
	int seed, 
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	std::vector<IFFControl*> allcontrols, 
	std::vector<IFFGrow*>& grows, 
	std::vector<IFFControl*>& controls,	// ��������������� grows
	float dim
	)
{
	GridData* pThis = &this->data;
	pThis->clear();
	pThis->grid.clear();
	pThis->lastStep = 0;
	std::list<Math::IFFEdgeData> added;
	pThis->currentseed = seed;

	pThis->grid.dimention0 = dim;
	pThis->setUpTransform(ffworldmatrix);

	/*/
	MTime currenttime = MAnimControl::currentTime();

	MTime startTime = MAnimControl::animationStartTime();
	MAnimControl::setCurrentTime( startTime);
	/*/

	try
	{
		for( int g=0; g<(int)allcontrols.size(); g++)
		{
			IFFControl* control = allcontrols[g];
			control->BindControls(*pThis, allcontrols);

			std::vector<IFFDeform*> deforms;
			control->getDeformers(deforms);

			for( int d=0; d<(int)deforms.size(); d++)
			{
				deforms[d]->init(*pThis);
			}
		}

		srand( pThis->currentseed);
		for( int g=0; g<(int)grows.size(); g++)
		{
			IFFControl* control = controls[g];
			grows[g]->Start(allcontrols, grows, control, *pThis);
		}
	}
	catch(...)
	{
		displayStringError("Exception in FFGridData::init");
	}

	/*/
	MAnimControl::setCurrentTime( currenttime);
	/*/

	/*/
	// �������� �����
	{
		std::list<Math::IFFEdgeData>::iterator it = added.begin();
//		for( int i=0; i<(int)added.size(); i++)
		for( ;it != added.end(); it++)
		{
			Math::IFFEdgeData& grow = *it;
			GridData::Edge& ed = grow.edge;
	//		ed.dump(stdout);
//			pThis->busies.insert(ed.v1());
//			pThis->busies.insert(ed.v2());
			GridData::EdgeData& data = pThis->alives[ed];
//			data.bGrowDirection = true;
//			data.iffdata = grow;

		}
	}
	/*/
	pThis->currentseed = rand();
}

bool FFGridData::Step(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	std::vector<IFFControl*> allcontrols, 
	std::vector<IFFGrow*>& grows, 
	std::vector<IFFControl*>& controls,	// ��������������� grows
	float randomOffset
	)
{
	GridData* pThis = &this->data;
	srand( pThis->currentseed);

	int place;
	try
	{
		place = 0;

		bool bHave = false;
		std::map<int, std::list<int> > sortedlist;
		for( int g=0; g<(int)grows.size(); g++)
		{
			
			IFFControl* control = controls[g];
			IFFGrow* grow = grows[g];
			int p = grow->PriorityNextThread(control, *pThis);
			bHave = bHave || (p>-10000000);
			p = -p;
			sortedlist[p].push_back(g);

		}
		if( !bHave)
			return false;

		std::map<int, std::list<int> >::iterator it = sortedlist.begin();
		for(;it != sortedlist.end(); it++)
		{
			std::list<int>& samelist = it->second;
			std::list<int>::iterator itl = samelist.begin();
			for(;itl != samelist.end(); itl++)
			{
				int g = *itl;

				IFFControl* control = controls[g];
				Math::IFFThreadData newthread;
				newthread.threadId = pThis->threads.size();
				place = 1;
				if( grows[g]->GrowNextThread(allcontrols, grows, control, *pThis, newthread))
				{
					newthread.generation = 0;
					place = 2;
					if(newthread.parentThreadId>=0)
					{
						Math::IFFThreadData& parent = pThis->threads[newthread.parentThreadId];
						newthread.generation = parent.generation + 1;
					}
					place = 3;
					newthread.controlId = grows[g]->controlId;
					if( pThis->addThread(newthread))
					{
						place = 4;
						if(newthread.bStartFromThis)
						{
							for( int g2=0; g2<(int)grows.size(); g2++)
							{
								place = 5;
								grows[g2]->OnNewThread(
									allcontrols, grows, controls[g2], *pThis, newthread		// ��� ���� �������
									);
							}
						}
					}
					break;
				}
			}
		}

		pThis->currentseed = rand();
	}
	catch(...)
	{
		displayStringError("Exception in FFGridData::Step place = %d", place);
		printf("Exception in FFGridData::Step place = %d\n", place);
	}
	fflush(stdout);
	return true;
}

// ����� ���������
void FFGridData::GrowStop(
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	std::vector<IFFGrow*>& grows,
	std::vector<IFFControl*>& controls	// ��������������� grows
	)
{
	GridData* pThis = &this->data;
	std::list<Math::IFFEdgeData> added;
	for( int g=0; g<(int)grows.size(); g++)
	{
		grows[g]->GrowStop(controls[g], *pThis);
	}
//	pThis->buildCurves();
	pThis->buildChains();
}

/*/
float assembleTexCoord(
	std::vector<IFFGrow*>& ffds, 
	Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
	GridData& griddata,					// �������
	const Math::IFFEdgeData& edge,			// �������� �����
	float pointonedge,					// ����� �� ����� (�� 0 �� 1)
	int texCoordType					// �� ������� ���� ��������� UV ����� ���� ������
	)
{
	float u = 0;
	for(int f=0; f<(int)ffds.size(); f++)
	{
		float texc=0;
		if( ffds[f]->getTexCoord(texc, griddata, edge, pointonedge, texCoordType))
			u = texc;
	}
	return u;
}
/*/

void FFGridData::draw(
	std::vector<IFFDeform*>& deforms,
	std::vector<Math::IFFWindow*>& ffds, 
	Math::Matrix4f& worldm, 
	float widthScaleValue,
	bool bViewGridDots, 
	bool bColor
	)
{
	/*/
	GridData* pThis = &this->data;
	pThis->grid.timestamp++;
	pThis->grid.setUpDeformers(worldm, &deforms[0], (int)deforms.size());

	int stat_x=0, stat_y=0, stat_z=0;

	static std::vector<Math::Vec3f> invisiblepoints;
	invisiblepoints.clear();
	if( bViewGridDots)
		invisiblepoints.reserve(10000);

	float curcolor[4];
	glGetFloatv( GL_CURRENT_COLOR, curcolor );

	std::map<GridData::Edge, GridData::EdgeData>::iterator it = pThis->alives.begin();
	for(;it != pThis->alives.end(); it++)
	{
		const GridData::Edge& edge = it->first;
		const GridData::EdgeData& edgedata = it->second;

		Math::Vec3f v1 = pThis->grid.getCellPos( pThis->grid.addCell(edge.v1()));
		Math::Vec3f v2 = pThis->grid.getCellPos( pThis->grid.addCell(edge.v2()));

		float srcw = 4.f/(edge.level()+1);
		srcw = srcw*(float)widthScaleValue;
		float w = 0;
		for(int f=0; f<(int)ffds.size(); f++)
		{
 			float pointonedge = 0.5f;
			float cw = ffds[f]->processEdge(worldm, *pThis, edgedata.iffdata, pointonedge, srcw);
			if( cw<=0) continue;

			w = __max(w, cw);
		}

		if(w<0.01f)
		{
			Math::Vec3f cavg = (v1+v2)*0.5f;
//			cavg = worldm*cavg;

			if( bViewGridDots)
				invisiblepoints.push_back(cavg);
			continue;
		}

		glLineWidth(w);

		glBegin(GL_LINE_STRIP);
		std::vector<Math::GridCellId> cells;
		subdivEdge( pThis->grid, edge, cells);
		for(int i=0; i<(int)cells.size(); i++)
		{
			Math::GridCellId id = cells[i];
			Math::GridCell* cell = pThis->grid.getCell(id);
			if( !cell) break;
			Math::Vec3f pos = pThis->grid.getCellPos(*cell);
			glVertex3f(pos);

			if(bColor)
			{
				float pointOnedge = i/(float)(cells.size()-1);
				float uv = assembleTexCoord(
					ffds, worldm, *pThis, edgedata.iffdata, pointOnedge, 0);
				glColor3f(uv, uv, uv);
			}
		}
		glEnd();
	}
	glColor3f( curcolor[0], curcolor[1], curcolor[2]);

	pThis->grid.setUpDeformers(Math::Matrix4f(1), NULL, 0);

	if( bViewGridDots)
		drawPoints(&invisiblepoints[0], (int)invisiblepoints.size(), 1);
	/*/
}
void FFGridData::drawVertexes(int viewGridLevel, std::vector<IFFDeform*>& deforms, Math::Matrix4f& worldm, Math::Vec3f viewpoint )
{
	GridData* pThis = &this->data;
	pThis->grid.timestamp++;
	pThis->grid.setUpDeformers(pThis, worldm, (Math::IGridDeformer**)&deforms[0], deforms.size());

	Math::Grid::Level& lev = pThis->grid.levels[viewGridLevel];

	if(viewpoint!=Math::Vec3f(0, 0, 0))
	{
		Math::GridCellId centerid;
		centerid.level = viewGridLevel;
		centerid.x = (int)(floor(viewpoint.x));
		centerid.y = (int)(floor(viewpoint.y));
		centerid.z = (int)(floor(viewpoint.z));

		int _min = 0, _max = 1;
		for(int x=_min; x<=_max; x++)
		{
			for(int y=_min; y<=_max; y++)
			{
				for(int z=_min; z<=_max; z++)
				{
					Math::GridCellId cur = centerid;
					cur += Math::Vec3i(x, y, z);
					Math::Vec3f pos1 = pThis->grid.getCellPos(cur);;
					for(int i=0; i<3; i++)
					{
						Math::enGridNeigbour3d nei = (Math::enGridNeigbour3d)(i<<1);
						Math::GridCellId necell = pThis->grid.getNeigbour(cur, nei);

						glBegin(GL_LINES);
						Math::Vec3f pos2 = pThis->grid.getCellPos(necell);
						glVertex3f(pos1);
						glVertex3f(pos2);
						glEnd();
					}
				}
			}
		}
		
	}
	else
	{
		std::map<Math::GridCellId, Math::GridCell>::iterator it = lev.cells.begin();
		for(;it != lev.cells.end(); it++)
		{
			Math::GridCell& cell = it->second;

			Math::Vec3f pos1 = pThis->grid.getCellPos(cell);
			for(int i=0; i<3; i++)
			{
				Math::enGridNeigbour3d nei = (Math::enGridNeigbour3d)(i<<1);
				const Math::GridCellId& id2 = pThis->grid.getNeigbour(it->first, nei);
				Math::Vec3f pos2 = pThis->grid.getCellPos(id2);

				glBegin(GL_LINES);
				glVertex3f(pos1);
				glVertex3f(pos2);
				glEnd();
			}
		}
	}
	pThis->grid.setUpDeformers(NULL, Math::Matrix4f(1),NULL, 0);
}










FFGridData::FFGridData()
{
//	initseed(0);
	this->bModify = false;
}
FFGridData::~FFGridData()
{
}

MTypeId FFGridData::typeId() const
{
	return FFGridData::id;
}

MString FFGridData::name() const
{ 
	return FFGridData::typeName; 
}

void* FFGridData::creator()
{
	return new FFGridData();
}

void FFGridData::copy( const MPxData& other )
{
	const FFGridData* arg = (const FFGridData*)&other;
	data.copy(&arg->data);
	this->bModify = true;
}

MStatus FFGridData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("FFGridData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus FFGridData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus FFGridData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus FFGridData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void FFGridData::serialize(Util::Stream& stream)
{
	int version = 1;
	stream >> version;
	if( version>=1)
	{
		if(stream.isLoading())
		{
			// ��������� �� ����� - ��� �������� � �����
			stream >> this->loadedFilename;

			Util::FileStream file;
			if( !file.open(this->loadedFilename.c_str(), false))
			{
				displayStringError("cant open file %s", this->loadedFilename.c_str());
				return;
			}
			this->serializeData(file);
		}
		// �������� ������ ���� ���� ����������� ��� ���������� ��� �����
		else 
		{
			if( this->loadedFilename != this->filename)
				this->bModify = true;
			else
				if( !Util::is_file_exist(this->loadedFilename.c_str()))
					this->bModify = true;

			if(this->bModify)
			{
				Util::FileStream file;
				Util::create_directory_for_file(filename.c_str());
				if( !file.open(this->filename.c_str(), true))
				{
					displayStringError("cant save to file %s", this->filename.c_str());
					return;
				}
				this->serializeData(file);
			}
			stream >> this->filename;
			this->loadedFilename = this->filename;
		}
	}

//	data.serialize(stream);
	// �������� ����
	this->bModify = false;

}

void FFGridData::serializeData(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		this->data.serialize(stream);
	}
}
