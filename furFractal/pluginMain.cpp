#include "pre.h"
#include <maya/MFnPlugin.h>
#include "furFractal.h"
#include "FFSphereLocator.h"
#include "FFGridData.h"
#include "ocellaris/ocellarisExport.h"
#include "FFCurveNeoLocator.h"
#include "FFControlData.h"
#include "FFGrowData.h"
#include "FFDeformData.h"
#include "FFMeshLocator.h"
#include "FFMeshGrowFromHair.h"
#include "FFGrowLocator.h"
#include "FFDeformLocator.h"
#include "FFDrawCache.h"
#include "FFDeformModifer.h"
#include "FFDeformModiferData.h"

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
fprintf(stderr, "loadPlugin furFractalNeo\n");
fflush(stderr);

	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

// #include "FFDrawCache.h"
	stat = plugin.registerData( 
		FFDrawCache::typeName, 
		FFDrawCache::id, 
		FFDrawCache::creator);
	if (!stat) 
	{
		displayString("cant register %s", FFDrawCache::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "FFDeformModiferData.h"
	stat = plugin.registerData( 
		FFDeformModiferData::typeName, 
		FFDeformModiferData::id, 
		FFDeformModiferData::creator);
	if (!stat) 
	{
		displayString("cant register %s", FFDeformModiferData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "FFDeformModifer.h"
	stat = plugin.registerNode( 
		FFDeformModifer::typeName, 
		FFDeformModifer::id, 
		FFDeformModifer::creator,
		FFDeformModifer::initialize, 
		MPxNode::kLocatorNode );
	if (!stat) 
	{
		displayString("cant register %s", FFDeformModifer::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "FFGridData.h"
	stat = plugin.registerData( 
		FFGridData::typeName, 
		FFGridData::id, 
		FFGridData::creator);
	if (!stat) 
	{
		displayString("cant register %s", FFGridData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "FFGrowData.h"
	stat = plugin.registerData( 
		FFGrowData::typeName, 
		FFGrowData::id, 
		FFGrowData::creator);
	if (!stat) 
	{
		displayString("cant register %s", FFGrowData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
// #include "FFDeformData.h"
	stat = plugin.registerData( 
		FFDeformData::typeName, 
		FFDeformData::id, 
		FFDeformData::creator);
	if (!stat) 
	{
		displayString("cant register %s", FFDeformData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "FFControlData.h"
	stat = plugin.registerData( 
		FFControlData::typeName, 
		FFControlData::id, 
		FFControlData::creator);
	if (!stat) 
	{
		displayString("cant register %s", FFControlData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "FFMeshGrowFromHair.h"
	stat = plugin.registerNode( 
		FFMeshGrowFromHair::typeName, 
		FFMeshGrowFromHair::id, 
		FFMeshGrowFromHair::creator,
		FFMeshGrowFromHair::initialize );
	if (!stat) 
	{
		displayString("cant register %s", FFMeshGrowFromHair::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
// #include "FFGrowLocator.h"
	stat = plugin.registerNode( 
		FFGrowLocator::typeName, 
		FFGrowLocator::id, 
		FFGrowLocator::creator,
		FFGrowLocator::initialize );
	if (!stat) 
	{
		displayString("cant register %s", FFGrowLocator::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
// #include "FFDeformLocator.h"
	stat = plugin.registerNode( 
		FFDeformLocator::typeName, 
		FFDeformLocator::id, 
		FFDeformLocator::creator,
		FFDeformLocator::initialize );
	if (!stat) 
	{
		displayString("cant register %s", FFDeformLocator::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "FFMeshLocator.h"
	stat = plugin.registerNode( 
		FFMeshLocator::typeName, 
		FFMeshLocator::id, 
		FFMeshLocator::creator,
		FFMeshLocator::initialize );
	if (!stat) 
	{
		displayString("cant register %s", FFMeshLocator::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}


// #include "FFCurveNeoLocator.h"
	stat = plugin.registerNode( 
		FFCurveNeoLocator::typeName, 
		FFCurveNeoLocator::id, 
		FFCurveNeoLocator::creator,
		FFCurveNeoLocator::initialize, 
//		MPxNode::kLocatorNode 
		MPxNode::kDependNode
		);
	if (!stat) 
	{
		displayString("cant register %s", FFCurveNeoLocator::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	/*/
// #include "FFSphereLocator.h"
	stat = plugin.registerNode( 
		FFSphereLocator::typeName, 
		FFSphereLocator::id, 
		FFSphereLocator::creator,
		FFSphereLocator::initialize, 
//		MPxNode::kLocatorNode 
		MPxNode::kDependNode
		);
	if (!stat) 
	{
		displayString("cant register %s", FFSphereLocator::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	/*/

// #include "furFractal.h"
	stat = plugin.registerNode( 
		furFractal::typeName, 
		furFractal::id, 
		furFractal::creator,
		furFractal::initialize, 
		MPxNode::kLocatorNode );
	if (!stat) 
	{
		displayString("cant register %s", furFractal::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	registerGenerator( furFractal::id, &furFractal::generator);


//	if( !MGlobal::sourceFile("furFractalMenu.mel"))
//	{
//		displayString("error source AEmyCmdTemplate.mel");
//	}
//	else
	{
//		if( !MGlobal::executeCommand("furFractalDebugMenu()"))
//		{
//			displayString("Dont found furFractalDebugMenu() command");
//		}
		MGlobal::executeCommand("furFractalNeo_RegistryPlugin()");
	}
	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterData( FFDrawCache::id );
	if (!stat) displayString("cant deregister %s", FFDrawCache::typeName.asChar());

	stat = plugin.deregisterData( FFDeformModiferData::id );
	if (!stat) displayString("cant deregister %s", FFDeformModiferData::typeName.asChar());

	stat = plugin.deregisterNode( FFDeformModifer::id );
	if (!stat) displayString("cant deregister %s", FFDeformModifer::typeName.asChar());

	stat = plugin.deregisterData( FFGridData::id );
	if (!stat) displayString("cant deregister %s", FFGridData::typeName.asChar());

	stat = plugin.deregisterData( FFGrowData::id );
	if (!stat) displayString("cant deregister %s", FFGrowData::typeName.asChar());

	stat = plugin.deregisterData( FFDeformData::id );
	if (!stat) displayString("cant deregister %s", FFDeformData::typeName.asChar());

	stat = plugin.deregisterData( FFControlData::id );
	if (!stat) displayString("cant deregister %s", FFControlData::typeName.asChar());

	stat = plugin.deregisterNode( FFMeshGrowFromHair::id );
	if (!stat) displayString("cant deregister %s", FFMeshGrowFromHair::typeName.asChar());

	stat = plugin.deregisterNode( FFGrowLocator::id );
	if (!stat) displayString("cant deregister %s", FFGrowLocator::typeName.asChar());

	stat = plugin.deregisterNode( FFDeformLocator::id );
	if (!stat) displayString("cant deregister %s", FFDeformLocator::typeName.asChar());

	stat = plugin.deregisterNode( FFMeshLocator::id );
	if (!stat) displayString("cant deregister %s", FFMeshLocator::typeName.asChar());

	stat = plugin.deregisterNode( FFCurveNeoLocator::id );
	if (!stat) displayString("cant deregister %s", FFCurveNeoLocator::typeName.asChar());

	unregisterGenerator( furFractal::id, &furFractal::generator);
	stat = plugin.deregisterNode( furFractal::id );
	if (!stat) displayString("cant deregister %s", furFractal::typeName.asChar());

//	stat = plugin.deregisterNode( FFSphereLocator::id );
//	if (!stat) displayString("cant deregister %s", FFSphereLocator::typeName.asChar());





	MGlobal::executeCommand("furFractalNeoDebugMenuDelete()");
	return stat;
}
