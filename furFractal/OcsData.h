#pragma once

#include "ocellaris/OcellarisExport.h"
#include "loadTexture.h"
#include <maya/MFnAnimCurve.h>

struct OcsData : public IOcsData
{
	MObject obj;
public:
	OcsData( MObject& obj)
	{
		this->obj = obj;
	}

	virtual cls::Param getAttribute(
		const char* name, cls::Param& defvalue)
	{
		ocExport_AddAttrType( this->obj, name, defvalue->type, defvalue);
		return ocExport_GetAttrValue( this->obj, name);
	};
	virtual void setAttribute(
		const char* name, 
		cls::Param& value)
	{
		ocExport_AddAttrType( this->obj, name, value->type, value);
		ocExport_SetAttrValue( this->obj, name, value);
	};

	virtual bool getTextureAttribute(
		const char* name, 
		float defvalue, 
		Math::matrixMxN<float>& dst, 
		int res)
	{
		MStatus stat;
		MFnDependencyNode dn(obj);
		MObject attr = dn.attribute(name, &stat);
		if( !stat)
		{
			MFnNumericData::Type dtype = MFnNumericData::kDouble;
			MFnNumericAttribute numAttr;
			attr = numAttr.create(name, name, dtype, 0, &stat);
			numAttr.setConnectable(true);
			numAttr.setStorable(true);
			numAttr.setWritable(true);
			numAttr.setReadable(true);
			numAttr.setHidden(false);
			numAttr.setKeyable(true);
			dn.addAttribute(attr);

			MPlug plug(obj, attr);
			plug.setDouble((double)defvalue);

			dst.init(1, 1);
			dst[0][0] = defvalue;
			return true;
		}
		MPlug plug(obj, attr);
		if( !loadTexture(plug, dst, res))
		{
			double val = defvalue;
			plug.getValue(val);
			dst.init(1, 1);
			dst[0][0] = (float)val;
		}
		return true;
	};
	virtual bool getAnimCurve(
		const char* name, 
		Math::AnimCurve<float>& dst, 
		int res)
	{
		float _defvalue = 0;
		if( !dst.data.empty())
			_defvalue = dst.data[0];
		cls::P<float> defvalue(_defvalue);
		ocExport_AddAttrType( this->obj, name, defvalue->type, cls::P<float>(defvalue));

		dst.clear();

		MStatus stat;
		MFnDependencyNode dn(obj);
		MObject attr = dn.attribute(name, &stat);
		if( !stat)
		{
			dst.data.push_back(_defvalue);
			return true;
		}

		MPlug src(obj, attr);
		double val;
		src.getValue(val);
		dst.data.push_back((float)val);

		MPlugArray plar;
		src.connectedTo(plar, true, false);
		if( plar.length()!=1)
			return false;

		MPlug plug = plar[0];

		if( !plug.node().hasFn( MFn::kAnimCurve))
			return false;

		MFnAnimCurve curve(plug.node());
		if( !dst.set(curve, res))
			return false;
		return true;
	}

};
