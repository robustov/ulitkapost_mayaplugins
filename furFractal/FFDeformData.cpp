//
// Copyright (C) 
// File: FFDeformDataCmd.cpp
// MEL Command: FFDeformData

#include "FFDeformData.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include "FFGridData.h"
#include "ocellaris/renderMemImpl.h"
#include <sstream>




FFDeformData::FFDeformData()
{
#ifdef DLLPROCEDURE_DUMP
printf("FFDeformData %x\n", (int)this);
#endif 

	control = NULL;
}
FFDeformData::~FFDeformData()
{
#ifdef DLLPROCEDURE_DUMP
printf("~FFDeformData %x\n", (int)this);
#endif 
	if( control)
		control->release();
	control = 0;
	dllproc.Free();
}

MTypeId FFDeformData::typeId() const
{
	return FFDeformData::id;
}

MString FFDeformData::name() const
{ 
	return FFDeformData::typeName; 
}

void* FFDeformData::creator()
{
	return new FFDeformData();
}


void FFDeformData::copy( const MPxData& other )
{
	const FFDeformData* arg = (const FFDeformData*)&other;

	DATACOPY(dllproc);
	DATACOPY(dllprocname);
	if( dllproc.isValid())
	{
		IFFDEFORM_CREATOR pc = (IFFDEFORM_CREATOR)dllproc.proc;
		control = (*pc)();
//		control->copy(arg->control);
	}
}

MStatus FFDeformData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("FFDeformData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus FFDeformData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus FFDeformData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus FFDeformData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void FFDeformData::serialize(Util::Stream& stream)
{
	int version = 1;
	stream >> version;
	if( version>=0)
	{
		if(stream.isLoading())
		{
			if( control)
				control->release();
			control = 0;
		}
		stream >> dllproc.dll;
		stream >> dllproc.entrypoint;
		if(stream.isLoading())
		{
			if( dllproc.dll == "furFractal.mll")
				dllproc.dll = "furFractalNeoMath.dll";
			dllproc.Load();
			if( dllproc.isValid())
			{
				IFFDEFORM_CREATOR pc = (IFFDEFORM_CREATOR)dllproc.proc;
				control = (*pc)();
			}
		}
		if(control)
			control->serialize(stream);
	}
	if( version>=1)
	{
		stream >> dllprocname;
	}
}

