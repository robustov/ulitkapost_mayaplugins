#pragma once

#include "Math/Grid.h"
#include "IFFEdgeData.h"
#include "IFFThreadData.h"
#include "IFFVertexData.h"
#include "IOcsData.h"
#include "IFFDeformModifer.h"
class GridData;

struct IFFControl;

// ��������� ��� ���������� 
struct IFFDeform : public Math::IGridDeformer
{
	void* control;
	bool bEnable;

	std::vector<IFFDeformModifer> modifers;
	bool invertModifers;

	std::vector<int> srcGrows;		// ������ ����������� �� ������� �����
	bool invertSrcGrows;			// ������������� ������ ����������� 

public:
	// 
	virtual void clear(){};

	virtual void release(
		)
	{
		delete this;
	}

	/*/
	// �������� �������� ����� ��� �����
	virtual void velocity(
		GridData& griddata,					// �������
		const IFFVertexData& edge,				// �������� �����
		float& velocity
		){};
	/*/
	
public:
	IFFDeform()
	{
		control = 0;
		deformid = 0;
		bEnable = true;
		this->invertModifers = false;
		invertSrcGrows = false;
	}
	virtual int getPriority(
		)const
	{
		return 100;
	}
	// �������������
	virtual void init(
		GridData& griddata
		)
	{
	}
	// �������������� box
	virtual void UpdateDirtyBox(
		Math::Box3f& box
		)
	{
	}

	virtual void AddVerts(
		GridData& griddata, 
		std::vector< Math::GridCellId>& verts, 
		float* weigths=NULL, 
		bool bDump=false
		)
	{
	}

	/*/
	virtual void copy(const IFFDeform* _arg)
	{
		const IFFDeform* arg = (const IFFDeform*)_arg;
		DATACOPY(deformid);
		control = 0;
	}
	/*/

	virtual void serialize(Util::Stream& stream)
	{
		int version = 3;
		stream >> version;
		if( version>=0)
		{
			SERIALIZE(deformid);
			SERIALIZE(bEnable);
		}
		if( version>=1)
		{
			SERIALIZE(modifers);
		}
		if( version>=2)
		{
			SERIALIZE(invertModifers);
		}
		if( version>=3)
		{
			SERIALIZE(srcGrows);
			SERIALIZE(invertSrcGrows);

		}
	}
	virtual void SerializeOcs(
		const char* _name, 
		cls::IRender* render, 
		bool bSave
		)
	{
		std::string name = _name;
		SERIALIZEOCS(deformid);
		SERIALIZEOCS(bEnable);

		for(int i=0; i<(int)modifers.size(); i++)
		{
			char buf[256];
			sprintf(buf, "%s::modifer%d", _name, i);
			modifers[i].SerializeOcs(buf, render, bSave);
		}
		SERIALIZEOCS(invertModifers);
	}
	// ������ ��������� �� �����
	virtual void ReadAttributes(
		IOcsData* node
		)
	{	
		READMAYABOOL(bEnable);
		READMAYABOOL(invertModifers);
		READMAYABOOL(invertSrcGrows);
	};

public:
	void ReinitGrowId()
	{
		void* p = this;
		this->deformid = *(int*)(&p);
	}
	float getDeformersFactor(const Math::Grid& grid, const Math::Vec3f& v)
	{
		float factor = 1;
		Math::Vec3f ws = grid.worldMatrix*v;
		for(int m=0; m<(int)this->modifers.size(); m++)
		{
			IFFDeformModifer& modifer = this->modifers[m];
			float f = modifer.getFactor(ws);
			factor = __min( factor, f);
		}
		if( this->invertModifers)
			factor = 1-factor;
		return factor;
	}
	// ����� �� ������������� � ����� grow
	bool isValidForDeform(
		int growId
		)
	{
		if(srcGrows.empty())
			return true;
		bool bFind = false;
		for(int i=0; i<(int)srcGrows.size(); i++)
		{
			if(srcGrows[i]==growId)
				bFind = true;
		}
		if(!this->invertSrcGrows)
			return bFind;
		else
			return !bFind;
	}

};
typedef IFFDeform* (*IFFDEFORM_CREATOR)();
