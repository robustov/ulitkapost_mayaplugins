#pragma once
#include "pre.h"
#include "FFDeformLocator.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFloatMatrix.h>
#include <maya/MRenderUtil.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>

#include <maya/MGlobal.h>
#include "mathNmaya/mathNmaya.h"

#include "FFDeformModiferData.h"

bool loadTexture(
	const MPlug& src, 
	Math::matrixMxN<float>& dst, 
	int res
	);

// render
//	MObject FFDeformLocator::texToper;
//	MObject FFDeformLocator::shaderId;
MObject FFDeformLocator::entrypoint;
MObject FFDeformLocator::startPositions, FFDeformLocator::startNormals;
MObject FFDeformLocator::i_modifers;
MObject FFDeformLocator::i_sourceGrows;

MObject FFDeformLocator::o_deform;
MObject FFDeformLocator::o_deformId;

FFDeformLocator::FFDeformLocator()
{
}
FFDeformLocator::~FFDeformLocator()
{
}
MStatus FFDeformLocator::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	if( plug == o_deform )
	{
		return Build_Control(plug, data);
	}
	return MS::kUnknownParameter;
}
void FFDeformLocator::LoadModifers( 
	MDataBlock& data, 
	std::vector<IFFDeformModifer>& modifers
	)
{
	MStatus stat;
	MArrayDataHandle inputDataCache = data.inputArrayValue(i_modifers, &stat);
	modifers.clear();
	modifers.reserve( inputDataCache.elementCount());
	for( unsigned i=0; i<inputDataCache.elementCount(); i++)
	{
//		if( inputDataCache.jumpToElement(i))
		if( inputDataCache.jumpToArrayElement(i))
		{
			MPxData* dataCache = inputDataCache.inputValue(&stat).asPluginData();
			FFDeformModiferData* ffwd = (FFDeformModiferData*)dataCache;
			if( !ffwd) 
				continue;

			modifers.push_back(ffwd->modifer);
		}
	}
}
void FFDeformLocator::LoadSource( 
	MDataBlock& data, 
	std::vector<int>& srcGrows
	)
{
	MStatus stat;
	MArrayDataHandle inputDataCache = data.inputArrayValue(i_sourceGrows, &stat);
	srcGrows.clear();
	srcGrows.reserve( inputDataCache.elementCount());
	for( unsigned i=0; i<inputDataCache.elementCount(); i++)
	{
//		if( inputDataCache.jumpToElement(i))
		if( inputDataCache.jumpToArrayElement(i))
		{
			int sg = inputDataCache.inputValue(&stat).asInt();
			srcGrows.push_back(sg);
		}
	}
}

MStatus FFDeformLocator::setDependentsDirty( const MPlug& plug, MPlugArray& plugArray)
{
	if( plug.isDynamic())
	{
		plugArray.append( MPlug(thisMObject(), o_deform));
	}
	return MPxNode::setDependentsDirty( plug, plugArray);
}

MStatus FFDeformLocator::Build_Control( const MPlug& plug, MDataBlock& data )
{
//	displayStringD( "FFDeformLocator::Build_Control");
	MStatus stat;

	MDataHandle outputData = data.outputValue(plug, &stat );

	typedef FFDeformData CLASS;
	CLASS* pData = NULL;

//	bool bNewData = false;
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<CLASS*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( CLASS::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<CLASS*>(fnDataCreator.data( &stat ));
//		bNewData = true;

		outputData.set(pData);
		MDataHandle outputData = data.outputValue(plug, &stat );
		userdata = outputData.asPluginData();
		pData = static_cast<CLASS*>(userdata);
	}

	std::string dllprocname = data.inputValue(entrypoint).asString().asChar();

	int id = 0;
	if( pData->control)
	{
		IFFDeform* cc = (IFFDeform*)pData->control;
		id = cc->deformid;
	}
	if( pData->dllprocname != dllprocname)
	{
		pData->dllproc.Free();
	}
	if( !pData->dllproc.isValid())
	{
		pData->dllproc.LoadFormated(dllprocname.c_str(), "furFractalNeoMath.dll");
		pData->dllprocname = dllprocname;
		pData->control = 0;
	}
	if( pData->dllproc.isValid())
	{
		if( !pData->control)
		{
			IFFDEFORM_CREATOR pc = (IFFDEFORM_CREATOR)pData->dllproc.proc;
			pData->control = (*pc)();
			IFFDeform* cc = (IFFDeform*)pData->control;
			if(pData->control)
				cc->deformid = id;
		}
		if(pData->control)
		{
			IFFDeform* cc = (IFFDeform*)pData->control;
			if( cc->deformid==0)
				cc->ReinitGrowId();

			LoadModifers(data, cc->modifers);
			LoadSource( data, cc->srcGrows);

//			printf( "deformid=%x\n", cc->deformid);
			fflush(stdout);

			/*/
			cc->texResolution = data.inputValue(texResolution).asInt();
			if( !loadTexture(
				MPlug(thisMObject(), this->growSpeed), 
				cc->growSpeed, 
				cc->texResolution
				))
			{
				cc->growSpeed.init(1, 1);
				cc->growSpeed[0][0] = (float)data.inputValue(growSpeed).asDouble();
			}

			{
				MObject arobj = data.inputValue(startPositions).data();
				MFnPointArrayData pa(arobj);
				cc->startPositions.resize(pa.length());
				for(int i=0; i<(int)cc->startPositions.size(); i++)
					::copy(cc->startPositions[i], pa[i]);
			}
			{
				MObject arobj = data.inputValue(startNormals).data();
				MFnVectorArrayData pa(arobj);
				cc->startDirection.resize(pa.length());
				for(int i=0; i<(int)cc->startDirection.size(); i++)
					::copy(cc->startDirection[i], pa[i]);
			}
			/*/

			// render
//			cc->texToper = (float)data.inputValue(texToper).asDouble();

			OcsData ocsData(thisMObject());
			cc->ReadAttributes(&ocsData);

			MDataHandle o_deformIddata = data.outputValue(o_deformId);
			o_deformIddata.setInt(cc->deformid);
			o_deformIddata.setClean();
		}
	}
//	if( bNewData)
//		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

void* FFDeformLocator::creator()
{
	return new FFDeformLocator();
}

MStatus FFDeformLocator::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		MObject grow[1];
		{
			int i=0;

			grow[i] = numAttr.create ("time","iti", MFnNumericData::kDouble, 0, &stat);
			::addAttribute(grow[i++], atInput);

			/*/
			grow[i] = numAttr.create ("growlevel","gle", MFnNumericData::kInt, 1, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(4);
			::addAttribute(grow[i++], atInput);

			grow[i] = numAttr.create ("seed","se", MFnNumericData::kInt, 0, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1000);
			::addAttribute(grow[i++], atInput);

			grow[i] = numAttr.create ("startVenCount","svc", MFnNumericData::kInt, 4, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(grow[i++], atInput);

			grow[i] = numAttr.create ("growlength","sle", MFnNumericData::kDouble, 10.0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(grow[i++], atInput);

			grow[i] = numAttr.create ("wigth","slej", MFnNumericData::kDouble, 1.0f, &stat);
			numAttr.setMin(0);
			numAttr.setMax(1);
			::addAttribute(grow[i++], atInput);
			/*/

//			startPositions = typedAttr.create( "startPositions", "spos", MFnData::kPointArray, &stat);
//			::addAttribute(startPositions, atInput|atHidden);

//			startNormals = typedAttr.create( "startDirections", "sdir", MFnData::kVectorArray, &stat);
//			::addAttribute(startNormals, atInput|atHidden);
		}

		{
			entrypoint = typedAttr.create( "entrypoint", "enpt", MFnData::kString, &stat);
			::addAttribute(entrypoint, atInput);
		}
		{
			i_sourceGrows = numAttr.create( "sourceGrows", "srgr", MFnNumericData::kInt, 0, &stat);
			numAttr.setReadable(false);
			numAttr.setArray(true);
			numAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			numAttr.setIndexMatters(false);
			::addAttribute( i_sourceGrows, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atArray );
		}

		{
			i_modifers = typedAttr.create( "modifers", "mods", FFDeformModiferData::id);
			typedAttr.setReadable(false);
			typedAttr.setArray(true);
			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			typedAttr.setIndexMatters(false);
			::addAttribute(i_modifers, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray );
		}

		{
			o_deform = typedAttr.create( "deform", "dfrm", FFDeformData::id);
			::addAttribute(o_deform, atReadable|atWritable|atStorable|atCached|atConnectable|atHidden);

			int s, i;
			s = sizeof( grow)/sizeof( MObject);
			for(i=0; i<s; i++)
				attributeAffects(grow[i], o_deform);

			attributeAffects(entrypoint,		o_deform);
			attributeAffects(i_modifers,		o_deform);
			attributeAffects(i_sourceGrows,		o_deform);
//			attributeAffects(startNormals,		o_deform);
//			attributeAffects(startPositions,	o_deform);
		}
		{
			o_deformId = numAttr.create ("deformId","deid", MFnNumericData::kInt, 0, &stat);
			::addAttribute(o_deformId, atReadable|atWritable|atConnectable|atStorable|atUnHidden);
		}

		if( !MGlobal::sourceFile("AEFFDeformLocatorTemplate.mel"))
		{
			displayString("error source AEFFDeformLocatorTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

