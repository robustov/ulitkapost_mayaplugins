#pragma once

#include "Math/Grid.h"
#include "IFFEdgeData.h"
#include "IFFThreadData.h"
class GridData;

namespace Math
{
	// ��������� ����
	// �������� ��������/�����
	// �������� ������ ���������/�����
	struct IFFWindow
	{
	public:
		// ���������� ����� �� ������� ������ ������� �����
		virtual float getThreadLength(
			Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
			Math::Matrix4f& ffworldmatrixinv,	// �������������� world ������� �������� 
			GridData& griddata,					// �������
			const IFFThreadData& edge				// �������� �����
			);
		// ���������� �������� ����� ��� �����
		// �������� ������������� � ���������� ����� ������� �� ������ ��������� getThreadLength
		virtual float getVelocity(
			Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
			Math::Matrix4f& ffworldmatrixinv,	// �������������� world ������� �������� 
			GridData& griddata,					// �������
			const IFFEdgeData& edge				// �������� �����
			);
		// ���������� ������ ����� �� ���������� �� ��� ����� � �� ���������� �� ������
		virtual float getWidth(
			Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
			Math::Matrix4f& ffworldmatrixinv,	// �������������� world ������� �������� 
			GridData& griddata,					// �������
			...
			);
		// ���������� �����.���������� ��� ����� �� �����
		virtual bool getTexCoord(
			float& texCoord, 
			Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
			Math::Matrix4f& ffworldmatrixinv,	// �������������� world ������� �������� 
			GridData& griddata,					// �������
			const IFFEdgeData& edge,			// �������� �����
			float pointonedge,					// ����� �� ����� (�� 0 �� 1)
			int texCoordType					// �� ������� ���� ��������� UV ����� ���� ������
			){return false;};
		// ��� ��������� �� ��������
		virtual void draw(
			Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
			GridData& griddata
			){};


		// ???
		// ���������� ������� ��� �������� � thread (����� � ������ ����������)
		virtual Math::Vec3f getNormal(
			Math::Matrix4f& ffworldmatrix,		// world ������� �������� 
			Math::Matrix4f& ffworldmatrixinv,	// �������������� world ������� �������� 
			GridData& griddata,					// �������
			...
			);

		// ???
		// ����� ��������� ����� ��� ������������ ����� �����
		// ��� ����?
		virtual bool getMainPoint(
			const Math::GridCellId& v1,	//����������� ����� v1->v2
			const Math::GridCellId& v2,
			const std::list<Math::GridCellId>& v3list,	// ��������� ����� ���. ������� �� ������� v2
			GridData& griddata,
			Math::GridCellId& mainPoint	//��������� �����, ���� ��������� true
			)=0; 
	};
}