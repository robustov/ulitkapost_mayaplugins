//
// Copyright (C) 
// File: FFCurveNeoLocatorCmd.cpp
// MEL Command: FFCurveNeoLocator

#include "FFCurveNeoLocator.h"

#include <maya/MGlobal.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MRampAttribute.h>
#include "MathNgl/MathNgl.h"
#include "MathNmaya/MathNmaya.h"
#include "FFGridData.h"
#include "FFGrowData.h"
#include "FFDeformData.h"
#include "Math/perlinnoise.h"

MObject FFCurveNeoLocator::i_curve;
MObject FFCurveNeoLocator::i_curveTesselation;
MObject FFCurveNeoLocator::i_curveInvert;
MObject FFCurveNeoLocator::i_deforms;
MObject FFCurveNeoLocator::i_grows;
MObject FFCurveNeoLocator::i_length;



/*/
MObject FFCurveNeoLocator::i_width;

MObject FFCurveNeoLocator::i_window;
	MObject FFCurveNeoLocator::i_windowposition;
	MObject FFCurveNeoLocator::i_time;
	MObject FFCurveNeoLocator::i_noisemagnitude;
	MObject FFCurveNeoLocator::i_noisefrequency;
	MObject FFCurveNeoLocator::i_noisedimention;
	MObject FFCurveNeoLocator::i_disgrowfactor;
	MObject FFCurveNeoLocator::i_debugShowVessel;
	MObject FFCurveNeoLocator::i_venToper;
	MObject FFCurveNeoLocator::i_SubGrowWidthFactor;
	MObject FFCurveNeoLocator::i_WidthFactorJitter;

MObject FFCurveNeoLocator::i_grow;
	MObject FFCurveNeoLocator::i_level;
	MObject FFCurveNeoLocator::i_bInvertDirection;
	MObject FFCurveNeoLocator::i_vessels;
	MObject FFCurveNeoLocator::i_maxdist;
	MObject FFCurveNeoLocator::maxdistJitter;
	MObject FFCurveNeoLocator::i_maxDistRamp;
//	MObject FFCurveNeoLocator::i_trueway_propencity;
	MObject FFCurveNeoLocator::i_growfrequency;
	MObject FFCurveNeoLocator::i_subGrowPropencity;
	MObject FFCurveNeoLocator::i_subGrowPhaseOffset;
	MObject FFCurveNeoLocator::i_SubGrowHistoryOffset;
	MObject FFCurveNeoLocator::i_seed;

MObject FFCurveNeoLocator::i_deformer;
	MObject FFCurveNeoLocator::i_xynoisemagnitude;
	MObject FFCurveNeoLocator::i_xynoisemagnitudeSubGrow;
	MObject FFCurveNeoLocator::i_xynoisefrequency;
	MObject FFCurveNeoLocator::i_xynoisedimention;
	MObject FFCurveNeoLocator::deformToper;

// render
	MObject FFCurveNeoLocator::texToper;
	MObject FFCurveNeoLocator::shaderId;
/*/

MObject FFCurveNeoLocator::o_control;


bool FFCurveNeoLocator::ReadCurve(
	CurveData& pThis, 
	MFnNurbsCurve& curve,
	int tess,
	bool curveInvert
	)
{
	MStatus stat;

	double startKnot, endKnot;
	curve.getKnotDomain(startKnot, endKnot);

	MPoint _pt1, _pt2;
	curve.getPointAtParam(0, _pt1, MSpace::kWorld);
	curve.getPointAtParam(0.001, _pt2, MSpace::kWorld);
	Math::Vec3f tangent;
	::copy(tangent, _pt2-_pt1);
	tangent.normalize();

	Math::Vec3f normal = -Math::cross(tangent, Math::Vec3f(1.34566f, 1, 0.3333f));
	normal.normalize();

	int vertcount = tess+1;

	pThis.fulllength = 0;
	pThis.verts.resize(vertcount);
	pThis.vertnormals.resize(vertcount);
	pThis.verttangents.resize(vertcount);
	for(int i=0; i<vertcount; i++)
	{
		double knot = i/((double)tess);
		knot = (endKnot-startKnot)*knot + startKnot;

		Math::Vec3f pt;

		MPoint _pt;
		curve.getPointAtParam(knot, _pt, MSpace::kWorld);
		::copy(pt, _pt);

		if(i!=0)
		{
			tangent = pt - pThis.verts[i-1];
			pThis.fulllength += tangent.length();
			tangent.normalize();
			normal = Math::cross(tangent, normal);
			normal = -Math::cross(tangent, normal);
			normal.normalize();
		}

		pThis.verts[i] = pt;
		pThis.vertnormals[i] = normal;
		pThis.verttangents[i] = tangent;
	}

	if( curveInvert)
	{
		// 
		std::reverse(pThis.verts.begin(), pThis.verts.end());
		std::reverse(pThis.vertnormals.begin(), pThis.vertnormals.end());
		std::reverse(pThis.verttangents.begin(), pThis.verttangents.end());
		for(int i=0; i<vertcount; i++)
		{
			pThis.vertnormals[i] = -pThis.vertnormals[i];
			pThis.verttangents[i] = -pThis.verttangents[i];
		}
	}

	/*/
	// ������� �� �������� 0.25
	double deltalen = 0.25;

	double fulllen = curve.length();
	int segcount = (int)(fulllen/deltalen);
	int vertcount = segcount+1;

	// ��������� �������
	MPoint _pt1, _pt2;
	curve.getPointAtParam(0, _pt1, MSpace::kWorld);
	double p = curve.findParamFromLength(deltalen);
	curve.getPointAtParam(p, _pt2, MSpace::kWorld);
	Math::Vec3f tangent;
	::copy(tangent, _pt2-_pt1);
	tangent.normalize();
	
	Math::Vec3f normal = -Math::cross(tangent, Math::Vec3f(1.34566f, 1, 0.3333f));
	normal.normalize();

	pThis.fulllength = 0;
	pThis.verts.resize(vertcount);
	pThis.vertnormals.resize(vertcount);
	pThis.verttangents.resize(vertcount);
	for(int i=0; i<vertcount; i++)
	{
		double len = fulllen*i/((float)segcount);
		if( len>fulllen) len = fulllen;
		double p = curve.findParamFromLength(len);

		Math::Vec3f pt, n;

		MPoint _pt;
		curve.getPointAtParam(p, _pt, MSpace::kWorld);
		::copy(pt, _pt);

//		MVector _n = curve.normal( p, MSpace::kWorld, &stat);
//		MVector _t = curve.tangent( p, MSpace::kWorld, &stat);
//		::copy(n, _n);
		if(i!=0)
		{
			tangent = pt - pThis.verts[i-1];
			pThis.fulllength += tangent.length();
			tangent.normalize();
			normal = Math::cross(tangent, normal);
			normal = -Math::cross(tangent, normal);
			normal.normalize();
		}

		pThis.verts[i] = pt;
		pThis.vertnormals[i] = normal;
		pThis.verttangents[i] = tangent;
		
	}
	/*/
	return true;
}



void FFCurveNeoLocator::LoadGrows( 
	MDataBlock& data, 
	CurveControl* cc
	)
{
	MStatus stat;
	MArrayDataHandle inputDataCache = data.inputArrayValue(i_grows, &stat);
	cc->growdlls.clear();
	cc->grows.clear();
	cc->growdlls.reserve( inputDataCache.elementCount());
	cc->grows.reserve( inputDataCache.elementCount());
	for( unsigned i=0; i<inputDataCache.elementCount(); i++)
	{
//		if( inputDataCache.jumpToElement(i))
		if( inputDataCache.jumpToArrayElement(i))
		{
			MPxData* dataCache = inputDataCache.inputValue(&stat).asPluginData();
			FFGrowData* ffwd = (FFGrowData*)dataCache;
			if( !ffwd) 
				continue;
			if( !ffwd->control)
				continue;

			cc->growdlls.push_back(ffwd->dllproc);

			IFFGrow* control = ffwd->control;
			control->controlId = cc->controlid;

			cc->grows.push_back(control);
		}
	}
	cc->deformer.control = cc;
	if( !cc->deformer.deformid)
		cc->deformer.deformid = cc->controlid;
}

void FFCurveNeoLocator::LoadDeforms( 
	MDataBlock& data, 
	CurveControl* cc
	)
{
	MStatus stat;
	MArrayDataHandle inputDataCache = data.inputArrayValue(i_deforms, &stat);
	cc->deformdlls.clear();
	cc->deforms.clear();
	cc->deformdlls.reserve( inputDataCache.elementCount());
	cc->deforms.reserve( inputDataCache.elementCount());
	for( unsigned i=0; i<inputDataCache.elementCount(); i++)
	{
//		if( inputDataCache.jumpToElement(i))
		if( inputDataCache.jumpToArrayElement(i))
		{
			MPxData* dataCache = inputDataCache.inputValue(&stat).asPluginData();
			FFDeformData* ffwd = (FFDeformData*)dataCache;
			if( !ffwd) 
				continue;
			if( !ffwd->control)
				continue;

			cc->deformdlls.push_back(ffwd->dllproc);

			IFFDeform* control = ffwd->control;
			control->control = cc;

			cc->deforms.push_back(control);
		}
	}
	cc->deformer.control = cc;
	if( !cc->deformer.deformid)
		cc->deformer.deformid = cc->controlid;

	OcsData ocsData(thisMObject());
	cc->deformer.ReadAttributes(&ocsData);

}


FFCurveNeoLocator::FFCurveNeoLocator()
{
}
FFCurveNeoLocator::~FFCurveNeoLocator()
{
}

MStatus FFCurveNeoLocator::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	if( plug == o_control )
	{
		return Build_Control(plug, data);
	}

	return MS::kUnknownParameter;
}
MStatus FFCurveNeoLocator::Build_Control( const MPlug& plug, MDataBlock& data )
{
//	displayStringD( "FFCurveNeoLocator::Build_Control");
	MStatus stat;

	MDataHandle outputData = data.outputValue(plug, &stat );

	typedef FFControlData CLASS;
	CLASS* pData = NULL;

//	bool bNewData = false;
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<CLASS*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( CLASS::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<CLASS*>(fnDataCreator.data( &stat ));

		outputData.set(pData);
		MDataHandle outputData = data.outputValue(plug, &stat );
		userdata = outputData.asPluginData();
		pData = static_cast<CLASS*>(userdata);
	}

	if( !pData->dllproc.isValid())
	{
		pData->dllproc.LoadFormated("curveControl", "furFractalNeoMath.dll");
	}
	if( pData->dllproc.isValid())
	{
		if( !pData->control)
		{
			IFFCONTROL_CREATOR pc = (IFFCONTROL_CREATOR)pData->dllproc.proc;
			pData->control = (*pc)();
		}
		if(pData->control)
		{
			CurveControl* cc = (CurveControl*)pData->control;
			if( cc->controlid==0)
				cc->ReinitGrowId();

			this->LoadGrows( data, cc);
			this->LoadDeforms( data, cc);

			int tess = data.inputValue( i_curveTesselation).asInt();
			
			MFnNurbsCurve curve( data.inputValue( i_curve).asNurbsCurveTransformed());
			bool curveInvert = data.inputValue(i_curveInvert).asBool();
			float length = this->ReadCurve(cc->curve, curve, tess, curveInvert);

			{
				MDataHandle o_growiddata = data.outputValue(i_length);
				o_growiddata.setDouble(cc->curve.fulllength);
				o_growiddata.setClean();
			}

			/*/
			int level = data.inputValue( i_level, &stat ).asInt();
			float maxdist = (float)data.inputValue( i_maxdist).asDouble();

			maxdist = maxdist*(float)(pow((double)2, level));			

			this->init(
				cc,
				curve,
				level, 
				data.inputValue( i_bInvertDirection, &stat ).asBool(), 
				data.inputValue( i_vessels).asInt(), 
				maxdist, 
				0
//				(float)data.inputValue( i_trueway_propencity).asDouble()
				);
			cc->maxdistJitter = (float)data.inputValue( maxdistJitter).asDouble();
			MRampAttribute maxDistRamp(thisMObject(), i_maxDistRamp);
			cc->maxDistRamp.set(maxDistRamp, 0, 1, 1);
			cc->growfrequency = (float)data.inputValue( i_growfrequency).asDouble();
			cc->SubGrowPropencity = (float)data.inputValue( i_subGrowPropencity).asDouble();
			cc->SubGrowPhaseOffset = (float)data.inputValue( i_subGrowPhaseOffset).asDouble();
			cc->SubGrowHistoryOffset = (int)data.inputValue( i_SubGrowHistoryOffset).asDouble();
			cc->WidthFactorJitter = (float)data.inputValue( i_WidthFactorJitter).asDouble();
			cc->SubGrowWidthFactor = (float)data.inputValue( i_SubGrowWidthFactor).asDouble();
			cc->startSeed = data.inputValue(i_seed).asInt();

//			cc-> = data.inputValue( i_grow_growPoints).asInt();
//			cc->maxdist = (float)data.inputValue( i_maxdist).asDouble();
//			cc->trueway_propencity = (float)data.inputValue( i_trueway_propencity).asDouble();
//			cc->growlevel = data.inputValue( i_level, &stat ).asInt();

			cc->bWindow = data.inputValue(i_window).asBool();
			cc->width = (float)data.inputValue(i_width).asDouble();
			cc->windowparam = (float)data.inputValue(i_windowposition).asDouble();
			cc->time = (float)data.inputValue(i_time).asDouble();
			cc->noisemagnitude = (float)data.inputValue(i_noisemagnitude).asDouble();
			cc->noisefrequency = (float)data.inputValue(i_noisefrequency).asDouble();
			cc->noisedimention = (float)data.inputValue(i_noisedimention).asDouble();
			cc->disgrowfactor = (float)data.inputValue(i_disgrowfactor).asDouble();
			cc->debugShowVessel= data.inputValue(i_debugShowVessel).asInt();
			cc->venToper= (float)data.inputValue(i_venToper).asDouble();
			

			cc->bGrow = data.inputValue(i_grow).asBool();

			cc->bDeformer = data.inputValue( i_deformer).asBool();
			cc->xynoisemagnitude = (float)data.inputValue(i_xynoisemagnitude).asDouble();
			cc->xynoisemagnitudeSubGrow = (float)data.inputValue(i_xynoisemagnitudeSubGrow).asDouble();
			cc->xynoisefrequency = (float)data.inputValue(i_xynoisefrequency).asDouble();
			cc->xynoisedimention = (float)data.inputValue(i_xynoisedimention).asDouble();
			cc->deformToper = (float)data.inputValue(deformToper).asDouble();

			cc->texToper = (float)data.inputValue(texToper).asDouble();
			/*/

			OcsData ocsData(thisMObject());
			cc->ReadAttributes(&ocsData);
		}
	}
//	if( bNewData)
//		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

bool FFCurveNeoLocator::isBounded() const
{
	MObject thisNode = thisMObject();
	return false;
}

MBoundingBox FFCurveNeoLocator::boundingBox() const
{
	MObject thisNode = thisMObject();

	MBoundingBox box;
	//...
	return box;
}

void FFCurveNeoLocator::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MObject thisNode = thisMObject();

//	MPlug plug(thisNode, i_input);

	view.beginGL(); 
	//...
	view.endGL();
};


void* FFCurveNeoLocator::creator()
{
	return new FFCurveNeoLocator();
}

MStatus FFCurveNeoLocator::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_grows = typedAttr.create( "grows", "igrs", FFGrowData::id);
			typedAttr.setReadable(false);
			typedAttr.setArray(true);
			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			typedAttr.setIndexMatters(false);
			::addAttribute(i_grows, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray );
		}
		{
			i_deforms = typedAttr.create( "deforms", "idfrms", FFDeformData::id);
			typedAttr.setReadable(false);
			typedAttr.setArray(true);
			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			typedAttr.setIndexMatters(false);
			::addAttribute(i_deforms, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray );
		}




/*/
		{
			i_window = numAttr.create ("bWindow","bwin", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_window, atInput);

			i_windowposition = numAttr.create ("windowPosition","gwp", MFnNumericData::kDouble, 1, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_windowposition, atInput|atKeyable);

			i_width = numAttr.create ("width","wi", MFnNumericData::kDouble, 1, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(2);
			::addAttribute(i_width, atInput);

			i_WidthFactorJitter = numAttr.create ("widthFactorJitter","wifj", MFnNumericData::kDouble, 0.2, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(i_WidthFactorJitter, atInput);

		}
/*/

		{
			i_curve = typedAttr.create( "curve", "cu", MFnData::kNurbsCurve);
			typedAttr.setReadable(false);
//			typedAttr.setArray(true);
//			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
//			typedAttr.setIndexMatters(false);
			::addAttribute(i_curve, atInput );

			i_curveTesselation = numAttr.create ("curveTesselation","cute", MFnNumericData::kInt, 20, &stat);
			numAttr.setMin(10);
			numAttr.setSoftMax(100);
			::addAttribute(i_curveTesselation, atInput);

			i_curveInvert = numAttr.create ("curveInvert","cui", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_curveInvert, atInput);

			i_length = numAttr.create ("curveLength","cule", MFnNumericData::kDouble, 0, &stat);
			::addAttribute(i_length, atInput);


/*/
			i_time = numAttr.create ("time","iti", MFnNumericData::kDouble, 0.01, &stat);
			::addAttribute(i_time, atInput);

			i_noisemagnitude = numAttr.create ("noiseMagnitude","nom", MFnNumericData::kDouble, 0, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_noisemagnitude, atInput);

			i_noisefrequency = numAttr.create ("noiseFrequency","nof", MFnNumericData::kDouble, 0.033, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_noisefrequency, atInput);

			i_noisedimention = numAttr.create ("noiseDimention","nod", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.0001);
			numAttr.setSoftMax(2);
			::addAttribute(i_noisedimention, atInput);

			i_disgrowfactor = numAttr.create ("disGrowFactor","dgf", MFnNumericData::kDouble, 2, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(4);
			::addAttribute(i_disgrowfactor, atInput);

			i_debugShowVessel = numAttr.create ("debugShowVessel","dbgvf", MFnNumericData::kInt, -1, &stat);
			numAttr.setMin(-1);
			numAttr.setSoftMax(20);
			::addAttribute(i_debugShowVessel, atInput);
			
			i_venToper = numAttr.create ("venToper","vent", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(i_venToper, atInput);

			i_SubGrowWidthFactor = numAttr.create ("subGrowWidthFactor","sgwf", MFnNumericData::kDouble, 0.5, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(i_SubGrowWidthFactor, atInput);
/*/

		}
		{
/*/
			i_grow = numAttr.create ("bGrow","bgr", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_grow, atInput);
			
			i_vessels = numAttr.create ("vessels","gve", MFnNumericData::kInt, 6, &stat);
			numAttr.setMin(1);
			numAttr.setSoftMax(10);
			::addAttribute(i_vessels, atInput);

			i_maxdist = numAttr.create ("maxDist","gmd", MFnNumericData::kDouble, 2, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(3);
			::addAttribute(i_maxdist, atInput);

			i_maxDistRamp = MRampAttribute::createCurveRamp("maxDistRamp", "mdr");
			::addAttribute(i_maxDistRamp, atInput);

			maxdistJitter = numAttr.create ("maxdistJitter","gmdj", MFnNumericData::kDouble, 0.5, &stat);
			numAttr.setMin(0);
			numAttr.setMax(0.99);
			::addAttribute(maxdistJitter, atInput);
			

//			i_trueway_propencity = numAttr.create ("truewayPropencity","gtwp", MFnNumericData::kDouble, 0.7, &stat);
//			numAttr.setMin(0);
//			numAttr.setMax(1);
//			::addAttribute(i_trueway_propencity, atInput);

			i_growfrequency = numAttr.create ("growFrequency","grfr", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.001);
			numAttr.setSoftMax(10);
			::addAttribute(i_growfrequency, atInput);

			i_subGrowPropencity = numAttr.create ("subGrowPropencity","sgpr", MFnNumericData::kDouble, 0.6, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(2);
			::addAttribute(i_subGrowPropencity, atInput);

			i_subGrowPhaseOffset = numAttr.create ("subGrowPhaseOffset","sgpo", MFnNumericData::kDouble, 0.2, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(i_subGrowPhaseOffset, atInput);

			i_SubGrowHistoryOffset = numAttr.create ("subGrowHistoryOffset","sgho", MFnNumericData::kDouble, 20, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(40);
			::addAttribute(i_SubGrowHistoryOffset, atInput);

			i_level = numAttr.create ("level","gle", MFnNumericData::kInt, 1, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(4);
			::addAttribute(i_level, atInput);

			i_bInvertDirection = numAttr.create ("invertDirection","ind", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_bInvertDirection, atInput);
			
			i_seed = numAttr.create ("seed","se", MFnNumericData::kInt, 0, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1000);
			::addAttribute(i_seed, atInput);
/*/

		}

/*/
		MObject xynoisemagnitudemin;
		{
			i_deformer = numAttr.create ("bDeformer","bde", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_deformer, atInput);

			i_xynoisemagnitude = numAttr.create ("xyNoiseMagnitude","xynm", MFnNumericData::kDouble, 0.5, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_xynoisemagnitude, atInput);

			xynoisemagnitudemin = numAttr.create ("xynoisemagnitudemin","xynmm", MFnNumericData::kDouble, 1, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(xynoisemagnitudemin, atInput);


			i_xynoisemagnitudeSubGrow = numAttr.create ("xynoisemagnitudeSubGrow","xynms", MFnNumericData::kDouble, 0.5, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_xynoisemagnitudeSubGrow, atInput);
			

			i_xynoisefrequency = numAttr.create ("xyNoiseFrequency","xynf", MFnNumericData::kDouble, 0.033, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_xynoisefrequency, atInput);

			i_xynoisedimention = numAttr.create ("xyNoiseDimention","xynd", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.0001);
			numAttr.setSoftMax(2);
			::addAttribute(i_xynoisedimention, atInput);
			
			deformToper = numAttr.create ("deformToper","deto", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.0001);
			numAttr.setSoftMax(2);
			::addAttribute(deformToper, atInput);
		}
/*/
/*/
		{
			texToper = numAttr.create ("texToper","teto", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.0001);
			numAttr.setSoftMax(2);
			::addAttribute(texToper, atInput);

			shaderId = numAttr.create ("shaderId","shid", MFnNumericData::kInt, 0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(4);
			::addAttribute(shaderId, atInput);
		}
/*/
		{
			o_control = typedAttr.create( "control", "con", FFControlData::id);
			::addAttribute(o_control, atReadable|atWritable|atStorable|atCached|atConnectable);

			attributeAffects(i_curve, o_control);
			attributeAffects(i_curveTesselation, o_control);
			attributeAffects(i_curveInvert,			o_control);
			attributeAffects(i_grows,				o_control);
			attributeAffects(i_deforms,				o_control);

/*/
			attributeAffects(i_window,			o_control);
			attributeAffects(i_windowposition,	o_control);
			attributeAffects(i_width,			o_control);

			attributeAffects(i_noisemagnitude,	o_control);
			attributeAffects(i_noisefrequency,	o_control);
			attributeAffects(i_noisedimention,	o_control);
			attributeAffects(i_time,			o_control);
			attributeAffects(i_disgrowfactor,	o_control);
			attributeAffects(i_debugShowVessel,	o_control);
			attributeAffects(i_venToper,		o_control);		
			attributeAffects(i_WidthFactorJitter,	o_control);
			attributeAffects(i_SubGrowWidthFactor,	o_control);

			attributeAffects(i_grow,			o_control);
			attributeAffects(i_level,			o_control);
			attributeAffects(i_vessels,			o_control);
			attributeAffects(i_maxdist,			o_control);
			attributeAffects(maxdistJitter,			o_control);
			attributeAffects(i_maxDistRamp,			o_control);
//			attributeAffects(i_trueway_propencity, o_control);
			attributeAffects(i_growfrequency, o_control);
			attributeAffects(i_bInvertDirection, o_control);
			attributeAffects(i_subGrowPropencity, o_control);
			attributeAffects(i_subGrowPhaseOffset, o_control);
			attributeAffects(i_SubGrowHistoryOffset, o_control);
			attributeAffects(i_seed,			o_control);

			attributeAffects(i_deformer,		o_control);
			attributeAffects(i_xynoisemagnitude, o_control);
			attributeAffects(xynoisemagnitudemin,	o_control);
			attributeAffects(i_xynoisemagnitudeSubGrow, o_control);
			attributeAffects(i_xynoisefrequency, o_control);
			attributeAffects(i_xynoisedimention, o_control);
			attributeAffects(deformToper, o_control);

			attributeAffects(texToper, o_control);
			attributeAffects(shaderId, o_control);
/*/
			
		}
		if( !MGlobal::sourceFile("AEFFCurveNeoLocatorTemplate.mel"))
		{
			displayString("error source AEFFCurveNeoLocatorTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}