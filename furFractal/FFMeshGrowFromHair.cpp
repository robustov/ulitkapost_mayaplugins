#include "FFMeshGrowFromHair.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFloatMatrix.h>
#include <maya/MRenderUtil.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>

#include <maya/MGlobal.h>
#include "mathNmaya/mathNmaya.h"
#include "Math/MeshGrowFromHair.h"

bool loadTexture(
	const MPlug& src, 
	Math::matrixMxN<float>& dst, 
	int res
	);

// render
//	MObject FFMeshGrowFromHair::texToper;
//	MObject FFMeshGrowFromHair::shaderId;
MObject FFMeshGrowFromHair::startPositions, FFMeshGrowFromHair::startNormals;

MObject FFMeshGrowFromHair::o_grow;
MObject FFMeshGrowFromHair::o_growId;

FFMeshGrowFromHair::FFMeshGrowFromHair()
{
}
FFMeshGrowFromHair::~FFMeshGrowFromHair()
{
}
MStatus FFMeshGrowFromHair::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	if( plug == o_grow )
	{
		return Build_Control(plug, data);
	}
	if( plug == o_growId )
	{
		data.inputValue(o_grow, &stat );
		return MS::kSuccess;		
	}
	return MS::kUnknownParameter;
}
MStatus FFMeshGrowFromHair::setDependentsDirty( const MPlug& plug, MPlugArray& plugArray)
{
	if( plug.isDynamic())
	{
		plugArray.append( MPlug(thisMObject(), o_grow));
	}
	return MPxNode::setDependentsDirty( plug, plugArray);
}

MStatus FFMeshGrowFromHair::Build_Control( const MPlug& plug, MDataBlock& data )
{
	displayStringD( "FFMeshGrowFromHair::Build_Control");
	MStatus stat;

	MDataHandle outputData = data.outputValue(plug, &stat );

	typedef FFGrowData CLASS;
	CLASS* pData = NULL;

	bool bNewData = false;
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<CLASS*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( CLASS::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<CLASS*>(fnDataCreator.data( &stat ));
		bNewData = true;
		if( bNewData)
			outputData.set(pData);
		MDataHandle outputData = data.outputValue(plug, &stat );
		userdata = outputData.asPluginData();
		pData = static_cast<CLASS*>(userdata);
	}

	if( !pData->dllproc.isValid())
	{
		pData->dllproc.LoadFormated("meshGrowFromHair", "furFractalNeoMath.dll");
	}
	if( pData->dllproc.isValid())
	{
		if( !pData->control)
		{
			IFFGROW_CREATOR pc = (IFFGROW_CREATOR)pData->dllproc.proc;
			pData->control = (*pc)();
		}
		if(pData->control)
		{
			MeshGrowFromHair* cc = (MeshGrowFromHair*)pData->control;
			if( cc->growid==0)
				cc->ReinitGrowId();

			printf( "growid=%x\n", cc->growid);
			fflush(stdout);
			/*/
			cc->texResolution = data.inputValue(texResolution).asInt();
			if( !loadTexture(
				MPlug(thisMObject(), this->growSpeed), 
				cc->growSpeed, 
				cc->texResolution
				))
			{
				cc->growSpeed.init(1, 1);
				cc->growSpeed[0][0] = (float)data.inputValue(growSpeed).asDouble();
			}
			/*/

			{
				MObject arobj = data.inputValue(startPositions).data();
				MFnVectorArrayData pa(arobj);
				cc->startPositions.resize(pa.length());
				for(int i=0; i<(int)cc->startPositions.size(); i++)
					::copy(cc->startPositions[i], pa[i]);
			}
			{
				MObject arobj = data.inputValue(startNormals).data();
				MFnVectorArrayData pa(arobj);
				cc->startDirection.resize(pa.length());
				for(int i=0; i<(int)cc->startDirection.size(); i++)
					::copy(cc->startDirection[i], pa[i]);
			}

			// render
//			cc->texToper = (float)data.inputValue(texToper).asDouble();

			OcsData ocsData(thisMObject());
			cc->ReadAttributes(&ocsData);

			MDataHandle o_growiddata = data.outputValue(o_growId);
			o_growiddata.setInt(cc->growid);
			o_growiddata.setClean();
		}
	}
	outputData.setClean();
	return MS::kSuccess;
}

void* FFMeshGrowFromHair::creator()
{
	return new FFMeshGrowFromHair();
}

MStatus FFMeshGrowFromHair::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		MObject grow[6];
		{
			int i=0;

			grow[i] = numAttr.create ("parameter","param", MFnNumericData::kDouble, 0, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(50);
			::addAttribute(grow[i++], atInput|atConnectable|atUnHidden);

			grow[i] = numAttr.create ("growlevel","gle", MFnNumericData::kInt, -2, &stat);
			numAttr.setSoftMin(-3);
			numAttr.setSoftMax(4);
			::addAttribute(grow[i++], atInput);

			grow[i] = numAttr.create ("seed","se", MFnNumericData::kInt, 0, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1000);
			::addAttribute(grow[i++], atInput);

			grow[i] = numAttr.create ("startVenCount","svc", MFnNumericData::kInt, 1, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(grow[i++], atInput);

			grow[i] = numAttr.create ("growlength","sle", MFnNumericData::kDouble, 10.0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(grow[i++], atInput);

			grow[i] = numAttr.create ("wigth","slej", MFnNumericData::kDouble, 1.0f, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(grow[i++], atInput);

			startPositions = typedAttr.create( "startPositions", "spos", MFnData::kVectorArray, &stat);
			::addAttribute(startPositions, atInput|atHidden);

			startNormals = typedAttr.create( "startDirections", "sdir", MFnData::kVectorArray, &stat);
			::addAttribute(startNormals, atInput|atHidden);
		}

		{
			/*/
			texToper = numAttr.create ("texToper","teto", MFnNumericData::kDouble, 10, &stat);
			numAttr.setMin(0.0001);
			numAttr.setSoftMax(2);
			::addAttribute(texToper, atInput);

			shaderId = numAttr.create ("shaderId","shid", MFnNumericData::kInt, 0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(4);
			::addAttribute(shaderId, atInput);
			/*/
		}

		{
//			MFnPluginData fnDataCreator;
//			MObject newDataObject = fnDataCreator.create( FFGrowData::id, &stat );
			MObject newDataObject = MObject::kNullObj;
			o_grow = typedAttr.create( "grow", "gro", FFGrowData::id, newDataObject);
			::addAttribute(o_grow, atReadable|atWritable|atStorable|atCached|atConnectable|atHidden);

			o_growId = numAttr.create ("growId","grid", MFnNumericData::kInt, 0, &stat);
			::addAttribute(o_growId, atReadable|atWritable|atConnectable|atStorable|atUnHidden);

			int s, i;

			s = sizeof( grow)/sizeof( MObject);
			for(i=0; i<s; i++)
			{
				attributeAffects(grow[i], o_grow);
				attributeAffects(grow[i], o_growId);
			}
			attributeAffects(startNormals,		o_grow);
			attributeAffects(startNormals,		o_growId);
			attributeAffects(startPositions,	o_grow);
			attributeAffects(startPositions,	o_growId);
		}

		if( !MGlobal::sourceFile("AEFFMeshGrowFromHairTemplate.mel"))
		{
			displayString("error source AEFFMeshGrowFromHairTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

