#pragma once

namespace Math
{

// ���������� � �����
// ��������� � IFFGrow::Start � Step
// ������������ IFFWindow
struct IFFEdgeData
{
	Math::GridFreeEdge edge;
	int seed;				// seed
	float phase;			// ���������� ����������� �� ����
	Math::Vec3i normal;		// ������� �����

	// ������������� ��� ��������������
	int growId;				// ��� ��������?
	int threadId;			// ����������?
	int vInThread;			// ����� �������� � �����
	float growtime;			// ����� ������� �����
	float edgelength;		// ����� �����
	float lenght;			// ����� �� ������ (�� �������������)
	float lenghtFromEnd;	// ����� �� ����� � ������ growtime (�� �������������)
//	Math::GridCellId prevvertex;		// ������� �� �������� ����� (��� ������������)

	float getEdgeLength(Math::Grid& grid) const 
	{
		return edgelength;
	}

	IFFEdgeData()
	{
		seed = 0;
		lenght = 0;
//		prevvertex = Math::Grid::getNullId();
		edgelength = 0;
		vInThread = -1;
	}
	IFFEdgeData(const Math::GridFreeEdge& edge, int growId, float phase=0, int threadId=0, int seed=0, float lenght=0, float lenghtFromEnd=-1)
	{
		this->edge = edge;
		this->growId = growId;
		this->threadId = threadId;
		this->phase = phase;
		this->seed = seed;
		this->lenght = lenght;
		this->lenghtFromEnd = lenghtFromEnd;
	}
};

}

template<class Stream> inline
Stream& operator >> (Stream& out, Math::IFFEdgeData& v)
{
	unsigned char version = 2;
	out >> version;
	if( version>=0)
	{
		out >> v.edge;
		out >> v.growId;		// ��� ��������?
		out >> v.phase;			// ���������� ����������� �� ����
		out >> v.threadId;
		out >> v.seed;
	}

	if( version>=1)
	{
		out >> v.edgelength;

//		out >> v.lenght;
//		out >> v.lenghtFromEnd;
//		out >> v.growtime;
//		out >> v.prevvertex;
	}
	if( version>=2)
	{
		out >> v.normal;
	}
		
	return out;
}

