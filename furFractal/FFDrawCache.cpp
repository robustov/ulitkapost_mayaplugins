//
// Copyright (C) 
// File: FFDrawCacheCmd.cpp
// MEL Command: FFDrawCache

#include "FFDrawCache.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <sstream>
#include "MathNmaya/MathNmaya.h"
#include "Util/evaltime.h"


FFDrawCache::FFDrawCache()
{
}
FFDrawCache::~FFDrawCache()
{
}

bool subdivEdge( Math::Grid& grid, const Math::GridEdge& edge, std::vector<Math::GridCellId>& cells);
bool subdivEdge( Math::Grid& grid, const Math::GridFreeEdge& edge, std::vector<Math::GridCellId>& cells);

void FFDrawCache::clear()
{
	this->lines.clear();
	this->invisiblepoints.clear();
}

void FFDrawCache::Build(
	GridData* pThis,
	float T, 
	std::vector<IFFDeform*>& deforms,
	std::vector<IFFGrow*>& grows, 
	float widthScaleValue, 
	int texType,
	int drawWidthCnt, 
	int dumpThread, 
	Math::Matrix4f& pruningCamera, 
	float* pruningDist
	)
{
//printf("FFDrawCache::Build\n");
	Util::FunEvalTimeCounter fetc_integrate, fetc_draw;

	Math::Matrix4f& worldm = pThis->ffworldmatrix;

	pThis->grid.timestamp++;
	pThis->grid.setUpDeformers(pThis, worldm, (Math::IGridDeformer**)&deforms[0], (int)deforms.size());

	// ������ t, l � lfe
	pThis->dumpThread = dumpThread;
	{
		Util::FunEvalTime fet(fetc_integrate);
		pThis->integrate(grows, T, false);
	}

	this->clear();
	this->invisiblepoints.reserve(10000);

	if( drawWidthCnt)
	{
		Util::FunEvalTime fet(fetc_draw);

		this->bStrip = true;
		this->lines.clear();
		this->invisiblepoints.clear();

		int vertcount = 0;
		for(int t=0; t<(int)pThis->threads.size(); t++)
		{
			Math::IFFThreadData& thread = pThis->threads[t];
			vertcount += (int)thread.curve.size();
		}
		this->invisiblepoints.reserve(vertcount);

		std::vector<Math::Vec3f> P2, P1, C;

		for(int t=0; t<(int)pThis->threads.size(); t++)
		{
			Math::IFFThreadData& thread = pThis->threads[t];
			if( thread.growId==-1)
				continue;
			// IFFGrow
			IFFGrow* grow = NULL;
			for(int f=0; f<(int)grows.size(); f++)
			{
				if( grows[f]->growid != thread.growId)
					continue;
				grow = grows[f];
				break;
			}
			if( !grow)
				continue;
			if( !grow->bView)
				continue;

			int vertcount = (int)thread.curve.size();
			P1.clear();
			P2.clear();
			C.clear();
			P1.reserve(vertcount);
			P2.reserve(vertcount);
			C.reserve(vertcount);

			// 
			Math::GridCellId id1 = thread.curve[0];
			Math::GridCellId idn = id1;
			idn += thread.startnormal;

			Math::Vec3f n = pThis->grid.getTopLevelCellPos(idn)-pThis->grid.getTopLevelCellPos(id1);
			n.normalize();
			Math::Vec3f toppt(0);
			float lastW = 1e32f;
			bool bInvisible = false;
			float maxW = 0;
			float pruningCameraDist2 = 1e32f;

			if(FurFractal_licflag!=1)
				bInvisible = true;

			Math::Vec3f pos0(0), pos1(0);
			for(int vert=0; vert<(int)thread.curve.size(); vert++, pos0=pos1)
			{
				Math::GridCellId id1 = thread.curve[vert];
				pos1 = pThis->grid.getCellPos(id1);

				if(bInvisible)
				{
					this->invisiblepoints.push_back(pos1);
					continue;
				}

				float w1 = pThis->getEdgeWidth(
					thread, vert, grows, deforms, widthScaleValue);

				// ��� ��������
				maxW = __max(maxW, w1);
				Math::Vec3f pos_ws = worldm*pos1;
				float d2 = (pruningCamera*Math::Vec3f(0)-pos_ws).length2();
				pruningCameraDist2 = __min(d2, pruningCameraDist2);


				if(vert!=0)
				{
					Math::Vec3f dir = pos1 - pos0;

					n = Math::cross(n, dir);
					n = -Math::cross(n, dir);
					n.normalize();
				}

				if(w1<=0 && lastW>0)
				{
					this->invisiblepoints.push_back(pos1);
					if(vert==0)
					{
						toppt = pos1;
						bInvisible = true;
						continue;
					}
					else
					{
						float factor = lastW/(lastW-w1);
						toppt = pos0*(1-factor) + pos1*factor;
						n = Math::Vec3f(0);
						pos1 = toppt;
					}
				}
				lastW = w1;


				Math::Vec3f c1(1);
				if(texType==-1)
				{
					if( grow)
						c1= grow->color;
				}
				else
				{
					float uv1 = pThis->getTexCoord(
						thread, vert, grows, deforms, texType);
					c1 = Math::Vec3f(uv1);
				}
				P1.push_back(pos1 + n*w1);
				P2.push_back(pos1 - n*w1);
				C.push_back(c1);

				if(w1<=0)
				{
					bInvisible = true;
					continue;
				}
			}

			if(texType==-2)
			{
				// ��������� �������
				float pruningCameraDist = sqrt(pruningCameraDist2);
				int renderType = pThis->getPruning(maxW, pruningCameraDist, pruningDist);

				Math::Vec3f c1(1);
				if(renderType==CURVE_LINEAR)
					c1 = Math::Vec3f(0, 0, 1);
				if(renderType==CURVE_CUBIC)
					c1 = Math::Vec3f(0, 1, 1);
				if(renderType==MESH)
					c1 = Math::Vec3f(0, 1, 0);
				if(renderType==SUBDIV)
					c1 = Math::Vec3f(1, 0, 0);

				for(int c=0;c<(int)C.size(); c++)
					C[c] = c1;
			}
				

			if( P1.size()>=2)
			{
				{
					Line& line = this->lines[thread.threadId];
					line.P = P1;
					line.C = C;
				}
				{
					Line& line = this->lines[thread.threadId+1000000];
					line.P = P2;
					line.C = C;
				}
			}
		}
	}
	else
	{
		Util::FunEvalTime fet(fetc_draw);

		this->bStrip = false;
		std::map<GridData::Edge, GridData::EdgeData>::iterator it = pThis->alives.begin();
		for(;it != pThis->alives.end(); it++)
		{
			const GridData::EdgeData& edgedata = it->second;
			const GridData::Edge& edge = edgedata.edge;
			
			IFFGrow* grow = NULL;
			for(int f=0; f<(int)grows.size(); f++)
			{
				if( grows[f]->growid != edgedata.growId)
					continue;
				grow = grows[f];
				break;
			}
			if( !grow)
				continue;
			if( !grow->bView)
				break;

			Math::IFFThreadData& thread = pThis->threads[edgedata.threadId];
			int vert = edgedata.vInThread;
			if( vert >= (int)thread.lengthFromEnd.size())
				continue;

			const Math::GridCellId& id0 = edge.v1();
			const Math::GridCellId& id1 = edge.v2();
			Math::Vec3f v1 = pThis->grid.getCellPos( pThis->grid.addCell(id0));
			Math::Vec3f v2 = pThis->grid.getCellPos( pThis->grid.addCell(id1));

			// ������ �����
			float w0 = pThis->getEdgeWidth(
				thread, vert, grows, deforms, widthScaleValue);
			float w1 = pThis->getEdgeWidth(
				thread, vert+1, grows, deforms, widthScaleValue);

	//		printf("%f %f\n", w0, w1);

			if(w0<0.01f)
			{
				Math::Vec3f cavg = (v1+v2)*0.5f;
	//			cavg = worldm*cavg;

				invisiblepoints.push_back(cavg);
				continue;
			}
			float w = __max(w0,w1);

			int iw = (int)( floor(w+0.5f));
			if(iw<1) iw=1;
			if(iw>6) iw=6;
			if( drawWidthCnt)
				iw = 1;

			bool bSubdivide = false;
			if( !bSubdivide && this->lines.find(iw)==this->lines.end())
			{
				Line& line = this->lines[iw];
				int vc = pThis->alives.size()*2;
				if( drawWidthCnt)
					vc = 2*vc;
				line.P.reserve(vc);
				line.C.reserve(vc);
			}

			Line& line = this->lines[iw];

			std::vector<Math::GridCellId> cells;
			if( !bSubdivide)
			{
				cells.push_back(edge.v1());
				cells.push_back(edge.v2());
			}
			else
			{
				subdivEdge( pThis->grid, edge, cells);
			}
			std::vector<Math::Vec3f> P, C;

			int reservecount = cells.size()*2;
			if( drawWidthCnt)
				reservecount = reservecount*2;
			P.reserve(reservecount);
			C.reserve(reservecount);

			for(int i=1; i<(int)cells.size(); i++)
			{
				Math::GridCellId id1 = cells[i-1];
				Math::GridCellId id2 = cells[i];
				Math::Vec3f pos1 = pThis->grid.getCellPos(id1);
				Math::Vec3f pos2 = pThis->grid.getCellPos(id2);
				float pointOnedge1 = (i-1)/(float)(cells.size()-1);
				float pointOnedge2 = i/(float)(cells.size()-1);
				if(w1<0)
				{
					float factor = w0/(w0-w1);
					pos2 = pos1*(1-factor) + pos2*factor;
				}
				Math::Vec3f c1, c2;

				if(texType==-1)
				{
					Math::Vec3f color(0, 1, 0);
					if( grow)
						color = grow->color;
					c1 = Math::Vec3f(color);
					c2 = Math::Vec3f(color);
				}
				else
				{
					float uv1 = pThis->getTexCoord(
						thread, vert, grows, deforms, texType);
					float uv2 = pThis->getTexCoord(
						thread, vert+1, grows, deforms, texType);

					c1 = Math::Vec3f(uv1);
					c2 = Math::Vec3f(uv2);
				}
				if(drawWidthCnt)
				{
					float w1 = pThis->getEdgeWidth(
						thread, vert, grows, deforms, widthScaleValue);
					float w2 = pThis->getEdgeWidth(
						thread, vert+1, grows, deforms, widthScaleValue);


					Math::GridCellId idn = id1;
					idn+=edgedata.normal;
					Math::Vec3f n = pThis->grid.getCellPos(idn, false)-pThis->grid.getCellPos(id1, false);
					n.normalize();

					P.push_back(pos1 + n*w1);
					P.push_back(pos2 + n*w2);
					P.push_back(pos1 - n*w1);
					P.push_back(pos2 - n*w2);
					C.push_back(c1);
					C.push_back(c2);
					C.push_back(c1);
					C.push_back(c2);
				}
				else
				{
					P.push_back(pos1);
					P.push_back(pos2);
					C.push_back(c1);
					C.push_back(c2);
				}
			}
			line.P.insert(line.P.end(), P.begin(), P.end());
			line.C.insert(line.C.end(), C.begin(), C.end());
		}

	}

	pThis->grid.setUpDeformers(NULL, Math::Matrix4f(1), NULL, 0);
	fflush(stdout);
	displayStringD("integrate=%f, draw=%f", fetc_integrate.sumtime, fetc_draw.sumtime);
}

void FFDrawCache::draw(
	M3dView &view, 
	bool colorize
	)
{
	
	std::map<int, Line>::iterator it = lines.begin();
	for(;it != lines.end(); it++)
	{
		Line& line = it->second;

		if(this->bStrip)
		{
			int id = it->first;
#ifdef _DEBUG
			if(id<1000000)
			{
				char buf[24];
				MPoint pt;
				::copy( pt, line.P[0]);
				itoa( it->first, buf, 10);
				view.drawText(buf, pt);
			}
#endif

			glLineWidth(1);

			glEnableClientState(GL_VERTEX_ARRAY);
			glVertexPointer( 3, GL_FLOAT, 0, &line.P[0]);
			if(colorize)
			{
				glEnableClientState(GL_COLOR_ARRAY);
				glColorPointer( 3, GL_FLOAT, 0, &line.C[0]);
			}

			glDrawArrays(GL_LINE_STRIP, 0, line.P.size());
			if(colorize)
				glDisableClientState(GL_COLOR_ARRAY);
			glDisableClientState(GL_VERTEX_ARRAY);

		}
		else
		{
			glLineWidth((float)it->first);

			glEnableClientState(GL_VERTEX_ARRAY);
			glVertexPointer( 3, GL_FLOAT, 0, &line.P[0]);
			if(colorize)
			{
				glEnableClientState(GL_COLOR_ARRAY);
				glColorPointer( 3, GL_FLOAT, 0, &line.C[0]);
			}

			glDrawArrays(GL_LINES, 0, line.P.size());
			if(colorize)
				glDisableClientState(GL_COLOR_ARRAY);
			glDisableClientState(GL_VERTEX_ARRAY);
		}
	}
}
void FFDrawCache::drawPoints(
	)
{
	// 
	std::map<int, Line>::iterator it = lines.begin();
	for(;it != lines.end(); it++)
	{
		Line& line = it->second;
		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer( 3, GL_FLOAT, 0, &line.P[0]);
		glDrawArrays(GL_POINTS, 0, line.P.size());
		glDisableClientState(GL_VERTEX_ARRAY);
	}
}

void FFDrawCache::drawInvisiblePoints(
	)
{
	// ����� �����������
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer( 3, GL_FLOAT, 0, &this->invisiblepoints[0]);
	glDrawArrays(GL_POINTS, 0, invisiblepoints.size());
	glDisableClientState(GL_VERTEX_ARRAY);
}

MTypeId FFDrawCache::typeId() const
{
	return FFDrawCache::id;
}

MString FFDrawCache::name() const
{ 
	return FFDrawCache::typeName; 
}

void* FFDrawCache::creator()
{
	return new FFDrawCache();
}

void FFDrawCache::copy( const MPxData& other )
{
	const FFDrawCache* arg = (const FFDrawCache*)&other;

	DATACOPY(bStrip);
	DATACOPY(lines);
	DATACOPY(invisiblepoints);
}

MStatus FFDrawCache::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("FFDrawCache: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus FFDrawCache::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus FFDrawCache::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus FFDrawCache::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void FFDrawCache::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
//		stream >> ;
	}
}

