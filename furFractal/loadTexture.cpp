#include "loadTexture.h"

#include <Maya/MPlugArray.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFloatMatrix.h>
#include <maya/MRenderUtil.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatArray.h>
#include <maya/MString.h>

bool loadTexture(
	const MPlug& src, 
	Math::matrixMxN<float>& dst, 
	int res
	)
{
	dst.clear();
	
	MPlugArray plar;
	src.connectedTo(plar, true, false);
	if( plar.length()!=1)
		return false;

	MPlug plug = plar[0];

	if( !plug.node().hasFn( MFn::kTexture2d))
		return false;

	MFloatArray uCoords(res*res), vCoords(res*res);
	// read from SHADER
	for(int u=0; u<res; u++)
		for(int v=0; v<res; v++)
		{
			int ind = u*res + v;
			uCoords[ind] = (float)(u+0.5f)/res;
			vCoords[ind] = (float)(v+0.5f)/res;
		}

	MFloatMatrix cameraMat;
	MFloatVectorArray colors, transps;
	bool result;
	result = MRenderUtil::sampleShadingNetwork( 
			plug.name(), res*res,
			false, false, cameraMat,
			NULL,
			&uCoords, &vCoords,
			NULL, NULL, NULL, NULL, NULL,
			colors, transps );
	if( !result)
		return false;
	if( colors.length()!=res*res)
		return false;

	dst.init(res, res);

	for(int x=0, i=0; x<res; x++)
	{
		for(int y=0; y<res; y++, i++)
		{
			float d = (colors[i][0]+colors[i][1]+colors[i][2])/3;
			dst[y][x] = d;
		}
	}
	return true;
}

