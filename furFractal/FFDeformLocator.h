#pragma once
#include "pre.h"
#include "ocellaris/IRender.h"
//
// Copyright (C) 
// File: FFDeformLocatorNode.h
//
// Dependency Graph Node: FFDeformLocator

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include <maya/MFnMesh.h> 
#include "IFFDeformModifer.h"

#include "FFDeformData.h"

class FFDeformLocator : public MPxNode
{
public:
	FFDeformLocator();
	virtual ~FFDeformLocator(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	///
	virtual MStatus setDependentsDirty( const MPlug& plug, MPlugArray& plugArray);

	static void* creator();
	static MStatus initialize();

	MStatus Build_Control( const MPlug& plug, MDataBlock& data );

	void LoadSource( 
		MDataBlock& data, 
		std::vector<int>& srcGrows
		);

	void LoadModifers( 
		MDataBlock& data, 
		std::vector<IFFDeformModifer>& modifers
		);

public:

	// render
	static MObject entrypoint;
	static MObject startPositions, startNormals;
	static MObject i_modifers;
	static MObject i_sourceGrows;

	static MObject o_deform;
	static MObject o_deformId;

public:
	static MTypeId id;
	static const MString typeName;
};

