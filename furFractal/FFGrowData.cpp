//
// Copyright (C) 
// File: FFGrowDataCmd.cpp
// MEL Command: FFGrowData

#include "FFGrowData.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include "FFGridData.h"
#include "ocellaris/renderMemImpl.h"
#include <sstream>




FFGrowData::FFGrowData()
{
#ifdef DLLPROCEDURE_DUMP
printf("FFGrowData %x\n", (int)this);
#endif 

	control = NULL;
}
FFGrowData::~FFGrowData()
{
#ifdef DLLPROCEDURE_DUMP
printf("~FFGrowData %x\n", (int)this);
#endif 
	if( control)
		control->release();
	control = 0;
	dllproc.Free();
}

MTypeId FFGrowData::typeId() const
{
	return FFGrowData::id;
}

MString FFGrowData::name() const
{ 
	return FFGrowData::typeName; 
}

void* FFGrowData::creator()
{
	return new FFGrowData();
}


void FFGrowData::copy( const MPxData& other )
{
	const FFGrowData* arg = (const FFGrowData*)&other;

	DATACOPY(dllproc);
	if( dllproc.isValid())
	{
		IFFGROW_CREATOR pc = (IFFGROW_CREATOR)dllproc.proc;
		control = (*pc)();
		control->copy(arg->control);
	}
}

MStatus FFGrowData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("FFGrowData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus FFGrowData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus FFGrowData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus FFGrowData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void FFGrowData::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version==0)
	{
		if(stream.isLoading())
		{
			if( control)
				control->release();
			control = 0;
		}
		stream >> dllproc.dll;
		stream >> dllproc.entrypoint;
		if(stream.isLoading())
		{
			if(dllproc.dll=="furFractal.mll")
				dllproc.dll = "furFractalNeoMath.dll";
			dllproc.Load();
			if( dllproc.isValid())
			{
				IFFGROW_CREATOR pc = (IFFGROW_CREATOR)dllproc.proc;
				control = (*pc)();
			}
		}
		if(control)
			control->serialize(stream);
	}
}

