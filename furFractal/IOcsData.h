#pragma once

#include "ocellaris/IRender.h"
#include "Math/matrixMxN.h"
#include "Math/AnimCurve.h"

struct IOcsData
{
	virtual cls::Param getAttribute(
		const char* name, 
		cls::Param& defvalue)
	{
		return cls::Param();
	};
	virtual void setAttribute(
		const char* name, 
		cls::Param& value)
	{
	};
	virtual bool getTextureAttribute(
		const char* name, 
		float defvalue, 
		Math::matrixMxN<float>& dst, 
		int res)
	{
		dst.init(1, 1);
		dst[0][0] = defvalue;
		return true;
	}
	virtual bool getAnimCurve(
		const char* name, 
		Math::AnimCurve<float>& dst, 
		int res)
	{
		return false;
	}
};

#define READMAYAFLOAT(ARG) {cls::P<float> pf = node->getAttribute(#ARG, cls::P<float>(this->##ARG) );if( !pf.empty()) this->##ARG = pf.data();}
#define READMAYAINT(ARG) {cls::P<int> pf = node->getAttribute(#ARG, cls::P<int>(this->##ARG) );if( !pf.empty()) this->##ARG = pf.data();}
#define READMAYABOOL(ARG) {cls::P<bool> pf = node->getAttribute(#ARG, cls::P<bool>(this->##ARG) );if( !pf.empty()) this->##ARG = pf.data();}
#define READMAYACOLOR(ARG) {cls::P<Math::Vec3f> pf = node->getAttribute(#ARG, cls::P<Math::Vec3f>(this->##ARG, cls::PT_COLOR) );if( !pf.empty()) this->##ARG = pf.data();}
#define READMAYAFLOATTEX(ARG) {node->getTextureAttribute(#ARG, 1, this->##ARG, 128);}
#define READMAYAFLOATANIM(ARG) {node->getAnimCurve(#ARG, this->##ARG, 256);}
