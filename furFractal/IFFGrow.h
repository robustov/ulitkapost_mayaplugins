#pragma once

#include "Math/furFractalMath.h"
#include "Math/Math.h"
#include "Math/Grid.h"
#include "IFFEdgeData.h"
#include "IFFThreadData.h"
#include "IFFVertexData.h"
#include "IOcsData.h"

class GridData;

struct IFFControl;

#pragma warning (disable: 4251 4275)

//namespace Math
//{

	// ��������� ��� ������������ 
	struct FURFRACTALMATH_API IFFGrow
	{
		bool bEnable, bView;
		int growid;
		int controlId;
		int startSeed, seed;			// startSeed �������� seed �������� ��� Step
		Math::Vec3f color;
		float texToper;
		std::vector<int> srcGrows;		// ������ ����������� �� ������� �����
		bool invertSrcGrows;			// ������������� ������ ����������� 
		// ����� �� ����?
		bool growFromThis;

	// ����� ���������
	public:
		// ��������� �����
		int growlevel;
		float growlength, growlengthJitter;
		Math::matrixMxN<float> growlengthTex;

		// ��������������
		float delay, delayRandom;
		Math::matrixMxN<float> delayTex;
		int notDelayedCount;			// ����� �������� ������� �������� ��� ��������
		bool delayAbsolute;				// ���� ��� delay � growAnimation

		// ������ ������������ ����� ����� �� �������
		Math::AnimCurve<float> growAnimation;

		Math::AnimCurve<float> velocity;
		float velocityJitter;
		Math::matrixMxN<float> velocityTex;

		// ������������
		float wigth, wigthJitter;
		float widthTopper;

	public:
		virtual void release(
			)
		{
			delete this;
		}

	// ��� �����
	public:
		// ��������� �������
		virtual void Start(
			std::vector<IFFControl*>& allcontrols,
			std::vector<IFFGrow*>& allgrows,
			IFFControl* _control, 
			GridData& griddata
			)
		{
			srand(startSeed);
			seed = rand();
		};
		// ������� - ����� ��������� �������� ����� - 
		// ���� ���� - ���������� �� ���
		virtual void OnNewThread(
			std::vector<IFFControl*>& allcontrols,
			std::vector<IFFGrow*>& allgrows,
			IFFControl* _control, 
			GridData& griddata, 
			Math::IFFThreadData& newthread		// ��� ���� �������
			){};

		// ������������ ��������� �����
		virtual bool GrowNextThread(
			std::vector<IFFControl*>& allcontrols,
			std::vector<IFFGrow*>& allgrows,
			IFFControl* _control, 
			GridData& griddata, 
			Math::IFFThreadData& newthread
			)=0;
		// ��������� ��� ��������� ����� ���������� ������ �����������
		// ...

		// ����� ���������
		virtual void GrowStop(
			IFFControl* _control, 
			GridData& griddata
			){};

		// ����� ���������
		virtual int PriorityNextThread(
			IFFControl* _control, 
			GridData& griddata
			){return -10000000;};

	// ��� ��������������
	public:
		// breakLength length growtime
		virtual bool calcGrowTime(
			GridData& griddata,					// �������
			Math::IFFThreadData& thread,		// ����
			float parentGrowTime,				// grow time � ����� �������
			float parentLength,					// ����� � ����� �������
			float T,							// ���������� �������� T
			float& breakLength, 
			float& breakLengthVirtual
			);

		// �������� ������
		// ����� �������� ���������� �������� T ��� ���� � ��������
		virtual float getDelay(
			GridData& griddata,					// �������
			Math::IFFThreadData& thread,		// ����
			float parentGrowTime,				// grow time � ����� �������
			float parentLength,					// ����� � ����� �������
			float& lengthwanted,				// ������ ��������� ����� �������
			float& T							// ���������� �������� T
			);
		// ���� �������� ����� - ��������� ����������� ����� �� �����
		virtual float getBreakLength(
			GridData& griddata, 
			Math::IFFThreadData& thread, 
			float lengthwanted,				// ��������� ����� �������
			float restTime
			);
		// ����������� ����� �� ����� (�� ��� ������ ���������)
		virtual float getBreakLengthVirtual(
			GridData& griddata, 
			Math::IFFThreadData& thread, 
			float lengthwanted,				// ��������� ����� �������
			float restTime
			);
		// ���������� �������� ����� ��� �����
		// �������� ������������� � ���������� ����� ������� �� ������ ��������� getThreadLength
		virtual float getVelocity(
			GridData& griddata,					// �������
			Math::IFFThreadData& thread,		// ����
			int v								// ������� � ����	
			);
	protected:
		virtual bool calcGrowTimeAnimation(
			GridData& griddata,					// �������
			Math::IFFThreadData& td,		// ����
			float T,						// ���������� �������� T
			float parentGrowTime,				// grow time � ����� �������
			bool bGrowAnimation,			// ������ �� grow animation?
			float& breakLength, 
			float& breakLengthVirtual
			);

	// ��� ������������
	public:
		// topper
		virtual float getToper(
			GridData& griddata,
			Math::IFFThreadData& thread,
			int v,
			int toperType
			);

		// ������
		virtual float getWidth(
			GridData& griddata,
			Math::IFFThreadData& thread,
			int v,
			float parentWidth1,
			float parentWidth2
			);
		// ���������� �����.���������� ��� ����� �� ����� (���� ����� �� ������������)
		virtual float getTexCoord(
			GridData& griddata,					// �������
			Math::IFFThreadData& thread,
			int v,
			int texCoordType					// �� ������� ���� ��������� UV ����� ���� ������
			){return false;};
		// ������
		virtual float getGrowAnimation(
			float T
			);

	public:
		IFFGrow();
		virtual void copy(const IFFGrow* _arg);

		virtual void serialize(Util::Stream& stream);
		virtual void SerializeOcs(
			const char* _name, 
			cls::IRender* render, 
			bool bSave
			);
		// ������ ��������� �� �����
		virtual void ReadAttributes(
			IOcsData* node
			);
		void ReinitGrowId();

		// ����� �� ����� � ����� grow
		bool isValidForParent(
			Math::IFFThreadData& thread
			);

	};
	typedef IFFGrow* (*IFFGROW_CREATOR)();
//}

