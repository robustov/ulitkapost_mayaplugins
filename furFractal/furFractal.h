#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>
#include "Math/Grid.h"
#include "ocellaris/IGeometryGenerator.h"
#include "ocellaris/IRender.h"
#include "Util/DllProcedure.h"

class FFGridData;
class MFnNurbsCurve;
#include "IFFWindow.h"
#include "IFFGrow.h"
#include "IFFControl.h"
#include "FFDrawCache.h"

class furFractal : public MPxLocatorNode
{
public:
	furFractal();
	virtual ~furFractal(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	// ����� ���� ��������� ��� �����
	MStatus shouldSave( 
		const MPlug& plug, 
		bool& result );

	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	// ��������� ������ ���������
	void RenderProxy(cls::IRender* render);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	bool excludeAsLocator()const{return false;}

	static void* creator();
	static MStatus initialize();

public:
	MStatus Build_Grid(
		const MPlug& plug, MDataBlock& data);
	FFGridData* Load_Grid(
		MDataBlock& data);

	MStatus Build_DrawCache(
		const MPlug& plug, MDataBlock& data);
	FFDrawCache* Load_DrawCache(
		MDataBlock& data);

	MStatus Build_OutMesh(
		const MPlug& plug, MDataBlock& data);

	void LoadControls( 
		MDataBlock& data, 
		std::vector<IFFControl*>& ffcs, 
		std::vector<Util::DllProcedure*>* dlls=NULL
		);

	void LoadWindows( 
		MDataBlock& data, 
		std::vector<Math::IFFWindow*>& ffds);
	void LoadGrows( 
		MDataBlock& data, 
		std::vector<IFFGrow*>& ffgr, 
		std::vector<IFFControl*>& ctrls
		);
	void LoadDeformers( 
		MDataBlock& data, 
		std::vector<IFFDeform*>& ffds
		);

	// ����������� �����������
	bool Rebuild(int operation);

public:
	static MObject i_maxStep[2];
	static MObject i_T;
	static MObject i_widthScale;
	static MObject i_dimention;
	static MObject i_controls;
	static MObject i_deforms;

	static MObject i_bPruning;
	static MObject i_pruningCamera;
	static MObject i_pruningDistance[3];

	static MObject i_rebuildStartFrame;
	static MObject i_viewGridDots;
	static MObject i_viewGrid;
	static MObject i_viewGridLevel;
	static MObject i_viewGridPoint;
	static MObject i_drawBox;
	static MObject i_color;
	static MObject i_drawWidthCnt;
	static MObject i_texType;
	static MObject i_dumpThread;

	static MObject i_renderType;
	static MObject i_renderWidth;
	static MObject i_tesselation;


	static MObject i_binding;
	static MObject o_output;
	static MObject o_drawcache;

	static MObject o_mesh;
	// ����������
	static MObject o_venCount, o_segCount, o_estimationTime;

public:
	static MTypeId id;
	static const MString typeName;

	MDataBlock forcecache( MDGContext& ctx=MDGContext::fsNormal ) const
	{
		furFractal* pThis = (furFractal*)this;
		MDataBlock data = pThis->forceCache(ctx);
		return data;
	}



public:
	//! @ingroup implement_group
	//! \brief ������� ratoidImitatorGenerator
	//! 
	struct Generator : public cls::IGeometryGenerator
	{
		// ���������� ���, ������ ��� �������������
		virtual const char* GetUniqueName(
			){return "furFractal";};

		//! ����������� ���� � �������� 
		virtual bool OnPrepareNode(
			cls::INode& node, 
			cls::IExportContext* context,
			MObject& generatornode
			);

		virtual bool OnGeometry(
			cls::INode& node,				//!< ������ 
			cls::IRender* render,			//!< render
			Math::Box3f& box,				//!< box 
			cls::IExportContext* context,	//!< context
			MObject& generatornode			//!< generator node �.� 0
			);
	};
	static Generator generator;


};


