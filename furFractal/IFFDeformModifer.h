#pragma once

#include "Math/Ramp.h"
#include "ocellaris/IOcellaris.h"
#include "ocellaris/IRender.h"
#include "Math/Ramp.h"

struct IFFDeformModifer
{
	Math::Matrix4f worldmatrix;
	Math::Matrix4f worldmatrixInv;
	float radius;
	Math::Ramp ramp;

	enum enType
	{
		MT_SPHERE = 0,
		MT_CYLINDER = 1, 
	};
	int type;

	IFFDeformModifer()
	{
		radius = 1;
		type = MT_SPHERE;
	}

	virtual void SerializeOcs(
		const char* _name, 
		cls::IRender* render, 
		bool bSave
		)
	{
		std::string name = _name;
		SERIALIZEOCS(worldmatrix);
		SERIALIZEOCS(worldmatrixInv);
		SERIALIZEOCS(radius);
		SERIALIZEOCS(type);

		ramp.serializeOcs( (name+"::ramp_").c_str(), render, bSave);
	}
	float getFactor(Math::Vec3f& ws)
	{
		Math::Vec3f lpt = this->worldmatrixInv*ws;
		float len = lpt.length();
		if( this->radius>0.0001)
			len = len/this->radius;
		if( len>1)
			return 1;
		float f = this->ramp.getValue(len);
		return f;
	}
};


template<class Stream> inline
Stream& operator >> (Stream& out, IFFDeformModifer& v)
{
	unsigned char version = 2;
	out >> version;
	if( version>=0)
	{
		out >> v.worldmatrix;
		out >> v.radius;
	}
	if( version>=1)
	{
		out >> v.worldmatrixInv;
		out >> v.ramp;
	}
	if( version>=2)
	{
		out >> v.type;
	}
	return out;
}

