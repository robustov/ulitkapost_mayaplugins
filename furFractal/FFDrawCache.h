#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include <maya/M3dView.h>
#include "Util/Stream.h"

#include "Math/Grid.h"
#include "IFFWindow.h"
#include "IFFGrow.h"
#include "Math/GridData.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", FFDrawCache::id );
	}
/*/


class FFDrawCache : public MPxData
{
public:
	FFDrawCache();
	virtual ~FFDrawCache(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

	void draw(
		M3dView &view, 
		bool bColor
		);
	void drawPoints(
		);
	void drawInvisiblePoints(
		);

public:
	// ������ ����� �� 6 - ����� � ����
	struct Line
	{
		std::vector<Math::Vec3f> P;
		std::vector<Math::Vec3f> C;
	};
	
	bool bStrip;
	std::map<int, Line> lines;
	std::vector<Math::Vec3f> invisiblepoints;

	void clear();
	void Build(
		GridData* pThis,
		float T, 
		std::vector<IFFDeform*>& deforms,
		std::vector<IFFGrow*>& grows, 
		float widthScaleValue,
		int texType,// = 1, 
		int drawWidthCnt,// = 0,
		int dumpThread,// = -1
		Math::Matrix4f& pruningCamera,		// ������ ��� ��������
		float* pruningDist
		);

public:
	static MTypeId id;
	static const MString typeName;

protected:
	void serialize(Util::Stream& stream);
};
