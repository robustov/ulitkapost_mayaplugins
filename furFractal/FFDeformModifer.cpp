//
// Copyright (C) 
// File: FFDeformModiferCmd.cpp
// MEL Command: FFDeformModifer

#include "ocellaris/IRender.h"
#include "FFDeformModifer.h"

#include <maya/MGlobal.h>
#include <maya/MFnPluginData.h>
#include "MathNMaya/MathNMaya.h"
#include "MathNGl/MathNGl.h"
#include <maya/MRampAttribute.h>
#include <maya/MFnEnumAttribute.h>

MObject FFDeformModifer::i_type;
MObject FFDeformModifer::i_radius;
MObject FFDeformModifer::i_affect;
MObject FFDeformModifer::o_modifer;

FFDeformModifer::FFDeformModifer()
{
}
FFDeformModifer::~FFDeformModifer()
{
}

MStatus FFDeformModifer::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	if( plug.array() == o_modifer )
	{
		return Build_Control(plug, data);
	}
	if( plug == o_modifer )
	{
		for( int s=0; s<(int)plug.numElements(); s++)
		{
			Build_Control(plug.elementByPhysicalIndex(s), data);
		}
		return MS::kSuccess;
	}

	return MS::kUnknownParameter;
}

MStatus FFDeformModifer::Build_Control( const MPlug& plug, MDataBlock& data )
{
//	displayStringD( "FFDeformModifer::Build_Control %s", plug.name().asChar());
	MStatus stat;

	MDataHandle outputData = data.outputValue(plug, &stat );

	typedef FFDeformModiferData CLASS;
	CLASS* pData = NULL;

	bool bNewData = false;
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<CLASS*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( CLASS::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<CLASS*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	MArrayDataHandle dah = data.inputArrayValue( this->worldMatrix);
	dah.jumpToElement(0);
	dah.inputValue().asMatrix();

	MPlug wmplug(thisMObject(), this->worldMatrix);
	MObject wmobj;
	wmplug.elementByLogicalIndex(0).getValue(wmobj);
	MMatrix mwm = MFnMatrixData(wmobj).matrix();
	Math::Matrix4f wm;
	copy(wm, mwm);
	Math::Matrix4As3f wm3(wm);

	IFFDeformModifer& modifer = pData->modifer;
	modifer.worldmatrix = wm;
	modifer.worldmatrixInv = wm.inverted();
	modifer.radius = (float)data.inputValue(i_radius).asDouble();
	short type = data.inputValue(i_type).asShort();
	modifer.type = type;

	MRampAttribute maxDistRamp(thisMObject(), i_affect);
	modifer.ramp.set(maxDistRamp, 0, 1, 1);

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

bool FFDeformModifer::isBounded() const
{
	MObject thisNode = thisMObject();
	return true;
}

MBoundingBox FFDeformModifer::boundingBox() const
{
	MObject thisNode = thisMObject();

	double r;
	MPlug(thisMObject(), i_radius).getValue(r);

	MPoint pt0(r, r, r);
	MPoint pt1(-r, -r, -r);
	MBoundingBox box(pt1, pt0);

	//...
	return box;
}

void FFDeformModifer::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MObject thisNode = thisMObject();

	MStatus stat;
	MPlug plug(thisNode, o_modifer);
	plug = plug.elementByLogicalIndex(0, &stat);
	if( !stat) return;

	MObject val;
	stat = plug.getValue(val);
	if( !stat) return;

	MPxData* pxdata = MFnPluginData(val).data();
	if( !pxdata) return;

	FFDeformModiferData* modiferdata = (FFDeformModiferData*)pxdata;
	IFFDeformModifer& modifer = modiferdata->modifer;
	

	view.beginGL(); 

	float r = modifer.radius*2;
	switch( modifer.type)
	{
		case IFFDeformModifer::MT_SPHERE:
			drawSphere(modifer.radius*2);
			drawLine(Math::Vec3f(0), Math::Vec3f(0, -modifer.radius, 0));
			break;
		case IFFDeformModifer::MT_CYLINDER:
			drawCircleXZ(r, Math::Vec3f(0, 0.5f*r, 0));
			drawCircleXZ(r, Math::Vec3f(0, -0.5f*r, 0));
			glBegin( GL_LINES );
			//�������
//			glVertex3f(Math::Vec3f(0.05f, 0.45f, 0));
//			glVertex3f(Math::Vec3f(0.0, 0.5f, 0));
//			glVertex3f(Math::Vec3f(-0.05f, 0.45f, 0));
//			glVertex3f(Math::Vec3f(0.0, 0.5f, 0));

			// 
			glVertex3f(Math::Vec3f(0, 0.5f*r, 0));
			glVertex3f(Math::Vec3f(0, -0.5f*r, 0));
			glVertex3f(Math::Vec3f(0.5f*r, 0.5f*r, 0));
			glVertex3f(Math::Vec3f(0.5f*r, -0.5f*r, 0));
			glVertex3f(Math::Vec3f(-0.5f*r, 0.5f*r, 0));
			glVertex3f(Math::Vec3f(-0.5f*r, -0.5f*r, 0));
			glVertex3f(Math::Vec3f(0, 0.5f*r, 0.5f*r));
			glVertex3f(Math::Vec3f(0, -0.5f*r, 0.5f*r));
			glVertex3f(Math::Vec3f(0, 0.5f*r, -0.5f*r));
			glVertex3f(Math::Vec3f(0, -0.5f*r, -0.5f*r));
			glEnd();
			break;
	}
	view.endGL();
};


void* FFDeformModifer::creator()
{
	return new FFDeformModifer();
}

MStatus FFDeformModifer::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnEnumAttribute	enumAttr;
	MStatus				stat;

	try
	{
		{
			i_type = enumAttr.create ("modiferType", "mtp", 0);
			enumAttr.addField("SPHERE", IFFDeformModifer::MT_SPHERE);
			enumAttr.addField("CYLINDER", IFFDeformModifer::MT_CYLINDER);
			::addAttribute(i_type, atInput);

			i_radius = numAttr.create ("radius","rad", MFnNumericData::kDouble, 10, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(20);
			::addAttribute(i_radius, atInput);

		}
		{
			i_affect = MRampAttribute::createCurveRamp("affect", "afc");
			::addAttribute(i_affect, atInput);
		}
		{
			MObject newDataObject = MObject::kNullObj;
			o_modifer = typedAttr.create( "modifer", "mod", FFDeformModiferData::id, newDataObject);
			typedAttr.setReadable(false);
			typedAttr.setArray(true);
			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			typedAttr.setIndexMatters(false);
			stat = typedAttr.setWorldSpace(true);
			::addAttribute(o_modifer, atReadable|atWritable|atStorable|atCached|atConnectable|atHidden);

			stat = attributeAffects( i_radius, o_modifer );
			stat = attributeAffects( i_affect, o_modifer );
			stat = attributeAffects( i_type, o_modifer );
		}
		if( !MGlobal::sourceFile("AEFFDeformModiferTemplate.mel"))
		{
			displayString("error source AEFFDeformModiferTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}