#pragma once

#include <maya/MPlug.h>
#include "Math/matrixMxN.h"

bool loadTexture(
	const MPlug& src, 
	Math::matrixMxN<float>& dst, 
	int res
	);

