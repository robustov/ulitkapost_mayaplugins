#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: FFMeshGrowFromHairNode.h
//
// Dependency Graph Node: FFMeshGrowFromHair

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include <maya/MFnMesh.h> 

#include "FFGrowData.h"
#include "Math/MeshControl.h"

class FFMeshGrowFromHair : public MPxNode
{
public:
	FFMeshGrowFromHair();
	virtual ~FFMeshGrowFromHair(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	///
	virtual MStatus setDependentsDirty( const MPlug& plug, MPlugArray& plugArray);

	static void* creator();
	static MStatus initialize();

	MStatus Build_Control( const MPlug& plug, MDataBlock& data );

public:

	// render
//	static MObject texToper;
//	static MObject shaderId;
	static MObject startPositions, startNormals;

	static MObject o_grow;
	static MObject o_growId;

public:
	static MTypeId id;
	static const MString typeName;
};

