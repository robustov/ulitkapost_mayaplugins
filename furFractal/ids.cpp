#include "pre.h"

const unsigned TypeIdBase = 0xda128;

// node static ids and names

#include "furFractal.h"
MTypeId furFractal::id( TypeIdBase+0 );
const MString furFractal::typeName( "furFractalNeo" );


#include "FFSphereLocator.h"
MTypeId FFSphereLocator::id( TypeIdBase+02 );
const MString FFSphereLocator::typeName( "FFSphereNeoLocator" );

#include "FFGridData.h"
MTypeId FFGridData::id( TypeIdBase+03 );
const MString FFGridData::typeName( "FFGridNeoData" );

#include "FFControlData.h"
MTypeId FFControlData::id( TypeIdBase+04 );
const MString FFControlData::typeName( "FFControlNeoData" );

#include "FFCurveNeoLocator.h"
MTypeId FFCurveNeoLocator::id( TypeIdBase+05 );
const MString FFCurveNeoLocator::typeName( "FFCurveNeoLocator" );

#include "FFMeshLocator.h"
MTypeId FFMeshLocator::id( TypeIdBase+06 );
const MString FFMeshLocator::typeName( "FFMeshNeoLocator" );

#include "FFDrawCache.h"
MTypeId FFDrawCache::id( TypeIdBase+07 );
const MString FFDrawCache::typeName( "FFDrawNeoCache" );

#include "FFGrowData.h"
MTypeId FFGrowData::id( TypeIdBase+0x8 );
const MString FFGrowData::typeName( "FFGrowData" );

#include "FFMeshGrowFromHair.h"
MTypeId FFMeshGrowFromHair::id( TypeIdBase+0x9 );
const MString FFMeshGrowFromHair::typeName( "FFMeshGrowFromHair" );

#include "FFGrowLocator.h"
MTypeId FFGrowLocator::id( TypeIdBase+0xa );
const MString FFGrowLocator::typeName( "FFGrowLocator" );

#include "FFDeformData.h"
MTypeId FFDeformData::id( TypeIdBase+0xb );
const MString FFDeformData::typeName( "FFDeformData" );

#include "FFDeformLocator.h"
MTypeId FFDeformLocator::id( TypeIdBase+0xc );
const MString FFDeformLocator::typeName( "FFDeformLocator" );

#include "FFDeformModifer.h"
MTypeId FFDeformModifer::id( TypeIdBase+0xd );
const MString FFDeformModifer::typeName( "FFDeformModifer" );

#include "FFDeformModiferData.h"
MTypeId FFDeformModiferData::id( TypeIdBase+0xe );
const MString FFDeformModiferData::typeName( "FFDeformModiferData" );

