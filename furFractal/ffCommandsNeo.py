#usage:
# import ffCommands
# reload (ffCommands)
# ffCommands.clear("furFractal1")
#

from ctypes import *
from ctypes.wintypes import *
from _ctypes import LoadLibrary, FreeLibrary
import maya.cmds as cmd 
import maya.OpenMaya as OpenMaya

def build(operation, nodename):
 cmd.loadPlugin("furFractalNeo.mll", quiet=True);
 handle = cdll.LoadLibrary('furFractalNeo.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_int, c_char_p)
  ff_build = prototype(("ff_build", handle))
  result = ff_build(operation, nodename)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle);

def rebind(nodename):
 cmd.loadPlugin("furFractalNeo.mll", quiet=True);
 handle = cdll.LoadLibrary('furFractalNeo.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_char_p)
  ff_rebind = prototype(("ff_rebind", handle))
  result = ff_rebind(nodename)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle);

def regrow(nodename):
 cmd.loadPlugin("furFractalNeo.mll", quiet=True);
 handle = cdll.LoadLibrary('furFractalNeo.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_char_p)
  ff_regrow = prototype(("ff_regrow", handle))
  result = ff_regrow(nodename)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle);

