#include "FFGrowLocator.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFloatMatrix.h>
#include <maya/MRenderUtil.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>

#include <maya/MGlobal.h>
#include "mathNmaya/mathNmaya.h"
#include "Math/MeshGrowFromHair.h"

bool loadTexture(
	const MPlug& src, 
	Math::matrixMxN<float>& dst, 
	int res
	);

// render
//	MObject FFGrowLocator::texToper;
//	MObject FFGrowLocator::shaderId;
MObject FFGrowLocator::entrypoint;
MObject FFGrowLocator::startPositions, FFGrowLocator::startNormals;

MObject FFGrowLocator::o_grow;
MObject FFGrowLocator::o_growId;
MObject FFGrowLocator::i_sourceGrows;

FFGrowLocator::FFGrowLocator()
{
}
FFGrowLocator::~FFGrowLocator()
{
}
MStatus FFGrowLocator::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	if( plug == o_grow )
	{
		return Build_Control(plug, data);
	}
	if( plug == o_growId )
	{
		data.inputValue(o_grow, &stat );
		return MS::kSuccess;		
	}
	return MS::kUnknownParameter;
}
MStatus FFGrowLocator::setDependentsDirty( const MPlug& plug, MPlugArray& plugArray)
{
	if( plug.isDynamic())
	{
		plugArray.append( MPlug(thisMObject(), o_grow));
	}
	return MPxNode::setDependentsDirty( plug, plugArray);
}
void FFGrowLocator::LoadSource( 
	MDataBlock& data, 
	std::vector<int>& srcGrows
	)
{
	MStatus stat;
	MArrayDataHandle inputDataCache = data.inputArrayValue(i_sourceGrows, &stat);
	srcGrows.clear();
	srcGrows.reserve( inputDataCache.elementCount());
	for( unsigned i=0; i<inputDataCache.elementCount(); i++)
	{
//		if( inputDataCache.jumpToElement(i))
		if( inputDataCache.jumpToArrayElement(i))
		{
			int sg = inputDataCache.inputValue(&stat).asInt();
			srcGrows.push_back(sg);
		}
	}
}

MStatus FFGrowLocator::Build_Control( const MPlug& plug, MDataBlock& data )
{
	displayStringD( "FFGrowLocator::Build_Control");
	MStatus stat;

	MDataHandle outputData = data.outputValue(plug, &stat );

	typedef FFGrowData CLASS;
	CLASS* pData = NULL;

//	bool bNewData = false;
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<CLASS*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( CLASS::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<CLASS*>(fnDataCreator.data( &stat ));
//		bNewData = true;
		outputData.set(pData);
		MDataHandle outputData = data.outputValue(plug, &stat );
		userdata = outputData.asPluginData();
		pData = static_cast<CLASS*>(userdata);
	}

	std::string dllprocname = data.inputValue(entrypoint).asString().asChar();

	if( !pData->dllproc.isValid())
	{
		pData->dllproc.LoadFormated(dllprocname.c_str(), "furFractalNeoMath.dll");
	}
	if( pData->dllproc.isValid())
	{
		if( !pData->control)
		{
			IFFGROW_CREATOR pc = (IFFGROW_CREATOR)pData->dllproc.proc;
			pData->control = (*pc)();
		}
		if(pData->control)
		{
			IFFGrow* cc = (IFFGrow*)pData->control;
			if( cc->growid==0)
				cc->ReinitGrowId();

			LoadSource( data, cc->srcGrows);
#ifdef _DEBUG
			printf( "growid=%x\n", cc->growid);
			fflush(stdout);
#endif

			/*/
			cc->texResolution = data.inputValue(texResolution).asInt();
			if( !loadTexture(
				MPlug(thisMObject(), this->growSpeed), 
				cc->growSpeed, 
				cc->texResolution
				))
			{
				cc->growSpeed.init(1, 1);
				cc->growSpeed[0][0] = (float)data.inputValue(growSpeed).asDouble();
			}

			{
				MObject arobj = data.inputValue(startPositions).data();
				MFnPointArrayData pa(arobj);
				cc->startPositions.resize(pa.length());
				for(int i=0; i<(int)cc->startPositions.size(); i++)
					::copy(cc->startPositions[i], pa[i]);
			}
			{
				MObject arobj = data.inputValue(startNormals).data();
				MFnVectorArrayData pa(arobj);
				cc->startDirection.resize(pa.length());
				for(int i=0; i<(int)cc->startDirection.size(); i++)
					::copy(cc->startDirection[i], pa[i]);
			}
			/*/

			// render
//			cc->texToper = (float)data.inputValue(texToper).asDouble();

			OcsData ocsData(thisMObject());
			cc->ReadAttributes(&ocsData);

			MDataHandle o_growiddata = data.outputValue(o_growId);
			o_growiddata.setInt(cc->growid);
			o_growiddata.setClean();
		}
	}
//	if( bNewData)
//		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

void* FFGrowLocator::creator()
{
	return new FFGrowLocator();
}

MStatus FFGrowLocator::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		MObject grow[1];
		{
			int i=0;

			grow[i] = numAttr.create ("parameter","param", MFnNumericData::kDouble, 0, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(50);
			::addAttribute(grow[i++], atInput|atConnectable|atUnHidden);

			/*/
			grow[i] = numAttr.create ("growlevel","gle", MFnNumericData::kInt, 1, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(4);
			::addAttribute(grow[i++], atInput);

			grow[i] = numAttr.create ("seed","se", MFnNumericData::kInt, 0, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1000);
			::addAttribute(grow[i++], atInput);

			grow[i] = numAttr.create ("startVenCount","svc", MFnNumericData::kInt, 4, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(grow[i++], atInput);

			grow[i] = numAttr.create ("growlength","sle", MFnNumericData::kDouble, 10.0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(grow[i++], atInput);

			grow[i] = numAttr.create ("wigth","slej", MFnNumericData::kDouble, 1.0f, &stat);
			numAttr.setMin(0);
			numAttr.setMax(1);
			::addAttribute(grow[i++], atInput);
			/*/

			startPositions = typedAttr.create( "startPositions", "spos", MFnData::kPointArray, &stat);
			::addAttribute(startPositions, atInput|atHidden);

			startNormals = typedAttr.create( "startDirections", "sdir", MFnData::kVectorArray, &stat);
			::addAttribute(startNormals, atInput|atHidden);
		}

		{
			entrypoint = typedAttr.create( "entrypoint", "enpt", MFnData::kString, &stat);
			::addAttribute(entrypoint, atInput);
		}

		{
//			MFnPluginData fnDataCreator;
//			MObject newDataObject = fnDataCreator.create( FFGrowData::id, &stat );
			MObject newDataObject = MObject::kNullObj;
			o_grow = typedAttr.create( "grow", "gro", FFGrowData::id, newDataObject);
			::addAttribute(o_grow, atReadable|atWritable|atStorable|atCached|atConnectable|atHidden);

			o_growId = numAttr.create ("growId","grid", MFnNumericData::kInt, 0, &stat);
			::addAttribute(o_growId, atReadable|atWritable|atConnectable|atStorable|atUnHidden);

			i_sourceGrows = numAttr.create( "sourceGrows", "srgr", MFnNumericData::kInt, 0, &stat);
			numAttr.setReadable(false);
			numAttr.setArray(true);
			numAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			numAttr.setIndexMatters(false);
			::addAttribute( i_sourceGrows, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atArray );

			int s, i;
			s = sizeof( grow)/sizeof( MObject);
			for(i=0; i<s; i++)
				attributeAffects(grow[i], o_grow);

			attributeAffects(entrypoint,		o_grow);
//			attributeAffects(entrypoint,		o_growId);
			attributeAffects(startNormals,		o_grow);
//			attributeAffects(startNormals,		o_growId);
			attributeAffects(startPositions,	o_grow);
//			attributeAffects(startPositions,	o_growId);
			attributeAffects(i_sourceGrows,		o_grow);
		}

		if( !MGlobal::sourceFile("AEFFGrowLocatorTemplate.mel"))
		{
			displayString("error source AEFFGrowLocatorTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

