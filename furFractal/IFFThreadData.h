#pragma once

namespace Math
{
	// ���������� � thread
	// ��������� � IFFGrow::Start � Step
	// ������������ IFFWindow
	struct IFFThreadData
	{
		Math::Vec3i startnormal;
		std::vector< Math::GridCellId> curve;
		std::vector< float> length;				// �����
		std::vector< Math::Vec3f> uvw;			// tex coord
		std::vector< float> parentTransmition;	// ������ �������� �� ������� 

		int threadId;			// id �����
		int parentThreadId;		// ��������
		int parentThreadIdVert;	// ������� � �������� �� �������� �����
			int secondary_parentThreadId;		// ������ �������� 
			int secondary_parentThreadIdVert;	// � ��� ������� - ��� ������
		int generation;			// ���������
		int controlId;			// � ������ �������� ���������
		int growId;				// ��� ��������?
		int seed;				// seed
		float rndForDelay;		// ������ ��� delay
		bool bStartFromThis;	// ���� true � ���� IFFGrow ���������� OnNewThread

		// ������������� ��� ��������������
		std::vector< float> growtime;			// ����� �����
		std::vector< float> lengthFromEnd;		// �����
		std::vector< float> width;				// ������
		float T;								// ���������� �������� ��� ���� ����� � ��������
//		float velocityfactor;

		// ��������
		struct ChildLink
		{
			int myVert;			// ��� ������� � �������� ���������
			int childId;		// threadId
		};
		std::vector<ChildLink> childs;

		IFFThreadData()
		{
			bStartFromThis = true;
			generation = 0;
			growId = controlId = threadId = -1;
			seed = 0;
			parentThreadId = parentThreadIdVert = -1;		// ��������
			startnormal = Math::Vec3i(0, 1, 0);
			rndForDelay = rand()/(float)RAND_MAX;
			secondary_parentThreadId = secondary_parentThreadIdVert = -1;
		}
		bool isValid()
		{
			size_t sz = curve.size();
			if( length.size()!=sz)
				return false;
			if( uvw.size()!=sz)
				return false;
//			if( parentTransmition.size()!=sz)
//				return false;
			if( growtime.size()!=sz)
				return false;
			if( lengthFromEnd.size()!=sz)
				return false;
			return true;
		}

	};
}

template<class Stream> inline
Stream& operator >> (Stream& out, Math::IFFThreadData& v)
{
	unsigned char version = 6;
	out >> version;
	if( version>=0)
	{
		out >> v.curve;
		out >> v.length;

		out >> v.threadId;
		out >> v.parentThreadId;
		out >> v.parentThreadIdVert;
		out >> v.generation;
		out >> v.controlId;
		out >> v.growId;
		out >> v.seed;	

		out >> v.childs;	
	}
	if( version>=1)
	{
		out >> v.startnormal;
	}
	if( version>=2)
	{
		out >> v.T;
	}
	if( version>=3)
	{
		out >> v.uvw;
	}
	if( version>=4)
	{
		float velocityfactor=0;
		out >> velocityfactor;
	}
	if( version>=5)
	{
		out >> v.length;
		out >> v.rndForDelay;
		out >> v.secondary_parentThreadId;
		out >> v.secondary_parentThreadIdVert;
	}
	if( version>=6)
	{
		out >> v.parentTransmition;
	}

	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, Math::IFFThreadData::ChildLink& v)
{
	unsigned char version = 0;
	out >> version;
	if( version>=0)
	{
		out >> v.myVert;			// ��� ������� � �������� ���������
		out >> v.childId;		// threadId
	}
	return out;
}

