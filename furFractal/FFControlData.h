#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"
#include "Math/Grid.h"
#include "IFFWindow.h"
#include "IFFGrow.h"
#include "IFFControl.h"
#include "Util/DllProcedure.h"
#include "OcsData.h"

class FFControlData : public MPxData
{
public:
	IFFControl* control;
	// ����� ����� ��� ��������
	Util::DllProcedure dllproc;

	/*/
	Math::IFFWindow* getWindow(){if(control && control->isWindow()) return control; return NULL;};
	IFFGrow* getGrow(){if(control && control->isGrow()) return control; return NULL;};
	Math::IGridDeformer* getDeformer(){if(control && control->isDeformer()) return control; return NULL;};
	/*/

public:
	FFControlData();
	virtual ~FFControlData(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;

protected:
	void serialize(Util::Stream& stream);
};

