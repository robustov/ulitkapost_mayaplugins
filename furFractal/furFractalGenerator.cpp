#include "furFractal.h"
#include <maya/MAnimControl.h>
#include <maya/MDagPath.h>
#include <maya/MFnPluginData.h>
#include "MathNgl/MathNgl.h"
#include "MathNmaya/MathNmaya.h"
#include "FFGridData.h"
#include "Math/Spline.h"
#include "ocellaris/OcellarisExport.h"
#include "Util/FileStream.h"
#include "ocellaris/renderFileImpl.h"
#include "Util/misc_create_directory.h"

// 
// ��������� 
// 
furFractal::Generator furFractal::generator;

//! ����������� ���� � �������� 
bool furFractal::Generator::OnPrepareNode(
	cls::INode& node, 
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	MObject obj = node.getObject();

	// ������� Grid Data � ����
	std::string filename;
	filename = context->getEnvironment("RIBDIR");
	filename+="/";
	filename+=node.getName();
	filename+=".griddata";
	Util::correctFileName((char*)filename.c_str());

	Util::FileStream file;
	Util::create_directory_for_file(filename.c_str());
	if( !file.open(filename.c_str(), true))
	{
		fprintf(stderr, "Cant open %s\n", filename.c_str());
		return false;
	}

	MFnDependencyNode dn(obj);
	if( dn.typeId()!=furFractal::id)
		return false;

	// ����� gridData � ����
	furFractal* ff = (furFractal*)dn.userNode();
	MDataBlock data = ff->forceCache();
	FFGridData* ffgriddata = ff->Load_Grid(data);
	GridData& gridData = ffgriddata->data;
	Math::Box3f dirtybox = gridData.getDirtyBox(true);

	gridData.serialize(file);

	// ����� �������� � ����
	std::vector<IFFControl*> controls;
	std::vector<Util::DllProcedure*> controldlls;
	ff->LoadControls(data, controls, &controldlls);

	int ctrlcount = (int)controls.size();
	file >> ctrlcount;
	for(int i=0; i<ctrlcount; i++)
	{
		file >> *controldlls[i];
		controls[i]->serialize(file);
	}

//		std::vector<IFFControl*> ffcs;
//		ff->LoadControls(data, ffcs);

	// ��������� ��� �����
	cls::IRender* objectContext = context->objectContext(obj, this);
	objectContext->Parameter("ff::filename", cls::PS(filename));
	objectContext->Parameter("ff::dirtybox", dirtybox);

	// ������ ���� ��� ��������!
	{
		// ������� Grid Data � ����
		std::string filename;
		filename = context->getEnvironment("RIBDIR");
		filename+="/";
		filename+=node.getName();
		filename+=".proxy.ocs";
		Util::correctFileName((char*)filename.c_str());

		cls::renderFileImpl file;
		file.openForWrite(filename.c_str(), true);

		cls::P<int> rType = ocExport_GetAttrValue( obj, "rType" );
		cls::P<float> renderWidth = ocExport_GetAttrValue( obj, "renderWidth" );

		file.GlobalAttribute("global::ff_renderWidth", renderWidth);
		file.GlobalAttribute("global::ff_rType", rType);

		// ��������� ������ �������!!!
		ff->RenderProxy(&file);
	}

	return true;
}

bool furFractal::Generator::OnGeometry(
	cls::INode& node,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject obj = node.getObject();

	// ������������ ��������� ���������� � ��������
	{
		std::string filename;
		cls::IRender* objectContext = context->objectContext(obj, this);
		objectContext->GetParameter("ff::filename", filename);

		objectContext->GetParameter("ff::dirtybox", box);

		cls::P<int> rType = ocExport_GetAttrValue( obj, "rType" );

		cls::P<float> renderWidth = ocExport_GetAttrValue( obj, "renderWidth" );
		cls::P<float> T = ocExport_GetAttrValue( obj, "time" );

		cls::P<int> tesselation = ocExport_GetAttrValue( obj, "tesselation" );

		MFnDagNode dagFn( obj );
		MDagPath path;
		dagFn.getPath( path );
		Math::Matrix4f worldFFMatrix;
		::copy( worldFFMatrix, path.inclusiveMatrix() );

		render->Parameter("ff::filename", cls::PS(filename));
		render->Parameter("ff::worldFFMatrix", worldFFMatrix);

		render->Parameter("ff::renderWidth", renderWidth);
render->ParameterFromAttribute("ff::renderWidth", "global::ff_renderWidth");
render->GlobalAttribute("global::ff_renderWidth", renderWidth);

		render->Parameter("ff::tesselation", tesselation);

		render->Parameter("ff::rType", rType);
render->ParameterFromAttribute("ff::rType", "global::ff_rType");
render->GlobalAttribute("global::ff_rType", rType);

		render->Parameter("ff::Time", T);

		cls::P<Math::Matrix4f> pruningCamera = ocExport_GetAttrValue( obj, "pruningCamera" );
		cls::P<bool> pruning = ocExport_GetAttrValue( obj, "pruning" );
		cls::P<float> pruningDist0 = ocExport_GetAttrValue( obj, "pruningDistance0" );
		cls::P<float> pruningDist1 = ocExport_GetAttrValue( obj, "pruningDistance1" );
		cls::P<float> pruningDist2 = ocExport_GetAttrValue( obj, "pruningDistance2" );

		render->Parameter("ff::pruning", pruning);
		render->Parameter("ff::pruningCamera", pruningCamera);
		render->Parameter("ff::pruningDist0", pruningDist0);
		render->Parameter("ff::pruningDist1", pruningDist1);
		render->Parameter("ff::pruningDist2", pruningDist2);


		// Save Controls
		MFnDependencyNode dn(obj);
		if( dn.typeId()!=furFractal::id)
			return false;
		furFractal* ff = (furFractal*)dn.userNode();
		MDataBlock data = ff->forceCache();

		std::vector<IFFControl*> ffcs;
		std::vector<IFFGrow*> ffgs;
		std::vector<IFFDeform*> deforms;
		ff->LoadControls(data, ffcs);
		for(int c=0; c<(int)ffcs.size(); c++)
		{
			IFFControl* control = ffcs[c];

			char buf[24];
			sprintf(buf, "control%d", control->controlid);
			control->SerializeOcs(buf, render, true);

			control->getGrows(ffgs);
			control->getDeformers(deforms);
		}
		for(int g=0; g<(int)ffgs.size(); g++)
		{
			IFFGrow* grow = ffgs[g];
			char buf[24];
			sprintf(buf, "grow%d", grow->growid);
			grow->SerializeOcs(buf, render, true);
		}
		for(int d=0; d<(int)deforms.size(); d++)
		{
			IFFDeform* deform = deforms[d];
			char buf[24];
			sprintf(buf, "deform%d", deform->deformid);
			deform->SerializeOcs(buf, render, true);

			deform->UpdateDirtyBox( box);
		}

		render->Parameter("ff::dirtybox", box);

		/*/
		std::vector<Util::DllProcedure*> dllproc;
		ff->LoadControls(data, ffcs, &dllproc);
		std::vector<std::string> controlnames;
		controlnames.reserve(ffcs.size());
		for(int c=0; c<(int)ffcs.size(); c++)
		{
			char buf[24];
			sprintf(buf, "ctrl%02d", c);
			if( !ffcs[c]) continue;

			Util::DllProcedure* dll = dllproc[c];
			if(!dll || !dll->isValid())
				continue;

			render->Parameter( (std::string(buf)+"::entrypoint").c_str(), dll->GetFormated().c_str());
			ffcs[c]->SerializeOcs(buf, render, true);
			controlnames.push_back(buf);
		}
		render->Parameter("ff::controlnames", cls::PSA(controlnames));
		/*/

		render->RenderCall("furFractalNeoMath.dll@FF");
		return true;
	}
}



