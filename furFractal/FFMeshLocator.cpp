#include "FFMeshLocator.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFloatMatrix.h>
#include <maya/MRenderUtil.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>

#include <maya/MGlobal.h>
#include "mathNmaya/mathNmaya.h"
#include "loadTexture.h"

MObject FFMeshLocator::i_grows;
MObject FFMeshLocator::i_deforms;

MObject FFMeshLocator::i_window;
	MObject FFMeshLocator::i_time;
	MObject FFMeshLocator::i_windowposition;
	MObject FFMeshLocator::i_venToper;
	MObject FFMeshLocator::i_width, FFMeshLocator::i_WidthFactorJitter;
	MObject FFMeshLocator::disgrowfactor;

MObject FFMeshLocator::i_grow;
	MObject FFMeshLocator::i_mesh;
	MObject FFMeshLocator::i_level;
	MObject FFMeshLocator::invertNormals;
	MObject FFMeshLocator::i_seed;
	MObject FFMeshLocator::startVenCount;
	MObject FFMeshLocator::segLenght;
	MObject FFMeshLocator::segLenJitter;
	MObject FFMeshLocator::curl;
	MObject FFMeshLocator::curlJitter;
	MObject FFMeshLocator::startOffset;
	MObject FFMeshLocator::surfaceOffset;
	MObject FFMeshLocator::surfaceOffsetJitter;
	MObject FFMeshLocator::growFrequency;
	MObject FFMeshLocator::forkAngle; 
	MObject FFMeshLocator::forkAngleJitter;
	MObject FFMeshLocator::stopIfCross;
	MObject FFMeshLocator::startPositions;
	MObject FFMeshLocator::maxSegCount;
	MObject FFMeshLocator::growFromOther;
	MObject FFMeshLocator::growSpeed;
	MObject FFMeshLocator::texResolution;

MObject FFMeshLocator::i_deformer;
	MObject FFMeshLocator::i_xynoisemagnitude;
	MObject FFMeshLocator::i_xynoisefrequency;
	MObject FFMeshLocator::i_xynoisedimention;
	MObject FFMeshLocator::deformToper;

// render
	MObject FFMeshLocator::texToper;
	MObject FFMeshLocator::shaderId;

MObject FFMeshLocator::o_control;

FFMeshLocator::FFMeshLocator()
{
}
FFMeshLocator::~FFMeshLocator()
{
}

bool FFMeshLocator::ReadMesh(
	MeshData* pThis, 
	MFnMesh& mesh, 
	bool invertNormals
	)
{
	pThis->verts.clear();			// verts
	pThis->vertnormals.clear();	// N
	pThis->facenormals.clear();
	pThis->vertsUv.clear();
	pThis->faces.clear();			// faces
	pThis->edgenormals.clear();	// ������� �����
	pThis->openedges.clear();		// �������� �����

	MStatus stat;
	MFloatPointArray pa;
	stat = mesh.getPoints(pa, MSpace::kWorld);

	int vc = pa.length();
	pThis->verts.resize(vc);
	pThis->vertnormals.resize(vc, Math::Vec3f(0));
	pThis->vertsUv.resize(vc, Math::Vec2f(0));
	for(unsigned v=0; v<(unsigned)vc; v++)
	{
		MFloatPoint& pt = pa[v];
		::copy(pThis->verts[v], pt);
	}
	MIntArray triangleCounts, triangleVertices;
	mesh.getTriangles( triangleCounts, triangleVertices );

	int pc = mesh.numPolygons();
	std::vector<Math::Vec3f> polynorms;
	polynorms.resize(pc);
	std::map<Math::Edge32, int> edges;
	for ( int p=0, _fv=0; p<pc; p++) 
	{
		MIntArray vertices;
		MFloatVectorArray normals;
		mesh.getPolygonVertices(p, vertices);

		//*/
		Math::Vec3f normal(0);
		int tric = triangleCounts[p];
		for( int t=0; t<tric; t++)
		{
			int v0 = triangleVertices[_fv++];
			int v1 = triangleVertices[_fv++];
			int v2 = triangleVertices[_fv++];
			
			Math::Vec3f c1 = pThis->verts[v1] - pThis->verts[v0];
			Math::Vec3f c2 = pThis->verts[v2] - pThis->verts[v0];
			Math::Vec3f n = Math::cross( c1, c2).normalized();
			normal += n;
		}
		normal.normalize();
		if( invertNormals)
			normal = -normal;
		polynorms[p] = normal;

		/*/
		MVector n;
		mesh.getPolygonNormal(p, n, MSpace::kWorld);
		Math::Vec3f normal;
		::copy( normal, n);
		polynorms[p] = normal;
		//*/


		Math::Polygon32 p32;
		p32.resize( vertices.length());
		for(int v=0; v<p32.size(); v++)
		{
			int vind = vertices[v];
			p32[v] = vind;
			pThis->vertnormals[vind] += normal;

			float s, t;
			mesh.getPolygonUV(p, v, s, t);
			pThis->vertsUv[vind] = Math::Vec2f(s, t);
		}
		// edges
		for(v=0; v<p32.size(); v++)
		{
			Math::Edge32 edge(p32.cycle(v), p32.cycle(v+1));
			// edges
			if( edges.find(edge) == edges.end())
				edges[edge] = 1;
			else
				edges[edge]++;
		}
	}

	///////////////////////////////////////////////////
	// ����� �������� ����� (openedges)
	{
		std::map<Math::Edge32, int>::iterator it = edges.begin();
		for(;it != edges.end(); it++)
		{
			const Math::Edge32& edge = it->first;
			if(it->second==1)
			{
				MeshData::OpenEdge ope;
				ope.edge = edge;
				pThis->openedges.push_back(ope);
			}
		}
		for(unsigned e=0; e<pThis->openedges.size(); e++)
		{
			MeshData::OpenEdge& ope = pThis->openedges[e];
			for(unsigned e1=0; e1<pThis->openedges.size(); e1++)
			{
				if(e==e1) continue;
				int ind = pThis->openedges[e1].edge.opposite(ope.edge.v1);
				if(ind>=0)
					ope.v_1 = ind;
				ind = pThis->openedges[e1].edge.opposite(ope.edge.v2);
				if(ind>=0)
					ope.v2_ = ind;
			}
		}
	}

	// faces
	int facecount = 0;
	for(int p=0; p<(int)triangleCounts.length(); p++)
		facecount+= triangleCounts[p];
	pThis->faces.reserve(facecount);
	pThis->facenormals.reserve(facecount);
	int fv=0;
	int trvc = triangleVertices.length();
	for(p=0; p<(int)triangleCounts.length(); p++)
	{
		Math::Vec3f& normal = polynorms[p];
		int tc = triangleCounts[p];
		for( int t=0; t<tc; t++)
		{
			pThis->faces.push_back(Math::Face32());
			Math::Face32& face = pThis->faces.back();
			face.v[0] = triangleVertices[fv+0];
			face.v[1] = triangleVertices[fv+1];
			face.v[2] = triangleVertices[fv+2];
			fv+=3;
			for(int e=0; e<3; e++)
			{
				Math::Edge32 edge(face.cycle(e), face.cycle(e+1));
				if( pThis->edgenormals.find(edge) == pThis->edgenormals.end())
					pThis->edgenormals[edge] = normal;
				else
					pThis->edgenormals[edge] += normal;
			}
			pThis->facenormals.push_back(normal);
		}
	}

	// ������������� �������
	for(v=0; v<pThis->vertnormals.size(); v++)
	{
		pThis->vertnormals[v].normalize();
	}
	std::map< Math::Edge32, Math::Vec3f>::iterator it = pThis->edgenormals.begin();
	for(;it != pThis->edgenormals.end(); it++)
	{
		it->second.normalize();
	}

	return true;
}


MStatus FFMeshLocator::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	std::string plname = plug.name().asChar();

	if( plug == o_control )
	{
		return Build_Control(plug, data);
	}
	return MS::kUnknownParameter;
}
MStatus FFMeshLocator::setDependentsDirty( const MPlug& plug, MPlugArray& plugArray)
{
	if( plug.isDynamic())
	{
		plugArray.append( MPlug(thisMObject(), o_control));
	}
	return MPxNode::setDependentsDirty( plug, plugArray);
}
void FFMeshLocator::LoadGrows( 
	MDataBlock& data, 
	std::vector<IFFGrow*>& ffcs, 
	std::vector<Util::DllProcedure>& dlls, 
	int controlId
	)
{
	MStatus stat;
	MArrayDataHandle inputDataCache = data.inputArrayValue(i_grows, &stat);
	ffcs.clear();
	dlls.clear();
	ffcs.reserve( inputDataCache.elementCount());
	dlls.reserve( inputDataCache.elementCount());
	for( unsigned i=0; i<inputDataCache.elementCount(); i++)
	{
//		if( inputDataCache.jumpToElement(i))
		if( inputDataCache.jumpToArrayElement(i))
		{
			MPxData* dataCache = inputDataCache.inputValue(&stat).asPluginData();
			FFGrowData* ffwd = (FFGrowData*)dataCache;
			if( !ffwd) 
				continue;
			if( !ffwd->control)
				continue;

			dlls.push_back(ffwd->dllproc);

			IFFGrow* control = ffwd->control;
			control->controlId = controlId;

			ffcs.push_back(control);
		}
	}
}

void FFMeshLocator::LoadDeforms( 
	MDataBlock& data, 
	MeshControl* cc
	)
{
	MStatus stat;
	MArrayDataHandle inputDataCache = data.inputArrayValue(i_deforms, &stat);
	cc->deformdlls.clear();
	cc->deforms.clear();
	cc->deformdlls.reserve( inputDataCache.elementCount()+1);
	cc->deforms.reserve( inputDataCache.elementCount()+1);
	for( unsigned i=0; i<inputDataCache.elementCount(); i++)
	{
//		if( inputDataCache.jumpToElement(i))
		if( inputDataCache.jumpToArrayElement(i))
		{
			MPxData* dataCache = inputDataCache.inputValue(&stat).asPluginData();
			FFDeformData* ffwd = (FFDeformData*)dataCache;
			if( !ffwd) 
				continue;
			if( !ffwd->control)
				continue;

			cc->deformdlls.push_back(ffwd->dllproc);

//			IFFDEFORM_CREATOR pc = (IFFDEFORM_CREATOR)ffwd->dllproc.proc;
//			IFFDeform* control = (*pc)();
			IFFDeform* control = ffwd->control;
			control->control = cc;

			cc->deforms.push_back(control);
		}
	}
	cc->deformer.control = cc;
	if( !cc->deformer.deformid)
		cc->deformer.deformid = cc->controlid;

	OcsData ocsData(thisMObject());
	cc->deformer.ReadAttributes(&ocsData);

}

MStatus FFMeshLocator::Build_Control( const MPlug& plug, MDataBlock& data )
{
	displayStringD( "FFMeshLocator::Build_Control");
	MStatus stat;

	MDataHandle outputData = data.outputValue(plug, &stat );

	typedef FFControlData CLASS;
	CLASS* pData = NULL;

//	bool bNewData = false;
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<CLASS*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( CLASS::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<CLASS*>(fnDataCreator.data( &stat ));
//		bNewData = true;
		outputData.set(pData);
		MDataHandle outputData = data.outputValue(plug, &stat );
		userdata = outputData.asPluginData();
		pData = static_cast<CLASS*>(userdata);
	}

	if( !pData->dllproc.isValid())
	{
		pData->dllproc.LoadFormated("meshControl", "furFractalNeoMath.dll");
	}
	if( pData->dllproc.isValid())
	{
		if( !pData->control)
		{
			IFFCONTROL_CREATOR pc = (IFFCONTROL_CREATOR)pData->dllproc.proc;
			pData->control = (*pc)();
		}
		if(pData->control)
		{
			MeshControl* cc = (MeshControl*)pData->control;
			if( cc->controlid==0)
				cc->ReinitGrowId();

			this->LoadGrows( data, cc->grows, cc->growdlls, cc->controlid);
			this->LoadDeforms( data, cc);

			bool invertNormals = data.inputValue( this->invertNormals).asBool();
			MDataHandle dh = data.inputValue( i_mesh, &stat);
			if( !stat)
				return MS::kFailure;
			if( dh.type() != MFnData::kMesh)
				return MS::kFailure;
			MObject obj = dh.asMeshTransformed();
			if( !obj.isNull())
			{
				MFnMesh mesh(obj, &stat);
				this->ReadMesh(&cc->mesh, mesh, invertNormals);
			}

			cc->texResolution = data.inputValue(texResolution).asInt();
			if( !loadTexture(
				MPlug(thisMObject(), this->growSpeed), 
				cc->growSpeed, 
				cc->texResolution
				))
			{
				cc->growSpeed.init(1, 1);
				cc->growSpeed[0][0] = (float)data.inputValue(growSpeed).asDouble();
			}

			cc->bWindow = data.inputValue(i_window).asBool();

			cc->windowparam = (float)data.inputValue(i_windowposition).asDouble();
			cc->time = (float)data.inputValue(i_time).asDouble();
			cc->width = (float)data.inputValue(i_width).asDouble();
			cc->WidthFactorJitter = (float)data.inputValue( i_WidthFactorJitter).asDouble();
			cc->venToper= (float)data.inputValue(i_venToper).asDouble();
			cc->disgrowfactor= (float)data.inputValue(disgrowfactor).asDouble();

			cc->bGrow = data.inputValue(i_grow).asBool();
			cc->growlevel = data.inputValue( i_level, &stat ).asInt();
			cc->startSeed = data.inputValue(i_seed).asInt();
			cc->startVenCount = data.inputValue(startVenCount).asInt();
			cc->segLenght	= (float)data.inputValue(segLenght).asDouble();
			cc->segLenJitter= (float)data.inputValue(segLenJitter).asDouble();
			cc->curl		= (float)data.inputValue(curl).asDouble();
			cc->curlJitter	= (float)data.inputValue(curlJitter).asDouble();
			cc->startOffset	= (float)data.inputValue(startOffset).asDouble();
			cc->surfaceOffset	= (float)data.inputValue(surfaceOffset).asDouble();
			cc->surfaceOffsetJitter	= (float)data.inputValue(surfaceOffsetJitter).asDouble();
			cc->growFrequency	= (float)data.inputValue(growFrequency).asDouble();
			cc->forkAngle		= (float)data.inputValue(forkAngle).asDouble();
			cc->forkAngleJitter	= (float)data.inputValue(forkAngleJitter).asDouble();
			cc->stopIfCross	= (float)data.inputValue(stopIfCross).asDouble();
			cc->maxSegCount = data.inputValue(maxSegCount ).asInt();
			cc->growFromOther = data.inputValue(growFromOther).asBool();
			
			MObject arobj = data.inputValue(startPositions).data();
			MFnVectorArrayData pa(arobj);
			cc->startPositions.resize(pa.length());
			for(int i=0; i<(int)cc->startPositions.size(); i++)
				::copy(cc->startPositions[i], pa[i]);

			cc->bDeformer = data.inputValue( i_deformer).asBool();
			cc->xynoisemagnitude = (float)data.inputValue(i_xynoisemagnitude).asDouble();
			cc->xynoisefrequency = (float)data.inputValue(i_xynoisefrequency).asDouble();
			cc->xynoisedimention = (float)data.inputValue(i_xynoisedimention).asDouble();
			cc->deformToper = (float)data.inputValue(deformToper).asDouble();

			// render
			cc->texToper = (float)data.inputValue(texToper).asDouble();

			OcsData ocsData(thisMObject());
			cc->ReadAttributes(&ocsData);
		}
	}
//	if( bNewData)
//		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

void* FFMeshLocator::creator()
{
	return new FFMeshLocator();
}

MStatus FFMeshLocator::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_window = numAttr.create ("bWindow","bwin", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_window, atInput);

			i_time = numAttr.create ("time","iti", MFnNumericData::kDouble, 0.01, &stat);
			::addAttribute(i_time, atInput);

			i_windowposition = numAttr.create ("windowPosition","gwp", MFnNumericData::kDouble, 50, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(100);
			::addAttribute(i_windowposition, atInput|atKeyable);

			i_width = numAttr.create ("width","wi", MFnNumericData::kDouble, 1, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(2);
			::addAttribute(i_width, atInput);

			i_WidthFactorJitter = numAttr.create ("widthFactorJitter","wifj", MFnNumericData::kDouble, 0.2, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(i_WidthFactorJitter, atInput);

			i_venToper = numAttr.create ("venToper","vent", MFnNumericData::kDouble, 10, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(i_venToper, atInput);

			disgrowfactor = numAttr.create ("disgrowfactor","digf", MFnNumericData::kDouble, 10, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(disgrowfactor, atInput);
			
		}
		MObject maxGeneration;
		MObject grow[2];
		{
			i_grow = numAttr.create ("bGrow","bgr", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_grow, atInput);

			i_mesh = typedAttr.create( "mesh", "me", MFnData::kMesh);
			typedAttr.setReadable(false);
			::addAttribute(i_mesh, atInput|atHidden );

			i_level = numAttr.create ("level","gle", MFnNumericData::kInt, 1, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(4);
			::addAttribute(i_level, atInput);

			i_seed = numAttr.create ("seed","se", MFnNumericData::kInt, 0, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1000);
			::addAttribute(i_seed, atInput);

			maxGeneration = numAttr.create ("maxGeneration","mg", MFnNumericData::kInt, 5, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(1000);
			::addAttribute(maxGeneration, atInput);

			startVenCount = numAttr.create ("startVenCount","svc", MFnNumericData::kInt, 4, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(startVenCount, atInput);

			segLenght = numAttr.create ("segLenght","sle", MFnNumericData::kDouble, 10.0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(segLenght, atInput);

			segLenJitter = numAttr.create ("segLenghtJitter","slej", MFnNumericData::kDouble, 0.8, &stat);
			numAttr.setMin(0);
			numAttr.setMax(1);
			::addAttribute(segLenJitter, atInput);

			curl = numAttr.create ("curl","cu", MFnNumericData::kDouble, 10, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(90);
			::addAttribute(curl, atInput);

			curlJitter = numAttr.create ("curlJitter","cuj", MFnNumericData::kDouble, 0.5, &stat);
			numAttr.setMin(0);
			numAttr.setMax(1);
			::addAttribute(curlJitter, atInput);

			startOffset = numAttr.create ("startOffset","stof", MFnNumericData::kDouble, 0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(2);
			::addAttribute(startOffset, atInput);

			surfaceOffset = numAttr.create ("surfaceOffset","suof", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(2);
			::addAttribute(surfaceOffset, atInput);

			surfaceOffsetJitter = numAttr.create ("surfaceOffsetJitter","sfj", MFnNumericData::kDouble, 0.9, &stat);
			numAttr.setMin(0);
			numAttr.setMax(1);
			::addAttribute(surfaceOffsetJitter, atInput);

			growFrequency = numAttr.create ("growFrequency","gfr", MFnNumericData::kDouble, 0.1, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(growFrequency, atInput);

			forkAngle = numAttr.create ("forkAngle","fan", MFnNumericData::kDouble, 45, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(90);
			::addAttribute(forkAngle, atInput);

			forkAngleJitter = numAttr.create ("forkAngleJitter","faj", MFnNumericData::kDouble, 0.5, &stat);
			numAttr.setMin(0);
			numAttr.setMax(1);
			::addAttribute(forkAngleJitter, atInput);

			stopIfCross = numAttr.create ("stopIfCross","sic", MFnNumericData::kDouble, 0.0, &stat);
			numAttr.setMin(0);
			numAttr.setMax(1);
			::addAttribute(stopIfCross, atInput);

			startPositions = typedAttr.create( "startPositions", "spos", MFnData::kVectorArray, &stat);
			::addAttribute(startPositions, atInput|atHidden);

			maxSegCount = numAttr.create ("maxSegCount","msc", MFnNumericData::kInt, 2000, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10000);
			::addAttribute(maxSegCount, atInput);
			
			growFromOther = numAttr.create ("growFromOther","gfo", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(growFromOther, atInput);

			invertNormals = numAttr.create ("invertNormals","ino", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(invertNormals, atInput);

			texResolution = numAttr.create ("texResolution","tere", MFnNumericData::kInt, 128, &stat);
			::addAttribute(texResolution, atInput);

			growSpeed = numAttr.create ("growSpeed","grs", MFnNumericData::kDouble, 1.0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(growSpeed, atInput);

			int i=0;
			grow[i] = numAttr.create ("growFromHairStartPhaseJitter","gfhspj", MFnNumericData::kDouble, 0.0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(grow[i++], atInput);
			
			grow[i] = numAttr.create ("filletRadius","fira", MFnNumericData::kDouble, 2.0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(5);
			::addAttribute(grow[i++], atInput);
		}
		MObject subgrows[9];
		{
			int i=0;
			subgrows[i] = numAttr.create ("curl2","crl2", MFnNumericData::kDouble, 10.0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(30);
			::addAttribute(subgrows[i++], atInput);

			subgrows[i] = numAttr.create ("curl2jitter","crl2j", MFnNumericData::kDouble, 0.0, &stat);
			numAttr.setMin(0);
			numAttr.setMax(1);
			::addAttribute(subgrows[i++], atInput);

			subgrows[i] = numAttr.create ("forkAngle2","fa2", MFnNumericData::kDouble, 90.0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(180);
			::addAttribute(subgrows[i++], atInput);

			subgrows[i] = numAttr.create ("forkAngle2jitter","fa2j", MFnNumericData::kDouble, 0.0, &stat);
			numAttr.setMin(0);
			numAttr.setMax(1);
			::addAttribute(subgrows[i++], atInput);

			subgrows[i] = numAttr.create ("length2","le2", MFnNumericData::kDouble, 2.0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(subgrows[i++], atInput);

			subgrows[i] = numAttr.create ("length2jitter","le2j", MFnNumericData::kDouble, 0.0, &stat);
			numAttr.setMin(0);
			numAttr.setMax(1);
			::addAttribute(subgrows[i++], atInput);

			subgrows[i] = numAttr.create ("density2","de2", MFnNumericData::kDouble, 0.0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(subgrows[i++], atInput);

			subgrows[i] = numAttr.create ("subgrowPhaseOffset","sgpo", MFnNumericData::kDouble, 1.0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(2);
			::addAttribute(subgrows[i++], atInput);

			subgrows[i] = numAttr.create ("maxGeneration2","mg2", MFnNumericData::kInt, 2, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(subgrows[i], atInput);
		}

		MObject xynoisemagnitudemin;

		{
			i_deformer = numAttr.create ("bDeformer","bde", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_deformer, atInput);

			i_xynoisemagnitude = numAttr.create ("xyNoiseMagnitude","xynm", MFnNumericData::kDouble, 1, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_xynoisemagnitude, atInput);

			xynoisemagnitudemin = numAttr.create ("xynoisemagnitudemin","xynmm", MFnNumericData::kDouble, 1, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(xynoisemagnitudemin, atInput);

			i_xynoisefrequency = numAttr.create ("xyNoiseFrequency","xynf", MFnNumericData::kDouble, 0.33, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_xynoisefrequency, atInput);

			i_xynoisedimention = numAttr.create ("xyNoiseDimention","xynd", MFnNumericData::kDouble, 5, &stat);
			numAttr.setMin(0.0001);
			numAttr.setSoftMax(2);
			::addAttribute(i_xynoisedimention, atInput);
			
			deformToper = numAttr.create ("deformToper","deto", MFnNumericData::kDouble, 10, &stat);
			numAttr.setMin(0.0001);
			numAttr.setSoftMax(2);
			::addAttribute(deformToper, atInput);
		}
		{
			texToper = numAttr.create ("texToper","teto", MFnNumericData::kDouble, 10, &stat);
			numAttr.setMin(0.0001);
			numAttr.setSoftMax(2);
			::addAttribute(texToper, atInput);

			shaderId = numAttr.create ("shaderId","shid", MFnNumericData::kInt, 0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(4);
			::addAttribute(shaderId, atInput);
		}
		{
			i_grows = typedAttr.create( "grows", "igrs", FFGrowData::id);
			typedAttr.setReadable(false);
			typedAttr.setArray(true);
			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			typedAttr.setIndexMatters(false);
			::addAttribute(i_grows, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray );
		}
		{
			i_deforms = typedAttr.create( "deforms", "idfrms", FFDeformData::id);
			typedAttr.setReadable(false);
			typedAttr.setArray(true);
			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			typedAttr.setIndexMatters(false);
			::addAttribute(i_deforms, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray );
		}

		{
//			MFnPluginData fnDataCreator;
//			MObject newDataObject = fnDataCreator.create( FFControlData::id, &stat );
			MObject newDataObject = MObject::kNullObj;
			o_control = typedAttr.create( "control", "con", FFControlData::id, newDataObject);
			::addAttribute(o_control, atReadable|atWritable|atStorable|atCached|atConnectable|atHidden);

			attributeAffects(i_grows,				o_control);
			attributeAffects(i_deforms,				o_control);

			attributeAffects(i_window,				o_control);
			attributeAffects(i_time,				o_control);
			attributeAffects(i_windowposition,		o_control);
			attributeAffects(i_venToper,			o_control);
			attributeAffects(disgrowfactor,			o_control);
			attributeAffects(i_width,				o_control);
			attributeAffects(i_WidthFactorJitter,	o_control);

			attributeAffects(i_grow,				o_control);
			attributeAffects(i_mesh,				o_control);
			attributeAffects(i_level,				o_control);
			attributeAffects(invertNormals,			o_control);
			attributeAffects(i_seed,				o_control);
			attributeAffects(maxGeneration,			o_control);
			attributeAffects(startVenCount,			o_control);
			attributeAffects(segLenght,				o_control);
			attributeAffects(segLenJitter,			o_control);
			attributeAffects(curl,					o_control);
			attributeAffects(curlJitter,			o_control);
			attributeAffects(startOffset,			o_control);
			attributeAffects(surfaceOffset,			o_control);
			attributeAffects(surfaceOffsetJitter,	o_control);
			attributeAffects(growFrequency,			o_control);
			attributeAffects(forkAngle,				o_control);
			attributeAffects(forkAngleJitter,		o_control);
			attributeAffects(stopIfCross,			o_control);
			attributeAffects(startPositions,		o_control);
			attributeAffects(maxSegCount,			o_control);
			attributeAffects(growFromOther,			o_control);
			attributeAffects(texResolution,			o_control);
			attributeAffects(growSpeed,				o_control);

			int s, i;

			s = sizeof( grow)/sizeof( MObject);
			for(i=0; i<s; i++)
				attributeAffects(grow[i], o_control);

			s = sizeof( subgrows)/sizeof( MObject);
			for(i=0; i<s; i++)
				attributeAffects(subgrows[i], o_control);

			attributeAffects(i_deformer,			o_control);
			attributeAffects(i_xynoisemagnitude,	o_control);
			attributeAffects(i_xynoisefrequency,	o_control);
			attributeAffects(i_xynoisedimention,	o_control);
			attributeAffects(xynoisemagnitudemin,	o_control);
			attributeAffects(deformToper,			o_control);

			attributeAffects(texToper,				o_control);
			attributeAffects(shaderId, o_control);
		}

		if( !MGlobal::sourceFile("AEFFMeshNeoLocatorTemplate.mel"))
		{
			displayString("error source AEFFMeshNeoLocatorTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

