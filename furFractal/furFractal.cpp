//
// Copyright (C) 
// File: furFractalCmd.cpp
// MEL Command: furFractal

#include "furFractal.h"
#include <maya/MAnimControl.h>
#include <maya/MDagPath.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnMeshData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnNumericData.h>
#include <maya/MFnMatrixData.h>
#include <maya/MFileIO.h>
#include "Util/misc_create_directory.h"

#include "MathNgl/MathNgl.h"
#include "MathNmaya/MathNmaya.h"
#include "MathNmaya/MayaProgress.h" 
#include "FFGridData.h"
#include "FFControlData.h"
#include "FFDeformData.h"
#include "ocellaris/renderCacheImpl.h"
#include "Util/evaltime.h"


float rnd()
{
	return rand()/(float)RAND_MAX;
}

/*/
struct Point_Deformer : public Math::IGridDeformer
{
	Math::Matrix4f base_inv;
	Math::Matrix4f current;
	virtual Math::Vec3f deform(const Math::Vec3f& src, const Math::Vec3f& bindmapping)
	{
		Math::Vec3f v = base_inv*src;
		v = current*v;
		return v;
	}
};
/*/

MObject furFractal::i_maxStep[2];
MObject furFractal::i_T;
MObject furFractal::o_output;
MObject furFractal::o_drawcache;
MObject furFractal::o_mesh;

MObject furFractal::i_widthScale;

MObject furFractal::i_controls;
MObject furFractal::i_deforms;

MObject furFractal::i_rebuildStartFrame;

MObject furFractal::i_viewGridDots;
MObject furFractal::i_viewGrid;
MObject furFractal::i_viewGridLevel;
MObject furFractal::i_viewGridPoint;
MObject furFractal::i_drawBox;
MObject furFractal::i_color;
MObject furFractal::i_drawWidthCnt;
MObject furFractal::i_texType;
MObject furFractal::i_dumpThread;

MObject furFractal::i_bPruning;
MObject furFractal::i_pruningCamera;
MObject furFractal::i_pruningDistance[3];

MObject furFractal::i_dimention;

MObject furFractal::i_renderType;
MObject furFractal::i_renderWidth;
MObject furFractal::i_tesselation;

// ����������
MObject furFractal::o_venCount, furFractal::o_segCount, furFractal::o_estimationTime;


furFractal::furFractal()
{
}
furFractal::~furFractal()
{
}

MStatus furFractal::compute( const MPlug& plug, MDataBlock& data )
{
	Util::FunEvalTimeCounter fetc;
	{
		MStatus stat;

		if( plug == o_output )
		{
			{
				Util::FunEvalTime fet(fetc);
				stat = Build_Grid(plug, data);
			}
			displayStringD("furFractal::compute=%f", fetc.sumtime);
			return stat;
		}
		if( plug == o_mesh )
		{
			{
				Util::FunEvalTime fet(fetc);
				stat = Build_OutMesh(plug, data);
			}
			displayStringD("furFractal::compute=%f", fetc.sumtime);
			return stat;
		}
		if( plug == o_drawcache )
		{
			{
				Util::FunEvalTime fet(fetc);
				stat = Build_DrawCache(plug, data);
			}
			displayStringD("furFractal::compute=%f", fetc.sumtime);
			return stat;
		}
	}
	

	return MS::kUnknownParameter;
}
// ����� ���� ��������� ��� �����
MStatus furFractal::shouldSave( const MPlug& plug, bool& result )
{
	if( plug==o_output)
	{
		displayStringD("HairControlsAnim::shouldSave %s", plug.name().asChar());

		MDataBlock data = forceCache();
		FFGridData* ffgriddata = Load_Grid(data);

		std::string filename = MFileIO::currentFile().asChar();
		std::string mbname = Util::extract_filename(filename.c_str());
		filename = Util::extract_foldername(filename.c_str());
		filename += ".ffCache//";
		filename += mbname;
		filename += ".";
		std::string dgNodeName = MFnDependencyNode(thisMObject()).name().asChar();
		Util::correctFileName( (char*)dgNodeName.c_str());
		filename += dgNodeName;
		filename += ".ff";

		ffgriddata->filename = filename;

		result = true;
		return MS::kSuccess;
	}
	return MPxNode::shouldSave( plug, result);
}

MStatus furFractal::Build_Grid(
	const MPlug& plug, MDataBlock& data)
{
	displayStringD( "furFractal::Build_Grid");
	MStatus stat;

	typedef FFGridData CLASS;
	CLASS* pData = NULL;

	MDataHandle outputData = data.outputValue(plug);
	bool bNewData = false;
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<CLASS*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( CLASS::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<CLASS*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}
	pData->bModify = true;

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}
MStatus furFractal::Build_DrawCache(
	const MPlug& plug, MDataBlock& data)
{
//	displayStringD( "furFractal::Build_DrawCache");
	MStatus stat;

	typedef FFDrawCache CLASS;
	CLASS* pData = NULL;

	MDataHandle outputData = data.outputValue(plug);
	bool bNewData = false;
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<CLASS*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( CLASS::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<CLASS*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	FFGridData* pFFGridData = Load_Grid(data);
	GridData* gridData = &pFFGridData->data;

	std::vector<IFFDeform*> deforms;
	LoadDeformers( data, deforms);
	std::vector<Math::IFFWindow*> ffws;
//	LoadWindows(data, ffds);
	std::vector<IFFGrow*> ffgs;
	std::vector<IFFControl*> ctrls;
	LoadGrows(data, ffgs, ctrls);

	Math::Matrix4f worldpos;
	{
		MPlug plugwms(thisMObject(), this->worldMatrix);
		MPlug plugwm = plugwms.elementByLogicalIndex( 0 );

		MObject mval;
		plugwm.getValue(mval);
		MFnMatrixData se(mval);
		MMatrix m = se.matrix();
		::copy(worldpos, m);
	}
	double widthScaleValue = data.inputValue(i_widthScale).asDouble();

	bool viewGrid = data.inputValue(i_viewGrid).asBool();
	bool drawBox  = data.inputValue(i_drawBox).asBool();
	float T = (float)data.inputValue( i_T, &stat ).asDouble();
	int drawWidthCnt = data.inputValue( i_drawWidthCnt).asInt();
	int texType = data.inputValue( i_texType).asInt();
	int dumpThread = data.inputValue( i_dumpThread).asInt();
	
	float renderWidth = (float)data.inputValue(i_renderWidth).asDouble();
	if( drawWidthCnt) widthScaleValue = renderWidth;

	pData->clear();
	gridData->setUpTransform(worldpos);

	MObject val = data.inputValue( i_pruningCamera).data();
	MFnMatrixData md(val);
	Math::Matrix4f pruningCamera = Math::Matrix4f::id;
	::copy( pruningCamera, md.matrix());

	float pruningDist[3];
	pruningDist[0] = (float)data.inputValue( i_pruningDistance[0]).asDouble();
	pruningDist[1] = (float)data.inputValue( i_pruningDistance[1]).asDouble();
	pruningDist[2] = (float)data.inputValue( i_pruningDistance[2]).asDouble();
	
	try
	{
		if( !viewGrid)
		{
			pData->Build(gridData, T, deforms, ffgs, (float)widthScaleValue, texType, drawWidthCnt, dumpThread, pruningCamera, pruningDist);
		}
	}
	catch(...)
	{
		displayStringError("Exception in furFractal::Build_DrawCache");
	}

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

// ��������� ������ ���������
void furFractal::RenderProxy(cls::IRender* render)
{
	MDataBlock data = this->forceCache();
	FFGridData* pFFGridData = Load_Grid(data);
	GridData* gridData = &pFFGridData->data;

	std::vector<IFFDeform*> deforms;
	LoadDeformers( data, deforms);
	std::vector<Math::IFFWindow*> ffds;
	LoadWindows(data, ffds);

	Math::Matrix4f worldm;
	{
		MPlug plugwm(thisMObject(), this->worldMatrix);
		MObject mval;
		plugwm.getValue(mval);
		MFnMatrixData se(mval);
		MMatrix m = se.matrix();
		::copy(worldm, m);
	}

	GridData* pThis = gridData;

	pThis->grid.timestamp++;
	pThis->grid.setUpDeformers(pThis, worldm, (Math::IGridDeformer**)&deforms[0], (int)deforms.size());

	int hilevel = 10000;
	int edgecount = 0;
	std::map<GridData::Edge, GridData::EdgeData>::iterator it = pThis->alives.begin();
	for(;it != pThis->alives.end(); it++)
	{
		const GridData::Edge& edge = it->first;
		const GridData::EdgeData& edgedata = it->second;
		if( edge.level() < hilevel)
		{
			hilevel = edge.level();
			edgecount = 0;
		}
		if( edge.level() == hilevel)
			edgecount++;
	}

	cls::PA<Math::Vec3f> P(cls::PI_VERTEX);
	cls::PA<int> nverts(cls::PI_PRIMITIVE);
	P.reserve(edgecount*2);
	nverts.reserve(edgecount);

	Math::Box3f scenebox;
	it = pThis->alives.begin();
	for(;it != pThis->alives.end(); it++)
	{
		const GridData::Edge& edge = it->first;
		const GridData::EdgeData& edgedata = it->second;

		if( hilevel!=edge.level())
			continue;
		
		Math::Vec3f v1 = pThis->grid.getCellPos( pThis->grid.addCell(edge.v1()));
		Math::Vec3f v2 = pThis->grid.getCellPos( pThis->grid.addCell(edge.v2()));

		P.push_back(v1);
		P.push_back(v2);
		nverts.push_back(2);
		scenebox.insert(v1);
		scenebox.insert(v2);
	}

	pThis->grid.setUpDeformers(NULL, Math::Matrix4f(1), NULL, 0);


	render->Attribute("file::box", scenebox);
	render->PushAttributes();
	render->Parameter("#width", 1);
	render->Parameter("#curves::nverts", nverts);
	render->Parameter("P", P);
	render->Parameter("#curves::interpolation", "linear");
	render->Parameter("#curves::wrap", "nonperiodic");
	render->SystemCall(cls::SC_CURVES);
	render->PopAttributes();
}


MStatus furFractal::Build_OutMesh(
	const MPlug& plug, MDataBlock& data)
{
	MStatus status;
	MFnMeshData dataCreator;
	MDataHandle outputHandle = data.outputValue(plug, &status);

	MObject newOutputData = dataCreator.create(&status);

	FFGridData* ffGridData = Load_Grid(data);
	if( !ffGridData) return MS::kFailure;
	GridData& griddata = ffGridData->data;

	std::vector<Math::IFFWindow*> ffws;
	std::vector<IFFGrow*> ffgs;
	std::vector<IFFControl*> ffgrctrls;
	std::vector<IFFDeform*> ffdeforms;
	std::vector<IFFDeform*> ffds;

	this->LoadWindows( data, ffws);
	this->LoadGrows( data, ffgs, ffgrctrls);
	this->LoadDeformers( data, ffdeforms);

	MFnDagNode dagFn( thisMObject() );
	MDagPath path;
	dagFn.getPath( path );
	Math::Matrix4f worldFFMatrix;
	::copy( worldFFMatrix, path.inclusiveMatrix() );

	float renderWidth = (float)data.inputValue(i_renderWidth).asDouble();
	int tesselation = data.inputValue(i_tesselation).asInt();

   	griddata.grid.timestamp=0;
	griddata.grid.setUpDeformers( &griddata, worldFFMatrix, (Math::IGridDeformer**)&ffdeforms[0], (int)ffdeforms.size() );

	cls::renderCacheImpl<> cache;
	try
	{
		if( !griddata.RenderChaines(worldFFMatrix, ffws, ffgs, ffds, 2, renderWidth, tesselation, &cache, true, false))
		{
			griddata.grid.setUpDeformers( NULL, Math::Matrix4f::id, NULL, 0 );
			return MS::kSuccess;
		}
	}
	catch(...)
	{
		griddata.grid.setUpDeformers( NULL, Math::Matrix4f::id, NULL, 0 );
		displayStringError("Exception in furFractal::Build_OutMesh");
		return MS::kSuccess;
	}

	griddata.grid.setUpDeformers( NULL, Math::Matrix4f::id, NULL, 0 );

	cls::PA<int> nverts, verts;
	cls::PA<Math::Vec3f> P, N;
	cls::PA<float> s, t, u_toper, v_toper, u_length, v_length, u_normallength, u_generation, u_width;

	cache.GetParameter("P", P );
	cache.GetParameter("N", N );
	cache.GetParameter("s", s );
	cache.GetParameter("t", t );
	cache.GetParameter("mesh::nverts", nverts);
	cache.GetParameter("mesh::verts", verts);
	cache.GetParameter("u_toper", u_toper );
	cache.GetParameter("v_toper", v_toper );
	cache.GetParameter("u_length", u_length );
	cache.GetParameter("v_length", v_length );
	cache.GetParameter("v_normallength", u_normallength );
	cache.GetParameter("v_generation", u_generation);
	cache.GetParameter("v_width", u_width);

	MPointArray vertexArray(P.size());
	MIntArray polygonCounts(nverts.size()), polygonConnects(verts.size()), uvFaces(verts.size());
	MFloatArray uArray(s.size()), vArray(t.size()), u_t(u_toper.size()), v_t(v_toper.size()), u_l(u_length.size()), v_l(v_length.size());
	MFloatArray u_nl( u_normallength.size());
	int i;
	for(i=0; i<P.size(); i++)
		::copy( vertexArray[i], P[i]);
	for(i=0; i<nverts.size(); i++)
		polygonCounts[i] = nverts[i];
	for(i=0; i<verts.size(); i++)
	{
		polygonConnects[i] = verts[i];
		uvFaces[i]=i;
	}
	for(i=0; i<s.size(); i++)
		uArray[i] = s[i];
	for(i=0; i<t.size(); i++)
		vArray[i] = t[i];
	for(i=0; i<u_toper.size(); i++)
		u_t[i] = u_toper[i];
	for(i=0; i<v_toper.size(); i++)
		v_t[i] = v_toper[i];
	for(i=0; i<u_length.size(); i++)
		u_l[i] = u_length[i];
	for(i=0; i<v_length.size(); i++)
		v_l[i] = v_length[i];
	for(i=0; i<u_normallength.size(); i++)
		u_nl[i] = u_normallength[i];

	// ���������� cache � mesh
	MFnMesh	meshFn;
	MObject newMesh = meshFn.create(
		vertexArray.length(), polygonCounts.length(), vertexArray,
		polygonCounts, polygonConnects, 
		newOutputData, &status);
	if( newMesh.isNull())
		return MS::kSuccess;

	{
		MString uvname = "st";
		meshFn.createUVSetDataMeshWithName(uvname);
		meshFn.setUVs(uArray, vArray, &uvname);
		meshFn.assignUVs(polygonCounts, uvFaces, &uvname);
	}
	{
		MString uvname = "toper";
		meshFn.createUVSetDataMeshWithName(uvname);
		meshFn.setUVs(u_t, v_t, &uvname);
		meshFn.assignUVs(polygonCounts, uvFaces, &uvname);
	}
	if(!v_length.empty() && !u_length.empty())
	{
		MString uvname = "length";
		meshFn.createUVSetDataMeshWithName(uvname);
		meshFn.setUVs(u_l, v_l, &uvname);
		meshFn.assignUVs(polygonCounts, uvFaces, &uvname);
	}
	if(!s.empty() && !u_normallength.empty())
	{
		MString uvname = "normallength";
		meshFn.createUVSetDataMeshWithName(uvname);
		meshFn.setUVs(u_t, u_nl, &uvname);
		meshFn.assignUVs(polygonCounts, uvFaces, &uvname);

	}
	{
		MFloatArray u( (unsigned int)u_generation.size(), 0);
		for(i=0; i<u_generation.size(); i++)
			u[i] = u_generation[i];
		MFloatArray v( (unsigned int)u_width.size(), 0);
		for(i=0; i<u_width.size(); i++)
			v[i] = u_width[i];

		MString uvname = "gw";
		meshFn.createUVSetDataMeshWithName(uvname);
		meshFn.setUVs(u, v, &uvname);
		meshFn.assignUVs(polygonCounts, uvFaces, &uvname);
		{
			MString uvname = "map1";
			meshFn.createUVSetDataMeshWithName(uvname);
			meshFn.setUVs(u, v, &uvname);
			meshFn.assignUVs(polygonCounts, uvFaces, &uvname);
		}
	}
	

	outputHandle.set(newOutputData);
	data.setClean( plug );
	return MS::kSuccess;
}


// ����������� �����������
bool furFractal::Rebuild(int operation)
{
	bool bInit = operation==1;

	MTime currenttime = MAnimControl::currentTime();

	MStatus stat;
	MDataBlock data = this->forceCache();

	try
	{
		MTime startTime = data.inputValue( i_rebuildStartFrame).asTime();
		MAnimControl::setCurrentTime( startTime);


	if( bInit)
	{
		MPlug plug(thisMObject(), o_output);
		if( plug.isNull())
			return 0;

		plug.setValue(MObject::kNullObj);
	}


	FFGridData* ffgridData = this->Load_Grid(data);
	GridData* gridData = &ffgridData->data;

	MDagPath path;
	MDagPath::getAPathTo(thisMObject(), path);
	Math::Matrix4f worldm;
	::copy( worldm, path.inclusiveMatrix());

	float T = (float)data.inputValue( i_T, &stat ).asDouble();
	int maxStep = data.inputValue( i_maxStep[0], &stat ).asInt();
	int maxStepRefresh = data.inputValue( i_maxStep[1], &stat ).asInt();
	float dim = (float)data.inputValue( i_dimention).asDouble();

	std::vector<IFFGrow*> ffgr;
	std::vector<IFFControl*> ctrls;
	LoadGrows(data, ffgr, ctrls);

	std::vector<IFFDeform*> deforms;
	LoadDeformers(data, deforms);

	std::vector<IFFControl*> allcontrols;
	LoadControls(data, allcontrols);

if( operation==2)
	return true;

	MayaProgress mp("furFractal", "Rebuild...");

	Util::FunEvalTimeCounter fetc;

	printf("startFF simulation\n");

	for( int d=0; d<(int)deforms.size(); d++)
	{
		deforms[d]->clear();
	}
	
	int dumpThread = data.inputValue( i_dumpThread).asInt();

	ffgridData->data.dumpThread = dumpThread;
//	gridData->grid.timestamp++;
	if( bInit)
	{
		Util::FunEvalTime fet(fetc);
		ffgridData->init(0, worldm, allcontrols, ffgr, ctrls, dim);
	}

	ffgridData->data.dumpThread = dumpThread;
	for( int i=1; gridData->lastStep<maxStep; i++)
	{
		gridData->grid.timestamp++;
		
		char text[256];
		sprintf(text, "Rebuild: step %04d vessels=%05d", gridData->lastStep, gridData->alives.size());

		bool res = mp.OnProgress(gridData->lastStep/(float)maxStep, text);
		if( !res) break;

//		displayStringD("Step %f", pData->lastTime);
		gridData->grid.timestamp = -1;
		{
			Util::FunEvalTime fet(fetc);
			bool bAdded = ffgridData->Step(
				worldm, 
				allcontrols, 
				ffgr, 
				ctrls, 
				0
				);
			if( !bAdded)
				break;	
		}
		gridData->lastStep++;

		if( (i%maxStepRefresh) == 0)
		{
			Build_DrawCache(MPlug(thisMObject(), o_drawcache), data);
			MGlobal::executeCommand("refresh");
		}
		fflush(stdout);
		fflush(stderr);
	}

	{
		mp.OnProgress(1, "Final");
		Util::FunEvalTime fet(fetc);
		ffgridData->GrowStop(
			worldm, 
			ffgr,
			ctrls 
			);
	}

	if( !gridData->grid.TestGrid())
	{
		displayStringError("Grid is NAN!!!");
	}


	int venCount = gridData->alives.size();
	int segCount = gridData->threads.size();
	double estimationTime = fetc.sumtime;
	
	ffgridData->bModify = true;

	MPlug(thisMObject(), o_venCount).setValue(venCount);
	MPlug(thisMObject(), o_segCount).setValue(segCount);
	MPlug(thisMObject(), o_estimationTime).setValue(estimationTime);

	fflush(stdout);
	fflush(stderr);

	Build_DrawCache(MPlug(thisMObject(), o_drawcache), data);

	}
	catch(...)
	{
		displayStringError("Exception in furFractal::Rebuild");
	}
	MAnimControl::setCurrentTime( currenttime);

	return true;
}

FFGridData* furFractal::Load_Grid(
	MDataBlock& data)
{
	typedef FFGridData CLASS;
	MObject i_attribute = o_output;

	MStatus stat;
	MDataHandle inputData = data.inputValue(i_attribute, &stat);
	MPxData* pxdata = inputData.asPluginData();
	CLASS* hgh = (CLASS*)pxdata;
	if( !hgh)
	{
		Build_Grid( MPlug(thisMObject(), i_attribute), data);
		MDataHandle inputData = data.inputValue(i_attribute, &stat);
		MPxData* pxdata = inputData.asPluginData();
		CLASS* hgh = (CLASS*)pxdata;
		return hgh;
	}
	return hgh;
}
FFDrawCache* furFractal::Load_DrawCache(
	MDataBlock& data)
{
	typedef FFDrawCache CLASS;
	MObject i_attribute = o_drawcache;

	MStatus stat;
	MDataHandle inputData = data.inputValue(i_attribute, &stat);
	MPxData* pxdata = inputData.asPluginData();
	CLASS* hgh = (CLASS*)pxdata;
	if( !hgh)
	{
		Build_DrawCache( MPlug(thisMObject(), i_attribute), data);
		MDataHandle inputData = data.inputValue(i_attribute, &stat);
		MPxData* pxdata = inputData.asPluginData();
		CLASS* hgh = (CLASS*)pxdata;
		return hgh;
	}
	return hgh;
}




bool furFractal::isBounded() const
{
	MObject thisNode = thisMObject();
	return false;
}

MBoundingBox furFractal::boundingBox() const
{
	MObject thisNode = thisMObject();
	furFractal* pThis = (furFractal*)this;

	MBoundingBox box;

	Util::FunEvalTimeCounter fetc;
	{
		Util::FunEvalTime fet(fetc);

		MStatus stat;
		MDataBlock data = pThis->forceCache();
		FFGridData* ffgridData = pThis->Load_Grid(data);
		GridData* gridData = &ffgridData->data;
		Math::Box3f dirtybox = gridData->getDirtyBox();

//		Math::Box3f dirtybox(100);
		::copy(box, dirtybox);

	}
	displayStringD("boundingBox=%f", fetc.sumtime);

	return box;
}

void furFractal::LoadControls( 
	MDataBlock& data, 
	std::vector<IFFControl*>& ffcs, 
	std::vector<Util::DllProcedure*>* dlls
	)
{
	MStatus stat;
	MArrayDataHandle inputDataCache = data.inputArrayValue(i_controls, &stat);
	ffcs.reserve( inputDataCache.elementCount());
	if( dlls)
		dlls->reserve( inputDataCache.elementCount());
	for( unsigned i=0; i<inputDataCache.elementCount(); i++)
	{
//		if( inputDataCache.jumpToElement(i))
		if( inputDataCache.jumpToArrayElement(i))
		{
			MPxData* dataCache = inputDataCache.inputValue(&stat).asPluginData();
			FFControlData* ffwd = (FFControlData*)dataCache;
			if( !ffwd) 
				continue;
			if( !ffwd->control)
				continue;
			ffcs.push_back(ffwd->control);

			if( dlls)
				dlls->push_back(&ffwd->dllproc);

			/*/
			FFControlHistoryData* pHistory = ;
			if( !inputDataHistoryCache.jumpToElement(i))
			{
				// ��������
				typedef FFControlHistoryData CLASS;
				FFControlHistoryData = NULL;

				MObject newDataObject = fnDataCreator.create( MTypeId( CLASS::id ), &stat );

				MArrayDataBuilder builder(&data, i_controlHistories, i);
				MDataHandle data = builder.addElement(i);
				data.set(newDataObject);
				inputDataHistoryCache.set(builder);
			}
			/*/
		}
	}
}

void furFractal::LoadWindows( MDataBlock& data, std::vector<Math::IFFWindow*>& ffds)
{
	FFGridData* griddata = furFractal::Load_Grid(data);

	// controls
	std::vector<IFFControl*> ffcs;
	LoadControls(data, ffcs);
	for(int i=0; i<(int)ffcs.size(); i++)
	{
		if( !ffcs[i]) continue;
//		if( !ffcs[i]->isWindow()) continue;

//		ffds.push_back(ffcs[i]);
	}

}
void furFractal::LoadGrows( 
	MDataBlock& data, 
	std::vector<IFFGrow*>& ffds,
	std::vector<IFFControl*>& ctrls
	)
{
	// controls
	std::vector<IFFControl*> ffcs;
	LoadControls(data, ffcs);
	for(int i=0; i<(int)ffcs.size(); i++)
	{
		IFFControl* c = ffcs[i];
		if( !c) continue;

		for(int g=0; g<(int)c->grows.size(); g++)
		{
			IFFGrow* grow = c->grows[g];
			ffds.push_back(grow);
			ctrls.push_back(c);
		}
	}
}

void furFractal::LoadDeformers( 
	MDataBlock& data, 
	std::vector<IFFDeform*>& ffds
	)
{
	MStatus stat;
	// controls
	std::vector<IFFControl*> ffcs;
	LoadControls(data, ffcs);
	ffds.reserve(ffcs.size()*3+10);

	/*/
	{
		MArrayDataHandle inputDataCache = data.inputArrayValue(i_deforms, &stat);
		for( unsigned i=0; i<inputDataCache.elementCount(); i++)
		{
			if( inputDataCache.jumpToArrayElement(i))
			{
				MPxData* dataCache = inputDataCache.inputValue(&stat).asPluginData();
				FFDeformData* ffwd = (FFDeformData*)dataCache;
				if( !ffwd) 
					continue;
				if( !ffwd->control)
					continue;

				IFFDeform* control = ffwd->control;
				control->control = NULL;

				ffds.push_back(control);
			}
		}
	}
	/*/

	for(int i=0; i<(int)ffcs.size(); i++)
	{
		IFFControl* c = ffcs[i];
		if( !c) continue;

		c->getDeformers(ffds);
	}
	// ���������� ���������� �� ����������
	IFFControl::sortDeformers(ffds);

//	printf( "deforms:\n");
//	for(int d=0; d<(int)ffds.size(); d++)
//	{
//		printf( "deform=%d\n", ffds[d]->getPriority());
//	}
	fflush(stdout);
}


void furFractal::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	Util::FunEvalTimeCounter fetc;
	{
		Util::FunEvalTime fet(fetc);
	MStatus stat;
	MObject thisNode = thisMObject();

	MDataBlock data = forceCache();

	FFGridData* pData = Load_Grid(data);
	GridData* gridData = &pData->data;

	bool bViewGrid = MPlug( thisNode, i_viewGrid).asBool();
	bool bViewGridDots = MPlug( thisNode, i_viewGridDots).asBool();
	bool bDrawBox = MPlug( thisNode, i_drawBox).asBool();
	bool bColor = MPlug( thisNode, i_color).asBool();
	int drawWidthCnt = MPlug( thisNode, i_drawWidthCnt).asInt();
	int texType = MPlug( thisNode, i_texType).asInt();

	std::vector<Math::IFFWindow*> ffds;
	LoadWindows(data, ffds);

	std::vector<IFFDeform*> deforms;
	LoadDeformers( data, deforms);

//	MPlug plug(thisNode, i_time);
	double val;
	MPlug plug(thisNode, o_output);
	plug.getValue(val);

	MPlug widthScalePlug( thisNode, i_widthScale );
	double widthScaleValue = widthScalePlug.asDouble();

	Math::Matrix4f worldm;
	::copy( worldm, path.inclusiveMatrix());

	view.beginGL(); 

	if(bDrawBox)
	{
		Math::Box3f dirtybox = gridData->getDirtyBox();
		drawBox(dirtybox);
	}
	if( !bViewGrid)
	{
		GLfloat width;
		glGetFloatv( GL_LINE_WIDTH, &width );
		float curcolor[4];
		glGetFloatv( GL_CURRENT_COLOR, curcolor );

		FFDrawCache* drawCache = Load_DrawCache(data);

		drawCache->draw(view, bColor);

		glColor3f( curcolor[0], curcolor[1], curcolor[2]);
		glLineWidth(width);

		if( status != M3dView::kDormant)
		{
			drawCache->drawPoints();
		}
		if( bViewGridDots)
		{
			drawCache->drawInvisiblePoints();
		}
	}
	if( bViewGrid)
	{
		MObject val;
		MPlug( thisNode, i_viewGridPoint).getValue(val);
		MFnNumericData nd(val);
		MPoint pt;
		nd.getData3Double(pt.x, pt.y, pt.z);
		Math::Vec3f _pt;
		::copy(_pt, pt);

		int viewGridLevel = MPlug( thisNode, i_viewGridLevel).asInt();
		pData->drawVertexes(viewGridLevel, deforms, worldm, _pt);
	}

	// ���� �����������
	for( int w=0; w<(int)ffds.size(); w++)
	{
		if( ffds[w])
			ffds[w]->draw(worldm, *gridData);
	}

	// ��� �����������
	{
		drawLine(Math::Vec3f(0), Math::Vec3f(2, 0, 0));
		drawLine(Math::Vec3f(0), Math::Vec3f(0, 1, 0));
		glLineStipple(1, 0x8888);
		glEnable(GL_LINE_STIPPLE);
		drawLine(Math::Vec3f(0), Math::Vec3f(0, 0, 2));
		glDisable(GL_LINE_STIPPLE);
	}

	view.endGL();
	}
	displayStringD("draw=%f", fetc.sumtime);
};

void* furFractal::creator()
{
	return new furFractal();
}

MStatus furFractal::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnEnumAttribute	enumAttr;
	MFnUnitAttribute	unitAttr;
	MStatus				stat;

	try
	{
		{
			i_maxStep[0] = numAttr.create ("maxStep","mas", MFnNumericData::kInt, 500, &stat);
			::addAttribute(i_maxStep[0], atInput);

			i_maxStep[1] = numAttr.create ("maxStepRefresh","msr", MFnNumericData::kInt, 100, &stat);
			::addAttribute(i_maxStep[1], atInput);
		}
		{
			i_T = numAttr.create ("time","tm", MFnNumericData::kDouble, 10, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(20);
			::addAttribute(i_T, atInput);
		}
		{
			i_controls = typedAttr.create( "controls", "ctrs", FFControlData::id);
			typedAttr.setReadable(false);
			typedAttr.setArray(true);
			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			typedAttr.setIndexMatters(false);
			::addAttribute(i_controls, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray );
		}
		{
			i_deforms = typedAttr.create( "deforms", "idfrms", FFDeformData::id);
			typedAttr.setReadable(false);
			typedAttr.setArray(true);
			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			typedAttr.setIndexMatters(false);
			::addAttribute(i_deforms, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray );
		}
		/*/
		{
			i_growUpPropencity = numAttr.create ("growUpPropencity","gup", MFnNumericData::kDouble, 0.3, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(0.3);
			numAttr.setMax(1);
			::addAttribute(i_growUpPropencity, atInput);
		}
		{
			i_growPropencity = numAttr.create ("growPropencity","gp", MFnNumericData::kDouble, 0.01, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(0.3);
			numAttr.setMax(1);
			::addAttribute(i_growPropencity, atInput);
		}
		{
			i_growLowPropencity = numAttr.create ("growLowPropencity","glp", MFnNumericData::kDouble, 0.1, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(0.3);
			numAttr.setMax(1);
			::addAttribute(i_growLowPropencity, atInput);
		}
		{
			i_maxLevel = numAttr.create ("maxLevel","ml", MFnNumericData::kInt, 2, &stat);
			::addAttribute(i_maxLevel, atInput);
		}
		/*/
		{
			i_rebuildStartFrame = unitAttr.create("rebuildStartFrame","rsf", MTime(1, MTime::uiUnit()), &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_rebuildStartFrame, atInput);
		}

		{
			i_viewGridDots = numAttr.create ("viewGridDots","vgd", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_viewGridDots, atInput);

			i_viewGrid = numAttr.create ("viewGrid","vg", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_viewGrid, atInput);

			i_viewGridLevel = numAttr.create ("viewGridLevel","vgl", MFnNumericData::kInt, -1, &stat);
			numAttr.setSoftMin(-4);
			numAttr.setSoftMax(4);
			::addAttribute(i_viewGridLevel, atInput);

			
			i_viewGridPoint = numAttr.create ("viewGridPoint","vgpt", MFnNumericData::k3Double, 0, &stat);
			::addAttribute(i_viewGridPoint, atInput);

			i_drawBox = numAttr.create ("drawBox","drb", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_drawBox, atInput);

			i_color = numAttr.create ("colorize","clr", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_color, atInput);
		
			i_drawWidthCnt = numAttr.create ("drawWidthCnt","dwc", MFnNumericData::kInt, 0, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(2);
			::addAttribute(i_drawWidthCnt, atInput);

			i_texType = numAttr.create ("texType","tt", MFnNumericData::kInt, -1, &stat);
			numAttr.setSoftMin(-1);
			numAttr.setSoftMax(3);
			::addAttribute(i_texType, atInput);

			i_dumpThread = numAttr.create ("dumpThread","duth", MFnNumericData::kInt, -1, &stat);
			numAttr.setSoftMin(-1);
			numAttr.setSoftMax(50);
			::addAttribute(i_dumpThread, atInput);
		}
		{
			i_bPruning = numAttr.create ("pruning","pru", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_bPruning, atInput);

			i_pruningCamera = typedAttr.create( "pruningCamera", "prca", MFnData::kMatrix);
			::addAttribute(i_pruningCamera, atInput);

			i_pruningDistance[0] = numAttr.create ("pruningDistance0","prd0", MFnNumericData::kDouble, 100, &stat);
			numAttr.setSoftMin(1);
			numAttr.setSoftMax(1000);
			::addAttribute(i_pruningDistance[0], atInput);

			i_pruningDistance[1] = numAttr.create ("pruningDistance1","prd1", MFnNumericData::kDouble, 200, &stat);
			numAttr.setSoftMin(1);
			numAttr.setSoftMax(1000);
			::addAttribute(i_pruningDistance[1], atInput);

			i_pruningDistance[2] = numAttr.create ("pruningDistance2","prd2", MFnNumericData::kDouble, 400, &stat);
			numAttr.setSoftMin(1);
			numAttr.setSoftMax(1000);
			::addAttribute(i_pruningDistance[2], atInput);
		}

		/*/
		{
			i_deforms_point = typedAttr.create( "deforms_point", "dfp", MFnData::kMatrix);
			typedAttr.setReadable(false);
			typedAttr.setArray(true);
			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			typedAttr.setIndexMatters(false);
			::addAttribute(i_deforms_point, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray );
		}
		{
			i_deforms_point_base = typedAttr.create( "deforms_point_base", "dfpb", MFnData::kMatrix);
			typedAttr.setReadable(false);
			typedAttr.setArray(true);
			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			typedAttr.setIndexMatters(false);
			::addAttribute(i_deforms_point_base, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray );
		}
		{
			i_deforms_curve = typedAttr.create( "deforms_curve", "dfc", MFnData::kNurbsCurve);
			typedAttr.setReadable(false);
			typedAttr.setArray(true);
			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			typedAttr.setIndexMatters(false);
			::addAttribute(i_deforms_curve, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray );
		}
		/*/

		{
			i_renderType = enumAttr.create ("rType","rtype", 3, &stat);
			enumAttr.addField("Curve_Linear", 0);
			enumAttr.addField("Curve_Cubic", 1);
			enumAttr.addField("Mesh", 2);
			enumAttr.addField("Subdiv", 3);
			enumAttr.addField("Test", 4);
			::addAttribute(i_renderType, atInput);
		}
		{
			i_renderWidth = numAttr.create ("renderWidth","rw", MFnNumericData::kDouble, 0.5, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(2.0);
			::addAttribute(i_renderWidth, atInput);
		}
		{
			i_tesselation = numAttr.create ("tesselation","tess", MFnNumericData::kInt, 0, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(5);
			::addAttribute(i_tesselation, atInput);
		}

		{
			o_output = typedAttr.create( "grid", "gr", FFGridData::id);
			::addAttribute(o_output, atReadable|atWritable|atStorable|atCached|atHidden);

//			stat = attributeAffects( i_time, o_output );
//			stat = attributeAffects( i_seed, o_output );
//			stat = attributeAffects( i_growLowPropencity, o_output );
//			stat = attributeAffects( i_growPropencity, o_output );
//			stat = attributeAffects( i_windows, o_output );
			stat = attributeAffects( i_deforms, o_output );
			stat = attributeAffects( i_controls, o_output );
		}
		{
			i_widthScale = numAttr.create ("widthScale","ws", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(10.0);
			::addAttribute(i_widthScale, atInput);
		}
		{
			i_dimention = numAttr.create ("dimention","dim", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(2.0);
			::addAttribute(i_dimention, atInput);
		}
		{
			o_drawcache = typedAttr.create( "drawcache", "drc", FFDrawCache::id);
			::addAttribute(o_drawcache, atReadable|atUnWritable|atUnStorable|atCached|atHidden);

			stat = attributeAffects( i_color, o_drawcache );
			stat = attributeAffects( i_drawWidthCnt, o_drawcache );
			stat = attributeAffects( i_renderWidth, o_drawcache );
			stat = attributeAffects( i_texType, o_drawcache );
			stat = attributeAffects( i_dumpThread, o_drawcache );
			stat = attributeAffects( i_pruningCamera, o_drawcache );
			stat = attributeAffects( i_pruningDistance[0], o_drawcache );
			stat = attributeAffects( i_pruningDistance[1], o_drawcache );
			stat = attributeAffects( i_pruningDistance[2], o_drawcache );
			stat = attributeAffects( i_T, o_drawcache );
			stat = attributeAffects( i_controls, o_drawcache );
			stat = attributeAffects( i_deforms, o_drawcache );
			stat = attributeAffects( i_viewGrid, o_drawcache );
			stat = attributeAffects( i_drawBox, o_drawcache );
			stat = attributeAffects( i_widthScale, o_drawcache );
		}

		{
			o_mesh = typedAttr.create( "outmesh", "ome", MFnData::kMesh);
			::addAttribute(o_mesh, atReadable|atWritable|atUnStorable|atCached|atConnectable|atHidden);

			stat = attributeAffects( i_renderWidth, o_mesh );
			stat = attributeAffects( i_tesselation, o_mesh );
			stat = attributeAffects( i_T, o_mesh );
			stat = attributeAffects( i_controls, o_mesh );
			stat = attributeAffects( i_deforms, o_mesh);
		}

		// ����������
		{
			o_venCount = numAttr.create ("venCount","svk", MFnNumericData::kInt, 0, &stat);
			::addAttribute(o_venCount, atReadable|atUnWritable|atConnectable|atStorable);

			o_segCount = numAttr.create ("segCount","ssc", MFnNumericData::kInt, 0, &stat);
			::addAttribute(o_segCount, atReadable|atUnWritable|atConnectable|atStorable);

			o_estimationTime = numAttr.create ("estimationTime","set", MFnNumericData::kDouble, 0, &stat);
			::addAttribute(o_estimationTime, atReadable|atUnWritable|atConnectable|atStorable);
		}
		

		if( !MGlobal::sourceFile("AEfurFractalNeoTemplate.mel"))
		{
			displayString("error source AEfurFractalNeoTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}


