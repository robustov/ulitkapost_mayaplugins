#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: FFGrowLocatorNode.h
//
// Dependency Graph Node: FFGrowLocator

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include <maya/MFnMesh.h> 

#include "FFGrowData.h"
#include "Math/MeshControl.h"

class FFGrowLocator : public MPxNode
{
public:
	FFGrowLocator();
	virtual ~FFGrowLocator(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	///
	virtual MStatus setDependentsDirty( const MPlug& plug, MPlugArray& plugArray);

	static void* creator();
	static MStatus initialize();

	MStatus Build_Control( const MPlug& plug, MDataBlock& data );

	void LoadSource( 
		MDataBlock& data, 
		std::vector<int>& srcGrows
		);

public:

	// render
	static MObject entrypoint;
	static MObject startPositions, startNormals;

	static MObject o_grow;
	static MObject o_growId;
	static MObject i_sourceGrows;

public:
	static MTypeId id;
	static const MString typeName;
};

