//
// Copyright (C) 
// File: FFDeformModiferDataCmd.cpp
// MEL Command: FFDeformModiferData

#include "FFDeformModiferData.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <sstream>


FFDeformModiferData::FFDeformModiferData()
{
}
FFDeformModiferData::~FFDeformModiferData()
{
}

MTypeId FFDeformModiferData::typeId() const
{
	return FFDeformModiferData::id;
}

MString FFDeformModiferData::name() const
{ 
	return FFDeformModiferData::typeName; 
}

void* FFDeformModiferData::creator()
{
	return new FFDeformModiferData();
}

void FFDeformModiferData::copy( const MPxData& other )
{
	const FFDeformModiferData* arg = (const FFDeformModiferData*)&other;
	DATACOPY(modifer)
}

MStatus FFDeformModiferData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("FFDeformModiferData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus FFDeformModiferData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus FFDeformModiferData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus FFDeformModiferData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void FFDeformModiferData::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		stream >> modifer;
	}
}

